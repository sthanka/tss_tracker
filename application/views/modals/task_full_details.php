    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a></div>
                    <h4 class="panel-title">Task Overview</h4>
                </div>
                <?php //echo "<pre>"; print_r($task_details) ; exit;?>
                <div class="panel-body auto-height">
                    <div class="col-sm-12 clearfix border-bottom">
                        <div class="col-sm-2">
                            <div class="form-view">
                                <p name="Project"><?= $task_details[0]['project_name'] ?></p>
                                <label>Project</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?= $task_details[0]['task_name'] ?></div>
                                <label>Task</label>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?= $task_details[0]['estimated_time'] ?></div>
                                <label>Estimated Time</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?=$task_details[0]['additional_time']?></div>
                                <label>Additional Time</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?=$task_details[0]['log_time']?></div>
                                <label>Log Time</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?=$task_details[0]['remain_time']?></div>
                                <label>Remaining Time</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix padding0">
                        <div class="">
                            <ul class="nav tsk-detail-tabs" id="tsk-detail-tabs" role="tablist">
                                <h4></h4>

                                <li role="presentation" class="active"><a href="#DepttskDetails" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Department Time</a></li>
                                <li role="presentation" class=""><a href="#tskDetails" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Log Time</a></li>
                                <li role="presentation" onclick="getLogTimeList()" class=""><a href="#DetailedtskDetails" aria-controls="settings" role="tab"
                                                                          data-toggle="tab">Detailed Log Time</a></li>
                                <li role="presentation"><a href="#tskTimeline" aria-controls="profile" role="tab"
                                                           data-toggle="tab">Timeline</a></li>
                                <li role="presentation" ><a href="#tskChecklist" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Checklist</a></li>
                                <li role="presentation"><a href="#tskUseCase" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Use Case</a></li>
                                <li role="presentation"><a href="#tskAttachment" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Attachment</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="DepttskDetails" style="padding-top: 0px;">
                                    <h4 class="margin0" style="padding-top: 5px;">Log Time</h4>
                                    <div class="col-sm-12" style="padding-left: 0px;padding-top: 10px;">
                                        <?php
                                        //echo "<pre>"; print_r($tskDetails); exit;
                                        if(count($task_department) > 0){
                                        ?>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Department</th>
                                                    <th>Estimated time</th>
                                                    <th>Alloted time</th>
                                                    <th>Additional time</th>
                                                    <th>Status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($task_department as $k=>$v){ ?>
                                                <tr>
                                                    <td><?=$v['department_name']?></td>
                                                    <td><?=$v['esti_time']?></td>
                                                    <td><?=$v['est_time']?></td>
                                                    <td><?=$v['add_time']?></td>
                                                    <td><?=$v['task_status']?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php } else { ?>
                                            <div class="no-nodetails">No data found</div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="tskDetails" style="padding-top: 0px;">
                                    <h4 class="margin0" style="padding-top: 5px;">Log Time</h4>
                                    <div class="col-sm-12" style="padding-left: 0px;padding-top: 10px;">
                                        <?php
                                        //echo "<pre>"; print_r($tskDetails); exit;
                                        if(count($tskDetails) > 0){
                                        ?>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Department</th>
                                                    <!--<th>Start Date</th>
                                                    <th>End Date</th>-->
                                                    <th>Estimated Time</th>
                                                    <th>Additional Time</th>
                                                    <th>Log Time</th>
                                                    <th>Status</th>
                                                    <th>Completed Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($tskDetails as $k=>$v){ ?>
                                                <tr>
                                                    <td><?=$v['name']?></td>
                                                    <td><?=$v['department_name']?></td>
                                                    <!--<td><?/*=$v['start_date']*/?></td>
                                                    <td><?/*=$v['end_date']*/?></td>-->
                                                    <td><?=$v['est_time'] ?></td>
                                                    <td><?=$v['add_time'] ?></td>
                                                    <td><?=$v['log_time'] ?></td>
                                                    <td><?=$v['status'] ?></td>
                                                    <td><?php if($v['status'] == 'completed'){ echo $v['actual_end_date']; }else{ echo ' -- '; } ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php } else { ?>
                                            <div class="no-nodetails">No data found</div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="DetailedtskDetails" style="padding-top: 0px;">
                                    <h4 class="margin0" style="padding-top: 5px;">Log Time</h4>
                                    <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                        <div class="grid-details-table">
                                            <div class="grid-details-table-header">
                                                <h3 >Log Time</h3>
                                                <input type="text" class=" pull-right logtimesearch" style="padding-top: 5px;"/>
                                                <a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                                            </div>
                                            <div class="grid-details-table-content">
                                                <div class="tbl_wrapper border0 ">
                                                    <table id="time-logs-info-table" class="table-responsive" ></table>
                                                    <div id="time-logs-info-table-page"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskTimeline">
                                    <h4 class="margin0" style="padding-top: 5px;">TimeLine</h4>
                                    <ul class="timele-history" style="height: 300px;overflow: auto;">
                                        <?php if(count($time_line)>0){ foreach($time_line as $line){ ?>

                                            <li class="timele-history-wrap">
                                                <div class="timele-history-list">
                                                    <div class="timele-history-list-date"><?=date('d-M-Y',strtotime($line['created_date_time']))?></div>
                                                    <div class="timele-history-list-content">
                                                        <div class="timele-history-list-head">
                                                            <span><?=date('h:i a',strtotime($line['created_date_time']))?></span>
                                                            <!--<span class="blue"><?/*= $line['user_name'] */?> </span>-->
                                                            <span class="blue"><?= $line['department_name'] ?> </span>
                                                            <span class="blue"><?= $line['description'] ?> </span>
                                                            <!--<span>UX</span>--></div>
                                                        <!--<p><?/*=  $line['description'] */?></p>-->
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } } else { ?>
                                        <li class="timele-history-wrap">No Timeline</li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskUseCase">
                                    <h4 class="margin0">Use case</h4>
                                    <div class="col-sm-12 pb20">
                                        <ul class="u-list pl20 pl0" >

                                        <li class="no-usecase"><?=$use_cases==""?'---':$use_cases?></li>

                                        </ul>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskAttachment">
                                    <h4 class="margin0">Attachments</h4>
                                    <div class="col-sm-12 pb20">
										<div class="col-sm-12 pb20 pl0">
                                            <?php if(count($attachment)>0){ ?>
                                                <ul class="attachment-list-view">
                                                    <?php
                                                    foreach($attachment as $att){ ?>
                                                        <li><a href="<?= WEB_BASE_URL ?>uploads/<?= $att['attachment'] ?>" download ><?= $att['attachment'] ?></a><a href="<?= WEB_BASE_URL ?>uploads/<?= $att['attachment'] ?>" download  ><i class="fa fa-download"></i></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php }else{ ?>
                                                <ul class="attachment-list-view">
                                                    <li class="no-attachment">No Attachments</li>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskChecklist" style="padding-top: 0px;">
                                    <h4 class="margin0">Checklist</h4>
                                    <div class="col-sm-12 padding0">
                                        <?php if(count($checkList)>0){
                                            for($s=0;$s<count($checkList);$s++){ ?>
                                            <div class="col-sm-12 padding0 pl0 tsk-users-chklist-wrapper">
                                                <div class="col-sm-12">
                                                    <span class="col-sm-4"><u><?=$checkList[$s]['user_name']?></u></span>
                                                <?php if($checkList[$s]['approved_by']) { ?><span class="col-sm-4">Approval By: <u><?=$checkList[$s]['approved_by']?></u></span><?php } ?>
                                                    <?php if($checkList[$s]['approved_date']) { ?><span class="col-sm-4">Approval Date: <u><?=$checkList[$s]['approved_date']?></u></span><?php } ?>
                                                </div>
                                                <table class="table table-bordered" style="margin-left: 10px; margin: 10px;">
                                                    <?php if(count($checkList[$s]['check_list'])>0){ foreach($checkList[$s]['check_list'] as $k=>$v){ ?>
                                                    <tr>
                                                        <td width="50%"><?= $v['checklist_name'] ?></td>
                                                        <td><input type="radio" disabled <?=$v['checklist_value']==1?'checked="checked"':''?> > Verified</td>
                                                        <td><input type="radio" disabled  <?=$v['checklist_value']==0?'checked="checked"':''?> > N/A</td>
                                                    </tr>
                                                    <?php } } else{ ?>
                                                        <li class="no-attachment">No Checklist found</li>
                                                    <?php } ?>
                                                </table>
                                            </div>
                                        <?php } }?>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var $refTblLoggrid = $('#time-logs-info-table');
        $(document).ready(function () {

        });

        function getLogTimeList(){

            var task_id = '<?= $task_details[0]['id_project_task']?>';

                $refTblLoggrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/getLogTimeList?view=1&task_id='+task_id,
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    extSearchField: '.logtimesearch',
                    colNames: ['Id', 'project_task_id','User Name', 'Task Type', 'Description', 'Start Time', 'End Time', 'Total time','Date'],
                    colModel: [
                        { name: 'log_time_id', index: 'log_time_id', hidden: true ,key:true},
                        { name: 'project_task_id', index: 'project_task_id',  hidden: true},
                        { name: 'user_name', index: 'user_name'},
                        { name: 'task_type_name', index: 'task_type_name' },
                        { name: 'comments', index: 'comments' },
                        { name: 'start_time', index: 'start_time' },
                        { name: 'end_time', index: 'end_time' },
                        { name: 'duration', index: 'duration' },
                        { name: 'created_date_time', index: 'created_date_time' },
                    ],
                    loadComplete: function () {
                        if(jQuery("#time-logs-info-table").getDataIDs().length==0){
                            //$("#gbox_time-logs-info-table").parent().parent().parent().hide();
                        }else{
                            //$(".grid-details-table").show();
                        }
                    },
                    pager: 'time-logs-info-table-page'
                }).navGrid('#time-logs-info-table-page', {
                    edit: false, add: false, del: false, search: false, refresh: true,
                });


        }
    </script>
