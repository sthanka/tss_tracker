<div id="vtab01" class="tab-pane padding0 active">
    <div class="hScroll col-sm-12 clearfix clearboth padding0">
        <div class="tasks-list">
            <?php if($this->session->userdata('user_type_id')==2){ ?>
            <div class="tasks-col">
                <h3 class="normal-heading">Unassigned</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <?php for ($s = 0; $s < count($unassigned); $s++) { ?>
                            <div class="task-block">
                                <p class="">
                                    <a class="link"
                                       href="<?= WEB_BASE_URL ?>index.php/Project/taskDetails/<?= $unassigned[$s]['parent_id'] ?>"><?= $unassigned[$s]['task_name'] ?> (<?=$unassigned[$s]['parent_name'] ?>)</a>
                                    <a class="pull-right task-actions" onclick="showAction(this)">
                                        <i class="fa fa-cog"></i>
                                    </a>

                                </p>
                                <label class="module-name"><?= $unassigned[$s]['module_name'] ?></label>
                                <p class="task-dates">
                                                            <span><?= date('d-m-Y', strtotime($unassigned[$s]['start_date'])) ?>
                                                                <i>Start Date</i>
                                                            </span>
                                                            <span><?= date('d-m-Y', strtotime($unassigned[$s]['end_date'])) ?>
                                                                <i>End Date</i>
                                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $unassigned[$s]['estimated_time'] ?></b></p>
                                <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>0</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <?php $count = $this->Project_modal->getAttachmentsCount(array('project_task_id' => $unassigned[$s]['id_project_task'])); ?>
                                        <b><?=$count[0]['total']?></b>
                                    </div>
                                    <!--<div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                        <p><?php if(empty($unassigned)){ echo "No tasks exists"; } ?></p>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="tasks-col">
                <h3 class="normal-heading">Assigned</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <?php for ($s = 0; $s < count($assigned); $s++) { ?>
                            <div class="task-block actions-exists">
                                <p class="">
                                    <a class="link"
                                       href="<?= WEB_BASE_URL ?>index.php/Project/taskDetails/<?= $assigned[$s]['parent_id'] ?>"><?= $assigned[$s]['task_name'] ?> (<?=$assigned[$s]['parent_name']?>)</a>
                                    <a class="pull-right task-actions"  onclick="showAction(this)">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                    <ul class="actions" style="" >
                                        <li>
                                            <a data-task-id="" onclick="changeTaskStatus('<?=$assigned[$s]['id_task_workflow']?>','accepted','<?=$assigned[$s]['project_id']?>')" href="#">Accept</a>

                                        </li>
                                        <li><a data-task-id="" onclick="changeTaskStatus('<?=$assigned[$s]['id_task_workflow']?>','reject','<?=$assigned[$s]['project_id']?>')" href="#">Reject</a></li>
                                    </ul>
                                </p>
                                <label class="module-name"><?= $assigned[$s]['module_name'] ?></label>
                                <label class="module-name">Assigned to <?= $assigned[$s]['email'] ?></label>
                                <p class="task-dates">
                                                            <span><?= date('d-m-Y', strtotime($assigned[$s]['start_date'])) ?>
                                                                <i>Start Date</i>
                                                            </span>
                                                            <span><?= date('d-m-Y', strtotime($assigned[$s]['end_date'])) ?>
                                                                <i>End Date</i>
                                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $assigned[$s]['current_estimated_time'] ?></b></p>
                                <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>1</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <?php $count = $this->Project_modal->getAttachmentsCount(array('project_task_id' => $assigned[$s]['id_project_task'])); ?>
                                        <b><?=$count[0]['total']?></b>
                                    </div>
                                    <!--<div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <p><?php if(empty($assigned)){ echo "No tasks exists"; } ?></p>
                </div>
            </div>
            <div class="tasks-col">
                <h3 class="normal-heading">To Do</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <?php for ($s = 0; $s < count($to_do); $s++) { ?>
                            <div class="task-block actions-exists">
                                <p class="">
                                    <a class="link"
                                       href="<?= WEB_BASE_URL ?>index.php/Project/taskDetails/<?= $to_do[$s]['parent_id'] ?>"><?= $to_do[$s]['task_name'] ?> (<?=$to_do[$s]['parent_name']?>)</a>
                                    <a class="pull-right task-actions" onclick="showAction(this)">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                    <ul class="actions" style="">
                                        <li>
                                            <a data-task-id="" onclick="changeTaskStatus('<?=$to_do[$s]['id_task_workflow']?>','hold','<?=$to_do[$s]['project_id']?>')" href="#">Hold</a>
                                        </li>
                                        <li>
                                            <a data-task-id="" onclick="changeTaskStatus('<?=$to_do[$s]['id_task_workflow']?>','progress','<?=$to_do[$s]['project_id']?>')" href="#">Start</a>
                                        </li>
                                    </ul>
                                </p>
                                <label class="module-name"><?= $to_do[$s]['module_name'] ?></label>
                                <label class="module-name">Assigned to <?= $to_do[$s]['user_name'] ?></label>
                                <p class="task-dates">
                                                            <span><?= date('d-m-Y', strtotime($to_do[$s]['start_date'])) ?>
                                                                <i>Start Date</i>
                                                            </span>
                                                            <span><?= date('d-m-Y', strtotime($to_do[$s]['end_date'])) ?>
                                                                <i>End Date</i>
                                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $to_do[$s]['current_estimated_time'] ?></b></p>
                                <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>1</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <?php $count = $this->Project_modal->getAttachmentsCount(array('project_task_id' => $to_do[$s]['id_project_task'])); ?>
                                        <b><?=$count[0]['total']?></b>
                                    </div>
                                    <!--<div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                        <p><?php if(empty($to_do)){ echo "No tasks exists"; } ?></p>
                    </div>
                </div>
            </div>
            <div class="tasks-col">
                <h3 class="normal-heading">In Progress</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <?php for ($s = 0; $s < count($progress); $s++) { ?>
                            <div class="task-block  actions-exists">
                                <p class="">
                                    <a class="link"
                                       href="<?= WEB_BASE_URL ?>index.php/Project/taskDetails/<?= $progress[$s]['parent_id'] ?>"><?= $progress[$s]['task_name'] ?> (<?=$progress[$s]['parent_name']?>)</a>
                                    <a class="pull-right task-actions" onclick="showAction(this)">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                <ul class="actions" style="" >
                                    <li>
                                        <a data-task-id="" onclick="changeTaskStatus('<?=$progress[$s]['id_task_workflow']?>','hold','<?=$progress[$s]['project_id']?>')" href="#">Hold</a>
                                    </li>
                                    <li>
                                        <a data-task-id="" onclick="changeTaskStatus('<?=$progress[$s]['id_task_workflow']?>','completed','<?=$progress[$s]['project_id']?>')" href="#">completed</a>
                                    </li>
                                </ul>
                                </p>
                                <label class="module-name"><?= $progress[$s]['module_name'] ?></label>
                                <label class="module-name">Assigned to <?= $progress[$s]['user_name'] ?></label>
                                <p class="task-dates">
                                                            <span><?= date('d-m-Y', strtotime($progress[$s]['start_date'])) ?>
                                                                <i>Start Date</i>
                                                            </span>
                                                            <span><?= date('d-m-Y', strtotime($progress[$s]['end_date'])) ?>
                                                                <i>End Date</i>
                                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $progress[$s]['current_estimated_time'] ?></b></p>
                                <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>1</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <?php $count = $this->Project_modal->getAttachmentsCount(array('project_task_id' => $progress[$s]['id_project_task'])); ?>
                                        <b><?=$count[0]['total']?></b>
                                    </div>
                                    <!--<div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                        <p><?php if(empty($progress)){ echo "No tasks exists"; } ?></p>
                    </div>
                </div>
            </div>
            <div class="tasks-col">
                <h3 class="normal-heading">Done</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0 ">
                        <?php for ($s = 0; $s < count($completed); $s++) { ?>
                            <div class="task-block">
                                <p class="">
                                    <a class="link"
                                       href="<?= WEB_BASE_URL ?>index.php/Project/taskDetails/<?= $completed[$s]['parent_id'] ?>"><?= $completed[$s]['task_name'] ?> (<?=$completed[$s]['parent_name']?>)</a>
                                    <a class="pull-right task-actions">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                </p>
                                <label class="module-name"><?= $completed[$s]['module_name'] ?></label>
                                <label class="module-name">Assigned to <?= $completed[$s]['user_name'] ?></label>
                                <p class="task-dates">
                                                            <span><?= date('d-m-Y', strtotime($completed[$s]['start_date'])) ?>
                                                                <i>Start Date</i>
                                                            </span>
                                                            <span><?= date('d-m-Y', strtotime($completed[$s]['end_date'])) ?>
                                                                <i>End Date</i>
                                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $completed[$s]['current_estimated_time'] ?></b></p>
                                <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>1</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <?php $count = $this->Project_modal->getAttachmentsCount(array('project_task_id' => $completed[$s]['id_project_task'])); ?>
                                        <b><?=$count[0]['total']?></b>
                                    </div>
                                    <!--<div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                        <p><?php if(empty($completed)){ echo "No tasks exists"; } ?></p>
                    </div>
                </div>
            </div>
            <div class="tasks-col">
                <h3 class="normal-heading">On Hold</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <?php for ($s = 0; $s < count($hold); $s++) { ?>
                            <div class="task-block actions-exists">
                                <p class="">
                                    <a class="link" href="<?=WEB_BASE_URL?>index.php/Project/taskDetails/<?= $hold[$s]['parent_id'] ?>"><?= $hold[$s]['task_name'] ?> (<?=$hold[$s]['parent_name']?>)</a>
                                    <a class="pull-right task-actions" onclick="showAction(this)">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                    <ul class="actions" style="">
                                        <li><?php $id_p_t = $hold[$s]['id_project_task'] ?>
                                            <a data-task-id="" onclick="changeTaskStatus('<?=$hold[$s]['id_task_workflow']?>','progress','<?=$hold[$s]['project_id']?>')" href="#">Progress</a>
                                        </li>
                                    </ul>
                                </p>
                                <label class="module-name"><?= $hold[$s]['module_name'] ?></label>
                                <label class="module-name">Assigned to <?= $hold[$s]['user_name'] ?></label>
                                <p class="task-dates">
                                                            <span><?= date('d-m-Y', strtotime($hold[$s]['start_date'])) ?>
                                                                <i>Start Date</i>
                                                            </span>
                                                            <span><?= date('d-m-Y', strtotime($hold[$s]['end_date'])) ?>
                                                                <i>End Date</i>
                                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $hold[$s]['current_estimated_time'] ?></b></p>
                                <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>1</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <?php $count = $this->Project_modal->getAttachmentsCount(array('project_task_id' => $hold[$s]['id_project_task'])); ?>
                                        <b><?=$count[0]['total']?></b>
                                    </div>
                                    <!--<div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                        <p><?php if(empty($hold)){ echo "No tasks exists"; } ?></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    TssLib.docReady(function(){
        $('.tasks-list').width($('.tasks-col').length * 205);
        $('.vScroll').css('max-height', 400)
        $('.vScroll').mCustomScrollbar({
            theme: "dark",
            scrollbarPosition: 'inside',
            autoHideScrollbar: true,
            scrollButtons: {enable: true},
            scrollInertia: 3,
            autoExpandScrollbar: false
        });
        $('.hScroll').mCustomScrollbar({
            axis: "x",
            theme: "dark",
            scrollButtons: {
                enable: true,
                scrollAmount: 100
            },
            scrollInertia: 10,
            scrollEasing: "easeInOut",
            autoExpandScrollbar: true
        });
    });
</script>