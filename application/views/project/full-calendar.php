<div id="taskDetails" class="modal-wrapper" >
    <div id="detailsContent">

    </div>
</div>
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3></h3>
        </div>
        <div>
            <div id="workLoadReport"></div>
        </div>
        <div class="pt20 clearfix">
            <div class="col-sm-9">
                <div id='calendar'></div>
            </div>
            <div class="col-sm-3 padding0">
                <div class="status-tab filters-wrap">
                    <ul class="list-inline mb0">
                    <li><a href="javascript:;" class="color-green" onclick="getUserTasks('today',this);">Today</a></li>
                    <li><a href="javascript:;" class="" onclick="getUserTasks('pending',this);">Pending</a></li>
                    <li><a href="javascript:;" class="" onclick="getUserTasks('progress',this);">In-Process</a></li>
                    <li><a href="javascript:;" class="" onclick="getUserTasks('hold',this);">Hold</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 padding0">
                <div class="actions-view-wrapper">
                    <h4 class="tsk-for-week-heading">Tasks for the week <?php if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead') == 1){ ?><a onclick="showTimer(this)" class="link pull-right"><i class="fa fa-ellipsis-v font-18"></i></a><?php } ?>
                        <ul class="grid-actions">
                            <li><a onclick="openOtherActivitiesModal(this)">Other Activities</a></li>
                        </ul>
                    </h4>
                    <div class="col-sm-12 padding5">
                        <input type="text" placeholder="Search task" class="taskSearch" style="width: 100%;">
                    </div>
                    <div class="week-task-details"><span class="">Allocated Hours :</span><span id="allocatedHours">--</span></div>
                    <div class="week-task-details"><span class="">#Projects:</span><span id="projects">--</span></div>
                    <div class="week-task-details"><span class="">#Tasks :</span><span id="taskCount">--</span></div>

                    <ul class="actions-list">
                        <div class=" ">
                                <table id="all-tasks-list" class="table-responsive mt-20"></table>
                                <div id="tasks-list-page"></div>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="showByProjectDetails" class="modal-wrapper" style="display:none">
    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#">
                            <i class="glyphicon glyphicon-remove"></i></a></div>
                    <h4 class="panel-title">Show By</h4>
                </div>
                <div class="panel-body auto-height">
                    <div class="col-sm-12 padding0">
                        <div class="col-sm-6 pl0">
                            <h4 class="margin0 f14">Projects</h4

                            <div class="col-sm-12 padding0 tsk-chklist-wrapper">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">Q-Lana
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">Miles
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">IP Metro
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">Okaridge
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 pr0">
                            <h4 class="margin0 f14">Status</h4

                            <div class="col-sm-12 padding0 tsk-chklist-wrapper">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">Completed
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">New
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">In Progress
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">On Hold
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 clearfix text-center pb20">
                            <button class="button-common">Update</button>
                            <button class="button-color">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="assignedTaskList" class="modal-wrapper" style="display:none">
    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" >
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                            class="glyphicon glyphicon-remove"></i></a></div>
                <h4 class="panel-title">Task List</h4>
            </div>
            <div id="assignedTaskList-table" class="panel-body auto-height">

            </div>
        </div>

    </div>
</div>

<div id="otherActivitiesDetails" class="modal-wrapper" style="display:none">
    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                class="glyphicon glyphicon-remove"></i></a></div>
                    <h4 class="panel-title">Other Activities</h4>
                </div>
                <div class="panel-body auto-height">
                    <div class="col-sm-12 clearfix padding0">
                        <div class="">
                            <!-- Nav tabs -->
                            <ul class="nav tsk-detail-tabs" role="tablist">
                                <!--<li role="presentation" class="active"><a onclick="callOtherActivities('break')"   href="#tskBreaks" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Breaks</a></li>-->
                                <?php if($this->session->userdata('is_lead') == 1){ ?>
                                    <li role="presentation" ><a onclick="callOtherActivities('meeting')"  href="#tskMeetings" aria-controls="home"
                                                                              role="tab" data-toggle="tab">Meetings</a></li>
                                    <li role="presentation" ><a onclick="callOtherActivities('call')" href="#tskClientCall" aria-controls="messages" role="tab"
                                                               data-toggle="tab">Client Call</a></li>
                                    <li role="presentation" ><a onclick="callOtherActivities('interview')" href="#tskInterviews" aria-controls="messages" role="tab"
                                                                data-toggle="tab">Interview</a></li>
                                <?php } ?>


                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane padding0" id="tskMeetings">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Meetings</h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content clearfix pt20">
                                            <form id="other_meeting_log_time" class="form-horizontal align-error" method="POST" >
                                                <div class="col-sm-12 padding0 clearfix pt10">
                                                    <div class="col-sm-6 pl0">
                                                        <div class="form-group bt-default">
                                                            <label>Type</label>
                                                            <select name="task_type"id="meeting_types" class="rval" >
                                                                <option value="">select Type</option>
                                                            </select>
                                                        </div>
                                                        <div><a onclick="closeOtherActivityModal('meeting')" href="#">Create new meeting type</a></a></div>
                                                    </div>
                                                    <div class="col-sm-6 pr0">
                                                        <div class="form-group bt-default">
                                                            <label>Employees</label>
                                                            <select multiple name="user_id" id="meeting_users" class="rval">
                                                                <option value="">select user</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input type="text" class="form-control rval tssDatepicker" name="date" enddate="0" id="meeting_date"/>
                                                            </div>
                                                            <label class="control-label"><span class="req-str">*</span>Date:</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <h4 class="margin0">Time Log</h4>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker">
                                                                <input id="meeting_start_time" name="start_time" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1 text-center">
                                                        to
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker timepicker">
                                                                <input id="meeting_end_time" name="end_time" type="text"  type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 padding0">
                                                    <div class="form-group bt-default">
                                                        <h4 class="margin0">Description</h4>
                                                        <textarea name="comments" id="comments" class="form-control comments"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 mt10 clearfix">
                                                    <div class="text-center">
                                                        <input class="button-common" type="submit"  value="Save" >
                                                        <a class="button-common"  data-dismiss="modal" >Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="grid-details-table">
                                                <div class="grid-details-table-content">
                                                    <div class="tbl_wrapper border0 ">
                                                        <table id="time-logs-meeting-table" class="table-responsive" style="width: 800px;"></table>
                                                        <div id="time-logs-meeting-table-page"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane padding0" id="tskClientCall">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Client Calls</h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content clearfix pt20">
                                            <form id="other_call_log_time" class="form-horizontal align-error" method="POST" >
                                                <div class="col-sm-12 padding0 clearfix pt10">
                                                    <div class="col-sm-6 pl0">
                                                        <div class="form-group bt-default">
                                                            <label>Type</label>
                                                            <select name="task_type" id="call_types" class="rval" >
                                                                <option value="">Select Type</option>
                                                            </select>
                                                        </div>
                                                        <div><a onclick="closeOtherActivityModal('call')" href="#">Create new call type</a></a></div>
                                                    </div>
                                                    <div class="col-sm-6 pr0">
                                                        <div class="form-group bt-default">
                                                            <label>Employees</label>
                                                            <select multiple name="user_id" id="call_users" class="rval" ></select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input type="text" class="form-control rval tssDatepicker" name="date" enddate="0" id="call_date"/>
                                                            </div>
                                                            <label class="control-label"><span class="req-str">*</span>Date:</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <h4 class="margin0">Time Log</h4>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker">
                                                                <input id="call_start_time" name="start_time" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1 text-center">
                                                        to
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker timepicker">
                                                                <input id="call_end_time" name="end_time" type="text"  type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 padding0">
                                                    <div class="form-group bt-default">
                                                        <h4 class="margin0">Description</h4>
                                                        <textarea name="comments" id="comments" class="form-control comments"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 mt10 clearfix">
                                                    <div class="text-center">
                                                        <input class="button-common" type="submit"  value="Save" >
                                                        <a class="button-common"  data-dismiss="modal" >Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="grid-details-table">
                                                <div class="grid-details-table-content">
                                                    <div class="tbl_wrapper border0 ">
                                                        <table id="time-logs-call-table" class="table-responsive" style="width: 800px;"></table>
                                                        <div id="time-logs-call-table-page"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <!--<div role="tabpanel" class="tab-pane active padding0" id="tskBreaks">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Break</h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content clearfix pt20">
                                            <form id="other_break_log_time" class="form-horizontal align-error" method="POST" >
                                                <div class="col-sm-12 padding0 clearfix pt10">
                                                    <div class="col-sm-6 pl0">
                                                        <div class="form-group bt-default">
                                                            <select name="task_type"id="break_types" class="rval" >
                                                                <option value="">select Type</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input type="text" class="form-control rval tssDatepicker" name="date" enddate="0" id="break_date"/>
                                                            </div>
                                                            <label class="control-label"><span class="req-str">*</span>Date:</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <h4 class="margin0">Time Log</h4>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker">
                                                                <input id="break_start_time" name="start_time" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1 text-center">
                                                        to
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker">
                                                                <input id="break_end_time" name="end_time" type="text"  type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 padding0">
                                                    <div class="form-group bt-default">
                                                        <h4 class="margin0">Description</h4>
                                                        <textarea name="comments" id="comments" class="form-control comments"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 mt10 clearfix">
                                                    <div class="text-center">
                                                        <input class="button-common" type="submit"  value="Save" >
                                                        <a class="button-common"  data-dismiss="modal" >Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="grid-details-table">
                                                <div class="grid-details-table-content">
                                                    <div class="tbl_wrapper border0 ">
                                                        <table id="time-logs-break-table" class="table-responsive" style="width: 800px;"></table>
                                                        <div id="time-logs-break-table-page"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane padding0" id="tskInterviews">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Interview</h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content clearfix pt20">
                                            <form id="other_interview_log_time" class="form-horizontal align-error" method="POST" >
                                                <div class="col-sm-12 padding0 clearfix pt10">
                                                    <div class="col-sm-6 pl0">
                                                        <div class="form-group bt-default">
                                                            <label>Position</label>
                                                            <select name="task_type" id="interview_types" class="rval" >
                                                                <option value="">Select type</option>
                                                            </select>
                                                        </div>
                                                        <div><a onclick="closeOtherActivityModal('interview')" href="#">Create new interview type</a></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input type="text" class="form-control rval tssDatepicker" name="date" enddate="0" id="interview_date"/>
                                                            </div>
                                                            <label class="control-label"><span class="req-str">*</span>Date:</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix padding0 mt10">
                                                    <h4 class="margin0">Time Log</h4>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker">
                                                                <input id="interview_start_time" name="start_time" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1 text-center">
                                                        to
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group bt-default">
                                                            <div class="bootstrap-timepicker">
                                                                <input id="interview_end_time" name="end_time" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 padding0">
                                                    <div class="form-group bt-default">
                                                        <h4 class="margin0">Description</h4>
                                                        <textarea name="comments" id="comments" class="form-control comments"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 mt10 clearfix">
                                                    <div class="text-center">
                                                        <input class="button-common" type="submit"  value="Save" >
                                                        <a class="button-common"  data-dismiss="modal" >Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="grid-details-table">
                                                <div class="grid-details-table-content">
                                                    <div class="tbl_wrapper border0 ">
                                                        <table id="time-logs-interview-table" class="table-responsive" style="width: 800px;"></table>
                                                        <div id="time-logs-interview-table-page"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var reference = "Allotted";
    var $taskGrid = $('#all-tasks-list');
    var callEvents = [];
    var initOnce = 0;
    var meetingTbltype = 0;
    var breakTbltype = 0;
    var interviewTbltype = 0;
    var callTbltype = 0;
    var cal = '';
    function closeOtherActivityModal(url){
        TssLib.closeModal({width: 1024}, $('#otherActivitiesDetails').html());
        window.location="<?=site_url('Task_types')?>/"+url;
    }
    function callLoadEvents(){
        var start_date = new Date(cal.fullCalendar('getView').start._d);
        var end_date = new Date(cal.fullCalendar('getView').end._d);
        postJsonAsyncWithBaseUrl("project/events", {start_date:(start_date.getMonth() + 1) + '/' + start_date.getDate() + '/' +  start_date.getFullYear(),end_date:(end_date.getMonth() + 1) + '/' + end_date.getDate() + '/' +  end_date.getFullYear()}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    callEvents = result.data.data;
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', callEvents);
                    $('#calendar').fullCalendar('rerenderEvents' );
                    $taskGrid.trigger('reloadGrid');
                    loadUserWorkLoad();
                }
            }
        });
        //get Holidays
        postJsonAsyncWithBaseUrl("Holiday/getListOfHolidaysByDateRange", {start_date:(start_date.getMonth() + 1) + '/' + start_date.getDate() + '/' +  start_date.getFullYear(),end_date:(end_date.getMonth() + 1) + '/' + end_date.getDate() + '/' +  end_date.getFullYear()}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var dates = result.data.data;
                    var days = [];
                    for(var i in dates){
                        days[dates[i].date] = {id: dates[i].id, className: 'holiday' ,date: dates[i].date , title: dates[i].title, description: dates[i].description};
                    }

                    $("td, th").each(function(){
                        if(days[$(this).attr("data-date")]){
                            $(this).addClass("fully_colored_holiday");
                        }
                    });
                }
            }
        });
    }
    function openTaskDetailsModal(id) {
        var $modal = TssLib.openModel({width: 1024},$('#taskDetails').html());
        $.get("loadModals/"+id+"?modalName=TaskDetails",  function( data ) {
            $modal.find('#detailsContent').html(data);
            $modal.find('#start_time,#end_time').timepicker({minuteStep: 1});
        });
    }
    var $modal = '';
    function openOtherActivitiesModal(e) {
        $modal = TssLib.openModel({width: 1024}, $('#otherActivitiesDetails').html());
        meetingTbltype = 0;
        breakTbltype = 0;
        interviewTbltype = 0;
        callTbltype = 0;
        callOtherActivities('break');
        $modal.find('#break_start_time,#break_end_time,#meeting_start_time,#meeting_end_time,#call_start_time,#call_end_time,#clientCallStTime,#clientCallEndTime,#breakStTime,#breakEndTime,#start_time,#end_time,#interview_start_time,#interview_end_time').timepicker({minuteStep: 1});
        $(".collapse-pane").on('click' , function(){
            $(this).next(".toggle-content").slideToggle();
        });
    }
    function openShowByProjectModal(e) {
        var $modal = TssLib.openModel({width: 550}, $('#showByProjectDetails').html());
    }
    function closeTaskAttachment(e) {
        $(e).parent().hide();
    }
    function callOtherActivities(type){
        $('#break_start_time,#break_end_time,#meeting_start_time,#meeting_end_time,#call_start_time,#call_end_time,#clientCallStTime,#clientCallEndTime,#breakStTime,#breakEndTime,#start_time,#end_time,#interview_start_time,#interview_end_time').timepicker('setTime',formatAMPM(new Date()));
        getJsonAsyncWithBaseUrl("Project/getTaskTypes/"+type, {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var  projects = result.data.data;
                    TssLib.populateSelect($('#'+type+'_types'), { success: true, data: projects }, 'name', 'id_task_type');
                }
            }
        });
        postJsonAsyncWithBaseUrl("user/getAllUsers", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    TssLib.populateSelect($('#'+type+'_users'), {success: true, data: result.data}, 'email', 'id_user');
                }
            }
        });
        if(window[type+'Tbltype'] == 0){
            var $refTblgrid = $('#time-logs-'+type+'-table');
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Project/getOtherActivityTimeGrid?task_type='+type,
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['Id', 'project_task_id', 'Task Type','Start Time', 'End Time', 'Description', 'Total time','Date'],
                colModel: [
                    { name: 'log_time_id', index: 'log_time_id', hidden: true ,key:true},
                    { name: 'project_task_id', index: 'project_task_id',  hidden: true},
                    { name: 'name', index: 'name' },
                    { name: 'start_time', index: 'start_time' },
                    { name: 'end_time', index: 'end_time' },
                    { name: 'comments', index: 'comments' },
                    { name: 'duration', index: 'duration' },
                    { name: 'created_date_time', index: 'created_date_time' },
                ],
                loadComplete: function () {
                    if(jQuery('#time-logs-'+type+'-table').getDataIDs().length==0){
                        $('#gbox_time-logs-'+type+'-table').parent().parent().parent().hide();
                    }else{
                        $('#gbox_time-logs-'+type+'-table').parent().parent().parent().show();
                    }
                },
                pager: 'time-logs-'+type+'-table-page'
            }).navGrid('#time-logs-'+type+'-table-page', {
                edit: false, add: false, del: true, search: false, refresh: true,
                delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        deleteLogTime(id, type);
                    }, 'Yes', 'No');
                }
            });
            window[type+'Tbltype'] = 1;
        }else{
            var $refTblgrid = $('#time-logs-'+type+'-table');
            $refTblgrid.trigger('reloadGrid');
        }

        if(type == 'break'){
            TssLib.ajaxForm({
                jsonContent: true,
                form: $modal.find('#other_break_log_time'), callback: function (result) {
                    if (result.data.status) {
                        resetOtherFormData($modal , type);
                        var $refTblgrid = $('#time-logs-'+type+'-table');
                        $refTblgrid.trigger('reloadGrid');
                        callLoadEvents();
                        TssLib.notify('Time logged successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    if(validateTime($('#break_start_time').val(),$('#break_end_time').val()) <= 0 ){ TssLib.notify('start time should be less than end time', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { created_date_time: $('#break_date').val()});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/logTimeOther');
                    return true;
                }
            });
        }
        if(type == 'call'){
            TssLib.ajaxForm({
                jsonContent: true,
                form: $modal.find('#other_call_log_time'), callback: function (result) {
                    if (result.data.status) {
                        resetOtherFormData($modal , type);
                        var $refTblgrid = $('#time-logs-'+type+'-table');
                        $refTblgrid.trigger('reloadGrid');
                        callLoadEvents();
                        TssLib.notify('Time logged successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    if(validateTime($('#call_start_time').val(),$('#call_end_time').val()) <= 0 ){ TssLib.notify('start time should be less than end time', 'warn', 5); return false; }
                    if($('#call_users').val() == null ){ TssLib.notify('select Users', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { created_date_time: $('#call_date').val()});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/logTimeOther');
                    return true;
                }
            });
        }
        if(type == 'meeting'){
            TssLib.ajaxForm({
                jsonContent: true,
                form:$modal.find('#other_meeting_log_time'), callback: function (result) {
                    if (result.data.status) {
                        resetOtherFormData($modal , type);
                        var $refTblgrid = $('#time-logs-'+type+'-table');
                        $refTblgrid.trigger('reloadGrid');
                        callLoadEvents();
                        TssLib.notify('Time logged successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    if(validateTime($('#meeting_start_time').val(),$('#meeting_end_time').val()) <= 0 ){ TssLib.notify('start time should be less than end time', 'warn', 5); return false; }
                    if($('#meeting_users').val()== null ){ TssLib.notify('Select users', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { created_date_time: $('#meeting_date').val()});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/logTimeOther');
                    return true;
                }
            });
        }
        if(type == 'interview'){
            TssLib.ajaxForm({
                jsonContent: true,
                form: $modal.find('#other_interview_log_time'), callback: function (result) {
                    if (result.data.status) {
                        resetOtherFormData($modal , type);
                        var $refTblgrid = $('#time-logs-'+type+'-table');
                        $refTblgrid.trigger('reloadGrid');
                        callLoadEvents();
                        TssLib.notify('Time logged successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    if(validateTime($('#interview_start_time').val(),$('#interview_end_time').val()) <= 0 ){ TssLib.notify('start time should be less than end time', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { 'task_type': $('#interview_types').val(), created_date_time: $('#interview_date').val()});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/logTimeOther');
                    return true;
                }
            });
        }
    }
    function resetOtherFormData($modal, type){
        $modal.find('.comments').val('');
        $modal.find('#'+type+'_types').select2('val', '');
        $modal.find('#'+type+'_users').select2('val', '');
        $modal.find('#break_start_time,#break_end_time,#meeting_start_time,#meeting_end_time,#call_start_time,#call_end_time,#clientCallStTime,#clientCallEndTime,#breakStTime,#breakEndTime,#start_time,#end_time,#interview_start_time,#interview_end_time').timepicker({minuteStep: 1, defaultTime: formatAMPM(new Date()) });
        $('#break_start_time,#break_end_time,#meeting_start_time,#meeting_end_time,#call_start_time,#call_end_time,#clientCallStTime,#clientCallEndTime,#breakStTime,#breakEndTime,#start_time,#end_time,#interview_start_time,#interview_end_time').timepicker('setTime',formatAMPM(new Date()));
    }
    function deleteLogTime(id, type=0){
        getJsonAsyncWithBaseUrl('Project/deleteLogTime?log_id='+id, {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data.status) {
                    if(type!=0){
                        var $refTblgrid = $('#time-logs-'+type+'-table');
                        $refTblgrid.trigger('reloadGrid');
                    }else{
                        getLogTimeList();
                        $taskGrid.trigger('reloadGrid');
                        $refTblLoggrid.trigger('reloadGrid');
                    }
                    callLoadEvents();
                }
            }
        });
    }
    function loadUserWorkLoad(){
        $.post("<?=site_url('Project/loadUserWork')?>", {user: <?= $this->session->userdata('user_id') ?>}, function (data) {
            setTimeout(function(){ $('#workLoadReport').html(data); },1000);
        });
    }

    function getDayTaskList(user_id,day_id,project_id,type, taskId,id_task_week_flow)
    {
        var html='<table class="table table-bordered">';
        if(type==1)
            html+='<tr><th>Project Name</th><th>Task Name</th><th>Alloted Time</th></tr>';
        else
            html+='<tr><th>Project Name</th><th>Task Name</th><th>Alloted Time</th></tr>';
        var $modal = TssLib.openModel({width: 850}, $('#assignedTaskList').html());
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL+'index.php/Project/getUserDayTasksForFuture',
            dataType: 'json',
            data: {user_id:user_id,day_id:day_id,project_id:project_id,taskId: taskId,id_task_week_flow:id_task_week_flow},
            success:function(res){
                for(var s=0;s<res.length;s++){
                    html+='<tr><td>'+res[s].project_name+'</td><td>'+res[s].task_name+'</td><td>'+res[s].time+'</td>';
                    html+='</tr>';
                }
                html+='</table';
                $('#assignedTaskList-table').html(html);
            }
        });
    }

    $(function () {

        loadUserWorkLoad();

        var _maxheight = 0;
        $('.user-workload .widjet').each(function (i, o) {
            if ($(o).height() > _maxheight) {
                _maxheight = $(o).height();
            }
        });
        $('.user-workload .widjet').height(_maxheight);

        //full calendar
        cal = $('#calendar').fullCalendar({
            buttonText:{prev:'Previous Week', next:'Next Week' },
            height: 'auto',
            businessHours: {
                start: '09:00',
                end: '21:00',
                dow: [1, 2, 3, 4, 5]
            },
            allDaySlot: false,
            allDayText: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            defaultView: 'agendaWeek',
            minTime: '9:00:00',
            maxTime: '21:00:00',
            defaultDate: new Date(),
            eventLimit: true, // allow "more" link when too many events
            events: callEvents,
            eventRender: function eventRender(event, element, view) {
                element.attr("categories", event.category);
                element.attr("task-id", event.taskId);
            },
            eventClick: function (calEvent, jsEvent, view, element) {
                initOnce = 0;
                var id = calEvent.project_task_id;
                if(calEvent.project_task_id)
                {
                    openTaskDetailsModal(calEvent.project_task_id);
                }else if(calEvent.types == 'future'){
                    getDayTaskList(<?= $this->session->userdata('user_id')?>, '',calEvent.projectId,'',calEvent.taskId,calEvent.id_task_week_flow);
                }

            },
            eventMouseover: function(calEvent, jsEvent, view, element) {
                var tooltip = '<div class="tooltipevent" ><div class="tooltipevent-body">' +
                    '<div>' + calEvent.title + '</div>';
                if(calEvent.types != 'holiday'){
                    tooltip +='<span>' + calEvent.task + '</span>' +
                        '<div>'+dateToAmPm(calEvent.start._i)+'</div>' +
                        '<div>'+dateToAmPm(calEvent.end._i)+'</div>';
                }
                tooltip += '</div></div>';
                $("body").append(tooltip);
                $(this).mouseover(function(e) {
                    $(this).css('z-index', 10000);
                    $('.tooltipevent').fadeIn('500');
                    $('.tooltipevent').fadeTo('10', 1.9);
                }).mousemove(function(e) {
                    $('.tooltipevent').css('top', e.pageY + -100);
                    $('.tooltipevent').css('left', e.pageX + 20);
                });
            },
            eventMouseout: function(calEvent, jsEvent) {
                $(this).css('z-index', 8);
                $('.tooltipevent').remove();
            }
        });
        function dateToAmPm(date){
            date = new Date(date);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }
        callLoadEvents();

        $('.fc-prev-button').click(function(){
            callLoadEvents();
        });
        $('.fc-next-button').click(function(){
            callLoadEvents();
        });
        function secondsToHms(d) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);
            //var resultTime = h+':'+m+':'+s;
            var resultTime = h+':'+m;
            return resultTime;
        }
        var allocatedHours = 0,projects = 0,taskCount = 0;

        $taskGrid.jqGrid({
            rowNum: -1,
            url: TssConfig.TT_SERVICE_URL + 'project/fullCalenderTaskList',
            multiselect: false,
            viewrecords: false,
            pgbuttons: false,     // disable page control like next, back button
            pgtext: null,         // disable pager text like 'Page 0 of 10'
            datatype: 'json',
            cmTemplate: { sortable: false },
            colNames: [''],
            extSearchField: '.taskSearch',
            colModel: [
                { name: 'project_name', index: 'project_name',  formatter: function(c,o,d){
                    var _html = '';
                    if(d.projectTitle == 0){
                        var status_tsk = "";

                        switch(d.current_status) {
                            case 'new':
                                status_tsk= 'status-new-tsk';
                                break;
                            case 'progress':
                                status_tsk= 'status-progress-tsk';
                                break;
                            case 'accepted':
                                status_tsk= 'status-accepted-tsk';
                                break;
                            case 'completed':
                                status_tsk= 'status-completed-tsk';
                                break;
                            case 'hold':
                                status_tsk= 'status-hold-tsk';
                                break;
                            default: status_tsk = 'status-progress-tsk';
                        }
                        var actual_time = 0,estimated_time = 0;
                        if(d.actual_time != '00:00:00' && d.actual_time != null){
                            actual_time = d.actual_time.split(":");
                            actual_time = ((actual_time[0]*60*60)+(actual_time[1]*60))
                        }
                        if(d.estimated_time != '' && d.estimated_time != null){
                            estimated_time = d.estimated_time.split(":");
                            estimated_time = ((estimated_time[0]*60*60)+(estimated_time[1]*60))
                            allocatedHours = allocatedHours+estimated_time;
                        }
                        var remainingTimeVal = estimated_time - actual_time;
                        var remainingTime = secondsToHms(Math.abs(remainingTimeVal));
                        remainingTime = (remainingTimeVal >= 0 ? remainingTime+'' : remainingTime+' extra');

                        d.current_status = d.current_status=='approval_waiting'?'Waiting for approval':d.current_status

                        _html += '<div class="'+status_tsk+'" >' +
                        '<div class="tsk-topwrap" onclick="openTaskDetailsModal('+d.id_project_task+')" >' +
                        '<h4>'+d.task_name+'<span class="status" style="color:sienna;font-size: 12px">('+d.current_status+')</span></h4>' +
                        /*'<div><span class="btn">'+ d.current_status+'</span>' +
                            '<span>'+remainingTime+'</span>' +
                            '</div>' +*/
                        '<div><b>'+d.project_name+'</b></div>' +
                            /*'<span>'+ d.description+'</span>' +*/
                        '<div class="clearfix est-time-wrap">' +
                            '<span class="pull-left">Alloted:<b>'+ removeSeconds(d.estimated_time) +'</b></span>' +
                            '<span class="pull-left p-l"> Remain:<b>'+ removeSeconds(remainingTime) +'</b></span>' +
                            '<span class="pull-left p-l"> Actual:<b>'+ removeSeconds(d.actual_time) +'</b></span></div>' +
                        '</div>';
                        if(d.current_status == 'progress'){
                            _html += '<div class="tsk-bottomwrap">' +
                            '<div class="clearfix act-wrap"><span class="pull-left">Actions</span><span class="pull-right"><a task-object=\''+JSON.stringify(d)+'\' data-type="timer" onclick="logTime(this)"><i class="fa fa-play"></i></a></div>' +
                            '</div>';
                        }
                        _html += '</div>';
                        taskCount++;
                    }else{ projects++;}
                    return _html;
                }}
            ],
            loadBeforeSend: function(){ allocatedHours = 0,projects = 0,taskCount = 0; },
            loadComplete: function(data) {
                $('#projects').html(projects);
                $('#taskCount').html(taskCount);
                $('#allocatedHours').html(Math.floor(allocatedHours / 3600)+':'+Math.floor((allocatedHours - (Math.floor(allocatedHours / 3600)*3600)) / 60));
            }

            }).navGrid('#tasks-list-page', {
            edit: false, add: false, del: false, search: false, refresh: false
        });
    });

    function getUserTasks(status,element)
    {
        $('.filters-wrap a').removeClass('color-green');
        $taskGrid.jqGrid("setGridParam", {
            postData:{
                'task_status':status
            }
        }).trigger("reloadGrid");
        $('#tasks-list-page').trigger('reloadGrid');
        $(element).addClass('color-green');
    }

    function removeSeconds(time)
    {
        var arr = time.split(':');
        return arr[0]+':'+arr[1];
    }
</script>
<style>
    .fully_colored_holiday{ background-color: #AAD6E4; }
</style>
