<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package    CodeIgniter
 * @author    EllisLab Dev Team
 * @copyright    Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright    Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license    http://opensource.org/licenses/MIT	MIT License
 * @link    http://codeigniter.com
 * @since    Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        EllisLab Dev Team
 * @link        http://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------
if (!function_exists('generatePassword')) {
    function generatePassword($length)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
if (!function_exists('doUpload')) {

    function doUpload($temp_name, $image, $upload_path,$allowed_type='')
    {
        $ext = pathinfo($image, PATHINFO_EXTENSION);

        if ($allowed_type!='' && !in_array($ext, $allowed_type)) {
            return 0;
            exit;
        }

        list($txt, $ext) = explode(".", $image);
        $imageName = str_replace(' ','_',$txt) . "_" . time() . "." . $ext;
        move_uploaded_file($temp_name, $upload_path . $imageName);
        return $imageName;
    }

}
if (!function_exists('getImageUrl')) {
    function getImageUrl($image, $type='')
    {
        if ($image != '') {
            if (file_exists('uploads/' . $image)) {
                return WEB_BASE_URL . 'rest/uploads/' . $image;
            }
        }

        if ($type == 'profile') {
            return WEB_BASE_URL . 'rest/images/default-img.png';
        } else if ($type == 'company') {
            return WEB_BASE_URL . 'rest/images/company-logo.jpg';
        } else if ($type == 'flag') {
            return WEB_BASE_URL . 'rest/images/default-flag.png';
        }
        else{
            return WEB_BASE_URL . 'rest/images/default-img.png';
        }
    }
}
if (!function_exists('checkInValidFleExtensaion')) {
    function checkInValidFleExtensaion($file)
    {
        $disallowed =  array('exe','bat','sh');
        $filename = strtolower($file['name']);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(in_array($ext,$disallowed) ) {
            return true;
        }
    }
}

if (!function_exists('getExactImageUrl')) {
    function getExactImageUrl($image)
    {
        if ($image != '') {
            if (file_exists('uploads/' . $image)) {
                return WEB_BASE_URL . 'rest/uploads/' . $image;
            }
            else{
                return '';
            }
        }
        else{
            return '';
        }
    }
}

if (!function_exists('formatSizeUnits')) {
    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }
}

if (!function_exists('time_to_sec')) {
    function time_to_sec($time)
    {
        sscanf($time, "%d:%d:%d", $hours, $minutes,$s);
        $secs = $hours * 3600 + $minutes * 60;
        return $secs;
    }
}

if (!function_exists('sec_to_time')) {
    function sec_to_time($sec)
    {
        $hours = floor($sec / 3600);
        $mins = floor($sec / 60 % 60);
        if ( $hours < 10)
            $hours = "0".$hours;
        if ($mins < 10)
            $mins = "0".$mins;
        //$time = $hours.':'.$mins.":00";
        $time = $hours.':'.$mins;

        return $time;
    }
}

if (!function_exists('isWeekends')) {
    function isWeekends($data){
        if($data['date']){
            $dayofweek = date('w', strtotime($data['date']));
            if($dayofweek == 6 || $dayofweek == 0){
                return true;
            }else{
                return false;
            }
        }
    }
}

if (!function_exists('specialCharReplace')) {
    function specialCharReplace($data){
        $data = str_replace("'",'',$data);
        return $data;
    }
}

function getWorkingDays($startDate,$endDate,$holidays=array()){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
    $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
        $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}
