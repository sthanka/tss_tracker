<?php
/**
 * Created by PhpStorm.
 * User: RAKESH
 * Date: 04-05-2016
 * Time: 11:46 AM
 */
?>
<script src="<?=WEB_BASE_URL?>scripts/jquery.jqGrid.src.js"></script>
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Projects</h3>
        </div>
        <table id="grid" class="table-responsive"></table>
        <div id="pager"></div>
        <?php echo $customerGrid; ?>
    </div>
</div>
