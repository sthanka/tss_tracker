<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Reports</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Requirement Status Reports
                    </h3>
                </div>
                <div class="padding0">

                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <form id="reports_frm" class="NewErrorStyle" name="reports_frm" method="post"
                              action="<?= WEB_BASE_URL ?>index.php/Reports/generateReport">
                            <div class="col-sm-10 padding0">
                                <!--<div class="col-sm-6">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="start_date" value="<?/*=date('d/m/Y',strtotime("-1 days"))*/?>"
                                                       name="start_date" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>

                                        <label class="control-label col-sm-12">Start Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="end_date" value="<?/*=date('d/m/Y')*/?>"
                                                       name="end_date" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>
                                        <label class="control-label col-sm-12">End Date:</label>
                                    </div>
                                </div>-->
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="projectDropdown" name="project_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Project:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container mt15">
                                        <select id="requirementDropdown" name="requirement[]"
                                                class="form-control rval" multiple="multiple"
                                                includeselectalloption="true" enablefiltering="true">
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-12">Requirement:</label>
                                </div>
                                </div>
                                <!--<div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="departmentDropdown" name="department[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Department:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="employeeDropdown" name="user_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Employee:</label>
                                    </div>
                                </div>-->
                                <div class="col-sm-3">
                                    <!--<button class="button button-common module mt20"  onclick="resultType(0);" > Search </button>
                                    <button class="button button-common module mt20"  onclick="resultType(1);" > Excel </button>-->
                                    <input type="button" class="button button-common module mt20"
                                           onclick="validateReportsForm(0);" value="Search">
                                    <!--<input type="button" class="button button-common module mt20"
                                           onclick="validateReportsForm(1);" value="Excel">-->
                                </div>
                            </div>
                            <input type="hidden" name="type" id="type" value="0">
                        </form>

                    </div>
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="grid-details-table">
                            <div class="grid-details-table-header">
                                <h3 >Requirement Status List</h3>
                                <input type="text" class="searchInput pull-right"/>
                                <a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="grid-details-table-content clearfix padding0">
                                <div class="tbl_wrapper border0">
                                    <table id="project-list" class="table-responsive"></table>
                                    <div id="project-list-page"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#project-list');
        $(function () {
            $('#log_start_date').val(new Date().format('d/m/Y'));

            postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var projects = result.data;
                        TssLib.populateSelect($('#projectDropdown'), {
                            success: true,
                            data: projects
                        }, 'project_name', 'id_project');
                        TssLib.selectAllOptions('#projectDropdown');
                        loadRequirement();
                    }
                }
            });

            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Reports/getProjectRequirementStatusGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['Project Name', 'Requirement Name','No of Task','New','Progress','Completed','Estimated Effort','Additional Effort', 'Assigned Effort', 'Pending Effort','Completed Effort', 'Status'],
                colModel: [
                    {name: 'project_name', index: 'project_name', hidden: false, key: false},
                    {name: 'requirement_name', index: 'requirement_name', hidden: false, key: false,formatter: function(c,o,d){
                        return '<span title="'+ d.requirement_name+'">'+ d.requirement_name+'</span>';
                    }
                    },
                    {name: 'no_of_tasks', index: 'no_of_tasks', hidden: false, key: false, sortable: false},
                    {name: 'new', index: 'new', hidden: false, key: false, sortable: false},
                    {name: 'progress', index: 'progress', hidden: false, key: false, sortable: false},
                    {name: 'completed', index: 'completed', hidden: false, key: false, sortable: false},
                    {name: 'estimated_effort', index: 'estimated_effort', hidden: false, key: false, sortable: false},
                    {name: 'additional_time', index: 'additional_time', hidden: false, key: false, sortable: false},
                    {name: 'assigned_effort', index: 'assigned_effort', hidden: false, key: false, sortable: false},
                    {name: 'pending_effort', index: 'pending_effort', hidden: false, key: false, sortable: false},
                    {name: 'completed_effort', index: 'completed_effort', hidden: false, key: true},
                    /*{name: 'status', index: 'status', hidden: false, key: true}*/
                    { name: 'status', index: 'status', hidden: true, formatter: function(c,o,d){
                        var innerHtml = '';
                        if(c.toLowerCase() == 'new'){
                            innerHtml += '<span class="pull-left task-status task-inprogress">New</span>';
                        }
                        if(c.toLowerCase() == 'open'){
                            innerHtml += '<span class="pull-left task-status task-inprogress">New</span>';
                        }
                        if(c.toLowerCase() == 'in-progress'){
                            innerHtml += "<span class='pull-left task-status task-waiting'>In Progress</span>";
                        }
                        if(c.toLowerCase() == 'completed'){
                            innerHtml += '<span class="pull-left task-status task-completed">Completed</span>';
                        }
                        if(c.toLowerCase() == 'hold'){
                            innerHtml += '<span class="pull-left task-status task-open">Hold</span><span class="pull-left task-chat"></span>';
                        }
                        return innerHtml;
                    }
                    },
                ],
                loadComplete: function() {
                    setTimeout(function(){
                        resizeGrids();
                    }, 1000);
                },
                pager: 'project-list-page'
            }).navGrid('#project-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true
            });

            $('#projectDropdown').on('change', function () {
                if ( $( "#projectDropdown" ).val()  == null ) { return false; }
                loadRequirement();
            });
        });

        function loadRequirement(empty) {
            if(empty){
                TssLib.populateSelect($('#requirementDropdown'), {
                    success: true,
                    data: {id_requirements:"0", requirement_name:"Select Requirement"},
                }, 'requirement_name', 'id_requirements');
                TssLib.selectAllOptions('#requirementDropdown');
            }
            var project = $('#projectDropdown').val();
            project = project.join('-');
            postJsonAsyncWithBaseUrl("Reports/getProjectRequirement/" + project, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var requirement = result.data.data;
                        TssLib.populateSelect($('#requirementDropdown'), {
                            success: true,
                            data: requirement
                        }, 'requirement_name', 'id_requirements');
                        TssLib.selectAllOptions('#requirementDropdown');
                    }
                }
            });
        }

        function resultType(type) {
            $('#type').val(type);
        }

        function validateReportsForm(type) {

            var project = $('#projectDropdown').val();
            var requirement = $('#requirementDropdown').val();
            var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
            myPostData['project'] = project;
            myPostData['requirement'] = requirement;
            $refTblgrid.setGridParam({'postData': myPostData});
            $('#project-list').trigger("reloadGrid");

            $('#start_date').removeClass('err-bg');
            $('#end_date').removeClass('err-bg');
            $('#employeeDropdown').removeClass('err-bg');
            $('.error-msg').remove();

            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var project = $('#projectDropdown').val();
            var department = $('#departmentDropdown').val();
            var user_id = $('#employeeDropdown').val();
            var flag = 0;

            if (start_date == '') {
                $('#start_date').addClass('err-bg');
                $('#start_date').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (end_date == '') {
                $('#end_date').addClass('err-bg');
                $('#end_date').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (project == '') {
                $('#projectDropdown').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (department == '') {
                $('#departmentDropdown  ').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (user_id == '') {
                $('#employeeDropdown').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }

            if (flag == 0) {
                if (type == 0) {
                    var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
                    myPostData['project'] = project;
                    myPostData['department'] = department;
                    myPostData['user_id'] = user_id;
                    myPostData['start_date'] = start_date;
                    myPostData['end_date'] = end_date;
                    //console.log(user_id);
                    $refTblgrid.setGridParam({'postData': myPostData});
                    $('#project-list').trigger("reloadGrid");

                    return false;
                }
                else {
                    $('#reports_frm').submit();
                }
            }
        }
    </script>