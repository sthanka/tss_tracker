(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

    $('.add-btn button').on('click',function(){
        $(this).parent().next().fadeToggle();
    })
    $('.hide-btn').on('click',function(){
        $(this).parent().parent().fadeOut();
    })
})();

function gmail_logout()
{
    gapi.auth.signOut();
    location.reload();
}
function gmail_login()
{
    var myParams = {
        'clientid' : '263816501580-e57pbt2g3vq7iedrhjtoehi6lknn0ruc.apps.googleusercontent.com',
        //'clientid' : '472244376060-29v5udskvifbcj1fqdevm71ffblet4qh.apps.googleusercontent.com',
        'cookiepolicy' : 'single_host_origin',
        'callback' : 'loginCallback',
        'approvalprompt':'force',
        'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
    };
    gapi.auth.signIn(myParams);
}

function loginCallback(result)
{
    if(result['status']['signed_in'])
    {
        var request = gapi.client.plus.people.get(
            {
                'userId': 'me'
            });
        request.execute(function (resp)
        {
            var email = '';
            if(resp['emails'])
            {
                for(i = 0; i < resp['emails'].length; i++)
                {
                    if(resp['emails'][i]['type'] == 'account')
                    {
                        email = resp['emails'][i]['value'];
                    }
                }
            }


            $.ajax({
                async: false,
                type: 'POST',
                url: WEB_BASE_URL+'index.php/User/login',
                dataType: 'json',
                data: resp,
                success:function(res){
                    if(res.response == 1){
                        location.reload();
                    }
                    else{
                        TssLib.notify(res.data, 'warn', 5);
                    }
                }
            });

        });

    }

}
function onLoadCallback()
{
    gapi.client.setApiKey('AIzaSyC2wsRKhKWgagMp4JXsGPIkYus0Up_krBA');
    gapi.client.load('plus', 'v1',function(){});
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function updateUseCase(use_case_id,project_task_id)
{
    var html = '';
    var html_txt = '';

    var use_case = $('#user_case_text_'+project_task_id).Editor("getText");
    if(use_case!='')
    {
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL+'index.php/Project/updateUseCase',
            dataType: 'json',
            data: {use_case_id:use_case_id,project_task_id:project_task_id,use_case:use_case},
            success:function(res){
                if(res.status){
                    html = '<input type="button" id="use_case_btn_'+project_task_id+'" onclick="updateUseCase(\'\','+project_task_id+ ',\'\');" value="Save" class="button-common">';
                    html+='<input type="button" class="button-common hide-btn" value="Cancel">';

                    html_txt = '<li><h5>'+res.data.user_name+'</h5><p>'+use_case+'</p></li>';
                }

                $('#task_'+project_task_id+' .mt10').html(html);

                $('#u-list_'+project_task_id+'').prepend(html_txt);
                //$('#user_case_text_'+project_task_id).Editor().setText('');
                TssLib.notify('Use case Added Successfully');

                $('.hide-btn').on('click',function(){
                    $(this).parent().parent().fadeOut();
                })
            }
        });
    }
}

