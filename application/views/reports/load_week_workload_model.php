
            <?php $responsiveClass = 'col-sm-12'; ?>
            <div class="col-sm-12 padding0 clearfix">
                <div class="week-but mb5 text-center <?=$responsiveClass?>">
                <span class="" >
                     <?php if($last_week){ ?>
                         <span  class="fa fa-angle-left btn-red" onclick="refreshUrl(<?=$last_week?>);" ></span>
                     <?php }  ?>
                </span>
                    <span class="input-middle"><?=date('M d',$week)?>&nbsp;&nbsp;  - &nbsp;&nbsp;<?=date('M d',strtotime(date('d-m-Y', $week).'+ 6 days'))?></span>
                <span class="">
                    <?php if($next_week){ ?>
                        <span class="fa fa-angle-right btn-red" onclick="refreshUrl(<?=$next_week?>);"></span>
                    <?php } ?>
                    </span>

                    <input type="hidden" id="cur_week_time" value="<?=$week?>">
                </div>
                <div class="col-sm-3"></div>
            </div>

            <div class="col-sm-12 padding0">
                <div class="fixed-table clearfix <?=$responsiveClass?>">
                    <table class="task-assigning-table mr15" border="1">
                        <thead>
                        <th> <span class="th-box">TEAM</span></th>
                        <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week)))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d',strtotime(date('d-m-Y' , $week)))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d',strtotime(date('d-m-Y' , $week)))])){ echo 'title="'.$holidays[date('Y-m-d',strtotime(date('d-m-Y' , $week)))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week)))?> Monday</span></th>
                        <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 1 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+1 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+1 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+1 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 1 days'))?> Tuesday</span></th>
                        <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 2 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+2 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+2 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+2 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 2 days'))?> Wednesday</span></th>
                        <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 3 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+3 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+3 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+3 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 3 days'))?> Thursday</span></th>
                        <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 4 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+4 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+4 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+4 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 4 days'))?> Friday</span></th>
                        <!--<th class="<?/*= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 5 days'))?'current-date':'' */?> <?php /*if(isset($holidays[date('Y-m-d' ,strtotime('+5 day', $week))])){ echo 'holiday'; } */?>" <?php /*if(isset($holidays[date('Y-m-d' ,strtotime('+5 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+5 day', $week))]['title'].'"'; } */?>><span class="th-box"><?/*=date('d',strtotime(date('d-m-Y', $week).'+ 5 days'))*/?> Saturday</span></th>
                        <th class="<?/*= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 6 days'))?'current-date':'' */?> <?php /*if(isset($holidays[date('Y-m-d' ,strtotime('+6 day', $week))])){ echo 'holiday'; } */?>" <?php /*if(isset($holidays[date('Y-m-d' ,strtotime('+6 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+6 day', $week))]['title'].'"'; } */?>><span class="th-box"><?/*=date('d',strtotime(date('d-m-Y', $week).'+ 6 days'))*/?> Sunday</span></th>-->
                        <th class=""><span class="th-box"> Total</span></th>
                        </thead>
                    </table>
                </div>
                <div class="">


                    <div class="<?=$responsiveClass?> ">

                        <table class="task-assigning-table mr15 table-bordered text-center">
                            <thead><th>&nbsp;</th></thead>
                            <tbody>
                            <?php for($s=0;$s<count($users);$s++){ $total_time = 0;

                                ?>
                                <tr>
                                    <td>
                                        <div class="">
                                            <span title="<?=$users[$s]['first_name'].' '.$users[$s]['last_name']?>" class="user-inv-tsk-name"><span><?=$users[$s]['first_name'].' '.$users[$s]['last_name']?></span><span><?=$users[$s]['department_name']?></span></span>

                                        </div>
                                    </td>
                                    <?php if($this->session->userdata('is_lead')==1){ $type = 1; } else { $type = 0; } ?>
                                    <?php  for($st=0;$st<5;$st++){  ?>
                                        <?php
                                        $tmp = isset($user_log_time[$users[$s]['id_user']][date('j',strtotime(date('d-m-Y' , $week).'+ '.$st.' days'))]['time_sec'])?$user_log_time[$users[$s]['id_user']][date('j',strtotime(date('d-m-Y' , $week).'+ '.$st.' days'))]['time_sec']:'0';
                                        $per_time = 0;
                                        if($tmp<28800 && $users[$s]['employee_id']!='' && $users[$s]['employee_id']!=0) {

                                            $permission = $this->Report_model->getLeavePermission(array('user_id' => $users[$s]['employee_id'],'date' => date('Y-m-d',strtotime(date('d-m-Y' , $week).'+ '.$st.' days'))));
                                            if(!empty($permission)){
                                                if($permission[0]['from_time']=='M'){ $per_time = 28800; }
                                                else if($permission[0]['from_time']=='A'){ $per_time = 14400; }
                                            }
                                            $tmp = $tmp + $per_time;
                                        }
                                        ?>
                                        <td>
                                            <?=isset($user_log_time[$users[$s]['id_user']][date('j',strtotime(date('d-m-Y' , $week).'+ '.$st.' days'))]['time'])?$user_log_time[$users[$s]['id_user']][date('j',strtotime(date('d-m-Y' , $week).'+ '.$st.' days'))]['time']:'00:00'?>
                                            <?php if($per_time){ echo " + ".sec_to_time($per_time)." Permission"; } ?>
                                        </td>
                                    <?php


                                        $total_time = $total_time + $tmp;
                                    } ?>
                                    <td style="<?php if($total_time<144000){ ?>background-color:red;color:white;<?php }else{ ?>background-color:green;color:white;<?php } ?>">

                                        <?=sec_to_time($total_time)?>
                                    </td>
                                </tr>
                            <?php }  ?>
                            <?php if(!count($users)){ ?>
                                <tr>
                                    <td colspan="6" >
                                        <div class="text-center padding5">No Team Members Found</div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div>
                </div>
            </div>
