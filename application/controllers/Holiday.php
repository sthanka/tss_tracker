<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holiday extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Holiday_modal');
    }

    public function index()
    {
        $data['content'] = 'holiday/holiday';
        $this->load->view('partials/landing',$data);
    }

    public function user_list()
    {
        $data['content'] = 'holiday/user_list';
        $this->load->view('partials/landing',$data);
    }

    function excelHolidayUpload(){

        if (isset($_FILES['file']['tmp_name']) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
            $file = $_FILES['file']['tmp_name'];
            $userfile_extn = explode(".", strtolower($_FILES['file']['name']));

            $holidayData = false;

            if ($userfile_extn[1] == 'xlsx' || $userfile_extn[1] == 'xls') {
                $this->load->library('excel');
                $objPHPExcel = PHPExcel_IOFactory::load($file);
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                $header = [];
                $arr_data = [];
                $index = 0;
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
//                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();
                    if ($row == 1) {
                        $header[$row][$column] = trim($data_value);
                    } else {
                        $value = $header[1][$column];
                        $arr_data[$row][$value] = $data_value;
                    }
                    $index++;
                }
                $arr_data = array_values($arr_data);
//                echo '<pre>'; print_r($arr_data); exit;
                $err = false;
                for($s=0;$s<count($arr_data);$s++) {
                    $isError = false;
                    if( (isset($arr_data[$s]['date(dd-mm-yyyy)']) || $arr_data[$s]['date(dd-mm-yyyy)'] == '')
                        || (isset($arr_data[$s]['Title']) || $arr_data[$s]['Title'] == '') )
                    {
                        $current = $s+2;
                        if(!isset($arr_data[$s]['Title'])  || $arr_data[$s]['Title'] == ''){
                            $err[$current] = ['msg' => 'Title is missing'];
                            $isError = true;
                        }

                        if(!isset($arr_data[$s]['date(dd-mm-yyyy)']) || $arr_data[$s]['date(dd-mm-yyyy)'] == ''){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Date is blank'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Date is blank'];
                                $isError = true;
                            }
                        }else if(!preg_match('/[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/',$arr_data[$s]['date(dd-mm-yyyy)'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Date is incorrect'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Date is incorrect'];
                                $isError = true;
                            }
                        }

                        if($isError){
                            $error[$current] = array(
                                'errorMsg' => $err[$current]['msg']
                            );
                        }else{
                            $individualData = array(
                                'date' => date('Y-m-d',strtotime($arr_data[$s]['date(dd-mm-yyyy)'])),
                                'title' => $arr_data[$s]['Title'],
                                'description' => (isset($arr_data[$s]['Description']) && $arr_data[$s]['Description']=='')?'':$arr_data[$s]['Description'],
                            );
                            $id_holiday = $this->Holiday_modal->checkHolidayExist($individualData);
                            if(!$id_holiday){
                                $holidayData[] = $individualData;
                            }else{
                                $id_holiday = $this->Holiday_modal->updateHoliday($id_holiday , $individualData);
                            }
                        }
                    }
                }
                if(is_array($holidayData) && count($holidayData)>0)
                    $this->Holiday_modal->addHolidayBatchData($holidayData);
                if(isset($error) && count($error)>0){
                    $this->session->set_userdata('uploadError',$error);
                }
                $this->session->set_userdata('message','Uploaded successfully.');
                redirect(WEB_BASE_URL.'index.php/Holiday');
            } else {
                $this->session->set_userdata('message','Invalid File Extension');
                redirect(WEB_BASE_URL.'index.php/Holiday');
            }
        }else{
            $this->session->set_userdata('upload','No File Uploaded');
            redirect(WEB_BASE_URL.'index.php/Holiday');
        }
    }

    public function getHolidayGrid(){
        $params = $this->input->get();
        $list = $this->Holiday_modal->getHolidayGrid($params);
        echo json_encode($list);
    }
    /*Get list of holidays, fullCalender page, */
    public function getListOfHolidaysByDateRange(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $list = $this->Holiday_modal->getListOfHolidaysByDateRange($data);
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$list));exit;
    }

    public function addHoliday(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $id = $data['id_holiday'];

        if(isset($data['date']))
            $data['date'] = DateTime::createFromFormat('d/m/Y', $data['date'])->format('Y-m-d');

        if($id == null && $id == '') {
            $id_holiday = $this->Holiday_modal->checkHolidayExist($data);
            if(!$id_holiday){
                $res = $this->Holiday_modal->addSingleData($data);
            }else{
                echo json_encode(array('status'=>false,'error'=>true,'message'=>'Holiday already assigned to this date','data'=>[]));exit;
            }
        }else{
            unset($data['id_holiday']);
            $res = $this->Holiday_modal->updateHoliday($id , $data);
        }

        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$res));exit;
    }

    public function deleteHolidayById(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $list = $this->Holiday_modal->deleteHolidayById($data['id_holiday']);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$list));exit;
    }

}