/*jshint curly: true, eqeqeq: true, undef: true, devel: true, browser: true */
/*global jQuery */
 
/**
 * Turn a text box into an auto suggest box which search's and 
 * displays results specified in a JSON string
 * 
 *
 * @name jsonSuggest
 * @type jQuery plugin
 * @author Tom Coote (tomcoote.co.uk)
 * @version 2.0.1
 * @copyright Copyright 2011 Tom Coote
 * @license released under the BSD (3-clause) licences
 * 
 * @param settings <object>;
 *			url :				[default ''] A URL that will return a JSON response. Called via $.getJSON, it is passed a 
 * 								 data dictionary containing the user typed search phrase. It must return a JSON string that
 *								 represents the array of results to display.
 *			data :				[default []]An array or JSON string representation of an array of data to search through.
 *								 Example of the array format is as follows:
									[
										{
											id: 1,
											text: 'Thomas',
											image: 'img/avator1.jpg',	// optional
											extra: 'www.thomas.com'	// optional
										},
										{
											id: 2,
											text: 'Frederic',
											image: 'img/avator2.jpg',	// optional
											extra: 'www.freddy.com'	// optional
										},
										{
											id: 2,
											text: 'James',
											image: 'img/avator2.jpg',	// optional
											extra: 'www.james.com'	// optional
										}
									]
 *			minCharacters :		[default 1] Number of characters that the input should accept before running a search.
 *			maxResults :		[default undefined] If set then no more results than this number will be found.
 *			wildCard :			[default ''] A character to be used as a match all wildcard when searching. Leaving empty 
 *								 will mean results are matched inside strings but if a wildCard is present then results are 
 *								 matched from the beginning of strings.
 *			caseSensitive :		[default false] True if the filter search's are to be case sensitive.
 *			notCharacter :		[default !] The character to use at the start of any search text to specify that the results 
 *								 should NOT contain the following text.
 *			maxHeight :			[default 350] This is the maximum height that the results box can reach before scroll bars 
 *								 are shown instead of getting taller.	
 *			width:				[default undefined] If set this will become the width of the results box else the box will be 
 *								 the same width as the input.
 *			highlightMatches : 	[default true] This will add strong tags around the text that matches the search text in each result.
 *			onSelect :			[default undefined] Function that gets called once a result has been selected, gets passed in 
 *								 the object version of the result as specified in the JSON data.
 *			
 */

(function($) {

	$.fn.jsonSuggest = function(settings) {
		var defaults = {  
				url: '',
				data: [],
				minCharacters: 1,
				maxResults: undefined,
				wildCard: '',
				caseSensitive: false,
				notCharacter: '!',
				maxHeight: 350,
				highlightMatches: true,
				onSelect: undefined,
				width: undefined,
        property: 'text'
			},
			getJSONTimeout;
		settings = $.extend(defaults, settings);  
	
		return this.each(function() {
			/**
			* Escape some text so that it can be used inside a regular expression
			* without implying regular expression rules iself. 
			*/
			function regexEscape(txt, omit) {
				var specials = ['/', '.', '*', '+', '?', '|',
								'(', ')', '[', ']', '{', '}', '\\'];
				
				if (omit) {
					for (var i = 0; i < specials.length; i++) {
						if (specials[i] === omit) { specials.splice(i,1); }
					}
				}
				
				var escapePatt = new RegExp('(\\' + specials.join('|\\') + ')', 'g');
				return txt.replace(escapePatt, '\\$1');
			}
			
			var obj = $(this),
				wildCardPatt = new RegExp(regexEscape(settings.wildCard || ''),'g'),
				results = $('<div />', {class:"ac-section"}),
				
				//section = $('<section />'),
				currentSelection, pageX, pageY;
				
			
			
			/**
			* When an item has been selected then update the input box,
			* hide the results again and if set, call the onSelect function.
			*/
			function selectResultItem(item) {
			    obj.val($.trim(item[settings.customKeyToSelect]));
			    if (!TssLib.isBlank(settings.haveKeyValue) && settings.haveKeyValue) {
			        settings.valueFld.val($.trim(item[settings.customValueToSelect]));
			        settings.valueFld.attr('key-data',obj.val());
			    }
                /*
			    if ((settings.customKeyToSelect != settings.customKeyToSend) && !TssLib.isBlank(settings.customKeyToSend)) {
			        settings.customKeyToSendFld.val(item[settings.customKeyToSend]);
			    }
                */
				$(results).html('').hide();
				if (typeof settings.onSelect === 'function') {
					settings.onSelect(item);
				}
			}

			/**
			* Used to get rid of the hover class on all result item elements in the
			* current set of results and add it only to the given element. We also
			* need to set the current selection to the given element here.
			*/
			function setHoverClass(el) {
				$('tr td', results).removeClass('ui-state-hover');
				if (el) {
					$('td', el).addClass('ui-state-hover');
				}
				
				currentSelection = el;
			}
			
			/**
			* Build the results HTML based on an array of objects that matched
			* the search criteria, highlight the matches if that feature is turned 
			* on in the settings.
			*/
			function buildResults(resultObjects, filterTxt) {
 
			    settings.results = resultObjects
				filterTxt = '(' + filterTxt + ')';
			
				var bOddRow = true, i, iFound = 0,
					filterPatt = settings.caseSensitive ? new RegExp(filterTxt, 'g') : new RegExp(filterTxt, 'ig');
					
				$(results).html('').hide();
				
				    var table = $('<table />', {class: 'table-striped'});
					var container = $('<div />', {class: 'ac-container'});
					var item = $('<tr />');
					var itemHead = $('<thead />');
					//console.log($(results));
					
					$.each(settings.showDataHeader,function(index,val){
						$(item).append('<th><div>' + val + '</div></th>');						
					});	
						
					itemHead.append(item);
					$(table).append(itemHead);					
					$(container).append(table);							
					$(results).append(container);
					
				for (i = 0; i < resultObjects.length; i += 1) {
					//var itemtr = $('<tr />');
					var item = $('<tr />');
					//item.appendTo(itemtr);
						text = resultObjects[i][settings.customKeyToSelect];
						
					if (settings.highlightMatches === true) {
						text = (text+'').replace(filterPatt, '<strong>$1</strong>');
					}
					$.each(settings.showData,function(index,val){
						$(item).append('<td  >' + resultObjects[i][val] + '</td>');						
					});
 
                   
					
					item.addClass((bOddRow) ? 'odd' : 'even'). 
                        css('cursor', 'pointer').
                        click(function (e) {
                            e.preventDefault();
                            selectResultItem(resultObjects[$(this).parent().find('tr').index(this)]);
                        }).
						mouseover(function (e) {
						    setHoverClass(e);
					    });
					
					$(table).append(item);
			
					bOddRow = !bOddRow;
					
					iFound += 1;
					if (typeof settings.maxResults === 'number' && iFound >= settings.maxResults) {
						break;
					}
				}
				
				if ($('tr', results).length > 0) {
					currentSelection = undefined;
					$(results).show().css('height', 'auto');
					
					if ($(results).height() > settings.maxHeight) {
						//$(results).css({'overflow': 'auto', 'height': settings.maxHeight + 'px'});
					}
				}
				if (resultObjects.length == 0) {
				    $(table).append('<tr class="ui-menu-item even" role="menuitem"><td class="ui-corner-all" align="center" colspan="' + settings.showDataHeader.length + '">No data found</td></tr>');
				}
			}
			
			/**
			* Prepare the search data based on the settings for this plugin,
			* run a match against each item in the possible results and display any 
			* results on the page allowing selection by the user.
			*/
			function runSuggest(e) {	
				var search = function(searchData) {
					if (this.value.length < settings.minCharacters) {
						clearAndHideResults();
						return false;
					}
					
					var resultObjects = [],
						filterTxt = (!settings.wildCard) ? regexEscape(this.value) : regexEscape(this.value, settings.wildCard).replace(wildCardPatt, '.*'),
						bMatch = true, 
						filterPatt, i;
							
					if (settings.notCharacter && filterTxt.indexOf(settings.notCharacter) === 0) {
						filterTxt = filterTxt.substr(settings.notCharacter.length,filterTxt.length);
						if (filterTxt.length > 0) { bMatch = false; }
					}
					filterTxt = filterTxt || '.*';
					filterTxt = settings.wildCard ? '^' + filterTxt : filterTxt;
					filterPatt = settings.caseSensitive ? new RegExp(filterTxt) : new RegExp(filterTxt, 'i');
					
					// Look for the required match against each single search data item. When the not
					// character is used we are looking for a false match. 
					for (i = 0; i < searchData.length; i += 1) {
						if(settings.searchInCustomKey===true){
							if (filterPatt.test(searchData[i][settings.customKeyToSelect]) === bMatch) {
								resultObjects.push(searchData[i]);
							}
						}else{
							$.each(settings.showData,function(idx,vl){
									if (filterPatt.test(searchData[i][vl]) === bMatch) {
										resultObjects.push(searchData[i]);
										return false;
									}
							});
						}
					}
					
					buildResults(resultObjects, filterTxt);
				};
				
				if (settings.data && settings.data.length) {
					search.apply(this, [settings.data]);
				}
				else if (settings.url && typeof settings.url === 'string') {
					var text = this.value;
					if (text.length < settings.minCharacters) {
						clearAndHideResults();
						return false;
					}
                    
					$(results).html('<li class="ui-menu-item ajaxSearching"><a class="ui-corner-all">Searching...</a></li>').
						show().css('height', 'auto');
					
					getJSONTimeout = window.clearTimeout(getJSONTimeout);
					getJSONTimeout = window.setTimeout(function() {
					    var sendData = {};
					    //debugger;
					    sendData[TssLib.isBlank(settings.searchInputParameter) ? 'search' : settings.searchInputParameter] =
                            (TssLib.isBlank(settings.searchTextReplacer) ? text : settings.searchTextReplacer.replace(/_text_/g,text));
					    getJsonAsyncWithBaseUrl(settings.url, sendData, {
					        showLoader:settings.showLoader,
					        callback: function (data) {
					            if (data.success) {
					                if (data.data) {
					                    if(TssLib.isBlank(settings.resultsListKey))
					                        buildResults(data.data, text);
					                    else
					                        buildResults(data.data[settings.resultsListKey], text);
					                }else
					                    clearAndHideResults();
					            }
					            else {
					                    clearAndHideResults();
					            }
					        }
						});
					}, 500);
				}
			}
			
			/**
			* Clears any previous results and hides the result list
			*/
			function clearAndHideResults() {
				$(results).html('').hide();
			}            
            
			/**
			* To call specific actions based on the keys pressed in the input
			* box. Special keys are up, down and return. All other keys
			* act as normal.
			*/
			function keyListener(e) {
				switch (e.keyCode) {
					case 13: // return key
						$(currentSelection).trigger('click');
						return false;
					case 40: // down key
						showLog(currentSelection);
						if (typeof currentSelection === 'undefined') {
							currentSelection = $('tr:first', results).get(0);
						}
						else {
							currentSelection = $(currentSelection).next().get(0);
						}
						
						setHoverClass(currentSelection);
						if (currentSelection) {
							$(results).scrollTop(currentSelection.offsetTop);
						}
						
						return false;
					case 38: // up key
						if (typeof currentSelection === 'undefined') {
							currentSelection = $('tr:last', results).get(0);
						}
						else {
							currentSelection = $(currentSelection).prev().get(0);
						}
						
						setHoverClass(currentSelection);
						if (currentSelection) {
							$(results).scrollTop(currentSelection.offsetTop);
						}
						
						return false;
					default:
						runSuggest.apply(this, [e]);
				}
			}
			
			// Prepare the input box to show suggest results by adding in the events
			// that will initiate the search and placing the element on the page
			// that will show the results.
			
			$(results).addClass('jsonSuggest').
				attr('role', 'listbox').
				css({
                    //'top':0,
					'top': (obj.position().top + obj.outerHeight()) + 'px',
					'left': obj.position().left + 'px',
					'width': settings.width || (obj.outerWidth() + 'px'),
					'z-index': 999
				}).hide();
			 obj.unbind('keyup keydown blur focus');
			 obj.after(results).
				keyup(keyListener).
				keydown(function(e) {
					// for tab/enter key
					showLog($(results));
					if ((e.keyCode === 9 || e.keyCode === 13) && currentSelection) {
						$(currentSelection).trigger('click');
						return true;
					}
				}).
				blur(function (e) {
				    var inputContainer = obj.closest('.input_container').css({'z-index':'1000'});
				    inputContainer.find('.slide_input_bg').removeClass('animateBg');
					inputContainer.closest('.panel-body').css({'max-height':'350px','overflow-y':'auto'});
				    var self = this;
				    var ignoreResult = $(self).attr('ignore-result');
				    if (TssLib.isBlank(ignoreResult)) ignoreResult = 'false';
 					// We need to make sure we don't hide the result set
					// if the input blur event is called because of clicking on
					// a result item.
					var resPos = $(results).offset();
					resPos.bottom = resPos.top + $(results).height();
					resPos.right = resPos.left + $(results).width();
					if (pageY < resPos.top  || pageY > resPos.bottom  || pageX < resPos.left || pageX > resPos.right) {
					    $(results).hide();
					    if (ignoreResult == 'false') {
					        var _td = $('.ac-container table tbody').find('td:eleContainsIgnoreCase(' + $.trim(self.value) + ')');
					        _td.parent().trigger('click');
					        if ($.trim(self.value) === '' && settings.valueFld) {
					            settings.valueFld.val('');
					            settings.valueFld.attr('key-data', '');
					        } else {
					            if (!TssLib.isBlank(settings.haveKeyValue) && settings.haveKeyValue) {
					                setTimeout(function () {
					                    if ($.trim(settings.valueFld.attr('key-data')) != $.trim(self.value)) {
					                        if (TssLib.isBlank(settings.invMsg))
					                            settings.invMsg = 'Invalid data';
					                        TssLib.notify(settings.invMsg, 'warn');
					                        self.value = '';
					                        settings.valueFld.val('');
					                        settings.valueFld.attr('key-data', '');
					                    }
					                }, 300);
					            }
					        }
					    } else {
					        settings.valueFld.val('');
					        settings.valueFld.attr('key-data', '');
					    }
					}
				}).
				focus(function (e) {
				    var inputContainer = obj.closest('.input_container').css({'z-index':'1060'});
				    inputContainer.addClass('jsonsuggest-container').find('.slide_input_bg').addClass('animateBg');
					inputContainer.closest('.panel-body').css({'max-height':'inherit','overflow-y':'visible'});
				    var xHalfPos = ($(window).width() - $('.leftpanel').width()) / 2;
				    var yHalfPos = ($(window).height() - $('#header').height()) / 2;
				    if (obj.offset().top > yHalfPos) {
				        $(results).css({
				            'top': 'auto',
				            'bottom': (obj.position().top + obj.outerHeight()) + 'px',
				        });
				    }
				    else {
				        $(results).css({
				            'top': (obj.position().top + obj.outerHeight()) + 'px',
				            'bottom': 'auto'
				        });
				    }
				    if (obj.offset().left > xHalfPos) {
				        $(results).css({
				            'left': 'auto',
				            'right': '0'
				        });
				    } else {
				        $(results).css({
				            'left': obj.position().left + 'px'
				        });
				    }
				
					if ($('tr', results).length > 0) {
						$(results).show();
					}
					keyListener(e);
				}). 
				attr('autocomplete', 'off');
			$(document).mousemove(function(e) {
			    pageX = e.pageX;
				pageY = e.pageY;
			});
			
			// Escape the not character if present so that it doesn't act in the regular expression
			settings.notCharacter = regexEscape(settings.notCharacter || '');
			
			// Make sure the JSON data is a JavaScript object if given as a string.
			if (settings.data && typeof settings.data === 'string') {
				settings.data = $.parseJSON(settings.data);
			}
		});
	};

})(jQuery);
var _i = 0;