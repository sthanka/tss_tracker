<?php
if ($this->session->userdata('user_type_id') == 2 || ($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1)) {
    ?>
    <?php if (count($projectTaskList) > 0): ?>
        <table class="table table-striped table-sub-head" id="taskLoad">
            <thead>
            <tr>
                <th>Task</th>
                <th>Sub Task</th>
                <th>Estimated</th>
                <th>Allotted To</th>
                <th>Status</th>
                <th colspan="8" class="text-center">Week</th>
            </tr>
            </thead>
            <tbody>

            <tr class="main-tr">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?= date("D (d)", $startDate); ?></td>
                <td><?= date("D (d)", strtotime('+1 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+2 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+3 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+4 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+5 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+6 day', $startDate)); ?></td>
                <td>Total</td>
            </tr>

            <?php

            foreach ($projectTaskList as $key=>$eachProjectTasks):
                echo '<tr><td class="ProjectTitleRow rowTitle" colspan="13"><b>'.$key.'</b></td><tr>';
                foreach ($eachProjectTasks as $item): ?>
                    <tr>
                        <td><?= $item['parent_name'] .'('.$item['department_name'].')'?>
                            <input type="hidden" value="<?= $item['project_id'] ?>"/>
                            <input type="hidden" name="id_task_workflow" value="<?= $item['id_task_workflow'] ?>"/>
                        </td>
                        <td><?= $item['task_name'] ?><input type="hidden" value="<?= $item['id_project_task'] ?>"/></td>
                        <td><input type="text" class="sm-element timepicker" placeholder="Hh:Mm"
                                   value="<?= $item['estimated_time'] ?>"/></td>
                        <td>
                            <select class="no-sel2 full-width border" name="user_id">
                                <option value="0">--Select--</option>
                                <?php foreach ($projectTeamList as $team) {
                                    if($team['department_id']==$item['id_department']){
                                         $selected = ($item['assigned_to'] == $team['id_user']) ? 'selected' : '';
                                        echo '<option ' . $selected . ' value="' . $team['id_user'] . '">' . $team['email'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td><?php
                            switch ($item['status']){
                                case 'pending': echo '<span class="pull-left task-status task-inprogress">Pending</span>'; break;
                                case 'accepted': echo '<span class="pull-left task-status task-open">Accepted</span>'; break;
                                case 'progress': echo '<span class="pull-left task-status task-waiting">In Progress</span>'; break;
                                case 'completed': echo '<span class="pull-left task-status task-completed">Completed</span>'; break;
                                case 'hold': echo '<span class="pull-left task-status task-open">Hold</span>'; break;
                            }

                            ?>

                        </td>
                        <?php
                        $sum = 0;
                        for ($i = 0; $i <= 6; $i++) {
                            $time = 0;
                            if (isset($item['task'][$i]['time'])) {
                                $time = $item['task'][$i]['time'];
                                $sum = $sum + $time;
                            }
                            echo ' <td>';
                            echo ' <input name="allotted_time" type="text" placeholder="Hh:Mm" class="timepicker allotted_time sm-element form-control" value="' . $time . '"/>';
                            echo '<input type="hidden" name="date" value="' . date("Y-m-d", strtotime('+' . $i . ' day', $startDate)) . '"/>';
                            echo ' </td>';

                        }
                        ?>
                        <td><?= $sum ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="col-lg-12 text-center">
            <button class="btn blue-btn" onclick="saveWorkLoad()">Save</button>
        </div>
    <?php endif; ?>

    <?php //echo "<pre>"; print_r($deptProjectTaskList); exit;
    $user_work_load = $users = $tasks = array();
    $user_work_load_id = array();
    $si = 0;
    foreach ($deptProjectTaskList as $item):
        //getting workload by team
        //echo "<pre>"; print_r($item); exit;
        if(!in_array($item['assigned_to'],$user_work_load_id)){
            array_push($user_work_load_id,$item['assigned_to']);
            $user_work_load[$item['assigned_to']] = array(
                'user_id' => $item['assigned_to'],
                'user_name' => $item['user_name'],
                'user_email' => $item['email'],
                'day' => array(),
                'time' =>array()

            );
        }

        $sum = 0;
        for ($i = 0; $i <= 6; $i++) {
            $time = 0;
            if (isset($item['task'][$i]['time'])) {
                //for getting workload bu team
                if (in_array($item['assigned_to'], $user_work_load_id)) {
                    if (!in_array($i, $user_work_load[$item['assigned_to']]['day'])) {
                        array_push($user_work_load[$item['assigned_to']]['day'], $i);
                        if (!isset($user_work_load[$item['assigned_to']]['time'][$i])) {
                            $user_work_load[$item['assigned_to']]['time'][$i] = 0;
                        }

                        if($item['task'][$i]['time']=='' || $item['task'][$i]['time']==0){ $item['task'][$i]['time'] = '00:00'; }
                        //echo $item['task'][$i]['time'].'--'.'00:00';
                        //echo strtotime($item['task'][$i]['time']).'--'.strtotime("00:00").'-------';
                        //echo strtotime($item['task'][$i]['time'])-strtotime("00:00").'--->'.date('H:i',(strtotime($item['task'][$i]['time'])-strtotime("00:00"))).'<br>';
                        //$secs = strtotime($item['task'][$i]['time'])-strtotime("00:00");
                        $secs = 0;
                        if($item['task'][$i]['time'] !=0) {
                            sscanf($item['task'][$i]['time'], "%d:%d", $hours, $minutes);
                            $secs = $hours * 3600 + $minutes * 60;
                        }

                        $user_work_load[$item['assigned_to']]['time'][$i] = $user_work_load[$item['assigned_to']]['time'][$i] + $secs;

                    } else {
                       /* if($item['task'][$i]['time']=='' || $item['task'][$i]['time']==0){ $item['task'][$i]['time'] = '00:00'; }
                        $secs = strtotime($item['task'][$i]['time'])-strtotime("00:00");*/
                        $secs = 0;
                        if($item['task'][$i]['time'] !=0) {
                            sscanf($item['task'][$i]['time'], "%d:%d", $hours, $minutes);
                            $secs = $hours * 3600 + $minutes * 60;
                        }
                        $user_work_load[$item['assigned_to']]['time'][$i] = ($user_work_load[$item['assigned_to']]['time'][$i] + $secs);

                    }

                }
                //$time = $item['task'][$i]['time'];
                //$sum = $sum + $item['task'][$i]['time'];
            }

        }
        //echo "<pre>"; print_r($user_work_load); exit;
        ?>

    <?php endforeach; ?>

    <div class="col-sm-12 clearfix clearboth padding0">
        <div class="col-sm-12 padding0">
            <h3 class="heading">Workload by Team</h3>
            <div class="col-sm-12 clearfix clearboth padding0 top20">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Mon</th>
                        <th>Tus</th>
                        <th>Wed</th>
                        <th>Thu</th>
                        <th>Fri</th>
                        <th>Sat</th>
                        <th>Sun</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php //echo "<pre>"; print_r($user_work_load); exit;
                    foreach ($user_work_load as $user_work) {
                        $user_work_sum = 0; //echo $user_work_sum.'<br>';
                        ?>
                        <tr>
                            <td><?= $user_work['user_email'] ?></td>
                            <?php foreach ($user_work['day'] as $day) { ?>
                                <td><?php if($user_work['time'][$day]!='' && $user_work['time'][$day]!=0){  $hours = floor($user_work['time'][$day] / 3600);
                                        $mins = floor($user_work['time'][$day] / 60 % 60);
                                        echo $hours.':'.$mins; }else{ echo 0; } ?></td>
                                <!--<td><?/*= $user_work['time'][$day]- 3600 */?></td>-->
                            <?php if($user_work['time'][$day]!=0){ $user_work_sum = $user_work_sum + ($user_work['time'][$day]); } } ?>
                            <?php if (count($user_work['day']) == 0) {
                                for ($sth = 0; $sth < 7; $sth++) { ?>
                                    <td>0</td>
                                <?php }
                            } ?>
                            <td><?php  if($user_work_sum ==0) echo $user_work_sum;
                                        else {
                                            $hours = floor($user_work_sum / 3600);
                                            $mins = floor($user_work_sum / 60 % 60);
                                            echo $hours.':'.$mins;
                                        } ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        Number.prototype.padDigit = function () {
            return (this < 10) ? '0' + this : this;
        }

        TssLib.docReady(function () {

            $('#taskLoad').find("input[name='allotted_time']").change(function () {
                var $this = $(this).parent().parent('tr');
                $sum = 0;
                var t1 = "00:00";
                var mins = 0;
                var hrs = 0;
                $this.find('td').each(function () {
                    var tdRow = $(this);
                    // console.log('tdRow', tdRow);
                    var time = tdRow.find(".allotted_time").val();
                    //console.log('time', time);

                    if (time && time != 0) {
                        t1 = t1.split(':');
                        var t2 = time.split(':');
                        mins = Number(t1[1]) + Number(t2[1]);
                        minhrs = Math.floor(parseInt(mins / 60));
                        hrs = Number(t1[0]) + Number(t2[0]) + minhrs;
                        mins = mins % 60;
                        t1 = hrs.padDigit() + ':' + mins.padDigit();
                        //console.log('t1', t1);
                        // $sum = $sum + parseInt(time);
                    }
                })
                var estimation_time = $(this).parent().parent('tr').find('td').eq(2).find('input').val();
                if (estimation_time == "") {
                    $(this).parent().parent('tr').find('td').eq(2).find('input').val('00:00')
                }
                if ((hmsToSecondsOnly(estimation_time) < hmsToSecondsOnly(t1)) || estimation_time == "") {
                    $(this).val('00:00');
                    TssLib.notify('Check allotted Time', 'error', 5);
                } else {
                    $('td', $this).eq(12).html(t1);
                }
            })
            $('#taskLoad tbody').find("tr").each(function () {
                $(this).find("td>input[name='allotted_time']").trigger('change');
            })
            TssLib.bindAllEventsOnPageRender('#taskLoad');
            //mask
            $.mask.definitions['H'] = "[0-9]";
            $.mask.definitions['h'] = "[0-9]";
            $.mask.definitions['M'] = "[0-5]";
            $.mask.definitions['m'] = "[0-9]";
            $.mask.definitions['P'] = "[AaPp]";
            $.mask.definitions['p'] = "[Mm]";

            $(".timepicker").mask("Hh:Mm");
        })

        function hmsToSecondsOnly(str) {
            var hm = str;   // your input string
            var a = hm.split(':'); // split it at the colons

// minutes are worth 60 seconds. Hours are worth 60 minutes.
            var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60;
            return seconds;
        }

        function saveWorkLoad() {
            var insertData = [];
            $.each($('#taskLoad').find('tbody > tr'), function (i) {
                var $td = $('td', this);
                if (i != 0 && $td.eq(3).find('select').val() != "0") {

                    var $obj = {};
                    //$obj['project_id'] = $td.eq(0).find('input').val();
                    $obj['task_id'] = $td.eq(1).find('input').val();
                    $obj['estimated_time'] = $td.eq(2).find('input').val();
                    $obj['assigned_to'] = $td.eq(3).find('select').val();
                    $obj['id_task_workflow'] = $td.eq(0).find("input[name='id_task_workflow']").val();
                    $obj['project_id'] = $td.eq(0).find('input').val();

                    var $taskObj = [];
                    for (var j = 5; j <= 11; j++) {
                        var _obj = {};
                        _obj['date'] = $td.eq(j).find("input[name='date']").val();
                        _obj['project_id'] = $td.eq(0).find('input').val();
                        _obj['task_workflow_id'] = $td.eq(0).find("input[name='id_task_workflow']").val();
                        _obj['allotted_time'] = $td.eq(j).find("input[name='allotted_time']").val();
                        $taskObj.push(_obj);
                    }
                    $obj['task'] = $taskObj;
                    //$obj['task']['date'] =
                    // $obj['task']['allotted_time'] = $td.eq(4).find("input[name='allotted_time']").val();
                    insertData.push($obj);
                }

            })
            $.ajax({
                type: 'post',
                //contentType:"application/json; charset=utf-8",
                url: '<?php echo base_url('index.php/Workload/ajaxWorkloadSave/')?>',
                data: {'data': insertData},
                dataType: "json",
                async: true,
                success: function () {
                    TssLib.notify('Successfully Save', null, 5);
                    $('.selectable-list').find('.selected').trigger('click');
                }
            });
            //console.log('insertData', insertData);
        }

</script>
<?php
}
else{
if (count($projectTaskList) > 0): ?>
<table class="table table-striped table-sub-head" id="taskLoad">
    <thead>
    <tr>
        <th>Task</th>
        <th>Sub Task</th>
        <th>Estimated</th>
        <th colspan="8" class="text-center">Week</th>
    </tr>
    </thead>
    <tbody>
    <tr class="main-tr">
        <td></td>
        <td></td>
        <td></td>

                <td><?= date("D (d)", $startDate); ?></td>
                <td><?= date("D (d)", strtotime('+1 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+2 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+3 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+4 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+5 day', $startDate)); ?></td>
                <td><?= date("D (d)", strtotime('+6 day', $startDate)); ?></td>
                <td>Total</td>
            </tr>


    <?php
    $user_work_load = $users = $tasks = $user_work_load_day = array();
    $user_work_load_id = array();
    $si = $user_work_load_day['sum'] = 0;
    foreach ($projectTaskList as $eachProjectTask):
    foreach ($eachProjectTask as $item):
         if(!in_array($item['assigned_to'],$user_work_load_id)){
            array_push($user_work_load_id,$item['assigned_to']);
            $user_work_load[$item['assigned_to']] = array(
                'user_id' => $item['assigned_to'],
                'user_name' => $item['user_name'],
                'user_email' => $item['email'],
                'day' => array(),
                'time' =>array()

                    );
                }

                $users[$item['assigned_to']] = array(
                    'user_id' => $item['assigned_to'],
                    'user_name' => $item['user_name']
                );
                //$task_names
                $tasks[$item['id_project_task']] = array(
                    'project_task_id' => $item['id_project_task'],
                    'task_name' => $item['task_name'],
                    'parent_name' => $item['parent_name']
                );


                ?>
                <tr>
                    <td><?= $item['parent_name'] ?>
                        <input type="hidden" value="<?= $item['project_id'] ?>"/>
                        <input type="hidden" name="id_task_workflow" value="<?= $item['id_task_workflow'] ?>"/>
                    </td>
                    <td><?= $item['task_name'] ?><input type="hidden" value="<?= $item['id_project_task'] ?>"/></td>
                    <td><?= $item['estimated_time'] ?></td>

            <?php
            $sum = $day_sum = 0;
            for ($i = 0; $i <= 6; $i++) {
                $time = '00:00'; $secs =0;
                if(isset($item['task'][$i]['time']) && $item['task'][$i]['time']!='' && $item['task'][$i]['time']!=0) {
                    $time = $item['task'][$i]['time'];
                    sscanf($item['task'][$i]['time'], "%d:%d", $hours, $minutes);
                    $secs = $hours * 3600 + $minutes * 60;
                    $sum = $sum + $secs;

                    if($time==''){ $time ='00:00';  }

                    //getting day wise workload

                    if(!isset($user_work_load_day[$i])){ $user_work_load_day[$i]=0; }

                    $user_work_load_day[$i] =  $user_work_load_day[$i] + $secs;
                    $user_work_load_day['sum'] =  $user_work_load_day['sum'] + $sum;

                }
                else{

                    if(!isset($user_work_load_day[$i])){ $user_work_load_day[$i]=0; }

                    $user_work_load_day[$i] = $user_work_load_day[$i] + 0;
                    $user_work_load_day['sum'] =  $user_work_load_day['sum'] + $sum;

                }
                echo ' <td>';
                echo $time;
                //echo '<input type="hidden" name="date" value="' . date("Y-m-d", strtotime('+' . $i . ' day', $startDate)) . '"/>';
                echo ' </td>';


            }
            ?>
            <td>
                <?php
                $hours = floor($sum / 3600);
                $mins = floor($sum / 60 % 60);
                echo $hours.':'.$mins;

                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php endforeach; ?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <?php $total = 0; for($s=0;$s<=6;$s++){ $total = $total + $user_work_load_day[$s];
                $hours = floor($user_work_load_day[$s] / 3600);
                $mins = floor($user_work_load_day[$s] / 60 % 60);
            $hours = ($hours<10)?'0'.$hours : $hours;
            $mins = ($mins<10)?'0'.$mins : $mins;
                $sr = $hours.':'.$mins;
        ?>
            <td><?=$sr?></td>
        <?php }
        $hours = floor($total / 3600);
        $mins = floor($total / 60 % 60);
        $hours = ($hours<10)?'0'.$hours : $hours;
        $mins = ($mins<10)?'0'.$mins : $mins;
        $sr = $hours.':'.$mins;
        ?>
        <td><?=$sr?></td>
    </tr>
    </tbody>
</table>

<?php endif; } ?>

