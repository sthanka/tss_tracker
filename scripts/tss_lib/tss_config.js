var CACHE = false; //TssConfig.BASE_URL//TssConfig.GRID_MAX_ROW_NO//TssConfig.ALL_READY_EXIST
$.Class.extend('TssConfig',
		/* @static */
		{
		    CACHE: CACHE,
		    ALL_READY_EXIST: 'Already exist',
		    GET_SCRIPT_OPTIONS: { type: "GET", dataType: "script", cache: CACHE, async: false },
		    GET_JSON_OPTIONS: { type: "GET", dataType: "json", cache: CACHE },
		    POST_JSON_OPTIONS: { type: "POST", dataType: "json", cache: CACHE },
		    GET_PAGE_OPTIONS: { type: "GET", cache: CACHE },
		    POST_PAGE_OPTIONS: { type: "POST", cache: CACHE },
		    GRID_MAX_ROW_NO: 100000,
		    DATE_FORMAT_FOR_PICKER: 'dd/mm/yyyy',
		    DATE_FORMAT_JS: 'd/m/Y',
		    SHOW_LOGS: true,
		    NOTIFY_TIMER: 10,
		    TT_SERVICE_URL: WEB_BASE_URL+'index.php/',
		    BASE_URL: WEB_BASE_URL+'index.php/',
		    SITE_URL: WEB_BASE_URL,
		    USER_UPLOAD_URL: 'http://localhost/Uploads/UserUploads/',
		    REPORT_FILE_URL: 'http://localhost/ReportDownloads/',
		    /*
            BASE_URL: 'http://123.176.35.8/erp_services/Services/',
		    SITE_URL: 'http://123.176.35.8/erp/',
            */
		    SERVER_ERROR: 'Server Error, Please contact administrator'
		},
		/* @prototype */
		{
		    init: function () {

		    }
		});