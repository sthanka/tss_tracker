<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_types extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Task_list_model');
        if($this->session->userdata('user_type_id')==3){
            if($this->session->userdata('user_type_id')==2 || $this->session->userdata('is_lead')==1){

            }else{
                redirect(WEB_BASE_URL);
            }
        }
    }

    public function index(){
        $data['content'] = 'task_types/index';
        $this->load->view('partials/landing',$data);
    }

    public function meeting(){
        $data['content'] = 'task_types/meeting';
        $this->load->view('partials/landing',$data);
    }

    public function call(){
        $data['content'] = 'task_types/call';
        $this->load->view('partials/landing',$data);
    }

    public function interview(){
        $data['content'] = 'task_types/interview';
        $this->load->view('partials/landing',$data);
    }

    public function getTaskTypesGrid()
    {
        $params = $this->input->get();
        $taskTypesList = $this->Task_list_model->getTaskTypesGrid($params);
        echo json_encode($taskTypesList);
    }

    public function addTaskTypes(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $id = $data['id_task_type'];

        if($id == null && $id == '') {
            $res = $this->Task_list_model->addTaskTypes($data);
        }else{
            $res = $this->Task_list_model->updateTaskTypes($id,$data);
        }
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$res));
    }

    public function addTaskTypesOthers(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $id = $data['id_task_type'];

        if($this->Task_list_model->checkTaskTypeAlreadyPresent($data)){
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Task type already present','data'=>''));exit;
        }

        if($id == null && $id == '') {
            $res = $this->Task_list_model->addTaskTypesOthers($data);
        }else{
            $res = $this->Task_list_model->updateTaskTypesOthers($id,$data);
        }
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$res));exit;
    }

    public function deleteTaskTypesById()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if($this->Task_list_model->checkTaskTypesAssigned($data['task_type_id'])){
            $status = $this->Task_list_model->deleteTaskTypesById($data['task_type_id']);
            if($status){
                echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'Task type deleted'));
            }else{
                echo json_encode(array('status'=>false,'error'=>true,'message'=>''));
            }
        }else{
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Task type already in use', 'data'=>[]));
        }

    }
}