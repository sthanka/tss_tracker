$(window).load(function () {
    $body = $('body');
    $body.click(function () {
        var ele = $('div.ac-section.jsonSuggest');
        $.each(ele, function () {
            if ($(this).is(':visible')) {
                if ($(this).prev()[0] != $(':focus')[0])
                    $(this).prev().trigger('blur');
            }
        });
    });
    $(window).on('load resize',function(){
        minScroll();
    });

    // Page Preloader
    $('#body-status').fadeOut();
    $('#preloader').fadeOut();
    $leftPan = $('.leftpanel'), $mainPan = $('.mainpanel'), $mainWA = $('.main-work-area'), $pageHeader = $('.pageheader');
    if ($('#modules-ul').length > 0) {
        $leftPan.hide();
        $('.menutoggle').hide();
        $pageHeader.width($(document).width());
        $mainPan.css('padding-left', 0);
        $('.logopanel').css('margin-left', '-20px');
    } else {
        $pageHeader.width($(window).width() - $leftPan.width());
        $mainPan.css('padding-left', $leftPan.width());
    }

    $('.menutoggle').click(function () {
        var cur = $(this);
        if ($body.hasClass('leftpanel-collapsed')) {
            $body.removeClass('leftpanel-collapsed');
            $('.nav-active-sm').addClass('nav-active').removeClass('nav-active-sm').find('ul.children').show();
        } else {
            $body.addClass('leftpanel-collapsed');
            $('.nav-active').addClass('nav-active-sm').removeClass('nav-active').find('ul.children').hide();
        }
        $pageHeader.width($(document).width() - $leftPan.width());
        $mainPan.css('padding-left', $leftPan.width());
        resizeGrids();
    });
    $(window).resize(function () {
        var mainPH = $(window).height() - $('.headerbar').height();
        $mainPan.css('height', mainPH);
        $leftPan.css('height', mainPH);
        $mainWA.css('min-height', mainPH - $('.footer-logo').height() - 30);

        if ($('#modules-ul').length > 0) {
            $pageHeader.width($(document).width());
        } else {
            $pageHeader.width($(window).width() - $leftPan.width());
        }
    });
    $(window).resize();
    $mainPan.mCustomScrollbar({
        theme: "dark", scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true,autoHideScrollbar:true,
        callbacks: {
            onOverflowY: function () {
                $('.mCSB_inside > .mCSB_container').css('margin-right', 15);
                if ($('#modules-ul').length > 0) {
                    $('.pageheader').width($(document).width());
                }
                else {
                    { $('.pageheader').width($(document).width() - $leftPan.width()); }
                }
            },
            onOverflowYNone: function () { $('.mCSB_inside > .mCSB_container').css('margin-right', 15); },
            onScroll: function () { $('.tooltip').removeClass('in'); }
        }
    });
    $leftPan.mCustomScrollbar({ theme: "dark", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
    //Tooltip
    $(document).mousemove(function (e) {
		if ($('body').find('.tooltip').length > 1) {
           $('body').find('.tooltip:not(:last)').removeClass('in').remove();
       }
        $('body').on('click scroll keydown', function () {
            $('.tooltip').removeClass('in').remove();
        });
        $('[title]').tooltip({
            container: "body",
        }).removeAttr('title');
    });
    $('.modal').attr('data-backdrop', 'static');
    $('.modal').attr('data-keyboard', 'true');
    $('a[data-dismiss="modal"]').attr('title', 'Close');
	$('#oak_popup').on('hidden.bs.modal', function () {
		$('body').removeClass('blur');
	});
});
function _menuLeft(e) {
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseleave');
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseenter');
    $('.leftpanel .nav-parent > a').unbind('click');
    $('.leftpanel .nav-bracket > li').mouseenter(function (event) {
        var cur = $(this);
        var halfLPHt = parseInt(($(window).height() - 80) / 2);
        if ($body.hasClass('leftpanel-collapsed')) {
            cur.addClass('nav-hover');
            if (cur.offset().top < halfLPHt) {
                cur.find('ul.children').show().css({ 'top': cur.offset().top + cur.height() });
            } else {
                cur.find('ul.children').show().css({ 'top': cur.offset().top - cur.find('ul.children').height() + 8 });
            }
            $("li.nav-hover ul.children").mCustomScrollbar({ theme: "light", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
        }
    }).mouseleave(function (event) {
        var cur = $(this);
        cur.removeClass('nav-hover');
        cur.find('ul.children').css({ 'top': '' });
        if ($body.hasClass('leftpanel-collapsed'))
            cur.find('ul.children').hide();
    });
    // Toggle Left Menu
    $('.leftpanel .nav-parent > a').click(function () {
        var parent = $(this).parent();
        var sub = parent.find('> ul');
        if (!$('body').hasClass('leftpanel-collapsed')) {
            if (sub.is(':visible')) {
                sub.slideUp(200, function () {
                    parent.removeClass('nav-active active');
                    $('.mainpanel').css({ height: '' });
                });
            } else {
                $('.leftpanel .nav-parent').each(function () {
                    var t = $(this);
                    if (t.hasClass('nav-active')) {
                        t.find('> ul').slideUp(200, function () {
                            t.removeClass('nav-active');
                        });
                    }
                });
                $('.nav-bracket > li').removeClass('nav-active active')
                parent.addClass('nav-active');
                sub.slideDown(200);
            }
        }
    });
}
TssLib.docReady(function () {
    $('.custom-drop').click(function () {
        $('.dropdown-menu').closest('.open').removeClass('open')
    });

	$('#oak_popup').on('hidden.bs.modal', function () {
		$('body').find('.closeThisPopover').trigger('click');
	});
   /* $('body').on('click', function(event){
        var _target = $(event.target), _link = $('.link'), _i = _link = $('.link i.fa');
        if(!_target.is(_link)){
           $('.grid-actions').hide();
        }
        if(!_target.is(_i)){
            $('.grid-actions').hide();
        }
    });*/
});
$(document).ready(function(){
   /* if($('.tsk-chklist-wrapper input[type="checkbox"]:checked')){
        $(this).parent().css({'color':'red','text-decoration':'line-through'});
    };*/
   // $('.tsk-chklist-wrapper input[type="checkbox"]:checked').parent().css({'color':'red','text-decoration':'line-through'});
    //$('.tsk-chklist-wrapper input[type="checkbox"]:checked').alert();
});

function showFilterBox(e) {
    $('#filter-box').slideToggle();
}
function showAction(e){
    if(!$(e).hasClass('opened')){
        $(e).addClass('opened');
        $('body').find('ul.actions').hide();
        $(e).closest('.actions-exists').find('ul.actions').fadeIn(100);
    } else{
        $(e).removeClass('opened');
        $(e).closest('.actions-exists').find('ul.actions').hide();
    }
}