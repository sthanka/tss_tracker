
    <div class="modal fade" id="oak_popup">
        <div class="modal-dialog model-mg-width">
            <div class="modal-content model-mg-content"></div>
        </div>
    </div>
    <!-- Modal Dialog  dekete confirmation-->
    <div class="modal fade" id="oasis-confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </div>
                        <h4 class="panel-title"></h4>
                    </div>
                    <div class="panel panel-default panel-body padding20 clearfix">
                        <p></p>
                    </div>
                    <div class="panel-footer col-sm-12 clearfix text-right">
                        <button type="button" class="button-common module btn-danger">Yes</button>
                        <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--@RenderBody()-->
    <!--Render Body Start-->
    <div class="mainpanel" id="budgetTemplateTbl_wrapper">
        <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
            <div class="contentHeader">
                <h3>Departments</h3>
            </div>
            <div class="col-sm-12 clearfix clearboth pad-right-none">
                <div class="grid-details-table">
                    <div class="grid-details-table-header">
                        <h3>Department List</h3>
                        <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="grid-details-table-content clearfix padding0">
                        <div class="tbl_wrapper border0">
                            <table id="project-list" class="table-responsive"></table>
                            <div id="project-list-page"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="add-project-modal" class="modal-wrapper" style="display:none">
                <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                                <h4 class="panel-title">Add Department</h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval" name="department_name"  id="department_name"/>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Department Name :</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval" name="order"  id="order"/>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Order :</label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control rval" name="id_department" />
                            </div>
                            <div class="panel-footer text-center">
                                <button class="button-common" div-submit="true">Save</button>
                                <button class="button-color" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
            <script>
                var reference = "Allotted";
                TssLib.docReady(function () {
                    var $refTblgrid = $('#project-list');
                    $refTblgrid.jqGrid({
                        url: TssConfig.TT_SERVICE_URL + 'department/getDepartmentsGrid',
                        multiselect: false,
                        datatype: "json",
                        sortorder: "ASC",
                        extSearchField: '.searchInput',
                        colNames: ['Id', 'Name','Order','delete'],
                        colModel: [
                            { name: 'id_department', index: 'id_department', hidden: true ,key:true},
                            { name: 'department_name', index: 'department_name' },
                            { name: 'order', index: 'order' },
                            /*{ name: 'deptt_status', index: 'status' ,formatter: function (c, o, d) {
                                var sts = '';
                                if(d.department_status == 1){
                                    sts = '<span class="pull-left task-status task-completed">Active</span>';
                                }else{
                                    sts = '<span class="pull-left task-status task-waiting">Inactive</span>';
                                }
                                return  sts;
                                /!*return  sts+
                                    '<a class="pull-left task-status btn-danger ml10" data-original-title="Delete" onclick="deleteDepartment('+ d.id_department+')" href="javascript:;"><i class="fa fa-trash-o"></i></a>';*!/
                            }},*/
                            /*{ name: 'delete', index: 'department_status' ,formatter: function (c, o, d) {
                                var sts = '';
                                var status = d.department_status==1?'0':'1';
                                return  sts+
                                    '<a class="pull-left task-status btn-danger ml10" data-original-title="Delete" onclick="deleteDepartment('+ d.id_department+', '+ status+')" href="javascript:;"><i class="fa fa-trash-o"></i></a>';
                            }},*/
                            { name: 'status', index: 'department_status', hidden: true},
                        ],
                        pager: 'project-list-page'
                    }).navGrid('#project-list-page', {
                        edit: true, add: true, del: true, search: false, refresh: true,
                        addfunc: function () {
                            addEditCreateDepartment();
                        }, editfunc: function (id) {
                            var rowData = $refTblgrid.jqGrid('getRowData', id);
                            addEditCreateDepartment(rowData);
                        }, delfunc: function (id) {
                            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                                $(this).closest('.modal').modal('hide');
                                postJsonAsyncWithBaseUrl("Department/deleteDepartmentById", {'department_id': id}, {
                                    jsonContent: true,
                                    callback: function (result) {
                                        if (result.data != null) {
                                            if(result.data.status){
                                                TssLib.notify('Deleted successfully', null, 5);
                                                $('#project-list').trigger('reloadGrid');
                                            }else{
                                                TssLib.notify(result.data.message, 'warn', 5);
                                            }

                                        }
                                }});
                            }, 'Yes', 'No');
                        }
                     });
                });
                function dateFormat(c, o, d) {
                    if (TssLib.isBlank(c))
                        return '';
                    else
                        return c.DateWCF().format('d/m/Y');
                }
                function addEditCreateDepartment(rowData) {
                    var $modal = TssLib.openModel({ width: '600' }, $('#add-project-modal').html());
                    if (TssLib.isBlank(rowData)) {
                        $modal.find('.panel-title').text('Add Department');
                        $modal.find('[div-submit="true"]').text('Save');
                    } else {
                        TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                        $modal.find('.panel-title').text('Edit Department');
                        $modal.find('[div-submit="true"]').text('Update');
                    }
                    TssLib.ajaxForm({
                        jsonContent: true,
                        form: $('#linen-createSchedule-form'), callback: function (result) {

                            if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                            if (result.data.status) {
                                $('#project-list').trigger('reloadGrid');
                                TssLib.closeModal();
                                TssLib.notify('Department '+addedTest+' successfully ', null, 5);
                            }else{
                                TssLib.notify('Department name already exist ', 'warn', 5);
                            }
                        },
                        before: function (formObj) {
                            $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'department/addDepartment');
                            return true;
                        }
                    });
                }

            </script>
            <!-- InstanceEndEditable -->

        </div>
        <!--<a href="javascript:;" class="footer-logo clearfix"><img src="<?/*=WEB_BASE_URL*/?>images/people-combine-logo.png" /></a>-->
    </div>
    <!--Render Body End-->


