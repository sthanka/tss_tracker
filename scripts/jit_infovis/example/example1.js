var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
      showLog(text);
  }
};


function loadInfoVis() {
    var mdChildren = [
        { id: "1_1_4_1", name: "Executive Assistant", children: [] },
        { id: "1_1_4_2", name: "Sr, VP Strategy & Innovation", children: [] },
        { id: "1_1_4_3", name: "CFO", children: [] },
        { id: "1_1_4_4", name: "VP, Alliance & Partnership", children: [] },
        { id: "1_1_4_5", name: "Director Hospitality", children: [] }
    ];
    var jmdChildren = [
        { id: "1_1_3_1", name: "EA & MIS", children: [] },
        { id: "1_1_3_2", name: "Edu Audit", children: [] },
        { id: "1_1_3_3", name: "COO-K12", children: [] },
        { id: "1_1_3_4", name: "Head OI", children: [] },
        { id: "1_1_3_5", name: "SD Vikas", children: [] },
        { id: "1_1_3_6", name: "VP Education", children: [] },
        { id: "1_1_3_7", name: "CPO", children: [] },
        { id: "1_1_3_8", name: "CTO", children: [] },
        { id: "1_1_3_9", name: "HCC", children: [] },
        { id: "1_1_3_10", name: "FC", children: [] },
        { id: "1_1_3_11", name: "Maintenance Manager", children: [] }
    ];
    var chairmanChildren = [
        { id: "1_1_1", name: "Executive Assistant", children: [] },
        { id: "1_1_2", name: "Quality & Systems Audit", children: [] },
        { id: "1_1_3", name: "JMD, Education", children: jmdChildren },
        { id: "1_1_4", name: "MD", children: mdChildren },
        { id: "1_1_5", name: "DMD, Projects", children: [] },
        { id: "1_1_6", name: "CEO, University", children: [] },
        { id: "1_1_7", name: "CS", children: [] },
        { id: "1_1_8", name: "Director, Infra", children: [] }
    ];

    //init data
    /*
    var json = {
        id: "1",
        name: "Chairman",
        children: chairmanChildren
    };
   */
    //*
    var json = {
        id: "1",
        name: "Board of Directors", 
        children: [
            {
            id:"1_1",
                name:"Chairman", 
                children: chairmanChildren
            }
        ]
    };
    //*/
    //end
    var infovis = document.getElementById('infovis');
    var w = infovis.offsetWidth - 50, h = infovis.offsetHeight - 50;
    
    //init Hypertree
    var ht = new $jit.Hypertree({
      //id of the visualization container
      injectInto: 'infovis',
      //canvas width and height
      width: w,
      height: h,
      //Change node and edge styles such as
      //color, width and dimensions.
      Node: {
          dim: 9,
          color: "#f00" 
      },
      Edge: {
          lineWidth: 2,
          color: "#088"
      },
      onBeforeCompute: function(node){
          Log.write("centering");
      },
      //Attach event handlers and add text to the
      //labels. This method is only triggered on label
      //creation
      onCreateLabel: function(domElement, node){
          domElement.innerHTML = node.name;
          $jit.util.addEvent(domElement, 'click', function () {
              ht.onClick(node.id, {
                  onComplete: function() {
                      ht.controller.onComplete();
                  }
              });
          });
      },
      //Change node styles when labels are placed
      //or moved.
      onPlaceLabel: function(domElement, node){
          var style = domElement.style;
          style.display = '';
          style.cursor = 'pointer';
          if (node._depth <= 1) {
              style.fontSize = "1em";
              style.color = "#19274A";

          } else if(node._depth == 2){
              style.fontSize = "0.8em";
              style.color = "#A6D0E7";

          } else {
              style.display = 'none';
          }

          var left = parseInt(style.left);
          var w = domElement.offsetWidth;
          style.left = (left - w / 2) + 'px';
      },
      
      onComplete: function(){
          Log.write("done");
      }
    });
    //load JSON data.
    ht.loadJSON(json);
    //compute positions and plot.
    ht.refresh();
    //end
    ht.controller.onComplete();
}
