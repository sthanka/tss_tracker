/*--------------------------------------------------------------------------------------------------------*/
/*----------------------------------Tss Library js Start--------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------*/
$.Class.extend('TssLib',
		/* @static */
		{
		    init: function () {
		        //loadJs($tss_lib_path + 'tss_validations-1.0.js');
		        //loadJs($tss_lib_path + 'jquery.numeric.js');
		        $(function () {
		            TssLib.bindAllEventsOnPageRender(this);
		            $('body').delegate('.date-icon', 'click', function () { $(parent).prev().focus(); });
		            try {
		                // Radialize the colors
		                Tsscharts.getOptions().colors = Tsscharts.map(Tsscharts.getOptions().colors, function (color) {
		                    return {
		                        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
		                        stops: [
                                    [0, color],
                                    [1, Tsscharts.Color(color).brighten(-0.4).get('rgb')] // darken
		                        ]
		                    };
		                });
		            } catch (e) { }
		        });
		        /*
		        $('body').delegate('input[type="checkbox"]', 'change', function () {
		            showLog(1);
		        });
                *///Trying to Implement checkbox listner globally
		        $.support.cors = true;
		        $.ajaxSetup({
		            cache: CACHE,
		            error: function (x, e) {
		                if (x.status == 500)
		                    alert('Internel Server Error. Please contact administrator for status');
		                else if (x.status == 404)
		                    alert('Requested url not found, Please contact administrator.');
		                TssLib.removeAjaxLoader(this);
		            },
		            beforeSend: function () {
						if($('body').attr('name')!='no-loader'){
							if (TssLib.isBlank(this.showLoader) || (this.showLoader + '').toLowerCase() === 'true') {
								this.loader = $('<div/>', { 'class': 'oasis-loader-overlay fade' });
								$('body').append(this.loader);
							}
						}
						else{
							return ;
						}
		            },
		            complete: function () {
		                TssLib.removeAjaxLoader(this);
		            },
		            success: function () {
		                TssLib.removeAjaxLoader(this);
		            }
		        });
		    }, forwardAsPostUrl: function (url, o) { 
		        var _form = $('<form/>', { method: 'POST', action: url ,target:'_blank'});
		        $.each(o, function (k,v) {
		            _form.append($('<input/>', { type: 'hidden', name: k, value: v }));
		        });
		        $('body').append(_form);
		        _form.submit();
		    }, forwardAsPostWithBaseUrl: function (url, o) {
		        TssLib.forwardAsPostUrl(TssConfig.SITE_URL + url, o);
		    }, parseFloat: function (o) {
		        if (TssLib.isBlank(o))
		            return 0;
		        else {
		            var val = parseFloat($.trim(o));
		            if (isNaN(val))
		                return 0;
                    else
		            return val;
		        }
		    }, parseInt: function (o) {
		        if (TssLib.isBlank(o))
		            return 0;
		        else {
		            var val = parseInt($.trim(o), 10);
		            if (isNaN(val))
		                return 0;
		            else
		                return val;
		        }
		    }, PreselectCustomDataTagValue: function (selector, value) { 
                $(selector).removeClass('active');
		        if($(selector).attr('single-select') == 'true') { 
		            $(selector).find('a[data-id="'+value+'"]').addClass('active');
		        } else { 
		            if (!$.isArray(value)) {
                        value=[value];
                    }
		            $.each(value, function() {
                        $(selector).find('a[data-id="'+this+'"]').addClass('active');
                    });
		        }
		    }, IsAnyMatchInArray: function (a1, a2, key1, key2) {
		        var flg =false;
		        if(!$.isArray(a1)) a1=[a1];
                if(!$.isArray(a2)) a2=[a2];
                $.each(a1, function (i) {
                    var a1V = a1[i];  
                    if (!TssLib.isBlank(key1)) a1V =a1[i][key1];
                    if (!TssLib.isBlank(key2)) {
                        $.each(a2, function (j) {
                            if (a2[j][key2] == a1V) {
                                flg =true;
                                return false;
                            }
                        });
                            if(flg);
                            return false;
                    } else { 
                        $.each(a2, function (j) {
                            if (a2[j]==a1V) {
                                flg =true;
                                return false;
                            }
                        });
                            if(flg); 
        		    }
                });
                return flg;
		    }, CustomDataTagValue: function (selector, getObj) {
		        if (TssLib.isBlank(getObj)) getObj = false;
		        if ($(selector).attr('single-select') == 'true')
		            if (getObj)
		                return $(selector).find('a.active').data('data');
                    else
		                return $(selector).find('a.active').attr('data-id');
		        else {
		            var dat = [];
		            $.each($(selector).find('a.active'), function () {
		                if (getObj)
		                    dat.push($(this).data('data'));
		                else
		                    dat.push($(this).attr('data-id'));
		            });
		            return dat;
		        }
		    }, CustomDataTagTitle: function (selector, getObj) {
		        if (TssLib.isBlank(getObj)) getObj = false;
		        if ($(selector).attr('single-select') == 'true')
		            if (getObj)
		                return $(selector).find('a.active').data('data');
		            else
		                return $(selector).find('a.active').attr('data-original-title');
		        else {
		            var dat = [];
		            $.each($(selector).find('a.active'), function () {
		                if (getObj)
		                    dat.push($(this).data('data'));
		                else
		                    dat.push($(this).attr('data-original-title'));
		            });
		            return dat;
		        }
		    }, CustomDataTag: function (selector, data, option) {
		        if (TssLib.isBlank(option)) option = {};
		        option.singleSelect = TssLib.isBlank(option.singleSelect) ? true : option.singleSelect;
		        option.readOnly = TssLib.isBlank(option.readOnly) ? false : option.readOnly;
		        option.canRemoveAll = TssLib.isBlank(option.canRemoveAll) ? false : option.canRemoveAll;
		        option.checkUncheck = TssLib.isBlank(option.checkUncheck) ? false : option.checkUncheck;
		        option.selectAll = TssLib.isBlank(option.selectAll) ? true : option.selectAll;
		        option.maxSelect = TssLib.isBlank(option.maxSelect) ? 0 : option.maxSelect;
		        option.title = TssLib.isBlank(option.title) ? false : option.title;
		        selector = $(selector);
		        $.each(selector, function () {
		            var fld = $(this);
		            if (option.checkUncheck){
		                if (fld.parent().find('.TagChkAll-Uckall').length == 0) {
		                    var _lbl = $('<label style="font-size:10px;" class="checkbox-text-label TagChkAll-Uckall">\
                                         ( <a class="sact" style="cursor:pointer" title="Select All">Select All</a> \
                                        / <a class="usact" style="cursor:pointer" title="UnSelect All">Un Select All</a> ) \
                                         </label>');
		                    _lbl.insertBefore(fld);
		                    _lbl.find('a.sact').click(function (e) { $(this).parent().next().find('.boxed-tags>a').removeClass('active').addClass('active'); if (option.onSelect) option.onSelect(e); });
		                    _lbl.find('a.usact').click(function (e) { $(this).parent().next().find('.boxed-tags>a').removeClass('active'); if (option.onSelect) option.onSelect(e); });
		                }
		            }
		            if (!fld.hasClass('custom-data-tag')) fld.addClass('custom-data-tag');
		            var valKey=fld.attr('value-key');
		            var textKey=fld.attr('text-key');
		            if(TssLib.isBlank(valKey))valKey='Id';
		            if (TssLib.isBlank(textKey)) textKey = 'Name';
		            var _div = $('<div/>', { class: 'boxed-tags' });
		            fld.empty().append(_div);
		            fld.attr('single-select', option.singleSelect);
		            if (TssLib.isBlank(data) || jQuery.isEmptyObject(data)) {
                        _div.append($('<a/>', { text:'No Data'}));
                    }else    
		            $.each(data, function (i) {
		                var item = this;
		                var _aAttrOpt = { class: 'ellipsis ' + (((!option.singleSelect && (option.selectAll && (option.maxSelect == 0 || i < option.maxSelect))) || (i == 0)) ? 'active' : ''), text: item[textKey], 'data-id': item[valKey] };
		                if (option.title) {
		                    _aAttrOpt.title = item[textKey];
		                } 
		                var _a = $('<a/>', _aAttrOpt);
		                _a.data('data', item);
		                if (!TssLib.isBlank(option.minWidth)) _a.css('min-width', option.minWidth + 'px');
		                if (!TssLib.isBlank(option.maxWidth)) _a.css('max-width', option.maxWidth + 'px');
		                _div.append(_a);
		                if (!option.readOnly)
		                _a.click(function (e) {
		                    var cur = $(this);
		                    if (option.singleSelect) {
		                        cur.parent().find('>a').removeClass('active');
		                        cur.addClass('active');
		                        if (option.onSelect) option.onSelect(e);
		                    } else {
		                        if (cur.hasClass('active')) {
		                            if (cur.parent().find('>a.active').length > 1 || option.canRemoveAll) {
		                                cur.removeClass('active');
		                                if (option.onSelect) option.onSelect(e);
		                            }
		                        } else {
		                            if (option.maxSelect > 0 && cur.parent().find('>a.active').length >= option.maxSelect) {
		                                TssLib.notify('Sorry, you can\'t select morethan ' + option.maxSelect+'.', 'warn');
		                            }else{
		                                cur.addClass('active');
		                                if (option.onSelect) option.onSelect(e);
		                            }
		                        }
		                    }
		                });
		            });
		        }); 
		    }, LoadCsHtmlPage: function (view, ele) {
		        postPageAsyncWithSiteUrl('Common/LoadView', { viewName: view }, {
		            callback: function (rs) {
		                if (rs.success) {
		                    ele.empty().append(rs.data);
		                    TssLib.bindAllEventsOnPageRender(ele[0]);
		                }
		            }
		        });
		    }, formatHeaderForRotationInJqGrid: function (id, Head,text,height) {
		        var ele = $('#jqgh_' + id + '_' + Head);
		        ele.css('height',(height+10)+'px').css('position', 'absolute').css('top',(-height-8) );
		        var span = ele.find('span.s-ico');
		        var div = $('<div/>', { class: 'svg-hold', style: ('height:' + (height + 10) + 'px')  });
		        ele.empty().append(div).append(span);
		        var _svg = $('<svg/>', { xmlns: 'http://www.w3.org/2000/svg', style: 'border: 0px solid;', fill: '#666' });// width: '25', height: height + 6
		        var _g = $('<g/>', { transform: 'translate(0,' + height + ') rotate(-90 0 0)' });
		        _svg.append(_g);
		        var _text = $('<text/>', { x: '0', zindex: '1', transform: 'translate(0,17)', html: text });
		        _g.append(_text);
		        ele.find('.svg-hold').html(_svg);
		        _svg.attr("width", "25px").attr("height", (height + 6)+"px");
		    }, formatHeaderForAmountInJqGrid: function (id, Head) {
		        var ele = $('#jqgh_' + id + '_' + Head); 
		        var span = ele.find('span.s-ico');
		        ele.empty().append($('<span/>', {class:'fcur',text:Head})).append(span);
		    }, dateTimeFormatterForJqGrid: function (c, r, o) {
		        return c.DateWCF().format('d/m/Y h:i A');
		    }, dateFormatterForJqGrid: function (c, r, o) {
		        if (c == null) return '';
		        return c.DateWCF().format('d/m/Y');
		    }, amountFormatterForJqGrid: function (c, r, o) {
		        return $('<div/>', { class: 'text-right', style: 'width:100%;padding-right: 6px;', text: TssLib.indCurFlt(c) });//fcur
		    }, indCur: function (x) {
		        if (TssLib.isBlank(x) || isNaN(x))
		            return 0;
		        x = parseInt(x + '', 10);
		        x = x.toString();
		        var lastThree = x.substring(x.length - 3);
		        var otherNumbers = x.substring(0, x.length - 3);
		        if (otherNumbers != '')
		            lastThree = ',' + lastThree;
		        var retVal = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
		        return retVal; 
		    }, indCurFlt: function (x) {
		        if (TssLib.isBlank(x) || isNaN(x))
		            return 0;
		        x = parseFloat(x + '').toFixed(2).toFloat();
		        x = x.toString();
		        var afterDot = 0;
		        try {
		            afterDot = x.split('.')[1].length;
		            if (afterDot > 0) afterDot += 1;
		        } catch (e) { } 
		        var lastThree = x.substring(x.length - (3 + afterDot));
		        var otherNumbers = x.substring(0, x.length - (3 + afterDot));
		        if (otherNumbers != '')
		            lastThree = ',' + lastThree;
		        var retVal = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
		        return retVal;
		    }, bindAllEventsOnPageRender: function (parent) {
		        TssLib.animateBg(parent);
		        TssLib.inputModeBinders(parent);
		        TssLib.customMultiSelect(parent);
		        TssLib.customSelect(parent);
		        TssLib.tabsOnSelect(parent);
		        TssLib.animateSearchInHead(parent);
		    }, animateSearchInHead: function (parent) {
		        parent = $(parent);
		        $.each(parent.find('.gridSearch'), function (i, o) {
		            $(o).parent().find('.searchInput').blur(function () {
                        if($.trim($(this).val())==''){
                            $(this).removeClass('animated');
		                    $(this).stop(true, true).animate({
		                        'width': 0,
		                        'padding-left': 0
		                    });
		                }
                    });
		            $(o).click(function () {
		                var ele = $(this);
		                if(!ele.parent().find('.searchInput').hasClass('animated')){
		                    ele.parent().find('.searchInput').addClass('animated');
		                    ele.parent().find('.searchInput').stop(true, true).animate({
		                        'width': 140,
		                        'padding-left': 5
		                    }).focus();
		                }
		            });
		        });
		    }, buildDdlYear: function (years, changeEvent, type) {
		        var yrFld = $('#ddlYear');
		        $('#s2id_' + yrFld.attr('id')).show();
		        if (type == 'FY') {
		            TssLib.populateSelect(yrFld, { success: true, data: years }, 'Code', 'Id');
		            TssLib.preSelectValForSelectBox(yrFld, sessionData.FinancialYearId);
		        }
		        else if (type == 'AY') {
		            TssLib.populateSelect(yrFld, { success: true, data: years }, 'Code', 'Id');
		            TssLib.preSelectValForSelectBox(yrFld, sessionData.AcademicYearId);
		        }
		        else if (type == 'LY') {
		            TssLib.populateSelect(yrFld, { success: true, data: years }, 'Code', 'Id');
		            TssLib.preSelectValForSelectBox(yrFld, sessionData.LeadYearId);
		        }
		        yrFld.show();
		        yrFld.unbind('change');
		        if (!TssLib.isBlank(changeEvent)) {
		            yrFld.change(function (e) {
		                if (type == 'FY') {
		                    sessionData.FinancialYearId = this.value;
		                } else if (type == 'LY') {
		                    sessionData.LeadYearId = this.value;
		                }
		                localStorage.setItem("CheckLogin", JSON.stringify(sessionData));
		                changeEvent(e, this);
		            });
		        }
		    }, bindAnimateEvets: function (fld) {
		        if (fld.parent().find('.slide_input_bg').length == 0)
		            fld.parent().append($('<div/>', { class: 'slide_input_bg' }));
		        fld.focus(TssLib.focusInputSelectTextArea);
		        fld.blur(TssLib.blurInputSelectTextArea);
		    }, animateBg: function (parent) {
		        parent = $(parent);
		        $.each(parent.find('.animate-fld-bg input[type="text"]'), function (i, o) {
		            TssLib.bindAnimateEvets($(o));
		        });
		        $.each(parent.find('.animate-fld-bg input[type="checkbox"]'), function (i, o) {
		            TssLib.bindAnimateEvets($(o));
		        });
		        $.each(parent.find('.animate-fld-bg input[type="radio"]'), function (i, o) {
		            TssLib.bindAnimateEvets($(o));
		        });
		        $.each(parent.find('.animate-fld-bg select'), function (i, o) {
		            TssLib.bindAnimateEvets($(o));
		        });
		        $.each(parent.find('.animate-fld-bg textarea'), function (i, o) {
		            TssLib.bindAnimateEvets($(o));
		        });
		    }, focusInputSelectTextArea: function () {
		        $(this).parent().find('.slide_input_bg').addClass('animateBg');
		        $(this).parent().find('.slide_input_bg').height($(this).closest('.input_container').height());
		        $(this).removeClass('err-bg');
		        //$(this).closest('.form-group').find('.error-msg').remove();
		    }, blurInputSelectTextArea: function () {
		        $(this).parent().find('.slide_input_bg').removeClass('animateBg');
		    }, LOGOUT: function () {
		        $.cookie("UserName", null, { path: '/' });
		        $.cookie("UserLoggedIn", null, { path: '/' });
		        $.cookie("LoggedUserId", null, { path: '/' });
		        localStorage.setItem("CheckLogin", null);
		        localStorage.setItem("MenuData", null);
		        localStorage.setItem("MenuDataModule", null);
		    }, valueToDisplay: function (value) {
		        if (TssLib.isBlank(value)) {
		            return '';
		        } else {
		            return $.trim(value);
		        }
		    }, plotTssChart: function (data, eleChart, title, type,colors) {
		        var dataTmp = [];
		        $.each(data, function (i, o) {
		            dataTmp.push([o.key, o.value]);
		        });
		        var options = {
		            chart: {
		                type: type// 'column'
		            },
		            title: {
		                text: title
		            },
		            xAxis: {
		                type: 'category',
		                labels: {
		                    rotation: -45,
		                    y: 10,
		                    align: 'right',
		                    style: {
		                        fontSize: '13px'
		                    }
		                }
		            },
		            yAxis: {
		                min: 0,
		                title: {
		                    text: 'Count'
		                }
		            },
		            tooltip: {
		                headerFormat: '<span style="font-size:10px">{point.key} : <b>{point.y}</b></span><table>',
		                pointFormat: '',
		                footerFormat: '</table>',
		                shared: true,
		                useHTML: true
		            },
		            legend: {
		                enabled: false
		            },
		            series: [{
		                //name: 'Mail',
		                colorByPoint: true,
		                data: dataTmp
		            }]
		        };
		        if (type == 'column') {
		            options.plotOptions = {
		                series: {
		                    dataLabels: {
		                        enabled: true,
		                        format: '{point.y:,.0f}',
		                        color: (Tsscharts.theme && Tsscharts.theme.contrastTextColor) || 'black',
		                        softConnector: true
		                    },
		                    neckWidth: '20%',
		                    neckHeight: '15%'
		                }
		            };
		            options.chart.marginRight = 80;
		        } else {
		            options.plotOptions = {
		                series: {
		                    dataLabels: {
		                        enabled: true,
		                        format: '<b>{point.name}</b> ({point.y:,.0f})',
		                        color: (Tsscharts.theme && Tsscharts.theme.contrastTextColor) || 'black',
		                        softConnector: true
		                    },
		                    neckWidth: '20%',
		                    neckHeight: '15%'
		                }
		            };
		            if (!TssLib.isBlank(colors)) {
		                options.plotOptions.series.colors=colors;
                    }
		            options.chart.marginRight = 80;
		        }
		        $(eleChart).tsscharts(options);
		    }, getMultipleSelectVals: function (fld) {
		        var vals = [];
		        $.each($(fld).find('option:selected'), function (i, selected) {
		            vals.push($(selected).val());
		        });
		        return vals;
		    }, onPageHeightChange: function () { 
		    }, gradientColor: function (color) {
		        return {
		            radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
		            stops: [
                            [0, color],
                            [1, Tsscharts.Color(color).brighten(-0.4).get('rgb')] // darken
		            ]
		        };
		    }, capitaliseFirstLetter: function (string) {
		        if (TssLib.isBlank(string))
		            return '';
		        return string.charAt(0).toUpperCase() + string.slice(1);
		    }, updateCheckBoxValue: function (fld, flag) {
		        if (TssLib.isBlank(flag))
		            flag = fld.checked;
		        fld.checked = flag;
		        fld = $(fld);
		        if (fld.prev().length != 0)
		            fld.prev().val(fld[0].checked ? 1 : 0);
		    }, datepickerBinder: function (parent) {
		        parent = $(parent);
		        /*date picker start*/
		        $.each(parent.find('.tssDatepicker'), function (i, o) {
		            var format = $(o).attr('dt-format');
					var startView = $(o).attr('dt-viewmode');
					var minViewMode = $(o).attr('dt-minViewMode');
		            if (TssLib.isBlank(format))
		                format = TssConfig.DATE_FORMAT_FOR_PICKER;
		            var _options = {};
		            _options.format = format;
		            if (!TssLib.isBlank(startView))
		                _options.startView = startView;	
		            if (!TssLib.isBlank(minViewMode))
		                _options.minViewMode = minViewMode;											
		            $(o).attr('placeholder', format);
		            //Code for enddate in datepicker start
		            var enddate = $(o).attr('enddate');
		            if (!TssLib.isBlank(enddate))
		                _options.endDate = enddate;
		            //Code for enddate in datepicker end
		            //Code for startDate in datepicker start
		            var startDate = $(o).attr('startDate');
		            if (!TssLib.isBlank(startDate)) {
		                var date = new Date();
		                date.setDate(date.getDate() - startDate);
		                _options.startDate = date;
		            } else {
		                _options.startDate = '-150y';
		            }
		            //Code for startDate in datepicker end
		            //MIN MAX DATES START
		            var minDate = $(o).attr('minDate');
		            if (!TssLib.isBlank(minDate))
		                _options.startDate = minDate;
		            var maxDate = $(o).attr('maxDate');
		            if (!TssLib.isBlank(maxDate))
		                _options.endDate = maxDate;
		            //MIN MAX DATES END
		            _options.autoclose = true;
		            var onChangeEvent = $(o).attr('on-dt-change');
		            var onBlurEvent = $(o).attr('on-dt-blur');
		            if (TssLib.isBlank(onBlurEvent)) {
		                $(o).blur(function (ev) {
                            TssLib.isDatePicketDateIsValidOrNot(o); 
		                    var invalid = $(o).data('tss_invalid');
		                    if (!TssLib.isBlank(invalid) && invalid === true) {
		                        TssLib.notify('Invalid Date', 'warn');
		                        $(o).val('');
		                    }
                        });
		            } else {
		                $(o).blur(function (ev) {
		                    TssLib.isDatePicketDateIsValidOrNot(o);
		                    var invalid = $(o).data('tss_invalid');
		                    if (!TssLib.isBlank(invalid) && invalid === true) {
		                        TssLib.notify('Invalid Date', 'warn');
		                        $(o).val('');
		                    }
		                });
		            } 
		            if (TssLib.isBlank(onChangeEvent)) {
		                //showLog(_options);
		                $(o).datepicker(_options);
		                $(o).datepicker(_options).on('changeDate', function (ev) {
		                    TssLib.isDatePicketDateIsValidOrNot(o); 
		                });
		            } else {
		                //showLog(_options);
		                $(o).datepicker(_options).on('changeDate', function (ev) { 
		                    window[onChangeEvent]();
		                    TssLib.isDatePicketDateIsValidOrNot(o);
		                }); 
		            }
		            if(!TssLib.isBlank($(o).data('min')))
		                $(o).data("datepicker").setStartDate($(o).data('min'));
		            if (!TssLib.isBlank($(o).data('max')))
		                $(o).data("datepicker").setEndDate($(o).data('max'));
		        });
		        /*date picker end*/
		    }, isDatePicketDateIsValidOrNot: function (o) { 
		        var dpo = $(o).data("datepicker");
		        var date = null;
		        try {
		            var fmt = dpo.o.format.replace('yyyy', 'yy'); 
		            date = $.datepicker.parseDate(fmt, $(o).val());//$.datepicker.formatDate(dpo.o.format, dpo.getDate()));
		        } catch (e) { date = null }
                //showLog(date);
		        if (date == null || (new Date().getFullYear() - date.getFullYear()) >= 150) {//(new Date().getFullYear() - dpo.minDate().getFullYear())
		            $(o).data('tss_invalid', true);
		        } else {
		            $(o).data('tss_invalid', false);
		        }
		        if(TssLib.isBlank($(o).val()))
		            $(o).data('tss_invalid', false);
		    }, removeValueByKeyInJsonArray: function (arr, key, value) {
		        var tempList = [];
		        $.each(arr, function (i, o) {
		            if (value.indexOf(o[key]) == -1)
		                tempList.push(o);
		        });
		        return tempList;
		    }, numericBinders: function (parent) {
		        parent = $(parent);
		        parent.find(".pInt").numeric({ decimal: false, negative: false });
		        parent.find(".pnInt").numeric({ decimal: false, negative: true });
		        parent.find(".pDec").numeric({ decimal: '.', negative: false });
		        parent.find(".pnDec").numeric({ decimal: '.', negative: true });
		        onlyAphaBets(parent.find(".alphaOnly"));
		        onlyAphaNumeric(parent.find(".alphaNumericOnly"));
		    }, inputModeBinders: function (parent) {
		        parent = $(parent);
		        TssLib.numericBinders(parent[0]);
		        TssLib.datepickerBinder(parent[0]);
		    }, showResponseError: function (data) {
		        if (data.errorMessage != "")
		            TssLib.notify(data.errorMessage, 'error');
		        else
		            TssLib.notify(TssConfig.SERVER_ERROR, 'error');

		    }, docReady: function (call, ajaxCall) {
		        $(function () {
		            if (TssLib.isBlank(ajaxCall))
		                ajaxCall = [];
		            ajaxCall.push(CheckLoginAjax);
		            $.when.apply($, ajaxCall).done(function () {
		                call();
		            });
		        });
		    }, tabsOnSelect: function (parent) {
		        parent = $(parent);
		        parent.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		            //e.target // activated tab
		            //e.relatedTarget // previous tab
		            resizeGrids();
		        });
		    }, customMultiSelect: function (parent) {
		        parent = $(parent);
		        $.each(parent.find("select[multiple='multiple']"), function (i, o) {
		            try { $(o).multiselect('destroy'); } catch (e) { }
		            var canDisable = true;
		            if ($(o).find('option').length > 0) canDisable = false;
		            var caseInsensitieSearch = $(o).attr('enableCaseInsensitiveFiltering');
		            if (TssLib.isBlank(caseInsensitieSearch)) { caseInsensitieSearch = 'true'; }
		            $(o).multiselect({
		                buttonWidth: '100%', maxHeight: 200,
		                includeSelectAllOption: $(o).attr('includeSelectAllOption') === 'true' ? true : false,
		                enableFiltering: $(o).attr('enableFiltering') === 'true' ? true : false,
		                enableCaseInsensitiveFiltering: caseInsensitieSearch === 'true' ? true : false,
		                enableClickableOptGroups: $(o).attr('enableClickableOptGroups') === 'true' ? true : false,
		                allSelectedText: $(o).attr('allselectedtext') ? $(o).attr('allselectedtext') : 'All Selected',
		                buttonTitle: function (options, select) {
		                    var labels = [];
		                    options.each(function () {
		                        labels.push($(this).text());
		                    });
		                    return labels.join(', ');
		                },
		                onChange: function (element, checked) {
		                    if (checked === true) {
		                        var title = $(o).closest('div').find('.multiselect[data-toggle="dropdown"]').attr('title');
		                        $(o).closest('div').find('.multiselect[data-toggle="dropdown"]').attr('data-original-title', title);
		                        $(o).closest('div').find('.multiselect[data-toggle="dropdown"]').removeAttr('title');
		                    }
		                }
		            });
		            $(o).multiselect(canDisable ? 'disable' : 'enable');
		        });
		    }, customSelect: function (parent) {
		        // return;
		        // fld.select2("val", o);
		        parent = $(parent);
		        $.each(parent.find("select:not([ multiple='multiple'])"), function (i, o) {
		            var sel = $(o);
		            sel.css({ 'width': '100%' });
		            if (sel.hasClass('ui-pg-selbox') || sel.hasClass('no-sel2'))
		                return true;
		            var opt = {};
		            if (!TssLib.isBlank(sel.attr('enable-search')) && sel.attr('enable-search').toLowerCase() == 'false') { 
						opt.minimumResultsForSearch = -1
					}
		            try { sel.select2("destroy"); } catch (e) { }
		            if (!TssLib.isBlank(sel.attr('readonly')) && sel.attr('readonly').toLowerCase() == 'readonly') { sel.select2(opt); sel.select2("readonly", true); } else sel.select2(opt);
		        });
		    }, wizardTabs: function (selector, actions) {
		        selector = $(selector);
		        selector.find('li > a').click(function (e) {
		            e.preventDefault();
		            var current = $(this);
		            if (current.closest('li').hasClass('disabled'))
		                return false;
		            else {
		                var index = current.parent().index();
		                if (index == current.parent().parent().find('li').length - 1) {
		                    $('*[save-continue-button="true"]').hide();
		                } else {
		                    $('*[save-continue-button="true"]').show();
		                }
		                return true;
		            }
		        });
		        $('*[save-continue-button="true"]').click(function (e) {
		            e.preventDefault();
		            TssLib.tabWizardSaveClick(e, selector, true, actions);
		        });
		        $('*[save-button="true"]').click(function (e) {
		            e.preventDefault();
		            TssLib.tabWizardSaveClick(e, selector, false, actions);
		        });
		    }, tabWizardSaveClick: function (e, selector, forward, actions) {
		        var index = selector.find('li.active').index();
		        var tabContent = $(selector.find('li.active').find('a').attr('href'));
		        var done = true;
		        var message = "Save Success";
		        if (actions[index]) {
		            var ret = actions[index](tabContent);
		            done = ret[0];
		            message = ret[1];
		        }
		        if (done) {
		            TssLib.notify(message, 'success');
		            if (forward) {
		                selector.find('li.disabled').removeClass('disabled');
		                selector.find('li:eq(' + (index + 1) + ')').find('a').trigger('click');
		            }
		        } else {
		            TssLib.notify(message, 'error');
		        }
		    }, removeAjaxLoader: function (cur) {
		        try { cur.loader.remove(); } catch (e) { }
		    }, selectAllOptions: function (fld) {
		        var vals = [];
		        $.each($(fld).find('option'), function () {
		            vals.push($(this).val());
		        });
		        $(fld).multiselect('select', vals);
		    }, preselectDependentsLess: function (options) {
		        var temp = [];
		        $.each(options, function (i, o) {
		            temp.push({ selector: o[0], dataToSend: o[1] });
		        });
		        TssLib.preselectDependents(temp);
		    }, preselectDependentsLessAsync: function (options) {
		        var temp = [];
		        $.each(options, function (i, o) {
		            var otherOpt = {};
		            try { otherOpt = o[2]; if (TssLib.isBlank(otherOpt)) { otherOpt = {}; } } catch (e) { }
		            temp.push($.extend({ selector: o[0], dataToSend: o[1] }, otherOpt));
		        });
		        TssLib.preselectDependentsAsync(temp);
		    }, preselectDependentsAsync: function (options) {
		        TssLib.preselectDependentsRecurtion(options, 0, null);
		    }, preselectDependentsRecurtion: function (options, i, oldOption) {
		        var option = options[i];
		        if (i != 0) {
		            var ele = $(option.selector);
		            var url = TssLib.isBlank(ele.attr('pop-url')) ? option.url : ele.attr('pop-url');
		            url = (!TssLib.isBlank(ele.attr('append-base-url')) && ele.attr('append-base-url') == 'false') ? url : TssConfig.BASE_URL + url;
		            //New fix if select value is null start
		            var _selVal = null;
		            if (!TssLib.isBlank(oldOption.keyAppendToUrl) && !oldOption.keyAppendToUrl) {
		                if (!TssLib.isBlank(oldOption.isArray) && oldOption.isArray) {
		                    _selVal = oldOption.dataToSend;
		                } else {
		                    _selVal = oldOption.dataToSend[oldOption.dataKey];
		                }
		            } else {
		                _selVal = oldOption.dataToSend;
		            }
		            if (_selVal == null)
		                _selVal = "";
		            //New fix if select value is null end

		            var jsonToSend = {};
		            if (!TssLib.isBlank(oldOption.keyAppendToUrl) && !oldOption.keyAppendToUrl) {
		                jsonToSend = oldOption.dataToSend;
		            } else {
		                if (TssLib.isBlank(option.urlAppenders))
		                    url += oldOption.dataToSend;
		                else {
		                    var tempArr = [];
		                    $.each(option.urlAppenders, function (k, j) {
		                        if (TssLib.isBlank(j))
		                            tempArr.push("0");
		                        else
		                            tempArr.push(j);
		                    });
		                    option.urlAppenders = tempArr;
		                    url += option.urlAppenders.join('/') +
                                (option.urlAppenders.length > 0 ? ((TssLib.isBlank(option.noLastSlash) || !option.noLastSlash) ? '/' : '') : '');
		                }
		            }
		            var showLoader = TssLib.isBlank($(ele).attr('show-loader')) ? true : $(ele).attr('show-loader');
		            if ($.trim(_selVal) == "" && TssLib.isBlank(option.urlAppenders)) {
		                TssLib.populateSelect(ele, {});
		            } else {

		                var _extOpt = {};
		                if (!TssLib.isBlank(oldOption.method)) { _extOpt.method = oldOption.method; }
		                if (!TssLib.isBlank(oldOption.jsonContent)) { _extOpt.jsonContent = oldOption.jsonContent; }
		                getJsonAsync(url, jsonToSend,
                                    $.extend(_extOpt
		                            , {
		                                showLoader: showLoader, callback: function (data) {
		                                    TssLib.populateSelect(ele, data, option.optTextKey, option.optValKey, option.optDefText, option.optDefVal);
		                                    var selVal = null;
		                                    if (!TssLib.isBlank(option.keyAppendToUrl) && !option.keyAppendToUrl) {
		                                        if (!TssLib.isBlank(option.isArray) && option.isArray) {
		                                            selVal = option.dataToSend;
		                                        } else {
		                                            selVal = option.dataToSend[option.dataKey];
		                                        }
		                                    } else {
		                                        selVal = option.dataToSend;
		                                    }
		                                    if (_selVal == null)
		                                        _selVal = "";

		                                    if (ele.attr('multiple') == 'multiple') {
		                                        ele.multiselect('select', selVal);
		                                    } else {
		                                        TssLib.preSelectValForSelectBox(ele, selVal);
		                                    }

		                                    if (!TssLib.isBlank(option.trigger)) {
		                                        ele.trigger(option.trigger);
		                                    }
		                                    oldOption = option;
		                                    if (i + 1 != options.length) {
		                                        i = i + 1;
		                                        TssLib.preselectDependentsRecurtion(options, i++, oldOption);
		                                    }
		                                    if (option.callback) {
		                                        option.callback(ele);
		                                    }
		                                }
		                            })
                                            );
		            }
		        } else {
		            var ele = $(option.selector);
		            var selVal = null;
		            if (!TssLib.isBlank(option.keyAppendToUrl) && !option.keyAppendToUrl) {
		                if (!TssLib.isBlank(option.isArray) && option.isArray) {
		                    selVal = option.dataToSend;
		                } else {
		                    selVal = option.dataToSend[option.dataKey];
		                }
		            } else {
		                selVal = option.dataToSend;
		            }
		            if (ele.attr('multiple') == 'multiple') {
		                ele.multiselect('select', selVal);
		            } else {
		                TssLib.preSelectValForSelectBox(ele, selVal);
		            }
		            if (!TssLib.isBlank(option.trigger))
		                ele.trigger(option.trigger);
		            oldOption = option;
		            if (i + 1 != options.length) {
		                i = i + 1;
		                TssLib.preselectDependentsRecurtion(options, i++, oldOption);
		            }
		        }
		    }, clearSelect: function (selectors) {
		        if (TssLib.isBlank(selectors)) selectors = [];
		        if (!$.isArray(selectors))
		            selectors = [selectors];

		        $.each(selectors, function (i, o) {
		            TssLib.populateSelect(o, { success: true, data: [] });
		        });
		    }, preselectDependents: function (options) {
		        /*
                options=[]
                {url:'serviceUrl',selector:'#eleId or .eleId or $('#el')[0]',dataToSend:{id:1},dataKey:'id',keyAppendToUrl:false,
                optValKey:'',optTextKey:'',optDefVal:'',optDefText:''}
                */
		        var oldOption = null;
		        $.each(options, function (i, o) {
		            var option = options[i];
		            if (i != 0) {
		                var ele = $(option.selector);
		                var url = TssLib.isBlank(ele.attr('pop-url')) ? option.url : ele.attr('pop-url');
		                url = (!TssLib.isBlank(ele.attr('append-base-url')) && ele.attr('append-base-url') == 'false') ? url : TssConfig.BASE_URL + url;
		                var jsonToSend = {};
		                if (!TssLib.isBlank(oldOption.keyAppendToUrl) && !oldOption.keyAppendToUrl) {
		                    jsonToSend = oldOption.dataToSend;
		                } else {
		                    if (TssLib.isBlank(option.urlAppenders))
		                        url += oldOption.dataToSend;
		                    else {
		                        var tempArr = [];
		                        $.each(option.urlAppenders, function (k, j) {
		                            if (TssLib.isBlank(j))
		                                tempArr.push("0");
		                            else
		                                tempArr.push(j);
		                        });
		                        option.urlAppenders = tempArr;
		                        url += option.urlAppenders.join('/')
                                    (option.urlAppenders.length > 0 ? ((TssLib.isBlank(option.noLastSlash) || !option.noLastSlash) ? '/' : '') : '');
		                    }
		                }
		                var showLoader = TssLib.isBlank($(ele).attr('show-loader')) ? true : $(ele).attr('show-loader');
		                TssLib.populateSelect(ele,
                            getJson(url, jsonToSend, { showLoader: showLoader })
                            , option.optTextKey, option.optValKey, option.optDefText, option.optDefVal);
		            }
		            var selVal = null;
		            if (!TssLib.isBlank(option.keyAppendToUrl) && !option.keyAppendToUrl) {
		                selVal = option.dataToSend[option.dataKey];
		            } else {
		                selVal = option.dataToSend;
		            }
		            TssLib.preSelectValForSelectBox($(option.selector), selVal);
		            oldOption = option;
		        });
		    }, preSelectValForSelectBox: function (fld, o) {
		        if (fld.attr('multiple') === 'multiple') {
                    if(TssLib.isBlank(o))
                        fld.multiselect('clearSelection');
                    else
		                fld.multiselect('select', o);
		        } else {
		            var val = fld.find('option:containsIgnoreCaseExactMatch(' + o + ')').val();
		            if (val != undefined && val != '') {
		                fld.val(val);
		                fld.select2("val", val);
		            } else {
		                fld.val(o);
		                fld.select2("val", o); 
		            }
		        }
		    }, QueryString: function () {
		        // This function is anonymous, is executed immediately and
		        // the return value is assigned to QueryString!
		        var query_string = {
		        };
		        var query = window.location.search.substring(1);
		        var vars = query.split("&");
		        for (var i = 0; i < vars.length; i++) {
		            var pair = vars[i].split("=");
		            // If first entry with this name
		            if (typeof query_string[pair[0]] === "undefined") {
		                query_string[pair[0]] = pair[1];
		                // If second entry with this name
		            } else if (typeof query_string[pair[0]] === "string") {
		                var arr = [query_string[pair[0]], pair[1]];
		                query_string[pair[0]] = arr;
		                // If third or later entry with this name
		            } else {
		                query_string[pair[0]].push(pair[1]);
		            }
		        }
		        return query_string;
		    }, updateDatePicker: function (selector) {
		        selector = $(selector);
		        $.each(selector.find('.tssDatepicker'), function (i, o) {
		            $(o).datepicker('update');
		        });
		    },
		    renderData: function (selector, rowData) {
		        selector = $(selector);//'#someId','.someClass', jsElement
		        $.each(rowData, function (i, o) {
		            var fld = selector.find('*[name="' + i + '"]');
		            try {
		                if (fld[0].nodeName == 'SELECT') {
		                    TssLib.preSelectValForSelectBox(fld, o);
		                } else if (fld[0].nodeName == 'INPUT' || fld[0].nodeName == 'TEXTAREA') {
		                    o = TssLib.maskValueForRadioAndCheckBoxRendering(fld, o);
		                    fld.val(o);
		                } else {
		                    fld.html(o);
		                    if (fld.is('[data-original-title]') || fld.is('[title]')) {
		                        fld.attr('title', o);
		                        fld.attr('data-original-title', o);
		                    }
		                        
						}
		            } catch (ex) { /*showLog(ex); showLog(i+'--'+o);*/ }
		        });
		        TssLib.updateDatePicker(selector);
		        $.each(selector.find('*[place-key-data="true"]'), function (i, o) {
		            $(o).prev().attr('key-data', o.value);
		        });

		    },
		    maskValueForRadioAndCheckBoxRendering: function (fld, value) {
		        var checkFld = null;
		        if ($.isArray(fld[0])) checkFld = fld[0][0]; else checkFld = fld[0];
		        if (checkFld.type == undefined)
		            checkFld.type = 'text';
		        if (checkFld.nodeName == 'INPUT' && (checkFld.type.toLowerCase() == 'checkbox' || checkFld.type.toLowerCase() == 'radio'))
		            if (!$.isArray(value)) value = [value];
		        return value;
		    },
		    notify: function (message, type, duration) {//        TssLib.notify('Hi','info');//warn,success,error,info
		        var existingNotifications = $('article.alertify-log');
		        var flg =true;
		        $.each(existingNotifications, function() {
		            if ($(this).text() == message) {
		                flg = false;
		                return flg;
                    }
		        });
		        if (!flg)
		            return;
		        if (TssLib.isBlank(duration)) {
		            duration = TssConfig.NOTIFY_TIMER;
		        }
		        message = message + '<div class="timer-wrapper"><div class="pie spinner"></div><div class="pie filler"></div><div class="mask"></div></div>'
		        if (TssLib.isBlank(type)) type = 'success';
		        alertify.log(message, type, duration * 1000);
		        $('.filler').css({
		            'animation': 'fill ' + duration + 's steps(1, end) infinite'
		    });
		        $('.spinner').css({
		            'animation': 'rota ' + duration + 's linear infinite'
		    });
		        $('.mask').css({
		        'animation': 'mask ' + duration + 's steps(1, end) infinite'
		    })
		    },
		    confirm: function (title, message, callback, successBtnTitle, cancelBtnTitle, cancelCallBack) {
		        if (TssLib.isBlank(title))
		            title = 'TSS Tracker';
		        var selector = $('#oasis-confirm');
		        selector.find('.panel-title').html(title);
		        selector.find('.panel-body>p').html(message);
		        if (!TssLib.isBlank(successBtnTitle)) {
		            selector.find('.panel-footer>.btn-danger').html(successBtnTitle);
		        }
		        if (!TssLib.isBlank(cancelBtnTitle)) {
		            selector.find('.panel-footer>.btn-default').html(cancelBtnTitle);
		        }
		        selector.find('.panel-footer>.btn-danger').unbind('click');
		        selector.find('.panel-footer>.btn-danger').bind('click', callback);
		        if (!TssLib.isBlank(cancelCallBack)) {
		            selector.find('.panel-footer>.btn-default').unbind('click');
		            selector.find('.panel-footer>.btn-default').bind('click', cancelCallBack);
		        }
		        $(selector).modal({});
		    }, closeConfirmModal: function (flg) {
		        if (TssLib.isBlank(flg)) flg = false;
		        TssLib.closeModal(flg, '#oasis-confirm');
				$('body').removeClass('modal-open');
		    }, closeModal: function (flg, selector) {
		        if (TssLib.isBlank(selector)) {
		            selector = $('#oak_popup');
		        } else {
		            selector = $(selector);
		        }
		        if (TssLib.isBlank(flg)) {
		            flg = false;
		        }
		        if (flg) {
		            selector.hide();
		            selector.removeClass('in');
		            selector.data('bs.modal', null);
		            $('body').find('div.modal-backdrop').remove();
		        } else {
		            selector.modal('hide');
		        }
				$('body').removeClass('blur');
		    }, openModel: function (options, data, selector) {
		        if (TssLib.isBlank(selector)) {
		            selector = '#oak_popup';
		        }
		        var shownCallBack = function (e) {
		            if (options.callback) {
		                options.callback(e);
		            }
		            $(e.currentTarget).find('.multiselect.dropdown-toggle.btn').parent().remove();
		            TssLib.customSelect(e.currentTarget);
		            TssLib.customMultiSelect(e.currentTarget);
		            resizeGrids();
		            $.each($(selector).find('input:not(.noFocus):not(.readonly)'), function () {
		                var current = $(this);
		                if (current.is(':visible')) {
		                    current.focus();
		                    return false;
		                }
		            });
		            setTimeout(function () { $(selector).find('input:first:not(.noFocus)').focus(); }, 1000);
		            //$.each($(selector).find('input'), function () {
		            //    var current = $(this);
		            //    if (current.is(':visible')) {
		            //        current.focus();
		            //        return false;
		            //    }
		            //});
		            //$(selector).find('input:first').focus();
					//$('body').addClass('blur');
		            //$(selector).find('.model-mg-width').css('top', '50%').css('margin-top', -($(selector).find('.model-mg-width').height()/3));
		        }; 
		        if (TssLib.isBlank(options)) { options = {}; }
		        if (TssLib.isBlank(options.width)) { options.width = 600; }
		        $(selector).find('.model-mg-width').css('width', options.width);
		        $(selector).find('.model-mg-content').html(data);
		        $(selector).on('shown.bs.modal', shownCallBack).modal(options);
		        $(selector).find('.multiselect.dropdown-toggle.btn').parent().remove();
		        TssValidation.removeValidations(selector);
		        $(selector).find('.select2-container').remove();
		        TssLib.inputModeBinders(selector);
		        TssLib.customSelect(selector);
		        TssLib.tabsOnSelect(selector);
		        TssLib.animateBg(selector);
		        return $(selector);
		    },
		    parseJSON: function (value) {
		        try {
		            return $.parseJSON(value);
		        } catch (e) {
		            return [];
		        }
		    },
		    htmlDecodeForJson: function (value) {
		        var _val = '';
		        if (value && (value === '&nbsp;' || value === '&#160;' || (value.length === 1 && value.charCodeAt(0) === 160))) {
		            return "";
		        }
		        _val = !value ? value : String(value).replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&amp;/g, "&");
		        return TssLib.parseJSON(_val);
		    },
		    htmlDecode: function (value) {
		        if (value && (value === '&nbsp;' || value === '&#160;' || (value.length === 1 && value.charCodeAt(0) === 160))) {
		            return "";
		        }
		        return !value ? value : String(value).replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&amp;/g, "&");
		    },
		    htmlEncode: function (value) {
		        return !value ? value : String(value).replace(/&/g, "&amp;").replace(/\"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
		    },
		    populateSelect: function (selector, collection, textKey, idKey, defaultText, defaultValue) {
		        if (collection.success == false) {
		            collection = [];
		        }
		        var showLoader = TssLib.isBlank($(selector).attr('show-loader')) ? true : $(selector).attr('show-loader');
		        if (TssLib.isBlank(defaultText)) {
		            defaultText = 'Select';
		        }
		        if (TssLib.isBlank(defaultValue)) {
		            defaultValue = '';
		        }
		        if (collection == undefined || collection == null) {
		            collection = [];
		        } else {
		            if ((typeof collection) === 'string') {
		                /*
                        getJsonAsyncWithBaseUrl(collection, null, {
                            showLoader: showLoader, callback: function (resultData) {
                                TssLib.populateSelect(selector, resultData, textKey, idKey, defaultText, defaultValue);
                            }
                        });
                        collection = [];
                        */
		                collection = getJsonWithBaseUrl(collection, null, { showLoader: showLoader });
		            }
		            /*else {*/
		            if (collection.data == undefined || collection.data == null)
		                collection = [];
		            else {
		                if (collection.data.InfoList == undefined || collection.data.InfoList == null)
		                    collection = collection.data;
		                else
		                    collection = collection.data.InfoList;
		            }
		            /* }*/
		        }
		        if (!$.isArray(selector)) {
		            selector = [selector];
		        }
		        $.each(selector, function (k, l) {
		            l = $(l);
		            idKey = TssLib.isBlank(l.attr('opt-val-key')) ? idKey : l.attr('opt-val-key');
		            textKey = TssLib.isBlank(l.attr('opt-text-key')) ? textKey : l.attr('opt-text-key');
		            defaultValue = TssLib.isBlank(l.attr('opt-def-val')) ? defaultValue : l.attr('opt-def-val');
		            defaultText = TssLib.isBlank(l.attr('opt-def-text')) ? defaultText : l.attr('opt-def-text');
		            l.html('');
		            var _option = null;
		            //if ((!TssLib.isBlank(collection) && collection.length > 1) || (TssLib.isBlank(collection) || collection.length ==0)) {
		            if (TssLib.isBlank(l.attr('no-select')))
		                _option = $('<option/>', { value: defaultValue, text: defaultText });
		            //}
		            if (l.attr('multiple') === 'multiple') { } else l.append(_option);
		            if (l.attr('need-group-options') === 'true') {
		                $.each(collection, function (i, o) {
		                    var _id = o[l.attr('group-id-key')];
		                    var optgroup = $('<optgroup  />', { label: o[l.attr('group-options-lable-key')] });
		                    $.each(o[l.attr('group-childs-key')], function (i1, o1) {
		                        var _gopVal = o1[l.attr('group-option-value-key')];
		                        if (l.attr('group-option-value-key-hash') === 'true')
		                            _gopVal = _id + '#' + _gopVal;
		                        optgroup.append($('<option/>', { value: _gopVal, text: o1[l.attr('group-option-text-key')] }));
		                    });
		                    l.append(optgroup);
		                });
		            } else {
		                $.each(collection, function (i, o) {
		                    if (o.Item == undefined || o.Item == null) {
		                        _option = $('<option/>', { value: o[idKey], text: o[textKey] });
		                    } else {
		                        _option = $('<option/>', { value: o.Item[idKey], text: o.Item[textKey] });
		                    }
		                    _option.data('data', o);
		                    l.append(_option);
		                });
		            }

		            if (l.attr('multiple') === 'multiple') {
		                TssLib.customMultiSelect(l.parent()[0]);
		            }
		            l.trigger('change');
		        });

		    },
		    loadedJs: [],
		    loadedCss: [],
		    validateForm: function (form, byPassValidations) {
		        if (byPassValidations == undefined || byPassValidations == null || byPassValidations == false)
		            byPassValidations = false;
		        form.submit(function (e) {
		            var ret = {
		                success: true
		            };
		            if (!byPassValidations)
		                ret = TssValidation.validate(form[0]);
		            if (ret.success) {
		                //e.preventDefault();
		            } else {
		                e.preventDefault();
		            }
		        });
		    }, bundleFormFields: function (formObj, data, groupListName, options) {
		        /*Group Submit Start options.skipFieldsOfElements */
		        var groups = {};
		        var attrMissing = false;
		        $.each(formObj.find('*[group-submit="true"]'), function (i, o) {
		            var grName = $(o).attr('group-submit-name');
		            if (!TssLib.isBlank(grName)) {
		                if (TssLib.isBlank(groups[grName])) {
		                    groups[grName] = [];
		                }
		                groups[grName].push($(o).serializeObject());
		            } else {
		                attrMissing = true;
		            }
		        });
		        if (attrMissing)
		            alert('Name Attribute Missing');
		        $.each(groups, function (i, o) {
		            if (o.length > 0)
		                $.each(o, function (k, l) {
		                    $.each(l, function (k1, l1) {
		                        delete data[k1];
		                    });
		                });
		            data[i] = o;
		        });
		        /*Group Submit End options.skipFieldsOfElements */
		        return data;
		    }, ajaxForm: function (options) {
		        //{form:ele,byPassValidations:false,callback:function(){},before:function(formObj){}}
		        var options = $.extend({
		            byPassValidations: false,
		            doServiceCall: true,
		            skipFieldsOfElements: [],
		            jsonContent: false,
		            InternalRequest:false,
		            before: function (formObj, options) {
		                $(formObj).attr('action-send', $(formObj).attr('action'));
		                return true;
		            }, beforeSend: function (formObj, data, groupListName, options) {
		                return TssLib.bundleFormFields(formObj, data, groupListName, options);
		            }
		        }, options);
		        if (options.form == undefined || options.form == null || options.form.length == 0)
		            return false;
		        else {
		            if (options.form[0].nodeName != 'FORM') {
		                options.form.find('*[div-submit="true"]').click(function (e) {
		                    TssLib.formSubmit(e, options)
		                });
		            }
		            options.method = options.form.attr('method');
		            if (!TssLib.isBlank(options.method)) {
		                options.method = options.method.toLowerCase();
		            } else {
		                options.method = 'get';
		            }
		            options.form.submit(function (e) {
		                TssLib.formSubmit(e, options)
		            });
		            return true;
		        }
		    }, formSubmit: function (e, options) {
		        var curObj = options.form;
		        var beforeCall = false;
		        if (!TssLib.isBlank(options.before)) {
		            beforeCall = options.before(curObj, options);
		            if (!beforeCall) {
		                e.preventDefault();
		                return;
		            }
		        }
		        var ret = null;
		        if (options.byPassValidations) {
		            ret = {
		                success: true
		            };
		        } else {
		            ret = TssValidation.validate(curObj, options.skipFieldsOfElements);
		        }
		        if (ret.success) {
		            try {
		                var data = $(curObj).serializeObject();
		                var resultData = null;
		                var url = $(curObj).attr('action-send');
		                var additionalData = options.form.data('additionalData');
		                if (additionalData != undefined && additionalData != null && additionalData != '') {
		                    data = $.extend(data, additionalData);
		                }

		                var beforeSendAjax = curObj.attr('before-send-ajax');
		                if (TssLib.isBlank(beforeSendAjax) || beforeSendAjax != 'true') {
		                    beforeSendAjax = false;
		                } else {
		                    beforeSendAjax = true;
		                }
		                if (options.beforeSendAjax && beforeSendAjax) {
		                    options.beforeSendAjax(curObj);
		                    e.preventDefault();
		                    return;
		                }
		                url = (!TssLib.isBlank(options.appendBaseUrl) && options.appendBaseUrl) ? (TssConfig.BASE_URL + url) : url;
		                if (options.doServiceCall) {
		                    if (options.method == 'post')
		                        resultData = postJsonAsync(url, options.beforeSend(curObj, data, options.groupListName, options), { maintainSessionOnRest: options.maintainSessionOnRest, jsonContent: options.jsonContent, callback: options.callback, InternalRequest: options.InternalRequest });
		                    else
		                        resultData = getJsonAsync(url, options.beforeSend(curObj, data, options.groupListName, options), { maintainSessionOnRest: options.maintainSessionOnRest, jsonContent: options.jsonContent, callback: options.callback, InternalRequest: options.InternalRequest });
		                } else {
		                    resultData = options.beforeSend(curObj, data, options.groupListName, options);
		                    //resultData.sent = $.extend({},data);
		                    if (!TssLib.isBlank(options.callback)) {
		                        options.callback(resultData);
		                    }
		                }
		            } catch (ex) {
		                showLog(ex);
		            }
		        } else {
		        }
		        e.preventDefault();
		    }, processOptionAndData: function (data, options) {
		        var defaultOptions = {
		        };
		        if (options != undefined && options != null) {
		            if (options.jsonContent === true) {
		                defaultOptions.contentType = 'application/json;charset=utf-8';
		                if ((typeof data) != 'string') {
		                    data = JSON.stringify(data);
		                }
		            }
		            if (options.cache != undefined && options.cache != null)
		                defaultOptions.cache = options.cache;
		        }
		        if (TssLib.isBlank(options)) options = {
		        };
		        defaultOptions = $.extend(defaultOptions, options);
		        return [defaultOptions, data];
		    }, checkObjectWithKeyAndValueExistOrNot: function (arrayObj, key, val) {
		        var retObj = null;
		        $.each(arrayObj, function (i, o) {
		            if (o[key] == val) {
		                retObj = o;
		                return false;
		            }
		        });
		        return retObj;
		    }, getJson: function (url, data, options) {
		        return TssLib.commonAjax(url, data, options, 'getJson');
		    }, postJson: function (url, data, options) {
		        return TssLib.commonAjax(url, data, options, 'postJson');
		    }, getPage: function (url, data, options) {
		        return TssLib.commonAjax(url, data, options, 'getPage');
		    }, postPage: function (url, data, options) {
		        return TssLib.commonAjax(url, data, options, 'postPage');
		    }, commonAjax: function (url, data, options, mode) {
		        var modeOptionData = {};
		        if (mode === 'postPage') {
		            modeOptionData = TssConfig.POST_PAGE_OPTIONS;
		        } else if (mode === 'getPage') {
		            modeOptionData = TssConfig.GET_PAGE_OPTIONS;
		        } else if (mode === 'getJson') {
		            modeOptionData = TssConfig.GET_JSON_OPTIONS;
		        } else if (mode === 'postJson') {
		            modeOptionData = TssConfig.POST_JSON_OPTIONS;
		        }
		        modeOptionData = $.extend({}, modeOptionData);
		        var returnData = {};
		        data = (data === undefined) ? {} : data;
		        returnData.sent = data;
		        var processData = TssLib.processOptionAndData(data, options);
		        var ajaxOptions = $.extend(modeOptionData, processData[0], {
		            url: url, data: processData[1], success: function (data) {
		                returnData.success = true;
		                returnData.data = data;
		                TssLib.removeAjaxLoader(this);
		            }, error: function (xhr, status, errorThrown) {
		                returnData.success = false;
		                returnData.data = { xhr: xhr, status: status, errorThrown: errorThrown };
		                returnData.errorMessage = '';
		                if (TssLib.isBlank(returnData.errorMessage) && typeof errorThrown == 'string') {
		                    returnData.errorMessage = errorThrown;
		                } else {
		                    returnData.errorMessage = xhr.statusText;
		                    returnData.data.errorThrown = xhr.statusText;
		                }
		                if (!(ajaxOptions.dontShowErrorMessage === true) && !TssLib.isBlank(returnData.errorMessage) && returnData.errorMessage != 'OK') {
		                    TssLib.notify(returnData.errorMessage, 'error');
		                }
		                TssLib.removeAjaxLoader(this);
		            }
		        });
		        ajaxOptions.async = false;
		        //*
		        if (TssLib.isBlank(ajaxOptions.maintainSessionOnRest) || ajaxOptions.maintainSessionOnRest === true) {
		            ajaxOptions.xhrFields = {
		                withCredentials: true
		            };
		            ajaxOptions.crossDomain = true;
		        }
		        //*/
		        //showLog(ajaxOptions);
		        $.ajax(ajaxOptions);
		        return returnData;
		    }, getJsonAsync: function (url, data, options) {
		        return TssLib.commonAjaxAsync(url, data, options, 'getJson');
		    }, postJsonAsync: function (url, data, options) {
		        TssLib.commonAjaxAsync(url, data, options, 'postJson');
		    }, getPageAsync: function (url, data, options) {
		        TssLib.commonAjaxAsync(url, data, options, 'getPage');
		    }, postPageAsync: function (url, data, options) {
		        TssLib.commonAjaxAsync(url, data, options, 'postPage');
		    }, commonAjaxAsync: function (url, data, options, mode) {
		        var modeOptionData = {
		        };
		        if (mode === 'postPage') {
		            modeOptionData = TssConfig.POST_PAGE_OPTIONS;
		        } else if (mode === 'getPage') {
		            modeOptionData = TssConfig.GET_PAGE_OPTIONS;
		        } else if (mode === 'getJson') {
		            modeOptionData = TssConfig.GET_JSON_OPTIONS;
		        } else if (mode === 'postJson') {
		            modeOptionData = TssConfig.POST_JSON_OPTIONS;
		        }
		        modeOptionData = $.extend({}, modeOptionData);
		        var returnData = {
		        };
		        data = (data === undefined) ? {} : data;
		        returnData.sent = data;
		        var processData = TssLib.processOptionAndData(data, options);
		        var ajaxOptions = $.extend(modeOptionData, processData[0], {
		            url: url, data: processData[1], success: function (data) {
		                returnData.success = true;
		                returnData.data = data;
		                TssLib.removeAjaxLoader(this);
		                if (options != undefined && options != null && options.callback != undefined && options.callback != null)
		                    options.callback(returnData);
		            }, error: function (xhr, status, errorThrown) {
		                returnData.success = false;
		                returnData.data = { xhr: xhr, status: status, errorThrown: errorThrown };
		                returnData.errorMessage = '';
		                if (TssLib.isBlank(returnData.errorMessage) && typeof errorThrown == 'string') {
		                    returnData.errorMessage = errorThrown;
		                } else {
		                    returnData.errorMessage = xhr.statusText;
		                    returnData.data.errorThrown = xhr.statusText;
		                }
		                if (!(ajaxOptions.dontShowErrorMessage === true) && !TssLib.isBlank(returnData.errorMessage) && returnData.errorMessage != 'OK') {
		                    if (xhr.status == 453 || xhr.status == 452) {
		                        //453::Another Machine
		                        //452::Not Logged In
		                        $.cookie("status_msg", returnData.errorMessage, { path: '/' });
		                        TssLib.LOGOUT();
		                        //TssLib.notify(returnData.errorMessage, 'error');
		                        window.location = TssConfig.SITE_URL + "login";
		                    } else if (xhr.status == 405) {
		                        window.location = TssConfig.SITE_URL + "common/pagenotallowed";
		                    } else {
		                        TssLib.notify(returnData.errorMessage, 'error');
		                    }
		                }
		                TssLib.removeAjaxLoader(this);
		                if (options != undefined && options != null && options.callback != undefined && options.callback != null)
		                    options.callback(returnData);
		            }
		        });
		        ajaxOptions.async = true;
		        //*
		        if (TssLib.isBlank(ajaxOptions.maintainSessionOnRest) || ajaxOptions.maintainSessionOnRest === true) {
		            ajaxOptions.xhrFields = {
		                withCredentials: true
		            };
		            ajaxOptions.crossDomain = true;
		        }
		        //*/
		        //showLog(ajaxOptions);
		        return $.ajax(ajaxOptions);
		    }, isBlank: function (ele) {
		        if (typeof (ele) != 'object' && (ele === undefined || ele === null || $.trim(ele + '') === ''))
		            return true;
		        else {
		            if (ele == null || ($.isArray(ele) && ele.length == 0))
		                return true;
		            return false;
		        }
		    }, removeWhiteSpaces: function (s) {
		        return s.replace(/ /g, '');
		    }, containsArray: function (arr, ele) {
		        var found = false;
		        for (var i = 0; i < arr.length; i++)
		            if (arr[i] === ele) {
		                found = true;
		                break;
		            }
		        return found;
		    }, loadJs: function (jsUrl, reload) {
		        var found = TssLib.containsArray(TssLib.loadedJs, jsUrl);
		        var returnFlag = false;
		        if (found == false) {
		            $.ajax($.extend({}, TssConfig.GET_SCRIPT_OPTIONS, { url: jsUrl })).done(function (script, textStatus) {
		                returnFlag = true;
		            });
		            TssLib.loadedJs.push(jsUrl);
		        } else {
		            if (reload === true)
		                $.ajax($.extend({}, TssConfig.GET_SCRIPT_OPTIONS, { url: jsUrl })).done(function (script, textStatus) {
		                    returnFlag = true;
		                });
		            else
		                returnFlag = true;
		        }
		        return returnFlag;
		    }, loadCss: function (cssUrl, reload) {
		        var found = TssLib.containsArray(TssLib.loadedCss, cssUrl);
		        if (found == false) {
		            $("head").append($('<link/>', {
		                rel: 'stylesheet', type: 'text/css', href: cssUrl
		            }));
		            TssLib.loadedCss.push(cssUrl);
		        } else {
		            if (reload === true)
		                $("head").append($('<link/>', {
		                    rel: 'stylesheet', type: 'text/css', href: cssUrl
		                }));
		        }
		        return true;
		    }, getUrlParameter: function (parameterName) {
		        var sPageURL = window.location.search.substring(1);
		        var sURLVariables = sPageURL.split('&');
		        for (var i = 0; i < sURLVariables.length; i++) {
		            var sParameterName = sURLVariables[i].split('=');
		            if (sParameterName[0] == parameterName) {
		                return sParameterName[1];
		            }
		        }
		    }, getUrlParameterByUrl: function (parameterName, sPageURL) {
		        try {
		            sPageURL = sPageURL.split('?')[1];
		        } catch (e) { }
		        var sURLVariables = sPageURL.split('&');
		        for (var i = 0; i < sURLVariables.length; i++) {
		            var sParameterName = sURLVariables[i].split('=');
		            if (sParameterName[0] == parameterName) {
		                return sParameterName[1];
		            }
		        }
		    }, sortJsonArray: function (arr, key, mode) {
		        if (TssLib.isBlank(mode) || mode)
		            return arr.sort(function (a, b) { return a[key] > b[key]; });
		        else
		            return arr.sort(function (a, b) { return a[key] < b[key]; });
		    }, toWcfDateByDate: function (dt) {
		        dt = new Date(Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds(), dt.getMilliseconds()));
		        return dt.toMSJSON();
		    }, toWcfDateByField: function (fld) {
		        var dt = fld.datepicker('getDate');
		        dt = new Date(Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds(), dt.getMilliseconds()));
		        return dt.toMSJSON();
		    }, buildTssOasisTabs: function (element, context, array, options) {
		        element = $(element);
		        if (TssLib.isBlank(array) || array.length == 0) {
		            element.css('padding', '0 15px').css('background-color', 'transparent');
		            var isBlank = TssLib.isBlank;
		            var _divIn = $('<div/>', { class: 'show-mapped-subject col-sm-12 top5',style:'background-color:#fff' });
		            var _div = $('<div/>', { class: 'col-sm-12  padding0' }).append(_divIn);
		            element.empty().append(_div);
		            if (isBlank(options.NoDataContent) && isBlank(options.NoDataFormatter)) {
		                _divIn.append('No data to display.');
		            } else {
		                if (!isBlank(options.NoDataFormatter)) {
		                    _divIn.append(options.NoDataFormatter);
		                }else{
		                    _divIn.append(options.NoDataContent);
                        }
		            }
		            return;
		        } else {
		            element.css('padding','inherit').css('background-color', 'inherit');
		        }
		        if (TssLib.isBlank(options.portions))
		            options.portions = [2, 10];
		        if (TssLib.isBlank(options.checkDataChange))
		            options.checkDataChange = true;
		        var tempContext = context.replace(/ /g,'-');
		        element.data('options', options);
		        element.empty();
		        var _leftTabs = $('<div/>', { class: 'col-sm-' + options.portions[0] + ' oasis-left' });
		        var _rightTabs = $('<div/>', { class: 'col-sm-' + options.portions[1] + ' oasis-right' });
		        element.append(_leftTabs).append(_rightTabs);
		        var _leftTabContent = $('<ul/>', { class: 'tss-oasis-tabs-left' });
		        _leftTabs.append(_leftTabContent);
		        if(!TssLib.isBlank(options.showButton) && options.showButton){
		            var _p=$('<p/>',{style:'position: relative; top: -12px;', class:'text-center'});
		            var _a=$('<a/>',{href:'javascript:;',  class:'button button-common posRb',text:options.buttonName});
		            _p.append(_a);
		            _a.click(options.buttonClickEvent);
		            _leftTabs.append(_p);
		        } 
		        $.each(array, function (i, o) {
		            var _li = $('<li/>', { class: '', html: (TssLib.isBlank(options.leftTabFormatter)) ? o.Name : options.leftTabFormatter(o), id: tempContext + o.Id });
		            _li.data('data', o);
		            _li.click(TssLib.tssOasisTabLeftClick);
		            _leftTabContent.append(_li);
		        });
		        $(_leftTabContent.find('>li')[0]).trigger('click');
		    }, tssOasisTabLeftClick: function (e) {
		        var ele = $(e.currentTarget);
		        var element = ele.closest('.tss-tab-oasis');
		        var noCnf = true;
		        try { $TabCont } catch (e) { $TabCont = null; }
		        if (!TssLib.isBlank($TabCont)) {
		            var CurCont = null;
		            try { CurCont = element.find('.oasis-tss-right-tabs-content').serializeObject(); } catch (e) { }
		            if (!Object.identical(CurCont, $TabCont)) {
		                noCnf = false;
		            }
		        }
		        if (!noCnf) {
		            TssLib.confirm('OASIS Confirmation', 'Page is having unsaved data, Are you sure to continue?', function () {
		                $TabCont = null;
		                proceedTabClick();
		                TssLib.closeConfirmModal(true);
		            }, 'Yes', 'No');
		        } else {
		            $TabCont = null;
		            proceedTabClick();
		        }
		        function proceedTabClick() {
		            var data = ele.data('data');
		            var options = element.data('options');
		            if (!TssLib.isBlank(options.onClickOfLeftTab)) {
		                options.onClickOfLeftTab(data, function (response) {
		                    if (!TssLib.isBlank(options.noRightTabs) && options.noRightTabs) {
		                        element.find('.tss-oasis-tabs-left > li').removeClass('active');
		                        ele.addClass('active');
		                        element.find('.oasis-right').empty().append($('<div/>', { class: 'oasis-tss-right-tabs-content NewErrorStyle' }));
		                        $contEle = element.find('.oasis-tss-right-tabs-content');
		                        var lastData = '';
		                        if (options.checkDataChange) {
		                            lastData = '<script>TssLib.docReady(function () {' +
             '   StoreTabContObjForCheck();' +
             '});function StoreTabContObjForCheck(){TssLib.bindAllEventsOnPageRender($contEle[0]);try { $TabCont = $contEle.serializeObject(); } catch (e) { }}</script>';
		                        } else {
		                            lastData = '<script>TssLib.docReady(function () {' +
             '   StoreTabContObjForCheck();' +
             '});function StoreTabContObjForCheck(){TssLib.bindAllEventsOnPageRender($contEle[0]);try { $TabCont = null; } catch (e) { }}</script>';
		                        }

		                        $contEle.empty().append(response + lastData);
		                    }else
		                        TssLib.tssOasisRightTabBuild(data, response, ele);
		                });
		            }
		        }
		    }, tssOasisRightTabBuild: function (data, response, ele) {
		        var element = ele.closest('.tss-tab-oasis');
		        var options = element.data('options');
		        element.find('.tss-oasis-tabs-left > li').removeClass('active');
		        ele.addClass('active');
		        var rightEle = element.find('.oasis-right');
		        rightEle.empty();
		        var tempContext = data.Name.replace(/ /g,'-');
		        var _workAreaTabs = $('<ul/>', { class: 'tss-oasis-tabs-right right-' + tempContext + data.Id });
		        _workAreaTabs.data('data', data);
		        rightEle.append(_workAreaTabs);
		        $.each(response, function (i, o) {
		            var _li = $('<li/>', { class: '', style: 'cursor:pointer', html: (TssLib.isBlank(options.rightTabFormatter)) ? o.Name : options.rightTabFormatter(o), id: 'right-' + tempContext + data.Id + '-' + o.Id });
		            _li.data('data', o);
		            _li.click(TssLib.tssOasisTabRightClick);
		            _workAreaTabs.append(_li);
		        });
		        rightEle.append($('<div/>', { class: 'oasis-tss-right-tabs-content NewErrorStyle', id: 'right-' + tempContext + data.Id + '-content' }));
		        $(_workAreaTabs.find('>li')[0]).trigger('click');
		    }, tssOasisTabRightClick: function (e) {
		        var ele = $(e.currentTarget);
		        var element = ele.closest('.tss-tab-oasis');
		        var noCnf = true;
		        try { $TabCont } catch (e) { $TabCont = null; }
		        if (!TssLib.isBlank($TabCont)) {
		            var CurCont = null;
		            try { CurCont = element.find('.oasis-tss-right-tabs-content').serializeObject(); } catch (e) { }
		            if (!Object.identical(CurCont, $TabCont)) {
		                noCnf = false;
		            }
		        }
		        if (!noCnf) {
		            TssLib.confirm('OASIS Confirmation', 'Page is having unsaved data, Are you sure to continue?', function () {
		                $TabCont = null;
		                proceedTabClick();
		                TssLib.closeConfirmModal(true);
		            }, 'Yes', 'No');
		        } else {
		            $TabCont = null;
		            proceedTabClick();
		        }
		        function proceedTabClick() {
		            element.find('.tss-oasis-tabs-right > li').removeClass('active');
		            ele.addClass('active');
		            var data = ele.data('data');
		            var parData = ele.closest('ul').data('data');
		            var options = ele.closest('.tss-tab-oasis').data('options');
		            if (!TssLib.isBlank(options.onClickOfRightTab)) {
		                var tempContext = parData.Name.replace(/ /g,'-');
		                options.onClickOfRightTab(data, parData, 'right-' + tempContext + parData.Id + '-content', function (response) {
		                    $contEle = element.find('.oasis-tss-right-tabs-content');

		                    var lastData = '';
		                    if (options.checkDataChange) {
		                        lastData = '<script>TssLib.docReady(function () {' +
         '   StoreTabContObjForCheck();' +
         '});function StoreTabContObjForCheck(){TssLib.bindAllEventsOnPageRender($contEle[0]);try { $TabCont = $contEle.serializeObject(); } catch (e) { }}</script>';
		                    } else {
		                        lastData = '<script>TssLib.docReady(function () {' +
         '   StoreTabContObjForCheck();' +
         '});function StoreTabContObjForCheck(){TssLib.bindAllEventsOnPageRender($contEle[0]);try { $TabCont = null; } catch (e) { }}</script>';
		                    }
		                    $contEle.empty().append(response + lastData);
		                });
		            }
		        }
		    }, LeftRightSlider: function (selector) {
		        selector = $(selector);//'.cls','#as',obj[0] 
		        $.each(selector, function () {
		            var cur = $(this);
		            if (!cur.is(':visible'))
		                return true;
		            if (!cur.find('>table').hasClass('slide-handle')) {
		                cur.find('>*').addClass('slide-portion');
		            } 
		            var slidePortion = cur.find('*.slide-portion');
		            slidePortion.css('width','auto');
		            cur.empty().append(slidePortion);
		            var slWidth = 0;
		            var slideInertia = 0;
		            $.each(slidePortion.find('>*'), function () {
		                slideInertia=$(this).width()
		                slWidth += slideInertia + 10;
		            });
		            slidePortion.css('width', slWidth);
		            var tbl = $('<table/>', { class: 'slide-handle', style: 'table-layout: fixed; width: 100%;' });
		            cur.empty().append(tbl);
		            var tr = $('<tr/>', {});
		            tbl.append(tr);
		            var td = $('<td/>', { title: 'Scroll Left', html: '<i class="fa fa-chevron-left"/>', style: 'width:25px;background-color:#EEEEEE;padding: 0 7px;cursor:pointer;', valign: 'middle', class: 'lh-td' });
		            td.click(function () {
		                var _div = $(this).next('td').find('>*');
		                var lmgn = _div.attr('left-mgn');
		                lmgn = parseInt(  lmgn, 10);
		                if (lmgn <= 0) {
		                    var mgn = (lmgn + slideInertia);
		                    mgn=mgn > 0 ? 0 : mgn;
		                    _div.stop(true, true).animate({ 'margin-left': mgn });
		                    _div.attr('left-mgn', mgn);
		                    if (mgn == 0) {
		                        $(this).hide();
		                    }
		                    $(this).closest('table').find('.rh-td').show();
		                }
		            });
		            tr.append(td);
		            td = $('<td/>', { style: 'padding: 0px 5px;overflow:hidden', class: 'm-td' });
		            slidePortion.attr('left-mgn', 0).css('margin-left', 0);
		            td.append(slidePortion);
		            tr.append(td);
		            td = $('<td/>', { title: 'Scroll Right', html: '<i class="fa fa-chevron-right"/>', style: 'width:25px;background-color:#EEEEEE;padding: 0 7px;cursor:pointer;', valign: 'middle', class: 'rh-td' });
		            td.click(function () {
		                var _div = $(this).prev('td').find('>*');
		                var lmgn = _div.attr('left-mgn');
		                lmgn = parseInt(  lmgn, 10);
		                if (lmgn <= 0) {
		                    var mgn = (lmgn - slideInertia);
		                    var divW = ((-1) * (_div.width()-_div.parent().width()));
		                    mgn =mgn < divW ? divW : mgn;
		                    _div.stop(true, true).animate({ 'margin-left': mgn });
		                    _div.attr('left-mgn', mgn);
		                    if (divW == mgn){
		                        $(this).hide();
		                    }
		                    $(this).closest('table').find('.lh-td').show();
		                }
		            });
		            tr.append(td);
		            if (slWidth <= tbl.find('td.m-td').width()) {
		                tbl.find('td.lh-td').hide();
		                tbl.find('td.rh-td').hide();
		            }
		            tbl.find('td.lh-td').hide();
		        });
		    }
		},
		/* @prototype */
		{
		    init: function () {

		    }
		});
 
/*
    Original version by Chris O'Brien; MIT license
    prettycode.org, github.com/prettycode
*/

Object.identical = function (a, b, sortArrays) {
    function sort(object) {
        if (sortArrays === true && Array.isArray(object)) {
            return object.sort();
        }
        else if (typeof object !== "object" || object === null) {
            return object;
        }

        return Object.keys(object).sort().map(function (key) {
            return {
                key: key,
                value: sort(object[key])
            };
        });
    }

    return JSON.stringify(sort(a)) === JSON.stringify(sort(b));
};
function getPageAsyncWithSiteUrl(url, data, options) {
    TssLib.getPageAsync(TssConfig.SITE_URL + url, data, options);
}
function getJsonWithBaseUrl(url, data, options) {
    return TssLib.getJson(TssConfig.BASE_URL + url, data, options);
}
function postJsonWithBaseUrl(url, data, options) {
    return TssLib.postJson(TssConfig.BASE_URL + url, data, options);
}

function getJson(url, data, options) {
    return TssLib.getJson(url, data, options);
}
function postJson(url, data, options) {
    return TssLib.postJson(url, data, options);
}
function getPage(url, data, options) {
    return TssLib.getPage(url, data, options);
}
function postPage(url, data, options) {
    return TssLib.postPage(url, data, options);
}
function loadCss(cssUrl, reload) {
    return TssLib.loadCss(cssUrl, reload);
}
function loadJs(jsUrl, reload) {
    return TssLib.loadJs(jsUrl, reload);
}
function showLog(data) {
    if (TssConfig.SHOW_LOGS)
        console.log(data);
}

function getJsonAsyncWithBaseUrl(url, data, options) {
    return TssLib.getJsonAsync(TssConfig.BASE_URL + url, data, options);
}
function postJsonAsyncWithBaseUrl(url, data, options) {
    TssLib.postJsonAsync(TssConfig.BASE_URL + url, data, options);
}
function getJsonAsync(url, data, options) {
    return TssLib.getJsonAsync(url, data, options);
}
function postJsonAsync(url, data, options) {
    TssLib.postJsonAsync(url, data, options);
}
function getPageAsync(url, data, options) {
    TssLib.getPageAsync(url, data, options);
}
function postPageAsync(url, data, options) {
    TssLib.postPageAsync(url, data, options);
}
function postPageAsyncWithSiteUrl(url, data, options) {
    TssLib.postPageAsync(TssConfig.SITE_URL + url, data, options);
}
/*--------------------------------------------------------------------------------------------------------*/
/*----------------------------------Tss Library js End----------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------------------------------------*/
/*----------------------------------Tss Validation js Start-----------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------*/
$.Class.extend('TssValidation',
		/* @prototype */
		{
		    init: function () {

		    }, removeValidations: function (selector) {
		        var form = $(selector);
		        form.find('.has-error').find('.error').remove();
		        form.find('.has-error').removeClass('has-error');
		        form.find('.rval').removeClass('err-bg');
				form.find('.input_container').removeClass('err-bg');
		        form.find('.error-msg').remove();
		    }, validateEmail: function (email) {
		        var _res = true;
		        var mail_pattern = /^([a-zA-Z0-9_\-\.])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		        if (!TssLib.isBlank(email) && !mail_pattern.test(email)) {
		            _res = false;
		        }
		        return _res;
		    }, validate: function (selector, skipFieldsOfElements) {
		        var form = $(selector);
		        TssValidation.removeValidations(form);
		        form.find('.has-error').find('.error').remove();
		        form.find('.has-error').removeClass('has-error');
		        var errorFields = {};
		        var validatedFields = form.find('.rval');
				$.each(validatedFields, function (i, o) {
		            var name = $(o).attr('name');
		            var fields = $(o);
		            if (!fields.is(':visible') && !fields.hasClass('forceDoValidHidden'))
		                return true;
		            if (!TssLib.isBlank(name)) {
		                fields = form.find('*[name="' + name + '"]');
		            } else {
		                name = 'Field_' + i;
		                $(o).attr('name', name);
		            }
		            try {
		                if (skipFieldsOfElements != undefined && skipFieldsOfElements !== null && skipFieldsOfElements.length > 0) {
		                    var flag = false;
		                    $.each(skipFieldsOfElements, function (m, _el) {
		                        if ($.contains($(_el)[0], o)) {
		                            flag = true;
		                            return false;
		                        }
		                    });
		                    if (flag)
		                        return true;
		                }
		            } catch (ex) { showLog(ex); }
		            var type = null;
		            var custValid = $(o).attr('cust-valid');
		            if (!TssLib.isBlank(custValid)) {
		                try {
		                    var resVal = window['' + custValid](fields);
		                    if (!resVal[0]) {
		                        if (errorFields[name] == undefined || errorFields[name] == null)
		                            errorFields[name] = [];
		                        errorFields[name].push(resVal[1]);
		                    }
		                } catch (e) { showLog(e); }
		                return true;
		            }
		            var validationRules = $(o).attr('rules');
		            if (TssLib.isBlank(validationRules)) validationRules = 'required';
		            validationRules = validationRules.split(',');
		            $.each(fields, function () {
		                type = $(this).attr('type');
		                return false;
		            });
		            if (TssLib.containsArray(validationRules, 'required')) {
		                var _res;
		                if (type == 'radio' || type == 'checkbox')	//radio checkbox
		                    _res = TssValidation.requiredValidationForRadioChecked(fields);
		                else
		                    _res = TssValidation.requiredValidation(fields);
		                if (!_res[0]) {
		                    if (errorFields[name] == undefined || errorFields[name] == null)
		                        errorFields[name] = [];
		                    var msg = $(o).attr('required-msg');
		                    if (TssLib.isBlank(msg)) msg = 'Required';
		                    errorFields[name].push(msg);
		                }
		            }
		            if (TssLib.containsArray(validationRules, 'phone')) {
		                var _res = TssValidation.phoneValidation(fields);
		                if (!_res[0]) {
		                    if (errorFields[name] == undefined || errorFields[name] == null)
		                        errorFields[name] = [];
		                    var msg = $(o).attr('phone-msg');
		                    if (TssLib.isBlank(msg)) msg = 'Invalid Phone';
		                    errorFields[name].push(msg);
		                }
		            }
		            if (TssLib.containsArray(validationRules, 'email')) {
		                var _res;
		                if (type == 'radio' || type == 'checkbox')	//radio checkbox
		                    _res = [true];
		                else {
		                    _res = [true];
		                    var mail_pattern = /^([a-zA-Z0-9_\-\.])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		                    $.each(fields, function () {
		                        if (!TssLib.isBlank($(this).val()) && !mail_pattern.test($(this).val())) {
		                            _res = [false];
		                            return false;
		                        }
		                    });
		                }
		                if (!_res[0]) {
		                    if (errorFields[name] == undefined || errorFields[name] == null)
		                        errorFields[name] = [];
		                    var msg = $(o).attr('email-msg');
		                    if (TssLib.isBlank(msg)) msg = 'Invalid email';
		                    errorFields[name].push(msg);
		                }
		            }
		            if (TssLib.containsArray(validationRules, 'pattern')) {
		                var _res;
		                if (type == 'radio' || type == 'checkbox')	//radio checkbox
		                    _res = [true];
		                else {
		                    _res = [true];
		                    try {
		                        var patternString = $(o).attr('pattern-string');
		                        if (TssLib.isBlank(patternString)) patternString = '';
		                        patternString = new RegExp(patternString);
		                        $.each(fields, function () {
		                            if (!TssLib.isBlank($(this).val()) && !patternString.test($(this).val())) {
		                                _res = [false];
		                                return false;
		                            }
		                        });
		                    } catch (e) { showLog(e); }
		                }
		                if (!_res[0]) {
		                    if (errorFields[name] == undefined || errorFields[name] == null)
		                        errorFields[name] = [];
		                    var msg = $(o).attr('pattern-msg');
		                    if (TssLib.isBlank(msg)) msg = 'Invalid Field';
		                    errorFields[name].push(msg);
		                }
		            }
		        });
		        if ($.isEmptyObject(errorFields)) {
		            return { success: true };
		        } else {
		            var errorFields = { success: false, fields: errorFields };
		            try {
		                TssValidation.showErrorsOnDom(errorFields, form);
		            } catch (e) {
		                showLog(e);
		            }
		            return errorFields;
		        }
		    }, showErrorsOnDom: function (errorFields, form, sentAreFields, noFocus) {
		        if (TssLib.isBlank(noFocus))
		            noFocus = false;
		        if (form.hasClass('NewErrorStyle')) {
		            var focusFld = null;
					//showLog(errorFields);
					$.each(errorFields.fields, function (i, o) {
		                var field = null;
		                if (TssLib.isBlank(sentAreFields)) {
		                    sentAreFields = false;
		                }
		                if (sentAreFields) {
		                    field = o.field;
		                    o = o.errors;
		                } else {
		                    field = form.find('*[name="' + i + '"]');
		                }
		                field = field.length > 1 ? $(field[0]) : field;
		                if (field.hasClass('NoErrorWithValidation'))
		                    return true;
		                if (!noFocus && focusFld === null)
		                    focusFld = field;
		                if (field.closest('.form-group').find('.error-msg').length == 0) {
		                    field.closest('.form-group').prepend($('<span/>', { 'class': 'error-msg', text: o.join(', ') }));
		                } else {
		                    field.closest('.form-group').find('.error-msg').html(o.join(', '));
		                }
						if(field.prop('type')=="radio" || field.prop("type")=="select-one" || field.prop("type")=="select-multiple" || field.prop("type")=="checkbox"){
							field.closest('.input_container').addClass('err-bg');
						}else{
							 field.addClass('err-bg');
						}
		            });
		            if (!noFocus && focusFld != null)
		                focusFld.focus();
		            //TssLib.notify('Page is having fields with invalid data, Please verify.', 'warn');
		        } else {
		            var focusFld = null;
		            $.each(errorFields.fields, function (i, o) {
		                var field = form.find('*[name="' + i + '"]');
		                field = field.length > 1 ? $(field[0]) : field;
		                if (field.hasClass('NoErrorWithValidation'))
		                    return true;
		                if (focusFld === null)
		                    focusFld = field;
		                var type = null;
		                try { type = field.attr('type'); } catch (e) { }
		                if (field.closest('.form-group').length == 0) {
		                    field.parent().addClass('has-error');
		                }
		                field.closest('.form-group').addClass('has-error');
		                field.closest('.fldg').append($('<label/>', { 'class': 'error', text: o.join(', ') }));
		            });
		            if (noFocus) focusFld = null;
		            if (focusFld != null)
		                focusFld.focus();
		            //TssLib.notify('Page is having fields with invalid data, Please verify.', 'error');
		        }
		    }, requiredValidationForRadioChecked: function (fields) {
		        var flg = false;
		        $.each(fields, function (i, o) {
		            if (o.checked) {
		                flg = true;
		                return false;
		            }
		        });
		        return [flg, ''];
		    }, dateValidationForValidDate: function (fields) {
		        var flg = true;
		        return [flg, ''];
		    }, requiredValidation: function (fields) {
		        var emptyVal = $(fields).attr('rval');
		        if (TssLib.isBlank(emptyVal))
		            emptyVal = '';
		        if ($.trim($(fields).val()) == emptyVal)
		            return [false, ''];
		        return [true, ''];
		    }, phoneValidation: function (fields) {
		        var inputtxt = $.trim($(fields).val());
		        if (inputtxt == '')
		            return [true, ''];
		        var phoneno1 = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})[-. ]?([0-9]{2})$/;
		        //+XX-XXXX-XXXX-XX, +XX.XXXX.XXXX.XX, +XX XXXX XXXX XX
		        var phoneno2 = /^\(?([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //XX-XXX-XXX-XXXX, XX.XXX.XXX.XXXX, XX XXX XXX XXXX
		        var phoneno3 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //XXX-XXX-XXXX, XXX.XXX.XXXX, XXX XXX XXXX
		        var phoneno4 = /^\d{10}$/;
		        //XXXXXXXXXX
		        var phoneno5 = /^\d{12}$/;
		        //XXXXXXXXXXXX
		        var phoneno6 = /^\+?([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+XX-XXX-XXX-XXXX, +XX.XXX.XXX.XXXX, +XX XXX XXX XXXX
		        var phoneno7 = /^\+?([0-9]{2})\)?[-. ]?([0-9]{10})$/;
		        //+XX-XXXXXXXXXX, +XX.XXXXXXXXXX, +XX XXXXXXXXXX
		        var phoneno8 = /^\+?([0-9]{12})\)$/;
		        //+XXXXXXXXXXXX

		        //International
		        var phoneno9 = /^\+?([0-9]{1})\)?[ ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+X XXX-XXX-XXXX, +X XXX.XXX.XXXX, +X XXX XXX XXXX
		        var phoneno10 = /^\+?([0-9]{2})\)?[ ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+XX XXX-XXX-XXXX, +XX XXX.XXX.XXXX, +XX XXX XXX XXXX
		        var phoneno11 = /^\+?([0-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+X-XXX-XXX-XXXX, +X.XXX.XXX.XXXX, +X XXX XXX XXXX
		        var phoneno12 = /^\+?([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+XX-XXX-XXX-XXXX, +XX.XXX.XXX.XXXX, +XX XXX XXX XXXX
		        var phoneno13 = /^\+?([0-9]{1})\)?[ ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //+X XX-XXXX-XXXX, +X XX.XXXX.XXXX, +X XX XXXX XXXX
		        var phoneno14 = /^\+?([0-9]{2})\)?[ ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //+XX XX-XXXX-XXXX, +XX XX.XXXX.XXXX, +XX XX XXXX XXXX
		        var phoneno15 = /^\+?([0-9]{1})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //+X-XX-XXXX-XXXX, +X.XX.XXXX.XXXX, +X XX XXXX XXXX
		        var phoneno16 = /^\+?([0-9]{2})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //+XX-XX-XXXX-XXXX, +XX.XX.XXXX.XXXX, +XX XX XXXX XXXX
		        var phoneno17 = /^\?([0-9]{1})\)?[ ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //X XXX-XXX-XXXX, X XXX.XXX.XXXX, X XXX XXX XXXX
		        var phoneno18 = /^\?([0-9]{2})\)?[ ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //XX XXX-XXX-XXXX, XX XXX.XXX.XXXX, XX XXX XXX XXXX
		        var phoneno19 = /^\?([0-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //X-XXX-XXX-XXXX, X.XXX.XXX.XXXX, X XXX XXX XXXX
		        var phoneno20 = /^\?([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //XX-XXX-XXX-XXXX, XX.XXX.XXX.XXXX, XX XXX XXX XXXX
		        var phoneno21 = /^\?([0-9]{1})\)?[ ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //X XX-XXXX-XXXX, X XX.XXXX.XXXX, X XX XXXX XXXX
		        var phoneno22 = /^\?([0-9]{2})\)?[ ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //XX XX-XXXX-XXXX, XX XX.XXXX.XXXX, XX XX XXXX XXXX
		        var phoneno23 = /^\?([0-9]{1})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //X-XX-XXXX-XXXX, X.XX.XXXX.XXXX, X XX XXXX XXXX
		        var phoneno24 = /^\?([0-9]{2})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //XX-XX-XXXX-XXXX, XX.XX.XXXX.XXXX, XX XX XXXX XXXX
		        var phoneno25 = /^\+?([0-9]{3})\)?[ ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+XXX XXX-XXX-XXXX, +XXX XXX.XXX.XXXX, +XXX XXX XXX XXXX
		        var phoneno25 = /^\+?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //+XXX-XXX-XXX-XXXX, +XXX.XXX.XXX.XXXX, +XXX XXX XXX XXXX
		        var phoneno26 = /^\+?([0-9]{3})\)?[ ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //+XXX XX-XXXX-XXXX, +XXX XX.XXXX.XXXX, +XXX XX XXXX XXXX
		        var phoneno27 = /^\+?([0-9]{3})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //+XXX-XX-XXXX-XXXX, +XXX.XX.XXXX.XXXX, +XXX XX XXXX XXXX
		        var phoneno28 = /^\?([0-9]{3})\)?[ ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //XXX XXX-XXX-XXXX, XXX XXX.XXX.XXXX, XXX XXX XXX XXXX
		        var phoneno29 = /^\?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		        //XXX-XXX-XXX-XXXX, XXX.XXX.XXX.XXXX, XXX XXX XXX XXXX
		        var phoneno30 = /^\?([0-9]{3})\)?[ ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //XXX XX-XXXX-XXXX, XXX XX.XXXX.XXXX, XXX XX XXXX XXXX
		        var phoneno31 = /^\?([0-9]{3})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		        //XXX-XX-XXXX-XXXX, XXX.XX.XXXX.XXXX, XXX XX XXXX XXXX

		        if (phoneno1.test(inputtxt) ||
                    phoneno2.test(inputtxt) ||
                    phoneno3.test(inputtxt) ||
                    phoneno4.test(inputtxt) ||
                    phoneno5.test(inputtxt) ||
                    phoneno6.test(inputtxt) ||
		            phoneno7.test(inputtxt) ||
		            phoneno8.test(inputtxt) ||
                    phoneno9.test(inputtxt) ||
                    phoneno10.test(inputtxt) ||
                    phoneno11.test(inputtxt) ||
                    phoneno12.test(inputtxt) ||
                    phoneno13.test(inputtxt) ||
                    phoneno14.test(inputtxt) ||
                    phoneno15.test(inputtxt) ||
                    phoneno16.test(inputtxt) ||
                    phoneno17.test(inputtxt) ||
                    phoneno18.test(inputtxt) ||
                    phoneno19.test(inputtxt) ||
                    phoneno20.test(inputtxt) ||
                    phoneno21.test(inputtxt) ||
                    phoneno22.test(inputtxt) ||
                    phoneno23.test(inputtxt) ||
                    phoneno24.test(inputtxt) ||
                    phoneno25.test(inputtxt) ||
                    phoneno26.test(inputtxt) ||
                    phoneno27.test(inputtxt) ||
                    phoneno28.test(inputtxt) ||
                    phoneno29.test(inputtxt) ||
                    phoneno30.test(inputtxt) ||
                    phoneno31.test(inputtxt)
                    )
		            return [true, ''];
		        else
		            return [false, ''];
		    }
		},
		/* @static */
		{
		    init: function () {

		    }
		});
/*Sample class for demo*/
$.Class.extend('TssTest',
    /* @static */
    {

    },
    /* @prototype */
    {
        init: function (param) {
            this.param = param;
        }
    }
);


$.fn.serializeObject = function (dtFlg) {
    var _obj = this;
    if (this[0].nodeName != 'FORM')
        _obj = this.find('*');
    var o = {};
    var a = _obj.serializeArray();

    $.each(a, function () {
        //skip
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push($.trim(this.value) || '');
        } else {
            o[this.name] = $.trim(this.value) || '';
        }
    });
    /*Code for sending 0 for empty int fields start*/
    $.each(_obj.find('.pInt'), function (i, oj) {
        try {
            if (!TssLib.isBlank(oj.name) && TssLib.isBlank(o[oj.name]))
                o[oj.name] = '0';
        } catch (e) { }
    });
    $.each(_obj.find('.int'), function (i, oj) {
        try {
            if (!TssLib.isBlank(oj.name) && TssLib.isBlank(o[oj.name]))
                o[oj.name] = '0';
        } catch (e) { }
    });
    $.each(_obj.find('.pDec'), function (i, oj) {
        try {
            if (!TssLib.isBlank(oj.name) && TssLib.isBlank(o[oj.name]))
                o[oj.name] = '0';
        } catch (e) { }
    });
    $.each(_obj.find('.dec'), function (i, oj) {
        try {
            if (!TssLib.isBlank(oj.name) && TssLib.isBlank(o[oj.name]))
                o[oj.name] = '0';
        } catch (e) { }
    });
    $.each(_obj.find('.tssDatepicker'), function (i, oj) {
        try {
            if (!TssLib.isBlank(oj.name) && TssLib.isBlank(o[oj.name])) {
                if (dtFlg===true)
                    o[oj.name] = null;
                else
                    delete o[oj.name];
            } else {
                var dt = $(oj).datepicker('getDate');
                dt = new Date(Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds(), dt.getMilliseconds()));
                o[oj.name] = dt.toMSJSON();
            }
        } catch (e) { }
    });
    /*Code for sending 0 for empty int fields end*/
    var temp = o;
    $.each(o, function (k, v) {
        try {
            var field = _obj.find('*[name="' + k + '"]');
            field = field.length > 1 ? field[0] : field;
            try {
                if (field.hasClass('skip'))
                    delete temp[k];
            } catch (e) {
                if ($(field).hasClass('skip'))
                    delete temp[k];
            }

        } catch (e) { showLog(e); }
    });
    $.each(_obj.find('*[multi-val="true"]'), function (k, v) {
        if (!TssLib.isBlank(v.name)) {
            if (!$.isArray(temp[v.name])) temp[v.name] = TssLib.isBlank(temp[v.name])?[]:[temp[v.name]];
        }
    });
    var returnObj = $.extend(temp, getDisabledValues(_obj.find('*[disabled]')))
    return arrangeJson(returnObj);
};
function arrangeJson(ele) {
    var data = {};
    $.each(ele, function (k, v) {
        var keyArr = k.split('.');
        if (!TssLib.isBlank(keyArr) && keyArr.length > 0) {
            buildJsonWithDots(keyArr, v, data);
        } else
            data[k] = v;
    });
    return data;
}
function buildJsonWithDots(ka, val, data) {
    var obj = data;
    $.each(ka, function (i, o) {
        if (i < ka.length - 1) {
            if (TssLib.isBlank(obj[o]))
                obj[o] = {};
            obj = obj[o]
        } else {
            obj[o] = val;
        }

    });
}
function getDisabledValues(arr) {
    var json = {};
    $.each(arr, function (i, o) {
        var name = $(o).attr('name');
        if (!TssLib.isBlank(name)) {
            json[name] = o.value;
        }
    });
    return json;
}
/*--------------------------------------------------------------------------------------------------------*/
/*----------------------------------Tss Validation js End-------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------*/
var guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                   .toString(16)
                   .substring(1);
    }
    return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
               s4() + '-' + s4() + s4() + s4();
    };
})();

$.Class.extend('TssTree',
    /* @static */
    {

    },
    /* @prototype */
    {
        init: function (options) {
            var thisObject = this;
            this.options = $.extend({ data: [], expandAll: false, selector: '.tree' }, options);
            this._div = $(this.options.selector);
            this.options.id = this._div.attr('id');
            if (TssLib.isBlank(this.options.id)) this.options.id = 'tree id';
            if (TssLib.isBlank(this.options.data)) this.options.data = [];
            this._div.data('TreeData', this.options.data);
            if (!this._div.hasClass('tree'))
                this._div.addClass('tree');
            this._div.append($('<ul/>', {}));
            $.each(this.options.data, function (i, o) {
                thisObject.buildTreeNode(o, thisObject._div.find('>ul'), true);
            });
            this.completeTreeEvents();
            this._div.data('options', this.options);
            if (TssLib.isBlank(this.options.noItemsMessage))
                this.options.noItemsMessage = 'No Data';
            if (TssLib.isBlank(this.options.data))
                this._div.append($('<div/>', { 'class': 'noItems', style: 'text-align:center', text: this.options.noItemsMessage }));
            //Searching in Tree
            if (!TssLib.isBlank(thisObject.options.searchFieldSelector))
                $(thisObject.options.searchFieldSelector).keypress(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        thisObject.searchNode(this.value);
                    }
                });
        }, completeTreeEvents: function () {
            this._div.find('li:has(ul)').addClass('parent_li').find(' > span > i.fa-minus-square').parent().attr('title', 'Collapse');
            this._div.find('li:has(ul)').addClass('parent_li').find(' > span > i.fa-plus-square').parent().attr('title', 'Expand');
            this._div.find('li.parent_li > span').unbind('click');
            this._div.find('li.parent_li > span').click(function (e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(":visible")) {
                    children.hide('fast');
                    $(this).attr('title', 'Expand').find(' > i').addClass('fa-plus-square').removeClass('fa-minus-square');
                } else {
                    children.show('fast');
                    $(this).attr('title', 'Collapse').find(' > i').addClass('fa-minus-square').removeClass('fa-plus-square');
                }
                e.stopPropagation();
            });
        }, buildTreeNode: function (o, parentNode, isTop) {
            this._div.find('.noItems').remove();
            var thisObject = this;
            var uid = o.id;
            if (TssLib.isBlank(uid)) {
                uid = guid();
            }
            if (TssLib.isBlank(o.children)) o.children = [];
            var _li = $('<li/>', { 'class': !isTop ? '' : 'first-parent', 'style': !isTop ? 'display:none' : '' });
            var _span = $('<span/>', { id: this.options.id.replace(/\s/g, '') + '_' + uid });
            var _ico = $('<i/>', { 'class': 'fa fa-plus-square', 'style': 'float: left; margin-top: 4px;' });
            if (!TssLib.isBlank(o.bgColor))
                _span.css('background-color', o.bgColor);
            if (!TssLib.isBlank(o.fontColor)) {
                _span.css('color', o.fontColor);
                _ico.css('color', 'inherit');
            }
            _li.append(_span);
            _span.data('BuildData', o);
            _span.mouseover(function () {
                if (thisObject.options.onMouseOver) {
                    thisObject.options.onMouseOver(_span);
                }
                if (!TssLib.isBlank(o.bgColor)) {
                    $(_span).parent().find('.btn-wrapper>').css('background-color', o.bgColor);
                }
            });
            if (o.children.length > 0) _span.append(_ico);
            var _div = $('<div/>', { 'text': o.title, 'style': 'float: left;cursor:pointer; ' });
            _span.append(_div);
            _div.click(function () {
                if (thisObject.options.onclick) {
                    thisObject.options.onclick(_div);
                }
            });

            if (thisObject.options.addNopInputBox) {
                _span.append(" <span group-submit='true' group-submit-name='TocInformations' style='margin-left: 2px;border: 0px;'>( Nop : <input type='hidden' name='Id' value='" + o.obj.Id + "'/> <input type='text' name='NoOfPeriod' value='" + o.obj.NoOfPeriod + "' style='width:40px; height:21px;' class='text-center'/> ) </span>");
            }

            parentNode.append(_li);

            if (o.children.length > 0) {
                _li.append($('<ul/>', {}));
            }
            $.each(o.children, function (i, _o) {
                thisObject.buildTreeNode(_o, _li.find('>ul'), false);
            });
        }, addTreeNode: function (o, node) {
            var _div = $(node).closest('.tree');
            var options = _div.data('options');
            var _ul = $(node).closest('li').find('>ul');
            if ($(node).closest('li').find('>span>i').length == 0)
                $(node).closest('li').find('>span').prepend($('<i/>',
                    { 'class': 'fa', 'style': 'float: left; margin-top: 4px;' + (TssLib.isBlank(o.fontColor) ? '' : 'color:inherit') }));
            $(node).closest('li').find('>span>i').removeClass('fa-plus-square');
            $(node).closest('li').find('>span>i').addClass('fa-minus-square');
            if (_ul.length == 0) {
                _ul = $('<ul/>');
                $(node).closest('li').append(_ul);
            }
            this.buildTreeNode(o, _ul, true);
            this.completeTreeEvents(_div);
            _ul.find('>li').show('fast');
        }, deleteTreeNode: function (node) {
            var _ul = $(node).closest('li').parent();
            $(node).closest('li').remove();
            if (_ul.find('li').length == 0) {
                _ul.parent().find('span>i').remove();
                _ul.remove();
            }
            if ($.trim(this._div.find('>ul').text()) == '')
                this._div.append($('<div/>', { 'class': 'noItems', style: 'text-align:center', text: this.options.noItemsMessage }));
        }, updateTreeNode: function (text, node) {
            var _span = $(node).closest('li').find('>span');
            _span.data('BuildData').title = text;
            _span.find('div').text(text);
            ;
        }, getTreeNodeData: function (node) {
            return $(node).closest('li').find('>span').data('BuildData');
        }, addRootTreeNode: function (o) {
            if (this._div.find('>ul').length == 0) {
                this._div.append($('<ul/>'));
            }
            this.buildTreeNode(o, this._div.find('>ul'), true);
        }, expandAll: function () {
            var cur = this;
            $.each(cur._div.find('span>i.fa-plus-square'), function (i, o) {
                $(o).parent().trigger('click');
            });
        }, collapseAll: function () {
            var cur = this;
            $.each(cur._div.find('span>i.fa-minus-square'), function (i, o) {
                $(o).parent().trigger('click');
            });
        }, searchNode: function (key) {
            if ($.trim(key) == '') {
                this.expandAll();
                this._div.find('li').show();
                this._div.find('span>i').remove('fa-plus-square').addClass('fa-minus-square');
                return true;
            }
            this._div.find('li').hide();
            var lis = this._div.find('ul>li>span>div:containsIgnoreCase(' + key + ')');
            $.each(lis, function (i, o) {
                var _li = $(o).closest('li');
                _li.show();
                if (_li.find('>span>i').hasClass('fa-plus-square')) {
                    _li.find('>span>i').remove('fa-plus-square').addClass('fa-minus-square');
                }
                var _li = _li.parents('li.parent_li');
                while (_li.length > 0) {
                    _li.show();
                    if (_li.find('>span>i').hasClass('fa-plus-square')) {
                        _li.find('>span>i').remove('fa-plus-square').addClass('fa-minus-square');
                    }
                    _li = _li.parents('li.parent_li');
                }
            });
        }
    }
);
$.expr[":"].eleContainsIgnoreCase = $.expr.createPseudo(function (arg) {
    return function (elem) {
        if ($(elem).text().toUpperCase() == arg.toUpperCase()) {
            return $(elem);
        } else {
            return null;
        }
    };
});
$.expr[":"].containsIgnoreCaseExactMatch = $.expr.createPseudo(function (arg) {
    //showLog(arg);
    return function (elem) {
        //showLog(elem);
        return $(elem).text().toUpperCase() == arg.toUpperCase();
    };
});
$.expr[":"].containsIgnoreCaseExactMatchValue = $.expr.createPseudo(function (arg) {
    return function (elem) {
        return $(elem).val().toUpperCase() == arg.toUpperCase();
    };
});

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
Date.prototype.getDaysInMonth = function () {
    var day = this.getDate();
    var month = this.getMonth();
    var dt = new Date(this.getYear(), this.getMonth(), this.getDate());
    while (month == dt.addDays(1).getMonth()) {
        day = dt.getDate();
    }
    return day;
};
Date.prototype.toMSJSON = function () {
    var date = '/Date(' + this.getTime() + ')/'; //CHANGED LINE
    return date;
};

String.prototype.toInt = function () {
    var s = this.replace(/,/g, "");
    return TssLib.parseInt(s);
};
String.prototype.toFloat = function () {
    var s = this.replace(/,/g, "");
    return TssLib.parseFloat(s);
};
String.prototype.DateWCF = function () {
    var re = /-?\d+/;
    var m = re.exec(this);
    return new Date(parseInt(m[0]));
    //return new Date(parseInt(this.match(/\/Date\(([0-9]+)(?:.*)\)\//)[1]));
};
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    return this;
};
Date.prototype.format = function (format) {
    var returnStr = '';
    var replace = Date.replaceChars;
    for (var i = 0; i < format.length; i++) {
        var curChar = format.charAt(i); if (i - 1 >= 0 && format.charAt(i - 1) == "\\") {
            returnStr += curChar;
        }
        else if (replace[curChar]) {
            returnStr += replace[curChar].call(this);
        } else if (curChar != "\\") {
            returnStr += curChar;
        }
    }
    return returnStr;
};
Date.daysBetween = function (date1, date2) {
    var one_day = 1000 * 60 * 60 * 24;
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    var difference_ms = date2_ms - date1_ms;
    return Math.round(difference_ms / one_day);
}
Date.replaceChars = {
    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    longMonths: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    longDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

    // Day
    d: function () { return (this.getDate() < 10 ? '0' : '') + this.getDate(); },
    D: function () { return Date.replaceChars.shortDays[this.getDay()]; },
    j: function () { return this.getDate(); },
    l: function () { return Date.replaceChars.longDays[this.getDay()]; },
    N: function () { return this.getDay() + 1; },
    S: function () { return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th'))); },
    w: function () { return this.getDay(); },
    z: function () { var d = new Date(this.getFullYear(), 0, 1); return Math.ceil((this - d) / 86400000); }, // Fixed now
    // Week
    W: function () { var d = new Date(this.getFullYear(), 0, 1); return Math.ceil((((this - d) / 86400000) + d.getDay() + 1) / 7); }, // Fixed now
    // Month
    F: function () { return Date.replaceChars.longMonths[this.getMonth()]; },
    m: function () { return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1); },
    M: function () { return Date.replaceChars.shortMonths[this.getMonth()]; },
    n: function () { return this.getMonth() + 1; },
    t: function () { var d = new Date(); return new Date(d.getFullYear(), d.getMonth(), 0).getDate() }, // Fixed now, gets #days of date
    // Year
    L: function () { var year = this.getFullYear(); return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)); },   // Fixed now
    o: function () { var d = new Date(this.valueOf()); d.setDate(d.getDate() - ((this.getDay() + 6) % 7) + 3); return d.getFullYear(); }, //Fixed now
    Y: function () { return this.getFullYear(); },
    y: function () { return ('' + this.getFullYear()).substr(2); },
    // Time
    a: function () { return this.getHours() < 12 ? 'am' : 'pm'; },
    A: function () { return this.getHours() < 12 ? 'AM' : 'PM'; },
    B: function () { return Math.floor((((this.getUTCHours() + 1) % 24) + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1000 / 24); }, // Fixed now
    g: function () { return this.getHours() % 12 || 12; },
    G: function () { return this.getHours(); },
    h: function () { return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12); },
    H: function () { return (this.getHours() < 10 ? '0' : '') + this.getHours(); },
    i: function () { return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes(); },
    s: function () { return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds(); },
    u: function () {
        var m = this.getMilliseconds(); return (m < 10 ? '00' : (m < 100 ?
    '0' : '')) + m;
    },
    // Timezone
    e: function () { return "Not Yet Supported"; },
    I: function () {
        var DST = null;
        for (var i = 0; i < 12; ++i) {
            var d = new Date(this.getFullYear(), i, 1);
            var offset = d.getTimezoneOffset();

            if (DST === null) DST = offset;
            else if (offset < DST) { DST = offset; break; } else if (offset > DST) break;
        }
        return (this.getTimezoneOffset() == DST) | 0;
    },
    O: function () { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00'; },
    P: function () { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':00'; }, // Fixed now
    T: function () { var m = this.getMonth(); this.setMonth(0); var result = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); this.setMonth(m); return result; },
    Z: function () { return -this.getTimezoneOffset() * 60; },
    // Full Date/Time
    c: function () { return this.format("Y-m-d\\TH:i:sP"); }, // Fixed now
    r: function () { return this.toString(); },
    U: function () { return this.getTime() / 1000; }
};
 //extending the attr function to return all attrs
  (function($) {
          // duck-punching to make attr() return a map
          var _old = $.fn.attr;
          $.fn.attr = function() {
            var a, aLength, attributes, map;
            if (this[0] && arguments.length === 0) {
                    map = {};
                    attributes = this[0].attributes;
                    aLength = attributes.length;
                    for (a = 0; a < aLength; a++) {
                            map[attributes[a].name.toLowerCase()] = attributes[a].value;
                    }
                    return map;
            } else 
                    return _old.apply(this, arguments);

    }
  }(jQuery));
  /* Method to move values form one index to another*/
  /* Ashutosh Nigam - used in UserHeader.cshtml*/
  Array.prototype.move || Object.defineProperty(Array.prototype, "move", {
      value: function (index, howMany, toIndex) {
          var
          array = this,
          index = parseInt(index) || 0,
          index = index < 0 ? array.length + index : index,
          toIndex = parseInt(toIndex) || 0,
          toIndex = toIndex < 0 ? array.length + toIndex : toIndex,
          toIndex = toIndex <= index ? toIndex : toIndex <= index + howMany ? index : toIndex - howMany,
          moved;

          array.splice.apply(array, [toIndex, 0].concat(moved = array.splice(index, howMany)));

          return moved;
      }
  });


  function generateReciepts($gridData, $headerData) {
      var obj = null;
      var rec = null;
      var val1 = 'Lead Id:';
      var val2 = 'Enroll/Lead No:';
      var couponAmount = '';
      var payModeDetails = $headerData.PaymentMode;
      if (TssLib.isBlank(payModeDetails))
          payModeDetails = 'Net Banking';
      if (payModeDetails.toLowerCase() == 'cheque' || payModeDetails.toLowerCase() == 'dd') {
          payModeDetails += ' | ' + $headerData.ChequeDdNo + ' | ' + $headerData.ChequeDdDate.DateWCF().format('d/m/Y') + ' | ' + $headerData.BankName;
      } else if (payModeDetails.toLowerCase() == 'credit card') {
          payModeDetails += ' | ' + $headerData.Last4Digits;
      }
      var html = '<div class="wrapper" data-reciept="assets"><p class="campus">' + '' + '</p>' +//($headerData.CampusCode == null ? $headerData.Campus : $headerData.CampusCode)
                  '<h1>' + $headerData.CompanyName + '</h1>' +
                  '<p>' + $headerData.CompanyLocation + '</p>' +
                  '<table class="studentDetails" border="0" cellpadding="4" cellspacing="0">' +
                      '<tr>' +
                          '<td>' + (($headerData.EnrollNo == "") ? val1 : val2) + '</td>' +
                          '<td>' + (($headerData.EnrollNo == "") ? ($headerData.FkLeadId==0?'---':$headerData.FkLeadId): $headerData.EnrollNo) + '</td>' +
                          '<td>Class:</td>' +
                          '<td>' + ($headerData.Class==null? '---' : $headerData.Class) + '</td>' +
                          '<td>Campus:</td>' +
                          '<td>' + ($headerData.CampusCode == null ? $headerData.Campus : $headerData.CampusCode) + '</td>' +
                          '<td>Reciept No:</td>' +
                          '<td>' + $headerData.ReceiptNo + '</td>' +
                      '</tr>' +
                      '<tr>' +
                          '<td>Name:</td>' +
                          '<td>' + $headerData.Name + '</td>' +
                          '<td data-content="section-header">Section:</td>' +
                          '<td data-content="section">';
      html += $headerData.Section == null ? "---" : $headerData.Section;
      html += '</td>' +
     '<td data-content="status-header">Status:</td>' +
     '<td data-content="status">' + $headerData.Status + '</td>' +
     '<td>Date:</td>' +
     '<td>' + $headerData.ReceiptDate.DateWCF().format('d/m/Y') + '</td>' +
 '</tr>' +
                      '<tr>' +
                          '<td>Payment Mode:</td>' +
                          '<td colspan="7">' + payModeDetails + '</td>' +
                      '</tr>' +
'</table>' +
'<table data-genre="' + $headerData.ReceiptGenre + '" class="feeDetails" border="1" cellpadding="2" cellspacing="0">' +
 '<thead>' +
     '<tr>' +
         '<td data-genre="' + $headerData.ReceiptGenre + 'Type">Fee Type</td>' +
         '<td data-content="installment-header" data-genre="' + $headerData.ReceiptGenre + 'Installment">Installment</td>' +
         '<td data-content="gross-header" data-genre="' + $headerData.ReceiptGenre + 'Gross">Gross Amount(Rs.)</td>' +
         '<td data-content="net-header" data-genre="' + $headerData.ReceiptGenre + 'Net">Net Amount(Rs.)</td>' +
         '<td data-content="code-header" data-genre="' + $headerData.ReceiptGenre + 'Code">Code</td>' +
         '<td data-content="tax-header" data-genre="' + $headerData.ReceiptGenre + 'Tax">Tax(Rs.)</td>' +
         '<td data-content="coupon-header" data-genre="' + $headerData.ReceiptGenre + 'Coupon">Coupon Amount</td>' +
         '<td align="right" width="150" data-genre="' + $headerData.ReceiptGenre + 'Total">Total Amount(Rs.)</td>' +
     '</tr>' +
     '</thead>' +
 '<tbody>';
      for (var j = 0; j < $gridData.length; j++) {
          couponAmount = ($gridData[j].NetAmount - $headerData.TotalAmount);
          html += '<tr>' +
                      '<td data-genre="' + $headerData.ReceiptGenre + 'Type">' + $gridData[j].FeeHeadName + '</td>' +
                      '<td data-content="installment" data-genre="' + $headerData.ReceiptGenre + 'Installment">' + (($gridData[j].IsAdvance) ? 'Advance' : ($gridData[j].InstallmentName == null || $gridData[j].InstallmentName == 0 ? '---' : $gridData[j].InstallmentName)) + '</td>' +
                      '<td data-content="gross" data-genre="' + $headerData.ReceiptGenre + 'Gross">' + TssLib.indCurFlt($gridData[j].GrossAmount) + '</td>' +
                      '<td data-content="net" data-genre="' + $headerData.ReceiptGenre + 'Net">' + TssLib.indCurFlt($gridData[j].NetAmount) + '</td>' +
                      '<td data-content="code" data-genre="' + $headerData.ReceiptGenre + 'Code">' + $gridData[j].FeeHeadCode + '</td>' +
                      '<td data-content="tax" data-genre="' + $headerData.ReceiptGenre + 'Tax">' + TssLib.indCurFlt($gridData[j].Tax) + '</td>' +
                      '<td data-content="coupon" data-genre="' + $headerData.ReceiptGenre + 'Coupon">' + TssLib.indCurFlt(couponAmount) + '</td>' +
                      '<td align="right" data-genre="' + $headerData.ReceiptGenre + 'Total">' + TssLib.indCurFlt($gridData[j].NetAmount+ $gridData[j].Tax) + '</td>' +
                  '</tr>';
      }
      html += '</tbody>' +
              '</table>' +
              '<table class="total" border="0" cellpadding="4" cellspacing="0">' +
              '<tr>' +
              '<td colspan="6">' + $headerData.AmountInWords + '</td>' +
              '<td align="right" width="150">' + 'Rs.' + ' ' + TssLib.indCurFlt($headerData.TotalAmount) + '</td>' +
              '</tr>' +
              (!TssLib.isBlank($headerData.CouponCode)?(
              '<td colspan="7" align="right">Coupon Applied: ' + $headerData.CouponCode + ', Rs.' + TssLib.indCurFlt($headerData.CouponAmount) + '</td>'
              ):'') +
              '<tr>' +
              (TssLib.isBlank($headerData.Remarks) ? '' : ('<td colspan="7">Remark: ' + $headerData.Remarks + '</td>')) +
              '</tr>' +
              '</table></div>';
      $(".print_reciept").append(html);

      var contentArray = [];
      contentArray.push("section");
      contentArray.push("status");
      contentArray.push("installment");
      contentArray.push("gross");
      contentArray.push("net");
      contentArray.push("code");
      contentArray.push("tax");

      if (couponAmount == 0 || $headerData.ReceiptGenre != "Application") {
          $('[data-content="coupon-header"], [data-content="coupon"]').remove();
      }
      if ($headerData.ReceiptGenre == "Application") {
          $('[data-genre="' + $headerData.ReceiptGenre + 'Installment"], [data-genre="' + $headerData.ReceiptGenre + 'Code"], [data-genre="' + $headerData.ReceiptGenre + 'Net"]').remove()
      }
      if ($('[data-content="gross"]').text() == $('[data-content="net"]').text()) {
          //$('[data-genre="' + $headerData.ReceiptGenre + 'Gross"], [data-genre="' + $headerData.ReceiptGenre + 'Net"]').remove();
      }

      $.each(contentArray, function (i, o) {
          if ($('[data-content="' + o + '"]').text() == "") {
              $('[data-content="' + o + '-header"], [data-content="' + o + '"]').remove();
          }
      });
      $('[data-content="code-header"], [data-content="code"]').remove();
  }

    
  function buildGridWithHeaderGroup(id,refData) {
      var colNames = [];
      var groupHeaders = [];
      var array = []
      var title = '';
      var colModal = [];
      var i = 0;
      $.each(refData[0], function (key, value) {
          if (i == 0)
              colModal.push({ name: key, index: key, hidden: true });
          else
              colModal.push({ name: key, index: key });

          var flag = true;
          if (key.indexOf('_') > 0) {
              title = key.split('_')[0];
              array.push(key);
              colNames.push(key.split('_')[1]);
          } else {
              colNames.push(key);
              flag = false;
          }
          if (array.length > 0 && !flag) {
              showLog(flag);
              var _obj = {};
              _obj.startColumnName = array[0];
              _obj.numberOfColumns = array.length;
              _obj.titleText = title;
              groupHeaders.push(_obj);
              title = '';
              array = [];
              flag = true;
          }
          i++;

      });
      if (array.length > 0) {
          var _obj = {};
          _obj.startColumnName = array[0];
          _obj.numberOfColumns = array.length;
          _obj.titleText = title;
          groupHeaders.push(_obj);
      }
      var $refTblgrid = $('#' + id);
      try {
          $refTblgrid.jqGrid('GridUnload');
          $refTblgrid = $('#' + id);
      }
      catch (e) { }
      $refTblgrid.jqGrid({
          data: refData,
          datatype: 'local',
          shrinkToFit: false,
          colNames: colNames,
          colModel: colModal,
      });
      $refTblgrid.jqGrid('setGroupHeaders', {
          useColSpanStyle: true,
          groupHeaders: groupHeaders
      });
  }