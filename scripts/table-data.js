// JavaScript Document JqGrid JSON Data

//{name:'name',index:'name', width:90,sortable:false},
//			{name:'campaign',index:'campaign', width:100, align:"center"},
//			{name:'audience',index:'audience', width:100, align:"center"},
//			{name:'from',index:'from', width:100, align:"center"},
//			{name:'to',index:'to', width:100, align:"center"},
//			{name:'description',index:'description', width:100, align:"center"},
//			{name:'actions',index:'actions', width:100, align:"center", formatter:scListActionFmatter}


var budgetTable;
var itemTable;
var emailDatabase;
var preEventData = [];
var feeHeadData = [];
var feeStreamTblData;
var feestructructuretbldata;

var eventCampaignParticularsList;
var eventDetails = [
		{ row1: "Template 1", row2: "Road show", row3: "Public", row4: "2,55,345", row5: "3,28,232", row6: "Conduct", row7: "Conduct" }
];
// list actions
function scListActionFmatter(cellvalue, options, rowObject) {
    var returnHtml = "<a class='act-btn oasis-edit' onclick='LoadModalPopup(edit_modal)' data-toggle='modal' data-target='.bs-example-modal' href='javascript:;'><i class='glyphicon glyphicon-pencil'></i></a><a  class='act-btn oasis-delete confirm-delete' href='javascript:;'onclick='LoadModalPopup(oakCustomDeletePopup)' data-toggle='modal' data-target='.bs-example-modal'  data-message='Are you sure you want to delete this user ?'><i class='glyphicon glyphicon-trash'></i></a>";
    return returnHtml;
}

function templateActionFmatter(cellvalue, options, rowObject) {
    var returnHtml = "<a class='act-btn oasis-edit'  href='javascript:;'><i class='glyphicon glyphicon-pencil'></i></a><a  class='act-btn oasis-delete confirm-delete' href='javascript:;'onclick='LoadModalPopup(oakCustomDeletePopup)' data-toggle='modal' data-target='.bs-example-modal'  data-message='Are you sure you want to delete this user ?'><i class='glyphicon glyphicon-trash'></i></a>";
    return returnHtml;
}


function budgetActionFmatter(cellvalue, options, rowObject) {
    var returnHtml = "<a class='act-btn oasis-edit' onclick='LoadModalPopup(budget_edit_modal)' data-toggle='modal' data-target='.bs-example-modal' href='javascript:;'><i class='glyphicon glyphicon-pencil'></i></a><a  class='act-btn oasis-delete confirm-delete' href='javascript:;'onclick='LoadModalPopup(oakCustomDeletePopup)' data-toggle='modal' data-target='.bs-example-modal'  data-message='Are you sure you want to delete this user ?'><i class='glyphicon glyphicon-trash'></i></a>";
    return returnHtml;
}

function eventCampaignActionFmatter(cellvalue, options, rowObject) {
    //showLog(options);
    var returnHtml = "<a class='act-btn oasis-edit' href='javascript:;'><i class='glyphicon glyphicon-pencil'></i></a><a  class='act-btn oasis-delete confirm-delete' href='javascript:;'onclick='LoadModalPopup(oakCustomDeletePopup)' data-toggle='modal' data-target='.bs-example-modal'  data-message='Are you sure you want to delete this user ?'><i class='glyphicon glyphicon-trash'></i></a>";
    return returnHtml;
}

function feeHeadActionFmatter(cellvalue, options, rowObject) {
    //showLog(options);
    var returnHtml = "<a class='act-btn oasis-edit' onclick='LoadModalPopup(budget_edit_modal)' data-toggle='modal' data-target='.bs-example-modal' href='javascript:;'><i class='glyphicon glyphicon-pencil'></i></a><a  class='act-btn oasis-delete confirm-delete' href='javascript:;'onclick='LoadModalPopup(oakCustomDeletePopup)' data-toggle='modal' data-target='.bs-example-modal'  data-message='Are you sure you want to delete this user ?'><i class='glyphicon glyphicon-trash'></i></a>";
    return returnHtml;
}

//Backup list status
function backupListStatusFmatter(cellvalue, options, rowObject) {
    var returnHtml = "<span class='label label-success label-mini'>Active</span>";
    return returnHtml;
}
//<author>Srinivas</author>
//Create Date 7/08/2014
//Srinivas block Start
function CmsActionFmatterTextField(cellvalue, options, rowObject) {
    var returnHtml = "<input type='text' id='txtAmount' name='Amountpaid' /><span style='display:none; color:red'>Enter Amount</span>";

    return returnHtml;
}
function CmsActionFmatterButton(cellvalue, options, rowObject) {
    //var returnHtml = "<input type='button' id='btnReversal' />";
    var returnHtml = " <button class='button-common' type='submit' id='btnReversal'>Reversal</button>";
   
    return returnHtml;
}

function CmsActionFormatterButtons(cellvalue, options, rowObject) {
    var returnHtml = "<a  class='act-btn oasis-delete confirm-delete' href='javascript:;'onclick='LoadModalPopup(oakCustomDeletePopup)' data-toggle='modal' data-target='.bs-example-modal'  data-message='Are you sure you want to delete this user ?'><i class='glyphicon glyphicon-trash'></i></a><a class='act-btn oasis-edit'  href='javascript:;'><i class='glyphicon glyphicon-pencil'></i></a>";
    return returnHtml;
}
//Srinivas Block End

var $generatedLeadsgrid = "";
var $targetAudienceHomeGrid = "";

$(document).ready(function () {

    var tempData = { id: 34, itemName: 'Test Data', noOfUnits: 1, CostPerUnit: 45, Total: 123 };
    //EventCampaignParticulars grid	
    //<author>Debaprasad</author>
    $("#tblEventCampaignParticulars").jqGrid({
        autowidth: true,
        data: eventCampaignParticularsList,
        datatype: "local",
        rowList: [10],
        multiselect: false,
        colNames: ['Id', 'Item Name', 'No Of Units', 'Units Price', 'Total', 'Actions'],
        colModel: [
             { name: 'CampaignId', index: 'CampaignId', width: 10, sortable: false, align: "center", hidden: true },
            { name: 'itemName', index: 'Item Name', width: 150, sortable: false, align: "left" },
            { name: 'noOfUnits', index: 'No Of Units', width: 130, align: "left" },
            { name: 'CostPerUnit', index: 'Units Price', width: 120, align: "left" },
            { name: 'Total', index: 'Total', width: 80, align: "left" },
            { name: 'actions', index: 'actions', width: 100, align: "right", formatter: eventCampaignActionFmatter }
        ],

        pager: "tblEventCampaignParticulars-plist",
        caption: ""
    });

    //EventCampaignParticulars grid ends


    //Fee Structure grid	
    //<author>Debaprasad</author>
    $("#tblFeeStructure").jqGrid({
        autowidth: true,
        data: feestructructuretbldata,
        datatype: "local",
        multiselect: false,
        colNames: ['Class', 'Fee Stream', 'Student Type', 'Fee Payment Type', 'Free Head', 'Installment', 'Amount', 'Total Amount', 'Actions'],
        colModel: [
             { name: 'class', index: 'class', width: 50, sortable: false, align: "center" },
             { name: 'feestream', index: 'feestream', width: 80, sortable: false, align: "center" },
             { name: 'studenttype', index: 'studenttype', width: 90, sortable: false, align: "center" },
             { name: 'feepaymenttype', index: 'feepaymenttype', width: 120, sortable: false, align: "center" },
            { name: 'freehead', index: 'freehead', width: 80, sortable: false, align: "left" },
            { name: 'installment', index: 'installment', width: 80, align: "left" },
            { name: 'amount', index: 'amount', width: 70, align: "left" },
            { name: 'totalaamount', index: 'totalaamount', width: 100, align: "left" },
            { name: 'actions', index: 'actions', width: 100, align: "right", formatter: eventCampaignActionFmatter }

        ],

        pager: "tblFeeStructure-plist",
        caption: ""
    });

    //Fee Structure  grid ends



    //Fee Steam Grid of CMS Module Starts
    //<author>Debaprasad</author>
    var $feeStreamTblgrid = $("#feeStreamTbl");
    $feeStreamTblgrid.jqGrid({
        data: feeStreamTblData,
        datatype: "local",
        colNames: ['Name', 'Campus', 'Curriculum', 'Class', 'Actions'],
        colModel: [
            { name: 'name', index: 'name', width: 60, sortable: false, align: "left" },
            { name: 'campus', index: 'campus', width: 80, align: "left" },
            { name: 'curriculum', index: 'curriculum', width: 100, align: "left" },
            { name: 'class', index: 'class', width: 250, align: "left" },
            { name: 'actions', index: 'actions', width: 100, align: "left" }
        ],
        pager: "feeStreamTbl-plist",
        caption: ""
    });
    //Fee Strem Grid of CMS Module ends


    //Fee Head Grid of CMS Module Starts
    //<author>Debaprasad</author>
    var $feeHeadTblgrid = $("#feeHeadTbl");
    $feeHeadTblgrid.jqGrid({
        data: feeHeadData,
        datatype: "local",
        colNames: ['Name', 'Code', 'Company Type', 'Classification', 'Fee Head Category', 'Ledger Account', 'Tax Applicable', 'Actions'],
        colModel: [
            { name: 'EventId', index: 'EventId', width: 60, sortable: false, align: "left" },
            { name: 'TemplateName', index: 'TemplateName', width: 80, align: "left", sortable: false },
            { name: 'CampaignMode', index: 'CampaignMode', width: 90, align: "left" },
            { name: 'TargetAudience', index: 'TargetAudience', width: 100, align: "left" },
            { name: 'BudgetFrom', index: 'BudgetFrom', width: 100, align: "left" },
            { name: 'BudgetTo', index: 'BudgetTo', width: 100, align: "left" },
            { name: 'Description', index: 'Description', width: 100, align: "left" },
            { name: 'actions', index: 'actions', width: 100, align: "left" }
        ],
        pager: "feeHeadTbl-plist",
        caption: ""
    });
    //Fee Head Grid of CMS Module ends

 

    //group info grid ends
    //target audience tab grid



    //Target Audience info grid
    //<author>Debaprasad</author>
    var $eventDetailsTblgrid = $("#eventDetailsTbl");
    $eventDetailsTblgrid.jqGrid({
        width: 'auto',
        data: eventDetails,
        datatype: "local",
        colNames: ['Column', 'Column', 'Column', 'Column', 'Column', 'Column', 'Column'],
        colModel: [
			{ name: 'row1', index: 'row1', width: 100, sortable: false, align: "left" },
			{ name: 'row2', index: 'row2', width: 100, align: "left" },
			{ name: 'row3', index: 'row3', width: 100, align: "center" },
            { name: 'row4', index: 'row4', width: 100, align: "left" },
			{ name: 'row5', index: 'row5', width: 100, align: "center" },
		    { name: 'row6', index: 'row6', width: 100, align: "center" },
		    { name: 'row7', index: 'row7', width: 100, align: "center" },
        ],
        pager: "eventDetailsTbl-plist"
    });

    //Target Audience info grid ends

    //This Grid Has been used in Template page of CRM Module
    //<author>Debaprasad</author>
    //Item info grid
    var $TemplateTblgrid = $("#TemplateTbl");
    $TemplateTblgrid.jqGrid({
        data: itemTable,
        datatype: "local",
        colNames: ['ID', 'Item Name', 'No. of Units', 'Unit Measurement', 'Description', 'From', 'To', 'Quantity', 'Actions'],
        colModel: [
            { name: 'id', index: 'id', width: 100, sortable: false, align: "center", hidden: true },
			{ name: 'itemName', index: 'itemName', width: 100, sortable: false, align: "left" },
			{ name: 'noOfUnits', index: 'noOfUnits', width: 100, align: "left" },
			{ name: 'unitMeasurement', index: 'unitMeasurement', width: 100, align: "center" },
            { name: 'description', index: 'description', width: 100, align: "left" },
			{ name: 'unitPriceFrom', index: 'unitPriceFrom', width: 100, align: "center" },
		    { name: 'unitPriceTo', index: 'unitPriceTo', width: 100, align: "center" },
            { name: 'quantity', index: 'quantity', width: 100, align: "center", hidden: true },
			{ name: 'actions', index: 'actions', width: 100, align: "right", formatter: templateActionFmatter }
        ],
        pager: "TemplateTbl-plist",
        caption: ""
    });
    $TemplateTblgrid.jqGrid('setGroupHeaders', {
        useColSpanStyle: true,
        groupHeaders: [
          { startColumnName: 'unitPriceFrom', numberOfColumns: 2, titleText: 'Unit Price' }
        ]
    });


    //Company Table
    //This Grid was used in Company page of CMS Module
    //Create Date 6/08/2014
    //<author>Srinivas</author>
    
    var CompanyTblData =
    [
        { Id: '1', Name: 'Oakridge InternationalSchool', Code: 'IOS', Type: 'Society' },
        { Id: '2', Name: 'Oakridge InternationalSchool', Code: 'IOS', Type: 'Limited' },
        { Id: '3', Name: 'Oakridge InternationalSchool', Code: 'IOS', Type: 'Asset' },
        { Id: '4', Name: 'Oakridge InternationalSchool', Code: 'IOS', Type: 'Society' },
        { Id: '5', Name: 'Oakridge InternationalSchool', Code: 'IOS', Type: 'Society' },
        { Id: '6', Name: 'Oakridge InternationalSchool', Code: 'IOS', Type: 'Limited' },
   ]
    var $CompanyTblgrid = $("#CompanyTbl");
     $CompanyTblgrid.jqGrid({
         data: CompanyTblData,
        datatype: "local",
        colNames: ['ID', 'Name', 'Code', 'Type', 'Actions'],
        colModel: [
             
            { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
			{ name: 'Name', index: 'Name', width: 100, sortable: false, align: "left" },
			{ name: 'Code', index: 'Code', width: 100, align: "left" },
			{ name: 'Type', index: 'Type', width: 100, align: "left" },
           
			{ name: 'actions', index: 'actions', width: 100, align: "left", formatter: templateActionFmatter }
        ],
        //pager: "CompanyTbl-plist",
        caption: ""
    });

   

    //FeeGroup Table
    //This Grid was used in FeeGroup page of CMS Module
    //Create Date 6/08/2014
    //<author>Srinivas</author>
     var FeeGroupTblData =
    [
        { Id: '1', Name: 'Normal', Code: 'Nor' },
       { Id: '2', Name: 'General', Code: 'Gen' },
        { Id: '3', Name: 'Normal', Code: 'Nor' },
        { Id: '4', Name: 'General', Code: 'Gen' },
        { Id: '5', Name: 'Normal', Code: 'Nor' },
        { Id: '6', Name: 'General', Code: 'Gen' },
    ]
     var $FeeGroupTblgrid = $("#FeeGroupTbl");
      $FeeGroupTblgrid.jqGrid({
          data: FeeGroupTblData,
         datatype: "local",
         colNames: ['ID', 'Name', 'Code', 'Actions'],
         colModel: [

             { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
             { name: 'Name', index: 'Name', width: 100, sortable: false, align: "left" },
             { name: 'Code', index: 'Code', width: 100, align: "left" },
            

             { name: 'actions', index: 'actions', width: 100, align: "left", formatter: templateActionFmatter }
         ],
          //pager: "FeeGroupTbl-plist",
         caption: ""
     });

    
    //OrganizationTable
    //This Grid was used in Organization page of CMS Module
    //Create Date 6/08/2014
    //<author>Srinivas</author>

      var OrganizationTblData =
    [
        { Id: '1', Name: 'IOS', Full_Name: 'Oakridge InternationalSchool' },
        { Id: '2', Name: 'OSB', Full_Name: 'Oakridge InternationalSchool' },
        { Id: '3', Name: 'ACC', Full_Name: 'Oakridge InternationalSchool' },
        { Id: '4', Name: 'APC', Full_Name: 'Oakridge InternationalSchool' },
        { Id: '5', Name: 'ERT', Full_Name: 'Oakridge InternationalSchool' },
        { Id: '6', Name: 'EVR', Full_Name: 'Oakridge InternationalSchool' },
        { Id: '7', Name: 'ORM', Full_Name: 'Oakridge InternationalSchool' },
    ]
      var $OrganizationTblgrid = $("#OrganizationTbl");
      $OrganizationTblgrid.jqGrid({
          data: OrganizationTblData,
          datatype: "local",
          colNames: ['ID', 'Name', 'Full Name', 'Actions'],
          colModel: [

              { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
              { name: 'Name', index: 'Name', width: 50, sortable: false, align: "left" },
              { name: 'Full_Name', index: 'Full_Name', width: 100, align: "left" },

              { name: 'actions', index: 'actions', width: 100, align: "left", formatter: templateActionFmatter },
          ],
          //pager: "OrganizationTbl-plist",
          caption: ""
      });


    //Fee Head Category
    //This Grid was used in FeeHeadCategory page of CMS Module
    //Create Date 6/08/2014
    //<author>Srinivas</author>
      var FeeHeadCategoryTblgridData =
     [
         { Id: '1', Name: 'Normal', Code: 'Nor' },
         { Id: '2', Name: 'General', Code: 'Gen' },
         { Id: '3', Name: 'Normal', Code: 'Nor' },
         { Id: '4', Name: 'General', Code: 'Gen' },
         { Id: '5', Name: 'Normal', Code: 'Nor' },
         { Id: '6', Name: 'General', Code: 'Gen' },
     ]
      var $FeeHeadCategoryTblgrid = $("#FeeHeadCategoryTbl");
      $FeeHeadCategoryTblgrid.jqGrid({
          data: FeeHeadCategoryTblgridData,
          datatype: "local",
          colNames: ['ID', 'Name', 'Code', 'Actions'],
          colModel: [

              { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
              { name: 'Name', index: 'Name', width: 100, sortable: false, align: "left" },
              { name: 'Code', index: 'Code', width: 100, align: "left" },
              { name: 'actions', index: 'actions', width: 100, align: "left", formatter: templateActionFmatter }
          ],
          //pager: "FeeHeadCategoryTbl-plist",
          caption: ""
      });

    //FeeCollection Table
    //This Grid was used in FeeCollection page of CMS Module
    //Create Date 6/08/2014
    //<author>Srinivas</author>
      var FeeCollectionTblData =
     [
         
         { Id: '1', CompanyType:'Society', FeeHead: 'Tutions', Installment: '4', Amount:'25,656'},
         { Id: '2', CompanyType: 'Limited', FeeHead: 'Admission', Installment: '3', Amount: '25,897'},
         { Id: '3', CompanyType: 'Society', FeeHead: 'Advance', Installment: '2', Amount: '34,567' },
         { Id: '4', CompanyType: 'Limited', FeeHead: 'Examination', Installment: '4', Amount: '23,456' },
         { Id: '5', CompanyType: 'Society', FeeHead: 'Annual', Installment: '4', Amount: '25,678' },
     ]
      var $FeeCollectionTblgrid = $("#FeeCollectionTbl");
          $FeeCollectionTblgrid.jqGrid({
              data: FeeCollectionTblData,
          datatype: "local",
          colNames: ['ID', 'CompanyType', 'FeeHead','Installment','Amount', 'AmountPaid'],
          colModel: [

              { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
              { name: 'CompanyType', index: 'CompanyType', width: 100, sortable: false, align: "left" },
              { name: 'FeeHead', index: 'FeeHead', width: 100, align: "left" },
              { name: 'Installment', index: 'Installment', width: 100, align: "left" },
              { name: 'Amount', index: 'Amount', width: 100, align: "left" },
              { name: 'AmountPaid', index: 'AmountPaid', width: 100, align: "left", formatter: CmsActionFmatterTextField },
          ],
          //pager: "FeeHeadCategoryTbl-plist",
          caption: ""
          });

  
    //Fee Collections Table
    //This Grid was used in FeeCollections page of CMS Module
    //Create Date 7/08/2014
    //<author>Srinivas</author>
          var FeeCollectionsTblData =
         [

             { Id: '1', EnrollmentNo: 'OSI2011', Date: '25 jan 2014', ReceiptMode: 'Normal', Amount: '25,656' },
             { Id: '2', EnrollmentNo: 'OSI2011', Date: '25 jan 2014', ReceiptMode: 'Online', Amount: '25,656' },
             { Id: '3', EnrollmentNo: 'OSI2011', Date: '25 jan 2014', ReceiptMode: 'Online', Amount: '25,656' },
             { Id: '4', EnrollmentNo: 'OSI2011', Date: '25 jan 2014', ReceiptMode: 'Online', Amount: '84,656' },
             { Id: '5', EnrollmentNo: 'OSI2011', Date: '25 jan 2014', ReceiptMode: 'Online', Amount: '45,656' },

         ]
          var $FeeCollectionsTblgrid = $("#FeeCollectionsTbl");
          $FeeCollectionsTblgrid.jqGrid({
              data: FeeCollectionsTblData,
              datatype: "local",
              colNames: ['ID', 'Enrollment No', 'Date', 'Receipt Mode', 'Amount',''],
              colModel: [

                  { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
                  { name: 'EnrollmentNo', index: 'EnrollmentNo', width: 100, sortable: false, align: "left" },
                  { name: 'Date', index: 'Date', width: 100, align: "left" },
                  { name: 'ReceiptMode', index: 'ReceiptMode', width: 100, align: "left" },
                  { name: 'Amount', index: 'Amount', width: 100, align: "left"},
                 { name: '', index: '', width: 100, align: "left", formatter: CmsActionFmatterButton },
              ],
              //pager: "FeeCollectionsTbl-plist",
              caption: ""
          });

    //OTF Fee Structure
    //This Grid was used in OTFFeestructureTbl page of CMS Module
    //Create Date 8/08/2014
    //<author>Srinivas</author>
          var OTFFeestructureTblData =
             [

                 { Id: '1', Class: 'V', Company: 'OSIV', FeeHead: 'Tution', Amount: '25,656' },
                 { Id: '2', Class: 'VI', Company: 'OSIV', FeeHead: 'Tution', Amount: '25,656' },
                 { Id: '3', Class: 'VII', Company: 'OSIV', FeeHead: 'Tution', Amount: '25,656' },
                 { Id: '4', Class: 'VIII', Company: 'OSIV', FeeHead: 'Tution', Amount: '84,656' },
                 { Id: '5', Class: 'X', Company: 'OSIV', FeeHead: 'Tution', Amount: '45,656' },

             ]
          var $OTFFeestructureTblgrid = $("#OTFFeestructureTbl");
          $OTFFeestructureTblgrid.jqGrid({
              data: OTFFeestructureTblData,
              datatype: "local",
              colNames: ['ID', 'Class', 'Company', 'Fee Head', 'Amount'],
              colModel: [

                  { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
                  { name: 'Class', index: 'Class', width: 100, sortable: false, align: "left" },
                  { name: 'Company', index: 'Company', width: 100, align: "left" },
                  { name: 'FeeHead', index: 'FeeHead', width: 100, align: "left" },
                  { name: 'Amount', index: 'Amount', width: 100, align: "left" },
                  //{ name: '', index: '', width: 100, align: "left", formatter: CmsActionFormatterButtons },
              ],
              //pager: "FeeCollectionsTbl-plist",
              caption: ""
          });




    //OTF Fee StructureList
    //This Grid was used in FeeCollections page of CMS Module
    //Create Date 8/08/2014
    //<author>Srinivas</author>OTFFeestructurelistPopup
          var OTFFeestructurelistTblData =
             [

                 { Id: '1', JoiningClass: 'V', FeeStream: 'OSIV', StudentType: 'Tution', Amount: '25,656' },
                 { Id: '2', JoiningClass: 'VI', FeeStream: 'OSIV', StudentType: 'Tution', Amount: '25,656' },
                 { Id: '3', JoiningClass: 'VII', FeeStream: 'OSIV', StudentType: 'Tution', Amount: '25,656' },
                 { Id: '4', JoiningClass: 'VIII', FeeStream: 'OSIV', StudentType: 'Tution', Amount: '84,656' },
                 { Id: '5', JoiningClass: 'X', FeeStream: 'OSIV', StudentType: 'Tution', Amount: '45,656' },

             ]
          var $OTFFeestructurelistTblgrid = $("#OTFFeestructurelistTbl");
          $OTFFeestructurelistTblgrid.jqGrid({
              data: OTFFeestructurelistTblData,
              datatype: "local",
              colNames: ['ID', 'Joining Class', 'Fee Stream', 'Student Type', 'Amount'],
              colModel: [

                  { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
                  { name: 'JoiningClass', index: 'JoiningClass', width: 100, sortable: false, align: "left" },
                  { name: 'FeeStream', index: 'FeeStream', width: 100, align: "left" },
                  { name: 'StudentType', index: 'StudentType', width: 100, align: "left" },
                  { name: 'Amount', index: 'Amount', width: 100, align: "left" },
                  //{ name: '', index: '', width: 100, align: "left", formatter: CmsActionFormatterButtons },
              ],
              //pager: "FeeCollectionsTbl-plist",
              caption: ""
          });


    //tblpopup
    //OTF Fee StructureList
    //This Grid was used in OTFFeestructurelistpopup page of CMS Module
    //Create Date 8/08/2014
    //<author>Srinivas</author>
          var OTFFeestructurelistPopupData =
             [

                 { Id: '1', Class: 'V', Company: 'OSIV', FeeHead: 'Tution', Amount: '25,656' },
                 { Id: '2', Class: 'VI', Company: 'OSIV', FeeHead: 'Tution', Amount: '25,656' },
                 { Id: '3', Class: 'VII', Company: 'OSIV', FeeHead: 'Tution', Amount: '25,656' },
                 { Id: '4', Class: 'VIII', Company: 'OSIV', FeeHead: 'Tution', Amount: '84,656' },
                 { Id: '5', Class: 'X', Company: 'OSIV', FeeHead: 'Tution', Amount: '45,656' },

             ]
          var $OTFFeestructurelistPopupgrid = $("#OTFFeestructurelistPopup");
          $OTFFeestructurelistPopupgrid.jqGrid({
              data: OTFFeestructurelistPopupData,
              datatype: "local",
              colNames: ['ID', 'Class', 'Company', 'Fee Head', 'Amount'],
              colModel: [

                  { name: 'Id', index: 'Id', width: 100, sortable: false, align: "left", hidden: true },
                  { name: 'Class', index: 'Class', width: 100, sortable: false, align: "left" },
                  { name: 'Company', index: 'Company', width: 100, align: "left" },
                  { name: 'FeeHead', index: 'FeeHead', width: 100, align: "left" },
                  { name: 'Amount', index: 'Amount', width: 100, align: "left" },
                  //{ name: '', index: '', width: 100, align: "left", formatter: CmsActionFormatterButtons },
              ],
              //pager: "FeeCollectionsTbl-plist",
              caption: ""
          });
   
});



