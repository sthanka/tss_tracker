<?php

if(count($department)>0){ ?>
<div class="col-sm-12" style="padding-left: 0px; font-size: 12px;padding-top: 10px;">
    <table id="departmentTable">
        <thead>
        <tr>
            <th>Department</th>
            <th>Estimated Time</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach($department as $k=>$v){ ?>
            <tr>
                <td><span ><?= $v['department_name'] ?></span></td>
                <td><input data-id="<?=$v['department_id'] ?>" type="text" class="department msk_time" value="<?php if(isset($v['estimated_time'])){ echo $v['estimated_time']; }?>"></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
    <script>
        if($globalDs){
            $globalDs = false;
            $('#departmentTable .msk_time').attr('disabled', true);
        }else{
            $globalDs = false;
            $('#departmentTable .msk_time').attr('disabled', false);
        }
    </script>
<?php } else { ?>
    No Department added
<?php } ?>