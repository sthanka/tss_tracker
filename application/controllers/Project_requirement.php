<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_Requirement extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Project_modal');
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
    }

    public function index()
    {
        if($this->session->userdata('user_id'))
        {
            $data['content'] = 'project/index';
            $this->load->view('partials/landing',$data);
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }
    public function uploadRequirement()
    {
        if(isset($_POST) && isset($_FILES) && isset($_FILES['file']) && $_FILES['file']['size']>0)
        {
            $data = json_decode(file_get_contents("php://input"), true);
            if($data){ $_POST = $data; }
            $data = $this->input->post();
            if(empty($data)){
                $result = array('status'=>FALSE,'error'=>true,'message'=>'Invalid Data');
                echo json_encode($result);
            }
            if(checkInValidFleExtensaion($_FILES['file'])){
                $result = array('status'=>FALSE,'error'=>true,'message'=>'Invalid File Format');
                echo json_encode($result);exit;
            }
            $isUnique = $this->Project_modal->isUniqueRequirementName(array('requirement_name'=>$data['requirement_name'], 'project_id' => $data['project_id']));
            if(count($isUnique)>0){
                $result = array('status'=>FALSE,'error'=>true,'message'=>'Requirement Name already exist');
                echo json_encode($result);exit;
            }
            $file_name = doUpload($_FILES['file']['tmp_name'],$_FILES['file']['name'],'uploads/');
            $this->Project_modal->addRequirementDoc(array(
                'requirement_name' => $data['requirement_name'],
                /*'comments' => $data['comments'],*/
                'project_id' => $data['project_id'],
                'end_date' => DateTime::createFromFormat('d/m/Y', $data['end_date'])->format('Y-m-d'),
                'created_by' => $this->session->userdata('user_id'),
                'requirement_document' => $file_name
            ));

            //notification
            $team_leads = $this->Project_modal->getUsers(array(
                'project_id' => $data['project_id'],
                'is_lead' => 1
            ));
            $project_details = $this->Project_modal->getProjectDetails(array('project_id' => $data['project_id']));
            for($s=0;$s<count($team_leads);$s++)
            {
                $this->Project_modal->addNotification(array(
                    'user_id' => $team_leads[$s]['id_user'],
                    'subject' => 'Requirement',
                    'message' => 'New requirement uploaded for <b>'.$project_details[0]["project_name"].'</b> project',
                    'url' => WEB_BASE_URL.'index.php/project/',
                    'read_flag' => 1
                ));
            }

            echo json_encode(array('status'=>TRUE,'error'=>false,'data'=>array())); exit;
        }
        else
        {
            echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'Please select file', 'message'=> 'Please select file')); exit;
        }

    }

    public function getRequrementList()
    {
        $params = $this->input->get();
        if($this->session->userdata('user_id'))
        {
            $requirementList = $this->Project_modal->getRequrementListGrid($params);

            if($requirementList && isset($requirementList['rows'])){
                foreach($requirementList['rows'] as $key=>$val){
                    $total = 0;
                    $estimated_time = 0;
                    $log_time = 0;
                    $last_task_complition_date = ' -- ';
                    $res = $this->Project_modal->requirementTaskTotal(array('requirement_id'=>$val->id_requirements));
                    $additional_time = $this->Project_modal->getAdditionalTimeByRequirement(array('requirement_id'=>$val->id_requirements));
                    $additional_time = $additional_time[0]['additional_time'];
                    if(!empty($res)){
                        $total = $res[0]['total'];
                        $estimated_time = $res[0]['estimated_time'];
                        $id_project_task = $res[0]['id_project_task'];
                        $temp=0;
                        foreach($res as $k=>$v){
                            $taskFlow = null;
                            if(!is_null($id_project_task))
                                $taskFlow = $this->Project_modal->getUsersTaskDetailsByTaskId(array('project_task_id'=>$id_project_task));
                            $temp=0;
                            if(!empty($taskFlow)){
                                foreach($taskFlow as $k1=>$v1){
                                    if(!is_null($v1['actual_end_date'])){
                                        if(strtotime($v1['actual_end_date']) > strtotime($temp)){
                                            $temp=$v1['actual_end_date'];
                                        }
                                    }
                                }
                            }else{
                                if(!is_null($v['end_date'])){
                                    if(strtotime($v['end_date']) > strtotime($temp)){
                                        $temp=$v['end_date'];
                                    }
                                }
                            }
                            $last_task_complition_date=$temp;
                            $temp=0;
                        }

                        if(!empty($id_project_task) && $id_project_task!=''){
                            $logres = $this->Project_modal->getLogtime(array('project_task_id'=>$id_project_task));
                            if(!empty($logres)){
                                foreach($logres as $k=>$v){
                                    if($v['duration'])
                                        $log_time = $log_time + time_to_sec($v['duration']);
                                }
                            }
                        }
                    }
                    $temp = array(
                        'id_requirements'=>$val->id_requirements,
                        'project_id'=>$val->project_id,
                        'id_project_task'=>$id_project_task,
                        'requirement_name'=>$val->requirement_name,
                        'requirement_document'=>$val->requirement_document,
                        'comments'=>$val->comments,
                        'created_by'=>$val->created_by,
                        'end_date'=>date('d-M-Y',strtotime($val->end_date)),
                        'created_date_time'=>date('d-M-Y',strtotime($val->created_date_time)),
                        'e_date'=>$val->end_date,
                        'task_count'=>$total,
                        'tot_estimated_time'=>sec_to_time($estimated_time),
                        'additional_time'=>$additional_time,
                        'tot_log_time'=>sec_to_time($log_time),
                        'tot_remaining_time'=>sec_to_time(($estimated_time+time_to_sec($additional_time)) - $log_time),
                        'last_task_complition_date'=>$last_task_complition_date==0?' - ':date('d-M-Y',strtotime($last_task_complition_date)),
                        'created_date'=>date('d-M-Y',strtotime($val->created_date)),
                        'file_url'=>WEB_BASE_URL.'uploads/'.$val->requirement_document,
                    );
                    $requirementList['rows'][$key] = $temp;
                }
            }
            echo json_encode($requirementList);
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function deleteRequirement()
    {
        $data = $this->Project_modal->getRequirementTask($_POST);
        if(empty($data)){
            $this->Project_modal->deleteRequirement($_POST);
            echo json_encode(array('status'=>TRUE,'error'=>false,'data'=>array())); exit;
        }
        else{
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Task exists for this requirement')); exit;
        }

    }
}
