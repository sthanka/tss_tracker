<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Workload_model extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    public function getDepartmentProject()
    {
        $department_id = $this->session->userdata('department_id');
        $this->db->select('*');

        if( $this->session->userdata('user_type_id') == 3){
            $this->db->join('project_department pd', 'pd.project_id = p.id_project');
            $this->db->where('pd.department_id', $department_id);
        }

        $this->db->from('project p');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectTask($data)
    {

        $projectIds = $data['projectIds'];
        $departmentIds = $data['departmentIds'];
        $department_id  = $this->session->userdata('department_id');
        $this->db->select('CONCAT(u.first_name," ",u.last_name) as user_name,u.email,pt.*,pt.task_name as parent_name,d.department_name,d.id_department,pp.project_name,pw.*')
            ->join('project pp', 'pp.id_project = pt.project_id','left')
            ->join('task_workflow pw', 'pt.id_project_task=pw.project_task_id','left')
            ->join('user u', 'pw.assigned_to=u.id_user','left')
            ->join('department d', 'd.id_department = u.department_id','left')
            ->join('sprint s','s.id_sprint=(select sprint_id from project_task where id_project_task = pt.id_project_task)','left')
            ->where('YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1)')
            ->where('pw.status!="forward"')
            ->where('pw.status!="reject"')
            ->where('pw.status!="release"')
            ->where_in('pt.project_id', $projectIds);


        if($this->session->userdata('user_type_id') == 2){
            if(!empty($departmentIds))
                $this->db->where_in('u.department_id', $departmentIds);
        }
        if( $this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead')==1) {
            $this->db->where('u.department_id', $department_id);
            //$this->db ->where('pw.status!="completed"');
        }
        else if($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead')==0){
            $this->db->where('u.department_id', $department_id);
            $this->db->where('pw.assigned_to', $this->session->userdata('user_id'));
        }
        $this->db->from('project_task pt');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getDeptProjectTask($project_ids)
    {

        $department_id  = $this->session->userdata('department_id');
        $this->db->select('CONCAT(u.first_name," ",u.last_name) as user_name,u.email,pt.*,pt.task_name as parent_name,pw.*')
            ->join('task_workflow pw', 'pt.id_project_task=pw.project_task_id','left')
            ->join('user u', 'pw.assigned_to=u.id_user','left')
            ->join('sprint s','s.id_sprint=(select sprint_id from project_task where id_project_task = pt.id_project_task)','left')
            ->where('YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1)')
            ->where_in('pt.project_id', $project_ids)
            ->where('pw.status!="forward"')
            ->where('pw.status!="reject"')
            ->where('pw.status!="release"');
        if( $this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead')==1) {
            $this->db->where('u.department_id', $department_id);
        }

        $this->db->from('project_task pt');
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function createTaskWorkFlow($item)
    {

        $rowData = array(
            'project_task_id'=>$item['task_id'],
            'assigned_to'=>$item['assigned_to'],
            'estimated_time'=>$item['estimated_time'],
            'assigned_by'=>$this->session->userdata('user_id'),
        );
        $this->db->select('*')
            ->where('id_task_workflow',$item['id_task_workflow'])
            ->from('task_workflow');
        $query = $this->db->get();

        $taskArray = $query->row();
        $task_workflow_id = $taskArray->id_task_workflow;
        if($taskArray->assigned_to == $rowData['assigned_to']){
            $this->db->where('id_task_workflow',$taskArray->id_task_workflow);
            $this->db->update('task_workflow', $rowData);
        }else{
            $this->db->where('id_task_workflow',$taskArray->id_task_workflow);
            $this->db->update('task_workflow', array('status'=>'forward'));

            $this->db->insert('task_workflow', $rowData);
            $task_workflow_id = $this->db->insert_id();

        }

/*        echo "<pre>"; print_r($rowData);
echo "<pre>"; print_r($taskArray); exit;
        if(count($taskArray)==0){
            $task_workflow_id = $item['id_task_workflow'];
            $this->db->where('id_task_workflow',$task_workflow_id);
            $this->db->update('task_workflow', array('status'=>'forward'));

            $this->db->insert('task_workflow', $rowData);
            $task_workflow_id = $this->db->insert_id();
        }else{
            $task_workflow_id = $item['id_task_workflow'];
            $rowData['status'] ="forward";
            $this->db->where('id_task_workflow',$task_workflow_id);
            $this->db->update('task_workflow', $rowData);

            $this->db->where('id_task_workflow',$taskArray[0]['id_task_workflow']);
            $this->db->update('task_workflow',array('status'=>'pending'));
        }*/

       return  $task_workflow_id;
    }

    public function createTimeWorkFlow($data)
    {
        $workflowId = 0;
        if(isset($data[0]['task_workflow_id'])){
            $workflowIds = $data[0]['task_workflow_id'];
        }
        $this->db->where_in('task_workflow_id',$workflowIds);
        $this->db->delete('task_time_log');
        //echo $this->db->last_query();exit;

        $this->db->insert_batch('task_time_log', $data);
        return $this->db->insert_id();
    }
    public function getTimeWorkLoad($project_ids)
    {
        $this->db->select('*');
        $this->db->where('YEARWEEK(date,1) = YEARWEEK(NOW())');
        $this->db->where_in('project_id',$project_ids)->from('task_time_log');
        $result = $this->db->get();
        return $result->result_array();
    }
}