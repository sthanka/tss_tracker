<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!-- Modal Dialog  dekete confirmation-->
<div class="modal fade" id="oasis-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel panel-default panel-body padding20 clearfix">
                    <p></p>
                </div>
                <div class="panel-footer col-sm-12 clearfix text-right">
                    <button type="button" class="button-common module btn-danger">Yes</button>
                    <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Checklist</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Checklist List</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="client-list" class="table-responsive"></table>
                        <div id="client-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-client-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title"></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="checklist_name"  id="checklist_name" class="rval" />
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Checklist Name :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <label>Departments</label>
                                    <select multiple id="department_id" name="department_id" class="form-control rval"></select>
                                </div>
                            </div>
                            <input type="hidden" class="form-control rval" name="id_checklist" />
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        TssLib.docReady(function () {
            var $refTblgrid = $('#client-list');
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Check_list/getCheckListGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['Id', 'Name','comments'],
                colModel: [
                    { name: 'id_checklist', index: 'id_checklist', hidden: true ,key:true},
                    { name: 'checklist_name', index: 'checklist_name' },
                    { name: 'dept', index: 'dept', hidden: true },
                ],
                pager: 'client-list-page'
            }).navGrid('#client-list-page', {
                edit: true, add: true, del: true, search: false, refresh: true,
                addfunc: function () {
                    addEditCreateClient();
                }, editfunc: function (id) {
                    var rowData = $refTblgrid.jqGrid('getRowData', id);
                    addEditCreateClient(rowData);
                }, delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        getJsonAsyncWithBaseUrl("Check_list/deleteChecklist?id="+id, {}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data.status ) {
                                    $('#client-list').trigger('reloadGrid');
                                    TssLib.notify('Deleted successfully ', null, 5);
                                }else{
                                    TssLib.notify(result.data.message, 'warn', 5);
                                }
                            }
                        });
                    }, 'Yes', 'No');
                }
            });
        });
        function addEditCreateClient(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            postJsonAsyncWithBaseUrl("client/getClients", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        departmentsData = result.data;
                        TssLib.populateSelect($modal.find('#department_id'), { success: true, data: departmentsData.departments }, 'department_name', 'id_department');
                        if (TssLib.isBlank(rowData)) {
                            $modal.find('.panel-title').text('Add Checklist');
                            $modal.find('[div-submit="true"]').text('Save');
                            $modal.find('.status-drp').hide();
                        } else {
                            TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                            $modal.find('#department_id').select2("val",rowData.dept.split(','));
                            $modal.find('.panel-title').text('Edit Checklist');
                            $modal.find('[div-submit="true"]').text('Update');
                            $modal.find('.status-drp').hide();
                        }
                    }
                    else {

                    }
                }
            });

            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {
                    if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                    if (result.data.status) {
                        $('#client-list').trigger('reloadGrid');
                        TssLib.closeModal();
                        TssLib.notify('Checklist '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify('Checklist already exist ', 'warn', 5);
                    }
                },
                before: function (formObj) {
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Check_list/addChecklist');
                    return true;
                }
            });
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->


