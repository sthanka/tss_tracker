﻿// JavaScript Document   
(function ($) {
    var TssReportTable = function (element, options) {
        this.element = $(element);
        this.options = $.extend({
            YAxisData: [],
            XAxisData: [], 
            BodyData: [],
            BodyCellWidth:50,
            HaveOverView:false,
            OverViewWidth:100,
            YAxisLableWidth:100 
        }, options || {});

        // Public method - can be called from client code
        this.init = function () {
            var curObj = this; 
            curObj.element.addClass('oasis-report-table  clearboth clearfix');
            curObj.buildTable();
            curObj.scrollTable();
            return this;

        };

        this.scrollTable = function () {
            var curObj = this;
            var ele = curObj.element;
            var opt = curObj.options;
            var eleWidth = ele.width();
            var dedWidth = opt.OverViewWidth+2 + (opt.HaveOverView ? opt.OverViewWidth+2 : 0);
            var bodyEleLen=ele.find('.report-cells > table > thead > tr td').length;
            var bodyWidth = bodyEleLen * opt.BodyCellWidth + bodyEleLen+1;
            if (bodyWidth + dedWidth <= eleWidth) {
                ele.find('.report-cells').width(bodyWidth );
                ele.find('>table .mid-td').width(bodyWidth);
                ele.find('>table').width(bodyWidth + dedWidth);
            } else {
                ele.find('.report-cells').width(eleWidth - dedWidth+4);
                ele.find('.report-cells>table').width(bodyWidth+4);
                ele.find('>table').width(eleWidth);
                ele.find('.report-cells').css({
                    'overflow-x': 'auto' 
                });
                ele.find('.report-cells').mCustomScrollbar({ axis:"x", theme: "dark", scrollbarPosition: 'outside', scrollButtons: { enable: true }, scrollInertia: 3  });
            }
            return curObj;
        };

        this.buildTable = function () {
            var curObj = this; 
            var ele=curObj.element;
            var opt=curObj.options;
            var repTable=$('<table/>',{style:'table-layout:fixed',cellspacing:0,cellpadding:0});
            ele.append(repTable);
            var _tr=$('<tr/>');
            repTable.append(_tr);
            _tr.append($('<td/>', { style:'width:'+opt.YAxisLableWidth+'px;' }).append(curObj.buildLeftRightTdTable('YAxis-Label', opt.YAxisLableWidth)));
            _tr.append($('<td/>', { class:'mid-td'  }).append(curObj.buildMidTd('')));
            if(opt.HaveOverView){
                _tr.append($('<td/>', { style:'width:'+opt.OverViewWidth+'px;'  }).append(curObj.buildLeftRightTdTable('Overview-Label', opt.OverViewWidth)));
            }
            curObj.buildYAxisData(opt.YAxisData);
            curObj.buildXAxisData(opt.XAxisData);
            curObj.buildBodyData(opt.YAxisData, opt.XAxisData);
            return this;
        };
        this.buildYAxisData = function (data) {
            var curObj = this;
            var ele = curObj.element;
            var tbody = ele.find('.YAxis-Label tbody');
            var opt = curObj.options;
            tbody.empty();
            $.each(data, function () {
                var dt = this;
                tbody.append($('<tr/>', { 'y-id': dt.Id }).append($('<td/>', { title: dt.Name, text: dt.Name })));
                if (opt.HaveOverView) {
                    var ovTbody = ele.find('.Overview-Label tbody');
                    ovTbody.append($('<tr/>', { 'y-id': dt.Id }).append($('<td/>', { html:'&nbsp;' })));
                }
            });
            return curObj;
        }
        this.buildXAxisData = function (data) {
            var curObj = this;
            var ele = curObj.element;
            var _tr = ele.find('.mid-td table thead>tr');
            var opt = curObj.options;
            _tr.empty();
            $.each(data, function () {
                var dt = this;
                _tr.append($('<td/>', { style: 'width:' + opt.BodyCellWidth + 'px;', 'x-axis-h-id': dt.Id, title: dt.Name, text: dt.Name }));
            });
            return curObj;
        }
        this.buildBodyData = function (y,x) {
            var curObj = this;
            var ele = curObj.element;
            var _tb = ele.find('.mid-td table tbody');
            _tb.empty();
            $.each(y, function (i) {
                var ydt = this;
                var _tr = $('<tr/>', {});
                _tb.append(_tr);
                $.each(x, function (j) {
                    var xdt = this;
                    _tr.append($('<td/>', { 'x-axis-id': xdt.Id,'y-axis-id': ydt.Id, html:'&nbsp;' }));
                });
            });
            return curObj;
        }
        this.buildLeftRightTdTable=function(cls,wdth){
            var tble = $('<table/>', { style: 'table-layout:fixed;width:' + wdth+'px;', class: cls });
            tble.append($('<thead/>', {}).append($('<tr/>', {}).append($('<td/>', { style: 'border:0;width:' + wdth + 'px;' }))));
            tble.append($('<tbody/>',{}));
            return tble;
        }
        this.buildMidTd=function(cls){
            var _div=$('<div/>',{class:'report-cells'});
            var tble = $('<table/>', { style: 'table-layout:fixed'});
            _div.append(tble);
            tble.append($('<thead/>', {  }).append($('<tr/>', {})));
            tble.append($('<tbody/>', {  }));
            return _div;
        }
        this.sample = function ( ) {
            var curObj = this;
            return this;
        };
 
 
 

    };
    $.fn.TssReportTable = function (options) {
        return this.each(function () {
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data('this')) return;
            // pass options to plugin constructor
            var obj = new TssReportTable(this, options);
            // Store plugin object in this element's data
            element.data('this', obj);
            obj.init();
        });
    };
})(jQuery);