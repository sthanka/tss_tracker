<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    function __construct(){
        $this->db->query('SET SQL_BIG_SELECTS=1');
    }

    function next_result()
    {
        if (is_object($this->conn_id))
        {
            return mysqli_next_result($this->conn_id);
        }
    }

    function getProjectStatusReport($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'p.id_project desc';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) {
            if($whereParam == 'In progress' || $whereParam == 'in progress') $whereParam = 'progress';
            $whereParam = specialCharReplace($whereParam); /*$whereParam = "and (".$whereParam.")";*/
            if(preg_match_all('/:/', $whereParam)){
                $whereParam = "and (".$whereParam.")";
            }else{
                $whereParam = "AND (p.project_name LIKE '%".$whereParam."%'
                    OR p.status='".$whereParam."'
                 )";
            }
        }

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = array(0); }

        if(isset($paramArr['project'])){ $project_id = $paramArr['project']; }
        else{ $project_id = array(0); }

        if(isset($paramArr['start_date'])){ $start_date = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['start_date']))); }
        else{ $start_date = 0; }

        if(isset($paramArr['end_date'])){ $end_date = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['end_date'])));}
        else{ $end_date = 0; }
        //array_push($project_id,'""');
        //echo "<pre>"; print_r($project_id); exit;
//echo "<pre>"; print_r($sortField); exit;
        $SQL = 'SELECT
                 p.project_name
                    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( pt.estimated_time ) ) ),"%H:%i") AS estimated_effort
                    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( tfl.allowted_time ) ) ),"%H:%i") AS assigned_effort
                    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ),"%H:%i") AS completed_effort
                    ,p.`status`
                from project p
                join project_task pt on p.id_project = pt.project_id
                left join (select tf.project_task_id,SEC_TO_TIME( SUM( TIME_TO_SEC( tm.allowted_time ) ) ) AS allowted_time from task_flow tf join task_member tm on tf.id_task_flow = tm.task_flow_id group by tf.project_task_id) tfl on pt.id_project_task = tfl.project_task_id
                left join (select project_task_id,SEC_TO_TIME( SUM( TIME_TO_SEC(duration) ) ) as duration from log_time group by project_task_id) lt on pt.id_project_task = lt.project_task_id ';

        if(join(', ',($project_id))!=0)
            $whereClause = ' where true AND  (`p`.`id_project` in ('.join(', ',($project_id)).')) and pt.pm_approved = 1 '.$whereParam;
        else
            $whereClause = "where true AND pt.pm_approved = 1 ".$whereParam;

                $SQL .=$whereClause.' GROUP BY p.id_project
                order by '.$sortField.' ';
        $tempdb = clone $this->db;
        $SQL .=$optLimit;
        //echo $SQL; exit;
        $result = $this->db->query($SQL);
        $count = $tempdb->query($SQL);
        $total_count = $count->num_rows();
        //echo $this->db->last_query(); exit;

        /*$SQLCount = 'SELECT count(*)
                from project p
                join project_task pt on p.id_project = pt.project_id
                left join (select tf.project_task_id,SEC_TO_TIME( SUM( TIME_TO_SEC( tm.allowted_time ) ) ) AS allowted_time from task_flow tf join task_member tm on tf.id_task_flow = tm.task_flow_id group by tf.project_task_id) tfl on pt.id_project_task = tfl.project_task_id
                left join log_time lt on pt.id_project_task = lt.project_task_id

                WHERE (`p`.`id_project` in ('.join(', ',($project_id)).') or lt.project_id is null)
                order by '.$sortField;
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];*/

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    function getProjectRequirementStatusGrid($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'p.id_project';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) {
            if($whereParam == 'In progress' || $whereParam == 'in progress') $whereParam = 'progress';
            $whereParam = specialCharReplace($whereParam); /*$whereParam = "and (".$whereParam.")";*/
            if(preg_match_all('/:/', $whereParam)){
                $whereParam = "";
            }else{
                $whereParam = " AND (p.project_name LIKE '%".$whereParam."%'
                    OR r.requirement_name LIKE '%".$whereParam."%'
                    OR p.status='".$whereParam."'
                 )";
            }
        }
        $whereClause = "";

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = array(0); }

        if(isset($paramArr['project'])){ $project_id = $paramArr['project']; }
        else{ $project_id = array(0); }

        if(isset($paramArr['requirement'])){ $requirement = $paramArr['requirement']; }
                else{ $requirement = array(0); }
        if($requirement==''){ $requirement = array(0); }
        if(isset($paramArr['start_date'])){ $start_date = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['start_date']))); }
        else{ $start_date = 0; }

        if(isset($paramArr['end_date'])){ $end_date = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['end_date'])));}
        else{ $end_date = 0; }
        //array_push($project_id,'""');
        //echo "<pre>"; print_r($project_id); exit;
//echo "<pre>"; print_r($sortField); exit;
        $SQL = 'SELECT
                 p.project_name
    ,r.id_requirements
    ,r.requirement_name
    ,count(pt.id_project_task) as no_of_tasks
    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( pt.estimated_time ) ) ),"%H:%i") AS estimated_effort
    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( tfl.allowted_time ) ) ),"%H:%i") AS assigned_effort
    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( tfl.additional_time ) ) ),"%H:%i") AS additional_time
    ,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ),"%H:%i") AS completed_effort

    ,p.`status`
                from project p
                join requirements r on p.id_project = r.project_id
join project_task pt on r.id_requirements = pt.requirement_id
left join (select tf.project_task_id,SEC_TO_TIME( SUM( TIME_TO_SEC( tm.allowted_time ) ) ) AS allowted_time,SEC_TO_TIME( SUM( TIME_TO_SEC( tf.additional_time ) ) ) AS additional_time from task_flow tf join task_member tm on tf.id_task_flow = tm.task_flow_id group by tf.project_task_id) tfl on pt.id_project_task = tfl.project_task_id
left join (select project_task_id,SEC_TO_TIME( SUM( TIME_TO_SEC(duration) ) ) as duration from log_time group by project_task_id) lt on pt.id_project_task = lt.project_task_id ';
                /*if(join(', ',($project_id))!=0)
                    $SQL .= ' WHERE (`pt`.`project_id` in ('.join(', ',($project_id)).')) ';
                if(join(', ',($requirement))!=0)
                    $SQL .= ' AND (r.id_requirements in ('.join(', ',($requirement)).')) ';*/
        if(join(', ',($project_id))!=0)
            $whereClause = 'WHERE true AND  (`p`.`id_project` in ('.join(', ',($project_id)).')) and pt.pm_approved = 1 ';
        if(join(', ',($requirement))!=0)
            $whereClause = ' AND (r.id_requirements in ('.join(', ',($requirement)).')) ';

        if(join(', ',($project_id))!=0 || join(', ',($requirement))!=0)
            $whereClause .= $whereParam;
        else
            $whereClause .= 'WHERE true '.$whereParam;

        $whereClause .=' AND pt.pm_approved=1';

        $SQL .= $whereClause.' group by r.id_requirements ';
        $count = $this->db->query($SQL);

        $SQL .= ' order by '.$sortField.' '.$sortOrder.' ';
        $tempdb = clone $this->db;
        $SQL .=$optLimit;
//echo $SQL; exit;
        $result = $this->db->query($SQL);
        /*$count = $tempdb->query($SQL);*/
        $total_count = $count->num_rows();
        //echo $this->db->last_query(); exit;
        //echo $this->db->last_query(); exit;

        /*$SQLCount = 'SELECT count(*)
                from project p
                join project_task pt on p.id_project = pt.project_id
                left join (select tf.project_task_id,SEC_TO_TIME( SUM( TIME_TO_SEC( tm.allowted_time ) ) ) AS allowted_time from task_flow tf join task_member tm on tf.id_task_flow = tm.task_flow_id group by tf.project_task_id) tfl on pt.id_project_task = tfl.project_task_id
                left join log_time lt on pt.id_project_task = lt.project_task_id

                WHERE (`p`.`id_project` in ('.join(', ',($project_id)).') or lt.project_id is null)
                order by '.$sortField;
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];*/

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array('rows'=>[],'total'=>0,'page'=>$page);
        }
    }

    public function getUserLogTimeByDate($data)
    {
        $this->db->select('user_id,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( duration ) )),"%H:%i") as time,SUM( TIME_TO_SEC( duration )) as time_sec, DATE_FORMAT(created_date_time,"%e") as day');
        $this->db->from('log_time');
        if(isset($data['user_id']))
            $this->db->where_in('user_id',$data['user_id']);
        if(isset($data['start_date']) && isset($data['end_date']))
            $this->db->where('DATE_FORMAT(created_date_time,"%Y-%m-%d") BETWEEN "'.$data['start_date'].'" and "'.$data['end_date'].'"');
        $this->db->group_by('user_id');
        $this->db->group_by('created_date_time');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function getLeavePermission($data)
    {
        // Loading second db and running query.
        $CI = &get_instance();
        //setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('db2', TRUE);

        $this->db2->select('*');
        $this->db2->from('leave_request l');
        $this->db2->join('user u','l.request_from=u.user_id','left');
        if(isset($data['user_id']))
            $this->db2->where('u.user_name',$data['user_id']);
        if(isset($data['date']))
            $this->db2->where('("'.$data['date'].'" >=DATE_FORMAT(from_date,"%Y-%m-%d") and "'.$data['date'].'" <=DATE_FORMAT(to_date,"%Y-%m-%d"))');
        $query = $this->db2->get();

        return $query->result_array();
    }

    function addProjectWeek($data)
    {
        $this->db->insert('project_week',$data);
        $project_week_id = $this->db->insert_id();
    }

    function updateProjectWeek($data)
    {
        if(isset($data['id_project_week']))
            $this->db->where('id_project_week', $data['id_project_week']);
        $this->db->update('project_week',$data);
        $project_week_id = $this->db->insert_id();
    }

    function getProjectWeeks($data)
    {
        $this->db->select('*');
        $this->db->from('project_week');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['project_week_id']))
            $this->db->where('id_project_week',$data['project_week_id']);
        if(isset($data['week_start_date']) && isset($data['week_end_date'])){
            $this->db->where('week_start_date',$data['week_start_date']);
            $this->db->where('week_end_date',$data['week_end_date']);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    function getProjectPlan($data)
    {
        $this->db->select('*');
        $this->db->from('project_plan');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        /*if(isset($data['not_project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['project_week_id']))
            $this->db->where('project_week_id',$data['project_week_id']);*/

        $query = $this->db->get();
        return $query->result_array();
    }

    function getUserOtherProjectsEstimation($data)
    {
        $this->db->select('SEC_TO_TIME(SUM(IF(TIME_TO_SEC(pp.time),TIME_TO_SEC(pp.time),0))) assigned_log');
        $this->db->from('project_week pw');
        $this->db->join('project_plan pp', 'pp.project_week_id=pw.id_project_week', 'left');

        if(isset($data['project_id']))
            $this->db->where("pw.project_id!='".$data['project_id']."'");

        if(isset($data['user_id']))
            $this->db->where('pp.user_id',$data['user_id']);

        if(isset($data['week_start_date']) && isset($data['week_end_date'])){
            $this->db->where("((STR_TO_DATE('".$data['week_start_date']."','%Y-%m-%d') between pw.week_start_date AND pw.week_end_date) ||
(STR_TO_DATE('".$data['week_end_date']."','%Y-%m-%d') between pw.week_start_date AND pw.week_end_date))");
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function getUserLogTime($data)
    {
        $this->db->select('user_id,group_concat(DISTINCT(p.project_name)) as project_name,TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( duration ) )),"%H:%i") as time,SUM( TIME_TO_SEC( duration )) as time_sec');
        $this->db->from('log_time l');
        $this->db->join('project p','l.project_id=p.id_project','left');
        if(isset($data['project_id']))
            $this->db->where_in('l.project_id',$data['project_id']);

        if(isset($data['user_id']))
            $this->db->where_in('l.user_id',$data['user_id']);
        if(isset($data['start_date']) && isset($data['end_date']))
            $this->db->where('DATE_FORMAT(l.created_date_time,"%Y-%m-%d") BETWEEN "'.$data['start_date'].'" and "'.$data['end_date'].'"');
        if(isset($data['project_id_not']))
            $this->db->where_not_in('l.project_id',$data['project_id_not']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function checkProjectPlan($data)
    {
        $this->db->select('TIME_FORMAT(SEC_TO_TIME( SUM( TIME_TO_SEC( time ) )),"%H:%i") as time, id_project_plan');
        $this->db->from('project_plan');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['project_week_id']))
            $this->db->where('project_week_id',$data['project_week_id']);
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function addProjectPlan($data)
    {
        $this->db->insert('project_plan',$data);
        $project_plan = $this->db->insert_id();
    }

    function updateProjectPlan($data)
    {
        $this->db->where('id_project_plan',$data['id_project_plan']);
        $this->db->update('project_plan',$data);
        return 1;
    }

    function getWeekWorkTime($data)
    {
        $this->db->select('*');
        $this->db->from('task_week_flow');
        if(isset($data['user_id']))
            $this->db->where_in('user_id',$data['user_id']);
        if(isset($data['start_date']) && isset($data['end_date']))
            $this->db->where('DATE_FORMAT(created_date_time,"%Y-%m-%d") BETWEEN "'.$data['start_date'].'" and "'.$data['end_date'].'"');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

}