<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Reports</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Team workload Reports
                    </h3>
                </div>
                <div class="padding0">

                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <form id="reports_frm" class="NewErrorStyle" name="reports_frm" method="post"
                              action="<?= WEB_BASE_URL ?>index.php/Reports/generateReport">
                            <div class="col-sm-10 padding0">
                                <!--<div class="col-sm-6">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="start_date" value="<?/*=date('d/m/Y',strtotime("-1 days"))*/?>"
                                                       name="start_date" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>

                                        <label class="control-label col-sm-12">Start Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="end_date" value="<?/*=date('d/m/Y')*/?>"
                                                       name="end_date" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>
                                        <label class="control-label col-sm-12">End Date:</label>
                                    </div>
                                </div>-->
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="mt15">
                                            <select id="projectDropdown" name="projectDropdown"
                                                    class="form-control rval">

                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Project:</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <!--<button class="button button-common module mt20"  onclick="resultType(0);" > Search </button>
                                    <button class="button button-common module mt20"  onclick="resultType(1);" > Excel </button>-->
                                    <input type="button" class="button button-common module mt20"
                                           onclick="getDepartmentTaskList();" value="Search">
                                    <!--<input type="button" class="button button-common module mt20"
                                           onclick="validateReportsForm(1);" value="Excel">-->
                                </div>
                            </div>
                            <input type="hidden" name="type" id="type" value="0">
                        </form>

                    </div>
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="grid-details-table-content clearfix padding0">
                            <div class="tbl_wrapper border0">
                                <table id="department-list" class="table-responsive department-list" style="border: 1px solid #eee;">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#project-list');

        $(function () {
            getDepartmentTaskList();

            postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var projects = result.data;
                        TssLib.populateSelect($('#projectDropdown'), {
                            success: true,
                            data: projects
                        }, 'project_name', 'id_project');
                        //TssLib.selectAllOptions('#projectDropdown');
                        //loadDepartments();
                    }
                }
            });

        });

        function getDepartmentTaskList(){
            var project_id = $('#projectDropdown').val();
            if(!project_id){ return false; }
            getJsonAsyncWithBaseUrl('Project/ProjectTaskDetails?project_id='+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.response) {
                        if(result.data.data.length==0){
                            TssLib.notify('No records available', 'warn', 5);
                        }
                        var data = result.data.data, head = '', body = '', count =0;
                        for(var index in data){
                            var main = data[index];
                            if(count == 0){
                                head += '<tr>';
                                for(var ind in main){
                                    if(ind != 'id_project_task'){
                                        var heading = ind;
                                        if(ind == 'task_name'){ head += '<th width="15%">Task Name</thwidth>'; }
                                        else if(ind == 'description') { head += '<th width="15%">Description</th>'; }
                                        else{ head += '<th>'+heading+'</th>'; }
                                    }
                                }
                                count++;
                                head += '</tr>';
                            }
                            body += '<tr>';
                            for(var ind in main){
                                if(ind != 'id_project_task'){
                                    if(ind == 'task_name'){
                                        body += '<td class="taskname">'+main[ind]+'</td>';
                                    }else{
                                        body += '<td >'+main[ind]+'</td>';
                                    }
                                }
                            }
                            body += '</tr>';
                        }
                        $('#department-list > thead').html(head);
                        $('#department-list > tbody').html(body);
                        searchDepartmentTask();
                    }else{
                        $('#department-list > thead').html('');
                        $('#department-list > tbody').html('');
                        TssLib.notify('No records available', 'warn', 5);
                    }
                }
            });
        }

        function searchDepartmentTask(){
            $('#filter-task-box').keyup(function(){
                var value = $(this).val();
                var re = new RegExp( value, "gi" );
                $("table tr").each(function(index) {
                    if (index != 0) {

                        $row = $(this);

                        var id = $row.find("td:first").text();

                        if (!id.match(re)) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    }
                });
            });
        }
    </script>