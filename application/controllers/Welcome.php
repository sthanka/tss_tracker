<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
    }

    public function index()
    {
        $this->load->view('welcome_message');

    }

    public function taskList()
    {
        // $data['content'] = 'project_task/excel_upload';
        $this->load->view('project_task/excel_upload');
    }

    public function excelTaskUpload()
    {

        $project_id = $data['project_id'] = $_POST['project_id'];
        $project_departments = $data['project_departments'] = $this->Project_modal->getProjectDepartments($project_id);

        if(!isset($_POST['requirement_id']) || $_POST['requirement_id']=='' || $_POST['requirement_id']==0){

            $jsonData['status'] =false;
            $jsonData['data'] = '<div class="red">Please select requirement</div>';
        }
        else if (isset($_FILES['file']['tmp_name']) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
            $requirement_id = $data['requirement_id'] = $_POST['requirement_id'];
            $file = $_FILES['file']['tmp_name'];
            $userfile_extn = explode(".", strtolower($_FILES['file']['name']));

            if ($userfile_extn[1] == 'xlsx' || $userfile_extn[1] == 'xls') {
                $this->load->library('excel');
                $objPHPExcel = PHPExcel_IOFactory::load($file);
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                $header = [];
                $arr_data = [];
                $index = 0;
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    if ($row == 1) {
                        $header[$row][$column] = trim($data_value);
                    } else {
                        $value = $header[1][$column];
                        $arr_data[$row][$value] = $data_value;
                    }
                    $index++;
                }
                $index = 0;
                $taskList = [];
                //echo "<pre>"; print_r(array_keys($arr_data[2])); exit;
                foreach ($arr_data as $key => $item) {
                    $taskList[$index]['number'] = $key;
                    $convertTime = '';
                    /*if (isset($item['Estimation Time']) && !empty($item['Estimation Time'])) {
                        $convertTime = substr(preg_replace("/[\sh\sm]+/", "$1:$2", $item['Estimation Time']), 0, -1);
                        //$convertTime =  date("H:i", strtotime($convertTime));
                    }*/
                    for($a=0;$a<count($project_departments);$a++)
                    {
                        $convertTime = '';
                        //echo $project_departments[$a]['department_name'].' (00h00m)'.'<br>'; exit;
                        if (isset($item[$project_departments[$a]['department_name'].' (00h00m)']) && !empty($item[$project_departments[$a]['department_name'].' (00h00m)'])) {

                            $convertTime = substr(preg_replace("/[\sh\sm]+/", "$1:$2", $item[$project_departments[$a]['department_name'].' (00h00m)']), 0, -1);
                            //$convertTime =  date("H:i", strtotime($convertTime));
                        }
                        $taskList[$index][$project_departments[$a]['department_name']] = isset($convertTime) ? $convertTime : '';
                    }
                    //echo "<pre>"; print_r($taskList); exit;
                    //$taskList[$index]['project_name'] = isset($item['Project Name']) ? $item['Project Name'] : '';
                    $taskList[$index]['module_name'] = isset($item['Module Name']) ? $item['Module Name'] : '';
                    $taskList[$index]['task_name'] = isset($item['Task Name']) ? $item['Task Name'] : '';
                    $taskList[$index]['start_date'] = isset($item['Start Date (yyyymmdd)']) ? $item['Start Date (yyyymmdd)'] : date('Y-m-d');
                    $taskList[$index]['end_date'] = isset($item['End Date (yyyymmdd)']) ? $item['End Date (yyyymmdd)']: date('Y-m-d');

                    $taskList[$index]['users'] = isset($item['ASSIGN TO']) ? $item['ASSIGN TO'] : '';
                    $taskList[$index]['description'] = isset($item['Description']) ? $item['Description'] : '';
                    $taskList[$index]['task_type'] = isset($item['Task Type']) ? $item['Task Type'] : '';
                    $taskList[$index]['use_case'] = isset($item['Use case']) ? $item['Use case'] : '';
                    $index++;
                }
//                echo '<pre>'; print_r($taskList); exit;
                /*$excel_columns = array('Project Name','Module Name','Task Name','ASSIGN TO','Start Date','End Date','Description','Task Type');
                for ($a = 0; $a < count($project_departments); $a++) {
                    array_push($excel_columns , $project_departments[$a]['department_name']);
                }
                $index = 0;
                foreach ($arr_data as $key => $item) {
                    foreach($item as $k=>$v){
                        $status = false;
                        foreach($excel_columns as $k1=> $v1){
                            if($k == $v1){ $status=true; break; }
                        }
                        if(!$status){
                            $temp = $this->Project_modal->getDepartmentDetails(['department_name' => $k]);
                            if(count($temp)>0){
                                $deptDetail[$k] = $temp[0];
                                $project_departments[] = $temp[0];
                            }
                        }
                    }
                    if(count($deptDetail)>0){
                        foreach($deptDetail as $k=>$v){
                            if(isset($item[$v['department_name']]) && !empty($item[$v['department_name']]) ){
                                $convertTime = substr(preg_replace("/[\sh\sm]+/", "$1:$2", $item[$v['department_name']]), 0, -1);
                            }
                            $taskList[$index][$v['department_name']] = isset($convertTime) ? $convertTime : '';
                        }
                        $taskList[$index]['project_name'] = isset($item['Project Name']) ? $item['Project Name'] : '';
                        $taskList[$index]['module_name'] = isset($item['Module Name']) ? $item['Module Name'] : '';
                        $taskList[$index]['task_name'] = isset($item['Task Name']) ? $item['Task Name'] : '';
                        $taskList[$index]['start_date'] = isset($item['Start Date']) ? $item['Start Date'] : date('Y-m-d');
                        $taskList[$index]['end_date'] = isset($item['End Date']) ? $item['End Date']: date('Y-m-d');

                        $taskList[$index]['users'] = isset($item['ASSIGN TO']) ? $item['ASSIGN TO'] : '';
                        $taskList[$index]['description'] = isset($item['Description']) ? $item['Description'] : '';
                        $taskList[$index]['task_type'] = isset($item['Task Type']) ? $item['Task Type'] : '';
                        $index++;
                    }
                }*/
                // $data['excelHeader'] = $header[1];
                $data['row'] = $taskList;
                $jsonData['status'] =true;
                //echo "<pre>"; print_r($data); exit;
                $jsonData['data'] = $this->load->view('project_task/excel_view', $data,true);
            } else {
                $jsonData['status'] =false;
                $jsonData['data'] ='<div class="red">File extension not supported!</div>';
            }
        } else {
            $jsonData['status'] =false;
            $jsonData['data'] = '<div class="red">File Required</div>';
        }
        echo json_encode($jsonData);
    }

    function excelTaskUploadAjax()
    {
        $this->load->model('Project_modal');
        $this->load->model('User_model');
        $data = $this->input->post();
        $data['task_name'] = trim($data['task_name']);
            //echo "<pre>"; print_r($data); exit;
        $task['status'] = true;
        $task['message'] = '';
        $data['user_id'] = '';
        //validation
        $requirement_id = $data['requirement_id'];
        $project_id = $data['project_id'];
        $project_departments = $this->Project_modal->getProjectDepartments($project_id);
//        echo '<pre>'; print_r($project_departments); exit;

        /*$excel_columns = array('project_name', 'module_name', 'task_name', 'start_date', 'end_date', 'users', 'description', 'task_type');
        for ($a = 0; $a < count($project_departments); $a++) {
            array_push($excel_columns , $project_departments[$a]['department_name']);
        }

        foreach($data as $key=>$val){
            $status = false;
            foreach($excel_columns as $k1=> $v1){
                if($val == $v1){ $status=true; break; }
            }
            if(!$status){
                $temp = $this->Project_modal->getDepartmentDetails(['department_name' => $val]);
                echo '<pre>'; print_r($temp); exit;
                if(count($temp)>0){
                    $deptDetail[$key] = $temp[0];
                    $project_departments[] = $temp[0];
                }
            }
        }
        echo '<pre>'; print_r($project_departments); exit;*/
        //echo "<pre>"; print_r($data);
        //echo "<pre>"; print_r($data[strtolower(str_replace(' ','',$project_departments[0]['department_name']))]);
        //echo "<pre>"; print_r($project_departments); exit;
        for($q=0;$q<count($project_departments);$q++)
        {
            if(empty($data[strtolower(str_replace('.','',str_replace(' ','',$project_departments[$q]['department_name'])))])){
                $project_departments[$q]['department_name'] = '00:00';
                /*$task['status'] = false;
                $task['message'] = $project_departments[$q]['department_name']." Time Required";*/
            }
        }
        if (empty($data['task_name'])) {
            $task['status'] = false;
            $task['message'] = "Task Name Required";
        }
        else{
            $check_task = $this->Project_modal->getRequirementTask(array('project_id' => $project_id,'requirement_id' => $data['requirement_id'],'task_name'=>$data['task_name']));
            if(!empty($check_task)){
                $task['status'] = false;
                $task['message'] = "Task Name Already exists";
            }
        }
        if (empty($data['module_name'])) {
            $task['status'] = false;
            $task['message'] = "Module Name Required";
        }

        /*if (empty($data['users'])) {
            $task['status'] = false;
            $task['message'] = "Department Required";
        }*/
        if (empty($data['task_type'])) {
            $task['status'] = false;
            $task['message'] = "Task Type Required";
        }
        /*if (empty($data['project_name'])) {
            $task['status'] = false;
            $task['message'] = "Project Name Required";
        }*/else{
            //$project_id = $this->Project_modal->createExcelProject($data);
            if(!$project_id){
                $task['status'] = false;
                $task['message'] = "Invalid Project";
            }else{
                /*if (!empty($data['users'])) {
                    $departmentDetails =$this->Project_modal->getDepartmentDetails(array('department_name' => $data['users']));
                    if (empty($departmentDetails)) {
                        $task['status'] = false;
                        $task['message'] = "Invalid Department";
                    }
                    else
                    {*/
                        $projectDepartmentDetails =$this->Project_modal->getProjectDepartmentDetails(array('project_id' => $project_id));
                        //echo "<pre>"; print_r($projectDepartmentDetails); exit;
                        if(empty($projectDepartmentDetails))
                        {
                            $task['status'] = false;
                            $task['message'] = "Department not assigned";
                        }
                        else {
                            for($d=0;$d<count($projectDepartmentDetails);$d++)
                            {
                                if(trim($data[strtolower(str_replace('.','',str_replace(' ','',$projectDepartmentDetails[$d]['department_name'])))])!=0 && trim($data[strtolower(str_replace('.','',str_replace(' ','',$projectDepartmentDetails[$d]['department_name'])))])!=0) {
                                    $data['department_id'][] = $projectDepartmentDetails[$d]['id_department'];
                                    break;
                                }
                            }

                        }
                    /*}
                }*/
            }
        }

        if ($task['status']) {
            $data['project_id'] = $project_id;
            $data['requirement_id'] = $requirement_id;
            $module_id = $this->Project_modal->createExcelModule($data);
            $data['module_id'] = $module_id;
            /*if(isset($data[$data['users']])){
                $data['estimated_time'] = $data[$data['users']];
            }*/
            //echo "<pre>"; print_r($data); exit;
            $task = $this->Project_modal->createExceltask($data, 'task');
            $taskId = $task['task_id'];

            $task['status'] = true;
            $totalTime = 0;
            for($q=0;$q<count($project_departments);$q++)
            {
                if(isset($data[strtolower(str_replace('.','',str_replace(' ','',$project_departments[$q]['department_name'])))])){
                    $task['status'] = true;
                    $task_dept = array(
                        'project_id' => $project_id,
                        'task_id' => $task['task_id'],
                        'department_id' => $project_departments[$q]['id_department'],
                        'time' => $data[strtolower(str_replace('.','',str_replace(' ','',$project_departments[$q]['department_name'])))]
                    );
                    $totalTime = $totalTime + time_to_sec($data[strtolower(str_replace('.','',str_replace(' ','',$project_departments[$q]['department_name'])))]);
                    $task_department = $this->Project_modal->createExceltaskDepartment($task_dept);
                }
            }
            if($totalTime){
                $totalTime = sec_to_time($totalTime);
                $this->Project_modal->updateTask(array('id_project_task' => $taskId, 'estimated_time'=> $totalTime));
                //$res = $this->Project_modal->getTaskFlow(array('project_task_id' => $taskId));
                //echo "<pre>"; print_r($res); exit;
                /*if(count($res)>0){
                    $task_flow_id = $res[0]['id_task_flow'];
                    $this->Project_modal->updateTaskFlow(array('estimated_time'=> $data[strtolower(str_replace('.','',(str_replace(' ','',$data['users']))))]) , $task_flow_id);
                }*/
            }

            //notification
            $project_details = $this->Project_modal->getProjectDetails(array('project_id' => $data['project_id']));
            $this->Project_modal->addNotification(array(
                    'user_id' => $project_details[0]['project_manager'],
                    'subject' => 'Task',
                    'message' => 'New Tasks uploaded for <b>'.$project_details[0]["project_name"].'</b> project',
                    'url' => WEB_BASE_URL.'index.php/project/projectCreation/'.$project_details[0]["id_project"],
                    'read_flag' => 1
                ));

        }

        echo json_encode($task);
        exit;
    }

    public function getProjectsGrid()
    {
        $paramArr = $this->input->get();
        //echo "<pre>"; print_r($paramArr); exit;
        $start = isset($paramArr['start']) ? $paramArr['start'] : NULL;
        $limit = isset($paramArr['limit']) ? $paramArr['start'] : NULL;
        $sortField = isset($paramArr['sidx']) ? $paramArr['sidx'] : 'project_name';
        $sortOrder = isset($paramArr['sord']) ? $paramArr['sord'] : 'asc';
        $whereParam = isset($paramArr['whereParam']) ? $paramArr['whereParam'] : NULL;
        if (!empty($start) && !empty($limit)) $optLimit = "limit $start,$limit";
        else $optLimit = NULL;

        if (!empty($whereParam)) $whereParam = "and (" . $whereParam . ")";
        $whereClause = "where true " . $whereParam;

        $SQL = "SELECT * FROM project $whereClause order by $sortField $sortOrder $optLimit ";
        $result = $this->db->query($SQL);

        if ($result->num_rows() > 0) {
            $custlist = $result->result();

        } else {
            $custlist = null;
        }
        echo json_encode($custlist);
        exit;
        /*$this->db->select('*');
        $this->db->from('project');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();*/
        //return $query->result_array();
    }

    public function jqgrid()
    {
        $aData = array(
            'set_columns' => array(
                array(
                    'label' => 'Project',
                    'name' => 'project_name',
                    'width' => 100,
                    'size' => 10
                ),
                array(
                    'label' => 'Start Date',
                    'name' => 'project_start_date',
                    'width' => 220,
                    'size' => 10
                ),
                array(
                    'label' => 'End Date',
                    'name' => 'project_end_date',
                    'width' => 100,
                    'size' => 10
                )
            ),
            'div_name' => 'grid',
            'source' => 'index.php/welcome/getProjectsGrid/',
            'sort_name' => 'project_name',
            'add_url' => 'customer/exec/add',
            'edit_url' => 'customer/exec/edit',
            'delete_url' => 'customer/exec/del',
            'caption' => 'Project',
            'primary_key' => 'id',
            'grid_height' => 230
        );

        $data['content'] = 'project/jqgrid_view';
        $data['customerGrid'] = buildGrid($aData);
        $this->load->view('partials/landing', $data);

    }

    public function generateTaskUploadExcel()
    {
        $project_id = $_POST['project_id'];
        require_once 'application/third_party/PHPExcel/Classes/PHPExcel.php';

        $excel_columns = array('Module Name','Task Name','Start Date (yyyymmdd)','End Date (yyyymmdd)','Description','Task Type','Use case');

        $row_number=1; $column_index=65;
        $objPHPExcel = new PHPExcel();

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 10,
                'name'  => 'Verdana'
            ));

        $project_departments = $this->Project_modal->getProjectDepartments($project_id);
        $row_number=1; $column_index=65;
        $objPHPExcel->createSheet();

        for($ec=0;$ec<count($excel_columns);$ec++){
            $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue(chr($column_index).$row_number, $excel_columns[$ec]);

            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(25);

            $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
            $column_index++;
        }

        for($ec=0;$ec<count($project_departments);$ec++)
        {
            $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue(chr($column_index).$row_number, $project_departments[$ec]['department_name'].' (00h00m)');

            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(25);

            $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
            $column_index++;
        }

        //sample data
        $row_number=2; $column_index=65;
        /*$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue(chr($column_index).$row_number, 'project name');$column_index++;*/
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, 'Module Name');$column_index++;
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, 'Task Name');$column_index++;
        /*$objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, 'Dev - PHP');$column_index++;*/
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, '20160701');$column_index++;
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, '20160720');$column_index++;
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, 'Description');$column_index++;
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, 'development');$column_index++;
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, 'Use case');$column_index++;
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, '17h0m '); $column_index++;

        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=sample_task_sheet.xls");
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }
}
