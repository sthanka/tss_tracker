<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!-- Modal Dialog  dekete confirmation-->
<div class="modal fade" id="oasis-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel panel-default panel-body padding20 clearfix">
                    <p></p>
                </div>
                <div class="panel-footer col-sm-12 clearfix text-right">
                    <button type="button" class="button-common module btn-danger">Yes</button>
                    <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Task Types</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Task Types List</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="task-type-list" class="table-responsive"></table>
                        <div id="task_type-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-client-modal" class="modal-wrapper" style="display:none; width: 200px;">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Task Types</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12" style="margin-bottom: 1%;">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="name"  id="name"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Task Type Name :</label>
                                    </div>
                                </div>
                                <div class="col-sm-12" style="overflow-y: scroll; margin-top: 1%;">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval"  name="department_id" id="department_id" multiple ></select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Departments :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0" style="margin-top: 5%;">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="">
                                            <label><input type="radio"  name="product_type" value="billable" checked /> Productive</label>
                                            <label><input type="radio"  name="product_type" value="non-billable" /> Non Productive</label>
                                        </div>
                                        <!--<label class="control-label"><span class="req-str">*</span>Product Type :</label>-->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="">
                                            <label><input type="radio"  name="is_billable" value="1" checked /> Billable</label>
                                            <label><input type="radio"  name="is_billable" value="0" /> Non Billable</label>
                                        </div>
                                        <!--<label class="control-label"><span class="req-str">*</span>Product Type :</label>-->
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="form-control" name="id_task_type" value=""/>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        TssLib.docReady(function () {
            postJsonAsyncWithBaseUrl("client/getClients", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var departments = result.data.departments;
                        TssLib.populateSelect($('#department_id'), { success: true, data: departments }, 'department_name', 'id_department');
                    }
                }
            });
            var $refTblgrid = $('#task-type-list');
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Task_types/getTaskTypesGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['id_task_type', 'Name','Productive' ,'Billable' , 'task_type_key','is_billable','product_type','dept','department_name'],
                colModel: [
                    { name: 'id_task_type', index: 'id_task_type', hidden: true ,key:true},
                    { name: 'name', index: 'name' },
                    { name: 'product_type_label', index: 'product_type_label', formatter(c , o,  d){
                        return d.product_type_label=='billable'?'Yes':'No'
                    } },
                    { name: 'is_billable_label', index: 'is_billable_label', formatter(c , o,  d){
                        return d.is_billable_label==1?'Yes':'No'
                    } },
                    { name: 'task_type_key', index: 'task_type_key', hidden: true },
                    { name: 'is_billable', index: 'is_billable', hidden: true},
                    { name: 'product_type', index: 'product_type', hidden: true },
                    { name: 'dept', index: 'dept', hidden: true },
                    { name: 'department_name', index: 'department_name', hidden: true },
                ],
                pager: 'task_type-list-page'
            }).navGrid('#task_type-list-page', {
                edit: true, add: true, del: true, search: false, refresh: true,
                addfunc: function () {
                    addEditCreateTaskType();
                }, editfunc: function (id) {
                    var rowData = $refTblgrid.jqGrid('getRowData', id);
                    addEditCreateTaskType(rowData);
                }, delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        postJsonAsyncWithBaseUrl("Task_types/deleteTaskTypesById", {'task_type_id': id}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data != null) {
                                    if(result.data.status){
                                        TssLib.notify('Deleted successfully', null, 5);
                                        $('#task-type-list').trigger('reloadGrid');
                                    }else{
                                        TssLib.notify(result.data.message, 'warn', 5);
                                    }
                                }
                            }});
                    }, 'Yes', 'No');
                }
            });
        });
        function addEditCreateTaskType(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            if (TssLib.isBlank(rowData)) {
                $modal.find('.panel-title').text('Add Task Type');
                $modal.find('[div-submit="true"]').text('Save');
            } else {
                TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                $modal.find('.panel-title').text('Edit Task Type');
                $modal.find('[div-submit="true"]').text('Update');
                $modal.find('#department_id').select2("val",rowData.dept.split(','));

            }
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {

                    if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                    if (result.data.status) {
                        $('#task-type-list').trigger('reloadGrid');
                        TssLib.closeModal();
                        TssLib.notify('Task Type '+addedTest+' successfully ', null, 5);
                    }
                },
                before: function (formObj) {
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Task_types/addTaskTypes');
                    return true;
                }
            });
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->