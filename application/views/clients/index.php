<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!-- Modal Dialog  dekete confirmation-->
<div class="modal fade" id="oasis-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel panel-default panel-body padding20 clearfix">
                    <p></p>
                </div>
                <div class="panel-footer col-sm-12 clearfix text-right">
                    <button type="button" class="button-common module btn-danger">Yes</button>
                    <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Clients</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Clients List</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="client-list" class="table-responsive"></table>
                        <div id="client-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-client-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Client</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="client_name"  id="client_name"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Client Name :</label>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="client_first_name" id="client_first_name" />
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>First Name :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="client_last_name" id="client_last_name" />
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Last Name :</label>
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control" name="client_email" rules="email" id="client_email" />
                                        </div>
                                        <label class="control-label">email :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control pInt" name="client_contact" id="client_contact" />
                                        </div>
                                        <label class="control-label">Phone :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <label>Comment</label>
                                    <textarea name="comments" id="comments" class="form-control"></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control rval" name="id_client" />
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        TssLib.docReady(function () {
            var $refTblgrid = $('#client-list');
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'client/getClientsGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['Id', 'Name','first_name', 'last_name','client_email','client_contact','comments'],
                colModel: [
                    { name: 'id_client', index: 'id_client', hidden: true ,key:true},
                    { name: 'client_name', index: 'client_name' },
                    { name: 'client_first_name', index: 'client_first_name', hidden: true },
                    { name: 'client_last_name', index: 'client_last_name', hidden: true },
                    { name: 'client_email', index: 'client_email', hidden: true },
                    { name: 'client_contact', index: 'client_contact', hidden: true },
                    { name: 'comments', index: 'comments', hidden: true },
                ],
                pager: 'client-list-page'
            }).navGrid('#client-list-page', {
                edit: true, add: true, del: true, search: false, refresh: true,
                addfunc: function () {
                    addEditCreateClient();
                }, editfunc: function (id) {
                    var rowData = $refTblgrid.jqGrid('getRowData', id);
                    addEditCreateClient(rowData);
                }, delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        getJsonAsyncWithBaseUrl("client/deleteClient/"+id, {}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data.status ) {
                                    $('#client-list').trigger('reloadGrid');
                                    TssLib.notify('Deleted successfully ', null, 5);
                                }
                            }
                        });
                    }, 'Yes', 'No');
                }
            });
        });
        function addEditCreateClient(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            if (TssLib.isBlank(rowData)) {
                $modal.find('.panel-title').text('Add Client');
                $modal.find('[div-submit="true"]').text('Save');
            } else {
                TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                $modal.find('.panel-title').text('Edit Client');
                $modal.find('[div-submit="true"]').text('Update');
            }
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {

                    if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                    if (result.data.status) {
                        $('#client-list').trigger('reloadGrid');
                        TssLib.closeModal();
                        TssLib.notify('Client '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify('Client name already exist ', 'warn', 5);
                    }
                },
                before: function (formObj) {
                    if($(formObj).find("#client_contact").val().length>10){
                        TssLib.notify('Phone length can\'t be more than 10 digits', 'warn', 5);
                        return;
                    }
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'client/addClient');
                    return true;
                }
            });
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->


