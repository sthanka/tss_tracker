<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_backend_task_allotment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->library('excel');
        $this->load->model('Project_modal');
    }

    public function index()
    {
        $this->load->helper('directory');

        $absFolderPath = './uploads/excel/';   //absolute Path of directory

        $dirList = directory_map($absFolderPath, 1); //no recursion needed, and read the parent element

        $colName = array('Date', 'Project', 'Module', 'Task', 'From', 'To', 'Time Spent', 'Description');

        $dateField1 = 'Date';

        $data_value = array();

        foreach($dirList as $k=>$v){
			//$fullPath = realpath($absFolderPath.'\\'.$v);	//Windows
            $fullPath = realpath($absFolderPath.''.$v);

            $colArray = array();
            $currentRow = 0;
            $file_extn = explode(".", strtolower($v));

            $header = [];
            $arr_data = [];
            $index = 0;
            if ($file_extn[1] == 'xlsx' || $file_extn[1] == 'xls') {

                $objPHPExcel = PHPExcel_IOFactory::load($fullPath);
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();
                    if ($row == 1) {
                        $header[$row][$column] = trim($data_value);
                    } else {
                        $value = $header[1][$column];
                        $arr_data[$row][$value] = $data_value;
                    }
                    $index++;
                }
            } else {
                die('Invalid File Extension');
            }
        }
//        echo '<pre>'; print_r($arr_data); exit;
        $final = [];
        $errorFinal = [];
        $userId = '25';
        foreach($arr_data as $k=>$v){
            if(isset($v['Date'])
                && (isset($v['Project']) && ($v['Project']!='' || !empty($v['Project']) ) )
                && (isset($v['Module']) && ($v['Module']!='' || !empty($v['Module'])) ) ){
                $isError = false;
                /*echo '======================<br/>';
                echo '<pre>'; print_r($v);*/

                //Get project id
                $projectId = $this->Project_modal->createExcelProject(array('project_name'=>$v['Project']));

                //Get module id
                $module_id = $this->Project_modal->createExcelModule(array('project_id'=>$projectId, 'module_name'=> $v['Module']));

                //get Project Task Id
//                $task = $this->Project_modal->getTaskId(array('project_id'=>$projectId, 'module_id'=> $module_id, 'task_name'=> $v['Feature'].' >>'.$v['Task']));
                /*$task = $this->Project_modal->getTaskId(array('project_id'=>$projectId, 'module_id'=> $module_id, 'task_name'=> $v['Feature'].' >>'.$v['Task']));*/
                $isInserted = true;
                $task = $this->Project_modal->isTaskPresent(array('project_id'=>$projectId, 'module_id'=> $module_id, 'task_name'=> $v['Task']));

                if($task){
                    $isInserted = false;
                    $project_task_id = $task[0]['id_project_task'];
                    $est_time = sec_to_time(time_to_sec($task[0]['estimated_time'])+time_to_sec($v['Time Spent']));
                    $this->Project_modal->updateTask(array('id_project_task'=> $project_task_id ,'estimated_time' => $est_time));
                }else{
                    $isInserted = true;
                    $data = array(
                        'project_id'=>$projectId,
                        'project_module_id'=> $module_id,
                        'task_name'=> $v['Task'],
                        /*'task_type' => 'Feature',*/
                        'task_type' => $v['Description'],
                        'description' => $v['Description'],
                        'estimated_time' => $v['Time Spent'],
                        'status' => 'progress',
                        'created_date_time' => date('Y-m-d'),
                        'start_date' => date('Y-m-d',strtotime($v['Date'])),
                        'end_date' => date('Y-m-d',strtotime($v['Date'])),
                    );
                    $project_task_id = $this->Project_modal->insertNewTask($data);
                }

                if(count($task)<=0){
                    $isError =  true;
                    $errorFinal[$k] = $v;
                    $errorFinal[$k]['error'] = 'Invalid data - Task name';
                }else{
                    //get Deptt Id
//                    $dept = $this->Project_modal->getProjectDepartmentsEstTime(array('project_id'=>$projectId, 'task_id'=> $project_task_id));
                    $dept = $v['Project']=='Schooolpost'?'9':'7';        //Hard coded

                    if(!$dept){
                        $isError =  true;
                        $errorFinal[$k] = $v;
                        $errorFinal[$k]['error'] = 'Invalid data - Department';
                    }else{
//                        $deptId = $dept[0]['department_id'];
                        $deptId = $dept;        //Hard coded
                        //get Task Id
                        /*$task_flow_id = $this->Project_modal->getTaskFlow(array('department_id'=>$deptId, 'project_task_id'=> $project_task_id));
                        if(!$task_flow_id){
                            $isError =  true;
                            $errorFinal[$k] = $v;
                            $errorFinal[$k]['error'] = 'Invalid data - Task flow';
                        }*/
                        if($isInserted){
                            $data = array(
                                'project_task_id' => $project_task_id,
                                'department_id' => $deptId,
                                'estimated_time' => $v['Time Spent'],
                                'start_date' => date('Y-m-d',strtotime($v['Date'])),
                                'end_date' => date('Y-m-d',strtotime($v['Date'])),
                                'task_status' => 'progress',
                                'is_forward' => 0,
                                'created_date_time' => date('Y-m-d'),
                            );
                            $task_flow_id = $this->Project_modal->addTaskFlow($data);
                        }else{
                            $task_flow_id = $this->Project_modal->getTaskFlow(array('department_id'=>$deptId, 'project_task_id'=> $project_task_id));
                            /*echo '<pre>'; print_r($task_flow_id);
                            echo '----------------'.$k.' = '.$v['Task'];*/
                            /*if(count($task_flow_id)<=0){
                                $isError =  true;
                                $errorFinal[$k] = $v;
                                $errorFinal[$k]['error'] = 'Invalid data - Task flow';
                            }
                            if(!$isError){
                                $task_flow_id = $task_flow_id[0]['id_task_flow'];
                                $this->Project_modal->updateTaskFlow(array('estimated_time'=>$est_time), $task_flow_id);
                            }*/
                            if(count($task_flow_id)<=0) {
                                $data = array(
                                    'project_task_id' => $project_task_id,
                                    'department_id' => $deptId,
                                    'estimated_time' => $v['Time Spent'],
                                    'start_date' => date('Y-m-d', strtotime($v['Date'])),
                                    'end_date' => date('Y-m-d', strtotime($v['Date'])),
                                    'task_status' => 'progress',
                                    'is_forward' => 0,
                                    'created_date_time' => date('Y-m-d'),
                                );
                                $task_flow_id = $this->Project_modal->addTaskFlow($data);
                            }else{
                                $task_flow_id = $task_flow_id[0]['id_task_flow'];
                                $this->Project_modal->updateTaskFlow(array('estimated_time'=>$est_time), $task_flow_id);
                            }
                        }

                        if(!$isError){
                            //Task member
                            $data = array(
                                'task_flow_id' => $task_flow_id,
                                'assigned_to' => $userId,
                                'allowted_time' =>  $v['Time Spent'],
                                'additional_time' =>  '0',
                                'task_status' => 'progress',
                                'created_date_time' => date('Y-m-d',strtotime($v['Date'])),
                            );
                            $this->Project_modal->addTaskMember($data);

                            $task_week_flow = $this->Project_modal->isTaskAlloted(array('task_flow_id' => $task_flow_id, 'user_id' => $userId));
                            if($task_week_flow){
                                $dayId  = $task_week_flow[0]['day_id'];
                                /*$time  = sec_to_time(time_to_sec($task_week_flow[0]['time'])+time_to_sec($v['Time Spent']));*/
                                $dayId++;
                                $task_week_flow  = $task_week_flow[0]['id_task_week_flow'];
                                /*$this->Project_modal->updateTaskWeekFlow(array('id_task_week_flow'=>$task_week_flow, 'day_id'=> $dayId, 'time'=> $time));*/
                                $this->Project_modal->updateTaskWeekFlow(array('id_task_week_flow'=>$task_week_flow, 'day_id'=> $dayId));
                            }else{
                                $data = array(
                                    'task_flow_id' => $task_flow_id,
                                    'time' => sec_to_time(time_to_sec($v['Time Spent'])),
                                    'user_id' => $userId,
                                    'day_id' => 1,
                                    'date' => date('Y-m-d',strtotime($v['Date'])),
                                    'created_date_time' => date('Y-m-d'),
                                );
                                $this->Project_modal->addTaskWeekLoad($data);
                            }

                            //log time
                            $log = array(
                                'created_date_time' => date('Y-m-d',strtotime($v['Date'])),
                                'duration' => $v['Time Spent'],
                                'project_id' => $projectId,
                                'project_task_id' => $project_task_id,
                                'task_flow_id' => $task_flow_id,
                                'comments' => $v['Feature'].' - '.$v['Task'].' - '.$v['Description'],
                                'user_id' => $userId,      //Hard coded
                                'start_time' => date('h:i a',strtotime($v['From'])),
                                'end_time' => date('h:i a',strtotime($v['To'])),
                                'task_type' => '19',    //Hard coded
                            );
//                        echo '<pre>'; print_r($log); exit;

                            $this->Project_modal->logTime($log);
                            /*echo '<<<<<<<<<<<<<<<<<<<<br/>';*/
                        }

                    }
                }
            }
        }
//        echo '<pre>Res'; print_r($final);
        echo '<pre>Error'; print_r($errorFinal); exit;
    }

}