<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <!-- InstanceBeginEditable name="EditRegion3" -->

        <div class="contentHeader">
            <h3><?php echo $result['project_name']; ?></h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Project Overview
                    </h3>
                    <h3 class="pull-right">
                        <a class="pull-right border-left" href="<?= WEB_BASE_URL ?>index.php/Project">
                            <i class="fa fa-reply"></i>
                        </a>
                    </h3>
                </div>
                <div class="grid-details-table-content padding0">
                    <div class="col-sm-12 clearfix clearboth border-btm  pt10">
                        <div class="col-sm-2">
                            <div class="view_input">
                                <p change-date><?php echo $result['project_start_date']; ?></p>
                                <label>Start Date</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="view_input">
                                <p change-date><?php echo $result['project_end_date']; ?></p>
                                <label>End Date</label>
                            </div>
                        </div>
                        <!--<div class="col-sm-4">
                            <div class="view_input">
                                <p change-date><?php /*echo $result['project_end_date']; */ ?></p>
                                <label>Next Release Date</label>
                            </div>
                        </div>
                        <div class="view_input col-sm-2">
                            <p><span class="font-white orange-bg x-font"
                                     name="status"><?php /*echo $result['status']; */ ?></span></p>
                            <label class="margin0">Status <a class="cursor-pointer  font-12" onclick="changeStatus();"
                                                             data-original-title="change status"><i
                                        class="fa fa-random"></i></a></label>
                        </div>
                        <div class="view_input col-sm-2">
                            <p><span class="font-white red-bg x-font" name="Priority">High</span></p>
                            <label class="margin0">Priority <a class="cursor-pointer  font-12"
                                                               onclick="changePriority();"
                                                               data-original-title="change priority"><i
                                        class="fa fa-random"></i></a></label>
                        </div>-->
                    </div>
                    <div class="col-sm-12 clearfix clearboth border-btm top10">
                        <div class="col-sm-2">
                            <p class="blue-text">Description</p>
                        </div>
                        <div class="col-sm-10">
                            <p><?php echo $result['description']; ?> </p>
                        </div>
                    </div>
                    <?php if ($this->session->userdata('user_type_id') == 2 || ($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1)): ?>
                        <div class="col-sm-12 clearfix clearboth top10">
                            <div class="col-sm-2">
                                <p class="blue-text">Team Members</p>
                            </div>
                            <div class="col-sm-10">
                                <ul class="filter-list" id="teamMembers">
                                    <?php foreach ($projectUsers as $value) { ?>
                                        <li pro-user-id="<?php echo $value['id_employee']; ?>">
                                            <label><?php echo $value['email']; ?></label>
                                            <a onclick="removeMember(this);"><i class="fa fa-times"></i></a>
                                        </li>
                                    <?php } ?>

                                </ul>
                                <div class="col-sm-3 input-btn-parent">
                                    <input type="hidden" class="form-control rval" name="employeesId" id="employees"
                                           place-key-data="true" autocomplete="off">
                                    <input type="text" name="employees" maxlength="250" class="input-with-btn col-sm-12"
                                           placeholder="Team Member">
                                    <a class="button-common" onclick="addMember(this)"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-lg-12 padding0">
                        <ul class="nav nav-tabs nav-tabs-top custom-nav-tabs" role="tablist" id="project-tab">
                            <li class="active">
                                <a href="#tasks" data-toggle="tab">
                                    <p class="tab-list">Tasks</p>
                                </a>
                            </li>
                            <!--<li class="">
                                <a href="#time" data-toggle="tab">
                                    <p class="tab-list">Time</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#workload" data-toggle="tab">
                                    <p class="tab-list">Work Load</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#billing" data-toggle="tab">
                                    <p class="tab-list">Billing</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#config" data-toggle="tab">
                                    <p class="tab-list">Configuration</p>
                                </a>
                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content cv-tabs-container">
                <div id="tasks" class="tab-pane padding0 pb0 active">

                    <div class="cv-tabs col-sm-12 clearfix clearboth padding0 top20">
                        <?php if ($this->session->userdata('user_type_id') != 3) { ?>
                            <div class="col-xs-4 col-lg-3 pad-left-none">
                                <!-- required for floating -->
                                <!-- Nav tabs -->

                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <input type="text" placeholder="Enter Sprint Name" class="input-with-btn col-sm-12"
                                           maxlength="250">
                                    <a onclick="addSprint(this)" class="button-common"><i class="fa fa-plus"></i></a>
                                </div>

                                <ul class="nav tabs-left" id="sprintsTabs">

                                    <li>
                                        <a data-toggle="tabajax" id="backlogTab" data-target="#task-tab-content"
                                           href="<?php echo base_url('index.php/Project/backLogs/' . $project_id) ?>">
                                            <b>Backlogs</b>
                                            <a href="javascript:;" class="toggle-options" onclick="toggleOptions(this)">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>
                                            <ul class="toggle-options-list">
                                                <li><a href="javascript:;"> <a
                                                            onclick="uploadExcelModal(this)">Upload</a></a></li>
                                            </ul>


                                        </a>
                                    </li>

                                    <?php for ($s = 0; $s < count($sprint); $s++) { ?>
                                        <li>
                                            <a data-toggle="tabajax" data-target="#task-tab-content"
                                               href="<?php echo base_url('index.php/Project/sprintDetails/' . $project_id . '/' . $sprint[$s]['id_sprint']) ?>">
                                                <b><?= $sprint[$s]['name'] ?></b>
                                                <a href="javascript:;" class="toggle-options"
                                                   onclick="toggleOptions(this)">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </a>
                                                <ul class="toggle-options-list">
                                                    <li><a onclick="getSprint('<?= $sprint[$s]['id_sprint'] ?>');"
                                                           href="javascript:;">Edit</a></li>
                                                    <li><a href="javascript:;">Delete</a></li>
                                                </ul>
                                                <label>
                            	    <span class=""><?= date('d-m-Y', strtotime($sprint[$s]['start_date'])) ?>
                                        <i class="">Start Date</i>
                                    </span>
                                    <span class=""><?= date('d-m-Y', strtotime($sprint[$s]['end_date'])) ?>
                                        <i class="">End Date</i></span>
                                                    <?php
                                                    $datediff = (strtotime($sprint[$s]['end_date']) - strtotime($sprint[$s]['start_date']));
                                                    $diff = floor($datediff / (60 * 60 * 24));
                                                    ?>
                                                    <span><?php if ($diff > 0) {
                                                            echo $diff;
                                                        } else {
                                                            echo 0;
                                                        } ?>
                                                        <i class="">Days Left</i></span>
                                                </label>
                                                <div class="progress-bar">
                                                    <span data-percentage="40"></span>
                                                </div>
                                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                                    <div class="col-sm-12 clearfix clearboth padding0  text-right">
                                                        <a class="link mr5">
                                                            <i class="fa fa-files-o"></i>
                                                        </a>
                                                        <b>5</b>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        <?php } ?>
                        <div class="col-xs-8 col-lg-9 padding0">
                            <!-- Tab panes -->
                            <div class="tab-content" id="task-tab-content">

                            </div>
                        </div>
                    </div>

                    <!--SPRINT ADD POP-UP-->


                </div>
                <div id="time" class="tab-pane padding10">
                    TIME CONTENT
                </div>
                <div id="workload" class="tab-pane padding10">
                    WORKLOAD CONTENT
                </div>
                <div id="billing" class="tab-pane padding10">
                    BILLING CONTENT
                </div>
                <div id="config" class="tab-pane padding10">
                    CONFIGURATION CONTENT
                </div>
            </div>
        </div>


        <div id="sprint-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error">
                <div>
                    <form id="sprint_frm" class="NewErrorStyle" name="sprint_frm" method="post"
                          action="<?= WEB_BASE_URL ?>index.php/Project/addSprint">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                            class="glyphicon glyphicon-remove"></i></a></div>
                                <h4 class="panel-title">Add Sprint</h4>
                            </div>
                            <div class="panel-body align-error">

                                <div class="col-sm-12 clearfix clearboth">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <input type="text" name="name" id="name" class="form-control rval"/>
                                        </div>
                                        <label class="control-label">Name :</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <input type="text" name="start_date" id="start_date" value="" startDate="0"
                                                   class="form-control rval tssDatepicker"/>
                                        </div>
                                        <label class="control-label">Start Date :</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <input type="text" name="end_date" id="end_date" value="" startDate="0"
                                                   class="form-control rval tssDatepicker"/>
                                        </div>
                                        <label class="control-label">End Date :</label>
                                    </div>
                                </div>
                                <input type="hidden" name="project_id" id="project_id" value="<?= $project_id ?>">
                                <input type="hidden" name="id_sprint" id="id_sprint">

                            </div>
                            <div class="panel-footer text-center">
                                <button type="submit" class="button-common" id="addSprint">Save</button>
                                <button class="button-color" data-dismiss="modal">Cancel</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--MODAL POPS-->
        <div id="status-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="status-form">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Change Status</h4>
                        </div>
                        <input type="hidden" id="Id" name="Id" value="0"/>
                        <div class="panel-body align-error">
                            <div class="col-sm-12 padding0 clearfix clearboth">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval" name="Status" id="Status">
                                                <option value="">--Select--</option>
                                                <option value="open">Open</option>
                                                <option value="inprogress">In Progress</option>
                                                <option value="completed">Done</option>
                                                <option value="reopen">Reopen</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Status :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 padding0 tbleWdth clearboth clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <textarea class="form-control" name="Comments"></textarea>
                                        </div>
                                        <label class="control-label">Comments :</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common panelbtn" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="priorities-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Change priority</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 padding0 clearfix clearboth">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval" name="Priority" id="Priority">
                                                <option value="">--Select--</option>
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Priority :</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common panelbtn" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<link rel="stylesheet" type="text/css" href="../Content/CSS/bootstrap-fileupload.min.css">
        <script type="text/javascript" src="../Scripts/bootstrap-fileupload.min.js"></script>-->


        <div id="excel-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Tasklist</h4>
                        </div>
                        <div class="panel-body">

                            <form id="excelUploadForm" enctype="multipart/form-data" method="post"
                                  action="<?php echo site_url('/Welcome/excelTaskUpload') ?>">
                                <!--<input type="file" id="file" name="file"/>-->
                                <div class="col-sm-6">
                                    <div class="fileupload fileupload-new input-group" data-provides="fileupload">
                                        <div class="form-control" data-trigger="fileupload"><i
                                                class="glyphicon glyphicon-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span
                                            class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" id="file" name="file"></span>
                                        <a href="#" class="input-group-addon btn btn-default fileupload-exists"
                                           data-dismiss="fileupload">Remove</a>
                                    </div>
                                    <a class="link font-12" href="<?= WEB_BASE_URL ?>uploads/sample_task_sheet.xlsx"
                                       download target="_blank">Download Sample Excel File</a>
                                </div>
                                <div class="col-sm-4">
                                    <button class="button-common" type="submit" id="uploadExcel"> Upload</button>
                                </div>
                            </form>
                            <div id="ExcelRecords">

                            </div>

                        </div>
                        <div class="panel-footer text-center">

                            <button id="submitList" class="button-common" div-submit="true">Submit</button>
                            <button id="submitTaskOk" class="button-color" data-dismiss="modal">Ok</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <script>
            var filterStr = '';
            TssLib.docReady(function () {
                $('input[name="employees"]').jsonSuggest({
                    data:<?php echo json_encode($users, JSON_PRETTY_PRINT); ?>,//filterStr
                    showLoader: false,
                    showDataHeader: ['Name', 'Email'],
                    showData: ['first_name', 'email'],
                    customKeyToSelect: 'email',
                    customValueToSelect: 'id_user',
                    valueFld: $('input[name="employeesId"]'),
                    haveKeyValue: true,
                    searchInCustomKey: false,
                    minCharacters: 2,
                    width: 300,
                    maxResults: 50
                });

                $("#backlogTab").trigger("click");
                $('a[tab-context="tasks"]').trigger('click');

                $.each($('body').find('.progress-bar'), function (i, o) {
                    var _percentage = parseInt("10", $(o).find('span').attr('data-percentage'));
                    $(o).find('span').css({'width': _percentage});
                })
                <?php if($this->session->userdata('user_type_id') == 3): ?>
                $.get('<?php echo base_url('index.php/Project/sprintDetails/' . $project_id . '/') ?>', function (data) {
                    $('#task-tab-content').html(data);
                });
                <?php endif; ?>
            });
            function sprintTypeChange(sel) {

                <?php if($this->session->userdata('user_type_id') == 3){ ?>
                $.get('<?php echo base_url('index.php/Project/sprintDetails/' . $project_id ) ?>'+'/0/'+sel.value, function (data) {
                    $('#task-tab-content').html(data);
                });
                <?php }else { ?>
                $url = $('#sprintsTabs').find('li.active>a').attr('href');
                $.get($url + '/' + sel.value, function (data) {
                    $('#task-tab-content').html(data);
                });
                <?php } ?>
            }
            function changeTaskStatus(task_id,status,project_id)
            {
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: WEB_BASE_URL+'index.php/Project/changeTaskStatus',
                    dataType: 'json',
                    data: {id_task_workflow:task_id,status:status},
                    success:function(res){
                        if(res.status)
                        {
                            var typeValue = $('#sprintTypeDropdown').val();
                            <?php if($this->session->userdata('user_type_id') == 3){ ?>
                            $.get('<?php echo base_url('index.php/Project/sprintDetails/' . $project_id ) ?>'+'/0/'+typeValue, function (data) {
                                $('#task-tab-content').html(data);
                            });
                            <?php }else { ?>
                            $url = $('#sprintsTabs').find('li.active>a').attr('href');
                            $.get($url + '/' + typeValue, function (data) {
                                $('#task-tab-content').html(data);
                            });
                            <?php } ?>

                          /*  $.get(WEB_BASE_URL+'index.php/Project/sprintDetails/'+project_id+'/', function(data) {
                                $('#task-tab-content').html(data);
                            });
*/
                            TssLib.notify('Task status updated Successfully');
                        }
                        else{
                            TssLib.notify(res.data, 'warn', 5);
                        }
                    }
                });
            }

            function removeMember(e) {

                var proUserId = $(e).closest('li').attr('pro-user-id');
                TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                    $(this).closest('.modal').modal('hide');
                    getJsonAsyncWithBaseUrl("Project/projectUserDelete/" + proUserId, {}, {
                        jsonContent: true,
                        callback: function (result) {
                            if (result.data.status) {
                                $(e).closest('li').remove();
                                TssLib.notify('Deleted successfully ', null, 5);
                            }
                        }
                    });
                }, 'Yes', 'No');
            }
            function addMember(e) {
                var _memberVal = $(e).parent().find('[name="employeesId"]').val();
                var _memberName = $(e).parent().find('[name="employeesId"]').attr('key-data');
                if (_memberName.length > 0) {
                    postJsonAsyncWithBaseUrl("Project/addMember", {
                        'project_id':<?php echo $project_id; ?>,
                        'employee_id': _memberVal
                    }, {
                        jsonContent: true,
                        callback: function (result) {
                            //console.log(result);
                            if (result.data.status) {
                                $('#teamMembers').append('<li><label>' + _memberName + '</label><a onclick="removeMember(this);"><i class="fa fa-times"></i></a></li>');
                                $(e).parent().find('[name="employees"]').val('').focus();
                                TssLib.notify('added successfully ', null, 5);
                            } else {
                                TssLib.notify('employee already exist ', 'warn', 5);
                            }
                        }
                    });
                } else {
                    TssLib.notify('Please Enter Team Member Name', 'warn', 5);
                }
            }

    /*        $(window).on('load resize', function () {
                $('.tasks-list').width($('.tasks-col').length * 205);
                $('.vScroll').css('max-height', 400)
                $('.vScroll').mCustomScrollbar({
                    theme: "dark",
                    scrollbarPosition: 'inside',
                    autoHideScrollbar: true,
                    scrollButtons: {enable: true},
                    scrollInertia: 3,
                    autoExpandScrollbar: false
                });
                $('.hScroll').mCustomScrollbar({
                    axis: "x",
                    theme: "dark",
                    scrollButtons: {
                        enable: true,
                        scrollAmount: 100
                    },
                    scrollInertia: 10,
                    scrollEasing: "easeInOut",
                    autoExpandScrollbar: true
                });
            });*/
            function toggleOptions(e) {
                if (!$(e).hasClass('chooseNow')) {
                    $(e).addClass('chooseNow');
                    $('body').find('.toggle-options-list').hide();
                    $(e).next('.toggle-options-list').fadeIn();
                } else {
                    $(e).removeClass('chooseNow');
                    $(e).next('.toggle-options-list').hide();
                }
            }

            function addSprint(e) {
                var _sprintName = $(e).prev().val();

                if (_sprintName.length > 0) {
                    var $modal = TssLib.openModel({width: 550}, $('#sprint-modal').html());
                    var _inputSprintName, _startDateVal, _endDateVal;
                    $modal.find('[name="name"]').val(_sprintName);
                    $modal.find('[name="id_sprint"]').val(0);
                    TssLib.ajaxForm({
                        form: $modal.find('#sprint_frm'), callback: function (result) {
                            if (result.data.status) {
                                TssLib.closeModal();
                                location.reload();
                            }
                            else {
                                TssLib.notify(result.data.message, 'warn', 5);
                            }
                        },
                        before: function (formObj) {
                            $(formObj).data('additionalData', {
                                'start_date': $modal.find('[name="start_date"]').val(),
                                'end_date': $modal.find('[name="end_date"]').val()
                            });
                            $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/addSprint');
                            return true;
                        }
                    });
                }
                else {
                    TssLib.notify('Enter Sprint Name', 'warn', 5)
                }
            }

            function getSprint(id) {
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: WEB_BASE_URL + 'index.php/Project/getSprintAjax',
                    dataType: 'json',
                    data: {id: id},
                    success: function (res) {
                        if (res.response == 1) {
                            var data = res.data;

                            var $modal = TssLib.openModel({width: 550}, $('#sprint-modal').html());
                            var _inputSprintName, _startDateVal, _endDateVal;
                            $modal.find('[name="name"]').val(data.name);
                            $modal.find('[name="id_sprint"]').val(data.id_sprint);
                            $modal.find('[name="start_date"]').val(data.start_date);
                            $modal.find('[name="end_date"]').val(data.end_date);

                            TssLib.ajaxForm({
                                form: $modal.find('#sprint_frm'), callback: function (result) {
                                    if (result.data.status) {
                                        TssLib.closeModal();
                                        location.reload();
                                    }
                                    else {
                                        TssLib.notify(result.data.message, 'warn', 5);
                                    }
                                },
                                before: function (formObj) {
                                    $(formObj).data('additionalData', {
                                        'start_date': $modal.find('[name="start_date"]').val(),
                                        'end_date': $modal.find('[name="end_date"]').val()
                                    });
                                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/addSprint');
                                    return true;
                                }
                            });
                        }
                    }
                });
            }

            function uploadExcelModal(e) {
                var $modal = TssLib.openModel({width: 1024}, $('#excel-modal').html());
                $modal.find('#submitList').hide();
                $modal.find('#submitTaskOk').hide();
                $modal.find('#excelUploadForm').on('submit', function (e) {
                    e.preventDefault();

                    var formData = new FormData($(this)[0]);

                    $.ajax({
                        url: '<?php echo site_url('/Welcome/excelTaskUpload') ?>',
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if (data.status) {
                                $modal.find('#submitList').show();
                                $modal.find('#ExcelRecords').html(data.data);
                            } else {
                                $modal.find('#ExcelRecords').html(data.data);
                            }


                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                    return false;
                });
                $modal.find('#submitList').on('click', function () {
                    $('body').attr('name', 'no-loader');
                    // event.preventDefault();
                    $formFields = $("#saveTaskList").serializeArray();
                    // console.log('$formFields', $formFields);
                    var taskList = [];
                    $.each($('#taskList').find('tbody > tr'), function () {
                        var $td = $('td', this);
                        var $obj = {};
                        $obj['project_name'] = $td.eq(1).find('input').val();
                        $obj['module_name'] = $td.eq(2).find('input').val();
                        $obj['task_name'] = $td.eq(3).find('input').val();
                        $obj['start_date'] = $td.eq(4).find('input').val();
                        $obj['end_date'] = $td.eq(5).find('input').val();
                        $obj['estimated_time'] = $td.eq(6).find('input').val();
                        $obj['users'] = $td.eq(7).find('input').val();
                        $obj['description'] = $td.eq(8).find('input').val();
                        taskList.push($obj);

                    })
                    //console.log('taskList',taskList);
                    $.each($(taskList), function (ind, item) {
                        //console.log('item', item);
                        $('#taskList').find("#statusTD_" + ind).html('<i class="fa fa-spinner"></i>');
                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url('index.php/Welcome/excelTaskUploadAjax')?>',
                            data: item,
                            dataType: 'json',
                            async: true,
                            success: function (result) {
                                // console.log('tasssss', result);
                                statusChang(result, ind);
                                if (ind == ($(taskList).length - 1)) {
                                    $("#backlogTab").trigger("click");
                                    $modal.find('#submitList').hide();
                                    $('body').removeAttr('name', 'no-loader');
                                    $modal.find('#submitTaskOk').show();
                                    TssLib.notify('Task List Updated', null, 5);
                                }
                                //$('#taskList').find("#statusTD_"+ind).find('i.fa').fadeIn(100*ind);
                            }
                        });
                    })
                })
            }


            function statusChang(result, ind) {
                // console.log('    result', result);
                $('.panel-body').scrollTop(ind * 60);
                /*$('.panel-body').animate({
                 scrollTop: ind*40
                 });*/
                if (result.status) {
                    // $('#taskList').find("#statusTD_" + ind).html('<i class="fa fa-check green" ></i>' + result.message);
                    $('#taskList').find("#statusTD_" + ind).html(result.message);
                    $('#taskList').find("#rowTR_" + ind).addClass('success-bg');
                } else {
                    $('#taskList').find("#statusTD_" + ind).html(result.message);
                    $('#taskList').find("#rowTR_" + ind).addClass('error-bg')
                }
            }


            function toggleOptions(e) {
                if (!$(e).hasClass('chooseNow')) {
                    $(e).addClass('chooseNow');
                    $('body').find('.toggle-options-list').hide();
                    $(e).next('.toggle-options-list').fadeIn();
                } else {
                    $(e).removeClass('chooseNow');
                    $(e).next('.toggle-options-list').hide();
                }
            }
            $('[data-toggle="tabajax"]').click(function (e) {
                var $this = $(this),
                    loadurl = $this.attr('href'),
                    targ = $this.attr('data-target');

                $.get(loadurl, function (data) {
                    $(targ).html(data);
                });

                $this.tab('show');
                return false;
            });
        </script>
        <!-- InstanceEndEditable -->

    </div>
</div>