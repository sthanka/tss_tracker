<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance_Modal extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    public function checkAttendanceExist($data){
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->where(array(
            'user_id' => $data['user_id'],
            'date' => $data['date']
        ));
        $result = $this->db->get()->result();
        if(count($result) > 0){
            return $result[0]->id_attendance;
        }else{
            return false;
        }
    }

    public function addAttendance($data)
    {
        $this->db->insert('attendance', $data);
        return $this->db->insert_id();
    }

    public function updateAttendance($id_attendance , $data){
        $this->db->where('id_attendance', $id_attendance);
        $this->db->update('attendance', $data);
        return $id_attendance;
    }

    public function addPunchData($data)
    {
        $this->db->insert_batch('swipe_punch', $data);
        return $this->db->insert_id();
    }

    public function updatePunchData($id_swipe_punch , $data){
        $this->db->where('id_swipe_punch', $id_swipe_punch);
        $this->db->update('swipe_punch', $data);
        return $id_swipe_punch;
    }

    public function deletePunchData($data){
        if(isset($data['attendance_id'])){
            $this->db->where('attendance_id', $data['attendance_id']);
        }
        if(isset($data['id_swipe_punch'])){
            $this->db->where('id_swipe_punch', $data['id_swipe_punch']);
        }
        $this->db->delete('swipe_punch');
    }

    public function getAttendanceGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = $paramArr['sidx'] != ''?$paramArr['sidx']:'id_attendance';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(isset($paramArr['sdate']) && isset($paramArr['edate'])){
            /*$date = date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$paramArr['date'])));*/
            $date = 1;
        }
        else{ $date = 0; }

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = array(0); }

        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam);
            $timeParams = "";
            if(preg_match_all('/:/', $whereParam)){
                $timeParams = " || a.start_time like '%".$whereParam."%' || a.end_time like '%".$whereParam."%' || sp.in_time like '%".$whereParam."%' || sp.out_time like '%".$whereParam."%' || a.duration like '%".$whereParam."%' || sp.duration like '%".$whereParam."%' ";
            }
            $whereParam = " AND (a.emp_code LIKE '%".$whereParam."%' || u.first_name LIKE '%".$whereParam."%' || u.last_name LIKE '%".$whereParam."%' || a.status LIKE '%".$whereParam."%' || DATE_FORMAT(a.date,'%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') $timeParams )";
        }

        if(isset($paramArr['user_id'])) {
            $whereParam .= ' AND `a`.`user_id` in (' . join(', ', ($user_id)) . ')';
        }
        if($date!=0) {
            $whereParam .= ' AND `a`.`date` between "' . date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$paramArr['sdate']))) . '" AND " '.date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$paramArr['edate']))) .'"';
        }

        $whereClause = "where true ".$whereParam;

        if($sortField=='task_start_date')
            $sortField = " STR_TO_DATE(a.date,\"%d-%b-%Y\") ";

        /*$SQL = "SELECT a.id_attendance, a.user_id, a.emp_code, DATE_FORMAT(a.date, \"%d-%b-%Y\") as attend_date, a.start_time, a.end_time, a.`status`, a.duration day_duration,*/
        $SQL = "SELECT a.id_attendance, a.user_id, a.emp_code, DATE_FORMAT(a.date, \"%d-%b-%Y\") as attend_date, a.start_time, a.end_time, a.`status`, a.duration day_duration, a.punch_time,
                 sp.id_swipe_punch , sp.in_time, sp.out_time, sp.duration,
                 concat_ws(' ',u.first_name,u.last_name) name
                 FROM attendance a
                LEFT JOIN swipe_punch sp ON a.id_attendance=sp.attendance_id
                LEFT JOIN user u ON a.user_id=u.id_user
                $whereClause group by a.id_attendance order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT count(*)
                 FROM attendance a
                LEFT JOIN swipe_punch sp ON a.id_attendance=sp.attendance_id
                LEFT JOIN user u ON a.user_id=u.id_user
                $whereClause group by a.id_attendance ";
        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $final = $list = $result->result();
            foreach($list as $k=>$v){
                $swipeData = $this->getSwapTimeByAttendanceId(array('attendance_id' => $v->id_attendance));
                if(count($swipeData)>0){
                    $total = 0;
                    foreach($swipeData as $k1=>$v1){
                        $total += time_to_sec($v1->duration);
                    }
                    $final[$k]->total_duration = sec_to_time($total);
                }else{
                    $final[$k]->total_duration = '00:00:00';
                }
            }
            return array('rows'=>$final,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getAttendanceLatestGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = $paramArr['sidx'] != ''?$paramArr['sidx']:'id_attendance';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(isset($paramArr['sdate']) && isset($paramArr['edate'])){
            $date = date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$paramArr['date'])));
        }
        else{ $date = 0; }

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = array(0); }

        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam);
            $timeParams = "";
            if(preg_match_all('/:/', $whereParam)){
                $timeParams = " || a.start_time like '%".$whereParam."%' || a.end_time like '%".$whereParam."%' || sp.in_time like '%".$whereParam."%' || sp.out_time like '%".$whereParam."%' || a.duration like '%".$whereParam."%' || sp.duration like '%".$whereParam."%' ";
            }
            $whereParam = " AND (a.emp_code LIKE '%".$whereParam."%' || u.first_name LIKE '%".$whereParam."%' || u.last_name LIKE '%".$whereParam."%' || a.status LIKE '%".$whereParam."%' $timeParams )";
        }

        if(isset($paramArr['user_id'])) {
            $whereParam .= ' AND `a`.`user_id` in (' . join(', ', ($user_id)) . ')';
        }
        if($date!=0) {
            $whereParam .= ' AND `a`.`date` between "' . date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$paramArr['sdate']))) . '" AND " '.date('Y-m-d 00:00:00',strtotime(str_replace('/','-',$paramArr['edate']))) .'"';
        }

        $whereClause = "where true AND `a`.`date` IN ( SELECT MAX(date) from attendance ) ".$whereParam;

        if($sortField=='task_start_date')
            $sortField = " STR_TO_DATE(a.date,\"%d-%b-%Y\") ";

        $SQL = "SELECT a.id_attendance, a.user_id, a.emp_code, DATE_FORMAT(a.date, \"%d-%b-%Y\") as attend_date, a.start_time, a.end_time, a.`status`, a.duration day_duration,
                 sp.id_swipe_punch , sp.in_time, sp.out_time, sp.duration,
                 concat_ws(' ',u.first_name,u.last_name) name
                 FROM attendance a
                LEFT JOIN swipe_punch sp ON a.id_attendance=sp.attendance_id
                LEFT JOIN user u ON a.user_id=u.id_user
                $whereClause group by a.id_attendance order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT count(*)
                 FROM attendance a
                LEFT JOIN swipe_punch sp ON a.id_attendance=sp.attendance_id
                LEFT JOIN user u ON a.user_id=u.id_user
                $whereClause group by a.id_attendance ";
        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $final = $list = $result->result();
            foreach($list as $k=>$v){
                $swipeData = $this->getSwapTimeByAttendanceId(array('attendance_id' => $v->id_attendance));
                if(count($swipeData)>0){
                    $total = 0;
                    foreach($swipeData as $k1=>$v1){
                        $total += time_to_sec($v1->duration);
                    }
                    $final[$k]->total_duration = sec_to_time($total);
                }else{
                    $final[$k]->total_duration = '00:00:00';
                }
            }
            return array('rows'=>$final,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    /*function getSwapTimeByAttendanceId($data){
        $this->db->select('*');
        $this->db->from('swipe_punch');

        if(isset($data['attendance_id']))
            $this->db->where('attendance_id', $data['attendance_id']);

        if(isset($data['user_id']))
            $this->db->where('user_id', $data['user_id']);

        $list = $this->db->get()->result();
        return $list;
    }*/
    function getSwapTimeByAttendanceId($data){
        $this->db->select('*');
        $this->db->from('attendance');

        if(isset($data['attendance_id']))
            $this->db->where('id_attendance', $data['attendance_id']);

        if(isset($data['user_id']))
            $this->db->where('user_id', $data['user_id']);

        $list = $this->db->get()->result();
        return $list;
    }

    public function getAttendanceById($data){
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->where(array(
            'id_attendance' => $data['id_attendance']
        ));
        $result = $this->db->get()->result();
        if(count($result) > 0){
            return $result;
        }else{
            return false;
        }
    }

    public function getIdealAttendance($data){
        $this->db->select('*');
        $this->db->from('attendance');
        if(isset($data['user_id']))
            $this->db->where('user_id', $data['user_id']);

        if(isset($data['start_date']) || isset($data['end_date']))
            $this->db->where('date between date("'.$data['start_date'].'") AND date("'.$data['end_date'].'") ');

        $result = $this->db->get()->result();
//        die($this->db->last_query());
        return $result;

    }

    public function getAttendanceByDate($data){
        $this->db->select('SEC_TO_TIME(sum(TIME_TO_SEC(duration))) duration');
        $this->db->from('attendance');
        if(isset($data['user_id']))
            $this->db->where('user_id', $data['user_id']);

        if(isset($data['start_date']) || isset($data['end_date']))
            $this->db->where('date between date("'.$data['start_date'].'") AND date("'.$data['end_date'].'") ');

        $result = $this->db->get()->result();
//        die($this->db->last_query());
        return $result;

    }

}