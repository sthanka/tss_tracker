
<?php
//echo "<pre>"; print_r($records); exit;
foreach($records as $record){
    //echo "<pre>"; print_r($record['kpis']); exit;
    ?>


<div class="clearfix workload-padd">
    <div class="user-workload border clearfix">
        <div class="col-20 widjet widjet-emp-info">
            <ul class="padding10">
                <li class="emp-id emp-present"><?=   $record['usersDetails'][0]['employee_id']==''?' ----- ':$record['usersDetails'][0]['employee_id']?></li>
                <li class="emp-details">
                    <span class="display-block"><?=  $record['usersDetails'][0]['first_name'].' '.$record['usersDetails'][0]['last_name'] ?></span>
                    <span class="display-block"><?=$this->session->userdata('department_name')?> <?php if($this->session->userdata('is_lead') == 1){ echo 'Team Head'; } ?></span>
                </li>
                <?php if($record['percentage'] > 60){ $text = 'good';}else if($record['percentage'] < 30){  $text = 'bad'; }else{  $text = 'average'; } ?>
                <li><span class="p-status-<?=$text?>"><?=ucfirst($text)?></span></li>
                <li class="emp-wload">
                                    	<span>
                                            <span class="" style="width:60%;float:left;"><span class="display-block"><?=$record['work_load']?></span><span
                                                    class="display-block">workload</span></span>
                                            <span class="emp-wload-percent" style="40%"><?=$record['percentage']?>%</span>
                                        </span>
                </li>
                <li><span class=""><i class="fa fa-envelope-o mr10 blue-color"></i><?= $record['usersDetails'][0]['email'] ?></span>
                </li>
                <li><span><i class="fa fa-phone  mr10 blue-color"></i><?= $record['usersDetails'][0]['phone_number']==''?' --- ':$record['usersDetails'][0]['phone_number'] ?></span></li>
            </ul>
        </div>
        <div class="col-60 widjet widjet-emp-weekstatus">
            <?php if($this->session->userdata('user_type_id')!=2){ ?>
                <div class="emp-weekstatus">
                    <div class="blue-color">
                        <span><?= $record['taskStatus']['new']['y'] ?></span>
                        <span>New</span>
                    </div>
                    <div class="orange-color">
                        <span><?= $record['taskStatus']['progress']['y'] ?></span>
                        <span>In Progress</span>
                    </div>
                    <div class="red-color">
                        <span><?= $record['taskStatus']['hold']['y'] ?></span>
                        <span>On Hold</span>
                    </div>
                    <div class="green-color">
                        <span><?= $record['taskStatus']['completed']['y'] ?></span>
                        <span>Completed</span>
                    </div>
                    <div>
                        <span><?= $record['proCount'] ?></span>
                        <span>Projects</span>
                    </div>
                </div>
            <?php } else { ?>
                <div class="emp-weekstatus">
                    <div class="blue-color cursor-pointer" onclick="getUserTasks('new','<?= $record['taskStatus']['new']['y'] ?>',<?= $record['usersDetails'][0]['id_user'] ?>);">
                        <span><?= $record['taskStatus']['new']['y'] ?></span>
                        <span>New</span>
                    </div>
                    <div class="orange-color cursor-pointer" onclick="getUserTasks('progress','<?= $record['taskStatus']['progress']['y'] ?>',<?= $record['usersDetails'][0]['id_user'] ?>);">
                        <span><?= $record['taskStatus']['progress']['y'] ?></span>
                        <span>In Progress</span>
                    </div>
                    <div class="red-color cursor-pointer" onclick="getUserTasks('hold','<?= $record['taskStatus']['hold']['y'] ?>',<?= $record['usersDetails'][0]['id_user'] ?>);">
                        <span><?= $record['taskStatus']['hold']['y'] ?></span>
                        <span>On Hold</span>
                    </div>
                    <div class="green-color cursor-pointer" onclick="getUserTasks('completed',<?= $record['taskStatus']['completed']['y'] ?>,<?= $record['usersDetails'][0]['id_user'] ?>);">
                        <span><?= $record['taskStatus']['completed']['y'] ?></span>
                        <span>Completed</span>
                    </div>
                    <div>
                        <span><?= $record['proCount'] ?></span>
                        <span>Projects</span>
                    </div>
                </div>
            <?php } ?>
            <div class="emp-weekstatus-graphs padding10">
                <div class="">
                    <div data-days='<?=json_encode($record['days'])?>' data-daysLogs='<?=json_encode($record['daysLogs'])?>' class="week-status-graph" style="height:150px; margin: 0 auto;width:100%;"></div>
                </div>

            </div>
        </div>
        <!--<div class="col-20 widjet widjet-emp-taskstatus-grpah">
            <div class="padding10">
                <h4 class="margin0 padding10">Task Status</h4>

                <div class="">
                    <div data-chartdata='<?php /*echo json_encode([["name"=>$record['taskStatus']["new"]["name"],"y"=>$record['taskStatus']["new"]["y"],"color"=>$record['taskStatus']["new"]["color"]],["name"=>$record['taskStatus']["progress"]["name"],"y"=>$record['taskStatus']["progress"]["y"],"color"=>$record['taskStatus']["progress"]["color"]],["name"=>$record['taskStatus']["hold"]["name"],"y"=>$record['taskStatus']["hold"]["y"],"color"=>$record['taskStatus']["hold"]["color"]],["name"=>$record['taskStatus']["completed"]["name"],"y"=>$record['taskStatus']["completed"]["y"],"color"=>$record['taskStatus']["completed"]["color"]]]);*/?>' id="taskstatusDonut_chart" class="task-status-chart"
                         style="height:150px; margin: 0 auto;width: 100%;"></div>
                </div>
            </div>
        </div>-->
        <div class="col-20 widjet widjet-emp-wroking-projects">
            <ul>
                <h4 class="border-bottom margin0 padding10">My KPI's</h4>
                <li class=""><span>Productive Time</span><span><?= $record['kpis']['billable']['duration']?>(h:m)</span></li>
                <li class=""><span>Non Productive</span><span><?= $record['kpis']['non-billable']['duration']?>(h:m)</span></li>
                <li class=""><span>Other Activities</span><span><?= $record['kpis']['other']['duration']?>(h:m)</span></li>
                <li class=""><span>On Time Delivery</span><span><?=$record['on_time_delivery']?></span></li>
                <li class=""><span># Milestone Miss</span><span><?=$record['milestone_miss']?></span></li>
            </ul>
        </div>
    </div>
</div>

<?php  } ?>
<script>
    $(function () {

        var _maxheight = 0;
        $('.user-workload .widjet').each(function (i, o) {
            if ($(o).height() > _maxheight) {
                _maxheight = $(o).height();
            }
        });
        $('.user-workload .widjet').height(_maxheight);

/*        $(window).on('load resize', function () {*/
            $('.week-status-graph').each(function () {

                var daysLogs =  $(this).attr('data-daysLogs');
                daysLogs =   jQuery.parseJSON(daysLogs);

//                console.log(daysLogs);
                var alarteed = [], assigne = [];
                $.each( daysLogs, function( key, value ) {
                    alarteed.push(parseFloat(value.allowted_time.replace(":", ".")));
                    assigne.push(parseFloat(value.actual_time.replace(":", ".")));
                });
                var currentDays =  $(this).attr('data-days');
                currentDays =   jQuery.parseJSON(currentDays);

                $(this).highcharts({
                    chart: {
                        type: 'column',
                        spacing: [0, 0, 0, 0]
                    },
                    title: false,
                    xAxis: {
                        categories: currentDays
                    },
                    yAxis: [{
                        min: 0,
                        title: false,
                        tickInterval:1,
                        labels: {
                            enabled: false
                        }
                    }, {
                        title: false,
                        opposite: true
                    }],
                    legend: {
                        enabled: false
                    },

                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            grouping: false,
                            shadow: false,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Alloted',
                        color: '#0072BB',
                        data: alarteed,
                        pointPadding: 0.3,
                        pointPlacement: -0.2
                    }, {
                        name: 'Actual',
                        color: '#5CBA5C',
                        data: assigne,
                        pointPadding: 0.4,
                        pointPlacement: -0.2
                    }]
                });
            });

            $('.task-status-chart').each(function () {
                var datacht = $(this).attr('data-chartdata');
                datacht = jQuery.parseJSON(datacht);
                var arra = [];
                $.each( datacht, function( key, value ) {
                    arra.push({name:value.name,y:parseFloat(value.y),color:value.color});
                });
                datacht = arra;
                $(this).highcharts({
                    chart: {
                        type: 'pie',
                        style: {
                            fontFamily: "Lato,sans-serif"
                        },
                        spacing: [0, 0, 0, 0]
                    },
                    title: true,
                    plotOptions: {
                        pie: {
                            innerSize: 70,
                            depth: 0,
                            showInLegend: true,
                            margin: 0,
                            padding: 0,
                            spacing: [0, 0, 0, 0],
                            dataLabels: {
                                enabled: false
                            }
                        }
                    },
                    legend: {
                        align: "right",
                        verticalAlign: "middle",
                        layout: "vertical",
                        margin: 0,
                        padding: 0,
                        spacing: [0, 0, 0, 0]
                    },
                    tooltip: {
                        shared: true,
                        backgroundColor: '#e8e8e8',
                        borderColor: 'transparent'
                    },
                    series: [{
                        name: 'Task',
                        size: '100%',
                        innerSize: '40%',
                        data: datacht
                    }]
                });

            });
        });
   /* });*/
</script>

