
    <div class="modal fade" id="oak_popup">
        <div class="modal-dialog model-mg-width">
            <div class="modal-content model-mg-content"></div>
        </div>
    </div>
    <!-- Modal Dialog  dekete confirmation-->
    <div class="modal fade" id="oasis-confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </div>
                        <h4 class="panel-title"></h4>
                    </div>
                    <div class="panel panel-default panel-body padding20 clearfix">
                        <p></p>
                    </div>
                    <div class="panel-footer col-sm-12 clearfix text-right">
                        <button type="button" class="button-common module btn-danger">Yes</button>
                        <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--@RenderBody()-->
    <!--Render Body Start-->
    <div class="mainpanel" id="budgetTemplateTbl_wrapper">
        <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
            <div class="contentHeader">
                <h3>Users</h3>
            </div>
            <div class="col-sm-12 clearfix clearboth pad-right-none">
                <div class="grid-details-table">
                    <div class="grid-details-table-header">
                        <h3>Users List</h3>
                        <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="grid-details-table-content clearfix padding0">
                        <div class="tbl_wrapper border0">
                            <table id="project-list" class="table-responsive"></table>
                            <div id="project-list-page"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="add-user-modal" class="modal-wrapper" style="display:none">
                <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#" onclick="cancelAddUser(this)"><i class="glyphicon glyphicon-remove"></i></a> </div>
                                <h4 class="panel-title">Add User</h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval" name="first_name" id="first_name"  />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>First Name :</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval" name="last_name"  id="last_name" />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Last Name :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval" name="email" rules="email" id="email" />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>email :</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval pInt" id="phone_number" name="phone_number"  />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Phone :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval pInt" name="employee_id"  id="employee_id" />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Employee Id :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth padding0">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <label><input type="radio"  id="gender" name="gender"  value="male" checked /> Male</label>
                                                <label><input type="radio"  id="gender" name="gender"  value="female" /> Female</label>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>gender :</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container mt10">
                                                <select class="rval" name="status" id="status">
                                                    <option value="0">Inactive</option>
                                                    <option value="1">Active</option>
                                                </select>
                                            </div>
                                            <label class="control-label">Status :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 pt10 pb10 clearfix clearboth" >
                                    <div id="added-departments-list" ></div>
                                    <button type="button" class="btn btn-xs button-common f10" onclick="toggleAddDepartment(0,0)">Add Department</button>
                                    <div id="add-user-department" class="clearfix">

                                    </div>
                                    <input type="hidden" name="department_id" id="department_id_hidden" />
                                </div>
                                <div class="col-sm-12 pt10 pb10 clearfix clearboth">
                                    <div id="userModuleList"></div>
                                </div>
                                <input type="hidden" class="form-control rval" name="id_user" id="id_user" />
                            </div>
                            </div>

                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal" onclick="cancelAddUser(this)">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <script>
                var departmentNumber = 0;
                var reference = "Allotted";
                TssLib.docReady(function () {
                    var $refTblgrid = $('#project-list');
                    $refTblgrid.jqGrid({
                        url: TssConfig.TT_SERVICE_URL + 'user/getUsersGrid',
                        multiselect: false,
                        datatype: "json",
                        sortorder: "desc",
                        extSearchField: '.searchInput',
                        colNames: ['Id','first_name','last_name','status','gender', 'Name','Email', 'Contact No', 'Team leader', 'Status', 'employee_id'],
                        colModel: [
                            { name: 'id_user', index: 'id_user', hidden: true ,key:true},
//                            { name: 'department_id', index: 'department_id', hidden: true },
                            { name: 'first_name', index: 'first_name', hidden: true },
                            { name: 'last_name', index: 'last_name', hidden: true },
                            { name: 'status', index: 'status', hidden: true },
                            { name: 'gender', index: 'gender', hidden: true },
//                            { name: 'lead', index: 'lead', hidden: true },
                            { name: 'name', index: 'first_name' },
                            { name: 'email', index: 'email' },
                            { name: 'phone_number', index: 'phone_number' },
//                            { name: 'department_name', index: 'department_name', hidden: true },
                            { name: 'is_lead', index: 'is_lead', hidden: false,sortable: false, formatter: function (c, o, d) {
                                if(d.is_lead == 1){ var sts = 'Yes';}else{ var sts = '';}
                                return  sts;
                            }},
                            { name: 'epm_status', index: 'status' ,formatter: function (c, o, d) {
                                if(d.status == 1){ var sts = 'Active';}else{ var sts = 'Inactive';}
                                return  sts;
                            }},
                            { name: 'employee_id', index: 'employee_id', hidden: true }
                        ],
                        pager: 'project-list-page'
                    }).navGrid('#project-list-page', {
                        edit: true, add: true, del: true, search: false, refresh: true
                        ,addfunc: function () {
                            addEditCreateSchedule();
                        }, editfunc: function (id) {
                            var rowData = $refTblgrid.jqGrid('getRowData', id);
                            addEditCreateSchedule(rowData);
                        }, delfunc: function (id) {
                            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                                $(this).closest('.modal').modal('hide');
                                postJsonAsyncWithBaseUrl("User/deleteUserById", {'user_id': id}, {
                                    jsonContent: true,
                                    callback: function (result) {
                                        if (result.data != null) {
                                            if(result.data.status){
                                                TssLib.notify('Deleted successfully', null, 5);
                                                $('#project-list').trigger('reloadGrid');
                                            }else{
                                                TssLib.notify(result.data.message, 'warn', 5);
                                            }

                                        }
                                    }});
                            }, 'Yes', 'No');
                        }
                    });
                });
                function getUserModuleList($modal , userId){
                    $.post("<?=site_url('User/getAllUserModule')?>" , {'user_id': userId}, function( data ) {
                        $modal.find('#userModuleList').html(data);
                    });
                }
                function addEditCreateSchedule(rowData) {
                    var $modal = TssLib.openModel({ width: '600' }, $('#add-user-modal').html());
                    var id = '';
                    if(typeof rowData == 'undefined'){
                        id = 0;
                    }else{
                        id = rowData.id_user;
                    }
                    getUserModuleList($modal , id);
                    if (TssLib.isBlank(rowData)) {
                        $modal.find('.panel-title').text('Add User');
                        $modal.find('[div-submit="true"]').text('Save');
                        $modal.find('.status-drp').hide();
                    } else {
                        getJsonAsyncWithBaseUrl("User/getUserById?id_user="+rowData.id_user, {}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data != null) {
                                    var deptt = result.data;
                                    $modal.find('#status').val(rowData.status);
                                    $modal.find('#first_name').val(rowData.first_name);
                                    $modal.find('#last_name').val(rowData.last_name);
                                    $modal.find('#email').val(rowData.email);
                                    $modal.find('#employee_id').val(rowData.employee_id);
                                    $modal.find('#phone_number').val(rowData.phone_number);
                                    $modal.find('#gender').val(rowData.gender);
                                    $modal.find('#id_user').val(rowData.id_user);

                                    cancelAddUser(this);
                                    for(var index in deptt){
                                        if(deptt[index].department_id!==null){
                                            if(deptt[index].is_lead==1){
                                                toggleAddDepartment(deptt[index].department_id, deptt[index].department_id);
                                            }else{
                                                toggleAddDepartment(deptt[index].department_id, 0);
                                            }
                                        }
                                    }

                                    $modal.find('.panel-title').text('Edit User');
                                    $modal.find('[div-submit="true"]').text('Update');
                                    $modal.find('.status-drp').hide();
                                }
                            }
                        });
                    }
                    TssLib.ajaxForm({
                        jsonContent: true,
                        form: $('#linen-createSchedule-form'), callback: function (result) {
                            if (result.data.status) {
                                $('#project-list').trigger('reloadGrid');
                                TssLib.closeModal();
                                if (TssLib.isBlank(rowData)) {
                                    TssLib.notify('User added successfully ', null, 5);
                                }else{
                                    TssLib.notify('User updated successfully ', null, 5);
                                }
                            }else{
                                TssLib.notify(result.data.message , 'warn', 5);
                            }
                            selectedDepartments = [];
                        },
                        before: function (formObj) {
                            var sThisVal = [];
                            //For User modules
                            $('input:checkbox.moduleListClass').each(function () {
                                sThisVal.push(this.checked ? $(this).attr('date-module-id') : "");
                            });

                            $('.department-class').each(function(index, obj) {
                                var status=true;
                                var dept_id = $(this).find('#department_id').select2("val");
                                if(dept_id == ''){ status=false }
                                var lead = $(this).find('.department_is_lead').prop('checked');
                                if(lead){ var lead = 1;  }else{ var lead = 0; }
                                if(status) { selectedDepartments.push({'department_id':dept_id,'is_lead':lead}); }
                            });
                            if(selectedDepartments.length==0){ TssLib.notify('Select Department' , 'warn', 5); return false; }
                            $(formObj).data('additionalData', { 'department_id': selectedDepartments, 'modules' : sThisVal});
                            $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'User/addUserData');
                            return true;
                        }
                    });
                }
                function dateFormat(c, o, d) {
                    if (TssLib.isBlank(c))
                        return '';
                    else
                        return c.DateWCF().format('d/m/Y');
                }
                function toggleAddDepartment(index, lead){
                    var _html = '';
                    postJsonAsyncWithBaseUrl("client/getClients", {}, {
                        jsonContent: true,
                        callback: function (result) {
                            if (result.data != null) {
                                departmentsData = result.data.departments;
                                buildList(departmentsData, index, lead);
                            }
                    }});
                }
                var selectedDepartments = [];
                function buildList(departmentsData, selectedData, selectedLead){
                    var _html = '';
                    departmentNumber = departmentNumber+1;
                    _html += '<div  class="col-sm-12 padding0 clearfix department-class multiple-department-class-'+departmentNumber+'">';
                    _html += '<div class="col-sm-6 pl0">';
                    _html += '<div class="form-group ">';
                    _html += '<div class="input_container">';
                    _html += '<select class="form-control" class="multiple-department" id="department_id">';
                    _html += '<option value="">Select</option>';
                    for(var index in departmentsData){
                        if(selectedData==departmentsData[index]['id_department']){
                            _html += '<option selected value="'+departmentsData[index]['id_department']+'">'+departmentsData[index]['department_name']+'</option>';
                        }else{
                            _html += '<option value="'+departmentsData[index]['id_department']+'">'+departmentsData[index]['department_name']+'</option>';
                        }
                    }
                    _html += '</select>';
                    _html += '</div>';
                    _html += '<label class="control-label"><span class="req-str">*</span> Department :</label>';
                    _html += '</div>';
                    _html += '</div>';

                    _html += '<div class="col-sm-3">';
                    _html += '<div class="form-group">';
                    _html += '<div class="input_container">';
                    if(selectedLead!=0){
                        _html += '<input checked type="checkbox"  name="is_lead" class="department_is_lead" />';
                    }else{
                        _html += '<input type="checkbox"  name="is_lead" class="department_is_lead" />';
                    }
                    _html += '</div>';
                    _html += '<label class="control-label">Team lead :</label>';
                    _html += '</div>';
                    _html += '</div>';

                    _html += '<div class="col-sm-3">';
                    _html += '<a href="javascript:;" onclick="deleteDepartment(this)"> <i class="fa fa-trash-o mt20"></i> <a>';
                    _html += '</div>';

                    _html += '</div>';
                    $('#add-user-department').append(_html);
                    $('select').select2();
                }
                function cancelAddUser(This){
                    //Cancel all the department cretaed and close the moda
                    selectedDepartments = [];
                    departmentNumber = 0;
                    $('#add-user-department').html('');
//                    $('#add-user-department').html(_html);
                }
                function deleteDepartment(This){
                    $(This).parent().parent().remove();
                    departmentNumber = departmentNumber-1;
                }
            </script>
            <!-- InstanceEndEditable -->

        </div>
        <!--<a href="javascript:;" class="footer-logo clearfix"><img src="<?/*=WEB_BASE_URL*/?>images/people-combine-logo.png" /></a>-->
    </div>
    <!--Render Body End-->


