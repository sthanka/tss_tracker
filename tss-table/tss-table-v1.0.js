'use strict';
// JavaScript Document   
(function ($) {
    var TssTable= function (element, options) {
        this.element = $(element);
        this.options = $.extend({ 
			 data:[]
        	,searchKey:null
        	,filterdata:null
        	,ignoreCaseInSearch:false
        	,keysToSearch:null//['','']
			,theme:'default'
			,fetchMode:'local'
			,fetchMethod:'POST'
			,originalData:null
			,pager:true
			,perPage:10
			,page:1
			,defaultSort:null
			,defaultSortMode:null
			,noDataMessage:'No Records Exists'
			,showHeader:true
			,header:null
			,cols:null
			,postData:null
			,loadComplete:null
			}, options || {});
        // Public method - can be called from client code
        this.init = function () {
		    var curObj = this;
		    curObj.element.empty();
		    $(curObj.options.searchKey).val('');
			curObj.options.originalData=curObj.options.data.slice(); 
			if(curObj.options.theme==='default')
				curObj.defaultTheme();
			else if(curObj.options.theme==='widgetTheme')
				curObj.widgetTheme();
			curObj.bindSearchEvent();
			return this;
		};
		this.reInit = function (page) {
		    var curObj = this;
			curObj.options.data=curObj.options.originalData.slice();
			if(page) curObj.options.page=page;
			curObj.init();
		    return this;
		};
		this.bindSearchEvent = function () {
			var curObj = this;
			var ele=$(curObj.options.searchKey);
			if(ele.length>0){
				ele.keypress(function(e) {
				    if(e.which == 13) {
				    	if(curObj.options.fetchMode==='local'){ 
				    		if(curObj.options.filterdata)
				    			curObj.options.data=curObj.options.filterdata(curObj.options.originalData.slice());
				    		else{
				    			var results=[];
		    					var searchVal=$(curObj.options.searchKey).val();
		    					if(curObj.options.ignoreCaseInSearch===true)searchVal=searchVal.toUpperCase();
				    			$.each(curObj.options.originalData.slice(),function(i,o){
				    				if(curObj.options.keysToSearch && curObj.options.keysToSearch.length>0){
				    					$.each(curObj.options.keysToSearch,function(j,k){
				    						var val=o[k];
				    						if(curObj.options.ignoreCaseInSearch===true)val=o[k].toUpperCase();
				    						if((val+'').indexOf(searchVal)!=-1){
				    							results.push(o);
				    							return false;
				    						}
				    					});
				    				}else{
				    					$.each(o,function(j,k){
				    						k=(k==null?'':(k+''));
				    						var val=k;
				    						if(curObj.options.ignoreCaseInSearch===true)val=k.toUpperCase();
				    						if((val+'').indexOf(searchVal)!=-1){
				    							results.push(o);
				    							return false;
				    						}
				    					});
				    				} 
				    			});
				    			curObj.options.data=results;
				    		}
				    		if(curObj.options.pager) curObj.buildDefaultPager(curObj.options.data.length);
				    		curObj.buildDefaultBody();
				    	}else{
				    		curObj.serverFetch(1);	
				    	}
				    }
				});
				  $(options.searchKey).parent().find('.grid-search-btn').unbind('click');
			        $(options.searchKey).parent().find('.grid-search-btn').click(function () {
			        	curObj.serverFetch(1);	
			        });
			}
		    return this;
		};
  		this.widgetTheme=function(){
			var curObj = this; 
			if(curObj.element[0].nodeName!='DIV'){
				alert('Sorry, We can\'t build widgets with this element');
				return this;	
			}else{
			 	curObj.element.empty();
				curObj.buildDefaultMisc();
				if(curObj.options.fetchMode==='local'){
					//curObj.buildDefaultHeader();
					if(curObj.options.pager)
						curObj.buildDefaultPager(curObj.options.data.length);
					curObj.buildDefaultSort();
					curObj.buildWidgetBody();
				}else{
					//curObj.buildDefaultHeader();
					curObj.options.curSort=curObj.options.defaultSort;
					curObj.options.curOrder=curObj.options.defaultSortMode;
					curObj.buildDefaultSort();
					if(curObj.options.curSort && !curObj.options.curOrder)curObj.options.curOrder='asc';
					curObj.serverFetch();
				}
			}
			return this;
		}; 		
  		this.defaultTheme=function(){
			var curObj = this; 
			if(curObj.element[0].nodeName!='DIV'){
				alert('Sorry, We can\'t build table with this element');
				return this;	
			}else{
			 	curObj.element.empty();
				curObj.buildDefaultMisc();
				if(curObj.options.fetchMode==='local'){
					curObj.buildDefaultHeader();
					if(curObj.options.pager)
						curObj.buildDefaultPager(curObj.options.data.length);
					curObj.buildDefaultSort();
					curObj.buildDefaultBody();
				}else{
					curObj.buildDefaultHeader();
					curObj.options.curSort=curObj.options.defaultSort;
					curObj.options.curOrder=curObj.options.defaultSortMode;
					curObj.buildDefaultSort();
					if(curObj.options.curSort && !curObj.options.curOrder)curObj.options.curOrder='asc';
					curObj.serverFetch();
				}
			}
			return this;
		}; 
		this.serverFetch=function(page,queryData){
			var curObj = this; 
			if(!queryData)queryData={};
			if(page)curObj.options.page=page; 
			var sendData=$.extend(true,{page:curObj.options.page,size:curObj.options.perPage,search:$(curObj.options.searchKey).val(),sort:curObj.options.curSort,order:curObj.options.curOrder},queryData,curObj.options.postData);
			$.ajax({type: curObj.options.fetchMethod,data:sendData, dataType: "json",url:curObj.options.url,success:function(result){
				if(result.success){
					if(curObj.options.pager)
							curObj.buildDefaultPager(result.total);
					curObj.options.data=result.data;
					curObj.buildDefaultBody();
				}
			}});
			return this;
		};			
		this.buildDefaultMisc=function(){
			var curObj = this; 
			return this;
		};	
		this.buildDefaultSort=function(){
			var curObj = this; 
			if(curObj.options.defaultSort){
				if(!curObj.options.defaultSortMode)curObj.options.defaultSortMode='asc';
					curObj.element.find('.sortable').removeClass('sorted');
					var key=curObj.options.defaultSort;
					var  _th=curObj.element.find('th[sortIndex="'+key+'"]');
					_th.addClass('sorted');
					if(curObj.options.fetchMode==='local'){
						if(curObj.options.defaultSortMode==='asc') 
							curObj.options.data.sort(function(x,y){ return x[key] > y[key]; });
						else curObj.options.data.sort(function(x,y){ return x[key] < y[key];	});		
					}
					_th.attr('sort',(curObj.options.defaultSortMode==='asc')?'desc':'asc');						
			}
			return this;
		};						
		this.buildDefaultHeader=function(){
			var curObj = this; 
			if(curObj.options.showHeader===true){
				var _headTable=$('<table/>',{class:'tss-head-table'});
				curObj.element.append(_headTable);
				if(curObj.options.header===null){
					if(curObj.options.data!=null && curObj.options.data.length>0){
						var _thead=$('<thead/>',{});
						_headTable.append(_thead);
						var _tr=$('<tr/>',{});
						_thead.append(_tr);
						var i=0;
						$.each(	curObj.options.data[0],function(k,v){
								var _th=$('<th/>',{text:k});
								curObj.initializeSort(i++,_th,k);
								_tr.append(_th);
						});
					}
				}else{
						var _thead=$('<thead/>',{});
						_headTable.append(_thead);
						var _tr=$('<tr/>',{});
						_thead.append(_tr);
						$.each(	curObj.options.header,function(i,o){
								var _th=$('<th/>',{text:o,align:'left'});
								curObj.initializeSort(i,_th);
								if(curObj.options.cols[i] && curObj.options.cols[i].hidden===true)
									_th.hide();
								if(curObj.options.cols[i].classes)_th.addClass(curObj.options.cols[i].classes);
								_tr.append(_th);
						});
				}
			}
			return this;
		}; 
		this.initializeSort=function(i,_th,key){
			var curObj = this; 
			if(curObj.options.cols===null || !(curObj.options.cols[i].sortable===false)){
				key=(curObj.options.cols===null)?key: curObj.options.cols[i].sortKey?curObj.options.cols[i].sortKey:curObj.options.cols[i].key;
				_th.css('cursor','pointer');
				_th.addClass('sortable');
				_th.attr('sort','asc');
				_th.attr('sortIndex',key);
				_th.unbind('click');
				_th.bind('click',function(){ 
					var key=$(this).attr('sortIndex');
					var sort=$(this).attr('sort');
					curObj.options.page=1;
					curObj.element.find('.sortable').removeClass('sorted');
					$(this).addClass('sorted');
					if(curObj.options.fetchMode==='local'){ 
						if(sort==='asc') curObj.options.data.sort(function(x,y){ return x[key] > y[key]; });
						else curObj.options.data.sort(function(x,y){ return x[key] < y[key];	});		
						curObj.buildDefaultBody();
					}else{
						curObj.options.curSort=key;
						curObj.options.curOrder=sort;
						curObj.serverFetch();
					}
					$(this).attr('sort',(sort==='asc')?'desc':'asc');
					
				});
			}					
			return this;
		};
		this.adjustWidth=function(){
			var curObj = this; 
			var _tr=curObj.element.find('.tss-body-table tr:eq(0)');
			var _thr=curObj.element.find('.tss-head-table tr:eq(0)');
			if(_tr.length>0){ 
				$.each($(_tr).find('td'),function(i,o){
					$(_thr).find('th:eq('+i+')').width($(o).width());
				});
			}
			return this;
		};
		this.buildDefaultBody=function(){
			var curObj = this; 
			if(curObj.options.theme==='widgetTheme'){
				return curObj.buildWidgetBody();
			}
			var _bodyDiv=$('<div/>',{class:'tss-table-body-div'}); //,style:'height:200px;overflow:auto;'
			curObj.element.find('.tss-table-body-div').remove();
			curObj.element.find('.tss-head-table').after(_bodyDiv);
			//curObj.element.append(_bodyDiv);
			if(curObj.options.data!=null && curObj.options.data.length>0){
				var _bodyTable=$('<table/>',{class:'tss-body-table'});
				_bodyDiv.append(_bodyTable);
						var _tbody=$('<tbody/>',{});
						_bodyTable.append(_tbody); 
						if(curObj.options.page>curObj.options.totalPages)
							curObj.options.page=1;
						curObj.element.find('.pagination-wrapper .active').removeClass('active')
						curObj.element.find('a[value="'+curObj.options.page+'"]').addClass('active'); 
						var buildData=[];
						if(curObj.options.fetchMode==='local'){
							buildData=curObj.options.pager?
									(curObj.options.data.slice(
									(curObj.options.page-1)*curObj.options.perPage,
									 curObj.options.page*curObj.options.perPage))
									 :curObj.options.data;
						}else{
							buildData=curObj.options.data;
						}

						$.each(buildData,function(k,v){
							var _tr=$('<tr/>',{});
							_tbody.append(_tr);
							_tr.data('data',v);
							if(curObj.options.cols===null){
								$.each(	curObj.options.data[0],function(colKey,colVal){
										_tr.append($('<td/>',{text:v[colKey]}));
								});
							}else{
								$.each(	curObj.options.cols,function(i,o){
										var style='';
										if(o.width){
											style='width: '+$.trim(o.width)+(o.widthUnits?o.widthUnits:'%')+';';
										}
										var _td=$('<td/>',{title:v[o.key],text:v[o.key],style:style});
										if(o.hidden)
											_td.hide();
										if(o.classes)_td.addClass(o.classes);
										_tr.append(_td);
										if(o.formatter){
											o.formatter(v,o.key,_td);
										}
								});
							}
						});
						curObj.adjustWidth();
			}else{
				//Show no data	
				_bodyDiv.append('No Records');
				_bodyDiv.addClass('text-align-center table-no-data');
			}
			if(curObj.options.loadComplete)
				curObj.options.loadComplete(curObj);
			return this;
		}; 			
		this.buildWidgetBody=function(){
			var curObj = this; 
			var _bodyDiv=$('<div/>',{class:'tss-table-body-div'}); //,style:'height:200px;overflow:auto;'
			curObj.element.find('.tss-table-body-div').remove();
			if(curObj.element.find('.pagination-wrapper').length==0) curObj.element.append(_bodyDiv);
			else curObj.element.find('.pagination-wrapper').before(_bodyDiv);
			if(curObj.options.data!=null && curObj.options.data.length>0){
				var _bodyWidget=$('<div/>',{class:'tss-body-widget'});
				_bodyDiv.append(_bodyWidget);
						if(curObj.options.page>curObj.options.totalPages)
							curObj.options.page=1;
						curObj.element.find('.pagination-wrapper .active').removeClass('active')
						curObj.element.find('a[value="'+curObj.options.page+'"]').addClass('active'); 
						var buildData=[];
						if(curObj.options.fetchMode==='local'){
							buildData=curObj.options.pager?
									(curObj.options.data.slice(
									(curObj.options.page-1)*curObj.options.perPage,
									 curObj.options.page*curObj.options.perPage))
									 :curObj.options.data;
						}else{
							buildData=curObj.options.data;
						}
						$.each(buildData,function(k,v){
							if(curObj.options.widgetFormatter){
								curObj.options.widgetFormatter(v,k,_bodyWidget);
							}else{
								var _widget=$('<div/>',{});
								_bodyWidget.append(_widget);
								_widget.data('data',v);
								_widget.html(JSON.stringify(v));
								if(curObj.options.widgetClasses)_widget.addClass(curObj.options.widgetClasses);
							}
						}); 
			}else{
				//Show no data	
				_bodyDiv.append('No Records');
				_bodyDiv.addClass('text-align-center widget-no-data');
			}
			if(curObj.options.loadComplete)
				curObj.options.loadComplete(curObj);
			return this;
		}; 		
		this.buildDefaultPager=function(total){
			var curObj = this; 
			curObj.element.find('.pagination-wrapper').remove();
			var noOfPages = total/curObj.options.perPage;
			var _page=curObj.options.page;
			var _size=curObj.options.perPage;
			var _totalPages=Math.ceil((total/_size),10);
			if(_totalPages>1){				
				var showFirst=true;
				var showPrevious=true;
				var showNext=true;
				var showLast=true;				
				if(_page==_totalPages && _page==1){
					showFirst=false;
					showPrevious=false;
					showLast=false;
					showNext=false;
				}else if(_page==_totalPages && _page>1){
					showLast=false;
					showNext=false;
				}else if(_page!=_totalPages && _page==1){
					showFirst=false;
					showPrevious=false;
				}		
				var _paginationWrap=$('<div/>',{class:'pagination-wrapper'});
				curObj.element.append(_paginationWrap);
				var _paginationUl=$('<ul/>',{class:'pagination text-center'});//pagination-ul
				_paginationWrap.append(_paginationUl);			
				var _pages=parseInt(noOfPages,10);
				if(noOfPages>_pages)
					_pages++;
				curObj.options.totalPages=_pages;
				if(!curObj.options.page || curObj.options.page<=0)curObj.options.page=1;
				var pStart=(_pages>10 && curObj.options.page>6)?(curObj.options.page-5):0;
				var pEnd=_pages;
				if((pStart+10)>_pages){
					pStart=_pages-10;
					pStart=pStart<0?0:pStart;
				}else pEnd=pStart+10;
				for(var i=pStart;i<pEnd;i++){				
					var _li=$('<li/>',{});
					var _a=$('<a/>',{value:i+1,text:i+1});
					_li.append(_a);
					_a.unbind('click');
					_a.bind('click',function(){
						curObj.options.page=parseInt($(this).attr('value'),10);
						if(curObj.options.fetchMode==='local'){curObj.buildDefaultPager(total);curObj.buildDefaultBody();}else{curObj.serverFetch();}
					 });
					_paginationUl.append(_li);
				}
				
				if(showPrevious){
					//previous next
					var _prev = $('<li/>');
					_paginationUl.prepend(_prev);
					var _span=$('<span/>',{'aria-hidden':'true'});
					_span.append($('<i/>',{'class':'icon-angle-left'}));
					var _prevtxt = $('<a/>',{'class':'prev-btn page-navg','aria-label':'Previous'});
					_prevtxt.append(_span);
					_prev.append(_prevtxt);
					_prevtxt.unbind('click');
					_prevtxt.bind('click',function(){
						curObj.options.page=parseInt(curObj.options.page-1);
						if(curObj.options.fetchMode==='local'){curObj.buildDefaultPager(total);curObj.buildDefaultBody();}else{curObj.serverFetch();}
					 });
				}
				

				if(showFirst){
					var _first = $('<li/>');
					_paginationUl.prepend(_first);
					var _span=$('<span/>',{'aria-hidden':'true'});
					_span.append($('<i/>',{'class':'icon-angle-left'}));
					_span.append($('<i/>',{'class':'icon-angle-left'}));
					var _firsttxt = $('<a/>',{'class':'first-btn page-navg','aria-label':'Previous'});
					_firsttxt.append(_span);
					_first.append(_firsttxt);				
					_firsttxt.unbind('click');
					_firsttxt.bind('click',function(){
						curObj.options.page=parseInt(1);
						if(curObj.options.fetchMode==='local'){curObj.buildDefaultPager(total);curObj.buildDefaultBody();}else{curObj.serverFetch();}
					 });
				}
				
				if(showNext){
					var _nxt = $('<li/>');
					_paginationUl.append(_nxt);
					var _span=$('<span/>',{'aria-hidden':'true'});
					_span.append($('<i/>',{'class':'icon-angle-right'}));
					var _nxttxt = $('<a/>',{'aria-label':'Next','class':'nxt-btn page-navg'});
					_nxttxt.append(_span);
					_nxt.append(_nxttxt);
					_nxttxt.unbind('click');
					_nxttxt.bind('click',function(){
						curObj.options.page=parseInt(curObj.options.page+1);
						if(curObj.options.fetchMode==='local'){curObj.buildDefaultPager(total);curObj.buildDefaultBody();}else{curObj.serverFetch();}
					 });
				}
				
				if(showLast){
					var _last = $('<li/>');
					_paginationUl.append(_last);
					var _span=$('<span/>',{'aria-hidden':'true'});
					_span.append($('<i/>',{'class':'icon-angle-right'}));
					_span.append($('<i/>',{'class':'icon-angle-right'}));
					var _lasttxt = $('<a/>',{'aria-label':'Next','class':'last-btn page-navg'});
					_lasttxt.append(_span);
					_last.append(_lasttxt);
					_lasttxt.unbind('click');
					_lasttxt.bind('click',function(){
						curObj.options.page=parseInt(curObj.options.totalPages);
						if(curObj.options.fetchMode==='local'){curObj.buildDefaultPager(total);curObj.buildDefaultBody();}else{curObj.serverFetch();}
					 });
				}
			}	
			return this;
		}; 							
		this.sampleMethos=function(){
					var curObj = this; 
					return this;
				}; 
    };
    $.fn.TssTable = function (options) {
        return this.each(function () {
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data('this')) return;
            // pass options to plugin constructor
            var obj = new TssTable(this, options);
            // Store plugin object in this element's data
            element.data('this', obj);
            obj.init();
        });
    };
})(jQuery);