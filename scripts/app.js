(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

    $('.add-btn button').on('click',function(){
        $(this).parent().next().fadeToggle();
    })
    $('.hide-btn').on('click',function(){
        $(this).parent().parent().fadeOut();
    })
    $('body').on('click', function (evt) {
        if($(evt.target).hasClass('link') || $(evt.target).hasClass('fa-ellipsis-v')){}else{
            $('body').find('.grid-actions').hide();
            $('body').find('.inmemu').removeClass('inmemu');
        }
    });
})();
function minScroll(){
    $(".unassigned-task-wrap,.task-assigning-table-prnt,.actions-view-wrapper").mCustomScrollbar({
        theme:"dark",
        autoHideScrollbar: true,
        autoExpandScrollbar: true
    });
    var _unassignedTaskWrapMaxHeight = $('.unassigned-task-wrap').height($(window).height()-260);
    var _taskAssigningTablePrntHeight = $('.task-assigning-table-prnt').height($(window).height()-260);
    $('.unassigned-task-wrap').height(_unassignedTaskWrapMaxHeight);
    $('.task-assigning-table-prnt').height(_taskAssigningTablePrntHeight);
}
function showTimer(e){
    if(!$(e).hasClass('inmemu')){
        $('body').find('.grid-actions').hide();
        $(e).addClass('in');
        $(e).next().fadeIn(100);
        $(e).next('.grid-actions').css({
            'left': $(e).offset().left,
            'top':$(e).offset().top,
            'margin-left':'-'+($(e).next('.grid-actions').width()+5)+'px'
        });
    } else{
        $(e).removeClass('inmemu')
        $('body').find('.grid-actions').hide();
    }
}

function gmail_logout()
{
    gapi.auth.signOut();
    location.reload();
}
function gmail_login()
{
    var myParams = {
        'clientid' : '263816501580-e57pbt2g3vq7iedrhjtoehi6lknn0ruc.apps.googleusercontent.com',
        //'clientid' : '472244376060-29v5udskvifbcj1fqdevm71ffblet4qh.apps.googleusercontent.com',
        'cookiepolicy' : 'single_host_origin',
        'callback' : 'loginCallback',
        'approvalprompt':'force',
        'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
    };
    gapi.auth.signIn(myParams);
}

function loginCallback(result)
{
    if(result['status']['signed_in'])
    {
        var request = gapi.client.plus.people.get(
            {
                'userId': 'me'
            });
        request.execute(function (resp)
        {
            var email = '';
            if(resp['emails'])
            {
                for(i = 0; i < resp['emails'].length; i++)
                {
                    if(resp['emails'][i]['type'] == 'account')
                    {
                        email = resp['emails'][i]['value'];
                    }
                }
            }


            $.ajax({
                async: false,
                type: 'POST',
                url: WEB_BASE_URL+'index.php/User/login',
                dataType: 'json',
                data: resp,
                success:function(res){
                    if(res.response == 1){
                        location.reload();
                    }
                    else{
                        TssLib.notify(res.data, 'warn', 5);
                    }
                }
            });

        });

    }

}
function onLoadCallback()
{
    gapi.client.setApiKey('AIzaSyC2wsRKhKWgagMp4JXsGPIkYus0Up_krBA');
    gapi.client.load('plus', 'v1',function(){});
}


function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function updateUseCase(use_case_id,project_task_id)
{
    var html = '';
    var html_txt = '';

    var use_case = $('#user_case_text_'+project_task_id).Editor("getText");
    if(use_case!='')
    {
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL+'index.php/Project/updateUseCase',
            dataType: 'json',
            data: {use_case_id:use_case_id,project_task_id:project_task_id,use_case:use_case},
            success:function(res){
                if(res.status){
                    html = '<input type="button" id="use_case_btn_'+project_task_id+'" onclick="updateUseCase(\'\','+project_task_id+ ',\'\');" value="Save" class="button-common">';
                    html+='<input type="button" class="button-common hide-btn" value="Cancel">';

                    html_txt = '<li><h5>'+res.data.user_name+'</h5><p>'+use_case+'</p></li>';
                }
                $('#u-list_'+project_task_id+' .no-usecase').remove();
                $('#u-list_case_list_'+project_task_id+' .no-usecase').remove();
                $('#task_'+project_task_id+' .mt10').html(html);
                $('#u-list_'+project_task_id+'').prepend(html_txt);
                $('#u-list_case_list_'+project_task_id+'').prepend(html_txt);
                $('#user_case_text_'+project_task_id).Editor("setText",'');
                TssLib.notify('Use case Added Successfully');

                $('.hide-btn').on('click',function(){
                    $(this).parent().parent().fadeOut();
                })
            }
        });
    }
}

function getPrevTasks(user_id,day_id,project_id)
{
    var html='<table class="table table-bordered">';
    $.ajax({
        async: false,
        type: 'POST',
        url: WEB_BASE_URL+'index.php/Project/getUserDayTasks',
        dataType: 'json',
        data: {user_id:user_id,day_id:day_id,project_id:project_id},
        success:function(res){
            for(var s=0;s<res.length;s++){
                html+='<tr><td>'+res[s].project_name+'</td><td>'+res[s].task_name+'</td><td>'+res[s].time+'</td><td>'+res[s].task_status+'</td>';
                if(res[s].task_status=='new'){ html+='<td><span style="cursor: pointer;" onclick="deleteTaskWorkflow(this,'+res[s].id_task_week_flow+')">X</span></td>'; }
                else{ html+='<td></td>'; }
                html+='</tr>';
            }
            html+='</table';
            $('#prev_task').html(html);
        }
    });
}

function getUnassignedTaskList(project_id,status,e)
{
    $('.unassigned-task-wrap a').removeClass('color-green');
    $(e).addClass('color-green');
    /*var status = [];
    $('input[name="status[]"]').each(function(){
        if(this.checked)
            status.push(this.value);
    });
    console.log(status +'--'+type);
    $('#unasign-t').html('');
    if(status.length!=0)
    {
        TssLib.closeModal();
        if(type==0){ status=0; }*/
        var html = '';
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL+'index.php/Project/getUnassignedTasks',
            dataType: 'json',
            data: {project_id:project_id,status:status},
            success:function(res){
                if(res.response==1){
                    var data = res.data;
                    var counts = res.counts;
                    for(var s=0;s<data.length;s++)
                    {
                        var elementClass = (data[s]['alloted_time']=='00:00:00'?'not-alloted-task':'alloted-task');
                        html+='<div class="unassigned-task-box '+elementClass+'" >';
                        html+='<input type="hidden" name="project_task_id" value="'+data[s]["project_task_id"]+'">';
                        html+='<input type="hidden" name="project_id" value="'+data[s]["project_id"]+'">';
                        html+='<h4 name="project-title"><a href="javascript:;" >'+data[s]["project_name"]+'</a>';
                        if(data[s]["pending"])
                            html+='<a onclick="getEditTask(\''+data[s]["project_name"]+'\',\''+data[s]["task_name"]+'\',\''+data[s]["project_task_id"]+'\',\''+data[s]["project_id"]+'\',\''+data[s]["estimated_time"]+'\',\''+data[s]["remains"]+'\',1)" href="javascript:;" class="float-right"><span class="fa fa-pencil"></span></a>';
                        else
                            html+='<a onclick="getEditTask(\''+data[s]["project_name"]+'\',\''+data[s]["task_name"]+'\',\''+data[s]["project_task_id"]+'\',\''+data[s]["project_id"]+'\',\''+data[s]["estimated_time"]+'\',\''+data[s]["remains"]+'\')" href="javascript:;" class="float-right"><span class="fa fa-pencil"></span></a>';
                        html+='</h4>';
                        html+='<div name="task_name" class="task_name">'+data[s]["task_name"]+'</div>';
                        html+='<div  class="est-alot-rem">';
                        html+='<span>Estimated :<span  name="estimated-time">'+data[s]["estimated_time"]+'</span></span>';
                        html+='<span>Additional :<span>'+data[s]["additional_time"]+'</span></span>';
                        html+='<span><span>Alloted:</span><span>'+data[s]["alloted_time"]+'</span></span>';
                        html+='<span><span> Remains:</span><span name="remains"> '+data[s]["remains"]+'</span></span>';
                        html+='</div>';
                        html+='</div>';
                    }
                    $('#unasign-t').html(html);

                    $('#unassignedcounts').html('('+counts['unassigned']+')');
                    $('#assignedcounts').html('('+counts['assigned']+')');
                    $('#pendingcounts').html('('+counts['pending']+')');
                    $('#completedcounts').html('('+counts['completed']+')');

                    $(".unassigned-task-wrap").mCustomScrollbar("destroy");
                    $(".unassigned-task-wrap").mCustomScrollbar({
                        theme:"dark",
                        autoHideScrollbar: true,
                        autoExpandScrollbar: true
                    });

                    if(status=='unassigned')
                    {
                        $( ".unassigned-task-box" ).draggable({
                            appendTo: 'body',
                            containment: 'window',
                            scroll: false,
                            cursor: 'move',
                            helper: 'clone'
                        });

                        $(".unassigned-task-box").bind("drag", function(event, ui) {
                            ui.helper.css("border", "1px solid #000");
                        });
                    }
                    taskFilter();
                }
            }
        });
    /*}*/
}

function deleteTaskWorkflow($this,id)
{
    var $confirm = confirm('Are you sure want to delete?');
    if($confirm){
        $.ajax({
            async:false,
            type:'POST',
            url:WEB_BASE_URL+'index.php/Project/deleteTaskWeekFlow',
            dataType:'json',
            data:{id:id},
            success:function(res){
                $($this).parent().parent().remove();
                //location.reload();
                TssLib.closeModal();
                loadWeekWorkload(0);

            }
        });
    }
}


function getWeekWorkLoad(week)
{
    loadWeekWorkload(week);
}

function getProjectTaskDetails(project_task_id,pending)
{
    $.ajax({
        async: false,
        type: 'POST',
        url: WEB_BASE_URL+'index.php/Project/getProjectTaskDetails',
        dataType: 'json',
        data: {project_task_id:project_task_id},
        success:function(res){
            if(res.response==1)
            {
                var data = res.data;
                $('#task_name').val(data[0].task_name);
                $('#description').val(data[0].task_description);
                $('#estimated_time').val(data[0].estimated_time);
                $('#department_id').val(data[0].department_id);
                if(pending) {
                    $('#t-status #task_status').val(data[0].task_status).trigger('chosen:updated');
                }
                if(data[0].additional_time == null && data[0].additional_time != '00:00:00'){
                    $('#additional_time').parent().parent().hide();
                }else if(data[0].additional_time != null && data[0].additional_time == '00:00:00'){
                    $('#additional_time').parent().parent().hide();
                }else{
                    $('#additional_time').val(data[0].additional_time);
                    $('#additional_time').parent().parent().show();
                }
                if(data[0].is_forward==0){
                    $('#is_forward').attr('checked',false);
                } else if(data[0].is_forward==1){
                    $('#is_forward').attr('checked',true);
                }
                $('#task_flow_id').val(data[0].id_task_flow);
                $('#project_task_id').val(data[0].project_task_id);

                if(data[0].task_status=='new'){
                    $('#rej-div').css('display','block');
                }
                else{
                    $('#rej-div').css('display','none');
                }
            }
        }
    });
}

function getRejectTask()
{
    /*var task_flow_id = $('#task_flow_id').val();
        var project_task_id = $('#project_task_id').val();
        var $confirm = confirm('Are you sure want to reject?');
        if($confirm) {
            $.ajax({
                async: false,
                type: 'POST',
            url: WEB_BASE_URL + 'index.php/Project/getRejectTask',
            dataType: 'json',
            data: {task_flow_id: task_flow_id, project_task_id: project_task_id},
            success: function (res) {
                if (res.response == 1) {
                    TssLib.notify(res.data);
                    TssLib.closeModal();
                    location.reload();
                }
                else {
                    TssLib.notify(res.data, 'warn', 5);
                }
            }
        });
    }*/
    TssLib.confirm('Reject Confirmation', 'Are you sure to Reject?', function () {
        $(this).closest('.modal').modal('hide');

        var task_flow_id = $('#task_flow_id').val();
        var project_task_id = $('#project_task_id').val();

            $.ajax({
                async: false,
                type: 'POST',
                url: WEB_BASE_URL + 'index.php/Project/getRejectTask',
                dataType: 'json',
                data: {task_flow_id: task_flow_id, project_task_id: project_task_id},
                success: function (res) {
                    if (res.response == 1) {
                        TssLib.notify(res.data);
                        TssLib.closeModal();
                        location.reload();
                    }
                    else {
                        TssLib.notify(res.data, 'warn', 5);
                    }
                }
            });


    }, 'Yes', 'No');
}

function updateSessionDepartment(dept_id)
{
    $.ajax({
        async: false,
        type: 'POST',
        url: WEB_BASE_URL+'index.php/Project/updateSessionDepartment',
        dataType: 'json',
        data: {dept_id:dept_id},
        success:function(res){
            if(res.response==1)
            {
               location.reload();
            }
        }
    });
}
/*
    By - Abhilash
 */
function encodeSpecialChar(str)
{
    str = str.replace(/&/g, "&amp;");
    str = str.replace(/>/g, "&gt;");
    str = str.replace(/</g, "&lt;");
    str = str.replace(/"/g, "&quot;");
    str = str.replace(/'/g, "&#039;");
    return str;
}
/*
 By - Abhilash
 */
function decodeSpecialChar(str)
{
    str = str.replace(/&amp;/g, "&");
    str = str.replace(/&gt;/g, ">");
    str = str.replace(/&lt;/g, "<");
    str = str.replace(/&quot;/g, '"');
    str = str.replace(/&#039;/g, "'");
    return str;
}

