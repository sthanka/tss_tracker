
        <form id="excelUploadForm" enctype="multipart/form-data" method="post" action="<?php echo site_url('/welcome/excelTaskUpload') ?>">
            <input type="file" id="file" name="file"/>
            <button type="submit"> Upload</button>
        </form>
        <div id="result">

        </div>
        <script>
            $('form').on('submit', function (e) {
                e.preventDefault();

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: '<?php echo site_url('/Welcome/excelTaskUpload') ?>',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#result').html(data);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            });
        </script>
