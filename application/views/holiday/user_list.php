<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Holiday list</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="padding0">
                    <div class="col-sm-12 clearfix clearboth bg-white padding0">
                        <div class="grid-details-table">
                            <div class="grid-details-table-header">
                                <h3>&nbsp;</h3>
                                <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="grid-details-table-content clearfix padding0">
                                <div class="tbl_wrapper border0">
                                    <table id="attendance-list" class="table-responsive"></table>
                                    <div id="attendance-list-page"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#attendance-list');
        TssLib.docReady(function () {
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Holiday/getHolidayGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['id_holiday', 'Date', 'Title', 'Description', 'future'],
                colModel: [
                    { name: 'id_holiday', index: 'id_holiday', hidden: true ,key:true},
                    { name: 'date', index: 'date', hidden: false ,key:false, width: 15},
                    { name: 'title', index: 'title', hidden: false ,key:false, width: 25},
                    { name: 'description', index: 'description', hidden: true ,key:false},
                    { name: 'future', index: 'future', hidden: true ,key:false},
                ],
                pager: 'attendance-list-page'
            }).navGrid('#attendance-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true
            });
        });
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->