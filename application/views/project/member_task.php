<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Members Task Details</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Task Details
                    </h3>
                </div>
                <div class="padding0">

                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <form id="reports_frm" class="NewErrorStyle" name="reports_frm" method="post"
                              action="<?= WEB_BASE_URL ?>index.php/Reports/generateReport">
                            <div class="col-sm-10 padding0">
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="projectDropdown" name="project_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Project:</label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="employeeDropdown" name="user_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Employee:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="sdate" value="<?=date('d/m/Y',strtotime("-1 days"))?>"
                                                       name="sdate" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>

                                        <label class="control-label col-sm-12">Start Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="edate" value="<?=date('d/m/Y',strtotime("0 days"))?>"
                                                       name="edate" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>

                                        <label class="control-label col-sm-12">End Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <!--<button class="button button-common module mt20"  onclick="resultType(0);" > Search </button>
                                    <button class="button button-common module mt20"  onclick="resultType(1);" > Excel </button>-->
                                    <input type="button" class="button button-common module mt20"
                                           onclick="validateReportsForm(0);" value="Search">
                                </div>
                            </div>
                            <input type="hidden" name="type" id="type" value="0">
                        </form>

                    </div>
                    <!--<div class="col-sm-12 clearfix clearboth padding0">
                        <div class="grid-details-table-content clearfix padding0">
                            <div class="tbl_wrapper border0">
                                <table id="project-list" class="table-responsive"></table>
                                <div id="project-list-page"></div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-sm-12 clearfix clearboth bg-white padding0">
                        <div class="grid-details-table">
                            <div class="grid-details-table-header">
                                <h3>&nbsp;</h3>
                                <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="grid-details-table-content clearfix padding0">
                                <div class="tbl_wrapper border0">
                                    <table id="project-list" class="table-responsive"></table>
                                    <div id="project-list-page"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="taskDetails" class="modal-wrapper" >
        <div id="detailsContent">

        </div>
    </div>
    <script>
        var $refTblgrid = $('#project-list');

        $(function () {
            var d = new Date();
            d.setDate(d.getDate() - 1);
            $('#date').val(d.format('d/m/Y'));
            TssLib.datepickerBinder('#reports_frm');

            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Project/getMembersTaskGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['Project Name', 'User Name', 'Task Name', 'Allotted Time', 'Log Time', 'Status', 'project_task_id', 'user_id'],
                colModel: [
                    {name: 'project_name', index: 'project_name', hidden: false},
                    {name: 'user_name', index: 'user_name', hidden: false, key: false},
                    {name: 'task_name', index: 'task_name', hidden: false, key: false,formatter: function (c, o, d) {
                        return '<a href="javascript:;" onclick="openTaskMemeberModal('+ d.project_task_id +', '+d.user_id+')">'+d.task_name+'</a>';
                    }},
                    {name: 'allotted_time', index: 'allotted_time', hidden: false, key: false},
                    {name: 'logtime', index: 'logtime', hidden: false, key: false, sortable: false},
                    {name: 'status', index: 'status', hidden: false, key: false},
                    {name: 'project_task_id', index: 'project_task_id', hidden: true, key: false},
                    {name: 'user_id', index: 'user_id', hidden: true, key: false}
                ],
                pager: 'project-list-page'
            }).navGrid('#project-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true
            });



            postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var projects = result.data;
                        TssLib.populateSelect($('#projectDropdown'), {
                            success: true,
                            data: projects
                        }, 'project_name', 'id_project');
                        TssLib.selectAllOptions('#projectDropdown');
                        loadUsers();

                        $('#projectDropdown').on('change', function () {
                            loadUsers();
                        });
                    }
                }
            });



        });

        function openTaskMemeberModal(taskId, userId){
            var $modal = TssLib.openModel({width: 1024},$('#taskDetails').html());

            $.post("<?=site_url('Project/taskMemeberModal')?>" , {'task_id':taskId , 'user_id': userId}, function( data ) {
                $modal.find('#detailsContent').html(data);
            });
        }

        function loadUsers() {
            var project = $('#projectDropdown').val();
            if(!project){
                project =0;
            }
            else
                project = project.join(',');

            var postData = {};
            <?php if($this->session->userdata('department_id')){ ?>
                postData = {'project_id':project , 'department_id': '<?=$this->session->userdata('department_id')?>'};
            <?php } else { ?>
                postData = {'project_id':project }
            <?php } ?>

            postJsonAsyncWithBaseUrl("Project/getUserByDepartmentProject", postData, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var user = result.data.data;
                        TssLib.populateSelect($('#employeeDropdown'), {
                            success: true,
                            data: user
                        }, 'email', 'id_user');
                        TssLib.selectAllOptions('#employeeDropdown');

                        validateReportsForm(0);
                    }
                }
            });
        }
        function resultType(type) {
            $('#type').val(type);
        }

        function validateReportsForm(type) {
            $('.tssDatepicker ').removeClass('err-bg');
            $('#date').removeClass('err-bg');
            $('#employeeDropdown').removeClass('err-bg');
            $('.error-msg').remove();

            var sdate = $('#sdate').val();
            var edate = $('#edate').val();
            var project = $('#projectDropdown').val();
            var user_id = $('#employeeDropdown').val();
            var flag = 0;

            /*if (sdate == '') {
                $('#sdate').addClass('err-bg');
                $('#sdate').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (edate == '') {
                $('#edate').addClass('err-bg');
                $('#edate').before('<span class="error-msg">Required</span>');
                flag++;
            }*/
            if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                $('#sdate').addClass('err-bg');
                $('#sdate').before('<span class="error-msg">Required</span>');
                $('#edate').addClass('err-bg');
                $('#edate').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (project == '') {
                $('#projectDropdown').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (user_id == '') {
                $('#employeeDropdown').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }

            if (flag == 0) {
                if (type == 0) {
                    var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
                    myPostData['project'] = project;
                    myPostData['user_id'] = user_id;
                    myPostData['sdate'] = sdate;
                    myPostData['edate'] = edate;
                    $refTblgrid.setGridParam({'postData': myPostData});
                    $('#project-list').trigger("reloadGrid");

                    return false;
                }
                else {
                    $('#reports_frm').submit();
                }
            }
        }
    </script>