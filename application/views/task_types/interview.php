<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Task Types</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Task Types - Interview list</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="task-type-list" class="table-responsive"></table>
                        <div id="task_type-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-client-modal" class="modal-wrapper" style="display:none; width: 200px;">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Task Types</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="name"  id="name"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Task Type Name :</label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="id_task_type" value=""/>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        TssLib.docReady(function () {
            var $refTblgrid = $('#task-type-list');
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Task_types/getTaskTypesGrid/?taskType=interview',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['id_task_type', 'Name' , 'task_type_key'],
                colModel: [
                    { name: 'id_task_type', index: 'id_task_type', hidden: true ,key:true},
                    { name: 'name', index: 'name' },
                    { name: 'task_type_key', index: 'task_type_key', hidden: true },
                ],
                pager: 'task_type-list-page'
            }).navGrid('#task_type-list-page', {
                edit: true, add: true, del: true, search: false, refresh: true,
                addfunc: function () {
                    addEditCreateTaskType();
                }, editfunc: function (id) {
                    var rowData = $refTblgrid.jqGrid('getRowData', id);
                    addEditCreateTaskType(rowData);
                }, delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        postJsonAsyncWithBaseUrl("Task_types/deleteTaskTypesById", {'task_type_id': id}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data != null) {
                                    if(result.data.status){
                                        TssLib.notify('Deleted successfully', null, 5);
                                        $('#task-type-list').trigger('reloadGrid');
                                    }else{
                                        TssLib.notify(result.data.message, 'warn', 5);
                                    }
                                }
                            }});
                    }, 'Yes', 'No');
                }
            });
        });
        function addEditCreateTaskType(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            if (TssLib.isBlank(rowData)) {
                $modal.find('.panel-title').text('Add Task Type');
                $modal.find('[div-submit="true"]').text('Save');
            } else {
                TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                $modal.find('.panel-title').text('Edit Task Type');
                $modal.find('[div-submit="true"]').text('Update');
            }
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {

                    if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                    if (result.data.status) {
                        $('#task-type-list').trigger('reloadGrid');
                        TssLib.closeModal();
                        TssLib.notify('Task Type '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    $(formObj).data('additionalData', { taskType: 'interview'});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Task_types/addTaskTypesOthers');
                    return true;
                }
            });
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->