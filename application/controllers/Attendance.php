<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Attendance_modal');
    }

    public function index()
    {
        $data['content'] = 'attendance/attendance';
        $this->load->view('partials/landing',$data);
    }

    function excelAttendanceUpload(){

        if (isset($_FILES['file']['tmp_name']) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
            $file = $_FILES['file']['tmp_name'];
            $userfile_extn = explode(".", strtolower($_FILES['file']['name']));

            if ($userfile_extn[1] == 'xlsx' || $userfile_extn[1] == 'xls') {
                $this->load->library('excel');
                $objPHPExcel = PHPExcel_IOFactory::load($file);
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                $header = [];
                $arr_data = [];
                $index = 0;
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
//                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();
                    if ($row == 1) {
                        $header[$row][$column] = trim($data_value);
                    } else {
                        if(isset($header[1][$column])) {
                            $value = $header[1][$column];
                            $arr_data[$row][$value] = $data_value;
                        }
                        else{
                            $value = '';
                            $arr_data[$row][$value] = '';
                        }
                    }
                    $index++;
                }
                $arr_data = array_values($arr_data);
                $err = false;
//                echo '<pre>'; print_r($arr_data); exit;
                for($s=0;$s<count($arr_data);$s++) {
                    $isError = false;
                    if(isset($arr_data[$s]['emp_code'])
                        || isset($arr_data[$s]['date(dd-mm-yyyy)'])
                        || isset($arr_data[$s]['start_time'])
                        || isset($arr_data[$s]['end_time'])
                        || isset($arr_data[$s]['duration'])
                        || isset($arr_data[$s]['status'])
                        /*|| isset($arr_data[$s]['punch time'])*/ )
                    {
                        $isError = false;
                        $current = $s+2;
//                        $err = false;
                        if(!isset($arr_data[$s]['emp_code'])){
                            $err[$current] = ['msg' => 'Employee code is missing'];
                            $isError = true;
                        }else if(!preg_match('/[0-9]/',$arr_data[$s]['emp_code'])){
                            $err[$current] = ['msg' => 'Employee code is incorrect'];
                            $isError = true;
                        }

                        if(!isset($arr_data[$s]['date(dd-mm-yyyy)'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Date is blank'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Date is blank'];
                                $isError = true;
                            }
                        }else if(!preg_match('/[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/',$arr_data[$s]['date(dd-mm-yyyy)'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Date is incorrect'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Date is incorrect'];
                                $isError = true;
                            }
                        }

                        if(!isset($arr_data[$s]['start_time'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Start time is blank'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Start time is blank'];
                                $isError = true;
                            }
                        }else if(!preg_match('/[0-9]{1,2}:[0-9]{1,2}/',$arr_data[$s]['start_time'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Start time is incorrect'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Start time is incorrect'];
                                $isError = true;
                            }
                        }

                        if(!isset($arr_data[$s]['end_time'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', End time is blank'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'End time is blank'];
                                $isError = true;
                            }
                        }else if(!preg_match('/[0-9]{1,2}:[0-9]{1,2}/',$arr_data[$s]['end_time'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', End time is incorrect'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'End time is incorrect'];
                                $isError = true;
                            }
                        }

                        if(!isset($arr_data[$s]['duration'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Duration is blank'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Duration is blank'];
                                $isError = true;
                            }
                        }else if(!preg_match('/[0-9]{1,2}:[0-9]{1,2}/',$arr_data[$s]['duration'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Duration is incorrect'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Duration is incorrect'];
                                $isError = true;
                            }
                        }

                        if(!isset($arr_data[$s]['status'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Status is blank'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Status is blank'];
                                $isError = true;
                            }
                        }else if(!preg_match('/(absent|present|Absent|Present)/',$arr_data[$s]['status'])){
                            if($isError){
                                if(isset($err[$current]) && isset($err[$current]['msg'])){
                                    $tmpMsg = $err[$current]['msg'];
                                    $err[$current] = ['msg' => $tmpMsg.', Status is incorrect'];
                                }
                            }else{
                                $err[$current] = ['msg' => 'Status is incorrect'];
                                $isError = true;
                            }
                        }
                        $status = $arr_data[$s]['status'];
                        /*if($status == 'present' || $status == 'Present') {
                            if (!isset($arr_data[$s]['punch time'])) {
                                if ($isError) {
                                    if (isset($err[$current]) && isset($err[$current]['msg'])) {
                                        $tmpMsg = $err[$current]['msg'];
                                        $err[$current] = ['msg' => $tmpMsg . ', Punch time is blank'];
                                    }
                                } else {
                                    $err[$current] = ['msg' => 'Punch time is blank'];
                                    $isError = true;
                                }
                            }
                        }*/

                        if($isError){
                            $error[$current] = array(
                                'errorMsg' => $err[$current]['msg']
                            );
                        }else{
                            for($s=0;$s<count($arr_data);$s++)
                            {
                                $arr_data[$s] = array_values($arr_data[$s]);
                                if($arr_data[$s][0] != ''){
                                    $this->load->model('User_model');
                                    $user_id = $this->User_model->getUserIdByEmpCode(array('emp_code' => $arr_data[$s][0]));
                                    if($user_id){
                                        $status = $arr_data[$s][5];
                                        $swipe_data = array(
                                            'emp_code' => trim($arr_data[$s][0]),
                                            'user_id' => trim($user_id),
                                            'date' => date('Y-m-d',strtotime($arr_data[$s][1])),
                                            'start_time' => trim($arr_data[$s][2]),
                                            'end_time' => trim($arr_data[$s][3]),
                                            'duration' => trim($arr_data[$s][4]),
                                            'status' => trim(strtolower($status)),
                                            'punch_time' => (isset($arr_data[$s][6]) && $arr_data[$s][6])?$arr_data[$s][6]:'',
                                        );

//                                        $punch_time = $arr_data[$s][6];
                                        $isAttendancePresent = $this->Attendance_modal->checkAttendanceExist($swipe_data);
                                        if(!$isAttendancePresent){
                                            $attendance_id = $this->Attendance_modal->addAttendance($swipe_data);
                                        }else{
                                            $attendance_id = $isAttendancePresent;
                                            $this->Attendance_modal->updateAttendance($attendance_id , $swipe_data);
                                        }
                                        /*if($status == 'present' || $status == 'Present'){
                                            $punch_data = array();
                                            $punch_time = explode(',',$punch_time);
                                            for($r=0;$r<count($punch_time);$r++)
                                            {
                                                if($punch_time[$r] != ''){
                                                    $intime = explode('-',$punch_time[$r]);
                                                    $in_time = $intime[0];
                                                    $out_time = $intime[1];
                                                    $duration = sec_to_time(time_to_sec($out_time)-time_to_sec($in_time));
                                                    $punch_data[] = array(
                                                        'attendance_id' => $attendance_id,
                                                        'user_id' => $user_id,
                                                        'in_time' => $in_time,
                                                        'out_time' => $out_time,
                                                        'duration' => $duration
                                                    );
                                                }
                                            }
                                            if($isAttendancePresent){
                                                $this->Attendance_modal->deletePunchData(array('attendance_id' => $attendance_id));
                                            }
                                            $this->Attendance_modal->addPunchData($punch_data);
                                        }*/
                                    }else{
                                        $error[$s+2] = array(
                                            'emp_code' => $arr_data[$s][0],
                                            'user_id' => $user_id,
                                            'date' => date('Y-m-d',strtotime($arr_data[$s][1])),
                                            'start_time' => $arr_data[$s][2],
                                            'end_time' => $arr_data[$s][3],
                                            'duration' => $arr_data[$s][4],
                                            'status' => $arr_data[$s][5],
                                            'errorMsg' => 'Employee Id not found'
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
                if(isset($error) && count($error)>0){
                    $this->session->set_userdata('uploadError',$error);
                }
                $this->session->set_userdata('message','Uploaded successfully.');
                redirect(WEB_BASE_URL.'index.php/Attendance');

            } else {
                $this->session->set_userdata('message','Invalid File Extension');
                redirect(WEB_BASE_URL.'index.php/Attendance');
            }
        }else{
            $this->session->set_userdata('upload','No File Uploaded');
            redirect(WEB_BASE_URL.'index.php/Attendance');
        }
    }

    public function attendanceList()
    {
        $data['content'] = 'attendance/attendanceList';
        $this->load->view('partials/landing',$data);
    }

    public function getAttendanceGrid(){
        $params = $this->input->get();
        $list = $this->Attendance_modal->getAttendanceGrid($params);
        echo json_encode($list);
    }

    public function getAttendanceLatestGrid(){
        $params = $this->input->get();
        $list = $this->Attendance_modal->getAttendanceLatestGrid($params);
        echo json_encode($list);
    }

    public function addAttendance() {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $attendanceId = $data['id_attendance'];
        $res = $this->Attendance_modal->getAttendanceById(array('id_attendance' => $attendanceId));
        if(count($res)>0){
            if($res[0]->status != $data['status']){
                echo json_encode(array('status'=>false,'error'=>true,'message'=>'Attendance already present','data'=>[]));exit;
            }
        }else{
            //no data found
        }
        if($data['status'] == 'present'){
            $duration = sec_to_time(time_to_sec($data['end_time'])-time_to_sec($data['start_time']));
            $swipe_data = array(
                'status' => 'present',
                'start_time' => $data['start_time'],
                'end_time' => $data['end_time'],
                'duration' => $duration,
                'punch_time' => $data['punch_time'],
            );
            $res = $this->Attendance_modal->updateAttendance($attendanceId,$swipe_data);

            /*$user_id = $data['user_id'];

            $this->Attendance_modal->deletePunchData(array('attendance_id' => $attendanceId));
            if(isset($data['swipe']) && is_array($data['swipe'])){
                foreach($data['swipe'] as $k=>$v){
                    $in_time = $v['swapin'];
                    $out_time = $v['swapout'];
                    $duration = sec_to_time(time_to_sec($out_time)-time_to_sec($in_time));
                    $punch_data[] = array(
                        'attendance_id' => $attendanceId,
                        'user_id' => $user_id,
                        'in_time' => $in_time,
                        'out_time' => $out_time,
                        'duration' => $duration
                    );
                }
                $this->Attendance_modal->addPunchData($punch_data);
            }*/
        }else{
            $swipe_data = array(
                'status' => 'absent',
                'start_time' => '00:00',
                'end_time' => '00:00',
                'duration' => '00:00',
            );
            $res = $this->Attendance_modal->updateAttendance($attendanceId,$swipe_data);
//                $this->Attendance_modal->deletePunchData(array('attendance_id' => $attendanceId));
        }
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$res));exit;
    }

    public function getSwapTimeByAttendanceId(){
        $id = $this->input->post('attendance_id');
        $edit = $this->input->post('edit');
        $result['swap'] = $this->Attendance_modal->getSwapTimeByAttendanceId(array('attendance_id' => $id));
        $result['attendanceId'] = $id;
        $result['edit'] = $edit;
        $data=$this->load->view('modals/swapDetails',$result, TRUE);
        $this->output->set_output($data);
    }

}