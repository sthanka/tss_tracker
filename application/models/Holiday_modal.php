<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Holiday_Modal extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    public function checkHolidayExist($data){
        $this->db->select('*');
        $this->db->from('holidays');
        if(isset($data['date']))
            $this->db->where("date=STR_TO_DATE('".$data['date']."', '%Y-%c-%e')");

        if(isset($data['attendance_date']))
            $this->db->where("date=date('".$data['attendance_date']."')");

        $result = $this->db->get()->result();
//        die($this->db->last_query());
        if(count($result) > 0){
            return $result[0]->id_holiday;
        }else{
            return false;
        }
    }

    public function addAttendance($data){
        $this->db->insert('attendance', $data);
        return $this->db->insert_id();
    }

    public function updateHoliday($id_holiday , $data){
        $this->db->where('id_holiday', $id_holiday);
        $this->db->update('holidays', $data);
        return $id_holiday;
    }

    public function addHolidayBatchData($data) {
        $this->db->insert_batch('holidays', $data);
        return $this->db->insert_id();
    }

    public function addSingleData($data) {
        $this->db->insert('holidays', $data);
        return $this->db->insert_id();
    }

    public function deleteHolidayById($id){
        $this->db->where('id_holiday', $id);
        $this->db->delete('holidays');
        return true;
    }

    public function getHolidayGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = $paramArr['sidx'] != ''?$paramArr['sidx']:'date';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam))  {
            $whereParam = specialCharReplace($whereParam);
            if(preg_match_all('/-/', $whereParam)){
                $whereParam = " AND DATE_FORMAT(date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') ";
            }else{
                $whereParam = " AND (title LIKE '%".$whereParam."%' || description LIKE '%".$whereParam."%' )";
            }
        }

        $whereClause = "where true ".$whereParam;

        if($sortField=='date')
            $sortField = " DATE(date) ";

        $SQL = "SELECT *, DATE_FORMAT(date, '%d-%b-%Y') date, if(date>now(),true,false) future
              from holidays
                $whereClause order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT count(*)
                 from holidays
                $whereClause ";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $final = $list = $result->result();
            return array('rows'=>$final,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    /*Get list of holidays, Full Calender page, */
    public function getListOfHolidaysByDateRange($data){
        $SQL =  'select id_holiday, DATE_FORMAT(date, \'%Y-%m-%d\') date, title, description
                from holidays
                WHERE Date(date) between STR_TO_DATE(\''.$data['start_date'].'\', \'%c/%e/%Y\') AND STR_TO_DATE(\''.$data['end_date'].'\', \'%c/%e/%Y\')';
        $result = $this->db->query($SQL);
        $result = $result->result_array();
        //echo $this->db->last_query(); exit ;
        return $result;
    }

}