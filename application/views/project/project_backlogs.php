<div id="vtab01" class="tab-pane padding0 active">
    <div class="col-sm-12 clearfix clearboth padding0">
        <div class="tasks-list">
            <div class="tasks-col">
                <h3 class="normal-heading">Back Logs</h3>
                <div class="vScroll">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <?php foreach ($backLogList as $task): ?>
                            <div class="task-block actions-exists">
                                <p class="">
                                    <a class="link" href="<?=WEB_BASE_URL?>index.php/Project/taskDetails/<?=$task['id_project_task']?>"><?= $task['task_name'] ?></a>
                                    <a class="pull-right task-actions" onclick="showAction(this)">
                                        <i class="fa fa-cog"></i>
                                    </a>
                                <ul class="actions" style="">
                                    <li><a onclick="backLogsprintModal(this)"
                                           data-task-id="<?= $task['id_project_task'] ?>" href="#">Move To Sprint</a>
                                    </li>
                                </ul>
                                </p>
                                <label class="module-name"><?= $task['module_name'] ?></label>
                                <p class="task-dates">
                                        	<span><?= $task['start_date'] ?>
                                                <i>Start Date</i>
                                            </span>
                                            <span><?= $task['end_date'] ?>
                                                <i>End Date</i>
                                            </span>
                                </p>
                                <p>Estimated Time: <b><?= $task['estimated_time'] ?> Hours</b></p>
                          <!--      <p>Actual Time: <b>6 Hours</b></p>
                                <div class="info-links col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <b>3</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <b>3</b>
                                    </div>
                                    <div class="col-sm-4 padding0 text-center">
                                        <a class="link mr5">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <b>3</b>
                                    </div>
                                </div>-->
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php foreach ($sprintDetails as $sprint): ?>
                <div class="tasks-col">
                    <h3 class="normal-heading"><?= $sprint['name'] ?></h3>
                    <div class="vScroll">
                        <div class="col-sm-12 clearfix clearboth padding0">
                            <?php foreach ($sprint['task'] as $task): ?>
                                <div class="task-block actions-exists">
                                    <p>
                                        <a class="link" href="<?=WEB_BASE_URL?>index.php/Project/taskDetails/<?=$task['id_project_task']?>"><?= $task['task_name'] ?></a>
                                        <a class="pull-right task-actions" onclick="showAction(this)">
                                            <i class="fa fa-cog"></i>
                                        </a>
                                    <ul class="actions" style="">
                                        <li><a onclick="backLogsprintModal(this)"
                                               data-task-id="<?= $task['id_project_task'] ?>" href="#">Change Sprint</a>
                                        </li>
                                        <li><a  onclick="deleteSprint(this)"
                                                data-task-id="<?= $task['id_project_task'] ?>" href="#">Remove</a></li>
                                    </ul>
                                    </p>
                                    <label class="module-name"><?= $task['module_name'] ?></label>
                                    <p class="task-dates">
                                        	<span><?= $task['start_date'] ?>
                                                <i>Start Date</i>
                                            </span>
                                            <span><?= $task['end_date'] ?>
                                                <i>End Date</i>
                                            </span>
                                    </p>
                                    <p>Estimated Time: <b><?= $task['estimated_time'] ?> Hours</b></p>
                                <!--    <p>Actual Time: <b>6 Hours</b></p>
                                    <div class="info-links col-sm-12 clearfix clearboth padding0">
                                        <div class="col-sm-4 padding0 text-center">
                                            <a class="link mr5">
                                                <i class="fa fa-users"></i>
                                            </a>
                                            <b>3</b>
                                        </div>
                                        <div class="col-sm-4 padding0 text-center">
                                            <a class="link mr5">
                                                <i class="fa fa-paperclip"></i>
                                            </a>
                                            <b>3</b>
                                        </div>
                                        <div class="col-sm-4 padding0 text-center">
                                            <a class="link mr5">
                                                <i class="fa fa-files-o"></i>
                                            </a>
                                            <b>3</b>
                                        </div>
                                    </div>-->
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div id="backLogsprint-modal" class="modal-wrapper" style="display: none">
    <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                class="glyphicon glyphicon-remove"></i></a></div>
                    <h4 class="panel-title">Sprint </h4>
                </div>
                <form id="backLogsprintForm" method="post">

                    <div class="panel-body">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container">
                                    <select name="sprint_id" id="sprint_id">
                                        <option value="0">Select Sprint</option>
                                        <?php foreach ($sprintList as $sprint) { ?>
                                            <option <?php if ($sprint['id_sprint'] == 0) {
                                                echo "selected='selected'";
                                            } ?> value="<?= $sprint['id_sprint'] ?>"><?= $sprint['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="control-label">Sprint :</label>
                            </div>
                        </div>


                    </div>
                    <div class="panel-footer text-center">

                        <button id="submitList" class="button-common" div-submit="true">Submit</button>
                        <button class="button-color" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    TssLib.docReady(function(){
       /* $('.tasks-list').width($('.tasks-col').length * 205);
        $('.vScroll').css('max-height',400)
        $('.vScroll').mCustomScrollbar({ theme: "dark", scrollbarPosition: 'inside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: false });
        $('.hScroll').mCustomScrollbar({
            axis: "x",
            theme: "dark",
            scrollButtons: {
                enable: true,
                scrollAmount: 100
            },
            scrollInertia: 10,
            scrollEasing:"easeInOut",
            autoExpandScrollbar: true
        });*/
    });
    function deleteSprint(data){
        var formData = {
            'sprint_id': 0,
            'task_id': $(data).attr('data-task-id')
        };

        $.ajax({
            url: '<?php echo site_url('/project/taskSprintChange') ?>',
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                $("#backlogTab").trigger( "click" );
            }
        });
    }
    function backLogsprintModal(data) {
        var $modal = TssLib.openModel({width: 500}, $('#backLogsprint-modal').html());

        $modal.find('#backLogsprintForm').on('submit', function (e) {
            e.preventDefault();

            var formData = {
                'sprint_id': $modal.find('#backLogsprintForm').find('#sprint_id').val(),
                'task_id': $(data).attr('data-task-id')
            };

            $.ajax({
                url: '<?php echo site_url('/project/taskSprintChange') ?>',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    $("#backlogTab").trigger( "click" );
                    TssLib.closeModal();
                }
            });
            return false;
        });
    }
</script>