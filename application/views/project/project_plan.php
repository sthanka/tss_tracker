<div class="mainpanel" id="budgetTemplateTbl_wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper padding0 main-work-area">
        <div class="contentHeader">
            <h3>Project Details</h3>
        </div>
        <div class="col-sm-12 clearboth clearfix pb20 bg-white ml15">
            <div class="col-sm-9 padding0">
                <div class="col-sm-4">
                    <div class="form-group padding0">
                        <div class="multi-select-container">
                            <select id="projectDropdown" class="form-control" enablefiltering="true">
                            </select>
                        </div>
                        <label class="control-label col-sm-12">Project:</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <button onclick="refreshUrl(this)" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                </div>

            </div>
        </div>

        <div id="projectResult" class="col-sm-12 pl0 pr0"></div>

        <div id="load_week_workload"></div>
</div>

























<script>
    var project_id = 0;
    TssLib.docReady(function () {
        postJsonAsyncWithBaseUrl("project/get_projects", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var projects = result.data;
                    //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                    TssLib.populateSelect($('#projectDropdown'), {
                        success: true,
                        data: projects
                    }, 'project_name', 'id_project');
                    $("#projectDropdown").select2("val", $("#projectDropdown option:eq(1)").val());

                    refreshUrl()
                }
            }
        });

        /*$(".msk_time").onchange(function(){
            console.log(0);
            getUpdateData(this);
        });*/
        /*$("#load_week_workload .msk_time").keyup(function(){
            alert("The text has been changed.");
        });*/

        $("#load_week_workload").delegate(".msk_time", "keyup", function(){
            getUpdateData(this);
        });

    });

    function refreshUrl()
    {
        //window.location = WEB_BASE_URL+'index.php/reports/getWeekWorkloadReport/'+$week;
        var id = $('#projectDropdown').val();
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('index.php/Project/getProjectPlan')?>',
            data: {id:id},
            dataType: '',
            async: true,
            success: function (result) {
                $('#load_week_workload').html(result);
            }
        });
    }


    function getUpdateData(ele)
    {
        var value = ele.value;
        var keys = $(ele).attr('name');
        var project_id = $('#projectDropdown').val();
        //console.log(project_id);
        var arr = keys.split('_');
        var val = value.split(':');
        var updatedTime = (+val[0]) * 60 * 60 + (+val[1]) * 60;

        val = arr[2].split(':');
        var allotedTime = (+val[0]) * 60 * 60 + (+val[1]) * 60;

        if(updatedTime>allotedTime){
            TssLib.notify('Exceeds more time', 'warn', 5);
            return false;
        }else{
            var isValid = /^([0-9]?[0-9]|9[0-9]):([0-5][0-9])(:[0-5][0-9])?$/.test(value);
            if(isValid){
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url('index.php/Project/getUpdateProjectPlan')?>',
                    data: {user_id:arr[0],project_week_id:arr[1],project_id:project_id,value:value},
                    dataType: '',
                    async: true,
                    success: function (result) {
                        //console.log(result);
                    }
                });
            }
        }
    }

</script>