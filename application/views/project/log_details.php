<?php
/**
 * Created by PhpStorm.
 * User: rameshpaul
 * Date: 26/5/16
 * Time: 3:17 PM
 */
?>
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Log Details</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Log Time</h3>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="col-sm-12 clearboth clearfix pb20">
                        <div class="col-sm-2">
                            <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select id="task_users"  class="form-control"  includeselectalloption="true" enablefiltering="true">
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Users:</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group padding0">
                                <div class="input_container">
                                    <input type="text" class="rval form-control tssDatepicker" name="log_start_date" id="log_start_date" placeholder="yyyy-mm-dd">
                                </div>
                                <label class="control-label col-sm-12">Start Date:</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group padding0">
                                <div class="input_container">
                                    <input type="text" class="rval form-control tssDatepicker" name="log_end_date" id="log_end_date" placeholder="yyyy-mm-dd">
                                </div>
                                <label class="control-label col-sm-12">End Date:</label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button onclick="filterSearch()" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                        </div>
                    </div>
                    <div class="tbl_wrapper border0">
                        <table id="log-list" class="table-responsive"></table>
                        <div id="log-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    var $refTblgrid = $('#log-list');
    TssLib.docReady(function () {
        $refTblgrid.jqGrid({
            url: TssConfig.TT_SERVICE_URL + 'project/logDetailsGrid',
            multiselect: false,
            datatype: "json",
            sortorder: "desc",
            extSearchField: '.searchInput',
            colNames: ['Task Name','Email', 'Estimated Time','Log Time','Date'],
            colModel: [
                { name: 'task_name', index: 'task_name' },
                { name: 'email', index: 'email' },
                { name: 'estimated_time', index: 'estimated_time' },
                { name: 'duration', index: 'duration' },
                { name: 'created_date_time', index: 'created_date_time' },
            ],
            pager: 'log-list-page'
        }).navGrid('#log-list-page', {
            edit: false, add: false, del: false, search: false, refresh: true
        });
        postJsonAsyncWithBaseUrl("User/getAllUsers", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var  projects = result.data;
                    //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                    TssLib.populateSelect($('#task_users'), { success: true, data: projects }, 'email', 'id_user');
                }
            }
        });
    });
    function filterSearch(){
        $refTblgrid.jqGrid("setGridParam", {
            postData:{
                'task_users':$('#task_users').val(),
                'log_start_date':$('#log_start_date').val().split("/").reverse().join("-"),
                'log_end_date':$('#log_end_date').val().split("/").reverse().join("-")
            }
        }).trigger("reloadGrid");
        //postData:{department_ids:$('#department_filter').val(), project_ids:$('#project_filter').val() },
        $('#all-tasks-list').trigger('reloadGrid');
    }
</script>

</div>
<!--<a href="javascript:;" class="footer-logo clearfix"><img src="<?/*=WEB_BASE_URL*/?>images/people-combine-logo.png" /></a>-->
</div>
<!--Render Body End-->