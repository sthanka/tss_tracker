<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
         <div class="contentHeader">
            <h3>Day Monitoring</h3>
        </div>
         <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
            <div class="grid-details-table-header">
                <h3>
                    Monitoring
                </h3>
            </div>
            <div class="padding0">
                    <div class="contentHeader">
                       <!-- <h4 class="margin0">Filters :<a class="ml5" href="javascript:;">Clear All</a>-->
                            <div class="pull-left">
                                <div class="form-group padding0">
                                    <div class="input_container margin0">
                                        <input type="text" placeholder="dd/mm/yyyy" id="log_start_date" enddate="0" name="log_start_date" class="rval form-control tssDatepicker">
                                    </div>
                                    <label class="control-label col-sm-12">Select Date:</label>
                                </div>
                            </div>
<!--                        </h4>-->
                    </div>
                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <div class="col-sm-10 padding0">
                             <div class="col-sm-3">
                                <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select id="projectDropdown" class="form-control" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Project:</label>
                                 </div>
                            </div>
<!--                            <div class="col-sm-4">
                                <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select id="projectDropdown" class="form-control" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Workload:</label>
                                 </div>
                            </div>-->
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select id="departmentDropdown" class="form-control" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Department:</label>
                                 </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select id="employeeDropdown" class="form-control" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Employee:</label>
                                 </div>
                            </div>
                            <div class="col-sm-3">
                                <button onclick="applyMonitoringFilter(this)" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                            </div>
                        </div>
                        <div class="col-sm-2 padding0">
                            <ul class="dayMonitoring-legends">
                                <li><span class="task-h">Tasking Hours</span></li>
                                <li><span class="meet-h">Meeting Hours</span></li>
                                <li><span class="free-h">Free Hours</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-wrapper" id="filteredTable">
                        
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#log_start_date').val(new Date().format('d/m/Y') );

    	postJsonAsyncWithBaseUrl("project/get_projects", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var projects = result.data;
                    //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                    TssLib.populateSelect($('#projectDropdown'), {
                        success: true,
                        data: projects
                    }, 'project_name', 'id_project');
                }
            }
        });
        postJsonAsyncWithBaseUrl("Department/getDepartments", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var department = result.data.data;
                    //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                    TssLib.populateSelect($('#departmentDropdown'), {
                        success: true,
                        data: department
                    }, 'department_name', 'id_department');
                }
            }
        });
        postJsonAsyncWithBaseUrl("user/getAllUsers", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var projects = result.data;
                    //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                    TssLib.populateSelect($('#employeeDropdown'), {
                        success: true,
                        data: projects
                    }, 'first_name', 'id_user');
                }
            }
        });
    });
    function applyMonitoringFilter(This) {
        var projectIds = $('#projectDropdown').val();
        var departmentIds = $('#departmentDropdown').val();
        var employeeIds = $('#employeeDropdown').val();
        var log_start_date = $('#log_start_date').val();
        if(log_start_date.length>0){
            log_start_date = log_start_date.replace(/\//g,'-');
            $.get('<?=site_url('Project/filterDayWiseMonitoring')?>?date='+log_start_date+'&employee='+employeeIds+'&department='+departmentIds+'&project='+projectIds, function (result) {
                $('#filteredTable').html(result);
            });
        }else{
//            console.log('error');
            TssLib.notify('Please select a date ', 'warn', 5);
        }
    }
</script>