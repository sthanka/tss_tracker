<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <!-- InstanceBeginEditable name="EditRegion3" -->

        <div class="contentHeader">
            <h3>Workload</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Workload for Team
                    </h3>
                </div>
                <div class="grid-details-table-content">


                    <div class="row clearfix clearboth padding10">
                        <h3 class="heading border0">Projects Workload</h3>

                        <div class="grid-details-table-content padding0">
                            <div class="col-sm-12 clearboth clearfix pb20">
                                <div class="col-sm-4">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <select id="project_filter" class="form-control" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Project:</label>
                                    </div>
                                </div>
                                <?php if($this->session->userdata('user_type_id') == 2): ?>
                                <div class="col-sm-4">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <select id="department_filter" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Department:</label>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="col-sm-4">
                                    <button onclick="projectWorkLoad()" class="button button-common module mt20"
                                            div-submit="true" id=""> Search
                                    </button>
                                </div>
                            </div>

                            <div class="grid-details-table-content clearfix padding0">
                                <!-- <div class="col-sm-12 clearfix">
                                     <div class="pull-left"><h5 class="margin0">Module : <span class="blue">CRM</span></h5></div>
                                     <div class="pull-right new-tsk-tab-top-right">
                                         <span>Tasks</span>
                                         <span>OverDue Tasks</span>
                                         <span><a href="javascript:;">All Tasks</a></span>
                                     </div>
                                 </div>-->
                                <div class="tbl_wrapper border0">
                                    <table id="all-tasks-list" class="table-responsive"></table>
                                    <div id="tasks-list-page"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div id="workloadTable">

                    </div>


                </div>
            </div>
        </div>


        <link rel="stylesheet" type="text/css" href="<?= WEB_BASE_URL ?>/css/bootstrap-fileupload.min.css">
        <script type="text/javascript" src="<?= WEB_BASE_URL ?>/scripts/bootstrap-fileupload.min.js"></script>
        <script src="<?= WEB_BASE_URL ?>/scripts/highcharts/highcharts.js"></script>
        <script src="<?= WEB_BASE_URL ?>/scripts/highcharts/highcharts-more.js"></script>
        <script>
            TssLib.docReady(function () {
                postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            var projects = result.data;
                            //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                            TssLib.populateSelect($('#project_filter'), {
                                success: true,
                                data: projects
                            }, 'project_name', 'id_project');
                        }
                    }
                });

                $('.selectable-list > li').on('click', function (e) {
                    $('.selectable-list > li').removeClass('selected');
                    $(this).addClass('selected');
                    var projectId = $(this).attr('data-project-id');
                    //console.log('projectId',projectId);
                    $.get('<?=site_url('Workload/workloadTaskList')?>/' + projectId, function (result) {
                        //console.log('result', result);
                        $('#workloadTable').html(result);
                    });

                })
                $('.selectable-list li:first-child').trigger('click');


                var page = '';
                $('ul#project-tab>li>a').click(function () {
                    var cur = $(this);
                    $('ul#project-tab>li').removeClass('active')
                    cur.parent().addClass('active');
                    page = 'Pages/ProjectUser/';
                    var tabContext = cur.attr('tab-context');
                    if (tabContext === 'tasks') {
                        page += 'tasks.html';
                    }
                    if (tabContext === 'time') {
                        page += 'time.html';
                    }
                    if (tabContext === 'work-load') {
                        page += 'work-load.html';
                    }
                    if (tabContext === 'billing') {
                        page += 'billing.html';
                    }
                    if (tabContext === 'config') {
                        page += 'config.html';
                    }

                    if (page != 'Pages/ProjectUser/') {
                        getPageAsyncWithSiteUrl(page, null, {
                            callback: function (rs) {
                                if (rs.success) {
                                    var ele = $('#tabContext');
                                    if (!ele.hasClass('active'))ele.addClass('active');
                                    ele.empty().append(rs.data);
                                    TssLib.bindAllEventsOnPageRender(ele);
                                }
                            }
                        });
                    }
                });
                $('a[tab-context="tasks"]').trigger('click');

            });
            function projectWorkLoad() {
                var $project_ids = $('#project_filter').val();
                var $department_ids = $('#department_filter').val();
                //console.log('$project_ids',$project_ids);
                if ($project_ids) {
                    $.post('<?=site_url('Workload/workloadTaskList')?>', {'projectIds': $project_ids,'departmentIds':$department_ids}, function (result) {
                        //console.log('result', result);
                        $('#workloadTable').html(result);
                    });
                }
            }
            postJsonAsyncWithBaseUrl("client/getClients", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        departmentsData = result.data;
                        //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                        TssLib.populateSelect($('#department_filter'), {
                            success: true,
                            data: departmentsData.departments
                        }, 'department_name', 'id_department');
                    }
                }
            });

            /*     function changeStatus(id) {
             var $modalPopup = TssLib.openModel({width: 550}, $('#status-modal').html());

             TssLib.ajaxForm({
             jsonContent: true,
             doServiceCall: true,
             form: $('#status-form'), callback: function (data) {
             if (data.success) {
             if (isForEdit) {
             TssLib.notify('status Updated Successfully');
             }
             TssLib.closeModal();
             $taskmanager_1.trigger('reloadGrid');
             }
             }, before: function (formObj) {
             $(formObj).attr('action-send', TssConfig.BASE_URL + 'Service.svc');
             return true;
             }
             });
             }

             function changePriority(id) {
             var $modalPopup = TssLib.openModel({width: 550}, $('#priorities-modal').html());

             TssLib.ajaxForm({
             jsonContent: true,
             doServiceCall: true,
             form: $('#priorities-form'), callback: function (data) {
             if (data.success) {
             if (isForEdit) {
             TssLib.notify('Priorities Updated Successfully');
             }
             TssLib.closeModal();
             $taskmanager_1.trigger('reloadGrid');
             }
             }, before: function (formObj) {
             $(formObj).attr('action-send', TssConfig.BASE_URL + 'Service.svc');
             return true;
             }
             });
             }
             function removeMember(e) {
             $(e).closest('li').remove();
             }
             function addMember(e) {
             var _memberName = $(e).prev().val();
             if (_memberName.length > 0) {
             $('#teamMembers').append('<li><label>' + _memberName + '</label><a onclick="removeMember(this);">X</a></li>');
             $(e).prev().val('').focus();
             } else {
             TssLib.notify('Please Enter Team Member Name', 'warn', 5);
             }
             }
             */

        </script>
        <!-- InstanceEndEditable -->

    </div>

</div>