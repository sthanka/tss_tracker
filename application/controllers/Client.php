<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller
{

    /**
     * Project Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Client_modal');
    }

    public function index(){
        $data['content'] = 'clients/index';
        $this->load->view('partials/landing',$data);
    }

    public function getClientsGrid()
    {
        $params = $this->input->get();
        $clientList = $this->Client_modal->getClientsGrid($params);
        echo json_encode($clientList);
    }

    public function addClient()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $id = $data['id_client'];

        $result = $this->Client_modal->checkClientName($id, $data['client_name']);
        if (empty($result)) {
            if($id == null && $id == '') {
                $dep = $this->Client_modal->addClient($data);
            }else{
                $dep = $this->Client_modal->updateClient($id,$data);
            }
            echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$dep));
        } else {
            echo json_encode(array('status' => false, 'message' => 'Department name exist'));
        }
    }

    public function deleteClient($id)
    {
        $projectList = $this->Client_modal->deleteClient($id);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectList));
    }

    public function getClients()
    {
        $clientsList = $this->Client_modal->getClients();
        $departmentsList = $this->Client_modal->getDepartments();
        echo json_encode(['clients'=>$clientsList, 'departments'=>$departmentsList]);
    }

}