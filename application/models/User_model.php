<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    public function encode($value)
    {
        return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->key), $value, MCRYPT_MODE_CBC, md5(md5($this->key)))),'+/=', '-_,');
    }
    public function decode($value)
    {
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->key), base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_CBC, md5(md5($this->key))), "\0");
    }
    public function getUsersGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = $paramArr['sidx'] != ''?$paramArr['sidx']:'id_user';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and u.first_name LIKE '%".$whereParam."%' or u.last_name LIKE '%".$whereParam."%' or u.email LIKE '%".$whereParam."%' or u.phone_number LIKE '%".$whereParam."%'"; }
        $whereClause = "where true ".$whereParam;
        //echo '<pre>'; print_r($whereClause); exit;
        $user_id = $this->session->userdata('user_id');

        $SQL = "SELECT u.id_user, u.first_name, u.last_name, u.email, u.employee_id, u.gender, u.email, u.phone_number, u.status, CONCAT_WS(' ',u.first_name,u.last_name) as name, u.is_lead
          from `user` u
        $whereClause and user_type_id=3 and email is not NULL
        order by $sortField $sortOrder $optLimit";

        $result = $this->db->query($SQL)->result();

        $SQLCount = "select count(*) from `user` u $whereClause and user_type_id=3 and email is not NULL";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if(count($result) > 0) {
            $custlist = $result;
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }
    public function login($data)
    {
        $this->db->select('ur.user_role_name,u.user_role_id,u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.profile_image,c.id_company,c.company_name,c.company_logo,cb.legal_name,ar.approval_name as role_name');
        $this->db->from('user u');
        $this->db->join('user_role ur','u.user_role_id=ur.id_user_role','left');
        $this->db->join('company c','c.user_id=u.id_user','left');
        $this->db->join('company_user cu','u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->where(array('u.email_id' => $data['email_id'], 'u.password' => md5($data['password']), 'u.user_status' => 1));
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllUsers($data)
    {
        $this->db->select('*');
        $this->db->from('user');
        if(isset($data['user_type_id']))
            $this->db->where('user_type_id',$data['user_type_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $result = $query->result_array();
        foreach($result as $k=>$v){
            if(strlen(trim($v['first_name'].''.$v['last_name']))<=0 ){
                $result[$k]['first_name'] = $v['email'];
            }
            else{
                $result[$k]['first_name'] = $v['first_name'].' '.$v['last_name'];
            }
        }
        return $result;
    }
    public function getDepUsers($depId)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('department_id',$depId);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getUser($data)
    {
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('user_department ud','ud.user_id = u.id_user','left');
        $this->db->join('department d','d.id_department = ud.department_id','left');
        if(isset($data['id_user']))
            $this->db->where('id_user',$data['id_user']);
        if(isset($data['email']))
            $this->db->where('email',$data['email']);
        if(isset($data['status']))
            $this->db->where('status',$data['status']);
        if(isset($data['manager']))
            $this->db->where('is_manager=1');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getUsersByType($data)
    {
        $this->db->select('u.id_user, u.user_type_id, u.first_name, u.last_name, u.employee_id, u.user_image, u.email, u.is_lead, u.is_manager');
        $this->db->from('user u');
        if(isset($data['id_user']))
            $this->db->where('id_user',$data['id_user']);
        if(isset($data['email']))
            $this->db->where('email',$data['email']);
        if(isset($data['status']))
            $this->db->where('status',$data['status']);
        if(isset($data['manager']))
            $this->db->where('is_manager=1');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function checkEmail($data)
    {
        $this->db->select('*');
        $this->db->from('user');
        if(isset($data['id_user']))
            $this->db->where('id_user !=',$data['id_user']);
        if(isset($data['email']))
            $this->db->where('email',$data['email']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addUser($data)
    {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function updateUser($data)
    {
        $this->db->where('id_user', $data['id_user']);
        $this->db->update('user', $data);
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function addUserData($data)
    {
        $user_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'employee_id' => $data['employee_id'],
            'gender' => $data['gender'],
            'email' => $data['email'],
            'user_type_id' => $data['user_type_id'],
            'phone_number' => $data['phone_number'],
            'status' => $data['status']
        );
        $this->db->insert('user', $user_data);
        $lastInsertId = $this->db->insert_id();
        $departmentUser = $data['department_id'];
        $lead = 0;
        foreach($departmentUser as $k=>$v){
            $this->db->insert('user_department' , array(
                'user_id'=>$lastInsertId,
                'department_id'=>$v['department_id'],
                'is_lead'=>$v['is_lead'])
            );
        }
        return $this->db->insert_id();
    }
    public function updateUserData($data)
    {
        $temp = [];
        $departmentUser = $data['department_id'];
        $this->db->where('user_id', $data['id_user']);
        $this->db->delete('user_department');
        foreach($departmentUser as $k=>$v){
            if(!isset($temp[$v['department_id']])){
                $this->db->insert('user_department' , array(
                        'user_id'=>$data['id_user'],
                        'department_id'=>$v['department_id'],
                        'is_lead'=>$v['is_lead'])
                );
                $temp[$v['department_id']] =$v['department_id'];
            }
        }

        $user_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'employee_id' => $data['employee_id'],
            'gender' => $data['gender'],
            'email' => $data['email'],
            'user_type_id' => $data['user_type_id'],
            'phone_number' => $data['phone_number'],
            'status' => $data['status']
        );
        $this->db->where('id_user', $data['id_user']);
        $this->db->update('user', $user_data);
        return $data['id_user'];    //to get the id of updated user
    }

    public function getJobRole()
    {
        $this->db->select('*');
        $this->db->from('job_role');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectUsers($id)
    {
        $this->db->from('project_department pd');
        $this->db->join('user u','u.department_id = pd.department_id','left');
        $this->db->where('pd.project_id',$id);
        $this->db->group_by('u.id_user');
        $this->db->select(' u.*');
        $this->db->from('user');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDepartmentDetails($data)
    {
        $this->db->select('*');
        $this->db->from('department_member');
        if(isset($data['user_id']))
            $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function updateUserDepartment($data)
    {
        $this->db->where('id_user', $data['id_user']);
        $this->db->update('user', $data);
        //echo $this->db->last_query(); exit;
        return 1;
    }
    public function allTaskStatus($data)
    {
        $department_id = $this->session->userdata('department_id');

        $startDate = date('Y-m-d', strtotime("-21 days"));
        $endDate = date('Y-m-d', strtotime($startDate. "+30 days"));
        //echo $startDate.'---'.$endDate; exit;
        for($i=0; $i<=30;$i++){
            $resultDates[] = date('d', strtotime($startDate. "+".$i." days"));
            $resultLogDates[] = date('Y-m-d', strtotime($startDate. "+".$i." days"));
        }
        //echo '<pre>'; print_r($resultDates); exit;
        $taskLoadMonitor = $resultDates;
        $userId = $data['id_user'];

        /*$que = <<<abc
                    select pt.id_project_task,tm.assigned_to,lt.user_id,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( ttl.time ) ) ), '%H:%i' ),'00:00') as allowted_time,DATE_FORMAT(ttl.date,'%Y-%m-%d') as date,DATE_FORMAT(lt.created_date_time,'%Y-%m-%d'),
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time
                    from  task_flow tw
	                LEFT JOIN task_member tm on tm.task_flow_id=tw.id_task_flow
	                LEFT JOIN project_task pt on tw.project_task_id=pt.id_project_task
	                LEFT JOIN task_week_flow ttl on ttl.task_flow_id=tw.id_task_flow
                    LEFT JOIN log_time lt on tw.id_task_flow=lt.task_flow_id and ttl.date=DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')
	                where tm.assigned_to=$userId and ttl.user_id=$userId and ttl.date between "$startDate" and "$endDate" GROUP BY ttl.date
abc;*/

        if($this->session->userdata('user_type_id')==2)
        {
            $que = <<<abc
                    select 0 as actual_time,pt.id_project_task,tm.assigned_to,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( ttl.time ) ) ), '%H:%i' ),'00:00') as allowted_time,
	                DATE_FORMAT(ttl.date,'%Y-%m-%d') as date
                    from  task_flow tw
	                LEFT JOIN task_member tm on tm.task_flow_id=tw.id_task_flow
	                LEFT JOIN project_task pt on tw.project_task_id=pt.id_project_task
	                LEFT JOIN task_week_flow ttl on ttl.task_flow_id=tw.id_task_flow
	                where tm.assigned_to=$userId and ttl.user_id=$userId and ttl.date between "$startDate" and "$endDate" GROUP BY ttl.date
abc;
        }
        else
        {
            $que = <<<abc
                    select 0 as actual_time,pt.id_project_task,tm.assigned_to,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( ttl.time ) ) ), '%H:%i' ),'00:00') as allowted_time,
	                DATE_FORMAT(ttl.date,'%Y-%m-%d') as date
                    from  task_flow tw
	                LEFT JOIN task_member tm on tm.task_flow_id=tw.id_task_flow
	                LEFT JOIN project_task pt on tw.project_task_id=pt.id_project_task
	                LEFT JOIN task_week_flow ttl on ttl.task_flow_id=tw.id_task_flow
	                where tw.department_id=$department_id and tm.assigned_to=$userId and ttl.user_id=$userId and ttl.date between "$startDate" and "$endDate" GROUP BY ttl.date
abc;
        }

        //echo '<pre>'; print_r($que); exit;
        $result = $this->db->query($que);
        $logActualTimes = $result->result();

        if($this->session->userdata('user_type_id')==2)
        {
            $que = <<<abc
                    select lt.user_id,DATE_FORMAT(lt.created_date_time,'%Y-%m-%d') as date,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time
                    from  log_time lt
                    left join user u on lt.user_id=u.id_user
                    left join user_department ud on u.id_user=ud.user_id
	                where lt.user_id=$userId and DATE_FORMAT(lt.created_date_time,'%Y-%m-%d') between "$startDate" and "$endDate" and lt.project_task_id is not null GROUP BY DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')
abc;
        }
        else
        {
            $que = <<<abc
                    select lt.user_id,DATE_FORMAT(lt.created_date_time,'%Y-%m-%d') as date,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time
                    from  log_time lt
                    left join user u on lt.user_id=u.id_user
                    left join user_department ud on u.id_user=ud.user_id
	                where ud.department_id=$department_id and lt.user_id=$userId and DATE_FORMAT(lt.created_date_time,'%Y-%m-%d') between "$startDate" and "$endDate" and lt.project_task_id is not null GROUP BY DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')
abc;
        }

        //echo '<pre>'; print_r($que); exit;
        $result = $this->db->query($que);
        $logActualTimes1 = $result->result();
        //echo "<pre>"; print_r($logActualTimes); exit;
//        echo "<pre>"; print_r($logActualTimes); exit;
        for($s=0;$s<count($logActualTimes);$s++)
        {
            for($sr=0;$sr<count($logActualTimes1);$sr++)
            {
                if($logActualTimes[$s]->date==$logActualTimes1[$sr]->date){
                    $logActualTimes[$s]->actual_time = $logActualTimes1[$sr]->actual_time;
                }
            }
        }

        foreach($resultLogDates as $resultLogDate){
            $resultTime[$resultLogDate] = ['date'=>$resultLogDate,'actual_time'=>'00:00','allowted_time'=>'00:00'];
            foreach($logActualTimes as $logActualTime){
                if($resultLogDate == $logActualTime->date){
                    $resultTime[$resultLogDate] = ['date'=>$logActualTime->date,'actual_time'=>$logActualTime->actual_time,'allowted_time'=>$logActualTime->allowted_time];
                }
            }
        }

        $this->db->select('count(*) as count,tm.task_status as status');
        $this->db->from('task_flow tw');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        if($this->session->userdata('user_type_id')!=2){
            $this->db->where('tw.department_id',$this->session->userdata('department_id'));
        }
        $this->db->where('tm.assigned_to', $data['id_user']);
        $this->db->where('DATE_FORMAT(tw.start_date,\'%Y-%m-%d\') between "'.$startDate.'" and "'.$endDate.'"');
        $this->db->group_by('tm.task_status');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $results = $query->result_array();
        // for projects

        $this->db->select('*');
        $this->db->from('task_flow twf');
        $this->db->join('project_task pt','pt.id_project_task = twf.project_task_id','left');
        $this->db->join('task_member tm','twf.id_task_flow=tm.task_flow_id','left');
        if($this->session->userdata('user_type_id')!=2) {
            $this->db->where('twf.department_id', $this->session->userdata('department_id'));
        }
        $this->db->where('tm.assigned_to', $data['id_user']);
        $this->db->where('DATE_FORMAT(twf.start_date,\'%Y-%m-%d\') between "'.$startDate.'" and "'.$endDate.'"');
        $this->db->group_by('pt.project_id');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $proCount = $query->num_rows();

        $statusTypes = ['new','progress','accepted','hold','completed'];
        $avbTypes = [];
        foreach($results as $key => $val){
            $avbTypes[] = $val['status'];
        }
        $diff = array_diff($statusTypes,$avbTypes);
        foreach($diff as $item){
            $results[] = ['status'=>$item,'count'=>0];
        }
        $final = [];
        foreach($results as $item){
            switch ($item['status']) {
                case 'new':
                    $color = '#0072BB';
                    break;
                case 'progress':
                    $color = '#f26522';
                    break;
                case 'hold':
                    $color = '#FF150E';
                    break;
                case 'completed':
                    $color = '#5CBA5C';
                    break;
                default:
                    $color = '#5CBA5C';
            }
            $final[$item['status']] = ['name'=>$item['status'],'y'=>$item['count'],'color'=>$color];
        }

        return [$final,$proCount,$taskLoadMonitor,$resultTime];
    }

    public function getUserSprintWorkLoad($id){
        $startDate = date('Y-m-d', strtotime("-21 days"));
        $endDate = date('Y-m-d', strtotime($startDate. "+30 days"));
        /*$query = <<<abc
                    SELECT u.first_name,tw.project_task_id,pt.task_name,pt.start_date,pt.end_date,pt.estimated_time,lt.duration,ttl.time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( ttl.time ) ) ), '%H:%i' ),'00:00') as allowted_time
                    from task_workflow tw
                    LEFT JOIN project_task pt on tw.project_task_id= pt.id_project_task
                    LEFT JOIN log_time lt on pt.id_project_task= lt.project_task_id
                    LEFT JOIN task_time_log ttl on ttl.task_workflow_id=tw.id_task_workflow and ttl.date=DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')
                    LEFT JOIN user u on u.id_user= tw.assigned_to
                    LEFT JOIN sprint s on s.id_sprint= pt.sprint_id
                    WHERE YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1) and u.id_user = $id
abc;*/
        $department_id = $this->session->userdata('department_id');
        if($this->session->userdata('user_type_id')==2)
        {
            $query = <<<abc
                    SELECT u.email,tw.project_task_id,pt.task_name,pt.start_date,pt.end_date,tw.estimated_time,lt.duration,ttl.time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( ttl.time ) ) ), '%H:%i' ),'00:00') as allowted_time
                    from task_flow tw
                    LEFT JOIN project_task pt on tw.project_task_id= pt.id_project_task
                    LEFT JOIN task_week_flow ttl on ttl.task_flow_id=tw.id_task_flow
                    LEFT JOIN log_time lt on pt.id_project_task= lt.project_task_id and ttl.date=DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')
                    LEFT JOIN task_member tm on tm.task_flow_id= tw.id_task_flow
                    LEFT JOIN user u on u.id_user= tm.assigned_to
                    WHERE tm.assigned_to = $id and (DATE_FORMAT(ttl.date,"%Y-%m-%d") >= $startDate and DATE_FORMAT(ttl.date,"%Y-%m-%d") <= $endDate)
abc;
        }
        else
        {
            $query = <<<abc
                    SELECT u.email,tw.project_task_id,pt.task_name,pt.start_date,pt.end_date,tw.estimated_time,lt.duration,ttl.time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( ttl.time ) ) ), '%H:%i' ),'00:00') as allowted_time
                    from task_flow tw
                    LEFT JOIN project_task pt on tw.project_task_id= pt.id_project_task
                    LEFT JOIN task_week_flow ttl on ttl.task_flow_id=tw.id_task_flow
                    LEFT JOIN log_time lt on pt.id_project_task= lt.project_task_id and ttl.date=DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')
                    LEFT JOIN task_member tm on tm.task_flow_id= tw.id_task_flow
                    LEFT JOIN user u on u.id_user= tm.assigned_to
                    WHERE tw.department_id=$department_id and tm.assigned_to = $id and (DATE_FORMAT(ttl.date,"%Y-%m-%d") >= $startDate and DATE_FORMAT(ttl.date,"%Y-%m-%d") <= $endDate)
abc;
        }

        $result = $this->db->query($query);
        $result = $result->result();
        $actual_time = explode(':',$result[0]->actual_time);
        $allowted_time = explode(':',$result[0]->allowted_time);

        $actual_time = ($actual_time[0]*3600)+($actual_time[1]*60);
        $allowted_time = ($allowted_time[0]*3600)+($allowted_time[1]*60);
        if($allowted_time != 0 ){ $percentage = ($actual_time/$allowted_time)*100; }else{ $percentage = 0; }

        return ['percentage'=>intval($percentage),'actual_time'=>$result[0]->actual_time,'allowted_time'=>$result[0]->allowted_time];
    }
    public function getUserkpi($id){
        $department_id = $this->session->userdata('department_id');
        $startDate = date('Y-m-d', strtotime("-21 days"));
        $endDate = date('Y-m-d', strtotime($startDate. "+30 days"));
        if($this->session->userdata('user_type_id')==2)
        {
            $query = <<<abc
                        SELECT tt.product_type,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as duration from log_time lt
                        LEFT JOIN user u on lt.user_id=u.id_user
                        LEFT JOIN task_flow tf on lt.task_flow_id=tf.id_task_flow
                        LEFT JOIN task_type tt on tt.id_task_type = lt.task_type
                        WHERE  lt.user_id = $id and (DATE_FORMAT(lt.created_date_time,"%Y-%m-%d") >= '$startDate' and DATE_FORMAT(lt.created_date_time,"%Y-%m-%d") <= '$endDate')
                        GROUP BY tt.product_type
abc;
        }
        else
        {
            $query = <<<abc
                        SELECT tt.product_type,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as duration from log_time lt
                        LEFT JOIN user u on lt.user_id=u.id_user
                        LEFT JOIN task_flow tf on lt.task_flow_id=tf.id_task_flow
                        LEFT JOIN task_type tt on tt.id_task_type = lt.task_type
                        WHERE lt.user_id = $id and (DATE_FORMAT(lt.created_date_time,"%Y-%m-%d") >= '$startDate' and DATE_FORMAT(lt.created_date_time,"%Y-%m-%d") <= '$endDate')
                        GROUP BY tt.product_type
abc;
        }
        //echo $query;
        $result = $this->db->query($query);
        $result = $result->result_array();
        //echo "<pre>"; print_r($result);
        $finalRes = ['billable','non-billable','other'];
        $avbTypes = [];
        if(count($result)>0){
            foreach($result as $key=>$val ){
                foreach($finalRes as $item ){
                    if($val['product_type'] == $item){
                        $avbTypes[$item] = $val;
                    }else{
                        if(!isset($avbTypes[$item])){ $avbTypes[$item] = ['product_type'=>$item,'duration'=>'00:00'];}
                    }
                }
            }
        }else{
            foreach($finalRes as $item ){
                if(!isset($avbTypes[$item])){ $avbTypes[$item] = ['product_type'=>$item,'duration'=>'00:00'];}
            }
        }
        //echo '<pre>'; print_r($avbTypes); exit;
        return (array)$avbTypes;
    }

    public function getOnTimeDelivery($user_id)
    {
        $department_id = $this->session->userdata('department_id');
        $startDate = date('Y-m-d', strtotime("-21 days"));
        $endDate = date('Y-m-d');
        if($this->session->userdata('user_type_id')==2)
        {
            $sql = "select count(*) as total from task_flow tf LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tm.assigned_to=$user_id
	            AND (tf.actual_end_date=tf.end_date)
	            AND tm.task_status='completed'
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".$startDate."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".$endDate."')";
        }
        else
        {
            $sql = "select count(*) as total from task_flow tf LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tf.department_id=$department_id and tm.assigned_to=$user_id
	            AND (tf.actual_end_date=tf.end_date)
	            AND tm.task_status='completed'
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".$startDate."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".$endDate."')";
        }

        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $completed = $query->result_array();
        if($this->session->userdata('user_type_id')==2){
            $sql = "select count(*) as total from task_flow tf LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tm.assigned_to=$user_id
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".$startDate."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".$endDate."')";
        }
        else
        {
            $sql = "select count(*) as total from task_flow tf LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tf.department_id=$department_id and tm.assigned_to=$user_id
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".$startDate."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".$endDate."')";
        }

        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $total = $query->result_array();

        return $completed[0]['total']."/".$total[0]['total'];
    }

    public function getMileStoneMiss($data=array())
    {
        $user_id = $data['user_id'];
        $department_id = $this->session->userdata('department_id');
        $startDate = date('Y-m-d', strtotime("-21 days"));
        $endDate = date('Y-m-d');

        $sql = 'select * from project_milestones where ';
        if(isset($data['project_id']) && !empty($data['project_id']))
            $sql.=' project_id in ('.join(',',$data['project_id']).') and ';
        $sql.='DATE_FORMAT(milestone_start_date,"%Y-%m-%d") BETWEEN "'.$startDate.'" and "'.$endDate.'"';
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $m_s = $query->result_array();
        //echo "<pre>"; print_r($m_s); exit;
        $m_total = $m_c = 0;
        for($s=0;$s<count($m_s);$s++)
        {
            if($this->session->userdata('user_type_id')==2) {
                $sql = "select count(*) as total from task_flow tf
                LEFT JOIN project_task pt on tf.project_task_id=pt.id_project_task
                LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tm.assigned_to='".$user_id."'
	            AND pt.project_id='".$m_s[$s]['project_id']."'
	            AND (tf.actual_end_date=tf.end_date)
	            AND tm.task_status='completed'
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".date('Y-m-d',strtotime($m_s[$s]['milestone_start_date']))."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".date('Y-m-d',strtotime($m_s[$s]['milestone_end_date']))."')";
            }
            else{
                $sql = "select count(*) as total from task_flow tf
                LEFT JOIN project_task pt on tf.project_task_id=pt.id_project_task
                LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tm.assigned_to='".$user_id."'
	            AND pt.project_id='".$m_s[$s]['project_id']."'
	            AND (tf.actual_end_date=tf.end_date)
	            AND tm.task_status='completed'
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".date('Y-m-d',strtotime($m_s[$s]['milestone_start_date']))."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".date('Y-m-d',strtotime($m_s[$s]['milestone_end_date']))."')";
            }

            $query = $this->db->query($sql);
            //echo $this->db->last_query(); exit;
            $completed = $query->result_array();
            if($this->session->userdata('user_type_id')==2) {
                $sql = "select count(*) as total
                from task_flow tf
                LEFT JOIN project_task pt on tf.project_task_id=pt.id_project_task
                LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tm.assigned_to=$user_id
	            AND pt.project_id='".$m_s[$s]['project_id']."'
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".date('Y-m-d',strtotime($m_s[$s]['milestone_start_date']))."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".date('Y-m-d',strtotime($m_s[$s]['milestone_end_date']))."')";
            }
            else{
                $sql = "select count(*) as total
                from task_flow tf
                LEFT JOIN project_task pt on tf.project_task_id=pt.id_project_task
                LEFT JOIN task_member tm on tf.id_task_flow=tm.task_flow_id
	            where tm.assigned_to=$user_id
	            AND pt.project_id='".$m_s[$s]['project_id']."'
                AND (DATE_FORMAT(tf.start_date,'%Y-%m-%d')>'".date('Y-m-d',strtotime($m_s[$s]['milestone_start_date']))."' and DATE_FORMAT(tf.end_date,'%Y-%m-%d')<'".date('Y-m-d',strtotime($m_s[$s]['milestone_end_date']))."')";
            }

            $query = $this->db->query($sql);
            //echo $this->db->last_query(); exit;
            $total = $query->result_array();

            $m_c = $m_c + $completed[0]['total'];
            $m_total = $m_total + $total[0]['total'];
        }

        return $m_c."/".$m_total;
    }

    function getUserDepartments($data)
    {
        $this->db->select('*');
        $this->db->from('user_department ud');
        $this->db->join('department d','ud.department_id=d.id_department','left');
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function deleteUserById($userId){
        try{
            $this->db->where('user_id', $userId);
            $this->db->delete('user_department');

            $this->db->where('id_user', $userId);
            $this->db->delete('user');
            return 1;
        }catch(Exception $ex){
//            print_r($ex);die('asd');
        }

    }
    public function checkUserAssigned($userId){
        $this->db->select('id_employee');
        $this->db->from('project_employees');
        $this->db->where('employee_id',$userId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }

    public function getUserIdByEmpCode($data){
        $this->db->select('id_user');
        $this->db->from('user');
        if(isset($data['emp_code']))
            $this->db->where('employee_id', $data['emp_code']);

        $result = $this->db->get()->result();
        if(count($result) > 0){
            return $result[0]->id_user;
        }else{
            return false;
        }
    }

    public function userModule(){
        $data['content'] = 'users/user_module';
        $this->load->view('partials/landing',$data);
    }

    public function getUsersModuleGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = $paramArr['sidx'] != ''?$paramArr['sidx']:'id_module';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
//        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam);  $whereParam = "and u.first_name LIKE '%".$whereParam."%' or u.last_name LIKE '%".$whereParam."%' or u.email LIKE '%".$whereParam."%' or u.phone_number LIKE '%".$whereParam."%'";}
        $whereClause = "where true ".$whereParam;
        //echo '<pre>'; print_r($whereClause); exit;
//        $user_id = $this->session->userdata('user_id');

        $SQL = "SELECT *
          from `module`
        $whereClause
        order by $sortField $sortOrder $optLimit";

        $result = $this->db->query($SQL)->result();

        $SQLCount = "select count(*) from `module`  $whereClause ";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if(count($result) > 0) {
            $custlist = $result;
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function checkDuplicateUserModule($data){
        $this->db->select('*');
        $this->db->from('module');
        if(isset($data))
            $this->db->where('module_name', $data['module_name']);
        if(isset($data['id_module'])){
            $this->db->where('id_module!='.$data['id_module']);
        }

        $res = $this->db->get()->result();
        if(count($res)>0){
            return true;
        }else{
            return false;
        }
    }

    public function addUserModule($data){
        $this->db->insert('module', $data);
        return $this->db->insert_id();
    }

    public function updateUserModule($id , $data){
        $this->db->where('id_module', $id);
        $this->db->update('module', $data);
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function getAllUserModule(){
        $this->db->select('*');
        $this->db->from('module');
        $this->db->where('is_active', 1);

        return $this->db->get()->result();
    }

    public function getUserSpecificModule($data){
        $this->db->select('*');
        $this->db->from('module m');
        $this->db->join('user_module um', 'm.id_module=um.module_id', 'left');
        if(isset($data['id_module'])){
            $this->db->where('um.module_id', $data['id_module']);
        }

        if(isset($data['user_id'])){
            $this->db->where('um.user_id', $data['user_id']);
        }

        $result = $this->db->get()->result();

        return $result;
    }

    public function addUserModuleData($data , $userId){
        if(isset($data['modules'])){
            $list = $this->getAllUserModule();
            foreach($list as $k=>$v){
                $isPresent = false;
                foreach($data['modules'] as $k1 => $v1){
                    if($v->id_module == $v1){
                        $isPresent = true;
                        break;
                    }
                }
                if(!$isPresent){
                    $this->db->insert('user_module', array('module_id' => $v->id_module, 'user_id' => $userId, 'allow_access'=> 0));
                }else{
                    $this->db->insert('user_module', array('module_id' => $v->id_module, 'user_id' => $userId, 'allow_access'=> 1));
                }
            }
            return 1;
        }
    }

    public function deleteUserModuleData($userId){
        $this->db->where('user_id', $userId);
        $this->db->delete('user_module');
    }

    public function getUserNavigation($data){
        if(isset($data)){
            $this->db->select('m.module_name name, m.module_url url, m.module_key key, m.icons, "" submenu');
            $this->db->from('user_module um');
            $this->db->join('module m', 'm.id_module=um.module_id', 'left');

            if(isset($data['user_id']))
                $this->db->where('user_id', $data['user_id']);

            $this->db->where('allow_access', 1);

            $list = $this->db->get()->result();
            return $list;
        }else{
            return [];
        }
    }

    public function getUsers()
    {
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->from('user_department ud','u.id_user=ud.user_id','left');
        $this->db->join('department d','ud.department_id=d.id_department','left');
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        if(isset($data['department_id']))
            $this->db->where('ud.department_id',$data['department_id']);
        if(isset($data['is_lead']))
            $this->db->where('is_lead',$data['is_lead']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function isUserLead($data)
    {
        $this->db->select('*');
        $this->db->from('user_department');
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        if(isset($data['department_id']))
            $this->db->where('department_id',$data['department_id']);
        if(isset($data['is_lead']))
            $this->db->where('is_lead',$data['is_lead']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
}