<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <!--    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important; height: 200px;">-->
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important;">
        <div class="contentHeader">
            <h3>Attendance Info</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Attendance Info</h3>
                </div>
                <div class="padding0">

                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">

                        <div id="priorities-form" class="form-horizontal NewErrorStyle animate-fld-bg">
                            <div>
                                <div class="panel panel-default">
                                    <script>
                                        <?php if($this->session->userdata('message')){ ?> TssLib.notify('<?=$this->session->userdata('message')?>', null, 5); <?php $this->session->unset_userdata('message'); } ?>
                                    </script>
                                    <h4 class="section-heading">Attendance Upload</h4>
                                    <form id="excelUploadForm" enctype="multipart/form-data" method="post"
                                          action="<?php echo site_url('/Attendance/excelAttendanceUpload') ?>">

                                        <div class="col-sm-12 clearfix">
                                            <div class="col-sm-6 pl0">
                                                <div class="fileupload fileupload-new input-group" data-provides="fileupload">
                                                    <div class="form-control" data-trigger="fileupload"><i class="glyphicon glyphicon-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span></div>
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                        <input type="file" id="file" name="file"></span>
                                                    <a href="#" class="input-group-addon btn btn-default fileupload-exists" data-dismiss="fileupload" id="removeFile">Remove</a>
                                                </div>
                                                <a class="link font-12 pl0" href="<?= WEB_BASE_URL ?>uploads/sample_attendance_sheet.xlsx"
                                                   download target="_blank">Download Sample Excel File</a>
                                            </div>
                                            <div class="col-sm-4 pl0">
                                                <button class="button-common" type="submit" id="uploadExcel"> Upload</button>
                                            </div>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="type" id="type" value="0">
                        <?php if(count($this->session->userdata('uploadError')) > 0){ ?>
                            <div class="col-sm-12 clearboth clearfix pb20">
                                <div class="panel-window border top15">
                                    <div class="grid-details-table-content clearfix padding0">
                                        <div class="tbl_wrapper border0">
                                            <table id="project-list" class="table-grid-view">
                                                <thead>
                                                <tr>
                                                    <th>Row No.</th>
                                                    <th>Message</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $error = $this->session->userdata('uploadError');
                                                foreach($error as $k=>$v){ ?>
                                                    <tr>
                                                        <td><?= $k?></td>
                                                        <td><?= $v['errorMsg']?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }else if($this->session->userdata('upload')){ ?>
                        <div>   <?php echo $this->session->userdata('upload') ?>
                            <?php } $this->session->unset_userdata('uploadError');$this->session->unset_userdata('upload'); ?>
                        </div>
                    </div>
                    <div class="mt10">
                        <!-- -->
                        <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                            <div class="col-sm-10 padding0">
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="sdate" value="<?=date('d/m/Y',strtotime("-1 days"))?>"
                                                       name="sdate" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>
                                        <label class="control-label col-sm-12">Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="edate" value="<?=date('d/m/Y',strtotime("-1 days"))?>"
                                                       name="edate" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>
                                        <label class="control-label col-sm-12">Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 mt15">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <select id="employeeDropdown" class="form-control rval" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Employee:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button onclick="validateReportsForm(0,0)" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 clearfix clearboth bg-white padding0">
                            <div class="grid-details-table">
                                <div class="grid-details-table-header">
                                    <h3>&nbsp;</h3>
                                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="grid-details-table-content clearfix padding0">
                                    <div class="tbl_wrapper border0">
                                        <table id="attendance-list" class="table-responsive"></table>
                                        <div id="attendance-list-page"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                    </div>

                </div>
            </div>
        </div>
        <div id="add-attendance-modal" class="modal-wrapper" style="display:none; width: 200px;">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Attendance</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <!--                                        <input type="text" class="form-control" name="name"  id="name" disabled="disabled"/>-->
                                            <label name="name" id="name"></label>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Task Type Name :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <label name="emp_code" id="emp_code"></label>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Employee Code :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control msk_time rval" name="start_time"  id="start_time"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Log In Time :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control msk_time rval" name="end_time"  id="end_time"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Log Out Time :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <select id="status" name="status" class="form-control rval">
                                                <option value="">Select</option>
                                                <option value="present">Present</option>
                                                <option value="absent">Absent</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Status :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <label>Punch Timings</label>
                                    <textarea name="punch_time" id="punch_time" class="form-control"></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="id_swipe_punch" value=""/>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <script>
            var $refTblgrid = $('#attendance-list');
            var logAttedList = 0;
            TssLib.docReady(function () {
                $(".msk_time").mask("Hh:Mm");
                postJsonAsyncWithBaseUrl("User/getAllUsers", {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            var employee = result.data;
                            TssLib.populateSelect($('#employeeDropdown'), {
                                success: true,
                                data: employee
                            }, 'email', 'id_user');
                            TssLib.selectAllOptions('#employeeDropdown');
                            setTimeout(function(){ validateReportsForm(0); },1000);
                        }
                    }
                });
                loadDatatables();
                $('#excelUploadForm').on('submit', function(e){
//                e.preventDefault();
                    var valid = true;
                    // validation code here
                    var files = $("#file").get(0).files;
                    var selectedFile = $("#file").val();
                    var extArray = ['xls', 'xlsx'];
                    var extension = selectedFile.split('.');
                    if (files.length <= 0) {
                        TssLib.notify('Please select a file', 'warn', 5);
                        valid = false;
                    } else if (files.length > 0 && $.inArray(extension[extension.length - 1], extArray) <= -1) {
                        TssLib.notify('PLease select on xls and xlsx files', 'warn', 5);
                        valid = false;
                    }

                    if(!valid) {
                        e.preventDefault();
                    }
                });
            });
            function loadDatatables(){
                if(logAttedList == 0){
                    //getAttendanceGrid
                    $refTblgrid.jqGrid({
                        url: TssConfig.TT_SERVICE_URL + 'Attendance/getAttendanceGrid',
                        multiselect: false,
                        datatype: "json",
                        sortorder: "desc",
                        extSearchField: '.searchInput',
                        colNames: ['id_attendance', 'id_swipe_punch', 'user_id' , 'Name', 'Emp Code', 'Date', 'Start Time', 'End Time', 'Total Duration', 'Status', 'punch_time'],
                        colModel: [
                            { name: 'id_attendance', index: 'id_attendance', hidden: true ,key:true},
                            { name: 'id_swipe_punch', index: 'id_swipe_punch', hidden: true ,key:false},
                            { name: 'user_id', index: 'user_id', hidden: true ,key:false},
                            { name: 'name', index: 'name', hidden: false ,key:false},
                            { name: 'emp_code', index: 'emp_code', hidden: false ,key:false},
                            { name: 'attend_date', index: 'attend_date', hidden: false ,key:false},
                            { name: 'start_time', index: 'start_time', hidden: false },
                            { name: 'end_time', index: 'end_time', hidden: false },
                            { name: 'total_duration', index: 'total_duration', hidden: false, sortable:false },
                            { name: 'status', index: 'status', hidden: false },
                            { name: 'punch_time', index: 'punch_time', hidden: true },
                        ],
                        pager: 'attendance-list-page'
                    }).navGrid('#attendance-list-page', {
                        edit: true, add: false, del: false, search: false, refresh: true,
                        addfunc: function () {
                            addEditAttendance();
                        }, editfunc: function (id) {
                            var rowData = $refTblgrid.jqGrid('getRowData', id);
                            addEditAttendance(rowData);
                        }, delfunc: function (id) {
                            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                                $(this).closest('.modal').modal('hide');
                                postJsonAsyncWithBaseUrl("Task_types/deleteTaskTypesById", {'task_type_id': id}, {
                                    jsonContent: true,
                                    callback: function (result) {
                                        if (result.data != null) {
                                            if (result.data.status) {
                                                TssLib.notify('Deleted successfully', null, 5);
                                                $('#task-type-list').trigger('reloadGrid');
                                            } else {
                                                TssLib.notify(result.data.message, 'warn', 5);
                                            }
                                        }
                                    }
                                });
                            }, 'Yes', 'No');
                        }
                    });
                    logAttedList++;
                }else{
                    $refTblgrid.trigger('reloadGrid');
                }
            }
            function addEditAttendance(rowData) {
                var $modal = TssLib.openModel({ width: '600' }, $('#add-attendance-modal').html());
                if (TssLib.isBlank(rowData)) {
                    $modal.find('.panel-title').text('Add Attendance');
                    $modal.find('[div-submit="true"]').text('Save');
                } else {
                    TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                    $modal.find('.panel-title').text('Edit Attendance');
                    $modal.find('[div-submit="true"]').text('Update');
                }
                TssLib.ajaxForm({
                    jsonContent: true,
                    form: $('#linen-createSchedule-form'), callback: function (result) {
                        if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                        if (result.data.status) {
                            $refTblgrid.trigger('reloadGrid');
                            TssLib.closeModal();
                            TssLib.notify('Task Type '+addedTest+' successfully ', null, 5);
                        }else{
                            TssLib.notify(result.data.message, 'warn', 5);
                        }
                    },
                    before: function (formObj) {
                        var status = $('#status').val();
                        $(formObj).data('additionalData', { id_attendance: rowData.id_attendance, user_id: rowData.user_id, status: status  });
                        $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Attendance/addAttendance');
                        return true;
                    }
                });
            }
            function validateReportsForm(type) {
                $('#sdate').removeClass('err-bg');
                $('#edate').removeClass('err-bg');
                $('#employeeDropdown').removeClass('err-bg');
                $('.error-msg').remove();

                var flag = 0;

                var user_id = $('#employeeDropdown').val();
                var sdate = $('#sdate').val();
                var edate = $('#edate').val();

                if (user_id == null) {
                    $('#employeeDropdown').addClass('err-bg');
                    $('#employeeDropdown').parent().before('<span class="error-msg">Required</span>');
                    flag++;
                }

                if (flag == 0) {
                    if (type == 0) {
                        var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
                        myPostData['user_id'] = user_id;
                        myPostData['sdate'] = sdate;
                        myPostData['edate'] = edate;
                        $refTblgrid.setGridParam({'postData': myPostData});
                        $refTblgrid.trigger("reloadGrid");

                        return false;
                    }
                    else {
                        $('#reports_frm').submit();
                    }
                }
            }
        </script>