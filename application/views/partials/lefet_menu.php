<!-- leftpanel -->
<div class="leftpanel" id="leftNav">
    <div class="leftpanelinner">
        <div class="nav-slider" id="nav-slider">
            <div class="slider-content" id="scollBox">
                <ul class="nav nav-pills nav-stacked nav-bracket" id="left-panel-main-menus-ul">
                </ul>
            </div>
        </div>
    </div><!-- leftpanelinner -->
</div>
<!-- leftpanel -->