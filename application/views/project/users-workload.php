<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Users Workload</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Workload
                    </h3>
                </div>
                <div class="padding0">
                    <!--<div class="contentHeader">
                        <h4 class="margin0">Filters :<a class="ml5" href="javascript:;">Clear All</a></h4>
                    </div>-->
                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <div class="col-sm-4">
                            <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select class="form-control" id="department_id" >
                                        <option value="0">ALL</option>
                                        <?php for($s=0;$s<count($department_list);$s++){ ?>
                                            <option value="<?=$department_list[$s]['id_department']?>"><?=$department_list[$s]['department_name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Department:</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group padding0">
                                <div class="multi-select-container">
                                <select id="employeeDropdown"  class="form-control" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                </select>
                            </div>
                            <label class="control-label col-sm-12">Employee:</label>
                             </div>
                        </div>
                        <div class="col-sm-4">
                            <button onclick="userWorkLoadFilter(this)" class="button button-common module mt20" div-submit="true" id=""> Search</button>
                        </div>
                    </div>
                    <div id="workLoadReport"></div>
                    <?php /*if($this->session->userdata('user_type_id')==2){ */?>
                    <div id="add-user-modal" class="modal-wrapper" style="display:none">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                                <h4 class="panel-title">Task List</h4>
                            </div>
                            <div class="panel-body">
                                    <div class="col-sm-12 clearfix clearboth padding0" id="status_wise_task_list"  >
                                        <div class="grid-details-table-content clearfix padding0">
                                            <div class="tbl_wrapper border0">
                                                <table id="project-list" class="table-responsive"></table>
                                                <div id="project-list-page"></div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <?php /*} */?>
                </div>
            </div>

        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#department_id').on('change',function(){
            if ( !$( "#employeeDropdown" ).length ) { return false; }
            var department_id = $(this).val();
            userDropDown(department_id);
        });
        function userDropDown(department_id){
            var url = "user/getAllUsers"
            if(department_id != 0){ url = "Project/getDepartmentUsers/"+department_id }
            getJsonAsyncWithBaseUrl(url, {}, {
                jsonContent: true,
                callback: function (result) {
                    var drpDown = result.data; if(department_id != 0){ drpDown = result.data.data; }
                    TssLib.populateSelect($('#employeeDropdown'), {
                        success: true,
                        data: drpDown
                    }, 'email', 'id_user');
                    TssLib.selectAllOptions('#employeeDropdown');
                    userWorkLoadFilter();
                }
            });
        }
        userDropDown(0);

        //for admin displaying grid for new,in-progress,completed task list


    });
    function userWorkLoadFilter(This) {
        var user = $('#employeeDropdown').val();
        if (user != '') {
            $.post("<?=site_url('Project/loadUserWork')?>", {user: user.toString()}, function (data) {
                $('#workLoadReport').html(data);
            });
        }else{
            TssLib.notify('User not selected', 'warn', 5);
        }
    }

    function getUserTasks(status,count,userId)
    {
        if(count==0){return false;}

        var $modal = TssLib.openModel({ width: '1200' }, $('#add-user-modal').html());
        var $refTblgrid = $modal.find('#project-list');
        $refTblgrid.jqGrid({
            url: TssConfig.TT_SERVICE_URL + 'Project/userTasksByStatus',
            multiselect: false,
            datatype: "json",
            sortorder: "desc",
            postData:{
                'user_id' : userId,
                'status' : status
            },
            extSearchField: '.searchInput',
            colNames: ['Project Name','Task Name','Time','Status'],
            colModel: [
                { name: 'project_name', index: 'project_name', hidden: false ,key:false},
                { name: 'task_name', index: 'task_name', hidden: false ,key:true},
                { name: 'time', index: 'time', hidden: false ,key:true},
                { name: 'task_status', index: 'task_status', hidden: false ,key:true}
            ],
            pager: 'project-list-page'
        }).navGrid('#project-list-page', {
            edit: false, add: false, del: false, search: false, refresh: true
        });
        //console.log($refTblgrid);
    }
</script>
