<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Request More Time</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Request More Time</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="task-type-list" class="table-responsive"></table>
                        <div id="task_type-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" class="form-control" name="id_work_request_data" value=""/>
        <div id="add-client-modal" class="modal-wrapper" style="display:none; width: 200px;">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Request more time</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input disabled="disabled" type="text" class="form-control" name="duration" startdate="0" id="duration"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Additional time:</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix">
                                    <div class="form-group bt-default">
                                        <label>User comments</label>
                                        <textarea disabled="disabled" name="user_comments" id="user_comments" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix">
                                    <div class="form-group bt-default">
                                        <label><span class="req-str">*</span>Admin Comment</label>
                                        <textarea name="admin_comments" id="admin_comments" class="form-control rval"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix" id="action_div">
                                    <button class="button-common" onclick="changeStatus(this,1)">Approve</button>
                                    <button class="button-common" onclick="changeStatus(this,0)">Reject</button>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="id_request_more_time" id="id_request_more_time" value=""/>
                            <input type="hidden" class="form-control" name="user_id" id="user_id" value=""/>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#task-type-list');
        TssLib.docReady(function () {

            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Project/getRequestMoreTime',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                shrinktofit:true,
                autowidth:true,
                extSearchField: '.searchInput',
                colNames: ['Id_request_more_time','user_comments','admin_comments','Project Name', 'Requirement' , 'Task Name','User Name',/*'Assigned To',*/ 'Allotted Time' , 'Additional Time' , 'Requested on','Status', 'Action'],
                colModel: [
                    { name: 'Id_request_more_time', index: 'Id_request_more_time', hidden: true ,key:true},
                    { name: 'user_comments', index: 'user_comments', hidden: true ,key:true},
                    { name: 'admin_comments', index: 'admin_comments', hidden: true ,key:true},
                    { name: 'project_name', index: 'project_name', width: 20},
                    { name: 'requirement_name', index: 'requirement_name', width: 20 },
                    { name: 'task_name', index: 'task_name', width: 20 },
                    { name: 'user_name', index: 'user_name', width: 15 },
                    /*{ name: 'assigned_to', index: 'assigned_to', width: 15 },*/
                    { name: 'allotted_time', index: 'allotted_time', width: 15 },
                    { name: 'duration', index: 'duration', width: 15},
                    { name: 'requested_on', index: 'requested_on', width: 15 },
                    { name: 'status', index: 'status', width: 10 },
                    { name: 'Action', index: 'action', width: 10, sortable: false, formatter: function (c, o, d) {
                        return '<a href="javascript:;" onclick="openWorkRequestModal('+ d.Id_request_more_time +')">Edit</a>';
                    }}
                ],
                loadComplete: function() {
                    setTimeout(function(){
                        resizeGrids();
                    }, 1000);
                },
                pager: 'task_type-list-page'
            }).navGrid('#task_type-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true
            });
            $refTblgrid.trigger('reloadGrid');
        });

        function changeStatus(This, status){
            var id = $('#id_request_more_time').val();
            var admin_comments = $('#admin_comments').val();
            var user_id = $('#user_id').val();
            var status = status==1?'approved':'declined';

            if(admin_comments != ''){
                postJsonAsyncWithBaseUrl("Project/requestMoreTimeAction", {'id_request_more_time': id, user_id: user_id, status: status, admin_comments: admin_comments, approved_by: <?= $this->session->userdata('user_id') ?>}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            if(result.data.status){
                                TssLib.notify('Updated successfully', null, 5);
                                $refTblgrid.trigger('reloadGrid');
                                TssLib.closeModal();
                            }else{
                                TssLib.notify(result.data.message, 'warn', 5);
                            }
                        }
                    }});
            }else{
                TssLib.notify('Admin comment is mandatory', 'warn', 5);
            }
        }
        function openWorkRequestModal(id) {
            console.log(id);
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            $('#id_request_more_time').val(id);
            var rowData = $refTblgrid.jqGrid('getRowData', id);
            if(rowData.status=='pending'){
                $('#action_div').css('display','block');
            }
            else{
                $('#action_div').css('display','none');
            }
            TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
            $modal.find('.panel-title').text('Request more time');
            $modal.find('[div-submit="true"]').text('Update');

        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->