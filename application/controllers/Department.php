<?php
/**
 * Created by PhpStorm.
 * User: rameshpaul
 * Date: 16/5/16
 * Time: 12:03 PM
 */
class Department extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Department_model');
    }
    public function index(){
        $data['content'] = 'department/index';
        $this->load->view('partials/landing',$data);
    }
    public function getDepartmentsGrid(){
        $params = $this->input->get();
        $projectList = $this->Department_model->getDepartmentsGrid($params);
        //$projectList = $this->Project_modal->getProjects();
        echo json_encode($projectList);
    }
    public function addDepartment()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $id = $data['id_department'];

        $result = $this->Department_model->checkDepartmentName($id, $data['department_name']);
        if (empty($result)) {
            if($id == null && $id == '') {
                $dep = $this->Department_model->addDepartment($data);
            }else{
                $dep = $this->Department_model->updateDepartment($id,$data);
            }
            echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$dep));
        } else {
            echo json_encode(array('status' => false, 'message' => 'Department name exist'));
        }
    }
    public function getDepartments()
    {
        $dep = $this->Department_model->getDepartments();
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$dep));
    }

    public function deleteDepartmentById()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $error = false;
        if(!$this->Department_model->checkProjectAssigned($data['department_id'])){
            $error = true;
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Department already assigned to a project', 'data'=>[]));
        }
        if(!$this->Department_model->checkUserAssigned($data['department_id'])){
            $error = true;
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'User already assigned to department', 'data'=>[]));
        }
        if(!$this->Department_model->checkTaskTypeAssigned($data['department_id'])){
            $error = true;
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Task type already assigned to department', 'data'=>[]));
        }
        if(!$this->Department_model->checkChecklistAssigned($data['department_id'])){
            $error = true;
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Checklist already assigned to department', 'data'=>[]));
        }

        if(!$error){
            $status = $this->Department_model->deleteDepartmentById($data['department_id']);
            if($status){
                echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'Department deleted'));
            }else{
                echo json_encode(array('status'=>false,'error'=>true,'message'=>''));
            }
        }


    }

}

