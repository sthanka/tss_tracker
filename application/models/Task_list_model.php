<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Task_list_model extends CI_Model
{
    public $key = '#@Tss_Tracker$#';


    public function getTaskTypesGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'id_task_type';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;

        $taskType = isset($paramArr['taskType'])?$paramArr['taskType']:'task';

        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam);  $whereParam = " and (tt.name LIKE '%".$whereParam."%' || if(`tt`.`product_type`='billable','Yes','No') LIKE '%".$whereParam."%' || if(`tt`.`is_billable`='1','Yes','No') LIKE '%".$whereParam."%') "; }

        $whereClause = "where true AND tt.task_type_key='".$taskType."' ".$whereParam;

        $SQL = "SELECT tt.*, d.*, txd.* ,GROUP_CONCAT(txd.department_id) dept, tt.product_type,tt.product_type product_type_label, tt.is_billable, tt.is_billable is_billable_label from `task_type` tt
                left join task_type_xref_department txd ON tt.id_task_type=txd.task_type_id
                left join department d ON txd.department_id=d.id_department
                $whereClause
            group by tt.id_task_type
          order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT * from `task_type` tt
                left join task_type_xref_department txd ON tt.id_task_type=txd.task_type_id
                left join department d ON txd.department_id=d.id_department
          $whereClause
          group by tt.id_task_type";
        /*$queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];*/
        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function addTaskTypes($data){
        $data['task_type_key'] = 'task';

        $department = $data['department_id'];
        $data['is_billable'] = $data['is_billable']==0 || $data['is_billable']=="0"?0:1;
        unset($data['department_id']);
        $this->db->insert('task_type', $data);
        $lastInsertId = $this->db->insert_id();
        if (count($department)>1) {
            foreach($department as $k=>$v){
                $this->db->insert('task_type_xref_department', array('task_type_id'=>$lastInsertId, 'department_id' => $v));
            }
        }else{
            $this->db->insert('task_type_xref_department', array('task_type_id'=>$lastInsertId, 'department_id' => $department));
            return true;
        }
    }

    public function addTaskTypesOthers($data){
        if(isset($data['taskType']) && $data['taskType'] != ''){
            $data['product_type'] = 'other';
            $data['task_type_key'] = $data['taskType'];
            unset($data['taskType']);
            $this->db->insert('task_type', $data);
            return $this->db->insert_id();
        }
    }

    public function updateTaskTypes($id,$data){
        $data['task_type_key'] = 'task';

        $department = $data['department_id'];
        $data['is_billable'] = $data['is_billable']==0 || $data['is_billable']=="0"?0:1;
        unset($data['department_id']);
        $this->db->where('id_task_type', $id);
        $this->db->update('task_type', $data);

        $this->db->where('task_type_id', $id);
        $this->db->delete('task_type_xref_department');

        if (count($department)>1) {
            foreach($department as $k=>$v){
                $this->db->insert('task_type_xref_department', array('task_type_id'=>$id, 'department_id' => $v));
            }
        }else{
            $this->db->insert('task_type_xref_department', array('task_type_id'=>$id, 'department_id' => $department));
            return true;
        }
    }

    public function updateTaskTypesOthers($id,$data){
        if(isset($data['taskType']) && $data['taskType'] != ''){
            $data['product_type'] = 'other';
            unset($data['taskType']);
            $this->db->where('id_task_type', $id);
            $this->db->update('task_type', $data);
            return true;
        }
    }

    public function deleteTaskTypesById($taskTypeId){
        try{
            $this->db->where('task_type_id', $taskTypeId);
            $this->db->delete('task_type_xref_department');

            $this->db->where('id_task_type', $taskTypeId);
            $this->db->delete('task_type');
            return 1;
        }catch(Exception $ex){
//            print_r($ex);die('asd');
        }
    }

    public function checkTaskTypeAlreadyPresent($data){
        if(isset($data['taskType']) && $data['taskType'] != ''){
            $data['product_type'] = 'other';
            $this->db->select('id_task_type');
            $this->db->from('task_type');
            $this->db->where('task_type_key',$data['taskType']);
            $this->db->where('name',$data['name']);
            $result = $this->db->get()->result();
            if(count($result)>0){
                return true;
            }else{
                return false;
            }
        }
    }

    public function checkTaskTypesAssigned($taskTypeId){
        $this->db->select('log_time_id');
        $this->db->from('log_time');
        $this->db->where('task_type',$taskTypeId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }
}