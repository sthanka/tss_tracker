<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->

        <div class="contentHeader">
            <!--<h3>Nagaraju</h3>-->
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Task Overview
                    </h3>
                    <h3 class="pull-right">
                        <a class="pull-right border-left" href="<?=WEB_BASE_URL?>index.php/Project/overview/<?=$task_details[0]['id_project']?>">
                            <i class="fa fa-reply"></i>
                        </a>
                    </h3>
                </div>
                <div class="grid-details-table-content padding0">
                    <div class="col-sm-12 clearfix clearboth border-btm pt10">
                        <div class="col-sm-2">
                            <p class="blue-text">Task Info</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p>
                                        <a href="<?= WEB_BASE_URL ?>index.php/project/overview/<?= $task_details[0]['id_project'] ?>"><?= $task_details[0]['project_name'] ?></a>
                                    </p>
                                    <label><?= $task_details[0]['task_name'] ?></label>
                                </div>
                            </div>

                            <?php /*if(($this->session->userdata('user_type_id')==2) || ($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==1)){ */
                            $total_tasks = $this->Project_modal->getSubTaskByParent($task_id);
                            $completed_task_dept = $this->Project_modal->getCompletedSubtaskDept($task_id,'completed',$this->session->userdata('department_id'));
                            $pend_task_dept = $this->Project_modal->getCompletedSubtaskDept($task_id,'completed',$this->session->userdata('department_id'));

                            ?>
                            <div class="view_input col-sm-2">

                                <?php if($this->session->userdata('user_type_id')==2 || ($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==1)) if(count($total_tasks)==count($pend_task_dept)){
                                /*if(empty($pend_task_dept)){*/
                                ?>
                                    <p><span class="font-white orange-bg x-font" name="status">

                                        Forward

                                    </span></p>
                                <label class="margin0">Status <a class="cursor-pointer  font-12" onclick="changeStatus();" data-original-title="change status"><i class="fa fa-random"></i></a></label>
                                <?php } /*}*/ ?>
                            </div>
                            <?php /*} */ ?>
                            <!--<div class="view_input col-sm-2">
                                <p><span class="font-white red-bg x-font" name="Priority">High</span></p>
                                <label class="margin0">Priority <a class="cursor-pointer  font-12" onclick="changePriority();" data-original-title="change priority"><i class="fa fa-random"></i></a></label>
                            </div>-->
                        </div>
                    </div>
                    <!--<div class="col-sm-12 clearfix clearboth border-btm top10">
                        <div class="col-sm-2">
                            <p class="blue-text">Description</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-12 clearfix clearboth">
                                <div class="view_input">
                                    <p><?/*=$task_details[0]['description']*/?></p>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-sm-12 clearfix clearboth top10">
                        <div class="col-sm-2">
                            <p class="blue-text">Task Status</p>
                        </div>
                        <div class="col-sm-10">

                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><?=date('d-m-Y',strtotime($task_details[0]['start_date']))?></p>
                                    <label>Start Date</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><?=date('d-m-Y',strtotime($task_details[0]['end_date']))?></p>
                                    <label>End Date</label>
                                </div>
                            </div>
                            <!--<div class="col-sm-2">
                                <div class="view_input">
                                    <p>Shiva NR</p>
                                    <label>Assigned By</label>
                                </div>
                            </div>-->
                            <!--<div class="col-sm-2">
                                <div class="view_input">
                                    <p>2/5</p>
                                    <label>Sub Tasks(Done/Total)</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><?/*=$task_details[0]['estimated_time']*/?></p>
                                    <label>Est. Time(Hrs)</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p>10</p>
                                    <label>Act. Time(Hrs)</label>
                                </div>
                            </div>-->
                        </div>
                    </div>


                    <div class="col-lg-12 padding0">
                        <ul class="nav nav-tabs nav-tabs-top custom-nav-tabs" role="tablist" id="project-tab">
                            <li class="active">
                                <a href="#timeline" data-toggle="tab">
                                    <p class="tab-list">Timeline</p>
                                </a>
                            </li>
                            <li>
                                <a href="#tasks" data-toggle="tab">
                                    <p class="tab-list">Tasks</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#use-case" data-toggle="tab" >
                                    <p class="tab-list">Use Case</p>
                                </a>
                            </li>
                            <!--<li class="">
                                <a href="#test-result" data-toggle="tab">
                                    <p class="tab-list">Test Result</p>
                                </a>
                            </li>-->
                            <li class="">
                                <a href="#attachments" data-toggle="tab">
                                    <p class="tab-list">Attachments</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#touch-points" data-toggle="tab">
                                    <p class="tab-list">Workflow</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#time-log" data-toggle="tab" onclick="reloadGried('time-log-list') ">
                                    <p class="tab-list">Time Log</p>
                                </a>
                            </li>
                            <!--<li class="">
                                <a href="#checklist" data-toggle="tab">
                                    <p class="tab-list">Checklist</p>
                                </a>
                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content cv-tabs-container">
                <div id="tasks" class="tab-pane padding0 pb0 ">
                    <div class="tbl_wrapper border0">
                        <table id="tasks-list" class="table-responsive"></table>
                        <div id="tasks-list-page"></div>
                    </div>

                </div>
                <div id="use-case" class="tab-pane padding0 pb0 ">
                    <?php if(!empty($sub_task)){  ?>
                    <div class="col-sm-12 clearfix clearboth">
                        <div class="col-xs-4 col-lg-3 pad-left-none">
                            <ul id="" class="nav tabs-left">
                                <?php for($s=0;$s<count($sub_task);$s++){  ?>
                                    <li class="<?php if($s==0){ echo "active"; } ?>" >
                                        <a href="#task_<?=$sub_task[$s]['id_project_task']?>" data-toggle="tab">
                                            <b><?=$sub_task[$s]['task_name']?></b>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-xs-8 col-lg-9 padding0">
                            <!-- Tab panes -->
                            <div class="tab-content mt0">
                                <?php for($s=0;$s<count($sub_task);$s++){  ?>
                                    <div class="tab-pane clearfix padding10 <?php if($s==0){ echo 'active'; } ?>" id="task_<?=$sub_task[$s]['id_project_task']?>">
                                        <div class="add-btn">
                                            <button class="button-common ">Add Use Case</button>
                                        </div>
                                        <div class="editor-block">
                                            <textarea name="use_case" id="user_case_text_<?=$sub_task[$s]['id_project_task']?>" data-texval='' ></textarea>
                                            <div class="mt10">
                                                <input type="button" id="use_case_btn_<?=$sub_task[$s]['id_project_task']?>" class="button-common mr5" value="Save" onclick="updateUseCase('','<?=$sub_task[$s]['id_project_task']?>','');">
                                                <input type="button" class="button-common hide-btn" value="Cancel">
                                            </div>
                                        </div>

                                        <ul class="u-list" id="u-list_<?=$sub_task[$s]['id_project_task']?>">

                                        <?php  for($r=0;$r<count($use_cases);$r++){
                                                if($use_cases[$r]['id_project_task']==$sub_task[$s]['id_project_task']){
                                            ?>
                                                <li>
                                                    <h5><?=$use_cases[$r]['user_name']?></h5>
                                                    <p><?=$use_cases[$r]['task_usecase']?></p>
                                                </li>
                                        <?php } } ?>


                                        </ul>
                                    </div>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div id="test-result" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <textarea class="form-control border col-sm-12" rows="6" placeholder="Enter Test Results"></textarea>
                    </div>
                </div>
                <div id="attachments" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <div class="col-xs-4 col-lg-3 pad-left-none">
                            <ul id="" class="nav tabs-left">
                                <?php
                                //echo '<pre>'; print_r($attachment); exit;
                                $attachment_task_id = array_values(array_unique(array_map(function($e){ return $e['id_project_task']; },$attachment)));
                                $attachment_task_name = $attachment_task_user = array();
                                for($r=0;$r<count($attachment_task_id);$r++)
                                {
                                    for($s=0;$s<count($attachment);$s++){ if($attachment_task_id[$r]==$attachment[$s]['id_project_task']){
                                        $attachment_task_name[$attachment_task_id[$r]] = $attachment[$s]['task_name'];
                                        $attachment_task_user[$attachment_task_id[$r]] = $attachment[$s]['user_id'];
                                    } }
                                }

                                for($s=0;$s<count($attachment_task_id);$s++){ ?>
                                    <li class="<?php if($s==0){ echo "active"; } ?>" >
                                        <a href="#task_attachment_<?=$attachment_task_id[$s]?>" data-toggle="tab">
                                            <b><?=$attachment_task_name[$attachment_task_id[$s]]?></b>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-xs-8 col-lg-9 padding0">
                            <!-- Tab panes -->
                            <div class="tab-content mt0">
                                <?php for ($s = 0; $s < count($attachment_task_id); $s++) { ?>
                                    <div class="tab-pane padding10 <?php if ($s == 0) {
                                        echo "active";
                                    } ?>" id="task_attachment_<?= $attachment_task_id[$s] ?>">
                                        <form
                                            class="NewErrorStyle form-horizontal NewErrorStyle animate-fld-bg align-error clearfix"
                                            method="post" id="task_attachment_frm_<?= $attachment_task_id[$s] ?>"
                                            action="<?= WEB_BASE_URL ?>index.php/Project/uploadTaskAttachment">


                                            <div class="col-sm-6 pl0">
                                                <div class="fileupload fileupload-new input-group"
                                                     data-provides="fileupload">
                                                    <div class="form-control" data-trigger="fileupload"><i
                                                            class="glyphicon glyphicon-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span
                                            class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                         <input type="file" name="task_attachment" id="task_attachment"
                                                class="form-control rval">
                                    </span>
                                                    <a href="#"
                                                       class="input-group-addon btn btn-default fileupload-exists"
                                                       data-dismiss="fileupload" id="remove_attachment_<?= $attachment_task_id[$s] ?>">Remove</a>
                                                </div>

                                            </div>
                                            <div class="col-sm-4">
                                                <input type="submit" class="button-common" name="btn" value="Upload">
                                            </div>

                                            <input type="hidden" name="project_task_id"
                                                   value="<?= $attachment_task_id[$s] ?>">
                                            <input type="hidden" name="user_id"
                                                   value="<?= $attachment_task_user[$attachment_task_id[$s]] ?>">
                                        </form>

                                            <table  class="table table-striped table-sub-head mt10">
                                                <thead>
                                                <?php $count = 0; for ($r = 0; $r < count($attachment); $r++) {
                                                if ($attachment_task_id[$s] == $attachment[$r]['id_project_task'] && $attachment[$r]['attachment'] != '' && $count==0) { $count++;  ?>
                                                <tr>
                                                    <th>Uploaded by</th>
                                                    <th>Download Link</th>
                                                </tr>
                                                <?php }
                                                } ?>
                                                </thead>
                                                <tbody>
                                                <?php for ($r = 0; $r < count($attachment); $r++) {

                                                    if ($attachment_task_id[$s] == $attachment[$r]['id_project_task'] && $attachment[$r]['attachment'] != '') { ?>
                                                        <tr>
                                                            <td><?=$attachment[$r]['user_name']?></td>
                                                            <td><a target="_blank"
                                                                   href="<?= WEB_BASE_URL ?>uploads/<?= $attachment[$r]['attachment'] ?>"><?= $attachment[$r]['attachment'] ?></a>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div id="touch-points" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <table class="table table-bordered">
                            <thead>
                            <tr><th>Task Name</th><th>Received on</th><th>Dept.Name</th><th>Assigned to</th><th>Status</th></tr>
                            </thead>
                            <tbody>
                            <?php /*for($s=0;$s<count($touch_points);$s++){ */?>
                                <tr>
                                    <td><?/*=$touch_points[$s]['task_name']*/?></td>
                                    <td><?/*=date('d-m-Y',strtotime($touch_points[$s]['created_date_time']))*/?></td>
                                    <td><?/*=$touch_points[$s]['department_name']*/?></td>
                                    <td><?/*=$touch_points[$s]['user_name']*/?></td>
                                    <td><?/*=$touch_points[$s]['status']*/?></td>
                                </tr>
                            <?php /*} */?>
                            </tbody>
                        </table>
                    </div>
                </div>-->
                <div id="time-log" class="tab-pane padding0 pb0 ">
                    <div class="tbl_wrapper border0">
                        <table id="time-log-list" class="table-responsive"></table>
                        <div id="time-log-page"></div>
                    </div>
                </div>
                <div id="touch-points" class="tab-pane padding0 pb0 ">
                    <div class="tbl_wrapper border0">
                        <table id="touch-points-list" class="table-responsive"></table>
                        <div id="touch-points-list-page"></div>
                    </div>
                </div>
                <div id="checklist" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <textarea class="form-control border col-sm-12" rows="6" placeholder="Enter Test Results"></textarea>
                    </div>
                </div>
                <div id="timeline" class="tab-pane padding0 pb0 active">

                        <div class="col-sm-12 clearfix clearboth padding10 bg-white">
                            <div class="tp-container padding10">
                                <?php if(!empty($time_line)){ ?>
                                <div class="tp-row">

                                    <?php for($s=0;$s<count($time_line);$s++){ ?>
                                        <div class="tp-content">
                                            <div class="tp-content-wrap">
                                                <p>
                                                    <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span><?=date('h:i a',strtotime($time_line[$s]['created_date_time']))?> By <a href="javascript:;"><?=$time_line[$s]['user_name']?></a><input type="hidden" name="TouchPointType" value="Lead Stage Move"><input type="hidden" name="Id" value="6278">
                                                </p>
                                                <p id="repText">
                                                    <?=$time_line[$s]['description']?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="tp-date" data-value="<?=date('dMY',strtotime($time_line[$s]['created_date_time']))?>">
                                            <?=date('d M',strtotime($time_line[$s]['created_date_time']))?>
                                            <span><?=date('Y',strtotime($time_line[$s]['created_date_time']))?></span>
                                        </div>
                                    <?php } ?>

                                    <!--<div class="tp-content">
                                        <div class="tp-content-wrap tp-admin">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM By <a href="javascript:;">Lokesh</a><input type="hidden" name="TouchPointType" value="Lead Stage Move"><input type="hidden" name="Id" value="6278">
                                            </p>
                                            <p id="repText">
                                                Status Updated To: Ramesh ,By: Ravi
                                            </p>
                                        </div>
                                        <div class="tp-content-wrap">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM  Comments Added By <a href="javascript:;">Lokesh</a><input type="hidden" name="TouchPointType" value="Lead Stage Move"><input type="hidden" name="Id" value="6278">
                                            </p>
                                            <p id="repText">
                                                Test1 Comments......
                                            </p>
                                            <p id="repText">
                                                Test2 Comments......
                                            </p>
                                            <p id="repText">
                                                Test3 Comments......
                                            </p>
                                        </div>
                                        <div class="tp-content-wrap tp-admin">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM Assigned To:
                                            </p>
                                            <p id="repText">
                                                Ravi
                                            </p>
                                        </div>
                                        <div class="tp-content-wrap tp-admin">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM Created By:
                                            </p>
                                            <p id="repText">
                                                Ramesh
                                            </p>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="tp-row start-row"><div data-value="31Mar2015" class="tp-date">Start</div><div class="tp-content">&nbsp;</div></div>
                                <?php } ?>
                            </div>
                            </div>
                        </div>

                </div>
            </div>

            </div>
        </div>

        <!--MODAL POPS-->
        <div id="status-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST"  id="status-form">
                <div>
                    <form method="post" action="<?=WEB_BASE_URL?>index.php/Project/updateMainTaskStatus" id="frm" name="frm" class="NewErrorStyle">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Change Status</h4>
                        </div>
                        <input type="hidden" id="Id" name="Id" value="0" />
                        <div class="panel-body align-error">

                            <!--<div class="col-sm-12 padding0 clearfix clearboth">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval"  name="status" id="status">
                                                <option value="">--Select--</option>
                                                <option value="open">Open</option>
                                                <option value="inprogress">In Progress</option>
                                                <option value="completed">Done</option>
                                                <option value="reopen">Reopen</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Status :</label>
                                    </div>
                                </div>
                            </div>-->

                            <div class="col-sm-12 padding0 tbleWdth clearboth clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <textarea class="form-control rval" name="comments"></textarea>
                                        </div>

                                        <label class="control-label">Comments :</label>
                                    </div>
                                    <input type="hidden" name="status" id="status" value="completed">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common panelbtn" type="submit" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                        <input type="hidden" name="parent_id" id="parent_id" value="<?=$task_id?>">
                    </form>
                </div>
            </div>
        </div>
        <div id="priorities-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Change priority</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 padding0 clearfix clearboth">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval" name="Priority" id="Priority">
                                                <option value="">--Select--</option>
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Priority :</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common panelbtn" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<link rel="stylesheet" type="text/css" href="../Content/CSS/bootstrap-fileupload.min.css">
        <script type="text/javascript" src="../Scripts/bootstrap-fileupload.min.js"></script>-->
        <script>
            function reloadGried(elmId){
                $('#'+elmId).trigger('reloadGrid');
            }

            function changeStatus() {
                var $modalPopup = TssLib.openModel({ width: 550 }, $('#status-modal').html());

                TssLib.ajaxForm({
                    jsonContent: true,
                    doServiceCall: true,
                    form: $modalPopup.find('#frm'), callback: function (data) {
                        var res = data.data;
                        if (res.status) {
                            TssLib.notify(res.data);
                            TssLib.closeModal();
                        }
                        else{
                            TssLib.notify(res.data,'warn',5);
                        }

                    }
                });
            }
            function changePriority(id) {
                var $modalPopup = TssLib.openModel({ width: 550 }, $('#priorities-modal').html());

                TssLib.ajaxForm({
                    jsonContent: true,
                    doServiceCall: true,
                    form: $('#priorities-form'), callback: function (data) {
                        if (data.success) {
                            if (isForEdit) {
                                TssLib.notify('Priorities Updated Successfully');
                            }
                            TssLib.closeModal();
                            $taskmanager_1.trigger('reloadGrid');
                        }
                    }, before: function (formObj) {
                        $(formObj).attr('action-send', TssConfig.BASE_URL + 'Service.svc');
                        return true;
                    }
                });
            }
            function removeMember(e){
                $(e).closest('li').remove();
            }
            function addMember(e){
                var _memberName = $(e).prev().val();
                if(_memberName.length > 0){
                    $('#teamMembers').append('<li><label>'+_memberName+'</label><a onclick="removeMember(this);">X</a></li>');
                    $(e).prev().val('').focus();
                } else{
                    TssLib.notify('Please Enter Team Member Name','warn',5);
                }
            }


            //task tab function
            TssLib.docReady(function () {
                $('#use-case').find('textarea').each(function( index ) {
                    $("#"+$( this).attr('id')).Editor({'bold': true, 'italics': true, 'underline':true, 'ol':true,'unlink':true});
                    //console.log($( this).attr('data-texval'));
                    $("#"+$( this).attr('id')).Editor("setText",$( this).attr('data-texval'));
                });
                $('a[tab-context="timeline"]').trigger('click');

                var $refTblgrid = $('#tasks-list');
                $refTblgrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/taskList/',
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    postData: { parent_id: <?=$task_id?> },
                    extSearchField: '.searchInput',
                    colNames: ['Id', 'Sub Task','Assigned To','Start Date', 'End Date','Estimated','Actual time','Status','Action'],
                    colModel: [
                        { name: 'id_project_task', index: 'id_project_task', hidden: true },
                        { name: 'task_name', index: 'task_name' },
                        { name: 'assigned_to_user', index: 'Assigned name' },
                        { name: 's_date', index: 'start_date' },
                        { name: 'e_date', index: 'end_date' },
                        { name: 'current_estimated_time', index: 'estimated_time' },
                        { name: 'actual_time', index: 'actual_time' },
                        { name: 'current_status', index: 'Status' },
                        { name: 'Action', index: 'Action',  formatter: function(c,o,d){
                            var _html = '';
                            if(d.current_status == 'In progress'){
                            _html +=  '<span class="pull-left"></span><a class="link pull-right" onclick="showTimer(this)"><i class="fa fa-ellipsis-v font-18"></i></a>';
                            _html += "<ul class='grid-actions'>"
                            _html += "<li><a href='javascript:;'  onclick='logTime(this)' data-type='timer' task-object='"+JSON.stringify(d)+"' >Log Time</a></li>";

                                //console.log('ddddddd',d);
                            _html += "<?php if($this->session->userdata('user_type_id') == 2  || ($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead')==1)){ ?>";

                            _html += "<li><a href='javascript:;'  onclick='logTime(this)' data-type='add' task-object='"+JSON.stringify(d)+"'  >Add Time</a></li>";

                            _html += "<?php } ?>"
                             _html += "</ul>";
                            }

                            return _html;
                        }
                        }
                    ],
                    pager:'tasks-list-page'
                }).navGrid('#tasks-list-page', {
                    edit: false, add: false, del: false, search: false, refresh: true,
                    addfunc: function () {
                        addEditCreateSchedule();
                    }, editfunc: function (id) {
                        var rowData = $refTblgrid.jqGrid('getRowData', id);
                        addEditCreateSchedule(rowData);
                    }, delfunc: function (id) {
                        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                            $(this).closest('.modal').modal('hide');
                            TssLib.notify('Deleted successfully ', null, 5);
                        }, 'Yes', 'No');
                    }
                });
                $refTblgrid.jqGrid('setGroupHeaders', {
                    useColSpanStyle: false,
                    groupHeaders:[
                        {startColumnName: 'Estimated', numberOfColumns: 3, titleText: 'Time Logged (in Hours)'}
                    ]
                });

                //touch-points grid
                var $refTblgrid = $('#touch-points-list'); //console.log($refTblgrid);
                $refTblgrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/touchPoint/',
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    postData: { parent_id: <?=$task_id?> },
                    extSearchField: '.searchInput',
                    colNames: ['Task Name', 'Received on', 'Dept.Name','Assigned To','Status'],
                    colModel: [
                        { name: 'task_name', index: 'task_name' },
                        { name: 'date', index: 'Date' },
                        { name: 'department_name', index: 'Dept.Name' },
                        { name: 'user_name', index: 'User Name' },
                        { name: 'status', index: 'status' }
                    ],
                    pager:'touch-points-list-page'
                }).navGrid('#touch-points-list-page', {
                    edit: false, add: false, del: false, search: false, refresh: true,
                    addfunc: function () {
                        addEditCreateSchedule();
                    }, editfunc: function (id) {
                        var rowData = $refTblgrid.jqGrid('getRowData', id);
                        addEditCreateSchedule(rowData);
                    }, delfunc: function (id) {
                        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                            $(this).closest('.modal').modal('hide');
                            TssLib.notify('Deleted successfully ', null, 5);
                        }, 'Yes', 'No');
                    }
                });
                $refTblgrid.jqGrid('setGroupHeaders', {
                    useColSpanStyle: false,
                    groupHeaders:[
                        {startColumnName: 'Estimated', numberOfColumns: 3, titleText: 'Time Logged (in Hours)'}
                    ]
                });


                var isDelLog = "<?php if($this->session->userdata('user_type_id') == 2  || ($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead')==1)){ echo true;}else{ echo false;} ?>";
                var $refTblgrid = $('#time-log-list');
                $refTblgrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/taskLogList/',
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    postData: { task_id: <?=$task_id?> },
                    extSearchField: '.searchInput',
                    colNames: ['Id', 'Description','Task Type','User','Date','From','To','Duration(H:M)','Comments'],
                    colModel: [
                        { name: 'log_time_id', index: 'log_time_id', hidden: true,key:true },
                        { name: 'task_name', index: 'task_name' },
                        { name: 'task_type', index: 'task_type' },
                        { name: 'user', index: 'user'},
                        { name: 'logDate', index: 'logDate' },
                        { name: 'start_time', index: 'start_time'},
                        { name: 'end_time', index: 'end_time'},
                        { name: 'a_time', index: 'a_time'},
                        { name: 'comments', index: 'comments' },
                    ],
                    pager:'time-log-page'
                }).navGrid('#time-log-page', {
                    edit: false, add: false, del: isDelLog, search: false, refresh: true,
                    delfunc: function (id) {
                        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                            $(this).closest('.modal').modal('hide');
                            getJsonAsyncWithBaseUrl("project/deleteLogTime/"+id, {}, {
                                jsonContent: true,
                                callback: function (result) {
                                    if (result.data.status ) {
                                        $('#time-log-list').trigger('reloadGrid');
                                        TssLib.notify('Deleted successfully ', null, 5);
                                    }
                                }
                            });
                        }, 'Yes', 'No');
                    }
                });
                $refTblgrid.jqGrid('setGroupHeaders', {
                    useColSpanStyle: false,
                    groupHeaders:[
                        {startColumnName: 'Estimated', numberOfColumns: 3, titleText: 'Time Logged (in Hours)'}
                    ]
                });




            });

            function getUseCase(e){
                $('body').find('[tab-context="use-case"]').trigger('click');
            }


    <?php for($s = 0;$s < count($attachment_task_id);$s++){ ?>
    jQuery('#task_attachment_frm_<?=$attachment_task_id[$s]?>').ajaxForm(function (data) {
        var res = JSON.parse(data);
        if (res.status) {
            var html = '<tr><td>'+res.data.user_name+'</td><td><a target="_blank" href="' + res.data.attachment + '">' + res.data.attachment_name + '</a></td></tr>'
            $('#task_attachment_<?=$attachment_task_id[$s]?>').find('table tbody').prepend(html);
            $('#task_attachment_<?=$attachment_task_id[$s]?>').find('table thead').html('<tr><th>Uploaded by</th><th>Download Link</th></tr>');
            $('#remove_attachment_<?=$attachment_task_id[$s]?>').trigger("click");
        }
        else {
            TssLib.notify(res.data, 'warn', 5);
        }
    });
    <?php } ?>


</script>
<!-- InstanceEndEditable -->

</div>

</div>