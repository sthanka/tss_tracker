<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->

        <div class="contentHeader">
            <!--<h3>Nagaraju</h3>-->
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Task Overview
                    </h3>
                    <h3 class="pull-right">
                        <a class="pull-right border-left">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </h3>
                </div>
                <div class="grid-details-table-content padding0">
                    <div class="col-sm-12 clearfix clearboth border-btm pt10">
                        <div class="col-sm-2">
                            <p class="blue-text">Task Info</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><a href="<?=WEB_BASE_URL?>index.php/project/overview/<?=$task_details[0]['id_project']?>"><?=$task_details[0]['project_name']?></a></p>
                                    <label><?=$task_details[0]['task_name']?></label>
                                </div>
                            </div>

                            <div class="view_input col-sm-2">
                                <p><span class="font-white orange-bg x-font" name="status">Pending</span></p>
                                <label class="margin0">Status <a class="cursor-pointer  font-12" onclick="changeStatus();" data-original-title="change status"><i class="fa fa-random"></i></a></label>
                            </div>
                            <div class="view_input col-sm-2">
                                <p><span class="font-white red-bg x-font" name="Priority">High</span></p>
                                <label class="margin0">Priority <a class="cursor-pointer  font-12" onclick="changePriority();" data-original-title="change priority"><i class="fa fa-random"></i></a></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth border-btm top10">
                        <div class="col-sm-2">
                            <p class="blue-text">Description</p>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-12 clearfix clearboth">
                                <div class="view_input">
                                    <p><?=$task_details[0]['description']?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth top10">
                        <div class="col-sm-2">
                            <p class="blue-text">Task Status</p>
                        </div>
                        <div class="col-sm-10">

                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><?=date('d-m-Y',strtotime($task_details[0]['start_date']))?></p>
                                    <label>Start Date</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><?=date('d-m-Y',strtotime($task_details[0]['end_date']))?></p>
                                    <label>End Date</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p>Shiva NR</p>
                                    <label>Assigned By</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p>2/5</p>
                                    <label>Sub Tasks(Done/Total)</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p><?=$task_details[0]['estimated_time']?></p>
                                    <label>Est. Time(Hrs)</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="view_input">
                                    <p>10</p>
                                    <label>Act. Time(Hrs)</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 padding0">
                        <ul class="nav nav-tabs nav-tabs-top custom-nav-tabs" role="tablist" id="project-tab">
                            <li class="active">
                                <a href="#tasks" data-toggle="tab">
                                    <p class="tab-list">Tasks</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#use-case" data-toggle="tab">
                                    <p class="tab-list">Use Case</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#test-result" data-toggle="tab">
                                    <p class="tab-list">Test Result</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#attachments" data-toggle="tab">
                                    <p class="tab-list">Attachments</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#time-log" data-toggle="tab">
                                    <p class="tab-list">Time Log</p>
                                </a>
                            </li>
                            <li class="">
                                <a href="#checklist" data-toggle="tab">
                                    <p class="tab-list">Checklist</p>
                                </a>
                            </li>

                            <li class="">
                                <a href="#timeline" data-toggle="tab">
                                    <p class="tab-list">Timeline</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content cv-tabs-container">
                <div id="tasks" class="tab-pane padding0 pb0 active">
                    <div class="tbl_wrapper border0">
                        <table id="tasks-list" class="table-responsive"></table>
                        <div id="tasks-list-page"></div>
                    </div>
                    <div class="dark-modal" id="TIMER" style="display:none">
                        <h3 class="dm-head show-min">
                            CRM
                            <a class="pull-right mr10" onclick="toggleScreen(this)"><i class="fa fa-thumb-tack"></i></a>
                        </h3>
                        <p class="dm-description">
                            Description of the task will come here
                        </p>
                        <label>Task Type</label>
                        <select>
                            <option value="">--Select--</option>
                            <option value="1">R&D</option>
                            <option value="2">Test</option>
                            <option value="3">Desgin</option>
                            <option value="4">Development / Code</option>
                            <option value="5">Review</option>
                        </select>
                        <label>Comments</label>
                        <textarea placeholder="Enter Comments" rows="2" class="col-sm-12 clearfix clearboth padding0 mb5"></textarea>
                        <div class="timer-area show-min">
                            <span>Task Time</span>
                            <b>01:25:58</b>
                            <a href="javascript:;" class="play" onclick="playPause(this, true)">
                                <i class="fa fa fa-pause"></i>
                                <i class="fa fa fa-play"></i>
                            </a>
                        </div>
                        <div class="time-details clearfix clearboth">
                            <div class="col-sm-8 padding0">
                                <p class="margin0">Start Time : <b>01:20 PM</b></p>
                                <p>End Time : <b>05:00 PM</b></p>
                            </div>
                            <div class="col-sm-4 padding0">
                                <p class="margin0">03:40</p>
                                <p>Total Time</p>
                            </div>
                        </div>
                        <div class="row-5">
                            <a class="green-btn-full">LOG TIME</a>
                            <a class="half-no-bg-btn" onclick="moreTimeRequest(this)">More Time Request</a>
                            <a class="half-no-bg-btn">Need Clarification</a>
                        </div>
                    </div>
                    <div class="dark-modal" id="MORE_TIME" style="display:none">
                    <h3 class="dm-head show-min">
                        CRM
                        <a class="pull-right mr10" onclick="toggleScreen(this)"><i class="fa fa-thumb-tack"></i></a>
                    </h3>
                    <p class="dm-description">
                        Description of the task will come here
                    </p>
                    <label>Reason</label>
                    <textarea placeholder="Enter Comments" rows="2" class="col-sm-12 clearfix clearboth padding0 mb5 form-control"></textarea>
                    <div class="col-sm-6 pad-left-none">
                        <label>Hours</label>
                        <input type="text" class="form-control" />
                    </div>
                    <div class="col-sm-6 pad-right-none">
                        <label>Minutes</label>
                        <input type="text" class="form-control" />
                    </div>
                    <label>% of Task Completed</label>
                    <input type="text" class="form-control" />
                    <div class="row-5">
                        <a class="green-bg half-btn" onclick="sendRequest(this)">SEND</a>
                        <a class="red-bg half-btn" onclick="sendRequest(this)">CANCEL</a>
                    </div>
                </div>
                </div>
                <div id="use-case" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <div class="col-xs-4 col-lg-3 pad-left-none">
                            <ul id="" class="nav tabs-left">
                                <?php for($s=0;$s<count($use_cases);$s++){ if($use_cases[$s]['user_id']!=0 && $use_cases[$s]['user_id']!=''){ ?>
                                    <li class="<?php if($s==0){ echo "active"; } ?>" >
                                        <a href="#task_<?=$use_cases[$s]['id_project_task']?>" data-toggle="tab">
                                            <b><?=$use_cases[$s]['task_name']?></b>
                                        </a>
                                    </li>
                                <?php } } ?>
                            </ul>
                        </div>
                        <div class="col-xs-8 col-lg-9 padding0">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <?php for($s=0;$s<count($use_cases);$s++){ if($use_cases[$s]['user_id']!=0 && $use_cases[$s]['user_id']!=''){ ?>
                                    <div class="tab-pane padding10 <?php if($s==0){ echo "active"; } ?>" id="task_<?=$use_cases[$s]['id_project_task']?>">
                                        <textarea name="use_case" id="user_case_text_<?=$use_cases[$s]['id_project_task']?>" ><?=$use_cases[$s]['task_usecase']?></textarea>
                                        <input type="button" id="use_case_btn_<?=$use_cases[$s]['id_project_task']?>" class="button-common" value="Save" onclick="updateUseCase('<?=$use_cases[$s]['id_task_usecase']?>','<?=$use_cases[$s]['id_project_task']?>','<?=$use_cases[$s]['user_id']?>');">
                                    </div>

                                <?php } } ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="test-result" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <textarea class="form-control border col-sm-12" rows="6" placeholder="Enter Test Results"></textarea>
                    </div>
                </div>
                <div id="attachments" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <div class="col-xs-4 col-lg-3 pad-left-none">
                            <ul id="" class="nav tabs-left">
                                <?php
                                $attachment_task_id = array_values(array_unique(array_map(function($e){ return $e['id_project_task']; },$attachment)));
                                $attachment_task_name = $attachment_task_user = array();
                                for($r=0;$r<count($attachment_task_id);$r++)
                                {
                                    for($s=0;$s<count($attachment);$s++){ if($attachment_task_id[$r]==$attachment[$s]['id_project_task']){
                                        $attachment_task_name[$attachment_task_id[$r]] = $attachment[$s]['task_name'];
                                        $attachment_task_user[$attachment_task_id[$r]] = $attachment[$s]['user_id'];
                                    } }
                                }

                                for($s=0;$s<count($attachment_task_id);$s++){ ?>
                                    <li class="<?php if($s==0){ echo "active"; } ?>" >
                                        <a href="#task_attachment_<?=$attachment_task_id[$s]?>" data-toggle="tab">
                                            <b><?=$attachment_task_name[$attachment_task_id[$s]]?></b>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-xs-8 col-lg-9 padding0">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <?php for($s=0;$s<count($attachment_task_id);$s++){ ?>
                                <div class="tab-pane padding10 <?php if($s==0){ echo "active"; } ?>" id="task_attachment_<?=$attachment_task_id[$s]?>">
                                    <form class="NewErrorStyle form-horizontal NewErrorStyle animate-fld-bg align-error" method="post" id="task_attachment_frm_<?=$attachment_task_id[$s]?>" action="<?=WEB_BASE_URL?>index.php/Project/uploadTaskAttachment">
                                        <div class="input_container">
                                            <input type="file" name="task_attachment" id="task_attachment" class="form-control rval">
                                        </div>
                                        <input type="submit" class="button-common" name="btn" value="Upload">
                                        <input type="hidden" name="project_task_id" value="<?=$attachment_task_id[$s]?>">
                                        <input type="hidden" name="user_id" value="<?=$attachment_task_user[$attachment_task_id[$s]]?>">
                                    </form>
                                  <?php for($r=0;$r<count($attachment);$r++){ if($attachment_task_id[$s]==$attachment[$r]['id_project_task'] && $attachment[$r]['attachment']!=''){ ?>
                                        <div><a target="_blank" href="<?=WEB_BASE_URL?>uploads/<?=$attachment[$r]['attachment']?>"><?=$attachment[$r]['attachment']?></a></div>
                                  <?php } } ?>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="time-log" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <textarea class="form-control border col-sm-12" rows="6" placeholder="Enter Test Results"></textarea>
                    </div>
                </div>
                <div id="checklist" class="tab-pane padding0 pb0 ">
                    <div class="col-sm-12 clearfix clearboth">
                        <textarea class="form-control border col-sm-12" rows="6" placeholder="Enter Test Results"></textarea>
                    </div>
                </div>

                <div id="timeline" class="tab-pane padding0 pb0 ">

                        <div class="col-sm-12 clearfix clearboth padding10 bg-white">
                            <div class="tp-container padding10">
                                <div class="tp-row">
                                    <div class="tp-date tp-today" data-value="26Jan2015">
                                        Today
                                    </div>
                                    <?php for($s=0;$s<count($time_line);$s++){ ?>
                                        <div class="tp-content">
                                            <div class="tp-content-wrap">
                                                <p>
                                                    <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span><?=date('h:i a',strtotime($time_line[$s]['created_date_time']))?> By <a href="javascript:;"><?=$time_line[$s]['user_name']?></a><input type="hidden" name="TouchPointType" value="Lead Stage Move"><input type="hidden" name="Id" value="6278">
                                                </p>
                                                <p id="repText">
                                                    <?=$time_line[$s]['description']?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="tp-date" data-value="<?=date('dMY',strtotime($time_line[$s]['created_date_time']))?>">
                                            <?=date('d M',strtotime($time_line[$s]['created_date_time']))?>
                                            <span><?=date('Y',strtotime($time_line[$s]['created_date_time']))?></span>
                                        </div>
                                    <?php } ?>

                                    <!--<div class="tp-content">
                                        <div class="tp-content-wrap tp-admin">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM By <a href="javascript:;">Lokesh</a><input type="hidden" name="TouchPointType" value="Lead Stage Move"><input type="hidden" name="Id" value="6278">
                                            </p>
                                            <p id="repText">
                                                Status Updated To: Ramesh ,By: Ravi
                                            </p>
                                        </div>
                                        <div class="tp-content-wrap">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM  Comments Added By <a href="javascript:;">Lokesh</a><input type="hidden" name="TouchPointType" value="Lead Stage Move"><input type="hidden" name="Id" value="6278">
                                            </p>
                                            <p id="repText">
                                                Test1 Comments......
                                            </p>
                                            <p id="repText">
                                                Test2 Comments......
                                            </p>
                                            <p id="repText">
                                                Test3 Comments......
                                            </p>
                                        </div>
                                        <div class="tp-content-wrap tp-admin">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM Assigned To:
                                            </p>
                                            <p id="repText">
                                                Ravi
                                            </p>
                                        </div>
                                        <div class="tp-content-wrap tp-admin">
                                            <p>
                                                <span class="typeConversation"><i class="fa fa-user"></i>&nbsp;</span>03:09 PM Created By:
                                            </p>
                                            <p id="repText">
                                                Ramesh
                                            </p>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="tp-row start-row"><div data-value="31Mar2015" class="tp-date">Start</div><div class="tp-content">&nbsp;</div></div>
                            </div>
                        </div>

                </div>
            </div>

            </div>
        </div>

        <!--MODAL POPS-->
        <div id="status-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="status-form">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Change Status</h4>
                        </div>
                        <input type="hidden" id="Id" name="Id" value="0" />
                        <div class="panel-body align-error">
                            <div class="col-sm-12 padding0 clearfix clearboth">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval" name="Status" id="Status">
                                                <option value="">--Select--</option>
                                                <option value="open">Open</option>
                                                <option value="inprogress">In Progress</option>
                                                <option value="completed">Done</option>
                                                <option value="reopen">Reopen</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Status :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 padding0 tbleWdth clearboth clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <textarea class="form-control" name="Comments"></textarea>
                                        </div>
                                        <label class="control-label">Comments :</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common panelbtn" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="priorities-modal" class="modal-wrapper" style="display: none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Change priority</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 padding0 clearfix clearboth">
                                <div class="col-sm-12">
                                    <div class="form-group margin0">
                                        <div class="input_container">
                                            <select class="form-control rval" name="Priority" id="Priority">
                                                <option value="">--Select--</option>
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Priority :</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common panelbtn" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<link rel="stylesheet" type="text/css" href="../Content/CSS/bootstrap-fileupload.min.css">
        <script type="text/javascript" src="../Scripts/bootstrap-fileupload.min.js"></script>-->
        <script>
            TssLib.docReady(function(){
                var page = '';

                $('a[tab-context="tasks"]').trigger('click');

            });

            function changeStatus(id) {
                var $modalPopup = TssLib.openModel({ width: 550 }, $('#status-modal').html());

                TssLib.ajaxForm({
                    jsonContent: true,
                    doServiceCall: true,
                    form: $('#status-form'), callback: function (data) {
                        if (data.success) {
                            if (isForEdit) {
                                TssLib.notify('status Updated Successfully');
                            }
                            TssLib.closeModal();
                            $taskmanager_1.trigger('reloadGrid');
                        }
                    }, before: function (formObj) {
                        $(formObj).attr('action-send', TssConfig.BASE_URL + 'Service.svc');
                        return true;
                    }
                });
            }
            function changePriority(id) {
                var $modalPopup = TssLib.openModel({ width: 550 }, $('#priorities-modal').html());

                TssLib.ajaxForm({
                    jsonContent: true,
                    doServiceCall: true,
                    form: $('#priorities-form'), callback: function (data) {
                        if (data.success) {
                            if (isForEdit) {
                                TssLib.notify('Priorities Updated Successfully');
                            }
                            TssLib.closeModal();
                            $taskmanager_1.trigger('reloadGrid');
                        }
                    }, before: function (formObj) {
                        $(formObj).attr('action-send', TssConfig.BASE_URL + 'Service.svc');
                        return true;
                    }
                });
            }
            function removeMember(e){
                $(e).closest('li').remove();
            }
            function addMember(e){
                var _memberName = $(e).prev().val();
                if(_memberName.length > 0){
                    $('#teamMembers').append('<li><label>'+_memberName+'</label><a onclick="removeMember(this);">X</a></li>');
                    $(e).prev().val('').focus();
                } else{
                    TssLib.notify('Please Enter Team Member Name','warn',5);
                }
            }


            //task tab function
            var reference = "Allotted";
            var refData = [
                { 'id': 1, 'Task':'Helpdesk Dashboard','Project':'SchoolPost', 'AssignedTo':'Nagaraju C','StartDate':'22/04/2016','EndDate':'28/04/2016','Estimated':'20','Actual':'24','Pending':'2' ,'Status':'In Progress','Actions':'Actions'},
                { 'id': 2, 'Task':'Admin Dashboard','Project':'Q-Lana', 'AssignedTo':'Mohan P','StartDate':'21/03/2016','EndDate':'26/05/2016','Estimated':'20','Actual':'20','Pending':'0' ,'Status':'Completed','Actions':'Actions'},
                { 'id': 3, 'Task':'Helpdesk Dashboard','Project':'SchoolPost', 'AssignedTo':'Nagaraju C','StartDate':'22/04/2016','EndDate':'28/04/2016','Estimated':'20','Actual':'0','Pending':'20' ,'Status':'To Do','Actions':'Actions'},
                { 'id': 4, 'Task':'Helpdesk Dashboard','Project':'SchoolPost', 'AssignedTo':'Nagaraju C','StartDate':'22/04/2016','EndDate':'28/04/2016','Estimated':'20','Actual':'24','Pending':'2' ,'Status':'In Progress','Actions':'Actions'}
            ];
            TssLib.docReady(function () {
                var $refTblgrid = $('#tasks-list');
                $refTblgrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/taskList/',
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    postData: { parent_id: <?=$task_id?> },
                    extSearchField: '.searchInput',
                    colNames: ['Id', 'Task', 'Project','Assigned To','Start Date', 'End Date','Estimated','Actual','Pending','Status','Action'],
                    colModel: [
                        { name: 'id_project_task', index: 'id_project_task', hidden: true },
                        { name: 'task_name', index: 'task_name' },
                        {
                            name: 'project_name', index: 'project_name', formatter: function (c, o, d) {
                            return '<a href="' + TssConfig.SITE_URL + 'index.php/Project/index">' + c + '</a>';
                        }
                        },
                        { name: 'user_name', index: 'user_name' },
                        { name: 'start_date', index: 'start_date' },
                        { name: 'end_date', index: 'end_date' },
                        { name: 'estimated_time', index: 'estimated_time' },
                        { name: 'Actual', index: 'Actual' },
                        { name: 'Pending', index: 'Pending' },
                        { name: 'status', index: 'status' },
                        { name: 'Action', index: 'Action',  formatter: function(c,o,d){
                            var _html = '<span class="pull-left">08:30 Hrs</span><a class="link pull-right mr10" onclick="showTimer(this)"><i class="fa fa-ellipsis-v font-18"></i></a>';
                            _html += '<ul class="grid-actions">'
                                +'<li><a href="javascript:;" onclick="logTime(this)">Log Time</a></li>'
                                +'<li><a href="javascript:;" onclick="moreTimeRequest(this)">Rquest More Time</a></li>'
                                +'<li><a href="javascript:;" onclick="logTime(this)">Need Clarification</a></li>'
                                +'<li><a href="javascript:;" onclick="logTime(this)">Completed</a></li>'
                                +'<li><a href="javascript:;" onclick="getUseCase(this)">Add Use Case</a></li>'
                                +'</ul>';
                            return _html;
                        }
                        }
                    ],
                    pager:'tasks-list-page'
                }).navGrid('#tasks-list-page', {
                    edit: false, add: false, del: false, search: false, refresh: true,
                    addfunc: function () {
                        addEditCreateSchedule();
                    }, editfunc: function (id) {
                        var rowData = $refTblgrid.jqGrid('getRowData', id);
                        addEditCreateSchedule(rowData);
                    }, delfunc: function (id) {
                        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                            $(this).closest('.modal').modal('hide');
                            TssLib.notify('Deleted successfully ', null, 5);
                        }, 'Yes', 'No');
                    }
                });
                $refTblgrid.jqGrid('setGroupHeaders', {
                    useColSpanStyle: false,
                    groupHeaders:[
                        {startColumnName: 'Estimated', numberOfColumns: 3, titleText: 'Time Logged (in Hours)'}
                    ]
                });

                $('.dark-modal').mouseenter(function(){
                    $(this).removeClass('minimized');
                }).mouseleave(function(){
                    $(this).addClass('minimized');
                })


            });

            function getUseCase(e){
                $('body').find('[tab-context="use-case"]').trigger('click');
            }
            function playPause(e){
                if(!$(e).hasClass('play')){
                    $(e).addClass('play').removeClass('pause');
                } else{
                    $(e).addClass('pause').removeClass('play');
                }
            }
            function toggleScreen(e){
                $(e).closest('.dark-modal').toggleClass('pin');
            }
            function logTime(e){
                $('#TIMER').show().removeClass('minimized');
                $(e).closest('.grid-actions').hide();
                $(e).prev().removeClass('in')
            }

            function moreTimeRequest(e){
                $('#MORE_TIME').slideDown()
                $('#TIMER').slideUp()
            }
            function sendRequest(e){
                $('#TIMER').slideDown()
                $('#MORE_TIME').slideUp()
            }

            <?php for($s=0;$s<count($attachment_task_id);$s++){ ?>
                jQuery('#task_attachment_frm_<?=$attachment_task_id[$s]?>').ajaxForm(function(data){
                    var res=JSON.parse(data);
                    if(res.status){
                        var html = '<div><a target="_blank" href="'+res.data.attachment+'">'+res.data.attachment_name+'</a><div>'
                        $('#task_attachment_<?=$attachment_task_id[$s]?>').append(html);
                    }
                });
            <?php } ?>


        </script>
        <!-- InstanceEndEditable -->

    </div>

</div>