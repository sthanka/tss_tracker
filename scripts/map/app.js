var LocsA = [{lat: 17.385044,
        	  lon: 10.9,
          	  title: 'Hyderabad',
			  html: [
				'<h3>Hyderabad</h3>',
				'<p>Start point A</p>'
			  ].join(''),
			  icon: 'http://maps.google.com/mapfiles/markerA.png',
			  animation: google.maps.Animation.DROP
    		}];
var LocsB = [{lat: 17.383334,
     		  lon: 78.486671,
              title: 'Hyderabad',
	          html: [
		  			'<h3>Hyderabad</h3>',
					'<p>Start point A</p>'
	 				].join(''),
	 			zoom: 8
    		}];
var LocsAB = LocsA.concat(LocsB);
var LocsC = [{lat: 17.4416,
	 		  lon: 78.3826,
	 		  title: 'Hitech City',
	 		  type : 'circle',
	 		  circle_options: {
				radius: 1000
			  },
	   		  visible: false
			}];
var LocsD = [{lat: 17.429382,
     		  lon: 78.486671
    		},
    		{lat: 17.385044,
     		 lon: 78.486671,
     		 stopover: true
   			}];
$(document).ready(function() { 
	var directions = new Maplace({
	  map_div: '#gmap-8',
	  generate_controls: false,
	  locations: LocsD,
	  type: 'directions',
	  draggable: true,
	  editable: true,
	  directions_panel: '#route',
	  afterRoute: function(distance) {
		$('#km').text((distance/1000)+'km');
	  }
	}).Load();
});
