<?php if($this->session->userdata('user_id')){ ?>
<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/TRACKER.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/jpg" href="<?=WEB_BASE_URL?>images/favicon.jpg" />
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>:: TSS :: TRACKER</title>
    <!-- InstanceEndEditable -->
    <link href="<?=WEB_BASE_URL?>css/bootstrap.css" rel="stylesheet" />
    <link href="<?=WEB_BASE_URL?>css/bootstrap-multiselect.css" rel="stylesheet" />
    <link href="<?=WEB_BASE_URL?>css/style.default.css" rel="stylesheet">
    <link href="<?=WEB_BASE_URL?>css/custom-styles.css" rel="stylesheet">
    <link href="<?=WEB_BASE_URL?>css/ui.jqgrid.css" rel="stylesheet" />
    <link href="<?=WEB_BASE_URL?>css/custom-select.css" rel="stylesheet" />
    <link href="<?=WEB_BASE_URL?>css/bootstrap-timepicker.min.css" rel="stylesheet" />

    <link href="<?=WEB_BASE_URL?>css/scroll/jquery.mCustomScrollbar.min.css" rel="stylesheet" />

    <link href="<?=WEB_BASE_URL?>css/bootstrap-fileupload.min.css" rel="stylesheet" />
    <link href="<?=WEB_BASE_URL?>css/editor.css" rel="stylesheet" />
    <link href="<?=WEB_BASE_URL?>css/fullcalendar.css" rel="stylesheet" />
    <!--@RenderSection("HeaderCss", required: false)-->


    <!-- editor start -->
    <!--<link rel="stylesheet" href="<?/*=WEB_BASE_URL*/?>css/editor.css" />-->
<!--    <link rel="stylesheet" href="<?/*=WEB_BASE_URL*/?>css/kendo.common-material.min.css" />
    <link rel="stylesheet" href="<?/*=WEB_BASE_URL*/?>css/kendo.material.min.css" />-->
    <!-- editor end-->

    <script>
        var WEB_BASE_URL = '<?=WEB_BASE_URL?>';
    </script>
    <script src="<?=WEB_BASE_URL?>scripts/jquery-2.1.1.min.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/jquery.maskedinput.min.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/jquery-ui-1.11.0.js"></script>
    <!-- <script src="<?/*=WEB_BASE_URL*/?>scripts/bootstrap-datepicker.js"></script>-->
    <script src="<?=WEB_BASE_URL?>scripts/custom-select.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/bootstrap.min.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/bootstrap-multiselect.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/bootstrap-timepicker.min.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/bootstrap-fileupload.min.js"></script>
    <!--Tss Library start-->
    <script src="<?=WEB_BASE_URL?>scripts/tss_lib/jquery.class.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/tss_lib/tss_config.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/tss_lib/tss_lib-1.0.js"></script>
    <!--<script src="<?/*=WEB_BASE_URL*/?>scripts/tss_lib/tsscharts.js"></script>
    <script src="<?/*=WEB_BASE_URL*/?>scripts/tss_lib/tssFunnel.js"></script>
    <script src="<?/*=WEB_BASE_URL*/?>scripts/tss_lib/tsscharts-more.js"></script>
    <script src="<?/*=WEB_BASE_URL*/?>scripts/tss_lib/drilldown.js"></script>-->
    <script src="<?=WEB_BASE_URL?>scripts/tss_lib/jquery.form.js"></script>
    <script src="<?=WEB_BASE_URL?>scripts/timer.jquery.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/editor.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/highcharts.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/moment.min.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/fullcalendar.min.js"></script>

    <!--Tss Library End-->
    <!--[if lt IE 9]>
    <script src="<?=WEB_BASE_URL?>scripts/html5shiv.js"></script>
    <![endif]-->
    <!--@RenderSection("Header../scripts", required: false)-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- InstanceBeginEditable name="head" -->
    <!-- InstanceEndEditable -->

    <!-- editor start -->
    <!--<script src="<?/*=WEB_BASE_URL*/?>scripts/editor.js"></script>-->
    <!--<script src="<?/*=WEB_BASE_URL*/?>scripts/kendo.all.min.js"></script>-->
    <!-- editor end-->
</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="body-status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<section>
    <div class="headerbar" id="header">
        <div class="logopanel">
            <a href="<?=WEB_BASE_URL?>" >
                <img src="<?=WEB_BASE_URL?>images/threshold-logo.png" class="school-post-change" />

            </a>
        </div>
        <!--Header static code start-->

        //notifications

        <div class="header-right">
            <ul class="headermenu">
           <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1) or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_lead')==1)) {
            $notifications = $this->Project_modal->getNotifications(array('user_id' => $this->session->userdata('user_id'),'read_flag' =>1));

            ?>
                <li class="welcome-user">
                    <div id="toggleOptions">
                        <p>Notifications (<?=count($notifications)?>)</p>
                    </div>
                    <div class="btn-group pull-left">
                        <button id="tog" type="button" class="btn btn-default dropdown-toggle">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <?php for($s=0;$s<count($notifications);$s++) { ?>
                                <li>
                                    <a href="javascript:;" onclick="addNotificationFlag('<?=$notifications[$s]['id_notification']?>','<?=$notifications[$s]['url']?>')" id="user-header-profile-anchor">
                                        <?=$notifications[$s]['message']?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
                <?php } ?>
                <?php if($this->session->userdata('department_id_array')){
                    $departs = $this->session->userdata('department_id_array');
                    $departs_name = $this->session->userdata('department_name_array');
                    $is_lead = $this->session->userdata('is_lead_array');
                    ?>
                    <li class="welcome-user">
                        <div>
                            <select name="" id="user_department_id" onchange="updateSessionDepartment(this.value);">
                                <?php for($s=0;$s<count($departs);$s++){ ?>
                                    <option <?php if($this->session->userdata('department_id')==$departs[$s]){ echo "selected='selected'"; } ?> value="<?=$departs[$s]?>@@@@<?=$departs_name[$s]?>@@@@<?=$is_lead[$s]?>"><?=$departs_name[$s]?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </li>
                <?php } ?>

                <li class="welcome-user">
                    <div id="toggleOptions">
                        <p>Welcome</p>
                        <p class="welcome-name"><span>
                            <?php if($this->session->userdata('user_id')){ echo $this->session->userdata('username'); } else { echo 'Username'; } ?>
                            </span>
                        </p>
                    </div>
                    <div class="btn-group pull-left">
                        <button id="tog" type="button" class="btn btn-default dropdown-toggle">
                            <span class="caret"></span>
                    <span class="user-img">
                        <img src="<?php if($this->session->userdata('user_id')){ echo $this->session->userdata('user_image'); } else { echo WEB_BASE_URL.'images/student-pic.png'; } ?>" alt="user" />
                    </span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li>
                                <a href="<?=WEB_BASE_URL?>index.php/User/profile" id="user-header-profile-anchor">
                                    <i class="glyphicon glyphicon-user"></i> My Profile
                                </a>
                            </li>

                            <li><a id="btnLogout" href="<?=WEB_BASE_URL?>index.php/User/logout">
                                    <i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>

        <div class="pageheader">
            <a class="menutoggle"></a>
            <!--    <h2>Budget Allocation </h2>-->
            <div class="breadcrumb-wrapper">
                <!--<ol class="breadcrumb">
                    <li><a href="<?/*=WEB_BASE_URL*/?>">Dashboard</a></li>
                    <li class="active">Page title will come here</li>
                </ol>-->
            </div>

        </div>
        <script>
            var studentAjax = null, CheckLoginAjax = null;
            TssLib.docReady(function () {
                $('.welcome-user').focusout(function () {
                    $(this).find('.dropdown-menu-usermenu').hide();
                });
                $('body').addClass('rebranding').attr('data-theme', 'change');
                $('#btnLogout').click(function () {
                    TssLib.LOGOUT();
                    window.location = TssConfig.SITE_URL;
                });

                $('.welcome-user').hover(function (ev) {
                    ev.preventDefault();
                    $(this).find('.dropdown-menu-usermenu').stop(true, true).show();
                });
                $('.welcome-user').mouseleave(function (event) {
                    event.preventDefault();
                    $('.welcome-user').unbind('hover');
                    $(this).find('.dropdown-menu-usermenu').stop(true, true).hide();
                });
            });

            //notifications
            function addNotificationFlag(notification_id,url)
            {
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: WEB_BASE_URL+'index.php/Project/addNotificationFlag',
                    dataType: 'json',
                    data: {notification_id:notification_id},
                    success:function(res){
                        //location.reload(url);
                        window.location = url;
                    }
                });
            }
        </script>
        <!--Header static code end-->
    </div>
<?php } else {?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title>::TSS:: LOGIN</title>
        <link rel="shortcut icon" type="image/jpg" href="<?=WEB_BASE_URL?>images/favicon.jpg" />
        <link rel="stylesheet" type="text/css" href="<?=WEB_BASE_URL?>css/tLogin.css">
        <link href="<?=WEB_BASE_URL?>css/style.default.css" rel="stylesheet">
        <link href="<?=WEB_BASE_URL?>css/custom-styles.css" rel="stylesheet">

        <script>
            var WEB_BASE_URL = '<?=WEB_BASE_URL?>';
        </script>
        <script src="<?=WEB_BASE_URL?>scripts/jquery-2.1.1.min.js"></script>
        <script src="<?=WEB_BASE_URL?>scripts/tss_lib/jquery.class.js"></script>
        <script src="<?=WEB_BASE_URL?>scripts/tss_lib/tss_config.js"></script>
        <script src="<?=WEB_BASE_URL?>scripts/tss_lib/tss_lib-1.0.js"></script>
    </head>
    <body>
<?php } ?>
