<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Report_model');
    }

    public function index()
    {
        $data['content'] = 'reports/reports';
        $this->load->view('partials/landing',$data);
    }

    public function projectStatus()
    {
        $data['content'] = 'reports/project_status_reports';
        $this->load->view('partials/landing',$data);
    }

    public function getProjectRequirementStatus()
    {
        $data['content'] = 'reports/project_status_reports1';
        $this->load->view('partials/landing',$data);
    }

    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp)
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
    }

    public function getWeekWorkloadReport($week=0)
    {
        $data['week'] = $week;
        if($week!=0){
            $check = $this->isValidTimeStamp($week);
            if($check==''){ redirect(WEB_BASE_URL); }
        }
        if($week==0){
            $data['last_week'] = strtotime('-7 day', strtotime('monday this week'));
            $data['week'] = strtotime('monday this week');
            //$data['current_week'] = strtotime('monday this week');
            $data['next_week'] = strtotime('+7 day', strtotime('monday this week'));
        }
        else{
            //date('Y-m-d', strtotime('previous monday', strtotime($week)))
            if(strtotime('monday this week')!=$week)
                $data['last_week'] = strtotime('-7 day', $week);
            else $data['last_week'] = strtotime('-7 day', $week);
            $data['week'] = $week;
            $data['next_week'] = strtotime('+7 day', $week);
        }

        //echo "<pre>"; print_r($data); exit;
        $data['users'] = $data['team_members'] = $users = $this->Project_modal->getUsers(array());
        //echo "<pre>"; print_r($data['users']); exit;
        //For holidays
        $this->load->model('Holiday_modal');
        $holidaysList = $this->Holiday_modal->getListOfHolidaysByDateRange(array('start_date' => date('m/d/Y',$data['week']), 'end_date' => date('m/d/Y',strtotime('+6 day', $data['week']))));

        $item = [];
        foreach($holidaysList as $k=>$v){
            $item[$v['date']] = array('id' => $v['id_holiday'], 'date' => $v['date'], 'title' => $v['title'], 'description' => $v['description']);
        }
        $data['holidays'] = $item;
        $start_date = date('Y-m-d',$data['week']);
        $end_date = date('Y-m-d',strtotime('+6 day', $data['week']));
        $user_ids = array_map(function($i){ return $i['id_user']; },$users);

        $log_time = $this->Report_model->getUserLogTimeByDate(array('user_id' => $user_ids,'start_date' => $start_date,'end_date' => $end_date));
        $data['user_log_time'] = array();
        for($s=0;$s<count($log_time);$s++)
        {
            $data['user_log_time'][$log_time[$s]['user_id']][$log_time[$s]['day']] = $log_time[$s];
        }

        $data['project_id'] = 0;
//echo "<pre>"; print_r($data['user_log_time']); exit;
        $data['content'] = 'reports/load_week_workload';
        $this->load->view('partials/landing',$data);
    }

    public function getWeekLoad()
    {
        $data['week'] = $week = $_POST['week'];
        if($week!=0){
            $check = $this->isValidTimeStamp($week);
            if($check==''){ redirect(WEB_BASE_URL); }
        }
        if($week==0){
            $data['last_week'] = strtotime('-7 day', strtotime('monday this week'));
            $data['week'] = strtotime('monday this week');
            //$data['current_week'] = strtotime('monday this week');
            $data['next_week'] = strtotime('+7 day', strtotime('monday this week'));
        }
        else{
            //date('Y-m-d', strtotime('previous monday', strtotime($week)))
            if(strtotime('monday this week')!=$week)
                $data['last_week'] = strtotime('-7 day', $week);
            else $data['last_week'] = strtotime('-7 day', $week);
            $data['week'] = $week;
            $data['next_week'] = strtotime('+7 day', $week);
        }

        //echo "<pre>"; print_r($data); exit;
        $data['users'] = $data['team_members'] = $users = $this->Project_modal->getUsers(array());
        //echo "<pre>"; print_r($data['users']); exit;
        //For holidays
        $this->load->model('Holiday_modal');
        $holidaysList = $this->Holiday_modal->getListOfHolidaysByDateRange(array('start_date' => date('m/d/Y',$data['week']), 'end_date' => date('m/d/Y',strtotime('+6 day', $data['week']))));

        $item = [];
        foreach($holidaysList as $k=>$v){
            $item[$v['date']] = array('id' => $v['id_holiday'], 'date' => $v['date'], 'title' => $v['title'], 'description' => $v['description']);
        }
        $data['holidays'] = $item;
        $start_date = date('Y-m-d',$data['week']);
        $end_date = date('Y-m-d',strtotime('+6 day', $data['week']));
        $user_ids = array_map(function($i){ return $i['id_user']; },$users);

        $log_time = $this->Report_model->getUserLogTimeByDate(array('user_id' => $user_ids,'start_date' => $start_date,'end_date' => $end_date));
        $data['user_log_time'] = array();
        for($s=0;$s<count($log_time);$s++)
        {
            $data['user_log_time'][$log_time[$s]['user_id']][$log_time[$s]['day']] = $log_time[$s];
        }

        $data['project_id'] = 0;
//echo "<pre>"; print_r($data['user_log_time']); exit;
        //$data['content'] = 'reports/load_week_workload_model';
        $data=$this->load->view('reports/load_week_workload_model',$data, TRUE);
        $this->output->set_output($data);
    }

    public function getTeamWorkloadReport()
    {
        $data['content'] = 'reports/team_workload_reports';
        $this->load->view('partials/landing',$data);
    }

    private function getDateDiffrence($date1 , $date2){
        $now = strtotime($date1);
        $your_date = strtotime($date2);
        $datediff = $now - $your_date;
        return floor($datediff/(60*60*24));
    }

    public function generateReport()
    {
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $user_id = $_POST['user_id'];
        $project_id = $_POST['project_id'];
        $total_duration = $over_all_duration = 0;
        require_once 'application/third_party/PHPExcel/Classes/PHPExcel.php';

        $user_log_time = array();

        $excel_columns = array('Project Name','User Name','Task Name','Task Type','Description','Start Time','End Time','Duration','Date');
        $task_wise_data_excel_columns = array('User Name','Project Name','Task Name','Date','Task Status','Allotted Time','Log Time','Remaining Time ');

        $row_number=1; $column_index=65;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)->setTitle('Overall Log Time');
        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 10,
                'name'  => 'Verdana'
            ));

        //getting user name and logtime sheet 0


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, "User Name");
        $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(50);
        $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
        $column_index++;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, "Total Time");
        $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(25);
        $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
        $column_index++;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, "Log Time");
        $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(25);
        $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
        $column_index++;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, "Difference");
        $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(25);
        $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
        $column_index++;
        $row_number++;


        $row_number=1; $column_index=65;
        $obj = $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1)->setTitle('Detailed Log Time');

        for($ec=0;$ec<count($excel_columns);$ec++){
            $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue(chr($column_index).$row_number, $excel_columns[$ec]);

            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($column_index))->setWidth(25);

            $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
            $column_index++;
        }

        $t_row_number = 1; $t_column_index=65;
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(2)->setTitle('Task Status and Time');
        for($ec=0;$ec<count($task_wise_data_excel_columns);$ec++){
            $objPHPExcel->setActiveSheetIndex(2)
                ->setCellValue(chr($t_column_index).$t_row_number, $task_wise_data_excel_columns[$ec]);

            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($t_column_index))->setWidth(25);

            $objPHPExcel->getActiveSheet()->getStyle(chr($t_column_index).$t_row_number)->applyFromArray($styleArray);
            $t_column_index++;
        }


        $excel_column_name = array('project_name','user_name','task_name','task_type','description','start_time','end_time','duration','date');

        for($s=0;$s<count($user_id);$s++)
        {
            $total_duration = 0;
            $user = $this->Project_modal->getUser(array('id_user' => $user_id[$s]));

            $user_log_time[$s]['user_name'] = $user[0]['first_name'].' '.$user[0]['last_name'];
            $user_log_time[$s]['log_time'] = $total_duration;

            $this->load->model('Attendance_modal');
            /*$this->load->model('Holiday_Modal');
            $this->load->model('User_model');*/
            $excel_ideal_time = $this->Attendance_modal->getIdealAttendance(array('user_id' => $user_id[$s],'start_date' => date('Y-m-d',strtotime(str_replace('/','-',$start_date))),'end_date' => date('Y-m-d',strtotime(str_replace('/','-',$end_date)))));
            $idealLogTime =0;
            foreach($excel_ideal_time as $k=>$v){
                $idealLogTime += time_to_sec($v->duration);
            }
            if($idealLogTime){
                $idealLogTime = sec_to_time($idealLogTime);
            }else{
                $idealLogTime ='00:00:00';
            }
            //echo "<pre>"; print_r($idealLogTime); exit;
            $user_log_time[$s]['ideal_time'] = $idealLogTime;

            $excel_data = $this->Project_modal->getLogtime(array('user_id' => $user_id[$s],'start_date' => date('Y-m-d',strtotime(str_replace('/','-',$start_date))),'end_date' => date('Y-m-d',strtotime(str_replace('/','-',$end_date))),'project_id' => $project_id));

            //get task wise user allotted time and log time
            $task_wise_data = $this->Project_modal->getTaskWiseUserAllottedTime(array('user_id' => $user_id[$s],'start_date' => date('Y-m-d',strtotime(str_replace('/','-',$start_date))),'end_date' => date('Y-m-d',strtotime(str_replace('/','-',$end_date))),'project_id' => $project_id));

            if(!empty($excel_data))
            {
                $row_number=$row_number+1;
                $d = [];
                for($ed=0;$ed<count($excel_data);$ed++)
                {
                    $column_index=65;
                    for($ec=0;$ec<count($excel_column_name);$ec++)
                    {
                        if($excel_data[$ed][$excel_column_name[$ec]]==''){ $excel_data[$ed][$excel_column_name[$ec]]='---'; }
                        $objPHPExcel->setActiveSheetIndex(1)
                            ->setCellValue(chr($column_index).$row_number, $excel_data[$ed][$excel_column_name[$ec]]);
                        $column_index=$column_index+1;
                    }
                    $total_duration = $total_duration+time_to_sec($excel_data[$ed]['duration']);
                    //getting user wise logtime
                    $user_log_time[$s]['log_time'] = $user_log_time[$s]['log_time']+time_to_sec($excel_data[$ed]['duration']);

                    $over_all_duration = $over_all_duration+time_to_sec($excel_data[$ed]['duration']);
                    $row_number=$row_number+1;
                }

                $column_index=$column_index-2;
                $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, sec_to_time($total_duration));
                $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
                /*$row_number=$row_number+1;
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($column_index).$row_number, sec_to_time($total_duration));*/

                $row_number = $row_number+1;
            }
            else
            {
                $row_number=$row_number+1;
                $column_index=65;
                for($ec=0;$ec<count($excel_column_name);$ec++)
                {
                    if($excel_column_name[$ec]=='user_name'){
                        $objPHPExcel->setActiveSheetIndex(1)
                            ->setCellValue(chr($column_index).$row_number, $user[0]['first_name'].' '.$user[0]['last_name']);
                    }
                    else{
                        $objPHPExcel->setActiveSheetIndex(1)
                            ->setCellValue(chr($column_index).$row_number, '--');
                    }

                    $column_index=$column_index+1;
                }
                $total_duration = 0;
                $column_index=$column_index-2;
                $row_number=$row_number+1;
                $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue(chr($column_index).$row_number, sec_to_time($total_duration));
                $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);
                $row_number = $row_number+1;
            }

            //task wise data
            $task_wise_data_excel_column_names = array('user_name','project_name','task_name','date','task_status','allotted_time','log_time','remain_time');

            if(!empty($task_wise_data))
            {
                $t_row_number=$t_row_number+1;
                for($ed=0;$ed<count($task_wise_data);$ed++)
                {
                    $t_column_index=65;
                    $log_time = $this->Project_modal->getLogTimeByUser(array('user_id' => $task_wise_data[$ed]['user_id'],'task_flow_id' => $task_wise_data[$ed]['task_flow_id'],'date' => $task_wise_data[$ed]['date']));
                    $task_wise_data[$ed]['log_time'] = $log_time[0]['log_time'];
                    $task_wise_data[$ed]['remain_time'] = sec_to_time(time_to_sec($task_wise_data[$ed]['allotted_time'])-time_to_sec($task_wise_data[$ed]['log_time']));

                    for($ec=0;$ec<count($task_wise_data_excel_column_names);$ec++)
                    {
                        if($task_wise_data[$ed][$task_wise_data_excel_column_names[$ec]]==''){ $task_wise_data[$ed][$task_wise_data_excel_column_names[$ec]]='---'; }
                        $objPHPExcel->setActiveSheetIndex(2)
                            ->setCellValue(chr($t_column_index).$t_row_number, $task_wise_data[$ed][$task_wise_data_excel_column_names[$ec]]);
                        $t_column_index=$t_column_index+1;
                    }
                    $t_row_number=$t_row_number+1;
                }
            }

        }

        $row_number=$row_number+1;
        $objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue(chr($column_index).$row_number, sec_to_time($over_all_duration));
        $objPHPExcel->getActiveSheet()->getStyle(chr($column_index).$row_number)->applyFromArray($styleArray);


        //getting user name and logtime sheet 0

        $row_number=2; $column_index=65; $grand_total = 0; $difference_total = 0; $ideal_total = 0;
        //echo "<pre>"; print_r($user_log_time); exit;
        for($s=0;$s<count($user_log_time);$s++)
        {
            $column_index=65;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue(chr($column_index).$row_number, $user_log_time[$s]['user_name']);

            $column_index=$column_index+1;
            if(isset($user_log_time[$s]['ideal_time'])){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($column_index).$row_number, $user_log_time[$s]['ideal_time']);
                $ideal_total += time_to_sec($user_log_time[$s]['ideal_time']);
            }else{
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($column_index).$row_number, '00:00:00');
            }

            $column_index=$column_index+1;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue(chr($column_index).$row_number, sec_to_time($user_log_time[$s]['log_time']));

            if(time_to_sec($user_log_time[$s]['ideal_time']) > $user_log_time[$s]['log_time']){
                $diffrence = time_to_sec($user_log_time[$s]['ideal_time']) - $user_log_time[$s]['log_time'];
            } else if($user_log_time[$s]['log_time'] > time_to_sec($user_log_time[$s]['ideal_time'])){
                $diffrence = time_to_sec($user_log_time[$s]['ideal_time']) - $user_log_time[$s]['log_time'];
                $diffrence = str_replace('0-', '', $diffrence);
            } else {
                $diffrence = 0;
            }

            $diffrence = time_to_sec($user_log_time[$s]['ideal_time']) - $user_log_time[$s]['log_time'];

            $difference_total += $diffrence;

            $column_index=$column_index+1;
            /*$objPHPExcel->setActiveSheetIndex(0)
//                ->setCellValue(chr($column_index).$row_number, sec_to_time($diffrence));
                ->setCellValue(chr($column_index).$row_number, str_replace('0-', '', sec_to_time($diffrence)));*/
            if(time_to_sec($user_log_time[$s]['ideal_time']) > $user_log_time[$s]['log_time']){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($column_index).$row_number, sec_to_time($diffrence));
            } else if($user_log_time[$s]['log_time'] > time_to_sec($user_log_time[$s]['ideal_time'])){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($column_index).$row_number, '-'.str_replace('0-', '', sec_to_time($diffrence)));
            } else {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($column_index).$row_number, '00:00:00');
            }

            $grand_total = $grand_total+$user_log_time[$s]['log_time'];
            $row_number++;
        }
        $column_index=65;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, "Grand Total");
        $column_index=$column_index+1;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, sec_to_time($ideal_total));
        $column_index=$column_index+1;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue(chr($column_index).$row_number, sec_to_time($grand_total));
        $column_index=$column_index+1;
        $objPHPExcel->setActiveSheetIndex(0)
//            ->setCellValue(chr($column_index).$row_number, sec_to_time($difference_total));
            ->setCellValue(chr($column_index).$row_number, str_replace('0-', '', sec_to_time($difference_total)));

        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$row_number.":D".$row_number."")->getFont()->setBold(true);

        //getting task wise details for project
        $t_row_number = 1; $t_column_index=65;
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(3)->setTitle('Task Details');
        $task_wise_data_excel_columns =  array('Project Name','Task Name','Department Name','Initial Estimated Time','Additional Time','Total Time','Log Time');
        for($ec=0;$ec<count($task_wise_data_excel_columns);$ec++){
            $objPHPExcel->setActiveSheetIndex(3)
                ->setCellValue(chr($t_column_index).$t_row_number, $task_wise_data_excel_columns[$ec]);

            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($t_column_index))->setWidth(25);

            $objPHPExcel->getActiveSheet()->getStyle(chr($t_column_index).$t_row_number)->applyFromArray($styleArray);
            $t_column_index++;
        }
        $task_wise_data_excel_column_names = array('project_name','task_name','department_name','estimated_time','additional_time','total_time','members');
        for($r=0;$r<count($project_id);$r++)
        {
            $task_wise_data = $this->Project_modal->getTaskTimeByProject(array('project_id' => $project_id[$r]));
            //echo "<pre>"; print_r($task_wise_data); exit;
            //echo "<pre>"; print_r($task_wise_data); exit;
            $t_row_number=$t_row_number+1;
            for($ed=0;$ed<count($task_wise_data);$ed++)
            {
                $t_column_index=65;
                $task_wise_data[$ed]['members'] = '';
                $log_time = $this->Project_modal->getLogTimeByTaskFlow(array('task_flow_id' => $task_wise_data[$ed]['id_task_flow']));
                //echo "<pre>"; print_r($log_time);
                if(!empty($log_time)){
                    for($io=0;$io<count($log_time);$io++){
                        if($io>0){ $task_wise_data[$ed]['members'].=', '; }
                        $task_wise_data[$ed]['members'].=$log_time[$io]['members'];
                    }
                }
                else $task_wise_data[$ed]['members'] = '--';

                for($ec=0;$ec<count($task_wise_data_excel_column_names);$ec++)
                {
                    if($task_wise_data[$ed][$task_wise_data_excel_column_names[$ec]]==''){ $task_wise_data[$ed][$task_wise_data_excel_column_names[$ec]]='---'; }
                    $objPHPExcel->setActiveSheetIndex(3)
                        ->setCellValue(chr($t_column_index).$t_row_number, $task_wise_data[$ed][$task_wise_data_excel_column_names[$ec]]);
                    $t_column_index=$t_column_index+1;
                }
                $t_row_number=$t_row_number+1;
            }

        }


        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=reports.xls");
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $writer->save('php://output');
        exit;
    }

    function getUserLogTime()
    {
        $params = $this->input->get();
        //echo "<pre>"; print_r($params); exit;
        $list = $this->Project_modal->getUserLogTimeGrid($params);
        echo json_encode($list);
    }

    function getProjectStatus()
    {
        $params = $this->input->get();
        //echo "<pre>"; print_r($params); exit;
        $list = $this->Report_model->getProjectStatusReport($params);
        echo json_encode($list);
    }

    function getProjectRequirementStatusGrid()
    {
        $params = $this->input->get();
        //echo "<pre>"; print_r($params); exit;
        $list = $this->Report_model->getProjectRequirementStatusGrid($params);
        //echo "<pre>"; print_r($list); exit;
        for($s=0;$s<count($list['rows']);$s++)
        {
            $status_wise_task_count = $this->Project_modal->getStatusWiseTaskCount(array('requirement_id' => $list['rows'][$s]->id_requirements));
            //echo "<pre>"; print_r($status_wise_task_count); exit;
            $list['rows'][$s]->new = $list['rows'][$s]->progress = $list['rows'][$s]->completed = 0;
            foreach($status_wise_task_count as $k=>$v){
                if($v['status']=='new')
                    $list['rows'][$s]->new = $v['count'];
                if($v['status']=='progress')
                    $list['rows'][$s]->progress = $v['count'];
                if($v['status']=='completed')
                    $list['rows'][$s]->completed = $v['count'];
            }
            if($list['rows'][$s]->assigned_effort==''){ $list['rows'][$s]->assigned_effort='00:00'; }
            if($list['rows'][$s]->completed_effort==''){ $list['rows'][$s]->completed_effort='00:00'; }
            $list['rows'][$s]->pending_effort = sec_to_time((time_to_sec($list['rows'][$s]->estimated_effort)+time_to_sec($list['rows'][$s]->additional_time))-time_to_sec($list['rows'][$s]->assigned_effort));
        }
        echo json_encode($list);
    }

    function getProjectRequirement($projectId)
    {
        $data = $this->Project_modal->getProjectRequirement(array('project_id' => $projectId));
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$data));
    }

    function getTeamWorkloadGrid()
    {
        $params = $this->input->get();
        $project_id = $params['project_id'];
        //$project_id = 1;
        $departments = $this->Project_modal->getProjectDepartments($project_id);
        $task = $this->Project_modal->getProjectTaskDetails(array('project_id' => $project_id));
        $resultArray = array();
        for($s=0;$s<count($task);$s++)
        {
            $resultArray[$task[$s]['id_project_task']]['id_project_task'] = $task[$s]['id_project_task'];
            $resultArray[$task[$s]['id_project_task']]['task_name'] = $task[$s]['task_name'];
            $resultArray[$task[$s]['id_project_task']]['description'] = $task[$s]['description'];
            for($r=0;$r<count($departments);$r++)
            {
                $task_flow_details = $this->Project_modal->getTimeFromTaskFlow(array('project_task_id' => $task[$s]['id_project_task'],'department_id' => $departments[$r]['department_id']));
                $detail= '--';
                if(!empty($task_flow_details)){
                    $log_time = $this->Project_modal->getTotalLogTime(array('task_flow_id' => $task_flow_details[0]['id_task_flow']));
                    $detail = 'Status : '.$task_flow_details[0]['task_status'].'</br>';
                    $detail.= 'Estimated time : '.$task_flow_details[0]['estimated_time'].'</br>';
                    $detail.= 'Additional time : '.$task_flow_details[0]['additional_time'].'</br>';
                    if(!empty($log_time)){
                        $detail.= 'Actual time : '.sec_to_time(time_to_sec($log_time[0]['actual_time'])).'</br>';
                    }
                    $detail.= 'Start Date : '.date('d-m-Y',strtotime($task_flow_details[0]['start_date'])).'</br>';
                    $detail.= 'End Date : '.date('d-m-Y',strtotime($task_flow_details[0]['end_date'])).'</br>';
                    if($task_flow_details[0]['actual_end_date']!='')
                        $detail.= 'Completed Date : '.date('d-m-Y',strtotime($task_flow_details[0]['actual_end_date'])).'</br>';
                    else
                        $detail.= 'Completed Date : ---</br>';
                }
                $resultArray[$task[$s]['id_project_task']][$departments[$r]['department_name']] = $detail;
            }
        }
        echo json_encode(array('response' =>1,'data'=>$resultArray));
    }




}