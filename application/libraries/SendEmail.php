<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class SendEmail {
    
    function sendMail($email,$subject,$message, $files){

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );

        $CI =& get_instance();
        
        $CI->load->library('email');
        $CI->email->initialize($config);

        $CI->email->set_newline("\r\n");

//        $CI->email->from('app.mazic@gmail.com', 'app.mazic');
        $CI->email->from('projectmanager@thresholdsoft.com', 'Project Manager');
        $CI->email->to($email);

        $CI->email->subject($subject);
        $CI->email->message($message);
        
        foreach($files as $k => $v){
            $CI->email->attach($v);
        }
        
        if($CI->email->send()){
            $CI->email->clear(TRUE);
            return true;
        }else{
            return $CI->email->print_debugger();
        }
    }
}