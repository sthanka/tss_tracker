<?php
//echo '<pre>'; print_r($swap); exit;
if(count($swap)>0){ ?>
<?php if($edit==1){ ?>
<div class="col-sm-12">
    <a href="javascript:;" onclick="addSwap(this, <?=$attendanceId?>)"><i class="fa fa-plus-circle"></i></a>
</div>
<?php } ?>
<div class="col-sm-12">
    <table id="swapTable">
        <thead>
            <tr>
                <th>Swap in</th>
                <th>Swap out</th>
                <?php if($edit==1){ ?><th></th><?php } ?>
            </tr>
        </thead>
        <tbody>

<?php foreach($swap as $k=>$v){ ?>
            <tr>
                <td><input type="text" class="msk_time swap" value="<?=$v->in_time?>" data-id="<?=$v->attendance_id?>" data-swap-id="<?=$v->id_swipe_punch?>" data-swapin="1"></td>
                <td><input type="text" class="msk_time swap" value="<?=$v->out_time?>" data-id="<?=$v->attendance_id?>" data-swap-id="<?=$v->id_swipe_punch?>" data-swapin="0"></td>
                <?php if($edit==1){ ?><td><a href="javascript:;" onclick="deleteSwap(this)"><i class="fa fa-trash-o"></i></a></td><?php } ?>
            </tr>
<?php } ?>
        </tbody>
    </table>
</div>
<?php }else{ ?>
    No Swap details found
<?php } ?>
<script>
<?php if($edit==1){ ?>
    function deleteSwap(This){
        $(This).closest('tr').remove();
    }
    function addSwap(This , id){
        var _html = '';
        _html += '<tr>' +
            '<td><input type="text" class="msk_time swap" value="" data-id="'+id+'" data-swap-id="0" data-swapin="1"></td>' +
            '<td><input type="text" class="msk_time swap" value="" data-id="'+id+'" data-swap-id="0" data-swapin="0"></td>' +
            '<td><a href="javascript:;" onclick="deleteSwap(this)"><i class="fa fa-trash-o"></i></a></td>' +
            '</tr>';
        $('#swapTable tr:last').after(_html);
        $(".msk_time").mask("Hh:Mm");
    }
<?php } ?>
</script>
