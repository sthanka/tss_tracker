<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Client_modal extends CI_Model
{
    public $key = '#@Tss_Tracker$#';


    public function getClients()
    {
        $this->db->select('*');
        $this->db->from('client');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getDepartments()
    {
        $this->db->select('*');
        $this->db->from('department');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getClientsGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'id_client';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and c.client_name LIKE '%".$whereParam."%'"; }
        $whereClause = "where true ".$whereParam;

        $SQL = "SELECT c.*  from `client` c $whereClause
          order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT count(*)  from `client` c
          $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function addClient($data){
        $this->db->insert('client', $data);
        return $this->db->insert_id();
    }

    public function updateClient($id,$data){
        $this->db->where('id_client', $id);
        return $this->db->update('client', $data);
    }

    public function deleteClient($id)
    {
        return $this->db->delete('client', array('id_client' => $id));
    }

    public function checkClientName($id,$name){
        $this->db->select('*');
        if($id != ''){
            $this->db->where('id_client !=', $id);
        }
        $this->db->where('client_name', $name);
        $this->db->from('client');
        $query = $this->db->get();
        return $query->result_array();

    }
}