/*
Navicat MySQL Data Transfer

Source Server         : admin 192.168.0.04
Source Server Version : 50549
Source Host           : 192.168.0.4:3306
Source Database       : tss_tracker

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2016-06-24 20:56:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `attachment`
-- ----------------------------
DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `id_attachment` int(11) NOT NULL AUTO_INCREMENT,
  `attachment_type` varchar(255) DEFAULT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `attachment_name` varchar(255) DEFAULT NULL,
  `attachment` text,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_attachment`),
  KEY `project_task_id` (`project_task_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`project_task_id`) REFERENCES `project_task` (`id_project_task`),
  CONSTRAINT `attachment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `client`
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `client_email` varchar(30) DEFAULT NULL,
  `client_contact` int(10) DEFAULT NULL,
  `comments` text,
  `creared_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', 'Pepsico', 'fsd@erf.com', '1231231231', 'lyguiyhiuhy', null);
INSERT INTO `client` VALUES ('2', 'BBDO', null, null, null, null);
INSERT INTO `client` VALUES ('3', 'Okridge', null, null, null, null);
INSERT INTO `client` VALUES ('4', 'Qlana', 'qlana@gmail.com', '1231231231', 'qlana dsf', '2016-06-23 19:40:04');
INSERT INTO `client` VALUES ('5', 'Miles', 'miles@gmail.com', '1231231231', 'miles', '2016-06-23 19:41:53');

-- ----------------------------
-- Table structure for `day`
-- ----------------------------
DROP TABLE IF EXISTS `day`;
CREATE TABLE `day` (
  `id_day` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_day`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of day
-- ----------------------------
INSERT INTO `day` VALUES ('1', 'Mon');
INSERT INTO `day` VALUES ('2', 'Tue');
INSERT INTO `day` VALUES ('3', 'Wed');
INSERT INTO `day` VALUES ('4', 'Thu');
INSERT INTO `day` VALUES ('5', 'Fri');
INSERT INTO `day` VALUES ('6', 'Sat');
INSERT INTO `day` VALUES ('7', 'Sun');

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id_department` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_department`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', 'UX');
INSERT INTO `department` VALUES ('2', 'UI');
INSERT INTO `department` VALUES ('3', 'Dev - PHP');
INSERT INTO `department` VALUES ('6', 'TESTING');
INSERT INTO `department` VALUES ('7', 'Dev - JAVA');
INSERT INTO `department` VALUES ('8', 'Deploy & Maintenance');
INSERT INTO `department` VALUES ('9', 'Dev - .Net');

-- ----------------------------
-- Table structure for `department_checklist`
-- ----------------------------
DROP TABLE IF EXISTS `department_checklist`;
CREATE TABLE `department_checklist` (
  `id_checklist` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `checklist_name` text,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_checklist`),
  KEY `department_id` (`department_id`) USING BTREE,
  CONSTRAINT `department_checklist_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id_department`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department_checklist
-- ----------------------------
INSERT INTO `department_checklist` VALUES ('1', '3', 'Is code commented appropriately?', '2016-06-13 12:25:08');
INSERT INTO `department_checklist` VALUES ('2', '3', 'Does the code adhere to the defined Company Standards?', '2016-06-13 12:25:10');
INSERT INTO `department_checklist` VALUES ('3', '3', 'Is there error handling in place?', '2016-06-13 12:25:11');
INSERT INTO `department_checklist` VALUES ('4', '3', 'Is there appropriate security?', '2016-06-13 12:25:11');
INSERT INTO `department_checklist` VALUES ('5', '3', 'Is there appropriate logging?', '2016-06-13 12:25:12');
INSERT INTO `department_checklist` VALUES ('6', '3', 'Is localisation required?', '2016-06-13 12:25:13');
INSERT INTO `department_checklist` VALUES ('7', '3', 'Have release notes and any accompanying documentation been produced?', '2016-06-13 12:25:16');
INSERT INTO `department_checklist` VALUES ('8', '3', 'Has all testing and debugging code been removed?', '2016-06-13 12:25:14');
INSERT INTO `department_checklist` VALUES ('9', '3', 'Has sensitive data been such as passwords and licence keys been removed?', '2016-06-13 12:25:14');
INSERT INTO `department_checklist` VALUES ('10', '3', 'Has performance been checked? Any memory leaks?', '2016-06-13 12:25:13');
INSERT INTO `department_checklist` VALUES ('11', '1', 'Is code commented appropriately?', '2016-06-13 12:28:22');
INSERT INTO `department_checklist` VALUES ('12', '1', 'Does the code adhere to the defined Company Standards?', '2016-06-13 12:28:23');
INSERT INTO `department_checklist` VALUES ('13', '1', 'Is there error handling in place?', '2016-06-13 12:28:23');
INSERT INTO `department_checklist` VALUES ('14', '1', 'Is there appropriate security?', '2016-06-13 12:28:24');
INSERT INTO `department_checklist` VALUES ('15', '1', 'Is there appropriate logging?', '2016-06-13 12:28:24');
INSERT INTO `department_checklist` VALUES ('16', '1', 'Is localisation required?', '2016-06-13 12:28:24');
INSERT INTO `department_checklist` VALUES ('17', '1', 'Have release notes and any accompanying documentation been produced?', '2016-06-13 12:28:25');
INSERT INTO `department_checklist` VALUES ('18', '1', 'Has all testing and debugging code been removed?', '2016-06-13 12:28:25');
INSERT INTO `department_checklist` VALUES ('19', '1', 'Has sensitive data been such as passwords and licence keys been removed?', '2016-06-13 12:28:26');
INSERT INTO `department_checklist` VALUES ('20', '1', 'Has performance been checked? Any memory leaks?', '2016-06-13 12:28:27');
INSERT INTO `department_checklist` VALUES ('21', '2', 'Is code commented appropriately?', '2016-06-13 12:28:33');
INSERT INTO `department_checklist` VALUES ('22', '2', 'Does the code adhere to the defined Company Standards?', '2016-06-13 12:28:34');
INSERT INTO `department_checklist` VALUES ('23', '2', 'Is there error handling in place?', '2016-06-13 12:28:34');
INSERT INTO `department_checklist` VALUES ('24', '2', 'Is there appropriate security?', '2016-06-13 12:28:34');
INSERT INTO `department_checklist` VALUES ('25', '2', 'Is there appropriate logging?', '2016-06-13 12:28:34');
INSERT INTO `department_checklist` VALUES ('26', '2', 'Is localisation required?', '2016-06-13 12:28:35');
INSERT INTO `department_checklist` VALUES ('27', '2', 'Have release notes and any accompanying documentation been produced?', '2016-06-13 12:28:35');
INSERT INTO `department_checklist` VALUES ('28', '2', 'Has all testing and debugging code been removed?', '2016-06-13 12:28:35');
INSERT INTO `department_checklist` VALUES ('29', '2', 'Has sensitive data been such as passwords and licence keys been removed?', '2016-06-13 12:28:35');
INSERT INTO `department_checklist` VALUES ('30', '2', 'Has performance been checked? Any memory leaks?', '2016-06-13 12:28:37');
INSERT INTO `department_checklist` VALUES ('31', '6', 'Is code commented appropriately?', '2016-06-13 12:29:29');
INSERT INTO `department_checklist` VALUES ('32', '6', 'Does the code adhere to the defined Company Standards?', '2016-06-13 12:29:29');
INSERT INTO `department_checklist` VALUES ('33', '6', 'Is there error handling in place?', '2016-06-13 12:29:29');
INSERT INTO `department_checklist` VALUES ('34', '6', 'Is there appropriate security?', '2016-06-13 12:29:29');
INSERT INTO `department_checklist` VALUES ('35', '6', 'Is there appropriate logging?', '2016-06-13 12:29:30');
INSERT INTO `department_checklist` VALUES ('36', '6', 'Is localisation required?', '2016-06-13 12:29:30');
INSERT INTO `department_checklist` VALUES ('37', '6', 'Have release notes and any accompanying documentation been produced?', '2016-06-13 12:29:30');
INSERT INTO `department_checklist` VALUES ('38', '6', 'Has all testing and debugging code been removed?', '2016-06-13 12:29:30');
INSERT INTO `department_checklist` VALUES ('39', '6', 'Has sensitive data been such as passwords and licence keys been removed?', '2016-06-13 12:29:31');
INSERT INTO `department_checklist` VALUES ('40', '6', 'Has performance been checked? Any memory leaks?', '2016-06-13 12:29:33');

-- ----------------------------
-- Table structure for `job_role`
-- ----------------------------
DROP TABLE IF EXISTS `job_role`;
CREATE TABLE `job_role` (
  `id_job_role` int(11) NOT NULL AUTO_INCREMENT,
  `job_role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_job_role`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_role
-- ----------------------------
INSERT INTO `job_role` VALUES ('1', 'Ceo');
INSERT INTO `job_role` VALUES ('2', 'Project Manage');
INSERT INTO `job_role` VALUES ('3', 'Seniour Software Engineer');
INSERT INTO `job_role` VALUES ('4', 'Junior Software Engineer');
INSERT INTO `job_role` VALUES ('5', 'Team Lead');

-- ----------------------------
-- Table structure for `log_time`
-- ----------------------------
DROP TABLE IF EXISTS `log_time`;
CREATE TABLE `log_time` (
  `log_time_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_flow_id` int(11) DEFAULT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `comments` text,
  `user_id` int(11) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `task_type` int(11) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_time_id`),
  KEY `task_type` (`task_type`) USING BTREE,
  CONSTRAINT `log_time_ibfk_1` FOREIGN KEY (`task_type`) REFERENCES `task_type` (`id_task_type`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_time
-- ----------------------------
INSERT INTO `log_time` VALUES ('1', '1', '1', '1', 'Testing', '25', '3:00 PM', '4:00 PM', '01:00:00', '4', '2016-06-23 00:00:00');
INSERT INTO `log_time` VALUES ('2', null, null, null, '', '25', '5:00 PM', '5:30 PM', '00:30:00', '8', '2016-06-23 00:00:00');
INSERT INTO `log_time` VALUES ('3', '2', '2', '1', 'sdfsdf', '25', '9:39 AM', '12:39 PM', '03:00:00', '4', '2016-06-23 00:00:00');
INSERT INTO `log_time` VALUES ('4', '2', '2', '1', 'fghgfh', '25', '1:00 PM', '2:00 PM', '01:00:00', '4', '2016-06-23 00:00:00');
INSERT INTO `log_time` VALUES ('5', '2', '2', '1', 'tetsing123', '27', '5:39 PM', '6:39 PM', '01:00:00', '4', '2016-06-23 00:00:00');
INSERT INTO `log_time` VALUES ('6', '2', '2', '1', 'testing', '28', '4:44 PM', '6:44 PM', '02:00:00', '4', '2016-06-23 00:00:00');
INSERT INTO `log_time` VALUES ('7', null, null, null, 'mn,nm', '25', '12:40 PM', '2:15 PM', '01:35:00', '11', '2016-06-24 00:00:00');
INSERT INTO `log_time` VALUES ('8', null, null, null, 'hjkhk', '25', '09:00 AM', '09:39 AM', '00:39:00', '11', '2016-06-24 00:00:00');
INSERT INTO `log_time` VALUES ('9', '2', '2', '1', 'dfgfd', '25', '3:40 PM', '5:40 PM', '02:00:00', '5', '2016-06-24 00:00:00');
INSERT INTO `log_time` VALUES ('10', '2', '2', '1', 'dfgfdgfd', '25', '2:19 PM', '3:39 PM', '01:20:00', '4', '2016-06-24 00:00:00');
INSERT INTO `log_time` VALUES ('13', '2', '2', '1', 'xcvxc', '25', '10:00 AM', '11:36 AM', '01:36:00', '4', '2016-06-24 00:00:00');
INSERT INTO `log_time` VALUES ('14', '2', '2', '1', 'xcvxc', '25', '09:40 AM', '09:50 AM', '00:10:00', '4', '2016-06-24 00:00:00');
INSERT INTO `log_time` VALUES ('17', '2', '2', '1', 'sdfs', '25', '12:16 PM', '12:40 PM', '00:24:00', '1', '2016-06-24 17:06:21');
INSERT INTO `log_time` VALUES ('18', '4', '4', '2', 'test', '27', '12:03 PM', '1:03 PM', '01:00:00', '4', '2016-06-24 19:05:07');
INSERT INTO `log_time` VALUES ('19', '4', '4', '2', 'asdasd', '27', '10:05 AM', '10:07 AM', '00:02:00', '4', '2016-06-24 19:05:46');
INSERT INTO `log_time` VALUES ('20', '4', '4', '2', 'asdasddasd', '27', '9:03 AM', '9:05 AM', '00:02:00', '4', '2016-06-24 19:06:18');
INSERT INTO `log_time` VALUES ('21', '4', '4', '2', 'test', '27', '9:07 AM', '9:08 AM', '00:01:00', '4', '2016-06-24 19:07:48');
INSERT INTO `log_time` VALUES ('22', '4', '4', '2', 'tesg', '27', '9:11 AM', '9:12 AM', '00:01:00', '4', '2016-06-24 19:11:45');
INSERT INTO `log_time` VALUES ('24', '4', '4', '2', 'qweqwe', '27', '09:13 am', '09:14 am', '00:01:00', '4', '2016-06-24 19:20:28');
INSERT INTO `log_time` VALUES ('25', null, null, null, 'wdqwd', '27', '11:19 am', '11:27 am', '00:08:00', '8', '2016-06-24 20:20:22');

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id_project` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_start_date` varchar(255) DEFAULT NULL,
  `project_end_date` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_project`),
  KEY `client_id` (`client_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('1', 'Qlana', '4', '06/06/2016', '29/07/2016', 'Credit Risk Management', 'in-progress', '2016-06-23 16:43:02');
INSERT INTO `project` VALUES ('2', 'Miles', '5', '13/06/2016', '24/06/2016', 'Training Institute', 'in-progress', '2016-06-23 16:44:57');

-- ----------------------------
-- Table structure for `project_department`
-- ----------------------------
DROP TABLE IF EXISTS `project_department`;
CREATE TABLE `project_department` (
  `id_project_department` int(45) NOT NULL AUTO_INCREMENT,
  `project_id` int(45) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `estimation_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_project_department`),
  KEY `project_id` (`project_id`) USING BTREE,
  KEY `department_id` (`department_id`) USING BTREE,
  CONSTRAINT `project_department_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id_project`),
  CONSTRAINT `project_department_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id_department`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_department
-- ----------------------------
INSERT INTO `project_department` VALUES ('1', '1', '3', '500');
INSERT INTO `project_department` VALUES ('2', '2', '3', '200');

-- ----------------------------
-- Table structure for `project_employees`
-- ----------------------------
DROP TABLE IF EXISTS `project_employees`;
CREATE TABLE `project_employees` (
  `id_employee` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_employee`),
  KEY `project_id` (`project_id`) USING BTREE,
  KEY `employee_id` (`employee_id`) USING BTREE,
  CONSTRAINT `project_employees_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id_project`),
  CONSTRAINT `project_employees_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_employees
-- ----------------------------
INSERT INTO `project_employees` VALUES ('1', '1', '24', '2016-06-23 16:43:31');
INSERT INTO `project_employees` VALUES ('2', '1', '25', '2016-06-23 16:43:31');
INSERT INTO `project_employees` VALUES ('3', '1', '27', '2016-06-23 16:43:31');
INSERT INTO `project_employees` VALUES ('4', '2', '24', '2016-06-23 16:45:25');
INSERT INTO `project_employees` VALUES ('5', '2', '25', '2016-06-23 16:45:25');
INSERT INTO `project_employees` VALUES ('6', '2', '27', '2016-06-23 16:45:25');

-- ----------------------------
-- Table structure for `project_milestones`
-- ----------------------------
DROP TABLE IF EXISTS `project_milestones`;
CREATE TABLE `project_milestones` (
  `id_milestone` int(11) NOT NULL AUTO_INCREMENT,
  `milestone_name` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `milestone_start_date` datetime NOT NULL,
  `milestone_end_date` datetime NOT NULL,
  `milestone_status` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_milestone`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_milestones
-- ----------------------------
INSERT INTO `project_milestones` VALUES ('1', 'Phase 1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ontrack', '2016-06-23 16:43:50');
INSERT INTO `project_milestones` VALUES ('2', 'Phase 1', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ontrack', '2016-06-23 16:45:46');
INSERT INTO `project_milestones` VALUES ('3', 'phase 1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'concern', '2016-06-23 19:41:13');
INSERT INTO `project_milestones` VALUES ('4', 'xzcxzc', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'concern', '2016-06-24 20:28:06');

-- ----------------------------
-- Table structure for `project_module`
-- ----------------------------
DROP TABLE IF EXISTS `project_module`;
CREATE TABLE `project_module` (
  `id_project_module` int(45) NOT NULL AUTO_INCREMENT,
  `project_id` int(45) DEFAULT NULL,
  `module_name` varchar(45) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_project_module`),
  KEY `project_id` (`project_id`) USING BTREE,
  CONSTRAINT `project_module_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id_project`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_module
-- ----------------------------
INSERT INTO `project_module` VALUES ('1', '1', 'Campaign', '2016-06-23 16:46:10');
INSERT INTO `project_module` VALUES ('2', '1', 'Data Migration', '2016-06-23 16:46:10');
INSERT INTO `project_module` VALUES ('3', '2', 'Campaign', '2016-06-23 16:46:10');

-- ----------------------------
-- Table structure for `project_open_items`
-- ----------------------------
DROP TABLE IF EXISTS `project_open_items`;
CREATE TABLE `project_open_items` (
  `id_project_item` int(11) NOT NULL AUTO_INCREMENT,
  `open_item_description` text,
  `project_id` int(11) DEFAULT NULL,
  `open_item_user_id` int(11) DEFAULT NULL,
  `open_item_status` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_project_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_open_items
-- ----------------------------

-- ----------------------------
-- Table structure for `project_task`
-- ----------------------------
DROP TABLE IF EXISTS `project_task`;
CREATE TABLE `project_task` (
  `id_project_task` int(45) NOT NULL AUTO_INCREMENT,
  `project_id` int(45) DEFAULT NULL,
  `project_module_id` int(45) DEFAULT NULL,
  `task_name` text,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(225) NOT NULL DEFAULT 'new',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_project_task`),
  KEY `project_id` (`project_id`) USING BTREE,
  KEY `project_module_id` (`project_module_id`) USING BTREE,
  CONSTRAINT `project_task_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id_project`),
  CONSTRAINT `project_task_ibfk_2` FOREIGN KEY (`project_module_id`) REFERENCES `project_module` (`id_project_module`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_task
-- ----------------------------
INSERT INTO `project_task` VALUES ('1', '1', '1', 'Campaign Management', 'Campaign Management', 'completed', '2016-06-23 16:46:10', '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-23 00:00:00');
INSERT INTO `project_task` VALUES ('2', '1', '2', 'Data Migration', 'Data migration for master data', 'completed', '2016-06-23 16:46:10', '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-23 00:00:00');
INSERT INTO `project_task` VALUES ('3', '2', '3', 'Send Grid Integration 2', 'Data migration for master data', 'new', '2016-06-23 16:46:10', '2016-06-20 00:00:00', '2016-06-24 00:00:00', null);
INSERT INTO `project_task` VALUES ('4', '2', '3', 'Send Grid Integration 1', 'Data migration for master data', 'new', '2016-06-23 16:46:10', '2016-06-20 00:00:00', '2016-06-24 00:00:00', null);
INSERT INTO `project_task` VALUES ('5', '2', '3', 'Send Grid Integration 3', 'Data migration for master data', 'new', '2016-06-23 16:46:10', '2016-06-20 00:00:00', '2016-06-24 00:00:00', null);
INSERT INTO `project_task` VALUES ('6', '2', '3', 'Send Grid Integration 4', 'Data migration for master data', 'new', '2016-06-23 16:46:11', '2016-06-20 00:00:00', '2016-06-24 00:00:00', null);
INSERT INTO `project_task` VALUES ('7', '2', '3', 'Send Grid Integration 5', 'Data migration for master data', 'new', '2016-06-23 16:46:11', '2016-06-20 00:00:00', '2016-06-24 00:00:00', null);

-- ----------------------------
-- Table structure for `request_more_time`
-- ----------------------------
DROP TABLE IF EXISTS `request_more_time`;
CREATE TABLE `request_more_time` (
  `Id_request_more_time` int(11) NOT NULL AUTO_INCREMENT,
  `task_flow_id` int(11) DEFAULT NULL,
  `project_task_id` int(11) NOT NULL,
  `duration` varchar(10) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id_request_more_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of request_more_time
-- ----------------------------

-- ----------------------------
-- Table structure for `sprint`
-- ----------------------------
DROP TABLE IF EXISTS `sprint`;
CREATE TABLE `sprint` (
  `id_sprint` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT 'pending',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sprint`),
  KEY `project_id` (`project_id`) USING BTREE,
  CONSTRAINT `sprint_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id_project`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sprint
-- ----------------------------

-- ----------------------------
-- Table structure for `task_flow`
-- ----------------------------
DROP TABLE IF EXISTS `task_flow`;
CREATE TABLE `task_flow` (
  `id_task_flow` int(11) NOT NULL AUTO_INCREMENT,
  `project_task_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `estimated_time` time DEFAULT NULL,
  `additional_time` time DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `actual_end_date` datetime DEFAULT NULL,
  `task_status` varchar(255) DEFAULT 'new',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_task_flow`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_flow
-- ----------------------------
INSERT INTO `task_flow` VALUES ('1', '1', '3', '10:00:00', null, '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-25 00:00:00', 'progress', '2016-06-23 16:46:10');
INSERT INTO `task_flow` VALUES ('2', '2', '3', '20:00:00', null, '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-24 00:00:00', 'progress', '2016-06-23 16:46:10');
INSERT INTO `task_flow` VALUES ('3', '3', '3', '06:00:00', null, '2016-06-20 00:00:00', '2016-06-27 00:00:00', '2016-06-27 00:00:00', 'progress', '2016-06-23 16:46:10');
INSERT INTO `task_flow` VALUES ('4', '4', '3', '05:00:00', null, '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-24 00:00:00', 'progress', '2016-06-23 16:46:10');
INSERT INTO `task_flow` VALUES ('5', '5', '3', '05:30:00', null, '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-24 00:00:00', 'new', '2016-06-23 16:46:10');
INSERT INTO `task_flow` VALUES ('6', '6', '3', '06:30:00', null, '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-24 00:00:00', 'new', '2016-06-23 16:46:11');
INSERT INTO `task_flow` VALUES ('7', '7', '3', '07:30:00', null, '2016-06-20 00:00:00', '2016-06-24 00:00:00', '2016-06-24 00:00:00', 'new', '2016-06-23 16:46:11');

-- ----------------------------
-- Table structure for `task_member`
-- ----------------------------
DROP TABLE IF EXISTS `task_member`;
CREATE TABLE `task_member` (
  `id_task_member` int(11) NOT NULL AUTO_INCREMENT,
  `task_flow_id` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `allowted_time` time NOT NULL,
  `additional_time` time NOT NULL,
  `task_status` varchar(255) NOT NULL DEFAULT 'new',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_task_member`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_member
-- ----------------------------
INSERT INTO `task_member` VALUES ('1', '1', '25', '08:00:00', '00:00:00', 'completed', '2016-06-23 16:51:05');
INSERT INTO `task_member` VALUES ('3', '3', '25', '04:30:00', '00:00:00', 'progress', '2016-06-23 16:51:55');
INSERT INTO `task_member` VALUES ('4', '1', '27', '02:00:00', '00:00:00', 'new', '2016-06-23 16:53:24');
INSERT INTO `task_member` VALUES ('5', '4', '25', '02:00:00', '00:00:00', 'new', '2016-06-23 16:54:40');
INSERT INTO `task_member` VALUES ('6', '6', '25', '00:00:00', '00:00:00', 'new', '2016-06-23 16:56:35');
INSERT INTO `task_member` VALUES ('7', '2', '27', '02:00:00', '00:00:00', 'completed', '2016-06-23 18:34:24');
INSERT INTO `task_member` VALUES ('8', '2', '28', '03:00:00', '00:00:00', 'completed', '2016-06-23 18:34:41');
INSERT INTO `task_member` VALUES ('9', '2', '25', '05:00:00', '00:00:00', 'progress', '2016-06-24 10:21:03');
INSERT INTO `task_member` VALUES ('10', '4', '27', '03:00:00', '00:00:00', 'progress', '2016-06-24 15:01:06');
INSERT INTO `task_member` VALUES ('11', '6', '29', '05:00:00', '00:00:00', 'new', '2016-06-24 16:07:50');
INSERT INTO `task_member` VALUES ('12', '2', '29', '03:00:00', '00:00:00', 'new', '2016-06-24 16:08:31');

-- ----------------------------
-- Table structure for `task_time_log`
-- ----------------------------
DROP TABLE IF EXISTS `task_time_log`;
CREATE TABLE `task_time_log` (
  `id_task_time_log` int(11) NOT NULL AUTO_INCREMENT,
  `task_workflow_id` int(11) DEFAULT NULL,
  `day_id` int(11) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `date` datetime DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `project_id` int(45) DEFAULT NULL,
  `task_flow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_task_time_log`),
  KEY `day_id` (`day_id`) USING BTREE,
  CONSTRAINT `task_time_log_ibfk_1` FOREIGN KEY (`day_id`) REFERENCES `day` (`id_day`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_time_log
-- ----------------------------

-- ----------------------------
-- Table structure for `task_type`
-- ----------------------------
DROP TABLE IF EXISTS `task_type`;
CREATE TABLE `task_type` (
  `id_task_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `task_type_key` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_task_type`),
  KEY `product_type` (`product_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_type
-- ----------------------------
INSERT INTO `task_type` VALUES ('1', 'R&D', 'billable', '3', 'task', '2016-06-22 14:53:56');
INSERT INTO `task_type` VALUES ('2', 'Testing', 'billable', '6', 'task', '2016-06-22 14:53:59');
INSERT INTO `task_type` VALUES ('3', 'Ux-Design', 'billable', '1', 'task', '2016-06-22 14:53:59');
INSERT INTO `task_type` VALUES ('4', 'Development / Code', 'billable', '3', 'task', '2016-06-22 14:54:00');
INSERT INTO `task_type` VALUES ('5', 'Review', 'non-billable', '3', 'task', '2016-06-22 21:14:27');
INSERT INTO `task_type` VALUES ('6', 'Ui-Design', 'billable', '2', 'task', '2016-06-22 14:54:05');
INSERT INTO `task_type` VALUES ('7', 'stanup-meating', 'other', '0', 'meeting', '2016-06-23 18:48:19');
INSERT INTO `task_type` VALUES ('8', 'tea-break', 'other', '0', 'break', '2016-06-23 18:48:22');
INSERT INTO `task_type` VALUES ('9', 'application-requirement', 'other', '0', 'call', '2016-06-23 18:48:25');
INSERT INTO `task_type` VALUES ('10', 'lunch_break', 'other', '0', 'break', '2016-06-23 18:48:25');
INSERT INTO `task_type` VALUES ('11', 'permisssion', 'other', '0', 'break', '2016-06-23 18:48:26');
INSERT INTO `task_type` VALUES ('12', 'general-meeting', 'other', '0', 'meeting', '2016-06-23 18:48:27');
INSERT INTO `task_type` VALUES ('13', 'bug-call', 'other', '0', 'call', '2016-06-23 18:48:28');

-- ----------------------------
-- Table structure for `task_usecase`
-- ----------------------------
DROP TABLE IF EXISTS `task_usecase`;
CREATE TABLE `task_usecase` (
  `id_task_usecase` int(11) NOT NULL AUTO_INCREMENT,
  `project_task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `task_usecase` mediumtext,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_task_usecase`),
  KEY `project_task_id` (`project_task_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `task_usecase_ibfk_1` FOREIGN KEY (`project_task_id`) REFERENCES `project_task` (`id_project_task`),
  CONSTRAINT `task_usecase_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_usecase
-- ----------------------------
INSERT INTO `task_usecase` VALUES ('1', '1', '25', 'this is for testing.........<br>', '2016-06-23 17:01:14');
INSERT INTO `task_usecase` VALUES ('2', '2', '25', 'cvbcvbvc', '2016-06-23 17:37:08');
INSERT INTO `task_usecase` VALUES ('3', '2', '25', 'sdfsdsdfsdfxzc', '2016-06-23 17:54:10');
INSERT INTO `task_usecase` VALUES ('4', '2', '27', '<b style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\"><a href=\"https://en.wikipedia.org/wiki/Len_Hutton\" style=\"color: rgb(11, 0, 128); background-image: none;\" data-original-title=\"Len Hutton\">Len Hutton</a></b><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;(23 June 1916 – 6 September 1990) was a&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Test_cricket\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Test cricket\">Test cricketer</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;who played for</span><a href=\"https://en.wikipedia.org/wiki/Yorkshire_County_Cricket_Club\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Yorkshire County Cricket Club\">Yorkshire</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;and&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/England_national_cricket_team\" class=\"mw-redirect\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"England national cricket team\">England</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;as an&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Batting_order_(cricket)#Opening_batsmen\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Batting order (cricket)\">opening batsman</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">. Marked out as a potential star from his teenage years, Hutton made his debut for Yorkshire in 1934 and by 1937 was playing for England. He set a record in 1938 for the highest individual innings in a Test match, scoring 364&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Run_(cricket)\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Run (cricket)\">runs</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;against Australia, a milestone that stood for nearly 20 years. During the Second World War, he received a serious arm injury from which he never fully recovered. In 1946, he assumed a role as the mainstay of England\'s batting; the team depended greatly on his&nbsp;</span>', '2016-06-23 18:39:40');
INSERT INTO `task_usecase` VALUES ('5', '2', '28', '<b style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\"><a href=\"https://en.wikipedia.org/wiki/Len_Hutton\" style=\"color: rgb(11, 0, 128); background-image: none;\" data-original-title=\"Len Hutton\">Len Hutton</a></b><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;(23 June 1916 – 6 September 1990) was a&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Test_cricket\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Test cricket\">Test cricketer</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;who played for</span><a href=\"https://en.wikipedia.org/wiki/Yorkshire_County_Cricket_Club\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Yorkshire County Cricket Club\">Yorkshire</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;and&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/England_national_cricket_team\" class=\"mw-redirect\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"England national cricket team\">England</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;as an&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Batting_order_(cricket)#Opening_batsmen\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Batting order (cricket)\">opening batsman</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">. Marked out as a potential star from his teenage years, Hutton made his debut for Yorkshire in 1934 and by 1937 was playing for England. He set a record in 1938 for the highest individual innings in a Test match, scoring 364&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Run_(cricket)\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-color: rgb(245, 255, 250);\" data-original-title=\"Run (cricket)\">runs</a><span style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-color: rgb(245, 255, 250);\">&nbsp;against Australia, a milestone that stood for nearly 20 years. During the Second World War, he received a serious arm injury from which he never fully recovered. In 1946, he assumed a role as the mainstay of England\'s batting; the team depended greatly on his&nbsp;</span>', '2016-06-23 18:44:42');
INSERT INTO `task_usecase` VALUES ('6', '1', '25', 'ghjghj', '2016-06-23 19:56:24');
INSERT INTO `task_usecase` VALUES ('7', '1', '25', 'bvnvbnbv', '2016-06-23 19:56:39');
INSERT INTO `task_usecase` VALUES ('8', '1', '25', 'vbnvbnv', '2016-06-23 19:56:58');
INSERT INTO `task_usecase` VALUES ('9', '4', '27', 'Test', '2016-06-24 19:04:36');

-- ----------------------------
-- Table structure for `task_week_flow`
-- ----------------------------
DROP TABLE IF EXISTS `task_week_flow`;
CREATE TABLE `task_week_flow` (
  `id_task_week_flow` int(11) NOT NULL AUTO_INCREMENT,
  `task_flow_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `time` time DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_task_week_flow`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_week_flow
-- ----------------------------
INSERT INTO `task_week_flow` VALUES ('1', '1', '4', '25', '2016-06-23 00:00:00', '03:00:00', '2016-06-23 16:51:05');
INSERT INTO `task_week_flow` VALUES ('3', '3', '4', '25', '2016-06-23 00:00:00', '02:00:00', '2016-06-23 16:51:55');
INSERT INTO `task_week_flow` VALUES ('4', '1', '1', '25', '2016-06-27 00:00:00', '03:00:00', '2016-06-23 16:53:03');
INSERT INTO `task_week_flow` VALUES ('5', '1', '1', '27', '2016-06-27 00:00:00', '02:00:00', '2016-06-23 16:53:24');
INSERT INTO `task_week_flow` VALUES ('6', '4', '1', '25', '2016-06-27 00:00:00', '02:00:00', '2016-06-23 16:54:40');
INSERT INTO `task_week_flow` VALUES ('7', '1', '5', '25', '2016-06-24 00:00:00', '02:00:00', '2016-06-23 16:55:28');
INSERT INTO `task_week_flow` VALUES ('10', '3', '1', '25', '2016-06-27 00:00:00', '02:30:00', '2016-06-23 16:58:33');
INSERT INTO `task_week_flow` VALUES ('11', '2', '4', '27', '2016-06-23 00:00:00', '02:00:00', '2016-06-23 18:34:24');
INSERT INTO `task_week_flow` VALUES ('12', '2', '4', '28', '2016-06-23 00:00:00', '03:00:00', '2016-06-23 18:34:41');
INSERT INTO `task_week_flow` VALUES ('13', '2', '5', '25', '2016-06-24 00:00:00', '05:00:00', '2016-06-24 10:21:03');
INSERT INTO `task_week_flow` VALUES ('14', '4', '5', '27', '2016-06-24 00:00:00', '03:00:00', '2016-06-24 15:01:06');
INSERT INTO `task_week_flow` VALUES ('15', '6', '5', '29', '2016-06-24 00:00:00', '05:00:00', '2016-06-24 16:07:50');
INSERT INTO `task_week_flow` VALUES ('16', '2', '5', '29', '2016-06-24 00:00:00', '03:00:00', '2016-06-24 16:08:31');

-- ----------------------------
-- Table structure for `task_workflow`
-- ----------------------------
DROP TABLE IF EXISTS `task_workflow`;
CREATE TABLE `task_workflow` (
  `id_task_workflow` int(11) NOT NULL AUTO_INCREMENT,
  `project_task_id` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `assigned_by` int(11) DEFAULT NULL,
  `estimated_time` varchar(255) DEFAULT NULL,
  `additional_time` time NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  PRIMARY KEY (`id_task_workflow`),
  KEY `assigned_to` (`assigned_to`) USING BTREE,
  KEY `project_task_id` (`project_task_id`) USING BTREE,
  CONSTRAINT `task_workflow_ibfk_1` FOREIGN KEY (`assigned_to`) REFERENCES `user` (`id_user`),
  CONSTRAINT `task_workflow_ibfk_2` FOREIGN KEY (`project_task_id`) REFERENCES `project_task` (`id_project_task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_workflow
-- ----------------------------

-- ----------------------------
-- Table structure for `timeline`
-- ----------------------------
DROP TABLE IF EXISTS `timeline`;
CREATE TABLE `timeline` (
  `id_timeline` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `module_type` varchar(255) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `description` text,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_timeline`),
  KEY `created_by` (`created_by`) USING BTREE,
  CONSTRAINT `timeline_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of timeline
-- ----------------------------
INSERT INTO `timeline` VALUES ('1', '20', 'project', '1', 'Qlana Project added', '2016-06-23 16:43:02');
INSERT INTO `timeline` VALUES ('2', '20', 'project', '2', 'Miles Project added', '2016-06-23 16:44:57');
INSERT INTO `timeline` VALUES ('3', '25', 'task', '1', 'Use case added', '2016-06-23 17:01:14');
INSERT INTO `timeline` VALUES ('4', '25', 'task', '2', 'Use case added', '2016-06-23 17:37:08');
INSERT INTO `timeline` VALUES ('5', '25', 'task', '2', 'Use case added', '2016-06-23 17:54:10');
INSERT INTO `timeline` VALUES ('6', '27', 'task', '2', 'Use case added', '2016-06-23 18:39:40');
INSERT INTO `timeline` VALUES ('7', '28', 'task', '2', 'Use case added', '2016-06-23 18:44:42');
INSERT INTO `timeline` VALUES ('8', '20', 'project', '1', 'Qlana Project updated', '2016-06-23 19:40:36');
INSERT INTO `timeline` VALUES ('9', '20', 'project', '2', 'Miles Project updated', '2016-06-23 19:42:10');
INSERT INTO `timeline` VALUES ('10', '25', 'task', '1', 'Use case added', '2016-06-23 19:56:24');
INSERT INTO `timeline` VALUES ('11', '25', 'task', '1', 'Use case added', '2016-06-23 19:56:39');
INSERT INTO `timeline` VALUES ('12', '25', 'task', '1', 'Use case added', '2016-06-23 19:56:58');
INSERT INTO `timeline` VALUES ('13', '27', 'task', '4', 'Use case added', '2016-06-24 19:04:36');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `gmail_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `employee_id` varchar(10) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `user_image` text,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `job_role_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `department_id` int(11) DEFAULT NULL,
  `is_lead` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`),
  KEY `user_type_id` (`user_type_id`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`id_user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '3', '110567892914406443418', 'basha', 'shaik', '17812', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'bhasha.s@thresholdsoft.com', '8341532551', '3', '1', null, '3', '0');
INSERT INTO `user` VALUES ('2', '3', '100967842973498784356', 'naresh', 'gurrala', '17812', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'naresh.g@thresholdsoft.com', '1231231231', '4', '1', null, '3', '0');
INSERT INTO `user` VALUES ('3', '3', '108703092988194400412', 'Rakesh', 'Tati', '17812', 'male', 'https://lh6.googleusercontent.com/-k6STKc2PSRA/AAAAAAAAAAI/AAAAAAAAAEo/OHoUztqf5c4/photo.jpg?sz=50', 'rakesh.t@thresholdsoft.com', '123456445', '3', '1', '2016-05-03 21:37:10', '3', '0');
INSERT INTO `user` VALUES ('4', '3', '103560276311327505127', 'murali', 't', '17812', 'male', 'https://lh5.googleusercontent.com/-w0PEZA8yVo4/AAAAAAAAAAI/AAAAAAAAADg/aFEmWvQDJ-4/photo.jpg?sz=50', 'murali.t@thresholdsoft.com', '', '3', '1', '2016-05-03 23:29:38', '0', '0');
INSERT INTO `user` VALUES ('5', '3', '115714982831432455022', 'ramesh', 'babu', '17812', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'ramesh.ch@thresholdsoft.com', null, null, '1', '2016-05-10 21:30:31', '0', '0');
INSERT INTO `user` VALUES ('6', '2', null, 'vinay', null, '17812', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'vinay.n@thresholdsoft.com', '1234567895', '3', '0', '2016-05-12 15:29:03', '6', '0');
INSERT INTO `user` VALUES ('7', '3', '108817487832003383121', 'Mohan', 'babu', '17812', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'mohanbabu.p@thresholdsoft.com', '9581565753', '5', '1', '2016-05-12 16:42:16', '3', '1');
INSERT INTO `user` VALUES ('8', '3', '109456565822586574544', 'manasa', 't', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'manasa.t@thresholdsoft.com', null, null, '1', '2016-05-13 15:49:09', '0', '0');
INSERT INTO `user` VALUES ('9', '3', '111054576070239197364', 'abhilash', 'nair', '17821', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'abhilash.r@thresholdsoft.com', '9652429394', '4', '1', '2016-05-16 18:13:40', '3', '0');
INSERT INTO `user` VALUES ('10', '3', '117056995650650533159', 'prasad', 'd', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'prasad.d@thresholdsoft.com', null, null, '1', '2016-05-17 12:15:31', '0', '0');
INSERT INTO `user` VALUES ('11', '3', '112238025800350958180', 'Mahesh', 'J', '17812', 'male', 'https://lh3.googleusercontent.com/-8_nIA8wJs5g/AAAAAAAAAAI/AAAAAAAAAAw/x2NAsI4pzu4/photo.jpg?sz=50', 'Mahesh.J@thresholdsoft.com', '9912780715', '3', '1', '2016-05-17 12:16:06', '0', '0');
INSERT INTO `user` VALUES ('12', '3', '108485150653629358655', 'Adimoolam', 'Venu', '17812', 'male', 'https://lh4.googleusercontent.com/-_OxRgeX_W6U/AAAAAAAAAAI/AAAAAAAAAFE/4XEaq8Yk9-4/photo.jpg?sz=50', 'venu.a@thresholdsoft.com', '', '0', '1', '2016-05-17 12:17:00', '0', '0');
INSERT INTO `user` VALUES ('13', '3', '114954609365236711351', 'nanibabu', 'p', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'nanibabu.p@thresholdsoft.com', null, null, '1', '2016-05-17 12:17:31', '0', '0');
INSERT INTO `user` VALUES ('14', '3', '101603715843859402269', 'ramakrishna', 's', '17812', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'ramakrishna.s@thresholdsoft.com', '', '0', '1', '2016-05-17 12:17:53', '0', '0');
INSERT INTO `user` VALUES ('15', '2', '100249859465174640981', 'ravi', 'kumar', '17812', null, 'https://lh5.googleusercontent.com/-0JMdH5XZcaE/AAAAAAAAAAI/AAAAAAAAAC4/dRqWeBTxX8w/photo.jpg?sz=50', 'ravikumar.p@thresholdsoft.com', null, null, '1', '2016-05-17 12:18:44', '0', '0');
INSERT INTO `user` VALUES ('16', '3', '105495204113838407944', 'venu', 'p', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'venu.p@thresholdsoft.com', null, null, '1', '2016-05-17 12:21:57', '8', '0');
INSERT INTO `user` VALUES ('17', '3', '111357403719374249173', 'raja', 'p', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'raja.p@thresholdsoft.com', null, null, '0', '2016-05-17 12:22:45', '2', '0');
INSERT INTO `user` VALUES ('18', '3', '102291705887879207347', 'sundeepthi', 'm', '17821', null, 'https://lh6.googleusercontent.com/-GsQKpKoYhtM/AAAAAAAAAAI/AAAAAAAAAAA/NUTiuA3u1Fo/photo.jpg?sz=50', 'sundeepthi.m@thresholdsoft.com', null, null, '1', '2016-05-17 12:35:33', '0', '0');
INSERT INTO `user` VALUES ('19', '3', '109798411939230916037', 'UX', 'lead', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'uxlead@thresholdsoft.com', null, null, '1', '2016-05-19 14:54:17', '1', '1');
INSERT INTO `user` VALUES ('20', '2', '104370347112601839304', 'project', 'manager', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'projectmanager@thresholdsoft.com', null, null, '1', '2016-05-19 15:03:07', '0', '0');
INSERT INTO `user` VALUES ('21', '3', '100477690086380094803', 'ux', 'dev', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'uxdev1@thresholdsoft.com', null, null, '1', '2016-05-19 15:05:58', '1', '0');
INSERT INTO `user` VALUES ('22', '3', '115836765780936645402', 'ui', 'lead', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'uilead@thresholdsoft.com', null, null, '1', '2016-05-19 15:07:32', '2', '0');
INSERT INTO `user` VALUES ('23', '3', '113884239898187936801', 'ui', 'dev', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'uidev1@thresholdsoft.com', null, null, '1', '2016-05-19 15:08:56', '2', '0');
INSERT INTO `user` VALUES ('24', '3', '105691870916356329188', 'php', 'lead', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'phplead@thresholdsoft.com', null, null, '1', '2016-05-19 15:09:56', '3', '1');
INSERT INTO `user` VALUES ('25', '3', '105457577879716475603', 'php', 'dev', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'phpdev1@thresholdsoft.com', null, null, '1', '2016-05-19 15:11:44', '3', '0');
INSERT INTO `user` VALUES ('26', '3', '101687141283895275959', 'Testing', 'lead', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'testlead@thresholdsoft.com', '1234567890', null, '1', '2016-05-19 15:12:41', '6', '1');
INSERT INTO `user` VALUES ('27', '3', '112890663854332299712', 'php', 'phpdev2', '17821', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'phpdev2@thresholdsoft.com', '7984654113', null, '1', '2016-05-23 10:34:53', '3', '0');
INSERT INTO `user` VALUES ('28', '3', '112116654485915313060', 'php', 'phpdev3', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'phpdev3@thresholdsoft.com', null, null, '1', '2016-05-23 10:35:22', '3', '0');
INSERT INTO `user` VALUES ('29', '3', '105669777025208253330', 'php', 'phpdev4', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'phpdev4@thresholdsoft.com', null, null, '1', '2016-05-23 10:35:51', '3', '0');
INSERT INTO `user` VALUES ('30', '3', '104151014109537764422', 'php', 'phpdev5', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'phpdev5@thresholdsoft.com', null, null, '1', '2016-05-23 10:36:33', '3', '0');
INSERT INTO `user` VALUES ('31', '3', '100949440436770385629', 'teste', 'testeng1', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'testeng1@thresholdsoft.com', null, null, '1', '2016-05-23 10:36:52', '6', '0');
INSERT INTO `user` VALUES ('32', '3', '106704141347911552060', 'teste', 'testeng2', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'testeng2@thresholdsoft.com', null, null, '1', '2016-05-23 10:37:09', '6', '0');
INSERT INTO `user` VALUES ('33', '3', '103369858289751822256', 'teste', 'testeng3', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'testeng3@thresholdsoft.com', null, null, '1', '2016-05-23 10:37:27', '6', '0');
INSERT INTO `user` VALUES ('34', '3', '105924145324800160266', 'ui', 'uidev', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'uidev2@thresholdsoft.com', null, null, '1', '2016-05-23 10:38:11', '2', '0');
INSERT INTO `user` VALUES ('35', '3', '112014270758939297086', 'ux', 'uxdev2', '17812', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'uxdev2@thresholdsoft.com', null, null, '1', '2016-05-23 10:38:30', '1', '0');
INSERT INTO `user` VALUES ('36', '3', '112014270758939297086', 'ux', 'nagaraju.c', '17821', null, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'nagaraju.c@thresholdsoft.com', null, null, '1', '2016-06-10 15:42:09', '1', '1');

-- ----------------------------
-- Table structure for `user_checklist`
-- ----------------------------
DROP TABLE IF EXISTS `user_checklist`;
CREATE TABLE `user_checklist` (
  `id_user_checklist` int(11) NOT NULL AUTO_INCREMENT,
  `checklist_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user_checklist`),
  KEY `checklist_id` (`checklist_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `user_checklist_ibfk_1` FOREIGN KEY (`checklist_id`) REFERENCES `department_checklist` (`id_checklist`),
  CONSTRAINT `user_checklist_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_checklist
-- ----------------------------

-- ----------------------------
-- Table structure for `user_type`
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `id_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES ('1', 'Super admin');
INSERT INTO `user_type` VALUES ('2', 'Admin');
INSERT INTO `user_type` VALUES ('3', 'user');
