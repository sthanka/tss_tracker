<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Project_modal');
        $this->load->model('Report_model');
        $this->load->model('User_model');
        $this->load->model('Holiday_modal');
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
    }

    public function index()
    {

        if($this->session->userdata('user_id'))
        {
            $data['content'] = 'project/index';
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        $this->load->view('partials/landing',$data);
        }
        else{
            redirect(WEB_BASE_URL);
        }

    }
    public function project_list()
    {
        $params = $this->input->get();
        if($this->session->userdata('user_id'))
        {
            $projectList = $this->Project_modal->getProjectsGrid($params);
            //$projectList = $this->Project_modal->getProjects();
            echo json_encode($projectList);
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }
    public function get_projects()
    {
        $projectList = $this->Project_modal->getProjects();
        echo json_encode($projectList);
    }

    public function task_list()
    {
        $params = $this->input->get();
        if($this->session->userdata('user_id'))
        {
            echo json_encode($this->Project_modal->getTaskGrid($params,'list'));
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function events()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo '<pre>'; print_r($data); exit;
        $taskList = $this->Project_modal->getTaskList($data);
        //echo '<pre>'; print_r(json_encode($taskList)); exit;
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$taskList));
    }
    public function loadModals($task_id)
    {
        //echo '<pre>'; print_r($task_id); exit;
        $data['task_id'] = $task_id;
        $data['task_details'] = $this->Project_modal->getTaskDetails(array('id_project_task' => $task_id,'user_id' => $this->session->userdata('user_id'),'department_id' => $this->session->userdata('department_id')));
        $task_time_details = $this->Project_modal->getTaskTimeDetailsByUserId(array('project_task_id' => $task_id,'user_id' => $this->session->userdata('user_id')));
        $data['task_details'][0]['allotted_time'] = $task_time_details[0]['allowted_time'];
        $data['task_details'][0]['actual_time'] = $task_time_details[0]['actual_time'];
        $data['task_details'][0]['remain_time'] = $task_time_details[0]['remain_time'];
        $data['task_details'][0]['remain_seconds'] = $task_time_details[0]['remain_seconds'];
        $data['sub_task'] = $this->Project_modal->getSubTaskByParent($task_id);
        $data['use_cases'] = $this->Project_modal->getTaskUseCase(array('parent_id' => $task_id));
        $data['user_use_cases'] = $this->Project_modal->getTaskUseCase(array('parent_id' => $task_id,'user_id' => $this->session->userdata('user_id')));
        //echo "<pre>"; print_r($data['use_cases']); exit;
        $data['attachment'] = $this->Project_modal->getTaskAttachments(array('parent_id' => $task_id));
        $data['user_attachment'] = $this->Project_modal->getTaskAttachments(array('parent_id' => $task_id,'user_id' => $this->session->userdata('user_id')));
        $data['time_line'] = $this->Project_modal->getTimeLine(array('module_id' => $task_id,'module_type' => 'task'));
        $data['touch_points'] = $this->Project_modal->getTouchPoints(array('parent_id' => $task_id,'department_id' => $this->session->userdata('department_id')));
//        $data['log_list'] = $this->Project_modal->getUserLogTime($task_id);
        $data['checkList'] = $this->Project_modal->getAllCheckList((array('project_task_id' => $task_id)));
        //$data['more_time_list'] = $this->Project_modal->getMoreTimeList($task_id);

        $data=$this->load->view('modals/taskDetails',$data, TRUE);
        $this->output->set_output($data);

    }

    public function loadCheckList($task_id)
    {
        $task_details = $this->Project_modal->getTaskDetails(array('id_project_task' => $task_id,'user_id' => $this->session->userdata('user_id'),'department_id' => $this->session->userdata('department_id')));
        $data['status'] = $task_details[0]['task_status'];
        $data['checklist'] = $this->Project_modal->getChecklist($task_id);
        $data['taskId'] = $task_id;
        $data=$this->load->view('modals/checkList',$data, TRUE);
        $this->output->set_output($data);
    }

    public function saveCheckListStatus()
    {
        $data = $this->input->post();
        $response = $this->Project_modal->saveCheckListStatus($data);
        if ($response === 'noid') {
            $result = array('status' => FALSE, 'error' => 'Invalid task', 'data' => '');
        } else if ($response) {
            $result = array('status' => true, 'message' => '', 'data' => []);
        }else{
            $result = array('status' => FALSE, 'error' => 'Invalid Data', 'data' => '');
        }
       $data['data'] = array_filter($data['data']);
        if(!empty($data['data']))
            $this->Project_modal->addTimeLine(array('created_by' => $this->session->userdata('user_id'),'module_type' => 'task','module_id' => $data['taskId'],'description' =>$this->session->userdata('username').' added checklist'));
        echo json_encode($result);
    }

    public function loadUserWork()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $user = explode(',',$data['user']);
        unset($data['user']);
        foreach($user as $k=>$userId){
            $data[$userId]['usersDetails'] = $this->User_model->getUser(['id_user'=>$userId]);
            $data[$userId]['usersDetails'][] = $this->User_model->getUserSprintWorkLoad($userId);

//            $data[$userId]['kpis'] = $this->User_model->getUserkpi($userId);
            $billable = $this->User_model->getUserkpi($userId);
            //echo "<pre>"; print_r($billable); exit;
            $totalHours = sec_to_time(time_to_sec($billable['billable']['duration']) + time_to_sec($billable['other']['duration']));
            $totalHours = explode(':', $totalHours);
            if(count($totalHours) > 0){
                $totalHours = $totalHours[0].':'.$totalHours[1];
            }else{
                $totalHours = '00:00';
            }
            $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
            $last_day_this_month  = date('Y-m-t');
            $this->load->model('Attendance_modal');
            $totalAttendance = $this->Attendance_modal->getAttendanceByDate(array('user_id' => $userId, 'start_date' => $first_day_this_month,  'end_date' => $last_day_this_month ) );
            if(count($totalAttendance) > 0){
                $totalAttendance = $totalAttendance[0]->duration;
            }else{
                $totalAttendance = '00:00:00';
            }
            /*$data[$userId]['kpis']['billable']['duration'] = $totalHours;
            $data[$userId]['kpis']['non-billable'] = sec_to_time(time_to_sec($totalAttendance) - time_to_sec($totalHours));
            $data[$userId]['kpis']['non-billable'] = explode(':', $data[$userId]['kpis']['non-billable']);*/
            $data[$userId]['kpis']['billable']['duration'] = $billable['billable']['duration'];
            $data[$userId]['kpis']['non-billable']['duration'] = $billable['non-billable']['duration'];
            //$data[$userId]['kpis']['non-billable'] = explode(':', $data[$userId]['kpis']['non-billable']);
            /*if(count($data[$userId]['kpis']['non-billable'])>0){
                $data[$userId]['kpis']['non-billable']['duration'] = str_replace('0-', '', $data[$userId]['kpis']['non-billable'][0].':'.$data[$userId]['kpis']['non-billable'][1]);
            }else{
                $data[$userId]['kpis']['non-billable']['duration'] = '00:00';
            }*/
            $data[$userId]['kpis']['other']['duration'] = $billable['other']['duration'];

            $data[$userId]['on_time_delivery'] = $this->User_model->getOnTimeDelivery($userId);
            //echo "<pre>"; print_r($data[$userId]['on_time_delivery']); exit;
            $user_projects = $this->Project_modal->getEmployeeProjects(array('user_id' => $userId));
            $user_projects_id = array_map(function($i){ return $i['project_id'];  },$user_projects);
            $data[$userId]['milestone_miss'] = $this->User_model->getMileStoneMiss(array('user_id' => $userId,'project_id' => $user_projects_id));
            $details = $this->User_model->allTaskStatus(['id_user'=>$userId]);
            $data[$userId]['taskStatus'] = $details[0];
            $data[$userId]['proCount'] = $details[1];
            $data[$userId]['days'] = $details[2];
            $data[$userId]['daysLogs']  = $ech_day_load = $details[3];
            $alloted = $actual = $f_work_load = 0;
            //echo "<pre>"; print_r($ech_day_load); exit;
            foreach($ech_day_load as $i){
                if(strtotime($i['date'])<=strtotime(date('Y-m-d'))){
                    $alloted = $alloted + time_to_sec($i['allowted_time']);
                    $actual = $actual + time_to_sec($i['actual_time']);
                }

                $f_work_load = $f_work_load + time_to_sec($i['allowted_time']);

            }
            if($actual == 0 || $alloted==0){
                $data[$userId]['percentage'] = 0;
            }else{
                $data[$userId]['percentage'] = round(($actual/$alloted)*100);
            }
            $data[$userId]['work_load'] = sec_to_time($f_work_load);
        }
        //echo "<pre>"; print_r($data); exit;
        $data=$this->load->view('modals/user-workload',array('records'=> $data), TRUE);
        $this->output->set_output($data);

    }
    public function add_project()
    {
        $project_id = '';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $id = $data['id_project'];
        unset($data['id_project']);
        $result = $this->Project_modal->checkProjectName($id,$data['project_name']);
        if(empty($result)){
            if($id == null && $id == '') {

                $project_id = $this->Project_modal->addProject($data);

                //save department

                //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'project',
                    'module_id' => $project_id,
                    'description' => $data['project_name'].' Project added'
                ));
                $mode = 'added';
            } else {
                $result = $this->Project_modal->updateProject($id, $data);
                $project_id = $id;
                    //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'project',
                    'module_id' => $result,
                    'description' => $data['project_name'].' Project updated'
                ));
                $mode = 'updated';
            }
            echo json_encode(array('status'=>true,'message'=>$mode,'data'=>$project_id));
        }else{
            echo json_encode(array('status'=>false,'message'=>'Project name exist'));
        }
    }
    public function add_project_dept_team(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $exist = $this->Project_modal->checkDepartmentExist($data);
        $data['estimation_percentage'] = 0;
        if($exist){
            echo json_encode(array('status' => false, 'error' => true, 'message' =>'Department already exist'));

        }else {
            /*if (!$this->Project_modal->checkTotalPercentage($data['estimation_percentage'], $data['project_id'])) {
                echo json_encode(array('status' => false, 'error' => true, 'message' => 'Exceeding 100%'));
            } else {*/
                $result = $this->Project_modal->addProjectDeptTeam($data);
                echo json_encode(array('status' => true, 'error' => false, 'data' => $result));
            //}

        }
    }
    public function addProjectMilestone(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $start_date = DateTime::createFromFormat('d/m/Y', $data['milestone_start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['milestone_end_date'])->format('Y-m-d');

        $data['milestone_start_date'] = $start_date;
        $data['milestone_end_date'] = $end_date;
//        echo '<pre>'; print_r($data); exit;
        $result = $this->Project_modal->addProjectMilestone($data);
        echo json_encode(array('status' => true, 'error' => false, 'data' => $result));
    }
    public function updateProjectMilestone(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $id = $data['milestone_id'];
        unset($data['milestone_id']);
        $start_date = DateTime::createFromFormat('d/m/Y', $data['milestone_start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['milestone_end_date'])->format('Y-m-d');

        $data['milestone_start_date'] = $start_date;
        $data['milestone_end_date'] = $end_date;

        $result = $this->Project_modal->updateProjectMilestone($data, $id);
        echo json_encode(array('status' => true, 'error' => false, 'data' => $result));
    }
    public function addProjectOpenItem(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $result = $this->Project_modal->addProjectOpenItem($data);
        echo json_encode(array('status' => true, 'error' => false, 'data' => $result));
    }
    public function updateProjectOpenItem(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $id = $data['opentask_id'];
        unset($data['opentask_id']);

        $result = $this->Project_modal->updateProjectOpenItem($data, $id);
        echo json_encode(array('status' => true, 'error' => false, 'data' => $result));
    }
    public function getDepartmentUsers($depId){
        $user = $this->Project_modal->getUser(array('department_id' => $depId));
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$user));
    }
    public function getUserByDepartmentProject(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $user = $this->Project_modal->getUserByDepartmentProject($data);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$user));
    }
    public function getProjectUsers($projectId){
            $user = $this->Project_modal->getProjectUsers($projectId);
            echo json_encode(array('status'=>true,'error'=>false,'data'=>$user));
        }

    public function save_project()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
        $id = '';
        $result = $this->Project_modal->checkProjectName($id,$data['project_name']);
        if(empty($result)){
            if($id == null && $id == '') {

                $project_id = $this->Project_modal->addProject($data);

                //save department

                //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'project',
                    'module_id' => $project_id,
                    'description' => $data['project_name'].' Project added'
                ));
            }
            else {
                $result = $this->Project_modal->updateProject($id, $data);

                //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'project',
                    'module_id' => $result,
                    'description' => $data['project_name'].' Project updated'
                ));
            }
            echo json_encode(array('status'=>true,'message'=>'','data'=>$result));
        }else{
            echo json_encode(array('status'=>false,'message'=>'Project name exist'));
        }


    }
    public function deleteOpenItem()
    {
        $itemId = $this->input->get('open_item_id');
        $openItem = $this->Project_modal->deleteOpenItem($itemId);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$openItem));
    }
    public function delete($id)
    {
        $projectList = $this->Project_modal->delete($id);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectList));
    }

    public function overview($id)
    {
        if($this->session->userdata('user_type_id')==3){
            redirect(WEB_BASE_URL.'index.php/project'); exit;
        }
        $project = $this->Project_modal->overview($id);
        $users = $this->User_model->getProjectUsers($id);
        $projectUsers = $this->Project_modal->getProjectUsers($id);
        $data['unassigned'] = $this->Project_modal->getUnassignedProjectTask(array('project_id' => $id, 'user_id' => $this->session->userdata('user_id')));
        $data['content'] = 'project/overview';
        $data['result'] = $project[0];
        $data['users'] = $users;
        $data['projectUsers'] = $projectUsers;
        $data['sprint'] = $this->Project_modal->getSprint(array('project_id' => $id));
        $data['project_id'] = $id;

        $this->load->view('partials/landing',$data);
    }
    public function projectUserDelete($id)
    {
        $projectList = $this->Project_modal->projectUserDelete($id);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectList));
    }

    public function addMember()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result); exit;
        }
        $result = $this->Project_modal->addMember($data);
        if($result == 0)
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'employee already exist'));
        else {
            echo json_encode(array('status' => true, 'error' => false, 'data' => $result));


        }
    }

    public function checkSprintName()
    {
        if($this->session->userdata('user_id'))
        {
            $data = $this->Project_modal->getSprint(array('name' => trim($_POST['name']), 'sprint_id_not' => $_POST['id_project'], 'project_id' => $_POST['project_id']));
            if(empty($data))
                echo json_encode(array('response'=>1,'data'=>''));
            else
                echo json_encode(array('response'=>0,'data'=>''));
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function addSprint()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $res = $this->Project_modal->getSprint(array('name' => trim($_POST['name']), 'sprint_id_not' => $_POST['id_sprint'], 'project_id' => $_POST['project_id']));

        if(!empty($res)){  echo json_encode(array('status'=>FALSE,'error'=>true,'message'=>'Name already exist')); exit; }

        /*if(strtotime(str_replace('/','-',$data['start_date']))==strtotime(str_replace('/','-',$data['end_date']))){
            echo json_encode(array('status'=>FALSE,'error'=>true,'message'=>'Start date and end date must not equal')); exit;
        }*/
        $start_date = date('Y-m-d', strtotime(str_replace('/','-',$data['start_date'])));
        $check_sprint = $this->Project_modal->getSprint(array('project_id' => $data['project_id'],'sprint_id_not' => $_POST['id_sprint'], 'week_day' => $start_date));
        //echo "<pre>"; print_r($check_sprint); exit;
        if(!empty($check_sprint)){  echo json_encode(array('status'=>FALSE,'error'=>true,'message'=>'Sprint already in start date week')); exit; }

        $sprint_data = array(
            'project_id' => $data['project_id'],
            'created_by' => $this->session->userdata('user_id'),
            'name' => $data['name'],
            'start_date' => date('Y-m-d', strtotime(str_replace('/','-',$data['start_date']))),
            'end_date' => date('Y-m-d', strtotime(str_replace('/','-',$data['end_date'])))
        );

        if(isset($_POST['id_sprint']) && $_POST['id_sprint']!='' && $_POST['id_sprint']!=0){
            $sprint_data = array(
                'id_sprint' => $_POST['id_sprint'],
                'name' => $data['name'],
                'start_date' => date('Y-m-d', strtotime(str_replace('/','-',$data['start_date']))),
                'end_date' => date('Y-m-d', strtotime(str_replace('/','-',$data['end_date'])))
            );
            $this->Project_modal->updateSprint($sprint_data);
        }
        else{
            $this->Project_modal->addSprint($sprint_data);
        }

        echo json_encode(array('status'=>TRUE,'error'=>true,'message'=>'')); exit;
    }

    public function getSprintAjax()
    {
        if($this->session->userdata('user_id'))
        {
            $sprint = $this->Project_modal->getSprint(array('id_sprint' => $_POST['id']));
            //echo "<pre>"; print_r($sprint); exit;
            $sprint = array(
                'name' => $sprint[0]['name'],
                'id_sprint' => $sprint[0]['id_sprint'],
                'start_date' => str_replace('-','/',date('d-m-Y',strtotime($sprint[0]['start_date']))),
                'end_date' => str_replace('-','/',date('d-m-Y',strtotime($sprint[0]['end_date'])))
            );

            echo json_encode(array('response' => 1,'data' => $sprint));
        }
    }

    public function taskDetails($task_id)
    {
        if($task_id=='' || $task_id==0)
            redirect(WEB_BASE_URL);
        $data['task_id'] = $task_id;
        $data['task_details'] = $this->Project_modal->getTaskDetails(array('id_project_task' => $task_id));

        $data['sub_task'] = $this->Project_modal->getSubTaskByParent($task_id);
        $data['use_cases'] = $this->Project_modal->getTaskUseCase(array('parent_id' => $task_id,'user_id' => $this->session->userdata('user_id')));
        //echo "<pre>"; print_r($data['use_cases']); exit;
        $data['attachment'] = $this->Project_modal->getTaskAttachments(array('parent_id' => $task_id,'user_id' => $this->session->userdata('user_id')));
        $data['time_line'] = $this->Project_modal->getTimeLine(array('module_id' => $task_id,'module_type' => 'task','user_id' => $this->session->userdata('user_id')));
        $data['touch_points'] = $this->Project_modal->getTouchPoints(array('parent_id' => $task_id));
        $data['content'] = 'project_task/task_details';
        //echo "<pre>"; print_r($data['attachment']); exit;
        $this->load->view('partials/landing',$data);
    }

    public function backLogs($project_id=''){

        $sprintList =  $this->Project_modal->getSprint(array('project_id' =>$project_id));
        $taskList =  $this->Project_modal->getProjectTask(array('project_id' =>$project_id,'parent_id'=>0));
        $sprintDetails =$backLogList= array();
        foreach($sprintList as $sprint){
            $obj = $sprint;
            $obj['task'] = array();
            foreach($taskList as $task){
                if($sprint['id_sprint']==$task['sprint_id']){
                    $taskObj = $task;
                    $taskObj['start_date'] = date("d/m/Y", strtotime($task['start_date']));
                    $taskObj['end_date'] = date("d/m/Y", strtotime($task['end_date']));
                    $obj['task'][] = $taskObj;
                }
            }
            $sprintDetails[] = $obj;
        }
        foreach($taskList as $task){
            if($task['sprint_id']==0){
                $taskObj = $task;
                $taskObj['start_date'] = date("d/m/Y", strtotime($task['start_date']));
                $taskObj['end_date'] = date("d/m/Y", strtotime($task['end_date']));
                $backLogList[] = $taskObj;
            }
        }
        $data['backLogList']=$backLogList;
        $data['sprintDetails']=$sprintDetails;
        $data['sprintList']=$sprintList;
        //echo "<pre>"; print_r($data); exit;
        $this->load->view('project/project_backlogs',$data);
    }

    public function taskSprintChange(){
        $data = $this->input->post();

        $this->db->where('id_project_task',$data['task_id']);
        $this->db->update('project_task', array('sprint_id'=>$data['sprint_id']));
        echo 1;
    }


    public function taskList()
    {
        $params = $this->input->get();
        $projectList = $this->Project_modal->taskListGrid($params);
        echo json_encode($projectList);
    }

    public function logTime()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['created_date_time']) && $data['created_date_time']!=''){
            $data['created_date_time'] = DateTime::createFromFormat('d/m/Y', $data['created_date_time'])->format('Y-m-d');
        }else{
            $data['created_date_time'] = date('Y-m-d');
        }

        /*$use_case = $this->Project_modal->getTaskUseCase(array('project_task_id' => $data['project_task_id'], 'user_id' => $data['user_id']));
        if(empty($use_case)){
            echo json_encode(array('status'=>FALSE,'error'=>false,'message'=>'Please write use case','data'=>[]));
            exit;
        }*/
        /*$totalChecklist = $this->Project_modal->getChecklist($data['project_task_id']);
        $checkList = $this->Project_modal->getTaskCheckList(array('task_id' => $data['project_task_id'], 'user_id' => $data['user_id']));
        if(count($totalChecklist) != count($checkList)){
            echo json_encode(array('status'=>FALSE,'error'=>false,'message'=>'Please select checklist','data'=>[]));
            exit;
        }*/
        /*Dis-allow future log time*/
        if( strtotime($data['created_date_time'].' '.$data['start_time']) > strtotime(date('d-m-Y H:i:s')) ||
            strtotime($data['created_date_time'].' '.$data['end_time']) > strtotime(date('d-m-Y H:i:s')) ){
            echo json_encode(array('status'=>FALSE,'message'=>'Future time not allowed','data'=>[]));
            exit;
        }

        $isHoliday = isWeekends(array('date' => $data['created_date_time']));
        if($isHoliday) {
            $approvedHolidayLog = $this->Project_modal->checkApprovedHoliday(array('date' => $data['created_date_time'], 'user_id' => $data['user_id']));
            //echo "<pre>"; print_r($approvedHolidayLog); exit;
            if ($approvedHolidayLog == 'declined' || $approvedHolidayLog == 'pending') {
                if ($approvedHolidayLog == 'declined') {
                    echo json_encode(array('status' => FALSE, 'message' => 'Work request is declined by Project Manager', 'data' => []));
                    exit;
                } else if ($approvedHolidayLog == 'pending') {
                    echo json_encode(array('status' => FALSE, 'message' => 'Work request is pending', 'data' => []));
                    exit;
                }
            } else if (!$approvedHolidayLog && $isHoliday) {
                echo json_encode(array('status' => FALSE, 'message' => 'Allotted date is an holiday', 'data' => []));
                exit;
            }
        }
        if( strtotime(date('d-m-Y',strtotime($data['created_date_time']))) < strtotime(date('d-m-Y')) ){
            $approvedHolidayLog = $this->Project_modal->checkApprovedHoliday(array('date' => $data['created_date_time'], 'user_id' => $data['user_id']));
            //echo "<pre>"; print_r($approvedHolidayLog); exit;
            if($approvedHolidayLog){
                if($approvedHolidayLog == 'declined' || $approvedHolidayLog == 'pending'){
                    if($approvedHolidayLog == 'declined'){
                        echo json_encode(array('status'=>FALSE,'message'=>'Work request is declined by Project Manager','data'=>[]));
                        exit;
                    }else if($approvedHolidayLog == 'pending'){
                        echo json_encode(array('status'=>FALSE,'message'=>'Work request is pending','data'=>[]));
                        exit;
                    }
                }else if(!$approvedHolidayLog && $isHoliday){
                    echo json_encode(array('status'=>FALSE,'message'=>'Allotted date is an holiday','data'=>[]));
                    exit;
                }
            }else{
                echo json_encode(array('status'=>FALSE,'message'=>'Please get approval from Project Manager for past dates','data'=>[]));
                exit;
            }
        }
        $logTime = $this->Project_modal->checkLogTime($data);
        if($logTime == 'timelogged'){
            echo json_encode(array('status'=>FALSE,'message'=>'Same log is already present','data'=>[]));
            exit;
        }
//        $checkUserAssignedToTask = $this->Project_modal->checkUserAssignedToTask($data);
        //if($checkUserAssignedToTask){
            $checkExtraTime = $this->Project_modal->checkExtraTime($data);

            if($checkExtraTime){
                $logTime = $this->Project_modal->logTime($data);
                echo json_encode(array('status'=>TRUE,'message'=>'','data'=>$logTime));
                exit;
            }else{
                echo json_encode(array('status'=>FALSE,'error'=>false,'message'=>'Allotted time should be less than logged time','data'=>[]));
                exit;
            }
        /*}else{
            echo json_encode(array('status'=>FALSE,'error'=>false,'message'=>'Task is not allotted for the selected date','data'=>[]));
            exit;
        }*/
    }
	public function logTimeOther()
    {
        $userError = [];
        $userErrorMsg = '';
        $logTime = '';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['created_date_time']) && $data['created_date_time']!=''){
            $data['created_date_time'] = DateTime::createFromFormat('d/m/Y', $data['created_date_time'])->format('Y-m-d');
        }else{
            $data['created_date_time'] = date('Y-m-d');
        }

        unset($data['date']);

        $isHoliday = isWeekends(array('date' => $data['created_date_time']));
        $approvedHolidayLog = $this->Project_modal->checkApprovedHoliday(array('date' => $data['created_date_time'], 'user_id' => $this->session->userdata('user_id')));
        if($approvedHolidayLog == 'declined' || $approvedHolidayLog == 'pending'){
            if($approvedHolidayLog == 'declined'){
                echo json_encode(array('status'=>FALSE,'message'=>'Holiday work request is declined by Admin','data'=>[]));
                exit;
            }else if($approvedHolidayLog == 'pending'){
                echo json_encode(array('status'=>FALSE,'message'=>'Holiday work request is pending','data'=>[]));
                exit;
            }
        }else if(!$approvedHolidayLog && $isHoliday){
            echo json_encode(array('status'=>FALSE,'message'=>'Allotted date is an holiday','data'=>[]));
            exit;
        }

        if(isset($data['user_id'])){
            if(!is_array($data['user_id'])){ $data['user_id'] = explode(',',$data['user_id']);}
            foreach($data['user_id'] as $item ){
                $storage = array('created_date_time'=> $data['created_date_time'],'comments'=>$data['comments'],'user_id'=>$item,'start_time'=>$data['start_time'],'end_time'=>$data['end_time'],'task_type'=>$data['task_type']);
                $logTimeStatus = $this->Project_modal->checkLogTime($storage);
                if($logTimeStatus == 'timelogged') {
                    array_push($userError, $item);
                }
            }

            if(count($userError)<=0){
                foreach($data['user_id'] as $item ){
                    $data = array('created_date_time'=> $data['created_date_time'],'comments'=>$data['comments'],'comments'=>$data['comments'],'user_id'=>$item,'start_time'=>$data['start_time'],'end_time'=>$data['end_time'],'task_type'=>$data['task_type']);
                    $logTime = $this->Project_modal->logTime($data);
                }
            }
        }else{
            $data['user_id'] =  $this->session->userdata('user_id');
            $logTimeStatus = $this->Project_modal->checkLogTime($data);
            if($logTimeStatus != 'timelogged') {
                $logTime = $this->Project_modal->logTime($data);
            }else{
                echo json_encode(array('status'=>FALSE,'message'=>'Same log is already present','data'=>[])); exit;
            }
        }
        if(is_array($userError) && count($userError)>0){
            $this->load->model('User_model');
            $queryData = [];
            foreach($userError as $k=>$v){
                $queryData['id_user'] = $v;
                $res = $this->User_model->getUser($queryData);
                if(is_array($res)){
                    $userErrorMsg .= $res[0]['email'] . ', ';
                }
            }
            $string = $userErrorMsg;
            $find_me = ',';

            $userErrorMsg = preg_replace('/'. $find_me .' $/', '', $string);
            $userErrorMsg .= ' has already logged';
            echo json_encode(array('status'=>false,'message'=>$userErrorMsg,'data'=>''));
        }else{
            echo json_encode(array('status'=>TRUE,'message'=>'','data'=>$logTime));
        }

    }

    public function updateUseCase()
    {
        if(isset($_POST))
        {
            if($_POST['use_case_id']==''){
                $_POST['use_case_id'] = $this->Project_modal->addTaskUseCase(array(
                    'project_task_id' => $_POST['project_task_id'],
                    'user_id' => $this->session->userdata('user_id'),
                    'task_usecase' => $_POST['use_case']
                ));

                //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'task',
                    'module_id' => $_POST['project_task_id'],
                    'description' => $this->session->userdata('username').' added use case'
                ));
            }
            else{
                $this->Project_modal->updateTaskUseCase(array(
                    'id_task_usecase' => $_POST['use_case_id'],
                    'project_task_id' => $_POST['project_task_id'],
                    'user_id' => $this->session->userdata('user_id'),
                    'task_usecase' => $_POST['use_case']
                ));

                //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'task',
                    'module_id' => $_POST['project_task_id'],
                    'description' => $this->session->userdata('username').' Use case updated'
                ));
            }

            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>array('use_case_id',$_POST['use_case_id'],'created_date_time'=>(new DateTime())->format('d/m/Y') ,'user_name' => $this->session->userdata('username')))); exit;
        }
    }

    public function uploadTaskAttachment()
    {
        if(isset($_POST) && isset($_FILES) && isset($_FILES['task_attachment']))
        {
            $file_name = doUpload($_FILES['task_attachment']['tmp_name'],$_FILES['task_attachment']['name'],'uploads/');
            $this->Project_modal->addAttachment(array(
                'attachment_type' => 'task',
                'project_task_id' => $_POST['project_task_id'],
                'user_id' =>$this->session->userdata('user_id'),
                'attachment_name' => $_FILES['task_attachment']['name'],
                'attachment' => $file_name
            ));

            //adding time line info
            $this->Project_modal->addTimeLine(array(
                'created_by' => $this->session->userdata('user_id'),
                'module_type' => 'task',
                'module_id' => $_POST['project_task_id'],
                'description' => $this->session->userdata('username').' added '.$_FILES['task_attachment']['name'].' Attachment'
            ));
            $user_name = $this->session->userdata('username');
            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>array('user_name'=>$user_name, 'attachment_name' => $_FILES['task_attachment']['name'],'attachment' => WEB_BASE_URL.'uploads/'.$file_name))); exit;
        }
        else
        {
            echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'Please select file')); exit;
        }

    }

    public function taskLogList()
    {
        $logList = $this->Project_modal->taskLogListGrid($_GET);
        echo json_encode($logList);
    }

    public function sprintDetails($project_id = '', $sprint_id = '', $type = 'assigned')
    {
        $data['type'] = $type;
        if ($this->session->userdata('user_type_id') == 3) {
            //$data['unassigned'] = $this->Project_modal->getUnassignedProjectTask(array('project_id' => $project_id,'user_id' => $this->session->userdata('user_id'),'week_date' => 1));
            if ($type == 'unassigned')
                $data['unassigned'] = array();
            if ($type == 'assigned')
                $data['assigned'] = $this->Project_modal->getAssignedProjectTask(array('project_id' => $project_id, 'user_id' => $this->session->userdata('user_id'), 'week_date' => 1));
            if ($type == 'to_do')
                $data['to_do'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'status' => 'accepted', 'user_id' => $this->session->userdata('user_id'), 'week_date' => 1));
            if ($type == 'progress')
                $data['progress'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'status' => 'progress', 'user_id' => $this->session->userdata('user_id'), 'week_date' => 1));
            if ($type == 'completed')
                $data['completed'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'status' => 'completed', 'user_id' => $this->session->userdata('user_id'), 'week_date' => 1));
            if ($type == 'hold')
                $data['hold'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'status' => 'hold', 'user_id' => $this->session->userdata('user_id'), 'week_date' => 1));
            $this->load->view('project/project_sprint_type_view', $data);
        } else {
            if ($type == 'unassigned')
                $data['unassigned'] = $this->Project_modal->getUnassignedProjectTask(array('project_id' => $project_id, 'sprint_id' => $sprint_id, 'user_id' => $this->session->userdata('user_id')));
            if ($type == 'assigned')
                $data['assigned'] = $this->Project_modal->getAssignedProjectTask(array('project_id' => $project_id, 'sprint_id' => $sprint_id, 'user_id' => $this->session->userdata('user_id')));
            if ($type == 'to_do')
                $data['to_do'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'sprint_id' => $sprint_id, 'status' => 'accepted', 'user_id' => $this->session->userdata('user_id')));
            if ($type == 'progress')
                $data['progress'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'sprint_id' => $sprint_id, 'status' => 'progress', 'user_id' => $this->session->userdata('user_id')));
            if ($type == 'completed')
                $data['completed'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'sprint_id' => $sprint_id, 'status' => 'completed', 'user_id' => $this->session->userdata('user_id')));
            if ($type == 'hold')
                $data['hold'] = $this->Project_modal->getProjectTaskByStatus(array('project_id' => $project_id, 'sprint_id' => $sprint_id, 'status' => 'hold', 'user_id' => $this->session->userdata('user_id')));
            // echo "<pre>"; print_r($data);
            $this->load->view('project/project_sprint_type_view', $data);
        }

    }
    public function changeTaskStatus()
    {//echo "<pre>"; print_r($_POST); exit;
        if(isset($_POST))
        {
            $id_task_flow = $_POST['id_task_flow'];
            $status = $_POST['status'];
            if($status=='progress')
            {
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'assigned_to' => $this->session->userdata('user_id')));

                //updating main task for in progress
                $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                $project_task = $this->Project_modal->getProjectTaskDetails(array('id_project_task' => $task_flow[0]['project_task_id']));
                //echo "<pre>"; print_r($project_task); exit;
                if(!empty($project_task) && strtolower($project_task[0]['task_status'])=='new'){
                    $this->Project_modal->updateProjectTask(array('id_project_task' => $project_task[0]['id_project_task'],'status' => 'progress'));
                }

                $this->Project_modal->updateTaskMember(array('task_status' => 'progress'),$task_members[0]['id_task_member']);
                $this->Project_modal->updateTaskFlow(array('task_status' => 'progress'),$id_task_flow);
                $message = 'Task has been started.';

                //adding time line info
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'task',
                    'module_id' => $task_flow[0]['project_task_id'],
                    'description' => $this->session->userdata('username').' started working'
                ));
            }
            else if($status=='release')
            {
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'assigned_to' => $this->session->userdata('user_id')));
                $task_week_flow = $this->Project_modal->getTaskWeekFlow(array('task_flow_id' => $id_task_flow,'user_id' => $this->session->userdata('user_id')));

                if(!empty($task_week_flow))
                    $this->Project_modal->deleteTaskWeekFlow(array('id_task_week_flow' => $task_week_flow[0]['id_task_week_flow']));

                if(!empty($task_members)){
                    sscanf($task_week_flow[0]['time'], "%d:%d:%d", $hours, $minutes, $sec);
                    $secs = $hours * 3600 + $minutes * 60;

                    sscanf($task_members[0]['allowted_time'], "%d:%d:%d", $hours, $minutes, $sec);
                    $secs = (($hours * 3600 + $minutes * 60) - $secs);

                    $hours = floor($secs / 3600);
                    $mins = floor($secs / 60 % 60);
                    $time = $hours.':'.$mins.':00';
                    $this->Project_modal->updateTaskMember(array('allowted_time' => $time),$task_members[0]['id_task_member']);

                    if(time_to_sec($time)==0){
                        $this->Project_modal->deleteTaskMember(array('id_task_member' => $task_members[0]['id_task_member']));
                    }
                }
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow));
                if(empty($task_members)){
                    $this->Project_modal->updateTaskFlow(array('task_status' => 'new'),$id_task_flow);
                }
                $message = 'Task has been released.';

                //adding time line info
                $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'task',
                    'module_id' => $task_flow[0]['project_task_id'],
                    'description' => $this->session->userdata('username').' released task'
                ));
            }
            else if($status=='hold')
            {
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'assigned_to' => $this->session->userdata('user_id')));

                $this->Project_modal->updateTaskMember(array('task_status' => 'hold'),$task_members[0]['id_task_member']);

                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'status_not' => 'hold'));
                if(empty($task_members)){
                    $this->Project_modal->updateTaskFlow(array('task_status' => 'hold'),$id_task_flow);
                }
                $message = 'Task has been hold.';

                //adding time line info
                $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                $this->Project_modal->addTimeLine(array(
                    'created_by' => $this->session->userdata('user_id'),
                    'module_type' => 'task',
                    'module_id' => $task_flow[0]['project_task_id'],
                    'description' => $this->session->userdata('username').' has hold the task'
                ));
            }
            else if($status=='completed')
            {
                $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                $project_task_id = $task_flow[0]['project_task_id'];
                if(count($this->Project_modal->getChecklist($project_task_id)) != count($_POST['data'])){
                    $message = 'Please select all checklist';
                    echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>$message)); exit;
                }

                $time_log = $this->Project_modal->getTimeLogByTaskWorkflowId(array('task_flow_id' => $id_task_flow,'user_id' => $this->session->userdata('user_id')));
                if(empty($time_log)){
                    echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'No time log exists')); exit;
                }

                //Comment only added
                $this->Project_modal->updateTaskMemberByTask(array('task_flow_id'=>$id_task_flow, 'task_comment' => $_POST['task_comment'], 'assigned_to'=> $this->session->userdata('user_id')));
                //Adding checklist
                $response = $this->Project_modal->saveCheckListStatus(array('data'=>$_POST['data'], 'taskId'=> $project_task_id));

                /*$task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                $totalChecklist = $this->Project_modal->getChecklist($task_flow[0]['project_task_id']);
                $checkList = $this->Project_modal->getTaskCheckList(array('task_id' => $task_flow[0]['project_task_id'], 'user_id' => $this->session->userdata('user_id')));
                if(count($totalChecklist) != count($checkList)){
                    echo json_encode(array('status'=>FALSE,'error'=>false,'data'=>'Please select checklist'));
                    exit;
                }*/

                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'assigned_to' => $this->session->userdata('user_id')));

                $this->Project_modal->updateTaskMember(array('task_status' => 'approval_waiting'),$task_members[0]['id_task_member']);
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'status_not' => 'approval_waiting'));
                if(empty($task_members)){

                    //if user completed the task
                    /*if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==0)
                    {*/
                        $this->Project_modal->updateTaskFlow(array('actual_end_date' => date('Y-m-d'), 'task_status' => 'approval_waiting'),$id_task_flow);
                        $message = 'Task waiting for team lead approval';
                        echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>$message)); exit;
                    /*}*/
                    $this->Project_modal->updateTaskFlow(array('actual_end_date' => date('Y-m-d'), 'task_status' => 'completed'),$id_task_flow);
                    $next_dept = 0;
                    $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                    $project_dept = $this->Project_modal->getProjectDepartments($task_flow[0]['project_id']);
                    for($s=0;$s<count($project_dept);$s++){
                        if($project_dept[$s]['department_id']==$task_flow[0]['department_id'] && $task_flow[0]['is_forward']){
                            if(isset($project_dept[$s+1]['department_id'])){
                                $next_dept = $project_dept[$s+1]['department_id'];
                            }
                        }
                    }

                    if($next_dept){
                        $taskDetails = $this->Project_modal->getTaskDetails(array('id_project_task' => $task_flow[0]['project_task_id']));
                        //$departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));
                        $task_department = $this->Project_modal->getTaskDepartment(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));

                        if(empty($task_department)){
                            $departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));
                        }
                        else{
                            $departmentEstimationTime = $task_department[0]['time'];
                        }

                        $this->Project_modal->addTaskFlow(array(
                            'project_task_id' => $task_flow[0]['project_task_id'],
                            'department_id' => $next_dept,
                            'estimated_time' => $departmentEstimationTime,
                            'start_date' => $task_flow[0]['start_date'],
                            'end_date' => $task_flow[0]['end_date'],
                        ));

                        $message = 'Task Forwarded to next department successfully.';

                        //adding time line info
                        $this->Project_modal->addTimeLine(array(
                            'created_by' => $this->session->userdata('user_id'),
                            'module_type' => 'task',
                            'module_id' => $task_flow[0]['project_task_id'],
                            'description' => $this->session->userdata('username').' has been completed the task'
                        ));
                    }
                    else{
                        $this->Project_modal->updateProjectTask(array(
                            'id_project_task' => $task_flow[0]['project_task_id'],
                            'status' => 'completed',
                            'completed_date' => date('Y-m-d')
                        ));

                        $message = 'Task completed successfully.';
                        //adding time line info
                        $this->Project_modal->addTimeLine(array(
                            'created_by' => $this->session->userdata('user_id'),
                            'module_type' => 'task',
                            'module_id' => $task_flow[0]['project_task_id'],
                            'description' => $this->session->userdata('username').' has been completed the task'
                        ));

                    }


                }
                else
                {
                    //adding time line info
                    $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                    $this->Project_modal->addTimeLine(array(
                        'created_by' => $this->session->userdata('user_id'),
                        'module_type' => 'task',
                        'module_id' => $task_flow[0]['project_task_id'],
                        'description' => $this->session->userdata('username').' has been completed the task'
                    ));

                    $message = 'Task completed successfully.';
                }

                //notifications
                $project_task_details = $this->Project_modal->getProjectTaskDetails(array('id_project_task' => $project_task_id));
                $emp = $this->Project_modal->getUsers(array(
                    'project_id' => $project_task_details[0]['project_id'],
                    'department_id' => $task_flow[0]['department_id'],
                    'is_lead' => 1
                ));
                $currentUser = $this->User_model->getUser(array('id_user' => $this->session->userdata('user_id')));
                $this->Project_modal->addNotification(array(
                    'user_id' => $emp[0]['id_user'],
                    'subject' => 'Task',
                    'message' => $currentUser[0]['first_name'].' '.$currentUser[0]['last_name'].' completed <b>'.$project_task_details[0]["task_name"].'</b> task',
                    'url' => WEB_BASE_URL.'index.php/Project/weekWorkload',
                    'read_flag' => 1
                ));
            }

            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>$message)); exit;
        }
    }

    /*function updateMainTaskStatus()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['status']))
        {
            if($data['status']=='completed')
            {
                $total_tasks = $this->Project_modal->getSubTaskByParent($data['parent_id']);
                $completed_task_dept = $this->Project_modal->getCompletedSubtaskDept($data['parent_id'],'completed',$this->session->userdata('department_id'));
                //echo "<pre>"; print_r($completed_task_dept); exit;
                if(count($total_tasks)!=count($completed_task_dept))
                {
                    echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'All sub tasks are not completed')); exit;
                }
                else
                {
                    $dept = $completed_task_dept[0]['department_id'];
                    $next_dept = 0;
                    $project_dept = $this->Project_modal->getProjectDepartments($completed_task_dept[0]['project_id']);
                    for($s=0;$s<count($project_dept);$s++)
                    {
                        if($project_dept[$s]['department_id']==$dept)
                        {
                            if(isset($project_dept[$s+1])){ $next_dept = $project_dept[$s+1]['department_id']; }
                        }
                    }

                    if($next_dept!=0){
                        $prev_lead = $this->Project_modal->getProjectTeamLead(array('department_id' => $dept,'is_lead'=>1));

                        //echo "<pre>"; print_r($prev_lead);
                        $lead = $this->Project_modal->getProjectTeamLead(array('department_id' => $next_dept,'is_lead'=>1));
                        //echo "<pre>"; print_r($lead); exit;
                        if(!empty($lead))
                        {
                            for($s=0;$s<count($completed_task_dept);$s++)
                            {
                                $new_task_workflow_id = $this->Project_modal->addTaskWorkFlow(array(
                                    'project_task_id' => $completed_task_dept[$s]['project_task_id'],
                                    'assigned_to' => $lead[0]['id_user'],
                                    'assigned_by' => $prev_lead[0]['id_user'],
                                    'estimated_time' => $completed_task_dept[$s]['current_estimated_time'],
                                ));


                            }
                            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'Task forwarded successfully.')); exit;
                        }
                        else
                        {
                            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'No team lead to forward')); exit;
                        }

                    }
                    else
                    {
                        echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'Task completed successfully.')); exit;
                    }
                }
            }
            else{
                echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'Status updated')); exit;
            }
        }
        else{
            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'Saved')); exit;
        }
    }*/

    function updateMainTaskStatus()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['status']))
        {
            if($data['status']=='completed')
            {
                $total_tasks = $this->Project_modal->getSubTaskByParent($data['parent_id']);
                $completed_task_dept = $this->Project_modal->getCompletedSubtaskDept($data['parent_id'],'completed',$this->session->userdata('department_id'));
                if(count($total_tasks)!=count($completed_task_dept))
                {
                    echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'All sub tasks are not completed')); exit;
                }
                else
                {
                    $dept = $completed_task_dept[0]['department_id'];
                    $project_id = $completed_task_dept[0]['project_id'];
                    $next_dept = 0;
                    $project_dept = $this->Project_modal->getProjectDepartments($completed_task_dept[0]['project_id']);
                    for($s=0;$s<count($project_dept);$s++)
                    {
                        if($project_dept[$s]['department_id']==$dept)
                        {
                            if(isset($project_dept[$s+1])){ $next_dept = $project_dept[$s+1]['department_id']; }
                        }
                    }

                    if($next_dept!=0){
                        $prev_lead = $this->Project_modal->getProjectTeamLead(array('department_id' => $dept,'is_lead'=>1,'project_id' => $project_id));

                        //echo "<pre>"; print_r($prev_lead);
                        $lead = $this->Project_modal->getProjectTeamLead(array('department_id' => $next_dept,'is_lead'=>1,'project_id' => $project_id));
                        //echo "<pre>"; print_r($lead); exit;
                        if(!empty($lead))
                        {
                            for($s=0;$s<count($completed_task_dept);$s++)
                            {
                                $check_task_exists = $this->Project_modal->CheckTaskExsitsInDept(array('task_id' => $completed_task_dept[$s]['project_task_id'],'department_id' => $lead[0]['department_id']));
                                if(empty($check_task_exists)){
                                    $new_task_workflow_id = $this->Project_modal->addTaskWorkFlow(array(
                                        'project_task_id' => $completed_task_dept[$s]['project_task_id'],
                                        'assigned_to' => $lead[0]['id_user'],
                                        'assigned_by' => $prev_lead[0]['id_user'],
                                        'estimated_time' => $completed_task_dept[$s]['current_estimated_time'],
                                    ));
                                }
                                else{
                                    if($check_task_exists[0]['status']=='reject'){
                                        $new_task_workflow_id = $this->Project_modal->addTaskWorkFlow(array(
                                            'project_task_id' => $completed_task_dept[$s]['project_task_id'],
                                            'assigned_to' => $lead[0]['id_user'],
                                            'assigned_by' => $prev_lead[0]['id_user'],
                                            'estimated_time' => $completed_task_dept[$s]['current_estimated_time'],
                                        ));
                                    }
                                }

                            }

                            //adding time line info
                            $this->Project_modal->addTimeLine(array(
                                'created_by' => $this->session->userdata('user_id'),
                                'module_type' => 'task',
                                'module_id' => $_POST['parent_id'],
                                'description' => $_POST['comments']
                            ));

                            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'Task forwarded successfully.')); exit;
                        }
                        else
                        {
                            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'No team lead to forward')); exit;
                        }

                    }
                    else
                    {
                        echo json_encode(array('status'=>FALSE,'error'=>true,'data'=>'Task completed successfully.')); exit;
                    }
                }
            }
            else{
                echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'Status updated')); exit;
            }
        }
        else{
            echo json_encode(array('status'=>TRUE,'error'=>true,'data'=>'Saved')); exit;
        }
    }

    function touchPoint()
    {
        $projectList = $this->Project_modal->TouchPointsGrid($_GET);
        echo json_encode($projectList);
    }
    public function logDetails()
    {
            $data['content'] = 'project/log_details';
            $this->load->view('partials/landing',$data);

    }
    function logDetailsGrid()
    {
        $logList = $this->Project_modal->logDetailsGrid($_GET);
        echo json_encode($logList);
    }
    public function usersWorkload()
    {
            $data['content'] = 'project/users-workload';
            $data['department_list'] = $this->Project_modal->getDepartmentList();
            $this->load->view('partials/landing',$data);

    }
    public function dayMonitoring()
    {
            $data['content'] = 'project/day-monitoring';
            $this->load->view('partials/landing',$data);

    }
    public function fullCalendar()
        {
                $data['content'] = 'project/full-calendar';
                $this->load->view('partials/landing',$data);
        }
    public function projectDetails()
    {
        $data['content'] = 'project/project_details';
        $this->load->view('partials/landing',$data);

    }
    public function projectCreation($id=0)
    {
        $data['project_id'] = $id;
        $data['department_name'] = $this->session->userdata('department_name');
        $projectDetails = $this->Project_modal->isProjectEmployee(array(
            'project_id'=> $data['project_id'],
            'user_id'=> $this->session->userdata('user_id')));
        if($this->session->userdata('user_type_id') == 2 || $this->session->userdata('is_manager') == 1 || count($projectDetails)>0){
            $data['content'] = 'project/project-creation';
            $this->load->view('partials/landing',$data);
        }else{
            redirect(WEB_BASE_URL.'index.php/project'); exit;
        }


    }
    public function getProjectDetails($projectId){
        $projectDetails = $this->Project_modal->getProjectDetailsById($projectId);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectDetails));
    }
    public function getDetailOfProject($projectId){
        $projectDetails = $this->Project_modal->getDetailOfProject($projectId);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectDetails));
    }
    public function getProjectDepartments($projectId){
        $projectDepartments = $this->Project_modal->getProjectDepartments($projectId);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectDepartments));
    }
    public function getProjectDetialsById()
    {
        $projectId = $this->input->get('project_id');
        $data['projectId'] = $projectId;
        $data['projectDetails'] = $this->Project_modal->getProjectDetailsById($projectId);
        $data['projectDetails'] = count($data['projectDetails'])>0?$data['projectDetails'][0]:'';
        
        $data['projectWorkload'] = $this->Project_modal->getProjectWorkloadPercentage($projectId);
        //echo "<pre>"; print_r($data['projectWorkload']); exit;
        $data['projectMilestone'] = $this->Project_modal->getMilestoneForProject($projectId);
        $data['projectOpenTask'] = $this->Project_modal->getOpenTaskForProject($projectId);
        //$data['projectDepartmentStatus'] = $this->Project_modal->getProjectDepartmentStatus($projectId);
        $data['projectDepartmentStatus'] = $this->Project_modal->getProjectDepartmentTime($projectId);
        //echo "<pre>"; print_r($data['projectDepartmentStatus']); exit;
        $data['projectProcessStatus'] = $this->Project_modal->getProjectProcessStatus($projectId);
        $data['projectYearlyStatus'] = $this->Project_modal->getProjectYearlyStatus($projectId, $data['projectDetails']['project_start_date']);

        $data=$this->load->view('modals/projectDetails',$data, TRUE);
        $this->output->set_output($data);
    }
    public function weekWorkload($project_id=0,$week=0,$department_id=0)
    {

        if($this->session->userdata('user_type_id')==3 ||  $this->session->userdata('user_type_id')==2)
        {
            if($this->session->userdata('is_lead')==1) {
                $data['department_projects'] = $this->Project_modal->getDepartmentProjects(array('department_id' =>$this->session->userdata('department_id')));
            }elseif($this->session->userdata('user_type_id')==2){
                $data['department_projects'] = $this->Project_modal->getProjectList();
                $data['department_list'] = $this->Project_modal->getDepartmentList();
                $data['department_id'] = $department_id;
            }
            else{
                $data['department_projects'] = $this->Project_modal->getEmployeeProjects(array('user_id' =>$this->session->userdata('user_id')));
            }

            $dep_project_ids = array_map(function($i){ return $i['project_id']; },$data['department_projects']);
            if($project_id!=0 && !in_array($project_id,$dep_project_ids)){
                redirect(WEB_BASE_URL);
            }


            if($project_id==0){
                if(!empty($data['department_projects']) && ($this->session->userdata('is_lead')==1)){
                    $data['project_id'] = $project_id = $data['department_projects'][0]['project_id'];
                }
                else{
                    $data['project_id'] = $project_id;
                }
            }
            else{
                $data['project_id'] = $project_id;
            }

            //echo "<pre>"; print_r($data['users']); exit;
            $data['content'] = 'project/week-workload';
            $this->load->view('partials/landing',$data);
        } else
            redirect(WEB_BASE_URL);
    }



    function loadWeekWorkload($project_id=0,$week=0,$department_id=0)
    {
        if($this->session->userdata('user_type_id')==3 ||  $this->session->userdata('user_type_id')==2)
        {
            if($this->session->userdata('is_lead')==1) {
                $data['department_projects'] = $this->Project_modal->getDepartmentProjects(array('department_id' =>$this->session->userdata('department_id')));
                $current_department_id = $this->session->userdata('department_id');
            }elseif($this->session->userdata('user_type_id')==2){
                $data['department_projects'] = $this->Project_modal->getProjectList();
                $data['department_list'] = $this->Project_modal->getDepartmentList();
                $current_department_id = $data['department_id'] = $department_id;
            }
            else{
                $data['department_projects'] = $this->Project_modal->getEmployeeProjects(array('user_id' =>$this->session->userdata('user_id')));
                $current_department_id = $this->session->userdata('department_id');
            }

            //$project_id_array = array_map(function($i){ return $i['project_id']; },$data['department_projects']);

            //echo "<pre>"; print_r($data['department_projects']); exit;

            $dep_project_ids = array_map(function($i){ return $i['project_id']; },$data['department_projects']);
            if($project_id!=0 && !in_array($project_id,$dep_project_ids)){
                redirect(WEB_BASE_URL);
            }

            if($week!=0){
                $check = $this->isValidTimeStamp($week);
                if($check==''){ redirect(WEB_BASE_URL); }
            }


            if($project_id==0){
                if(!empty($data['department_projects']) && ($this->session->userdata('is_lead')==1)){
                    $data['project_id'] = $project_id = $data['department_projects'][0]['project_id'];
                }
                else{
                    $data['project_id'] = $project_id;
                }
            }
            else{
                $data['project_id'] = $project_id;
            }

            $data['week'] = $week;
            if($week==0){
                $data['last_week'] = strtotime('-7 day', strtotime('monday this week'));
                $data['week'] = strtotime('monday this week');
                //$data['current_week'] = strtotime('monday this week');
                $data['next_week'] = strtotime('+7 day', strtotime('monday this week'));
            }
            else{
                //date('Y-m-d', strtotime('previous monday', strtotime($week)))
                if(strtotime('monday this week')!=$week)
                    $data['last_week'] = strtotime('-7 day', $week);
                else $data['last_week'] = strtotime('-7 day', $week);
                $data['week'] = $week;
                $data['next_week'] = strtotime('+7 day', $week);
            }
            //echo $monday = strtotime("last monday"); exit;
            //echo date('Y-m-d', strtotime('+7 day', strtotime('monday this week'))); exit;

            $start_date = date('Y-m-d',$data['week']);
            $end_date = date('Y-m-d',strtotime('+6 day', $data['week']));

            $data['unassigned_task'] = $this->Project_modal->getUserTasks(array('project_id' => $project_id));
            $data['unassigned_task_counts'] = $this->getUnassignedTasksCounts(array('project_id' => $project_id));
            //$data['unassigned_task'] = $this->Project_modal->getUserTasks();
            //echo "<pre>";print_r($data['unassigned_task']); exit;
            if($this->session->userdata('is_lead')==1) {
                $data['users'] = $data['team_members'] = $users = $this->Project_modal->getUsers(array('department_id' => $this->session->userdata('department_id'),'project_id' => $data['project_id']));
            }elseif($this->session->userdata('user_type_id')==2){
                $data['users'] = $data['team_members'] = $users = $this->Project_modal->getUsers(array('department_id' =>$department_id,'project_id' => $data['project_id']));

            }
            else{
                $data['users'] = $data['team_members'] = $users = $this->Project_modal->getUsers(array('department_id' => $this->session->userdata('department_id'),'user_id' => $this->session->userdata('user_id')));
            }
            //For holidays
            $this->load->model('Holiday_modal');
            $holidaysList = $this->Holiday_modal->getListOfHolidaysByDateRange(array('start_date' => date('m/d/Y',$data['week']), 'end_date' => date('m/d/Y',strtotime('+6 day', $data['week']))));

            $item = [];
            foreach($holidaysList as $k=>$v){
                $item[$v['date']] = array('id' => $v['id_holiday'], 'date' => $v['date'], 'title' => $v['title'], 'description' => $v['description']);
            }
            $data['holidays'] = $item;

            for($s=0;$s<count($users);$s++)
            {
                $data['users'][$s]['work_load'] = array();

                if($week==0) $start_week = strtotime("-7 day",strtotime("monday this week"));
                else $start_week = strtotime("last monday midnight",$week);
                $end_week = strtotime("next sunday",$start_week);
                $start_week = date("Y-m-d",$start_week);
                $end_week = date("Y-m-d",$end_week);

                $last_week_log_time = $this->Project_modal->getWeekLogTime(array('user_id' => $users[$s]['id_user'],'start_date' => $start_week, 'end_date' => $end_week,'department_id'=>$current_department_id));
                $last_week_work_load = $this->Project_modal->getWeekAllotedTime(array('user_id' => $users[$s]['id_user'],'start_date' => $start_week, 'end_date' => $end_week,'department_id'=>$current_department_id));
                $data['users'][$s]['last_week_work_load'] = round(((time_to_sec($last_week_work_load[0]['time'])/time_to_sec("40:00:00"))*100));

                if($week==0) $monday = strtotime("monday this week");
                else $monday = strtotime("monday this week",$week);
                $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
                $start_week = date("Y-m-d",$monday);
                $end_week = date("Y-m-d",$sunday);

                $current_week_work_load = $this->Project_modal->getWeekAllotedTime(array('user_id' => $users[$s]['id_user'],'start_date' => $start_week, 'end_date' => $end_week,'department_id'=>$current_department_id));
                $data['users'][$s]['current_week_work_load'] = round(((time_to_sec($current_week_work_load[0]['time'])/time_to_sec("40:00:00"))*100));

                if(time_to_sec($last_week_work_load[0]['time']==0)){  $data['users'][$s]['percentage'] = 0; }
                else
                    $data['users'][$s]['percentage'] = round(((time_to_sec($last_week_log_time[0]['time'])/time_to_sec($last_week_work_load[0]['time']))*100));;

                $res = $this->Project_modal->getUserTaskWorkload(array('assigned_to' => $users[$s]['id_user'],'user_id' => $users[$s]['id_user'],'start_date' => $start_date,'end_date' => $end_date));
                $data['users'][$s]['work_load'] = $res['res'];

                if(!empty($data['users'][$s]['work_load']))
                {
                    for($t=0;$t<count($data['users'][$s]['work_load']);$t++)
                    {
                        $data['users'][$s]['work_load'][$t]['project_count'] = 0;
                        for($sth=0;$sth<count($res['projects']);$sth++)
                        {
                            if($data['users'][$s]['work_load'][$t]['day_id']==$res['projects'][$sth]['day_id'])
                                $data['users'][$s]['work_load'][$t]['project_count'] = $res['projects'][$sth]['projects'];
                        }
                        $data['users'][$s]['work_load'][$t]['total_task'] = 0;
                        for($sth=0;$sth<count($res['total_tasks']);$sth++)
                        {
                            if($data['users'][$s]['work_load'][$t]['day_id']==$res['total_tasks'][$sth]['day_id'])
                                $data['users'][$s]['work_load'][$t]['total_task'] = $res['total_tasks'][$sth]['tasks'];
                        }
                        $data['users'][$s]['work_load'][$t]['completed_task'] = 0;
                        for($sth=0;$sth<count($res['completed_tasks']);$sth++)
                        {
                            if($data['users'][$s]['work_load'][$t]['day_id']==$res['completed_tasks'][$sth]['day_id'])
                                $data['users'][$s]['work_load'][$t]['completed_task'] = $res['completed_tasks'][$sth]['tasks'];
                        }
                    }
                }
            }
        }
        $data=$this->load->view('workload/load_week_workload',$data, TRUE);
        $this->output->set_output($data);
    }
    function roundToNextHour($dateString) {
        $date = new DateTime($dateString);
        $minutes = $date->format('i');
        if ($minutes > 0) {
            $date->modify("+1 hour");
            $date->modify('-'.$minutes.' minutes');
        }
        return $date;
    }

    public function filterDayWiseMonitoring(){
        $dayStart = '9:00 AM';
        $dayEnd = '';
        $dayDuration = '';
        $isToday = false;
        $today = date('Y/m/d');
        $params['date'] = date('Y/m/d', strtotime($this->input->get('date')));
        if($params['date'] == $today){
            $isToday = true;
            $now = new DateTime();
            $dayEnd = $this->roundToNextHour($now->format('Y-m-d H:i:s'));
            $dayEndHour = $dayEnd->format('H');
            $dayDuration = (int)$dayEndHour-9;
            $dayEnd = $dayEnd->format('h:i A');
        }else{
            $isToday = false;
            $dayDuration = 11;
            $dayEnd = '8:00 PM';
        }
        $params['user'] = $this->input->get('employee');
        $params['department'] = $this->input->get('department');
        $params['project'] = $this->input->get('project');
        if($this->input->get('employee') === 'null'){
            unset($params['user']);
        }
        if($this->input->get('department') === 'null'){
            unset($params['department']);
        }
        if($this->input->get('project') === 'null'){
            unset($params['project']);
        }
        $data['users'] = $this->Project_modal->filterDayWiseMonitoring($params);
        //echo "<pre>"; print_r($data['users']); exit;
        $filterDate = $params['date'];
        $singleUser = [];
        $temp = [];
        $len = 0;
        $actualHours = 0;
        if($data['users'] && count($data['users'])>0){
            foreach($data['users'] as $key=>$val){
                //echo "<pre>"; print_r($val); exit;
                $act_time = '0';
                foreach($val as $k=>$v){
                    //echo "<pre>"; print_r($v); exit;
                    if(isset($temp[$v['id_user']]))
                    {
                        $len = count($temp[$v['id_user']])-1;
                        $currentData = $temp[$v['id_user']][$len];
                        $tmp1 = date_create($filterDate.' '.$temp[$v['id_user']][$len]['endTime']);
                        $start = date('h:i a',$tmp1->getTimestamp());

                        $tmp2 = date_create($filterDate.' '.$v['start_time']);
                        $end = date('h:i a',$tmp2->getTimestamp());
                        $duration = date_diff($tmp2, $tmp1);
                        $durationInDecimal = '';
                        if($duration->format('%h')<=0){
                            $durationInDecimal .= '0.';
                        }else{
                            $durationInDecimal .= $duration->format('%h').'.';
                        }
                        if($duration->format('%i')<=9){
                            $durationInDecimal .= $duration->format('%i');
                        }else{
                            $durationInDecimal .= $duration->format('%i');
                        }
                        if($duration->invert==1){   /*tmp2 is greater than 1*/
                            $temp[$v['id_user']][] = array(
                                'taskName' => '',
                                'startTime' => $start,
                                'endTime' => $end,
                                'actual' => '',
                                'duration' => $durationInDecimal,
                                'workType' => 'free',
                                'isToday' => $isToday,
                                'totalDuration' => $dayDuration,
                                'test1' => 7
                            );
                            $temp[$v['id_user']][] = array(
                                'taskName' => $v['task_name'],
                                'startTime' => $v['start_time'],
                                'endTime' => $v['end_time'],
                                'actual' => $v['duration'],
                                'duration' => $this->getTimesindecimal($v['duration']),
                                'workType' => 'working',
                                'isToday' => $isToday,
                                'totalDuration' => $dayDuration,
                                'test1' => 6
                            );
                        }else{
                            $temp[$v['id_user']][] = array(
                                'taskName' => $v['task_name'],
                                'startTime' => $v['start_time'],
                                'endTime' => $v['end_time'],
                                'actual' => '00:00',
                                'duration' => $this->getTimesindecimal($v['duration']),
                                'workType' => 'working',
                                'isToday' => $isToday,
                                'totalDuration' => $dayDuration,
                                'test1' => 5
                            );
                        }
                        if(!isset($val[$k+1])){
                            $tmp1 = date_create($filterDate.' '.$v['end_time']);
                            $start = date('h:i a',$tmp1->getTimestamp());

                            $tmp2 = date_create($filterDate.' '.$dayEnd);
                            $end = date('h:i a',$tmp2->getTimestamp());

                            $duration = date_diff($tmp2, $tmp1);
                            $durationInDecimal = '';
                            if($duration->format('%h')<=0){
                                $durationInDecimal .= '0.';
                            }else{
                                $durationInDecimal .= $duration->format('%h').'.';
                            }
                            if($duration->format('%i')<=9){
                                $durationInDecimal .= $duration->format('%i');
                            }else{
                                $durationInDecimal .= $duration->format('%i');
                            }
                            if($duration->invert==1) {   /*tmp2 is greater than 1*/
                                $temp[$v['id_user']][] = array(
                                    'taskName' => '',
                                    'startTime' => $start,
                                    'endTime' => $end,

                                    'duration' => $durationInDecimal,
                                    'workType' => 'free',
                                    'isToday' => $isToday,
                                    'totalDuration' => $dayDuration,
                                    'test1' => 4
                                );
                            }
                        }
                    }
                    else
                    {
                        if($v['start_time'] == 0 && $v['end_time'] == 0){
                            $temp[$v['id_user']][] = array(
                                'taskName' => '',
                                'startTime' => $dayStart,
                                'endTime' => $dayEnd ,
                                'duration' => $dayDuration,
                                'workType' => 'free',
                                'isToday' => $isToday,
                                'totalDuration' => $dayDuration,
                                'test1' => 3
                            );
                        }else{

                            $tmp1 = date_create($filterDate.' '.$dayStart);
                            $start = date('h:i a',$tmp1->getTimestamp());

                            $tmp2 = date_create($filterDate.' '.$v['start_time']);
                            $end = date('h:i a',$tmp2->getTimestamp());
                            $duration = date_diff($tmp2, $tmp1);
                            $durationInDecimal = '';
                            if($duration->format('%h')<=0){
                                $durationInDecimal .= '0.';
                            }else{
                                $durationInDecimal .= $duration->format('%h').'.';
                            }
                            if($duration->format('%i')<=9){
                                $durationInDecimal .= $duration->format('%i');
                            }else{
                                $durationInDecimal .= $duration->format('%i');
                            }
                            if($duration->invert==1) {   /*tmp2 is greater than 1*/
                                $temp[$v['id_user']][] = array(
                                    'taskName' => '',
                                    'startTime' => $start,
                                    'endTime' => $end,
                                    'duration' => $durationInDecimal,
                                    'workType' => 'free',
                                    'isToday' => $isToday,
                                    'totalDuration' => $dayDuration,
                                    'test1' => 2
                                );
                            }

                            $temp[$v['id_user']][] = array(
                                'taskName' => $v['task_name'],
                                'startTime' => $v['start_time'],
                                'endTime' => $v['end_time'],
                                'duration' => $this->getTimesindecimal($v['duration']),
                                'workType' => 'working',
                                'isToday' => $isToday,
                                'totalDuration' => $dayDuration,
                                'test1' => 1
                            );

                        }

                    }

                    $act_time = $act_time + time_to_sec($v['duration']);
                }
                $singleUser[$v['id_user']] = array(
                    'name'=>$v['name'],
                    'projectCounts'=>$v['projectCounts'],
                    'taskCount'=>$v['taskCount'],
                    'estimated_time'=>$v['estimated_time'],
                    'status'=>$v['status'],
                    'id_user'=>$v['id_user'],
                    'actual' => sec_to_time($act_time),
                    'logged_time'=>$temp[$v['id_user']]
                );
            }
        }else{
            $singleUser = [];
        }
        //echo "<pre>"; print_r($singleUser[44]); exit;
        $data['data'] = $singleUser;
        $data=$this->load->view('modals/dayMonitoring',$data, TRUE);
        $this->output->set_output($data);
    }

    private function getTimesindecimal($curTime){
        $timeArray = explode(':',$curTime);
        if(count($timeArray)==3){
            return ($timeArray[0]).'.'.($timeArray[1]);
        }else if(count($timeArray)==2){
            return ($timeArray[0]).'.'.($timeArray[1]);
        }
    }
    public function getTaskTypes($type = 'task' ){
        $rowCount = $this->Project_modal->getTaskTypes($type);
        echo json_encode(array('data'=>$rowCount));
    }
    public function getTaskTypesByProjectId($type , $id){
        $deptId=0;
        $project_dept = $this->Project_modal->getProjectDepartments($id);
        for($s=0;$s<count($project_dept);$s++){
            if($project_dept[$s]['project_id']==$id){
                $deptId = $project_dept[$s]['department_id'];
            }
        }
        $list = $this->Project_modal->getTaskTypesByProjectId($type, $deptId);
        echo json_encode(array('data'=>$list));
    }

    //added by sthanka
    public function addTaskToUser()
    {
        $data = $_POST;
        //echo '<pre>'; print_r($data);
        //$project_task_details = $this->Project_modal->getProjectTaskDetails(array('id_project_task' => $data['project_task_id']));
        $user = $this->Project_modal->getUser(array('id_user' => $data['assigned_to']));
        $check = $this->Project_modal->getTaskFlow(array('project_task_id' => $data['project_task_id'], 'department_id' => $this->session->userdata('department_id')));
        $check1 = $this->Project_modal->getTaskMember(array('task_flow_id' => $check[0]['id_task_flow'], 'assigned_to' => $data['assigned_to']));
        $check_task_assigning_first_time = $this->Project_modal->getTaskMember(array('task_flow_id' => $check[0]['id_task_flow']));
        /*$ddate =date('Y-m-d');
        $date = new DateTime($ddate);
        $week = $date->format("W");
        //echo $week.'---';
        $gendate = new DateTime();
        $gendate->setISODate(date('Y'),$week,$data['log_day']);
        $date = $gendate->format('Y-m-d');*/
        $date = date('Y-m-d',$data['log_day']);
        $data['log_day'] = date('N',$data['log_day']);
        $data['time'] = $data['time'].':00';
        //echo "<pre>"; print_r($data['time']); exit;
        if(empty($check1))
        {
            /*$task_flow_id = $this->Project_modal->addTaskFlow(array(
                'project_task_id' => $data['project_task_id'],
                'department_id' => $user[0]['department_id'],
                'estimated_time' => $data['estimated_time'],
                'start_date' => $date,
                'end_date' => $date,
                'actual_end_date' => $date,
                'status' => 'new'
            ));*/
            $date1 = $date;
            if(strtotime($check[0]['end_date'])>strtotime($date)){
                $date1 = $check[0]['end_date'];
            }

            if(empty($check_task_assigning_first_time))
            {
                $task_work_flow_id = $this->Project_modal->updateTaskFlow(array(
                    'start_date' => $date,
                    'end_date' => $date1,
                    'actual_end_date' => $date1,
                ),$check[0]['id_task_flow']);
            }
            else
            {
                $task_work_flow_id = $this->Project_modal->updateTaskFlow(array(
                    'end_date' => $date1,
                    'actual_end_date' => $date1,
                ),$check[0]['id_task_flow']);
            }

            $task_member_id = $this->Project_modal->addTaskMember(array(
                'task_flow_id' => $check[0]['id_task_flow'],
                'assigned_to' => $data['assigned_to'],
                'allowted_time' => $data['time'],
            ));

            $task_week_flow = $this->Project_modal->addTaskWeekLoad(array(
                'task_flow_id' => $check[0]['id_task_flow'],
                'day_id' => $data['log_day'],
                'user_id' => $data['assigned_to'],
                'date' => $date,
                'time' => $data['time'],
            ));

            $this->Project_modal->addTimeLine(array(
                'created_by' => $this->session->userdata('user_id'),
                'module_type' => 'task',
                'module_id' => $data['project_task_id'],
                'description' => $this->session->userdata('username').' assigned task to '.$user[0]['first_name'].' '.$user[0]['last_name'].'.'
            ));
        }
        else
        {
            /*sscanf($check[0]['estimated_time'], "%d:%d:%d", $hours, $minutes, $sec);
            $secs = $hours * 3600 + $minutes * 60;

            sscanf($data['estimated_time'], "%d:%d", $hours, $minutes);
            $secs = ($secs + ($hours * 3600 + $minutes * 60));

            $hours = floor($secs / 3600);
            $mins = floor($secs / 60 % 60);
            $time = $hours.':'.$mins;*/

            $date1 = $date;
            if(strtotime($check[0]['end_date'])>strtotime($date)){
                $date1 = $check[0]['end_date'];
            }
            $task_work_flow_id = $this->Project_modal->updateTaskFlow(array(
                'end_date' => $date1,
                'actual_end_date' => $date1,
            ),$check[0]['id_task_flow']);

            //task member alloted time calculating

            sscanf($check1[0]['allowted_time'], "%d:%d:%d", $hours, $minutes, $sec);
            $secs = $hours * 3600 + $minutes * 60;

            sscanf($data['time'], "%d:%d:%d", $hours, $minutes, $sec);
            $secs = ($secs + ($hours * 3600 + $minutes * 60));

            $hours = floor($secs / 3600);
            $mins = floor($secs / 60 % 60);
            $time = $hours.':'.$mins.':00';

            $task_member_id = $this->Project_modal->updateTaskMember(array(
                'allowted_time' => $time
            ),$check1[0]['id_task_member']);

            $existing_task_week_flow = $this->Project_modal->getTaskWeekFlow(array(
                'task_flow_id' => $check[0]['id_task_flow'],
                'user_id' => $data['assigned_to'],
                'day_id' => $data['log_day'],
                'date' => $date
            ));
            $this->Project_modal->addTimeLine(array(
                'created_by' => $this->session->userdata('user_id'),
                'module_type' => 'task',
                'module_id' => $data['project_task_id'],
                'description' => $this->session->userdata('username').' assigned task to '.$user[0]['first_name'].' '.$user[0]['last_name'].'.'
            ));
            //echo "<pre>"; print_r($existing_task_week_flow); exit;
            if(empty($existing_task_week_flow)){
                $task_week_flow = $this->Project_modal->addTaskWeekLoad(array(
                    'task_flow_id' => $check[0]['id_task_flow'],
                    'day_id' => $data['log_day'],
                    'user_id' => $data['assigned_to'],
                    'date' => $date,
                    'time' => $data['time'],
                ));
            }
            else
            {
                $new_time = sec_to_time(time_to_sec($existing_task_week_flow[0]['time'])+time_to_sec($data['time']));
                $task_week_flow = $this->Project_modal->updateTaskWeekFlow(array(
                    'id_task_week_flow' => $existing_task_week_flow[0]['id_task_week_flow'],
                    'time' => $new_time,
                ));
            }
        }

        echo json_encode(array('status'=>TRUE,'error'=>true,'message'=>'')); exit;
    }

    public function getUserDayTasks()
    {
        $_POST['day_id'] = date('Y-m-d',$_POST['day_id']);
        unset($_POST['project_id']);
        $data = $this->Project_modal->getUserDayTasks($_POST);
        echo json_encode($data);
    }

    public function getUserDayTasksForFuture()
    {
//        $_POST['day_id'] = date('Y-m-d',$_POST['day_id']);
//        unset($_POST['project_id']);
        $data = $this->Project_modal->getUserDayTasksForFuture($_POST);
        echo json_encode($data);
    }

    public function deleteTaskWeekFlow()
    {
        $data = $this->Project_modal->getTaskWeekFlow(array('id_task_week_flow' => $_POST['id']));
        if(!empty($data))
        {
            $task_member = $this->Project_modal->getTaskMember(array('task_flow_id' => $data[0]['task_flow_id'],'assigned_to' => $data[0]['user_id']));

            sscanf($data[0]['time'], "%d:%d:%d", $hours, $minutes, $sec);
            $secs = $hours * 3600 + $minutes * 60;

            sscanf($task_member[0]['allowted_time'], "%d:%d:%d", $hours, $minutes, $sec);
            $secs = (($hours * 3600 + $minutes * 60) - $secs);

            $hours = floor($secs / 3600);
            $mins = floor($secs / 60 % 60);
            $time = $hours.':'.$mins.':00';

            $this->Project_modal->updateTaskMember(array('allowted_time' => $time),$task_member[0]['id_task_member']);
        }
        $data = $this->Project_modal->deleteTaskWeekFlow(array('id_task_week_flow' => $_POST['id']));

        if(isset($time) && time_to_sec($time)==0 && isset($task_member) && !empty($task_member)){
            $this->Project_modal->deleteTaskMember(array('id_task_member' => $task_member[0]['id_task_member']));
        }

        echo 1;
    }

    public function getUserForProject(){
        $projectId = $this->input->get('project_id');
        $data = $this->Project_modal->getUserForProject($projectId);
        //echo "<pre>"; print_r($data); exit;
        for($s=0;$s<count($data);$s++)
        {
            $log_time = $this->Project_modal->getUserLogTimeByProject(array('project_id' =>$projectId, 'user_id_arr' => $data[$s]->userid));
            $data[$s]->log_time = sec_to_time($log_time[0]['actual_time']);
        }
        if($projectId){
            echo json_encode(array('status'=>TRUE,'data'=>$data)); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Project not found')); exit;
        }
    }

    public function getMilestoneForProject(){
        $projectId = $this->input->get('project_id');
        if($projectId){
            echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->getMilestoneForProject($projectId))); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Project not found')); exit;
        }
    }

    public function getMilestoneById(){
        $milestoneId = $this->input->get('milestone_id');
        if($milestoneId){
            echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->getMilestoneById($milestoneId))); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Milestone not found')); exit;
        }
    }

    public function getOpenTaskForProject(){
        $projectId = $this->input->get('project_id');
        if($projectId){
            echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->getOpenTaskForProject($projectId))); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Project not found')); exit;
        }
    }

    public function getOpenTaskById(){
        $opentask_id = $this->input->get('opentask_id');
        if($opentask_id){
            echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->getOpenTaskById($opentask_id))); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Open task not found')); exit;
        }
    }

    public function deleteMilestone(){
        $milestoneId = $this->input->get('milestone_id');
        if($milestoneId){
            echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->deleteMilestone($milestoneId))); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Project not found')); exit;
        }
    }
    public function deleteTask(){
        $project_task_id = $this->input->get('project_task_id');
        if($project_task_id){
            //to get id_task_flow
            $taskFlowData = $this->Project_modal->getDepartmentProjectTask(array('id_project_task'=> $project_task_id));
            if($taskFlowData){
                $taskFlowId = $taskFlowData[0]['id_task_flow'];
                //Check if log exist for the same task
                if($this->Project_modal->checkLogExist(array('task_flow_id' => $taskFlowId))){
                    echo json_encode(array('status'=>FALSE,'message'=>'User already logged time')); exit;
                }else{
                    echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->deleteTask(array('project_task_id' => $project_task_id, 'task_flow_id' => $taskFlowId)),'message' => 'Task deleted successfully.')); exit;
                }
                //Check if alloted to any user
                /*$taskWeekFlowData = $this->Project_modal->getTaskWeekFlow(array('task_flow_id'=> $taskFlowId));
                if(count($taskWeekFlowData)>0){
                    echo json_encode(array('status'=>FALSE,'message'=>'Task already alloted to user')); exit;
                }else{
                    echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->deleteTask($project_task_id))); exit;
                }*/
            }else{
                echo json_encode(array('status'=>FALSE,'message'=>'Task  not found')); exit;
            }
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Task  not found')); exit;
        }
    }

    public function deleteProjectTask(){
        $project_task_id = $this->input->get('project_task_id');
        if($project_task_id){
            //to get id_task_flow
            $this->Project_modal->deleteProjectTask(array('project_task_id' => $project_task_id));
            echo json_encode(array('status'=>TRUE,'data'=>'','message' => 'Task deleted successfully.')); exit;
        }
    }

    public function updateProjectEmployee(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->Project_modal->deleteProjectDepartmenUsers($data['department_id'],$data['project_id']);
        $result = $this->Project_modal->addProjectDeptTeam($data,false);
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$result));
    }

    public function getLogTimeList(){
        $params = $this->input->get();
        $data = $this->Project_modal->getUserLogTime($params);
        echo json_encode($data);
    }
    public function getOtherActivityTimeGrid(){
        $params = $this->input->get();
        $data = $this->Project_modal->getOtherActivityTimeGrid($params);
        echo json_encode($data);
    }

    public function deleteLogTime(){
        $logId = $this->input->get('log_id');
        if($logId){
            echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->deleteLogTime($logId))); exit;
        }else{
            echo json_encode(array('status'=>FALSE,'message'=>'Log not found')); exit;
        }
    }

    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp)
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
    }

    function getUnassignedTasks()
    {
        $project_id = $_POST['project_id'];
        $status = $_POST['status'];

        $unassigned_task = $this->Project_modal->getUserTasks(array('project_id' => $project_id,'order' => 'id_task_flow'));
        //echo "<pre>"; print_r($unassigned_task); exit;
        $tasks = array();
        for($s=0;$s<count($unassigned_task);$s++)
        {
            $est_time = sec_to_time(time_to_sec($unassigned_task[$s]['estimated_time'])+time_to_sec($unassigned_task[$s]['additional_time']));
            $time = $this->Project_modal->getTaskAllotedTime($unassigned_task[$s]['project_task_id'],$est_time);

            if($status=='completed' && $unassigned_task[$s]['status'] == 'completed')
            {
                $tasks[] = array(
                    'id_task_flow' => $unassigned_task[$s]['id_task_flow'],
                    'project_task_id' => $unassigned_task[$s]['project_task_id'],
                    'project_id' => $unassigned_task[$s]['project_id'],
                    'project_name' => $unassigned_task[$s]['project_name'],
                    'task_name' => $unassigned_task[$s]['task_name'],
                    'estimated_time' => $unassigned_task[$s]['estimated_time'],
                    'additional_time' => $unassigned_task[$s]['additional_time'],
                    'status' => $unassigned_task[$s]['status'],
                    'alloted_time' => $time[0]['alloted_time'],
                    'remains' => $time[0]['remains']
                );
            }
            else if($status=='unassigned')
            {
                if(($time[0]['remains']!='00:00:00' && $time[0]['remains']!='') || $unassigned_task[$s]['estimated_time']=='00:00:00' && $unassigned_task[$s]['status'] != 'completed'  && $unassigned_task[$s]['status'] != 'approval_waiting')
                {
                    $tasks[] = array(
                        'id_task_flow' => $unassigned_task[$s]['id_task_flow'],
                        'project_task_id' => $unassigned_task[$s]['project_task_id'],
                        'project_id' => $unassigned_task[$s]['project_id'],
                        'project_name' => $unassigned_task[$s]['project_name'],
                        'task_name' => $unassigned_task[$s]['task_name'],
                        'estimated_time' => $unassigned_task[$s]['estimated_time'],
                        'additional_time' => $unassigned_task[$s]['additional_time'],
                        'status' => $unassigned_task[$s]['status'],
                        'alloted_time' => $time[0]['alloted_time'],
                        'remains' => $time[0]['remains']
                    );
                }
            }
            else if($status=='assigned')
            {
                if(time_to_sec($time[0]['alloted_time']) && time_to_sec($time[0]['alloted_time'])!=0 && time_to_sec($time[0]['remains'])==0  && $unassigned_task[$s]['status'] != 'completed'  && $unassigned_task[$s]['status'] != 'approval_waiting')
                {
                    $tasks[] = array(
                        'id_task_flow' => $unassigned_task[$s]['id_task_flow'],
                        'project_task_id' => $unassigned_task[$s]['project_task_id'],
                        'project_id' => $unassigned_task[$s]['project_id'],
                        'project_name' => $unassigned_task[$s]['project_name'],
                        'task_name' => $unassigned_task[$s]['task_name'],
                        'estimated_time' => $unassigned_task[$s]['estimated_time'],
                        'additional_time' => $unassigned_task[$s]['additional_time'],
                        'status' => $unassigned_task[$s]['status'],
                        'alloted_time' => $time[0]['alloted_time'],
                        'remains' => $time[0]['remains']
                    );
                }
            }
            else if($status=='approval_waiting')
            {
                if($unassigned_task[$s]['status']=='approval_waiting')
                {
                    $tasks[] = array(
                        'id_task_flow' => $unassigned_task[$s]['id_task_flow'],
                        'project_task_id' => $unassigned_task[$s]['project_task_id'],
                        'project_id' => $unassigned_task[$s]['project_id'],
                        'project_name' => $unassigned_task[$s]['project_name'],
                        'task_name' => $unassigned_task[$s]['task_name'],
                        'estimated_time' => $unassigned_task[$s]['estimated_time'],
                        'additional_time' => $unassigned_task[$s]['additional_time'],
                        'status' => $unassigned_task[$s]['status'],
                        'alloted_time' => $time[0]['alloted_time'],
                        'remains' => $time[0]['remains'],
                        'pending' => 1
                    );
                }
            }
        }
        echo json_encode(array('response' => 1, 'data' => $tasks, 'counts' => $this->getUnassignedTasksCounts($_POST)));
    }

    function getUnassignedTasksCounts($data)
    {
        $project_id = $data['project_id'];

        $unassigned_task = $this->Project_modal->getUserTasks(array('project_id' => $project_id));
        //echo "<pre>"; print_r($unassigned_task); exit;
        $tasks = array();
        $pending_count =0;
        $completed_count =0;
        $unassigned_count =0;
        $assigned_count =0;
        for($s=0;$s<count($unassigned_task);$s++)
        {
            $est_time = sec_to_time(time_to_sec($unassigned_task[$s]['estimated_time'])+time_to_sec($unassigned_task[$s]['additional_time']));
            $time = $this->Project_modal->getTaskAllotedTime($unassigned_task[$s]['project_task_id'],$est_time);

            if($unassigned_task[$s]['status'] == 'completed')
            {
                $completed_count++;
            }
            if(($time[0]['remains']!='00:00:00' && $time[0]['remains']!='') || $unassigned_task[$s]['estimated_time']=='00:00:00' && $unassigned_task[$s]['status'] != 'completed'  && $unassigned_task[$s]['status'] != 'approval_waiting')
            {
                $unassigned_count++;
            }
            if(time_to_sec($time[0]['alloted_time']) && time_to_sec($time[0]['alloted_time'])!=0 && time_to_sec($time[0]['remains'])==0  && $unassigned_task[$s]['status'] != 'completed'  && $unassigned_task[$s]['status'] != 'approval_waiting')
            {
                $assigned_count++;
            }
            if($unassigned_task[$s]['status']=='approval_waiting')
            {
                if($unassigned_task[$s]['status']=='approval_waiting')
                {
                    $pending_count++;
                }
            }
        }
        return array('assigned'=> $assigned_count, 'unassigned'=> $unassigned_count, 'pending'=> $pending_count, 'completed'=> $completed_count);
    }

    function fullCalenderTaskList()
    {
        $params = $this->input->get();
        //echo "<pre>"; print_r($params); exit;
        if($this->session->userdata('user_id'))
        {
            $projectData = $this->Project_modal->fullCalenderTaskList($params,'list');
            $projectList = $projectData['rows'];
            $rowCount =  $projectData['projectCount'];
            echo json_encode(array('rows'=>$projectList,'total'=>$rowCount));
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function getRequestMoreTimeGrid()
    {
        $params = $this->input->get();
        if($this->session->userdata('user_id'))
        {
            echo json_encode($this->Project_modal->getRequestMoreTimeGrid($params));
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function saveRequestMoreTime()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $dept = $this->User_model->getUserDepartments(array('user_id' => $this->session->userdata('user_id')));
        $task_details = $this->Project_modal->getProjectTaskDetails(array(
            'id_project_task' => $data['project_task_id']
        ));
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }

        if(!empty($data)){
            $check_request = $this->Project_modal->getRequestMoreTime(array(
                'project_task_id' => $data['project_task_id'],
                'employee_id' => $this->session->userdata('user_id')
            ));

            if(empty($check_request)){
                //$dept = $this->User_model->getUserDepartments(array('user_id' => $this->session->userdata('user_id')));
                $team_lead_assigned = 0;
                $users = $this->Project_modal->getUsers(array('is_lead' => 1,'department_id' => $dept[0]['department_id']));
                if(empty($users)){
                    $result = array('status'=>FALSE,'error'=>'No team lead present','data'=>'');
                    echo json_encode($result);
                }
                else
                {
                    $project_users = $this->Project_modal->getProjectUsers($task_details[0]['project_id']);

                    $project_user_ids = array_map(function($i){ return $i['id_user']; },$project_users);
                    for($sr=0;$sr<count($users);$sr++)
                    {
                        if(in_array($users[$sr]['id_user'],$project_user_ids)){
                            $team_lead_assigned = 1;
                            $data['assigned_to'] = $users[$sr]['id_user'];
                        }
                    }

                    if(!$team_lead_assigned){
                        if($task_details[0]['project_manager']=='' || $task_details[0]['project_manager']==0){
                            $result = array('status'=>FALSE,'error'=>'','data'=>'No project manager assign to this project..Please contact admin');
                            echo json_encode($result); exit;
                        }
                        $data['assigned_to'] = $task_details[0]['project_manager'];
                    }

                }

            }
            else{
                $flag = 0 ;
                for($s=0;$s<count($check_request);$s++)
                {
                    if($check_request[$s]['status']=='pending'){
                        $flag = 1;
                    }
                }

                if($flag){
                    $result = array('status'=>FALSE,'error'=>'','data'=>'Already some requested time is pending.');
                    echo json_encode($result); exit;
                }

                if($task_details[0]['project_manager']=='' || $task_details[0]['project_manager']==0){
                    $result = array('status'=>FALSE,'error'=>'','data'=>'No project manager assign to this project..Please contact admin');
                    echo json_encode($result); exit;
                }
                $data['assigned_to'] = $task_details[0]['project_manager'];
            }
        }

        if($this->Project_modal->saveRequestMoreTime($data)){
            $result = array('status'=>true,'error'=>'false','data'=>'');

            //adding time line info
            $this->Project_modal->addTimeLine(array(
                'created_by' => $this->session->userdata('user_id'),
                'module_type' => 'task',
                'module_id' => $_POST['project_task_id'],
                'description' => $this->session->userdata('username').' requested more time.'
            ));
            $currentUser = $this->User_model->getUser(array('id_user' => $this->session->userdata('user_id')));
            $this->Project_modal->addNotification(array(
                'user_id' => $data['assigned_to'],
                'subject' => 'More time',
                'message' => $currentUser[0]['first_name'].' '.$currentUser[0]['last_name'].' Asked more time for <b>'.$task_details[0]["task_name"].'</b> task',
                'url' => WEB_BASE_URL.'index.php/Project/requestMoreTimeApproval',
                'read_flag' => 1
            ));

            echo json_encode($result);
        }else{
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
    }

    public function updateEstimatedTimeForWeekWorkLoad()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }

        if($this->Project_modal->updateEstimatedTimeForWeekWorkLoad($data)){
            $result = array('status'=>true,'message'=>'Data saved','data'=>'');
            echo json_encode($result);
        }else{
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            echo json_encode($result);
        }
    }

    public function getMoreTimeRequestById($task_id){

        echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->getMoreTimeRequestById($task_id)));

    }

    public function actionRequestedTime(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        echo json_encode(array('status'=>TRUE,'data'=>$this->Project_modal->actionRequestedTime($data)));
    }

    public function getProjectTaskList()
    {
        $params = $this->input->get();

        if($this->session->userdata('user_id'))
        {
            echo json_encode($this->Project_modal->getProjectTaskGrid($params,'list'));
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function getProjectTaskDetails()
    {
        if(isset($_POST) && !empty($_POST))
        {
            $data = $this->Project_modal->getDepartmentProjectTask(array('id_project_task' => $_POST['project_task_id'],'department_id' => $this->session->userdata('department_id')));
            //echo "<pre>"; print_r($data); exit;
            echo json_encode(array('response' =>1,'data' => $data));
        }
    }
    public function updateProjectTask()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $start_date = DateTime::createFromFormat('d/m/Y', $data['start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['end_date'])->format('Y-m-d');
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['estimated_time'] = $data['estimated_time'].':00';
        $task_flow_id = $data['id_task_flow'];
        $project_id = $data['id_project'];
        $department_id = $data['department_id'];
        if(isset($data['id_task_flow'])) unset($data['id_task_flow']);
        if(isset($data['id_project'])) unset($data['id_project']);
        if(isset($data['department_id'])) unset($data['department_id']);
        //echo '<pre>'; print_r($data); exit;
        $resultData = $this->Project_modal->updateProjectTask($data);
        $departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $project_id,'department_id' => $department_id,'task_id'=>$data['id_project_task']));
        //echo '<pre>'; print_r($departmentEstimationTime); exit;
        $this->Project_modal->updateTaskFlow(array('estimated_time' => $departmentEstimationTime),$task_flow_id);

        echo json_encode(array('status'=>TRUE,'data'=>$resultData));

    }
    /*public function addProjectTask()
{
    $data = json_decode(file_get_contents("php://input"), true);
    if($data){ $_POST = $data; }
    $data = $this->input->post();

    $task['status'] = true;

    $departmentDetails =$this->Project_modal->getDepartmentDetails(['department_name' => $data['departmentName']]);

    if (empty($departmentDetails)) {
        $task['status'] = false;
        $task['message'] = "Invalid Department";
    }
    else
    {
        $projectDepartmentDetails =$this->Project_modal->getProjectDepartmentDetails(['department_id' => $departmentDetails[0]['id_department'],'project_id' => $data['project_id']]);
        if(empty($projectDepartmentDetails))
        {
            $task['status'] = false;
            $task['message'] = "Department not assigned";
        }
        else{
            $data['department_id'][] = $departmentDetails[0]['id_department'];
        }
    }
    if ($task['status']) {
        $start_date = DateTime::createFromFormat('d/m/Y', $data['start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['end_date'])->format('Y-m-d');
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['estimated_time'] = $data['estimated_time'].':00';
        if(isset($data['Field_4'])) unset($data['Field_4']);
        if(isset($data['Field_5'])) unset($data['Field_5']);
        if(isset($data['Field_6'])) unset($data['Field_6']);
        unset($data['id_project_task']);
        unset($data['id_task_flow']);
        unset($data['departmentName']);
        $data['module_name'] = $data['project_module_id'];
        unset($data['project_module_id']);
        $module_id = $this->Project_modal->createExcelModule($data);
        $data['module_id'] = $module_id;
        $task = $this->Project_modal->createExceltask($data, 'task');
        $task['status'] = true;


    }

    echo json_encode($task);
    exit;
}*/
    public function updateSingleProjectTask()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $start_date = DateTime::createFromFormat('d/m/Y', $data['start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['end_date'])->format('Y-m-d');
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
//        $data['estimated_time'] = $data['estimated_time'].':00';
        $task_flow_id = $data['id_task_flow'];
        $project_id = $data['id_project'];
        $department_id = $data['department_id'];
        if(isset($data['id_task_flow'])) unset($data['id_task_flow']);
        if(isset($data['id_project'])) unset($data['id_project']);
        if(isset($data['department_id'])) unset($data['department_id']);
        //echo '<pre>'; print_r($data); exit;
        $isUnique = $this->Project_modal->isUniqueRequirementTask(array('requirement_id'=>$data['requirement_id'],'task_name' => $data['task_name'],'id_project_task' => $data['id_project_task']));
        if(count($isUnique)>0){
            $result = array('status'=>FALSE,'error'=>true,'message'=>'Task name already assigned to same requirement');
            echo json_encode($result);exit;
        }
        $projectTask = array(
            'id_project_task' => $data['id_project_task'],
            'task_name' => $data['task_name'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            'description' => $data['description'],
            'requirement_id'=>$data['requirement_id'],
            'departments' => $department_id,
            'use_case' => $data['use_case']
        );
        $resultData = $this->Project_modal->updateProjectTask($projectTask);
        $project_departments = $data['department'];
        $totalTime = 0; $dept_est = 0;
        $task_flow_details = $this->Project_modal->getTaskFlow(array('id_task_flow' => $task_flow_id));
        for($q=0;$q<count($project_departments);$q++)
        {
            $task_dept = array(
                'project_id' => $project_id,
                'task_id' => $data['id_project_task'],
                'department_id' => $project_departments[$q]['id'],
                'time' => $project_departments[$q]['est']
            );
            $totalTime = $totalTime + time_to_sec($project_departments[$q]['est']);
            $task_department = $this->Project_modal->deletetaskDepartment($task_dept);
            $task_department = $this->Project_modal->createExceltaskDepartment($task_dept);
            if($project_departments[$q]['id']==$task_flow_details[0]['department_id'])
            {
                $dept_est = $project_departments[$q]['est'];
            }
        }

        if($totalTime){
            $totalTime = sec_to_time($totalTime);
            $this->Project_modal->updateTask(array('id_project_task' => $data['id_project_task'], 'estimated_time'=> $totalTime));
            /*$this->Project_modal->updateTaskFlow(array(
                'estimated_time' => $dept_est,
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'actual_end_date' => $data['end_date'],
            ),$task_flow_id);*/
        }

        echo json_encode(array('status'=>TRUE,'data'=>$resultData));

    }
    public function updatePendingProjectTask()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $start_date = DateTime::createFromFormat('d/m/Y', $data['start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['end_date'])->format('Y-m-d');
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $project_id = $data['id_project'];
        $department_id = $data['department_id'];
        $project_departments = $data['department'];
        $totalTime = 0;
        if(isset($data['id_task_flow'])) unset($data['id_task_flow']);
        if(isset($data['id_project'])) unset($data['id_project']);
        if(isset($data['department_id'])) unset($data['department_id']);
        $projectTask = array(
            'id_project_task' => $data['id_project_task'],
            'task_name' => $data['task_name'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            'description' => $data['description'],
            'departments' => $department_id,
            'estimated_time' => sec_to_time(time_to_sec($data['estimated_time'])),
            'requirement_id' => $data['requirement_id'],
            'use_case' => $data['use_case']
        );
        $resultData = $this->Project_modal->updateProjectTask($projectTask);

        for($q=0;$q<count($project_departments);$q++)
        {
            $task_dept = array(
                'project_id' => $project_id,
                'task_id' => $data['id_project_task'],
                'department_id' => $project_departments[$q]['id'],
                'time' => $project_departments[$q]['est']
            );
            $totalTime = $totalTime + time_to_sec($project_departments[$q]['est']);
            $task_department = $this->Project_modal->deletetaskDepartment($task_dept);
            $task_department = $this->Project_modal->createExceltaskDepartment($task_dept);
        }

        if($totalTime){
            $totalTime = sec_to_time($totalTime);
            $this->Project_modal->updateTask(array('id_project_task' => $data['id_project_task'], 'estimated_time'=> $totalTime));
        }

        echo json_encode(array('status'=>TRUE,'data'=>$resultData));
    }
    public function addProjectTask()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $task['status'] = true;
        $project_id = $data['project_id'];

        $start_date = DateTime::createFromFormat('d/m/Y', $data['start_date'])->format('Y-m-d');
        $end_date = DateTime::createFromFormat('d/m/Y', $data['end_date'])->format('Y-m-d');
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        if(isset($data['Field_4'])) unset($data['Field_4']);
        if(isset($data['Field_5'])) unset($data['Field_5']);
        if(isset($data['Field_6'])) unset($data['Field_6']);
        unset($data['id_project_task']);
        unset($data['id_task_flow']);
        unset($data['departmentName']);
//        $data['module_name'] = $data['project_module_id'];
        unset($data['project_module_id']);

        $isUnique = $this->Project_modal->isUniqueRequirementTask(array('requirement_id'=>$data['requirement_id'],'task_name' => $data['task_name']));
        if(count($isUnique)>0){
            $result = array('status'=>FALSE,'error'=>true,'message'=>'Task name already assigned to same requirement');
            echo json_encode($result);exit;
        }

        $module_id = $this->Project_modal->createExcelModule($data);

        $data['module_id'] = $module_id;
        $task = $this->Project_modal->createTask($data, 'task');
        $taskId = $task['task_id'];
        $task['status'] = true;

        $totalTime = 0;
        $project_departments = $data['department'];
        for($q=0;$q<count($project_departments);$q++)
        {
            $task['status'] = true;
            $task_dept = array(
                'project_id' => $project_id,
                'task_id' => $task['task_id'],
                'department_id' => $project_departments[$q]['id'],
                'time' => $project_departments[$q]['est']
            );
            $totalTime = $totalTime + time_to_sec($project_departments[$q]['est']);
            $task_department = $this->Project_modal->createExceltaskDepartment($task_dept);
        }
        if($totalTime){
            $totalTime = sec_to_time($totalTime);
            $this->Project_modal->updateTask(array('id_project_task' => $taskId, 'estimated_time'=> $totalTime));
            /*$projectDetails = $this->Project_modal->getTaskFlow(array('project_task_id' => $taskId));
            $flowId = $projectDetails[0]['id_task_flow'];
            $this->Project_modal->updateTaskFlow(array('estimated_time'=> $totalTime) , $flowId);*/
        }
        echo json_encode($task);
        exit;
    }
    public function getProjectMilestoneList($projectId){
        $list = $this->Project_modal->getProjectMilestoneList($projectId);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$list));
    }
    public function updateTask()
    {

        $this->Project_modal->updateProjectTask(array(
            'id_project_task' => $_POST['project_task_id'],
            'task_name' => $_POST['task_name'],
            'description' => $_POST['description']
        ));
        $task_flow = array(
            'estimated_time' => $_POST['estimated_time'].':00',
            'is_forward' => $_POST['is_forward']=='true'?'1':'0');
        $this->Project_modal->updateTaskFlow($task_flow,
            $_POST['task_flow_id']);



        echo json_encode(array('status'=>TRUE,'error'=>true,'message'=>'')); exit;
    }

    public function updateTaskByTeamLead()
    {
        //echo "<pre>"; print_r($_POST); exit;
        $this->Project_modal->updateProjectTask(array(
            'id_project_task' => $_POST['project_task_id'],
            'task_name' => $_POST['task_name'],
            'description' => $_POST['description']
        ));
        $task_flow = array(
            'estimated_time' => $_POST['estimated_time'].':00',
            'is_forward' => $_POST['is_forward']=='true'?'1':'0');
        $this->Project_modal->updateTaskFlow($task_flow,
            $_POST['task_flow_id']);

        $message = 'Updated successfully.';
        $id_task_flow = $_POST['task_flow_id'];
        $status = $_POST['task_status'];
        /*if(isset($_POST['task_status']) && $_POST['task_status']=='completed')
        {
            $id_task_flow = $_POST['task_flow_id'];
            //echo "<pre>"; print_r($id_task_flow); exit;
            $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
            if($task_flow[0]['task_status']=='approval_waiting')
            {
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'assigned_to' => $this->session->userdata('user_id')));

                $this->Project_modal->updateTaskMember(array('task_status' => 'completed'),$task_members[0]['id_task_member']);
                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'status_not' => 'completed'));
                if(empty($task_members)){

                    //if user completed the task
                    $this->Project_modal->updateTaskFlow(array('actual_end_date' => date('Y-m-d'), 'task_status' => 'completed'),$id_task_flow);
                    $next_dept = 0;
                    $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                    $project_dept = $this->Project_modal->getProjectDepartments($task_flow[0]['project_id']);
                    for($s=0;$s<count($project_dept);$s++){
                        if($project_dept[$s]['department_id']==$task_flow[0]['department_id'] && $task_flow[0]['is_forward']){
                            if(isset($project_dept[$s+1]['department_id'])){
                                $next_dept = $project_dept[$s+1]['department_id'];
                            }
                        }
                    }

                    if($next_dept){
                        $taskDetails = $this->Project_modal->getTaskDetails(array('id_project_task' => $task_flow[0]['project_task_id']));
                        //$departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));
                        $task_department = $this->Project_modal->getTaskDepartment(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));

                        if(empty($task_department)){
                            $departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));
                        }
                        else{
                            $departmentEstimationTime = $task_department[0]['time'];
                        }

                        $this->Project_modal->addTaskFlow(array(
                            'project_task_id' => $task_flow[0]['project_task_id'],
                            'department_id' => $next_dept,
                            'estimated_time' => $departmentEstimationTime,
                            'start_date' => $task_flow[0]['start_date'],
                            'end_date' => $task_flow[0]['end_date'],
                        ));

                        $message = 'Task Forwarded to next department successfully.';

                        //adding time line info
                        $this->Project_modal->addTimeLine(array(
                            'created_by' => $this->session->userdata('user_id'),
                            'module_type' => 'task',
                            'module_id' => $task_flow[0]['project_task_id'],
                            'description' => $this->session->userdata('username').' has been completed the task'
                        ));
                    }
                    else{
                        $this->Project_modal->updateProjectTask(array(
                            'id_project_task' => $task_flow[0]['project_task_id'],
                            'status' => 'completed',
                            'completed_date' => date('Y-m-d')
                        ));

                        $message = 'Task completed successfully.';
                        //adding time line info
                        $this->Project_modal->addTimeLine(array(
                            'created_by' => $this->session->userdata('user_id'),
                            'module_type' => 'task',
                            'module_id' => $task_flow[0]['project_task_id'],
                            'description' => $this->session->userdata('username').' has been completed the task'
                        ));
                    }
                }
            }

        }*/
        if($status=='progress')
        {
            $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow));

            //updating main task for in progress
            $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
            for($m=0;$m<count($task_members);$m++) {
                $this->Project_modal->updateTaskMember(array('task_status' => 'progress'), $task_members[$m]['id_task_member']);
            }
            $this->Project_modal->updateTaskFlow(array('task_status' => 'progress'),$id_task_flow);
            $message = 'Task status has been changed.';
        }
        else if($status=='completed')
        {
            $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
            if($task_flow[0]['task_status']=='approval_waiting')
            {



                $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow,'status_not' => 'approval_waiting'));
                if(empty($task_members)){


                    $task_members = $this->Project_modal->getTaskMembers(array('task_flow_id' => $id_task_flow));
                    for($m=0;$m<count($task_members);$m++)
                    {
                        $this->Project_modal->updateTaskMember(array('task_status' => 'completed', 'updated_by'=> $this->session->userdata('user_id'), 'updated_date_time'=> date('Y-m-d') ),$task_members[$m]['id_task_member']);
                    }

                    //if user completed the task
                    $this->Project_modal->updateTaskFlow(array('actual_end_date' => date('Y-m-d'), 'task_status' => 'completed'),$id_task_flow);
                    $next_dept = 0;
                    $task_flow = $this->Project_modal->getTaskFlow(array('id_task_flow' => $id_task_flow));
                    $project_dept = $this->Project_modal->getProjectDepartments($task_flow[0]['project_id']);
                    for($s=0;$s<count($project_dept);$s++){
                        if($project_dept[$s]['department_id']==$task_flow[0]['department_id'] && $task_flow[0]['is_forward']){
                            if(isset($project_dept[$s+1]['department_id'])){
                                $next_dept = $project_dept[$s+1]['department_id'];
                            }
                        }
                    }

                    if($next_dept){
                        $taskDetails = $this->Project_modal->getTaskDetails(array('id_project_task' => $task_flow[0]['project_task_id']));
                        //$departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));
                        $task_department = $this->Project_modal->getTaskDepartment(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));

                        if(empty($task_department)){
                            $departmentEstimationTime = $this->Project_modal->getDepartmentEstimationTime(array('project_id' => $taskDetails[0]['project_id'],'department_id' => $next_dept,'task_id'=> $task_flow[0]['project_task_id']));
                        }
                        else{
                            $departmentEstimationTime = $task_department[0]['time'];
                        }

                        $this->Project_modal->addTaskFlow(array(
                            'project_task_id' => $task_flow[0]['project_task_id'],
                            'department_id' => $next_dept,
                            'estimated_time' => $departmentEstimationTime,
                            'start_date' => $task_flow[0]['start_date'],
                            'end_date' => $task_flow[0]['end_date'],
                        ));

                        $message = 'Task Forwarded to next department successfully.';

                        //adding time line info
                        $this->Project_modal->addTimeLine(array(
                            'created_by' => $this->session->userdata('user_id'),
                            'module_type' => 'task',
                            'module_id' => $task_flow[0]['project_task_id'],
                            'description' => $this->session->userdata('username').' has been completed the task'
                        ));
                    }
                    else{
                        $this->Project_modal->updateProjectTask(array(
                            'id_project_task' => $task_flow[0]['project_task_id'],
                            'status' => 'completed',
                            'completed_date' => date('Y-m-d')
                        ));

                        $message = 'Task completed successfully.';
                        //adding time line info
                        $this->Project_modal->addTimeLine(array(
                            'created_by' => $this->session->userdata('user_id'),
                            'module_type' => 'task',
                            'module_id' => $task_flow[0]['project_task_id'],
                            'description' => $this->session->userdata('username').' has been completed the task'
                        ));
                    }
                }
            }
        }

        echo json_encode(array('status'=>TRUE,'error'=>true,'message'=>$message)); exit;
    }

    public function updateSessionDepartment()
    {
        if(isset($_POST['dept_id'])){
            $dept = explode('@@@@',$_POST['dept_id']);
            $this->session->set_userdata('department_id',$dept[0]);
            $this->session->set_userdata('department_name',$dept[1]);
            $this->session->set_userdata('is_lead',$dept[2]);

            echo json_encode(array('response' =>1,'data'=>''));
        }
    }

    public function userTasksByStatus()
    {
        $params = $this->input->get();
        $list = $this->Project_modal->userTasksByStatus($params);
        echo json_encode($list);
    }

    public function ProjectTaskDetails()
    {
        $params = $this->input->get();
        $project_id = $params['project_id'];
        //$project_id = 1;
        $departments = $this->Project_modal->getProjectDepartments($project_id);
        $task = $this->Project_modal->getProjectTaskDetails(array('project_id' => $project_id));
        $resultArray = array();
        for($s=0;$s<count($task);$s++)
        {
            $resultArray[$task[$s]['id_project_task']]['id_project_task'] = $task[$s]['id_project_task'];
            $resultArray[$task[$s]['id_project_task']]['task_name'] = $task[$s]['task_name'];
            $resultArray[$task[$s]['id_project_task']]['description'] = $task[$s]['description'];
            for($r=0;$r<count($departments);$r++)
            {
                $task_flow_details = $this->Project_modal->getTimeFromTaskFlow(array('project_task_id' => $task[$s]['id_project_task'],'department_id' => $departments[$r]['department_id']));
                $detail= '--';
                if(!empty($task_flow_details)){
                    $log_time = $this->Project_modal->getTotalLogTime(array('task_flow_id' => $task_flow_details[0]['id_task_flow']));
                    $detail = 'Status : '.$task_flow_details[0]['task_status'].'</br>';
                    $detail.= 'Estimated time : '.$task_flow_details[0]['estimated_time'].'</br>';
                    $detail.= 'Additional time : '.$task_flow_details[0]['additional_time'].'</br>';
                    if(!empty($log_time)){
                        $detail.= 'Actual time : '.sec_to_time(time_to_sec($log_time[0]['actual_time'])).'</br>';
                    }
                    $detail.= 'Start Date : '.date('d-m-Y',strtotime($task_flow_details[0]['created_date_time'])).'</br>';
                    //$detail.= 'End Date : '.date('d-m-Y',strtotime($task_flow_details[0]['end_date'])).'</br>';
                    if($task_flow_details[0]['task_status']=='completed' && $task_flow_details[0]['actual_end_date']!='')
                        $detail.= 'Completed Date : '.date('d-m-Y',strtotime($task_flow_details[0]['actual_end_date'])).'</br>';
                    else
                        $detail.= 'Completed Date : ---</br>';
                }
                $resultArray[$task[$s]['id_project_task']][$departments[$r]['department_name']] = $detail;
            }
        }
        echo json_encode(array('response' =>1,'data'=>$resultArray));
    }

    public function changeDepartmentSequence(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $data1 = [];
        $data2 = [];
        $res = $this->Project_modal->getProjectDepartmentId($data);
        if($res[0]->id_project_department){
            $newId = $res[0]->id_project_department;
            $data1 = $this->Project_modal->getProjectDepartmentData($newId)[0];
            $data2 = $this->Project_modal->getProjectDepartmentData($data['dropable_id'])[0];

            $this->Project_modal->updateProjectDepartmentById($data['dropable_id'], $data1);
            $this->Project_modal->updateProjectDepartmentById($newId, $data2);
        }
        echo json_encode(array('response' =>true,'data'=>[]));
    }

    public function getRejectTask()
    {
        $prev_dept = 0;
        $task_flow_id = $_POST['task_flow_id'];
        $project_task_id = $_POST['project_task_id'];
        $department_id = $this->session->userdata('department_id');
        $task_details = $this->Project_modal->getTaskDetails(array('id_task_flow'=>$task_flow_id,'id_project_task' => $project_task_id));
        $project_departments = $this->Project_modal->getProjectDepartments($task_details[0]['project_id']);
        for($s=0;$s<count($project_departments);$s++)
        {
            if($project_departments[$s]['department_id']==$department_id){
                if(isset($project_departments[$s-1]['department_id'])){
                    $prev_dept = $project_departments[$s-1]['department_id'];
                }
            }
        }
        //echo $prev_dept;exit;
        if($prev_dept==0){
            echo json_encode(array('response' =>0,'data'=>'There is no previous department to forward')); exit;
        }
        else
        {
            $prev_dept_task_details = $this->Project_modal->getTaskDetails(array('tw_department_id'=>$prev_dept,'id_project_task' => $project_task_id));
            if(!empty($prev_dept_task_details)){

                $this->Project_modal->deleteTaskWeekFlow(array('task_flow_id' => $task_flow_id));
                $this->Project_modal->deleteTaskMember(array('task_flow_id' => $task_flow_id));
                $this->Project_modal->deleteTaskFlow(array('task_flow_id' => $task_flow_id));

                $this->Project_modal->updateTaskFlow(array('task_status' => 'progress'),$prev_dept_task_details[0]['id_task_flow']);
                $this->Project_modal->updateTaskMemberByTaskFlowId(array('task_status' => 'progress'),$prev_dept_task_details[0]['id_task_flow']);
            }
            else
            {
                echo json_encode(array('response' =>0,'data'=>'There is no previous department to forward')); exit;
            }
        }

        echo json_encode(array('response' =>1,'data'=>'Task Forwarded Successfully.')); exit;
    }

    public function MemberTask()
    {
        $data['content'] = 'project/member_task';
        $this->load->view('partials/landing',$data);
    }

    public function getDepartmentProjects()
    {
        $projectList = $this->Project_modal->getDepartmentProjects(array('department_id' => $this->session->userdata('')));
        echo json_encode($projectList);
    }

    public function getMembersTaskGrid()
    {
        $params = $this->input->get();
        $list = $this->Project_modal->getMembersTaskGrid($params);
        if(count($list)>0){
            for($s=0;$s<count($list['rows']);$s++)
            {
                $log_time = $this->Project_modal->getLogTimeByUser(array('user_id' => $list['rows'][$s]->user_id,'task_flow_id' => $list['rows'][$s]->task_flow_id,'date' => $list['rows'][$s]->date));
                $list['rows'][$s]->logtime = $log_time[0]['log_time'];
            }
        }
        /*  $params = $this->input->get();
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
//        echo '<pre>'; print_r($data); exit;
//        $list = $this->Project_modal->getMembersTaskGrid($params);
        $list = $this->Project_modal->getMembersTaskGrid($data);
        if(count($list)>0){
            for($s=0;$s<count($list['rows']);$s++)
            {
                $log_time = $this->Project_modal->getLogTimeByUser(array('user_id' => $list['rows'][$s]->user_id,'task_flow_id' => $list['rows'][$s]->task_flow_id,'date' => $list['rows'][$s]->date));
                $list['rows'][$s]->logtime = $log_time[0]['log_time'];
            }
        }*/

        echo json_encode($list);
    }

    public function taskFullDetails()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $result = [];
        $result['task_department'] = $this->Project_modal->getProjectDepartmentsEstTime(array('task_id' => $data['task_id']));
        $result['task_details'] = $this->Project_modal->getTaskDetails(array('id_project_task' => $data['task_id']));
        $task_time_details = $this->Project_modal->getTaskCompleteTime(array('project_task_id' => $data['task_id']));
        //echo "<pre>"; print_r($result['task_details']); exit;
        $task_logtime_details = $this->Project_modal->getTaskLogTime(array('project_task_id' => $data['task_id']));
        //echo $this->db->last_query(); exit;
        if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)) {
            $result['task_details'][0]['estimated_time'] = $task_time_details[0]['task_time'];
        }
        else
            $result['task_details'][0]['estimated_time'] = $task_time_details[0]['estimated_time'];
        $result['task_details'][0]['additional_time'] = $task_time_details[0]['additional_time'];
        $result['task_details'][0]['log_time'] = $task_logtime_details[0]['actual_time'];
        if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){
            $task_time_details[0]['total_time'] = $task_time_details[0]['task_time'];
            $t_time = sec_to_time(time_to_sec($task_time_details[0]['total_time']) + time_to_sec($task_time_details[0]['additional_time']));
            $result['task_details'][0]['remain_time'] = sec_to_time(time_to_sec($t_time)-time_to_sec($result['task_details'][0]['log_time']));
        }
        else {
            $task_time_details[0]['total_time'] = sec_to_time(time_to_sec($task_time_details[0]['estimated_time']) + time_to_sec($task_time_details[0]['additional_time']));
            $result['task_details'][0]['remain_time'] = sec_to_time(time_to_sec($task_time_details[0]['total_time'])-time_to_sec($result['task_details'][0]['log_time']));
        }
        //echo $task_time_details[0]['total_time'];exit;

        $result['tskDetails'] = $this->Project_modal->getUsersTaskDetailsByTaskId(array('project_task_id' => $data['task_id']));
        $result['time_line'] = $this->Project_modal->getTimeLine(array('module_id' => $data['task_id'],'module_type' => 'task'));
        $check_list = array();
        foreach($result['tskDetails'] as $k=>$v){
//            echo '<pre>'; print_r($v); exit;
            $allotted_time = $this->Project_modal->getAllottedTimeOfUser(array('user_id' => $v['id_user'],'project_task_id' => $data['task_id']));
            $logTime = $this->Project_modal->getLogTimeByUser(array('user_id' => $v['id_user'],'project_task_id' => $data['task_id']));
            $result['tskDetails'][$k]['log_time'] = $logTime[0]['log_time'];
            $result['tskDetails'][$k]['allotted_time'] = $allotted_time[0]['allotted_time'];
            $result['tskDetails'][$k]['est_time'] = $allotted_time[0]['est_time'];
            $result['tskDetails'][$k]['add_time'] = $allotted_time[0]['add_time'];
            $check_list_data = $this->Project_modal->getAllCheckList(array('project_task_id' => $data['task_id'],'user_id' => $v['id_user']));
            $check_list[] = array(
                'user_name' => $v['name'],
                'user_id' => $v['id_user'],
                'approved_by' => $v['approved_by'],
                'approved_date' => $v['updated_date_time'],
                'check_list' => $check_list_data
            );
        }

        $use_case = $this->Project_modal->getProjectTaskDetails(array('id_project_task' => $data['task_id']));
        $result['use_cases'] = $use_case[0]['use_case'];
        $result['attachment'] = $this->Project_modal->getTaskAttachments(array('project_task_id' => $data['task_id']));
        $result['checkList'] = $check_list;
        $data=$this->load->view('modals/task_full_details',$result, TRUE);
        $this->output->set_output($data);
    }

    public function taskMemeberModal(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['user_id'])){
            $department = $this->User_model->getUserDepartments(array('user_id' => $data['user_id']));
            if(count($department)>0){
                $department = $department[0]['department_id'];
            }
        }else{
            $department = $this->session->userdata('department_id');
        }

        $result = [];
        $result['task_details'] = $this->Project_modal->getTaskDetails(array('id_project_task' => $data['task_id']));

        $task_time_details = $this->Project_modal->getTaskCompleteTime(array('project_task_id' => $data['task_id'],'department_id' => $department));
        //echo "<pre>"; print_r($task_time_details); exit;
        $task_logtime_details = $this->Project_modal->getTaskLogTime(array('project_task_id' => $data['task_id'],'department_id' => $department));

        $result['task_details'][0]['estimated_time'] = $task_time_details[0]['estimated_time'];
        $result['task_details'][0]['additional_time'] = $task_time_details[0]['additional_time'];
        $result['task_details'][0]['log_time'] = $task_logtime_details[0]['actual_time'];
        $task_time_details[0]['total_time'] = sec_to_time(time_to_sec($task_time_details[0]['estimated_time'])+time_to_sec($task_time_details[0]['additional_time']));
        //echo $task_time_details[0]['total_time'];exit;
        $result['task_details'][0]['remain_time'] = sec_to_time(time_to_sec($task_time_details[0]['total_time'])-time_to_sec($task_logtime_details[0]['actual_time']));
        if($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_lead')==1) {
            $result['tskDetails'] = $this->Project_modal->getUsersTaskDetailsByTaskId(array('project_task_id' => $data['task_id'],'department_id' => $this->session->userdata('department_id')));
        }
        else
            $result['tskDetails'] = $this->Project_modal->getUsersTaskDetailsByTaskId(array('project_task_id' => $data['task_id']));
        foreach($result['tskDetails'] as $k=>$v){
            $logTime = $this->Project_modal->getLogTimeByUser(array('user_id' => $v['id_user'], 'task_flow_id' => $v['task_flow_id']));
            $result['tskDetails'][$k]['log_time'] = $logTime[0]['log_time'];
            $check_list_data = $this->Project_modal->getAllCheckList(array('project_task_id' => $data['task_id'],'user_id' => $v['id_user']));
            $check_list[] = array(
                'user_name' => $v['name'],
                'user_id' => $v['id_user'],
                'check_list' => $check_list_data
            );
        }
        $result['time_line'] = $this->Project_modal->getTimeLine(array('module_id' => $data['task_id'],'module_type' => 'task','department_id' => $department));
        $use_case = $this->Project_modal->getProjectTaskDetails(array('id_project_task' => $data['task_id']));
        $result['use_cases'] = $use_case[0]['use_case'];
        $result['attachment'] = $this->Project_modal->getTaskAttachments(array('project_task_id' => $data['task_id'], 'department_id' => $department));
        $result['checkList'] = $check_list;

        $data=$this->load->view('modals/taskMemeberDetails',$result, TRUE);
        $this->output->set_output($data);
    }

    public function userTask(){
        $data['content'] = 'project/user_task';
        $this->load->view('partials/landing',$data);
    }

    public function userTaskGrid(){
//        $params = $this->input->get();

        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $list = $this->Project_modal->userTaskGrid($data);
        if(count($list)>0) {
            for ($s = 0; $s < count($list['rows']); $s++) {
                $log_time = $this->Project_modal->getLogTimeByUser(array('user_id' => $list['rows'][$s]->user_id, 'task_flow_id' => $list['rows'][$s]->task_flow_id, 'date' => $list['rows'][$s]->date));
                $list['rows'][$s]->logtime = $log_time[0]['log_time'];
            }
        }

        echo json_encode($list);exit;
    }

    function getDepartmentByProject($projectId){
        $result = [];
        $result['department'] = $this->Project_modal->getProjectDepartments($projectId);

        $data=$this->load->view('modals/departmentList',$result, TRUE);
        $this->output->set_output($data);
    }

    function getProjectDepartmentsEstTime(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $result = [];
        $result['department'] = $this->Project_modal->getProjectDepartments($data['project_id']);
        if(count($result['department'])>0){
            foreach($result['department'] as $k=>$v){
                $res = $this->Project_modal->getProjectDepartmentsEstTime(array('project_id' => $data['project_id'], 'task_id' => $data['task_id'], 'department_id' => $v['department_id']));
                if(count($res)>0){
                    $result['department'][$k]['estimated_time'] = $res[0]['time'];
                }

            }
        }

        $data=$this->load->view('modals/departmentList',$result, TRUE);
        $this->output->set_output($data);
    }

    public function getProjectModuleList($projectId){
        $list = $this->Project_modal->getProjectModuleList($projectId);
        echo json_encode(array('status'=>true,'error'=>false,'data'=>$list));exit;
    }

    public function userWorkRequest(){
        $data['content'] = 'project/user_work_request';
        $this->load->view('partials/landing',$data);
    }

    public function getWorkRequestGrid(){
        $params = $this->input->get();
        $list = $this->Project_modal->getWorkRequestGrid($params);
        echo json_encode($list);exit;
    }

    public function addWorkRequest(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $id = $data['id_work_request'];
        if($id == null && $id == '') { unset($data['id_work_request']); }

        $isSend = false;

        if(isset($data['date']))
            $data['date'] = DateTime::createFromFormat('d/m/Y', $data['date'])->format('Y-m-d');

        $to = '';
        $currentUser = $this->User_model->getUser(array('id_user' => $this->session->userdata('user_id')));
        $user = $this->User_model->getUser(array('id_user' => $data['approved_by']));
        if(count($user)>0){
            $to = trim($user[0]['email']);
        }
        unset($user);

        if($id == null && $id == '') {
            $data['user_id'] = $this->session->userdata('user_id');

            if(!$this->Project_modal->checkApprovedHoliday($data)){
                $userRes = $this->Project_modal->addWorkRequest($data);
                $isSend = true;
                $isUpdate = false;
            }else{
                echo json_encode(array('status'=>false,'error'=>true,'message'=>"Already requested for the same date"));exit;
            }
        }else{
            $userRes = $this->Project_modal->updateWorkRequest($id , $data);
            $isSend = true;
            $isUpdate = true;
            $result = $this->Project_modal->getWorkRequestById(array('id_work_request'=>$id));
            $data['date'] = $result->org_date;
            $data['description'] = $result->description;
        }
        if($isSend){
            $userDetails = $user = $this->User_model->getUser(array('id_user' => $data['user_id']));
            if(count($user)>0){
                $user = $user[0]['first_name'].' '.$user[0]['last_name'];
            }else{
                $user = '';
            }
            $user_email = '';
            if(isset($_SESSION['email'])){
                $user_email = ','.$_SESSION['email'];
            }
            //get template
            $date = date("d F Y");
            $data['date'] = date("d F Y" , strtotime($data['date']));
            if(!$isUpdate){
                $title = 'Work Request by '.$user.' on '.$data['date'].' to Project Manager';
                $html=$this->load->view('modals/emailTemplate',array('name' => $user, 'description' => $data['description'], 'date' => $date, 'request_for' => $data['date']), TRUE);
            } else {
                $to = $userDetails[0]['email'];
                $title = 'Work Response by Project Manager on '.$data['date'].' to '.$user;
                $html=$this->load->view('modals/emailTemplate',array(
                    'name' => $user,
                    'description' => $data['description'],
                    'date' => $date,
                    'request_for' => $data['date'],
                    'admin_comments' => $data['admin_comments'],
                    'status'=> $data['status'] ), TRUE);
            }

            $this->load->library('SendEmail');
            //send email if no error found
            $sendemail = new sendemail;
            $res = $sendemail->sendMail($to.','.$user_email, $title , $html, array());

            //notification
            $this->Project_modal->addNotification(array(
                'user_id' => $data['approved_by'],
                'subject' => 'Requirement',
                'message' => 'New Work request from '.$currentUser[0]['first_name'].' '.$currentUser[0]['last_name'],
                'url' => WEB_BASE_URL.'index.php/Project/userWorkApproval',
                'read_flag' => 1
            ));

        }
        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$userRes));exit;
    }

    public function deleteWorkRequestById(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $result = $this->Project_modal->isWorkRequestStatusChange($data['id_work_request']);
        if($result == 'notfound'){
            echo json_encode(array('status'=>false,'error'=>true,'message'=>"Request not found"));exit;
        }else if ($result == 'pending'){
            $projectList = $this->Project_modal->deleteWorkRequestById($data['id_work_request']);
            echo json_encode(array('status'=>true,'error'=>false,'data'=>$projectList));exit;
        }else if ($result == 'approved' || $result == 'declined'){
            echo json_encode(array('status'=>false,'error'=>true,'message'=>"Unable to delete, Project Manager $result request"));exit;
        }
    }

    public function userWorkApproval(){
        $data['content'] = 'project/user_work_approval';
        $this->load->view('partials/landing',$data);
    }

    public function requestMoreTimeApproval(){
        $data['content'] = 'project/request_more_time';
        $this->load->view('partials/landing',$data);
    }

    public function getAllWorkRequestGrid(){
        $params = $this->input->get();
        $list = $this->Project_modal->getAllWorkRequestGrid($params);
        echo json_encode($list);exit;
    }

    public function getWorkRequestById(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        echo json_encode(array('status'=>true,'error'=>false,'data'=>$this->Project_modal->getWorkRequestById($data)));exit;
    }

    public function projectRequirement($project_id)
    {
        $list = $this->Project_modal->getProjectRequirement(array('project_id' => $project_id));
        echo json_encode(array('status' => 1, 'data' =>$list));exit;
    }

    public function projectTaskList()
    {
        $params = $this->input->get();
        if($this->session->userdata('user_id'))
        {
            echo json_encode($this->Project_modal->getProjectTaskListGrid($params,'list'));
        }
        else{
            redirect(WEB_BASE_URL);
        }
    }

    public function approveProjectTask()
    {
        $task = $this->Project_modal->getProjectTask(array('id_project_task' => $_POST['task_id']));
        $estimated_time = $this->Project_modal->getProjectDepartmentsEstTime(array('task_id'=> $task[0]['id_project_task']));

        for($s=0;$s<count($estimated_time);$s++)
        {
            if($task[0]['departments']==$estimated_time[$s]['department_id'])
            {
                if(time_to_sec($estimated_time[$s]['esti_time'])==0){
                    if(isset($estimated_time[$s+1]['department_id'])){
                        $task[0]['departments'] = $estimated_time[$s+1]['department_id'];
                    }
                }
                else{
                    $taskWorkData = array(
                        'project_task_id'=>$task[0]['id_project_task'],
                        'estimated_time'=> $estimated_time[$s]['time'],
                        'start_date'=>$task[0]['start_date'],
                        'end_date'=>$task[0]['end_date'],
                        'actual_end_date'=>$task[0]['end_date'],
                        'department_id'=>$estimated_time[$s]['department_id']
                    );
                    $this->db->insert('task_flow',$taskWorkData);
                }
            }
        }


        if(!isset($taskWorkData)){
            echo json_encode(array('status' => false, 'data' =>'','message' => 'Please check departments time'));exit;
        }

        $list = $this->Project_modal->approveProjectTask(array(
            'id_project_task' => $_POST['task_id'],
            'pm_approved' => 1
        ));

        echo json_encode(array('status' => 1, 'data' =>''));exit;
    }

    public function approveMultipleProjectTask()
    {
        foreach($_POST['task_id'] as $val){
            $task = $this->Project_modal->getProjectTask(array('id_project_task' => $val));
            $estimated_time = $this->Project_modal->getProjectDepartmentsEstTime(array('department_id' => $task[0]['departments'],'task_id'=> $task[0]['id_project_task']));
            $taskWorkData = array(
                'project_task_id'=>$task[0]['id_project_task'],
                'estimated_time'=> $estimated_time[0]['time'],
                'start_date'=>$task[0]['start_date'],
                'end_date'=>$task[0]['end_date'],
                'actual_end_date'=>$task[0]['end_date'],
                'department_id'=>$task[0]['departments']
            );
            $this->db->insert('task_flow',$taskWorkData);

            $list = $this->Project_modal->approveProjectTask(array(
                'id_project_task' => $val,
                'pm_approved' => 1
            ));
        }

        echo json_encode(array('status' => 1, 'data' =>''));exit;
    }

    public function getRequestMoreTime()
    {
        $params = $this->input->get();
        $list = $this->Project_modal->getAllRequestMoreTimeGrid($params);
        echo json_encode($list);exit;
    }

    public function requestMoreTimeAction()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $update_data = array(
            'Id_request_more_time' => $data['id_request_more_time'],
            'admin_comments' => $data['admin_comments'],
            'status' => $data['status'],
            'approved_by' => $this->session->userdata('user_id')
        );
        $this->Project_modal->updateRequestMoreTime($update_data);

        if($data['status']=='approved')
        {
            $task_flow = $this->Project_modal->getRequestMoreTimeTaskFlow(array('Id_request_more_time' => $data['id_request_more_time']));
            //echo "<pre>"; print_r($task_flow);

                $old_additional_time = $task_flow[0]['additional_time'];
                if($old_additional_time==''){
                    $old_additional_time='00:00:00';
                }
                /*if($old_additional_time!='00:00:00' && $old_additional_time==null){
                    //
                } else if($old_additional_time=='00:00:00' && $old_additional_time!=null){
                    //
                } else{
                    $new_time = time_to_sec($task_flow[0]['duration'])+time_to_sec($old_additional_time);
                    $new_time = sec_to_time($new_time);
                }*/
            $new_time = time_to_sec($task_flow[0]['duration'])+time_to_sec($old_additional_time);
            $new_time = sec_to_time($new_time);
                //echo "<pre>"; print_r($new_time); exit;
            $this->Project_modal->updateTaskFlow(array('additional_time' => $new_time),$task_flow[0]['task_flow_id']);
            $task_member = $this->Project_modal->getTaskMembers(array('task_flow_id' => $task_flow[0]['task_flow_id'],'assigned_to' => $task_flow[0]['employee_id']));

            $this->Project_modal->updateTaskMemberByTask(array(
                'id_task_member' => $task_member[0]['id_task_member'],
                'allowted_time' => sec_to_time(time_to_sec($task_flow[0]['duration'])+time_to_sec($task_member[0]['allowted_time'])),
                'additional_time' => sec_to_time(time_to_sec($task_flow[0]['duration'])+time_to_sec($task_member[0]['additional_time']))
            ));
        }

        echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>''));exit;
    }

    public function getTaskTimeDetails()
    {
        $task_time_details = $this->Project_modal->getTaskTimeDetailsByUserId(array('id_task_flow' => $_POST['id_task_flow'],'user_id' => $this->session->userdata('user_id')));
        //echo "<pre>"; print_r($task_time_details); exit;
        echo json_encode(array('status'=>TRUE,'error'=>false,'data'=>$task_time_details[0]));exit;
    }

    public function getLogTimeByDeptGrid(){
        $params = $this->input->get();
        $data = $this->Project_modal->getLogTimeByDeptGrid($params);
        echo json_encode($data);
    }

    public function projectPlan()
    {
        $projects = $this->Project_modal->getProjects();
        //echo "<pre>"; print_r($projects); exit;
        $data['project_week'] = $this->Report_model->getProjectWeeks(array('project_id' => $projects[0]['id_project']));

        $project_plan = $this->Report_model->getProjectPlan(array('project_id' => $projects[0]['id_project']));
        $plan = array();
        for($s=0;$s<count($project_plan);$s++)
        {
            $plan[$project_plan[$s]['user_id']][$project_plan[$s]['project_week_id']] = $project_plan[$s];
        }
        $data['plan'] = $plan;
        $data['users'] = $data['team_members'] = $users = $this->Project_modal->getProjectUsers($projects[0]['id_project']);
        $holidaysList = $this->Holiday_modal->getListOfHolidaysByDateRange(array('start_date' => date('m/d/Y',strtotime($projects[0]['project_start_date'])), 'end_date' => date('m/d/Y',strtotime($projects[0]['project_end_date']))));
        //echo "<pre>"; print_r($holidaysList); exit;
        $data['content'] = 'project/project_plan';
        $this->load->view('partials/landing',$data);
    }

    public function weeks()
    {
        $start    = '2016-12-14';
        $end      = '2017-01-31';
        $project_id = 1;
        $s=0;
        while(strtotime($start)<=strtotime($end)){
            $ts = strtotime($start);
            if($s==0){
                $start_s = $ts;
            }
            else
                $start_s = (date('w', $ts) == 0) ? $ts : strtotime('monday this week', $ts);

            $start_date = date('Y-m-d', $start_s);
            $end_date = date('Y-m-d', strtotime('sunday this week', $start_s));
            if(strtotime($end)<strtotime($end_date))
                $end_date = $end;

            $this->Report_model->addProjectWeek(array(
                'project_id' => $project_id,
                'week_start_date' => $start_date,
                'week_end_date' => $end_date,
            ));

            $start = date('Y-m-d',strtotime('+7 day', strtotime($start)));

            $s++;
        }

    }

    public function getProjectPlan()
    {
        $params = $this->input->post();
        $project_id = $params['id'];
        $data['project_id'] = $params['id'];
        $projects = $data['project_details'] = $this->Project_modal->getProjectDetails(array('project_id' => $project_id));
        $data['project_week'] = $this->Report_model->getProjectWeeks(array('project_id' => $projects[0]['id_project']));

        //create project week if not exists
        if(empty($data['project_week']))
        {
            $project_id = $projects[0]['id_project'];
            $start = date('Y-m-d',strtotime(str_replace('/','-',$projects[0]['project_start_date'])));
            $end = date('Y-m-d',strtotime(str_replace('/','-',$projects[0]['project_end_date'])));
            $temp = array();
            $s=0;
            while(strtotime($start)<=strtotime($end)){
                $ts = strtotime($start);
                if($s==0){
                    $start_s = $ts;
                }
                else
                    $start_s = (date('w', $ts) == 0) ? $ts : strtotime('monday this week', $ts);

                $start_date = date('Y-m-d', $start_s);
                $end_date = date('Y-m-d', strtotime('sunday this week', $start_s));
                if(strtotime($end)<strtotime($end_date))
                    $end_date = $end;

                $this->Report_model->addProjectWeek(array(
                    'project_id' => $project_id,
                    'week_start_date' => $start_date,
                    'week_end_date' => $end_date,
                ));

                $temp[] = array(
                    'project_id' => $project_id,
                    'week_start_date' => $start_date,
                    'week_end_date' => $end_date,
                );

                $start = date('Y-m-d',strtotime('+7 day', strtotime($start)));

                $s++;
            }
//echo "<pre>"; print_r($temp); exit;
            $data['project_week'] = $this->Report_model->getProjectWeeks(array('project_id' => $projects[0]['id_project']));
        }
        else
        {
            $start = date('Y-m-d',strtotime(str_replace('/','-',$data['project_week'][count($data['project_week'])-1]['week_end_date'])));
            $start = date('Y-m-d',strtotime('+1 day', strtotime($start)));
            $now = time(); // or your date as well
            $endDate = strtotime($start);
            $datediff = $now - $endDate;
            $isNotWeek = true;
            $diff = floor($datediff / (60 * 60 * 24));

            if($projects && strtotime(str_replace('/','-',$projects[0]['project_end_date']))>strtotime(date('Y-m-d'))){
                $end = date('Y-m-d',strtotime(str_replace('/','-',$projects[0]['project_end_date'])));
                $isNotWeek = false;
            }else if($diff>=1 && $diff<6 && date('w',strtotime($start))>0){
                $end = date('Y-m-d',strtotime(date('Y-m-d')));
                $this->Report_model->updateProjectWeek(array('id_project_week'=>$data['project_week'][count($data['project_week'])-1]['id_project_week'], 'week_end_date' => $end));
                $isNotWeek = true;
            }else{
                $end = date('Y-m-d',strtotime(date('Y-m-d')));
                $isNotWeek = false;
            }

            $s=0;
            while(strtotime($start)<strtotime($end) && !$isNotWeek){
                $ts = strtotime($start);
                if($s==0){
                    $start_s = $ts;
                }
                else
                    $start_s = (date('w', $ts) == 0) ? $ts : strtotime('monday this week', $ts);

                $start_date = date('Y-m-d', $start_s);
                $end_date = date('Y-m-d', strtotime('sunday this week', $start_s));
                if(strtotime($end)<strtotime($end_date))
                    $end_date = $end;

                $this->Report_model->addProjectWeek(array(
                    'project_id' => $project_id,
                    'week_start_date' => $start_date,
                    'week_end_date' => $end_date,
                ));

                $start = date('Y-m-d',strtotime('+7 day', strtotime($start)));

                $s++;
            }

            $data['project_week'] = $this->Report_model->getProjectWeeks(array('project_id' => $projects[0]['id_project']));
        }
        $data['project_end_date'] = date('Y-m-d',strtotime(str_replace('/','-',$projects[0]['project_end_date'])));

        $project_plan = $this->Report_model->getProjectPlan(array('project_id' => $projects[0]['id_project']));
        $plan = array();
        for($s=0;$s<count($project_plan);$s++)
        {
            $plan[$project_plan[$s]['user_id']][$project_plan[$s]['project_week_id']] = $project_plan[$s];
        }
        $data['plan'] = $plan;
        $data['users'] = $data['team_members'] = $users = $this->Project_modal->getProjectUsers($projects[0]['id_project']);
        $holidaysList = $this->Holiday_modal->getListOfHolidaysByDateRange(array('start_date' => date('m/d/Y',strtotime($projects[0]['project_start_date'])), 'end_date' => date('m/d/Y',strtotime($projects[0]['project_end_date']))));

        $data=$this->load->view('project/project_plan_model',$data, TRUE);
        $this->output->set_output($data);
    }

    public function getUpdateProjectPlan()
    {
        $params = $this->input->post();
        $check = $this->Report_model->checkProjectPlan($params);
        if(empty($check) || !isset($check[0]['id_project_plan'])){
            $this->Report_model->addProjectPlan(array(
                'project_id' => $params['project_id'],
                'project_week_id' => $params['project_week_id'],
                'user_id' => $params['user_id'],
                'time' => $params['value']
            ));
        }
        else{
            $this->Report_model->updateProjectPlan(array(
                'id_project_plan' => $check[0]['id_project_plan'],
                'time' => $params['value']
            ));
        }
        echo json_encode(array('status' => TRUE,'data' => ''));
    }

    public function addNotificationFlag()
    {
        $this->Project_modal->updateNotification(array(
            'id_notification' => $_POST['notification_id'],
            'read_flag' => 0
        ));

        echo json_encode(array('status' => TRUE,'data' => ''));
    }

}
