﻿// JavaScript Document   
(function ($) {
    var TssReportTable = function (element, options) {
        this.element = $(element);
        this.options = $.extend({
            YAxisData: [],
            XAxisData: [], 
            BodyData: [],
            XHeadConfig: {},
            BodyCellWidth:50,
            HaveOverView: false,
            XOverView:false,
            OverViewWidth:100,
            YAxisLableWidth: 100,
            YAxisHead: '&nbsp;',
            YAxisHeadFormatter: null,
            OverViewHead: '&nbsp;',
            OverViewHeadFormatter: null,
            YKey: 'Id',
            YValue: 'Name',
            YFormatter: null,
            YTitleFormatter:null,
            XKey: 'Id',
            XValue: 'Name',
            XHeadHeight:24,
            XFormatter: null,
            XTitleFormatter: null,
            XRotation: false,
            XDefaultHtml: '&nbsp;'
        }, options || {});

        // Public method - can be called from client code
        this.init = function () {
            var curObj = this; 
            curObj.element.addClass('oasis-report-table  clearboth clearfix');
            curObj.buildElements();
            setTimeout(function () { curObj.scrollMiddle(); }, 100);
            return this;
        };
        this.scrollMiddle = function (flg) { 
            var curObj = this;
            var opt = curObj.options;
            var ele = curObj.element;
            var totWidth = ele.width()-40;
            var dedWidth=opt.YAxisLableWidth + (opt.HaveOverView ? opt.OverViewWidth : 1);
            var bodWidth = 0;
            $.each(opt.XAxisData, function (i) {
                /*
                var width = $($('div[x="'+this[opt.XKey]+'"]')[0]).width();//opt.BodyCellWidth;
                if (!TssLib.isBlank(opt.XHeadConfig[i]) && !TssLib.isBlank(opt.XHeadConfig[i].Rotate) && !opt.XHeadConfig[i].Rotate) {
                    if (!TssLib.isBlank(opt.XHeadConfig[i].Width)) {
                        width = opt.XHeadConfig[i].Width;
                    }
                }
                bodWidth += width;
                */
                bodWidth += $(ele.find('div[x="' + this[opt.XKey] + '"]')[0]).width();
            });
            //opt.XAxisData.length * opt.BodyCellWidth;
            if (totWidth < bodWidth + dedWidth) {
                ele.find('.mid-body-cont').width(totWidth - dedWidth);
                ele.find('.mid-body-data').width(bodWidth);
                ele.find('.mid-body-cont').css('display', 'block');
                if (TssLib.isBlank(flg))
                    ele.find('.mid-body-cont').mCustomScrollbar({ axis: "x", theme: "dark", scrollbarPosition: 'outside', scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
                else {
                    if (ele.find('.mid-body-cont').find('.mCSB_container').length > 0) {
                        ele.find('.mid-body-cont').mCustomScrollbar('update');
                    } else {
                        ele.find('.mid-body-cont').mCustomScrollbar({ axis: "x", theme: "dark", scrollbarPosition: 'outside', scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
                    }
                }
            } else {
                ele.find('.mid-body-cont').width(bodWidth);
                ele.find('.mid-body-data').width(bodWidth);
            }
        };
        this.buildElements = function () {
            var curObj = this;
            var opt = curObj.options;
            var ele = curObj.element; 
            var wrDiv = $('<div/>', { style: 'display: inline-block;' });
            var _div = $('<div/>', { class: 'y-label-cont cell-container', style: 'width:' + opt.YAxisLableWidth + 'px;float:left;' });
            wrDiv.append(_div);
            _div = $('<div/>', { class: 'mid-body-cont cell-container' });
                _div.append($('<div/>', { class: 'mid-body-data cell-container'}));
            wrDiv.append(_div);
            wrDiv.append(_div);
            _div = $('<div/>', { class: 'overview-label-cont cell-container', style: 'width:' + (opt.HaveOverView ? opt.OverViewWidth : 1) + 'px;float:left;' });
                wrDiv.append(_div);
            ele.append(wrDiv);
            curObj.buildYaxis();
            curObj.buildXaxis();
            curObj.buildBodyArea();
            var xhe = 0;
            $.each(ele.find('.x-head-top'), function () {
                if (xhe < $(this).height())
                    xhe = $(this).height();
            });
            var che = ele.find('.y-label-cont .cell:first').height();
            ele.find('.overview-label-cont').css('margin-top', (xhe - che) + 'px');
            ele.find('.y-label-cont').css('margin-top', (xhe - che) + 'px');
        };
        this.buildYaxis = function () {
            var curObj = this;
            var opt = curObj.options;
            if (TssLib.isBlank(opt.YAxisData)) {
                var _to={};
                _to[opt.YValue]='No Data';
                _to[opt.YKey]=-1;
                opt.YAxisData = [_to];
            }
            var ele = curObj.element;
            var _div = ele.find('.y-label-cont');
            if (!TssLib.isBlank(opt.YAxisData)) {
                _div.append($('<div/>', { y: 0, html: TssLib.isBlank(opt.YAxisHeadFormatter) ? opt.YAxisHead : opt.YAxisHeadFormatter, 'cell-type': 'y-axis-head', class: 'cell x-head x-head-top', style: 'padding:0;width:' + opt.YAxisLableWidth + 'px' }));
                var _ovDiv = ele.find('.overview-label-cont');
                _ovDiv.append($('<div/>', { y: 0, html: TssLib.isBlank(opt.OverViewHeadFormatter) ? opt.OverViewHead : opt.OverViewHeadFormatter, 'cell-type': 'overview-head', class: 'cell x-head x-head-top', style: 'width:' + (opt.HaveOverView ? opt.OverViewWidth : 1) + 'px' }));
            }
            $.each(opt.YAxisData, function () {
                var dat = this;
                var yDiv = $('<div/>', { y: dat[opt.YKey], html: TssLib.isBlank(opt.YFormatter) ? dat[opt.YValue] : opt.YFormatter(dat), title: TssLib.isBlank(opt.YTitleFormatter) ? dat[opt.YValue] : opt.YTitleFormatter(dat), 'cell-type': 'y-axis-lc', class: 'cell ellipsis', style: 'width:' + opt.YAxisLableWidth + 'px' });
                yDiv.data('data', dat);
                _div.append(yDiv);
                 var _ovDiv = ele.find('.overview-label-cont');
                 _ovDiv.append($('<div/>', { y: dat.Id, html: '&nbsp;', 'cell-type': 'overview-co', class: 'cell', style: 'border-left:' + (opt.HaveOverView ? 1 : 0) + 'px solid #ccc;width:' + (opt.HaveOverView ? opt.OverViewWidth : 1) + 'px' }));
            });
            if (!TssLib.isBlank(opt.YAxisData)) {
                if (opt.XOverView) {
                    _div.append($('<div/>', { y: 'xov', html: '&nbsp;', 'cell-type': 'y-axis-lc-xov', class: 'cell x-head x-head-bot', style: 'padding:0;width:' + opt.YAxisLableWidth + 'px' }));
                    var _ovDiv = ele.find('.overview-label-cont');
                    _ovDiv.append($('<div/>', { y: 'xov', html: '&nbsp;', 'cell-type': 'y-axis-overview-xov', class: 'cell x-head x-head-bot', style: 'width:' + (opt.HaveOverView ? opt.OverViewWidth : 1) + 'px' }));
                }
            }
        };
        this.buildXaxis = function () {
            var curObj = this;
            var opt = curObj.options;
            var ele = curObj.element;
            var _div = ele.find('.mid-body-data');
            var _divRow = $('<div/>', { style: 'float:left',class:'x-cel-row-head' });
            _div.append(_divRow);
            
                var maxWidth = 0;
                var maxChars = 0;
                if (opt.XRotation){
                    $.each(opt.XAxisData, function (i) {
                        if(!TssLib.isBlank(opt.XHeadConfig[i]) && !TssLib.isBlank(opt.XHeadConfig[i].Rotate) && !opt.XHeadConfig[i].Rotate){
                            return;
                        }
                        var dat = this;
                        var ele = $('<span/>', { html: TssLib.isBlank(opt.XFormatter) ? dat[opt.XValue] : opt.XFormatter(dat), style: 'border:0px solid red' });
                        if (maxChars < (TssLib.isBlank(opt.XTitleFormatter) ? dat[opt.XValue] : opt.XTitleFormatter(dat)).length)
                            maxChars = (TssLib.isBlank(opt.XTitleFormatter) ? dat[opt.XValue] : opt.XTitleFormatter(dat)).length;
                        $('body').append(ele);
                        if (maxWidth < ele.width())
                            maxWidth = ele.width(); 
                        ele.remove();
                    });
                }
                maxWidth = maxWidth - maxChars;
                $.each(opt.XAxisData, function (i) {
                    var dat = this;
                    var _xDiv = null;
                    if (opt.XRotation){
                        var _svg = $('<svg/>', { xmlns: 'http://www.w3.org/2000/svg', style: 'border: 0px solid;', fill: '#666', width: '25', height: maxWidth+6 });
                        var _g = $('<g/>', { transform: 'translate(0,' + maxWidth + ') rotate(-90 0 0)' });
                        _svg.append(_g);
                        var _text = $('<text/>', { x: '0', zindex: '1', transform: 'translate(0,17)', html: TssLib.isBlank(opt.XFormatter) ? dat[opt.XValue] : opt.XFormatter(dat) });
                        _g.append(_text);
                        if (!TssLib.isBlank(opt.XHeadConfig[i]) && !TssLib.isBlank(opt.XHeadConfig[i].Rotate) && !opt.XHeadConfig[i].Rotate) {
                            var width = opt.BodyCellWidth;
                            if (!TssLib.isBlank(opt.XHeadConfig[i].Width)) {
                                width = opt.XHeadConfig[i].Width;
                            }
                            _xDiv = $('<div/>', {
                                x: dat[opt.XKey], html: TssLib.isBlank(opt.XFormatter) ? dat[opt.XValue] : opt.XFormatter(dat), title: TssLib.isBlank(opt.XTitleFormatter) ? dat[opt.XValue] : opt.XTitleFormatter(dat), 'cell-type': 'x-axis-head', class: 'cell ellipsis x-head x-head-top border-btm',
                                style: ('height:' + (maxWidth + 6) + 'px;width:' + width + 'px;padding-top:' + (maxWidth + 6 - 25) + 'px;')
                            });
                        } else {
                            _xDiv = $('<div/>', {
                                x: dat[opt.XKey], html: $(_svg[0].outerHTML), title: TssLib.isBlank(opt.XTitleFormatter) ? dat[opt.XValue] : opt.XTitleFormatter(dat), 'cell-type': 'x-axis-head', class: 'cell ellipsis x-head x-head-top border-btm', style: 'height:' + (maxWidth + 6) + 'px;width:' + opt.BodyCellWidth + 'px'
                            });
                        }

                        _xDiv.data('data',dat);
                    } else {
                        _xDiv = $('<div/>', {
                            x: dat[opt.XKey], html: TssLib.isBlank(opt.XFormatter) ? dat[opt.XValue] : opt.XFormatter(dat), title: TssLib.isBlank(opt.XTitleFormatter) ? dat[opt.XValue] : opt.XTitleFormatter(dat), 'cell-type': 'x-axis-head', class: 'cell ellipsis x-head x-head-top border-btm', style: 'height:' + opt.XHeadHeight + 'px;width:' + opt.BodyCellWidth + 'px'
                        });
                        _xDiv.data('data', dat);
                    }
                    _divRow.append(_xDiv);
                });

        };
        this.buildBodyArea = function () {
            var curObj = this;
            var opt = curObj.options;
            var ele = curObj.element;
            var _div = ele.find('.mid-body-data');
            $.each(opt.YAxisData, function (i) {
                var y = this;
                var _divRow = $('<div/>', { style: 'float:left', class: 'x-cel-row'  });
                _div.append(_divRow);
                $.each(opt.XAxisData, function (i) {
                        var width = opt.BodyCellWidth;
                        if (!TssLib.isBlank(opt.XHeadConfig[i]) && !TssLib.isBlank(opt.XHeadConfig[i].Rotate) && !opt.XHeadConfig[i].Rotate) {
                            if (!TssLib.isBlank(opt.XHeadConfig[i].Width)) {
                                width = opt.XHeadConfig[i].Width;
                            }
                        }
                    var x = this;
                    _divRow.append($('<div/>', { x: x[opt.XKey], html: opt.XDefaultHtml, y: y[opt.YKey], 'cell-type': 'x-axis-co', class: 'cell cell-x', style: 'width:' + width + 'px' }));
                });
            });
            if (opt.XOverView)
                $.each(opt.XAxisData, function (i) {
                    var width = opt.BodyCellWidth;
                    if (!TssLib.isBlank(opt.XHeadConfig[i]) && !TssLib.isBlank(opt.XHeadConfig[i].Rotate) && !opt.XHeadConfig[i].Rotate) {
                        if (!TssLib.isBlank(opt.XHeadConfig[i].Width)) {
                            width = opt.XHeadConfig[i].Width;
                        }
                    }
                    var _divRow = $('<div/>', { style: 'float:left', class: 'x-cel-row' });
                    _div.append(_divRow);
                    var x = this;
                    _divRow.append($('<div/>', { x: x[opt.XKey], html: '&nbsp;', y: 'xov', 'cell-type': 'x-axis-xov-co', class: 'cell x-head', style: 'width:' + width + 'px' }));
                });
        };
        this.sample = function ( ) {
            var curObj = this;
            return this;
        };
        this.export = function (fileName) {
            var curObj = this;
            var opt = curObj.options;
            var ele = curObj.element;
            var data = [];
            var dataRow = [];
            $.each(ele.find('.x-head-top'), function () {
                var cellType=$(this).attr('cell-type');
                if (cellType != 'overview-head' || (cellType == 'overview-head' && opt.HaveOverView)) {
                    dataRow.push({ YHeader: true, XHeader: true, Text: $(this).text(), HTML: $(this).html() });//, CellType: $(this).attr('cell-type')
                } 
            });
            data.push(dataRow);
            $.each(ele.find('div[cell-type="y-axis-lc"]'), function () {
                dataRow = [];
                var fld = $(this);
                dataRow.push({ YHeader: true, Text: fld.text(), HTML: fld.html() });
                $.each(ele.find('div[cell-type="x-axis-co"][y="' + fld.attr('y') + '"]'), function () {
                    dataRow.push({ Text: $(this).text(), HTML: $(this).html() });
                });
                if (opt.HaveOverView) {
                    var ovFld = ele.find('div[cell-type="overview-co"][y="' + fld.attr('y') + '"]');
                    dataRow.push({ Text: ovFld.text(), HTML: ovFld.html() });
                }
                data.push(dataRow);
            });
            $.each(ele.find('div[cell-type="y-axis-lc-xov"]'), function () {
                dataRow = [];
                var fld = $(this);
                dataRow.push({ YHeader: true, Text: fld.text(), HTML: fld.html() });
                $.each(ele.find('div[cell-type="x-axis-xov-co"][y="' + fld.attr('y') + '"]'), function () {
                    dataRow.push({ Text: $(this).text(), HTML: $(this).html() });
                });
                if (opt.HaveOverView) {
                    var ovFld = ele.find('div[cell-type="y-axis-overview-xov"][y="' + fld.attr('y') + '"]');
                    dataRow.push({ Text: ovFld.text(), HTML: ovFld.html() });
                }
                data.push(dataRow);
            });
 

            ele.table2excel({
                      name: "Lead Source Info Report"//Sheet name
                    , fromTable: false
                    , data: data
                    , fileName: fileName
                });
            return data;
        };
    };
    $.fn.TssReportTable = function (options) {
        return this.each(function () {
            if (options.forceRefresh == undefined || options.forceRefresh==null) {
                options.forceRefresh=false;
            }
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data('this') && !options.forceRefresh) return;
            // pass options to plugin constructor
            var obj = new TssReportTable(this, options);
            // Store plugin object in this element's data
            element.data('this', obj);
            obj.init();
        });
    };
})(jQuery);

//Blue: 005fb0, 007ae3, 3199fc, 69b7fd, 94cdfd
var colorVals = [0, 7.5, 15, 22.5, 30, 37.5, 45, 52.5, 60, 67.5, 75, 82.5, 90, 97.5, 100];
var colors = ['#048b48', '#05a355', '#04c76b', '#04dc76', '#04f382', '#b65c04', '#d86d05', '#ff8106', '#ff9907', '#ffbc09', '#ff9696', '#ff7474', '#ff4949', '#ff0000', '#de0000'];
function getColor(n, asc) {
    if (asc == undefined || asc == null)
        asc = true;
    if (asc) {
        if (n == 0)
            return colors[14];
        else {
            for (var i = 1; i <= colorVals.length-1; i++) {
                if (n > colorVals[i - 1] && n <= colorVals[i]) {
                    return colors[colorVals.length-1-i];
                    break;
                }
            }
        }
    } else {
        if (n == 0)
            return colors[0];
        else {
            for (var i = 1; i <= colorVals.length - 1; i++) {
                if (n > colorVals[i - 1] && n <= colorVals[i]) {
                    return colors[i];
                    break;
                }
            }
        }
    }
}