<table border="1" cellpadding="0" cellspacing="0" width="600px">
    <tbody>
    <tr><td style="padding-left:5px">Name</td><td style="padding-left:5px"><?=$name?></td></tr>
    <tr><td style="padding-left:5px">Description</td><td style="padding-left:5px"><?=$description?></td></tr>
    <tr><td style="padding-left:5px">Requested for</td><td style="padding-left:5px"><?=$request_for?></td></tr>
    <tr><td style="padding-left:5px">Requested on</td><td style="padding-left:5px"><?=$date?></td></tr>
    <?php if(isset($admin_comments)) { ?>
        <tr><td style="padding-left:5px">Project Manager Comments</td><td style="padding-left:5px; font-weight: bold;"><?=$admin_comments?></td></tr>
        <tr><td style="padding-left:5px">Status</td><td style="padding-left:5px; font-weight: bold; color:<?=$status=='approved'?'green':'red'?>;"><?=ucfirst($status)?></td></tr>
    <?php } ?>
    </tbody>
</table>