<div class="mainpanel" id="budgetTemplateTbl_wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Week Workload</h3>
        </div>

        <div class="col-sm-12 clearboth clearfix pr0 pl0 pb10 ml15">
            <div class="col-sm-12 pl0 pr0 pb10 bg-white clearfix">
                <div class="col-sm-4">
                    <div class="form-group padding0">
                        <div class="multi-select-container">
                            <select class="form-control" id="project_id" >
                                <?php
                                if($this->session->userdata('user_type_id')==2)
                                    echo ' <option value="0">ALL</option>';
                                ?>

                                <?php for($s=0;$s<count($department_projects);$s++){ ?>
                                    <option <?php if($project_id==$department_projects[$s]['project_id']){ echo "selected='selected'"; } ?> value="<?=$department_projects[$s]['project_id']?>"><?=$department_projects[$s]['project_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <label class="control-label col-sm-12">Project:</label>
                    </div>
                </div>
                <?php if($this->session->userdata('user_type_id')==2):?>
                    <div class="col-sm-4">
                        <div class="form-group padding0">
                            <div class="multi-select-container">
                                <select class="form-control" id="department_id" >
                                    <option value="0">ALL</option>
                                    <?php for($s=0;$s<count($department_list);$s++){ ?>
                                        <option <?php if($department_id==$department_list[$s]['id_department']){ echo "selected='selected'"; } ?> value="<?=$department_list[$s]['id_department']?>"><?=$department_list[$s]['department_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <label class="control-label col-sm-12">Department:</label>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-sm-4">
                    <button class="button button-common module mt20" onclick="getWeekWorkLoad(0);"  id=""> Search </button>
                </div>
            </div>

        </div>
<div id="load_week_workload"> </div>

    <div id="unAssignedTaskshowByProjects" class="modal-wrapper" style="display:none">
        <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form">
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                    class="glyphicon glyphicon-remove"></i></a></div>
                        <h4 class="panel-title">Show By</h4>
                    </div>
                    <div class="panel-body auto-height">
                        <div class="col-sm-12 padding0">
                            <div class="col-sm-6 pl0">
                                <h4 class="margin0 f14"></h4
                                <div id="unassigned-task-status" class="col-sm-12 padding0 tsk-chklist-wrapper">
                                    </div>
                            </div>
                            <div class="col-sm-6 pr0">
                                <h4 class="margin0 f14">Status</h4

                                <div id="unassigned-task-status" class="col-sm-12 padding0 tsk-chklist-wrapper">
                                    <div class="checkbox">
                                        <label>
                                            <input name="status[]" value="assigned" type="checkbox">Assigned
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input name="status[]" value="unassigned" type="checkbox">Un Assigned
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix text-center pb20">
                                <button onclick="getUnassignedTaskList(<?=$project_id?>,1);" class="button-common">Update</button>
                                <button class="button-color" aria-hidden="true" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="assigningTaskTo" class="modal-wrapper" style="display:none">
        <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" >
            <div>
                <form id="assign_t_user" class="NewErrorStyle" name="assign_t_user" method="post"
                      action="<?= WEB_BASE_URL ?>index.php/Project/addTaskToUser">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Assign Task To</h4>
                        </div>
                        <div class="panel-body auto-height">
                            <div>
                                <h4 class="margin0" id="project_title" name="project-title"></h4>
                                <p class="margin0" id="task_name" name="task-name"></p>
                                <p class="margin0">
                                    <span class="mr15">
                                        <span>Estimated :</span>
                                        <span class="bold" name="estimated_time" id="un_assi_est"></span>
                                    </span>
                                    <span class="mr15">
                                        <span>Remains :</span>
                                        <span class="bold red-color" name="remains" id="un_assi_rem"></span>
                                    </span>
                                </p>
                                <div id="prev_task">

                                </div>
                                <div>
                                    <table class="add-assigning-tasks-table mt20">
                                        <thead>
                                        <th>Assign for</th>
                                        <th>On</th>
                                        <th>Engaged Hours</th>
                                        <th>Alloted Hours</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <!--<select class="custom-select2" id="userSelect" name="assigned_to">
                                                    <?php /*for($s=0;$s<count($team_members);$s++){ */?>
                                                        <option value="<?/*=$team_members[$s]['id_user']*/?>"><?/*=$team_members[$s]['first_name'].' '.$team_members[$s]['last_name']*/?></option>
                                                    <?php /*} */?>
                                                </select>-->
                                                <span><input type="text" disabled id="user_name"/></span>
                                            </td>
                                            <!--<td>
                                                <div class="margin0">
                                                    <input type="text" placeholder="dd/mm/yyyy" id="log_start_date" name="log_start_date" class="rval form-control tssDatepicker">
                                                </div>
                                            </td>-->
                                            <td><span><input type="text" disabled id="day"/></span></td>
                                            <td><span><input type="text" disabled id="engagedHours"/></span></td>
                                            <td><span class="mr10">
                                                    <input type="text" class="msk_time" name="time" id="allotedHours"/></span>

                                                <!--<a onclick="addingAsssignedTask()" class="f12"><span class="fa fa-plus-square"></span> Add User</a>-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="project_task_id" id="project_task_id">
                                    <input type="hidden" name="estimated_time" id="estimated_time">
                                    <input type="hidden" name="project_id" id="project_id">
                                    <input type="hidden" name="assigned_to" id="assigned_to">
                                    <input type="hidden" name="log_day" id="log_day">
                                    <input type="hidden" id="c_rem_time" value="">
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix text-center pt20 pb10">
                                <button type="submit" class="button-common" id="submit-una">Save</button>
                                <button class="button-color" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input type="hidden" id="cur_day" value="<?=strtotime(date("Y-m-d"))?>">

    <div id="requestMoreTime" class="modal-wrapper" style="display:none">
                <div >
                    <div>
                        <form id="edit_task_frm" class="NewErrorStyle" name="edit_task_frm" method="post"
                              action="<?= WEB_BASE_URL ?>index.php/Project/updateTaskByTeamLead">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                            class="glyphicon glyphicon-remove"></i></a></div>
                                <h4 class="panel-title">Edit Task</h4>
                            </div>
                            <div class="panel-body auto-height">
                                <div class="form-group">

                                    <div class="input_container">
                                        <label>Task Name</label>
                                        <input type="text"  name="task_name" class="form-control rval" id="task_name" value="" >
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="input_container">
                                        <label>Description</label>
                                        <input type="text"  name="description" class="form-control " id="description" value="" >
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="input_container">
                                        <label>Estimated Time</label>
                                        <input type="text"  name="estimated_time" class="form-control rval  msk_time" id="estimated_time" value="" >
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;">
                                    <div class="input_container">
                                        <label>Additional Time</label>
                                        <input disabled type="text" id="additional_time" name="additional_time" class="form-control rval  msk_time" value="" >
                                    </div>
                                </div>
                                <div class="form-group" id="t-status" style="display:none;">
                                    <div class="input_container">
                                        <label>Status</label>
                                        <select name="task_status" id="task_status" class="">
                                            <option value="progress">Progress</option>
                                            <option value="approval_waiting">Pending Approval</option>
                                            <option value="completed">Completed</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group mt10">
                                <!--<table class="table table-bordered" id="requestMoreTimeTable" style="display: none">
                                    <thead>
                                    <tr>
                                        <th style="width: 25%;"><span class="th-box">Name</span></th>
                                        <th style="width: 5%;"><span class="th-box">Hours</span></th>
                                        <th style="width: 30%;"><span class="th-box">User Comments</span></th>
                                        <th style="width: 30%;"><span class="th-box">Admin Comments</span></th>
                                        <th style="width: 10%;"><span class="th-box">Status</span></th>
                                        <th style="width: 10%;"><span class="th-box">Requested Date</span></th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>-->
                                    <div class="grid-details-table-content">
                                        <div class="tbl_wrapper border0 ">
                                            <table id="time-logs-info-table" class="table-responsive" ></table>
                                            <div id="time-logs-info-table-page"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="is_forward">Forward to next department</label>
                                        <input class="" id="is_forward" type="checkbox">
                                    </div>
                                </div>
                                <div class="form-group" id="rej-div" style="display: none">
                                    <div class="checkbox">
                                        <input type="button" onclick="getRejectTask();" class="btn-danger" name="rej-btn" id="rej-btn" value="Reject">
                                    </div>
                                </div>
                            </div>

                            <div class="panel-footer text-center">
                                <button type="submit" class="button-common" id="task-update">Save</button>
                            </div>
                            <input type="hidden" name="task_flow_id" id="task_flow_id" value="">
                            <input type="hidden" name="department_id" id="department_id" value="">
                            <input type="hidden" name="project_task_id" id="project_task_id" value="">
                            </div>
                        </form>
                    </div>

                    </form>
                </div>
            </div>

    <div id="assignedTaskList" class="modal-wrapper" style="display:none">
                <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a></div>
                            <h4 class="panel-title">Task List</h4>
                        </div>
                    <div id="assignedTaskList-table" class="panel-body auto-height">

                    </div>
                    </div>

                </div>
            </div>

    </div>
</div>

<script>
    $('#project_id').on('change',function(){
        /*if ( !$( "#department_id" ).length ) { return false; }
        var project_id = $(this).val();
        getJsonAsyncWithBaseUrl("Project/getProjectDepartments/"+project_id, {}, {
            jsonContent: true,
            callback: function (result) {
                var html = '<option value="0">ALL</option>';
                $.each( result.data.data, function( index, value ){
                    html += '<option value="'+value.department_id+'" > '+value.department_name+'</option>';
                });
                $('#department_id').html(html);
                $("#department_id").select2("val", "0");
                loadWeekWorkload(0);
            }
        });*/
        $("#project_id").val($(this).val());
        var project_id = $('#project_id').val();
        loadWeekWorkload(0);
    });
    function loadWeekWorkload(week) {
        var project_id = $('#project_id').val();
        var department_id = '';
        if($('#department_id').val())
            department_id = $('#department_id').val();
        if(week==0){
            week = $('#cur_week_time').val()
        }
        $.get('<?=site_url('Project/loadWeekWorkload')?>/'+project_id+"/"+week+"/"+department_id, function (result) {
            $('#load_week_workload').html(result);
        });

    }

    loadWeekWorkload(0);

    function getEditTask(project_name,task_name,project_task_id,project_id,estimated_time,remins_time,pending) {
        var $modal = TssLib.openModel({width: 850}, $('#requestMoreTime').html());
        if(pending){
            $('#t-status').css('display','block');
        }
        else{
            $('#t-status').css('display','none');
        }
        /*var $refTblLoggrid = $('#time-logs-info-table');*/
        getProjectTaskDetails(project_task_id,pending);
//        getMoreTimeRequest(project_task_id);
        getLogTimeList(project_task_id);

        TssLib.ajaxForm({
            form: $modal.find('#edit_task_frm'), callback: function (result) {
                console.log(result);
                if (result.data.status) {
                    TssLib.closeModal();
                    TssLib.notify(result.data.message, 'warn', 5);
                    getWeekWorkLoad(0);
                }
                else {
                    TssLib.notify(result.data.message, 'warn', 5);
                }
            },
            before: function (formObj) {
                var time = $modal.find('#estimated_time').val(),
                    is_forward = $modal.find('#is_forward').is(':checked'),
                    department_id = $modal.find('#department_id').val();

                var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(time);
                /*if(!isValid){
                    TssLib.notify('Invalid Time', 'error', 5);
                    return false;
                }*/
                $(formObj).data('additionalData', {
                    'project_task_id': project_task_id,
                    'is_forward': is_forward
                });
                $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/updateTaskByTeamLead');
                return true;
            }
        });
    }

    function getMoreTimeRequest(project_task_id){
        getJsonAsyncWithBaseUrl("project/getMoreTimeRequestById/"+project_task_id, {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var  res = result.data.data;
                    $('#requestMoreTimeTable').css('display','block');
                    var html = '';
                    for(var s=0;s<res.length;s++){
                        if(res[s].status == 'pending'){
                            html+='<tr>' +
                                '<td>'+res[s].first_name+' '+res[s].last_name+'</td>' +
                                '<td><input class="msk_time" type="text" name="time_requested" id="time_requested" value="'+res[s].duration+'"/></td>' +
                                '<td>'+res[s].user_comments+'</td>' +
                                '<td><input type="text" id="admin_comments"/></td>' +
                                '<td><a href="javascript:;" onclick="actionRequestedTime(this,1,'+res[s].Id_request_more_time+','+res[s].task_flow_id+','+project_task_id+')">Approve</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="actionRequestedTime(this,0,'+res[s].Id_request_more_time+','+res[s].task_flow_id+','+project_task_id+')">Reject</a></td>' +
                                '<td>'+res[s].created_date_time+'</td>' +
                                '</tr>';
                        }else{
                            html+='<tr>' +
                                '<td>'+res[s].first_name+' '+res[s].last_name+'</td>' +
                                '<td>'+res[s].duration+'</td>' +
                                '<td>'+res[s].user_comments+'</td>' +
                                '<td>'+res[s].admin_comments+'</td>' +
                                '<td>'+res[s].status+'</td>' +
                                '<td>'+res[s].created_date_time+'</td>' +
                                '</tr>';
                        }
                    }
                    html = (html == ''? '<tr><td colspan="5" style="text-align: center">No records found</td></tr>':html );

                    $('#requestMoreTimeTable').find('tbody').html(html);

                    if(res.length==0){ $('#requestMoreTimeTable').css('display','none'); }
                    $(".msk_time").mask("Hh:Mm");
                    $('#estimated_time').attr('disabled',true);
                }
            }
        });
    }

    function getLogTimeList(task_id){
        var $refTblLoggrid = $('#time-logs-info-table');

        $refTblLoggrid.jqGrid({
            url: TssConfig.TT_SERVICE_URL + 'Project/getLogTimeList?view=1&task_id='+task_id,
            multiselect: false,
            datatype: "json",
            sortorder: "desc",
            /*extSearchField: '.logtimesearch',*/
            colNames: ['Id', 'project_task_id','User Name', 'Task Type', 'Description', 'Start Time', 'End Time', 'Total time','Date'],
            colModel: [
                { name: 'log_time_id', index: 'log_time_id', hidden: true ,key:true},
                { name: 'project_task_id', index: 'project_task_id',  hidden: true},
                { name: 'user_name', index: 'user_name'},
                { name: 'task_type_name', index: 'task_type_name' },
                { name: 'comments', index: 'comments' },
                { name: 'start_time', index: 'start_time' },
                { name: 'end_time', index: 'end_time' },
                { name: 'duration', index: 'duration' },
                { name: 'created_date_time', index: 'created_date_time' },
            ],
            loadComplete: function () {
                if(jQuery("#time-logs-info-table").getDataIDs().length==0){
                    $("#gbox_time-logs-info-table").parent().parent().parent().hide();
                }else{
                    $(".grid-details-table").show();
                }
            },
            pager: 'time-logs-info-table-page'
        }).navGrid('#time-logs-info-table-page', {
            edit: false, add: false, del: false, search: false, refresh: true,
        });


    }

    function actionRequestedTime(This, action, id, taskflowId, taskId){
        var time = $(This).parent().parent().find('#time_requested').val();
        var comment = $(This).parent().parent().find('#admin_comments').val();
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL+'index.php/Project/actionRequestedTime',
            dataType: 'json',
            data: {taskflowId:taskflowId,Id: id, action:action, time:time, comment: comment},
            success:function(res){
                getProjectTaskDetails(taskId);
                getMoreTimeRequest(taskId);
                getWeekWorkLoad(0);
            }
        });
    }

    function showUnAssignedTaskshowByProjects(e) {
        var $modal = TssLib.openModel({width: 450}, $('#unAssignedTaskshowByProjects').html());
    }

    function showAssigningTaskTo(e) {
        //var $modal = TssLib.openModel({width: 850}, $('#assigningTaskTo').html());
    }

    function closeAssignedTask(e){
        $(e).closest('tr').remove();
    }

    function addingAsssignedTask(){
        var x = TssLib.getMultipleSelectVals("#userSelect");
        var y=x[0];
        var userVal = $("#userSelect").children("option[value='"+y+"']").text();
        var dtVal = $('#log_start_date').val();
        var engagedHours = $('#engagedHours').val();
        var allotedHours = $('#allotedHours').val();

        var _tr = $('<tr/>'),
            _userValTD = $('<td/>',{text:userVal}).appendTo(_tr),
            dtValTD = $('<td/>',{text:dtVal}).appendTo(_tr),
            engagedHoursTD = $('<td/>',{text:engagedHours}).appendTo(_tr),
            allotedHoursTD = $('<td/>',{html:'<span><input type="text" value="'+allotedHours+'"/> </span><a onclick="closeAssignedTask(this)"><i class="fa fa-times ml5"></i> '}).appendTo(_tr);
        $('.add-assigning-tasks-table').find('tbody').append(_tr);
        $('#log_start_date,#engagedHours,#allotedHours').val('');
    }

    function taskFilter(){
        $('#filter-unassigned-task-box').keyup(function(){

            var that = this, $allListElements = $('.unassigned-task-box');

            var $matchingListElements = $allListElements.filter(function(i, li){
                var listItemText = $(li).find('.task_name').text().toUpperCase(), searchText = that.value.toUpperCase();
                return ~listItemText.indexOf(searchText);
            });

            $allListElements.hide();
            $matchingListElements.show();

        });
    }

    function getDayTaskList(user_id,day_id,project_id,type=0) {
        var html='<table class="table table-bordered">';
        if(type==1)
            html+='<tr><th>Project Name</th><th>Task Name</th><td>Description</td><th>Alloted Time</th><th>Actual Time</th><th>Status</th><th>Action</th></tr>';
        else
            html+='<tr><th>Project Name</th><th>Task Name</th><td>Description</td><th>Alloted Time</th><th>Actual Time</th><th>Status</th></tr>';

        var $modal = TssLib.openModel({width: 850}, $('#assignedTaskList').html());
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL+'index.php/Project/getUserDayTasks',
            dataType: 'json',
            data: {user_id:user_id,day_id:day_id,project_id:project_id},
            success:function(res){
                for(var s=0;s<res.length;s++){
                    html+='<tr><td>'+res[s].project_name+'</td><td>'+res[s].task_name+'</td><td>'+res[s].description+'</td><td>'+res[s].time+'</td>';
                    var duration = res[s].duration==null?' -- ':res[s].duration;
                    if(res[s].task_status=='new'){
                        if(type==1)
                            html+='<td> -- </td><td><span class="pull-left task-status task-inprogress">New</span></td><td><span style="cursor: pointer;" onclick="deleteTaskWorkflow(this,'+res[s].id_task_week_flow+')"><i class="fa fa-trash-o" style="font-size: 16px;"></i></span></td>';
                        else if(type==0)
                            html+='<td> -- </td><td><span class="pull-left task-status task-inprogress">New</span></td>';
                    }
                    else if(res[s].task_status=='progress'){ html+='<td>'+duration+'</td><td><span class="pull-left task-status task-waiting">In Progress</span></td><td> -- </td>'; }
                    else if(res[s].task_status=='approval_waiting'){ html+='<td>'+duration+'</td><td><span class="pull-left task-status task-completed">Waiting for approval</span></td><td> -- </td>'; }
                    else if(res[s].task_status=='completed'){ html+='<td>'+duration+'</td><td><span class="pull-left task-status task-completed">Completed</span></td><td> -- </td>'; }
                    else if(res[s].task_status=='hold'){ html+='<td> -- </td><td><span class="pull-left task-status task-open">Hold</span></td>'; }
                    else{ html+='<td> -- </td><td> -- </td>'; }
                    html+='</tr>';
                }
                html+='</table';
                $('#assignedTaskList-table').html(html);
            }
        });
    }

    function hmsToSecondsOnly(str) {
        var hm = str;   // your input string
        var a = hm.split(':'); // split it at the colons

// minutes are worth 60 seconds. Hours are worth 60 minutes.
        var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60;
        return seconds;
    }

    function showEstimatedTimeEditOption(This) {
        $(This).parent().find('.estEdit').show();
        $(This).parent().find('.estDefalt').hide();
        $(This).hide();
        $(".msk_time").mask("Hh:Mm");
    }

    function SaveEstimatedTimeEditOption(This, id_task_flow) {
        //msk_time
        var time = $(This).parent().find('.msk_time').val();
        $.ajax({
            async: false,
            type: 'POST',
            url: WEB_BASE_URL + 'index.php/Project/updateEstimatedTimeForWeekWorkLoad',
            dataType: 'json',
            data: {id_task_flow: id_task_flow, time: time},
            success: function (res) {
                if (res.status) {
                    TssLib.notify(res.message);
                    $(This).parent().hide();
                    $(This).parent().parent().find('.estDefalt').html(time+':00').show();
                    $(This).parent().parent().find('.editEstimatedTimeEditOption').show();
                }
                else {
                    TssLib.notify(res.data, 'warn', 5);
                }
            }
        });
    }

</script>