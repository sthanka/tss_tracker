<!-- Modal Dialog  dekete confirmation-->
<div class="modal fade" id="oasis-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel panel-default panel-body padding20 clearfix">
                    <p></p>
                </div>
                <div class="panel-footer col-sm-12 clearfix text-right">
                    <button type="button" class="button-common module btn-danger">Yes</button>
                    <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="mainpanel">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3 class="pageTitle">Project Creation</h3>
        </div>
        <div class="col-sm-12 pr0">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3 class="pageTitle">Project Creation</h3>
                    <h3 class="pull-right dropdown">
                        <a href="<?= site_url('Project')?>" class="pull-right border-left"><i class="fa fa-list" title="List"></i></a>
                        <!--<a href="javascript:;" class="pull-right border-left"><i class="fa fa-plus"></i></a>-->
                    </h3>

                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                        <div class="col-sm-12 padding0 clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input_container">
                                        <input type="text"  name="project_name" class="form-control rval">
                                    </div>
                                    <label class="control-label">Name of the Project</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input_container mt10">
                                        <select id="client_id" name="client_id" class="rval"></select>
                                    </div>
                                    <label class="control-label">Client</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            </div>
                        </div>
                        <div class="col-sm-12 padding0 clearfix mb20">
                            <div class="col-sm-4">
                                <div class="form-group padding0">
                                    <div class="input_container">
                                        <input id="project_start_date" name="project_start_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                    </div>
                                    <label class="control-label col-sm-12">Start Date:</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group padding0">
                                    <div class="input_container">
                                        <input id="project_end_date" name="project_end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                    </div>
                                    <label class="control-label col-sm-12">End Date:</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 padding0 clearfix mb20">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input_container mt10">
                                        <select name="project_manager" id="project_manager" class="rval"></select>
                                    </div>
                                    <label class="control-label">Project Manager</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 padding0 clearfix mb20">
                            <!--<div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input_container">
                                        <input type="text" name="first_name" class="form-control rval">
                                    </div>
                                    <label class="control-label">Country</label>
                                </div>
                            </div>-->
                            <div class="col-sm-8">
                                <div class="form-group padding0">
                                    <div class="input_container" style="margin-top:12px;">
                                        <select id="status" name="status">
                                            <option value="in-progress">In Progress</option>
                                            <option value="open">Open</option>
                                            <option value="completed">Completed</option>
                                            <option value="reopen">Reopen</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-12">Status:</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 padding0 clearfix mb20">
                            <div class="col-sm-8">
                                <div class="form-group padding0">
                                    <div class="input_container">
                                        <textarea id="description" name="description" class="form-control rval"></textarea>
                                    </div>
                                    <label>Description</label>
                                </div>
                            </div>
                            <?php if($this->session->userdata('user_type_id') == 2){ ?>
                            <div class="col-sm-4">
                                <input type="hidden" name="id_project">
                                <button  div-submit="true" class="button button-common module mt30" > <?=($project_id)? 'Save':'Save & Continue';  ?> </button> <!--onclick="saveProject(this)"-->
                            </div>
                            <?php } ?>
                        </div>
                        </form>
                        <div class="col-sm-12 padding0" >
                            <?php if($project_id){ ?>
                                <ul role="tablist" class="nav nav-tabs nav-tabs-top custom-nav-tabs check-tabs">
                                    <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ ?>
                                    <li class="check-right active" ><a data-toggle="tab" href="#departments" ><p class="tab-list">Departments</p></a></li>
                                    <li class="check-right"><a data-toggle="tab" href="#milestones"><p class="tab-list">Milestones</p></a></li>
                                    <li class="check-right"><a data-toggle="tab" href="#openItems" onclick="openItemsTabOpen(this)"><p class="tab-list">Open Items</p></a></li>
                                    <?php } ?>
                                    <li class="check-right<?php if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==1){ ?>active <?php } ?>  "><a data-toggle="tab" href="#task_list" onclick="openTaskListTabOpen(this)"><p class="tab-list">Tasks List </p></a></li>
                                    <li class="check-right"><a data-toggle="tab" href="#upload" onclick="uploadExcelModal(this)" ><p class="tab-list">Tasks upload</p></a></li>
                                    <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1) or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_lead')==1)){ ?>
                                    <li class="check-right"><a data-toggle="tab" href="#documentupload" onclick="uploadDocumentModal(this)" ><p class="tab-list">Requirements</p></a></li>
                                    <?php } ?>
                                    <?php /*if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ */?>
                                        <li class="check-right"  onclick="getProjectTaskList(<?=$project_id?>)"  ><a data-toggle="tab" href="#projectTaskList"><p class="tab-list">Pending Approval</p></a></li>
                                    <?php /*} */?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($project_id){ ?>
                <div>
                    <div class="tab-content pb0">
                        <div id="departments" class="tab-pane padding10 <?php if($this->session->userdata('user_type_id')==2 or ($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_manager')==1)){ ?>active<?php } ?>">
                            <h4 class="section-heading">Department / Team</h4>
                            <div class="col-sm-12 padding0">
                                <table class="table add-department-table mt20 mb0">
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <form class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="creat-dep-team-form" >
                                        <tr>
                                            <td style="width:15%;">
                                                <div class="form-group">
                                                    <div class="input_container mt10">
                                                        <select name="department_id" id="department_id" class="rval"></select>
                                                    </div>
                                                    <label class="control-label">Department Name</label>
                                                </div>
                                            </td>
                                            <!--<td style="width:10%; >
                                                <div class="form-group">
                                                    <div class="input_container">
                                                        <input type="text" name="estimation_time" id="estimation_time"  class="form-control rval pInt">
                                                    </div>
                                                    <label class="control-label">Estimation Time (hours)</label>
                                                </div>
                                            </td>-->
                            <input type="hidden" name="estimation_time" id="estimation_time"  class="form-control rval pInt">
                                            <td style="">
                                                <div class="form-group">
                                                    <div class="input_container mt10">
                                                        <select multiple id="employee_id" name="employee_id" class="form-control rval"></select>
                                                    </div>
                                                    <label class="control-label">Team Members</label>
                                                </div>
                                            </td>
                                            <!--<td style="">
                                                <div class="form-group">
                                                    <div class="input_container">
                                                        <input type="text" name="estimation_percentage" id="estimation_percentage"  class="form-control rval pInt">
                                                    </div>
                                                    <label class="control-label">Estimation Percentage</label>
                                                </div>
                                            </td>-->
                                            <td style="width: 145px;">
                                                <button class="button button-common module mt10" div-submit="true"> Add Department</button>
                                            </td>
                                        </tr>
                                    </form>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="milestones" class="tab-pane padding10">
                            <h4 class="section-heading">Milestone</h4>
                            <div class="col-sm-12 padding0">
                                <table class="table add-milestone-table mt20 mb0">
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <form class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="create-milestone-form" >
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input_container">
                                                        <input type="text" id="milestone_name" name="milestone_name" class="form-control rval">
                                                    </div>
                                                    <label class="control-label">Milestone Name</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group padding0">
                                                    <div class="input_container">
                                                        <input type="text" name="milestone_start_date" placeholder="dd/mm/yyyy" id="milestone_start_date" class="rval form-control tssDatepicker">
                                                    </div>
                                                    <label class="control-label col-sm-12">Start Date:</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group padding0">
                                                    <div class="input_container">
                                                        <input type="text" placeholder="dd/mm/yyyy" name="milestone_end_date" id="milestone_end_date" class="rval form-control tssDatepicker">
                                                    </div>
                                                    <label class="control-label col-sm-12">End Date:</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group padding0" style="width: 200px">
                                                    <div class="input_container" style="margin-top:12px;">
                                                        <select name="milestone_status" id="milestone_status">
                                                            <option value="">Select</option>
                                                            <option value="ontrack">On Track</option>
                                                            <option value="concern">Concern</option>
                                                            <option value="delayed">Delayed</option>
                                                        </select>
                                                    </div>
                                                    <label class="control-label col-sm-12">Status:</label>
                                                </div>

                                            </td>
                                            <td>
                                                <button class="button button-common module mt20" div-submit="true" id=""> Add Milestone</button>
                                            </td>
                                        </tr>
                                    </form>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="openItems" class="tab-pane padding10">
                            <h4 class="section-heading">Open Items</h4>
                            <div class="col-sm-12 padding0">
                                <table class="table add-openitem-table mt20 mb0">
                                    <tbody></tbody>
                                    <tfoot>
                                    <form class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="create-openitems-form" >
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input_container">
                                                        <input type="text" name="open_item_description" id="open_item_description" class="form-control rval">
                                                    </div>
                                                    <label class="control-label">Description</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input_container mt10">
                                                        <select type="text" name="open_item_user_id" id="open_item_user_id" class="rval"></select>
                                                    </div>
                                                    <label class="control-label">User Name</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group padding0">
                                                    <div class="input_container" style="margin-top:12px;">
                                                        <select name="open_item_status" id="open_item_status">
                                                            <option value="">Select</option>
                                                            <option value="ontrack">On Track</option>
                                                            <option value="concern">Concern</option>
                                                            <option value="delayed">Delayed</option>
                                                        </select>
                                                    </div>
                                                    <label class="control-label col-sm-12">Status:</label>
                                                </div>

                                            </td>
                                            <td style="width:145px;">
                                                <button class="button button-common module mt20" div-submit="true" id=""> Add Open Item</button>
                                            </td>
                                        </tr>
                                    </form>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="task_list" class="tab-pane padding10 <?php if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==1){ ?>active<?php } ?>">
                            <div class="grid-details-table">
                                <div class="grid-details-table-header">
                                    <h3 >Task List</h3>
                                    <input type="text" class="searchInput pull-right"  />
                                    <a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="grid-details-table-content clearfix padding0">
                                    <div class="tbl_wrapper border0">
                                        <table id="tasks-list" class="table-responsive"></table>
                                        <div id="tasks-list-page"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="upload" class="tab-pane padding10">
                            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
                                <div>
                                    <div class="panel panel-default">
                                        <h4 class="section-heading">Task Upload</h4>
                                        <form id="excelUploadForm" enctype="multipart/form-data" method="post"
                                              action="<?php echo site_url('/Welcome/excelTaskUpload') ?>">
                                            <!--<input type="file" id="file" name="file"/>-->
                                            <div class="col-sm-12 clearfix">
                                                <div class="col-sm-12 padding0 clearfix mb20">
                                                <div class="col-sm-6 pl0">
                                                    <div class="form-group padding0">
                                                        <div class="input_container" style="margin-top:12px;">
                                                            <select id="requirement_id" name="requirement_id"></select>
                                                        </div>
                                                        <label class="control-label col-sm-12">Requirement:</label>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-sm-6 pl0">
                                                    <div class="fileupload fileupload-new input-group" data-provides="fileupload">
                                                        <div class="form-control" data-trigger="fileupload"><i
                                                                class="glyphicon glyphicon-file fileupload-exists"></i>
                                                            <span class="fileupload-preview"></span>
                                                        </div>
                                    <span class="input-group-addon btn btn-default btn-file"><span
                                            class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                        <input type="file" id="file" name="file"></span>
                                                        <a href="#" class="input-group-addon btn btn-default fileupload-exists"
                                                           data-dismiss="fileupload" id="removeFile">Remove</a>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4 pl0">
                                                    <button class="button-common" type="submit" id="uploadExcel"> Upload</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                                        </form>
                                        <form id="TaskUploadExcel" enctype="multipart/form-data" method="post"
                                              action="<?php echo site_url('/Welcome/generateTaskUploadExcel') ?>">
                                            <a class="link font-12 pl0" href="javascript:;" onclick="$('#TaskUploadExcel').submit();"
                                            >Download Sample Excel File</a>
                                            <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                                        </form>
                                        <div id="ExcelRecords">

                                        </div>

                                        <div class="panel-footer text-center">
                                            <button id="submitList" class="button-common" div-submit="true">Submit</button>
                                            <button id="submitCancelList" class="button-common" onclick="clearUploadDocument()">Cancel</button>
                                            <button id="submitTaskOk" class="button-color" data-dismiss="modal">Ok</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="documentupload" class="tab-pane padding10">
                            <div class="form-horizontal NewErrorStyle animate-fld-bg" method="POST" id="priorities-form">
                                <div>
                                    <div class="panel panel-default">
                                        <h4 class="section-heading">Requirements</h4>
                                        <?php if($this->session->userdata('user_type_id') == 2){ ?>
                                        <form id="documentUploadForm" enctype="multipart/form-data" method="post">
                                            <!--<div class="col-sm-12 clearfix">-->
                                                <div class="col-sm-12 clearfix">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input type="text" id="requirement_name" name="requirement_name" class="form-control">
                                                            </div>
                                                            <label class="control-label">Requirement Name</label>
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input type="text" id="requirement_comments" name="comments" class="form-control">
                                                            </div>
                                                            <label class="control-label">Comments</label>
                                                        </div>
                                                    </div>-->
                                                </div>
                                                <div class="col-sm-12 clearfix">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input_container">
                                                                <input id="req_end_date" name="end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                                            </div>
                                                            <label class="control-label"><span class="req-str">*</span>End Date</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 pl0 pt10">
                                                        <div class="fileupload fileupload-new input-group" data-provides="fileupload">
                                                            <div class="form-control" data-trigger="fileupload"><i
                                                                    class="glyphicon glyphicon-file fileupload-exists"></i>
                                                                <span class="fileupload-preview"></span></div>
                                        <span class="input-group-addon btn btn-default btn-file"><span
                                                class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                            <input type="file" id="file" name="file"></span>
                                                            <a href="#" class="input-group-addon btn btn-default fileupload-exists"
                                                               data-dismiss="fileupload" id="removeFile">Remove</a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix text-center">
                                                    <div class="padding10">
                                                        <button class="button-common" type="submit" id="uploadExcel"> Upload</button>
                                                    </div>
                                                </div>
                                            <!--</div>-->
                                            <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                                        </form>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- list -->
                            <div class="grid-details-table">
                                <div class="grid-details-table-header">
                                    <h3 >Requirement List</h3>
                                    <input type="text" class="searchInput pull-right reqSearch"  />
                                    <a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="grid-details-table-content clearfix padding0">
                                    <div class="tbl_wrapper border0">
                                        <table id="project-requirement-list" class="table-responsive"></table>
                                        <div id="project-requirement-list-page"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="projectTaskList" class="tab-pane padding10">
                            <?php if( $this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ ?>
                                <div class="col-sm-12 clearfix text-right" id="approveButton" style="display: none;">
                                    <div class="padding10">
                                        <button class="button-common" type="button" onclick="approveSelected()"> Approve Selected</button>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="grid-details-table">
                                <div class="grid-details-table-header">
                                    <h3 >Task List</h3>
                                    <input type="text" class="searchInput pull-right pendingTaskSearch"  />
                                    <a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="grid-details-table-content clearfix padding0">
                                    <div class="tbl_wrapper border0">
                                        <table id="Project-tasks-list" class="table-responsive"></table>
                                        <div id="Project-tasks-list-page"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div id="add-user-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Edit Department</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <select readonly id="modal_department_id" name="department_id" class="form-control rval"></select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Department Name</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <select multiple id="modal_employee_id" name="employee_id" class="form-control rval"></select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Team Members</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6" style="display: none;">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" name="estimation_time" id="modal_estimation_time"  class="form-control rval pInt">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Estimation Time (days)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer text-center">
                        <button class="button-common" div-submit="true">Save</button>
                        <button class="button-color" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="edit-milestone-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-edtMilestone-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Edit Milestone</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" name="milestone_name" id="milestone_name" class="form-control rval">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span> Milestone Name</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <select id="milestone_status" name="milestone_status" class="form-control rval">
                                                <option value="">Select</option>
                                                <option value="ontrack">On Track</option>
                                                <option value="concern">Concern</option>
                                                <option value="delayed">Delayed</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span> Milestone Status</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input id="milestone_start_date" name="milestone_start_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Start Date</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input id="milestone_end_date" name="milestone_end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>End Date</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer text-center">
                        <button class="button-common" div-submit="true">Update</button>
                        <button class="button-color" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="edit-openTask-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-editOpenTask-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Edit Open Task</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" name="open_item_description" id="open_item_description" class="form-control rval">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span> Description</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <select id="open_item_user_id" name="open_item_user_id" class="form-control rval">
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span> Username</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <select id="open_item_status" name="open_item_status" class="form-control rval">
                                                <option value="">Select</option>
                                                <option value="ontrack">On Track</option>
                                                <option value="concern">Concern</option>
                                                <option value="delayed">Delayed</option>
                                            </select>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span> Status</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer text-center">
                        <button class="button-common" div-submit="true">Update</button>
                        <button class="button-color" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-client-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Client</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="task_name"  id="task_name"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Task Name :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0 pmdiv">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input id="start_date" name="start_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Start Date :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input id="end_date" name="end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>End Date :</label>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval msk_time" name="estimated_time"  id="estimated_time"  />
                                        </div>
                                        <label class="control-label">Estimated Time :</label>
                                    </div>
                                </div>
                            </div>-->
                            <div class="moduleDiv pmdiv">

                            </div>
                            <div class="col-sm-12 clearfix">
                                <div id="getDepartmentList" style="padding-top: 12px;font-size: 12px;"></div>
                            </div>
                            <div class="col-sm-12 clearfix pl0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container mt10">
                                            <select name="requirement_id" id="requirement_id" class="rval"></select>
                                        </div>
                                        <label class="control-label">Requirement</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control rval"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <div class="change-padd">
                                        <label><span class="req-str">*</span>Use case</label>
                                        <textarea name="use_case" id="use_case" class="form-control rval"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control rval" name="id_project_task" />
                            <input type="hidden" class="form-control rval" name="id_task_flow" />
                            <input type="hidden" class="form-control rval" name="id_project" />
                            <input type="hidden" class="form-control rval" name="department_id" />
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="add-project-task-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-create-Schedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Client</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="task_name"  id="task_name"/>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Task Name :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input id="start_date" name="start_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Start Date :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input id="end_date" name="end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>End Date :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval msk_time" name="estimated_time"  id="estimated_time" disabled />
                                        </div>
                                        <label class="control-label">Estimated Time :</label>
                                    </div>
                                </div>
                            </div>
                            <div class="moduleDiv">

                            </div>
                            <div class="col-sm-12 clearfix">
                                <div id="getDepartmentList" style="padding-top: 12px;font-size: 12px;"></div>
                            </div>
                            <div class="col-sm-12 clearfix pl0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container mt10">
                                            <select name="requirement_id" id="requirement_id" class="rval"></select>
                                        </div>
                                        <label class="control-label">Requirement</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <div class="change-padd">
                                        <label>Description</label>
                                        <textarea name="description" id="description" class="form-control rval"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <div class="form-group bt-default">
                                    <div class="change-padd">
                                        <label><span class="req-str">*</span>Use case</label>
                                        <textarea name="use_case" id="use_case" class="form-control rval"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control rval" name="id_project_task" />
                            <input type="hidden" class="form-control rval" name="id_task_flow" />
                            <input type="hidden" class="form-control rval" name="id_project" />
                            <input type="hidden" class="form-control rval" name="department_id" />
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="taskDetails" class="modal-wrapper" >
        <div id="detailsContent">

        </div>
    </div>
    <Script>
        var departments = '<?php echo $department_name; ?>';
        var project_id = '<?php echo $project_id; ?>';

        TssLib.docReady(function () {
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {
                    if (result.data.status) {
                        project_id = result.data.data;
                        if(result.data.message == 'added'){ location.href= TssConfig.TT_SERVICE_URL +'Project/projectCreation/'+project_id; }
                        TssLib.notify('project '+result.data.message+' successfully ', null, 5);
                    }else{
                        TssLib.notify('project name already exist ', 'warn', 5);
                    }
                },
                before: function (formObj) {
                    var project_start_date = $(formObj).find("#project_start_date").val(),
                        project_end_date = $(formObj).find("#project_end_date").val(),
                        startDateParts = project_start_date.split('/'),
                        endDateParts = project_end_date.split('/');
                    if(new Date(startDateParts[2], parseInt(startDateParts[1], 10) - 1, startDateParts[0], '12', '00').getTime() > new Date(endDateParts[2], parseInt(endDateParts[1], 10) - 1, endDateParts[0], '12', '00').getTime()){ TssLib.notify('start date should greater than end date', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { 'project_end_date': project_end_date, 'project_start_date':project_start_date});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/add_project');
                    return true;
                }
            });
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#creat-dep-team-form'), callback: function (result) {
                    if (result.data.status) {
//                        addDepartment();
                        $("#department_id").select2("val", "");
                        $('#estimation_time').val('');
                        $('#estimation_percentage').val('');
                        $("#employee_id").select2("val", "");
                        getDepartmentList(project_id);
                        TssLib.notify('Department added successfully ', null, 5);
                    }else{
//                        TssLib.notify('Department already exist ', 'warn', 5);
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    getDepartmentList(project_id);
                    if(project_id == 0){ TssLib.notify('Please save project ', 'warn', 5); return false; }
                    /*if($('#department_id').val() == '' || $('#estimation_time').val() == '' || $('#employee_id').val() == null || $('#estimation_percentage').val() == ''){  TssLib.notify('Please fill All fields ', 'warn', 5); return false;}*/
                    if($('#department_id').val() == '' || $('#employee_id').val() == null || $('#estimation_percentage').val() == ''){  TssLib.notify('Please fill All fields ', 'warn', 5); return false;}
                    if($('#estimation_percentage').val() !== null && $('#estimation_percentage').val() >100){ TssLib.notify('Percentage Exceeds', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { 'project_id':project_id});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/add_project_dept_team');
                    return true;
                }
            });
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#create-milestone-form'), callback: function (result) {
                    if (result.data.status) {
                        getMilestoneList(project_id);
                        $('#milestone_name').val('');
                        $('#milestone_start_date').val('');
                        $('#milestone_end_date').val('');
                        $("#milestone_status").select2("val", "");
                        TssLib.notify('Milestone added successfully ', null, 5);
                    }
                },
                before: function (formObj) {
                    if(project_id == 0){ TssLib.notify('Please save project ', 'warn', 5); return false; }
                    if($('#milestone_name').val() == '' || $('#milestone_start_date').val() == '' || $('#milestone_end_date').val() == '' || $('#milestone_status').val() == '' ){  TssLib.notify('Please fill All fields ', 'warn', 5); return false;}
                    $(formObj).data('additionalData', { 'project_id':project_id});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/addProjectMilestone');
                    return true;
                }
            });
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#create-openitems-form'), callback: function (result) {
                    if (result.data.status) {
                        $('#open_item_description').val('');
                        $('#open_item_user_id').select2("val", "");
                        $('#open_item_status').select2("val", "");
                        getOpenItemList(project_id);
                        TssLib.notify('Openitem added successfully ', null, 5);
                    }
                },
                before: function (formObj) {
                    if(project_id == 0){ TssLib.notify('Please save project ', 'warn', 5); return false; };
                    if($('#open_item_description').val() == '' || $('#open_item_user_id').val() == '' || $('#open_item_status').val() == '' ){  TssLib.notify('Please fill All fields ', 'warn', 5); return false;}
                    $(formObj).data('additionalData', { 'project_id':project_id});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/addProjectOpenItem');
                    return true;
                }
            });

            if(project_id != 0){ getDepartmentList(project_id); getMilestoneList(project_id); getOpenItemList(project_id); getTaskList(project_id); $('.pageTitle').html('Project Edit'); }
            else{ $('.pageTitle').html('Project Creation'); }
            $globalDs = false;
        });

        $('#department_id').change(function(){
            if($(this).val() != ''){
                getJsonAsyncWithBaseUrl("Project/getDepartmentUsers/"+$(this).val(), {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            TssLib.populateSelect($('#employee_id'), { success: true, data: result.data.data }, 'email', 'id_user');
                        }
                    }
                });
            }
        });
        getJsonAsyncWithBaseUrl("User/getUsersByType?manager=1", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    TssLib.populateSelect($('#project_manager'), { success: true, data: result.data }, 'email', 'id_user');
                }
            }
        });
        function openTaskListTabOpen(){ getTaskList(project_id); }
        function openProjectTaskListTab(){ getProjectTaskList(project_id); }
        function openItemsTabOpen(){
            getJsonAsyncWithBaseUrl("Project/getProjectUsers/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var users = result.data.data;
                        TssLib.populateSelect($('#open_item_user_id'), {success: true, data: users}, 'email', 'id_user');
                    }
                }
            });
        }
        function clearUploadDocument(){
            var $form = $('#excelUploadForm');
            $form.find('#requirement_id').select2("val", "");
            $form.find("#removeFile").trigger('click');
            $form.find('#submitList').attr('disabled', false);
            $('#submitList').hide();
            $('#submitTaskOk').hide();
            $('#submitCancelList').hide();
            $('#ExcelRecords').html('');
        }
        function clearReqUploadDocument(){
            var $form = $('#documentUploadForm');
            $form.find('#requirement_name').val('');
            $form.find('#req_end_date').val('');
            $form.find('input').removeClass('err-bg');
            $form.find('.input-group').removeClass('err-bg');
            $form.find('.error-msg').remove();
            $form.find("#removeFile").trigger('click');
        }
        function uploadExcelModal(e) {
            clearUploadDocument();

            //getting project requirements
            $.ajax({
                url: '<?php echo site_url("/Project/projectRequirement") ?>/'+project_id,
                type: 'POST',
                data: '',
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        TssLib.populateSelect($('#requirement_id'), {success: true, data: data.data}, 'requirement_name', 'id_requirements');
                    } else {

                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });


        }
        var count = 0;
        $('#excelUploadForm').on('submit', function (e) {

            e.preventDefault();
            if(count==0){
                count++;

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: '<?php echo site_url('/Welcome/excelTaskUpload') ?>',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        if (data.status) {
                            $('#submitList').hide();
                            $('#submitTaskOk').hide();
                            $('#submitCancelList').hide();
                            $('#submitList').show();
                            $('#submitCancelList').show();
                            $('#ExcelRecords').html(data.data);
                        } else {
                            $('#ExcelRecords').html(data.data);
                            //count = 0;
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            }
        });

        $('#submitTaskOk').on('click',function(){
            $('#ExcelRecords').html('');
            $(this).hide();
            $('#removeFile').trigger('click')
        });


        $('#submitList').on('click', function () {
            var flag_ind = [];
            $(this).attr('disabled', true);
            $('body').attr('name', 'no-loader');
            // event.preventDefault();
            $formFields = $("#saveTaskList").serializeArray();
            // console.log('$formFields', $formFields);
            var taskList = [];
            $.each($('#taskList').find('tbody > tr'), function () {
                var $td = $('td', this);
//                    console.log($td.length);
                var $obj = {};
                //$obj['project_name'] = $td.eq(1).find('input').val();
                $obj['module_name'] = $td.eq(1).find('input').val();
                $obj['task_name'] = $td.eq(2).find('input').val();
                $obj['start_date'] = $td.eq(3).find('input').val();
                $obj['end_date'] = $td.eq(4).find('input').val();
                //$obj['users'] = $td.eq(6).find('input').val();
                $obj['description'] = encodeSpecialChar($td.eq(5).find('input').val());
                $obj['task_type'] = $td.eq(6).find('input[name="task_type"]').val();
                $obj['project_id'] = $td.eq(6).find('input[name="project_id"]').val();
                $obj['requirement_id'] = $td.eq(6).find('input[name="requirement_id"]').val();

                $obj['use_case'] = $td.eq(6).find('input[name="use_case"]').val();
                for(var s=7;s<$td.length;s++)
                {
                    $obj[$td.eq(s).find('input').attr('id')] = $td.eq(s).find('input').val();
                }
                taskList.push($obj);

            })
            //console.log('taskList',taskList);

            $.each($(taskList), function (ind, item) {
                console.log(flag_ind);
                if(!flag_ind.includes(ind)) {
                    flag_ind.push(ind);
                    console.log(flag_ind);
                    $('#taskList').find("#statusTD_" + ind).html('<i class="fa fa-spinner"></i>');
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('index.php/Welcome/excelTaskUploadAjax')?>',
                        data: item,
                        dataType: 'json',
                        async: false,
                        success: function (result) {
                            statusChang(result, ind);
                            if (ind == ($(taskList).length - 1)) {
                                //$("#backlogTab").trigger("click");
                                var $form = $('#excelUploadForm');
                                $form.find('#requirement_id').select2("val", "");
                                $form.find("#removeFile").trigger('click');
                                $form.find('#submitList').attr('disabled', false);
                                $('#submitList').hide();
                                $('#submitCancelList').hide();
                                $('body').removeAttr('name', 'no-loader');
                                $('#submitTaskOk').show();
                                $('#tasks-list').trigger('reloadGrid');
                                TssLib.notify('Task List Updated', null, 5);
                                $('#submitList').attr('disabled', false);
                                count = 0;
                            }
                        }
                    });
                }
            })
        })

        var $requirementDocGrid = $('#project-requirement-list');
        function uploadDocumentModal(e) {
            clearReqUploadDocument();
            getRequirementDocList();
            $requirementDocGrid.trigger('reloadGrid');
            $('#documentUploadForm').on('submit', function (e) {
                e.preventDefault();
                var count = 0;

                var formData = new FormData($(this)[0]);

                $('#date').removeClass('err-bg');

                var name = $('#requirement_name').val();
//                var comments = $('#requirement_comments').val();
                var end_date = $('#req_end_date').val();

                flag = 0;
                if (name == '') {
                    $('#requirement_name').addClass('err-bg');
                    $('#requirement_name').before('<span class="error-msg">Required</span>');
                    flag++;
                }

                if (end_date == '') {
                    $('#req_end_date').addClass('err-bg');
                    $('#req_end_date').before('<span class="error-msg">Required</span>');
                    flag++;
                }

                if($('#documentUploadForm').find("#removeFile").css('display')=='none'){
                    $('#documentUploadForm').find('.fileupload ').addClass('err-bg');
                    $('#documentUploadForm').find('.fileupload').before('<span class="error-msg">Required</span>');
                    flag++;
                }

                if(flag == 0){
                    /*if(count == 0){
                        count++;*/
                        $('#uploadExcel').attr('disable',true);
                        $.ajax({
                            url: '<?php echo site_url('/Project_requirement/uploadRequirement') ?>',
                            type: 'POST',
                            data: formData,
                            dataType: 'json',
                            success: function (data) {
                                if (data.status) {
                                    clearReqUploadDocument();
                                    $requirementDocGrid.trigger('reloadGrid');
                                    TssLib.notify('Document uploaded successfully', null, 5);
                                } else {
                                    TssLib.notify(data.message , 'warn', 5);
                                }
                                $('#uploadExcel').prop("disabled", false);
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    }
                /*}
                return false;*/
            });
        }
        function getRequirementDocList(){
            $requirementDocGrid.jqGrid({
                //rowNum: -1,
                url: TssConfig.TT_SERVICE_URL + 'Project_requirement/getRequrementList',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                postData: {'project_id':'<?php echo $project_id; ?>'},
                extSearchField: '.reqSearch',
                <?php //if($this->session->userdata('user_type_id') == 2){ ?>
                colNames: ['id_requirements','Name','Document','No.of Task','Est. Time','Add.Time','log time','Remaining','Appr.Com.date','End Date','Created Date','end_date','created_date_time','project_id', 'created_by','Action'],
                colModel: [
                    { name: 'id_requirements', index: 'id_requirements', hidden: true ,key:true},
                    { name: 'requirement_name', index: 'requirement_name',width: 30 },
                    { name: 'requirement_document', index: 'requirement_document',width: 40,formatter: function (c, o, d) {
                        return '<a title="' + d.requirement_document + '" href="' + d.file_url+ '" download="'+d.requirement_document+'" target="_blank">' + d.requirement_document + '</a>';
                    }
                    },
                    /*{ name: 'comments', index: 'comments', hidden: false,width: 100 },*/
                    { name: 'task_count', index: 'task_count', hidden: false,width: 12,sortable: false },
                    { name: 'tot_estimated_time', index: 'tot_estimated_time', hidden: false,width: 15, sortable: false },
                    { name: 'additional_time', index: 'additional_time', hidden: false,width: 15, sortable: false },
                    { name: 'tot_log_time', index: 'tot_log_time', hidden: false,width: 15, sortable: false },
                    { name: 'tot_remaining_time', index: 'tot_remaining_time', hidden: false,width: 15, sortable: false },
                    { name: 'last_task_complition_date', index: 'last_task_complition_date', hidden: false,width: 15, sortable: false },
                    { name: 'end_date', index: 'end_date', hidden: false,width: 25 },
                    { name: 'created_date', index: 'created_date_time', hidden: false,width: 25 },
                    { name: 'e_date', index: 'e_date',hidden: true },
                    { name: 'created_date_time', index: 'created_date_time',hidden: true },
                    { name: 'project_id', index: 'project_id',hidden: true },
                    { name: 'created_by', index: 'created_by',hidden: true },
                    { name: 'Action', index: 'Action',width: 15, sortable: false,  formatter: function(c,o,d){
                        var _html = '';
                        var innerHtml = '';
                        if(d.task_count!=0){
                            _html += '--';
                        }
                        else {
                            <?php if( $this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id') == 3 and $this->session->userdata('is_manager') == 1)){ ?>
                            _html += '<a href="javascript:;" onclick="deleteRequirement(' + d.id_requirements + ')" class="pull-left task-status btn-danger ml10" title="Delete"><i class="fa fa-trash-o"></i></a>';
                            <?php } else { ?>
                            _html += '--';
                            <?php } ?>
                        }
                        return _html;
                    }
                    }
                ],
                <?php /*} else if($this->session->userdata('is_lead') == 1){ */?>/*
                colNames: ['id_requirements','Name','Document','No. of Task','End Date','Created Date','end_date','created_date_time','project_id', 'created_by'],
                colModel: [
                    { name: 'id_requirements', index: 'id_requirements', hidden: true ,key:true},
                    { name: 'requirement_name', index: 'requirement_name',width: 100 },
                    { name: 'requirement_document', index: 'requirement_document',width: 40,formatter: function (c, o, d) {
                        return '<a title="' + d.requirement_document + '" href="' + d.file_url+ '" download="'+d.requirement_document+'" target="_blank">' + d.requirement_document + '</a>';
                    }
                    },

                    { name: 'task_count', index: 'task_count', hidden: false,width: 10 },
                    { name: 'e_date', index: 'e_date', hidden: false,width: 30 },
                    { name: 'created_date', index: 'created_date_time', hidden: false,width: 30 },
                    { name: 'end_date', index: 'end_date',hidden: true },
                    { name: 'created_date_time', index: 'created_date_time',hidden: true },
                    { name: 'project_id', index: 'project_id',hidden: true },
                    { name: 'created_by', index: 'created_by',hidden: true },
                ],
                */<?php /*} */?>
                pager: 'project-requirement-list-page'
            }).navGrid('#project-requirement-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true,
            });
        }

        function statusChang(result, ind) {
            // console.log('    result', result);
            $('.panel-body').scrollTop(ind * 60);
            /*$('.panel-body').animate({
             scrollTop: ind*40
             });*/
            if (result.status) {
                // $('#taskList').find("#statusTD_" + ind).html('<i class="fa fa-check green" ></i>' + result.message);
                $('#taskList').find("#statusTD_" + ind).html(result.message);
                $('#taskList').find("#rowTR_" + ind).addClass('success-bg');
            } else {
                $('#taskList').find("#statusTD_" + ind).html(result.message);
                $('#taskList').find("#rowTR_" + ind).addClass('error-bg')
            }
        }
        function removeDepartment(e){ $(e).closest('tr').remove(); };
        function addDepartment(){
            var _departmentName = $('#department_id').select2('data').text,
                _departmentId = $('#department_id').select2('val'),
                _addDepartmentEstimatedTime = $('#estimation_time').val(),
                _addDepartmentTeamMmembers = $('#employee_id').select2('data'),
                _TeamMmembersId = $('#department_id').select2('val'),
                _addDepartmentPercentage = $('#estimation_percentage').val();
            var user = '';
            if(_addDepartmentTeamMmembers.length>1){
                for(var a in _addDepartmentTeamMmembers){
                    user += _addDepartmentTeamMmembers[a].text+', ';
                }
            }else if(_addDepartmentTeamMmembers.length==1){
                for(var a in _addDepartmentTeamMmembers){
                    user += _addDepartmentTeamMmembers[a].text+', ';
                }
            }
            _tr = $('<tr  style="background-color: lightgrey;" class="departmentInputFieldDraggable ui-draggable ui-draggable-handle">' +
                '<td><div class="view_input">'+ '<p>'+_departmentName +'<p>'+'</div></td>'+
                /*'<td><div class="view_input">'+ '<p>' +_addDepartmentEstimatedTime +'</p>'+'<label class="margin0">Estimation Time</label>'+'</div></td>'+*/
                '<td><div class="view_input">'+ '<p>' + user + '</p>' +'<label class="margin0">Team Members</label>'+'</div></td>'+
                    /*'<td><div class="view_input">'+ '<p>' + _addDepartmentPercentage + '</p>' +'<label class="margin0">Estimation Percentage</label>'+'</div></td>' +'' +*/
//                '<td><div><button type="button" class="button-common button-color btn-default" onclick="getDepartmentList('+project_id+');" data-dismiss="modal">Cancel Swapping</button></div></td>' +'' +
                '<input type="hidden" id="data-deptt" value="'+_departmentId+'">' +
                '<input type="hidden" id="data-depttime" value="'+_addDepartmentEstimatedTime+'">' +
                '<input type="hidden" id="data-depttMember" value="'+_TeamMmembersId+'">' +
                    /*'<input type="hidden" id="data-depttPerc" value="'+_addDepartmentPercentage+'">' +*/
                '</tr>');

            $('.add-department-table tbody').append(_tr);
            loadDragdrop();
        };
        function loadDragdrop(){
            $(".departmentInputFieldDraggable" ).draggable({
                appendTo: 'body',
                containment: 'window',
                scroll: false,
                cursor: 'move',
                helper: 'clone'
            });
            $(".departmentInputFieldDraggable").bind("drag", function(event, ui) {
                ui.helper.css("border", "1px solid #000");
            });
            var _dragDropSelector = $( ".add-department-table tr" );
            _dragDropSelector.droppable({
                hoverClass: "ui-drop-hover",
                drop: function(event, ui) {
                    var department_id = ui.draggable.find('#data-deptt').val();
                    /*var estimation_time = ui.draggable.find('#data-depttime').val();*/
                    var employee_id = ui.draggable.find('#data-depttMember').val();
                    var estimation_percentage = ui.draggable.find('#data-depttPerc').val();
                    var dropable_id = $(this).find('#data-id').val();
                    var item = {'deptt': department_id, 'dropable_id': dropable_id, 'project_id': project_id};
                    //console.log(item);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('index.php/Project/changeDepartmentSequence')?>',
                        data: item,
                        dataType: 'json',
                        async: true,
                        success: function (result) {
                            getDepartmentList(project_id);
                            TssLib.notify('Department sequence updated', null, 5);
                        }
                    });
                },
                over:function(event, ui) {
                },
                out: function( event, ui ) {
                }
            });
            _dragDropSelector.sortable();
        }
        function getDepartmentList(projectId){
            getJsonAsyncWithBaseUrl("project/getUserForProject?project_id="+projectId, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.status == true) {
                        if (result.data.data !=  0) {
                            var rawData = result.data.data;
                            var _html = '';
                            for(var a in rawData){
                                _html += '<tr class="ui-droppable ui-sortable departmentInputFieldDraggable">' +
                                    '<td><div class="view_input drag-icon"><p>'+rawData[a].department_name+'</p></div></td>'+
                                    '<td><div class="view_input"><p>'+rawData[a].log_time+'</p><label class="margin0">Spend Efforts</label></div></td>';
                                    /*'<td><div class="view_input"><p>'+rawData[a].estimation_time+'</p><label class="margin0">Estimation Time</label></div></td>'*/

                                _html +=   '<td><div class="view_input added-list-tag-style"><p>';
                                var name = rawData[a].emp_name.split(',');
                                $.each($(name), function (ind, item) {
                                    _html +=   '<span>'+item+'</span>';
                                });
                                _html += '</p><label class="margin0">Team Members</label></div></td>' +
                                        /*'<td><div class="view_input"><p>'+rawData[a].estimation_percentage+'</p><label class="margin0">Estimation Percentage</label></div></td>'  +*/
                                    '<td><a onclick="editDepartment(this,'+rawData[a].id_project_department+',project_id)"><span class="fa fa-pencil"></span></a></td>' +
                                    '<input type="hidden" id="data-id" value="'+rawData[a].id_project_department+'">' +
                                    '<input type="hidden" id="data-deptt" value="'+rawData[a].department_id+'">' +
                                    /*'<input type="hidden" id="data-depttime" value="'+rawData[a].estimation_time+'">' +*/
                                    '<input type="hidden" id="data-depttMember" value="'+rawData[a].userid+'">' +
                                    '<input type="hidden" id="data-depttPerc" value="'+rawData[a].estimation_percentage+'">' +
                                    '</tr>';
                            }
                            $('.add-department-table > tbody').html(_html);
                        }else{
                            //
                            $('.add-department-table tbody').html(_html);
                        }
                        loadDragdrop();
                    }
                }
            });
        }
        function getMilestoneList(projectId){
            var _html = '';
            $('.add-milestone-table > tbody').html(_html);
            getJsonAsyncWithBaseUrl("project/getMilestoneForProject?project_id="+projectId, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.status == true) {
                        if (result.data.data !=  0) {
                            var rawData = result.data.data;
                            var $index= 0;
                            for(var a in rawData){ $index++;
                                _html += '<tr>' +
                                    '<td><div><span class="milestone-count">'+$index+'</span></div><div class="view_input"><p>'+rawData[a].milestone_name+'</p>' +
                                    '<label class="margin0">Milestone Name</label></div></td>' +
                                    '<td>' +
                                    '<div class="view_input">' +
                                    '<p>'+rawData[a].milestone_start_date+'</p>' +
                                    '<label class="margin0">Start Date</label>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td>' +
                                    '<div class="view_input">' +
                                    '<p>'+rawData[a].milestone_end_date+'</p>' +
                                    '<label class="margin0">End Date</label>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td>' +
                                    '<div class="view_input">' +
                                    '<p>'+rawData[a].milestone_status+'</p>' +
                                    '<label class="margin0">Status</label>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td>' +
                                    '<a onclick="editMilestone(this, '+rawData[a].id_milestone+')" class="milestone-delete"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;' +
                                    '<a onclick="removeMilestone(this, '+rawData[a].id_milestone+')" class="milestone-delete"><span class="fa fa-trash-o"></span></a>' +
                                    '</td>' +
                                    '</tr>';
                            }
                            $('.add-milestone-table > tbody').html(_html);
                        }else{
                            $('.add-milestone-table > tbody').html(_html);
                        }
                    }
                }
            });
        }
        function editMilestone(This, milestoneId){
            getJsonAsyncWithBaseUrl("project/getMilestoneById?milestone_id="+milestoneId, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        if (result.data.status == true) {
                            var rawData = result.data.data[0];
                            var $modal = TssLib.openModel({ width: '600' }, $('#edit-milestone-modal').html());
                            TssLib.renderData($modal.find('#linen-edtMilestone-form'), rawData);
                            TssLib.ajaxForm({
                                jsonContent: true,
                                form: $('#linen-edtMilestone-form'), callback: function (result) {
                                    if (result.data.status) {
                                        getMilestoneList(project_id);
                                        TssLib.closeModal();
                                        $('#milestone_name').val('');
                                        $('#milestone_start_date').val('');
                                        $('#milestone_end_date').val('');
                                        $("#milestone_status").select2("val", "");
                                        TssLib.notify('Milestone updated successfully ', null, 5);
                                    }
                                },
                                before: function (formObj) {
                                    if($('#milestone_name').val() == '' || $('#milestone_start_date').val() == '' || $('#milestone_end_date').val() == '' || $('#milestone_status').val() == '' ){  TssLib.notify('Please fill All fields ', 'warn', 5); return false;}
                                    $(formObj).data('additionalData', { 'milestone_id':milestoneId, 'milestone_start_date':$('#milestone_start_date').val(), 'milestone_end_date':$('#milestone_end_date').val()  });
                                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/updateProjectMilestone');
                                    return true;
                                }
                            });
                        }else{
                            TssLib.notify(result.data.message , 'warn', 5);
                        }
                    }
                }
            });


        }
        function getOpenItemList(projectId){
            $('.add-openitem-table > tbody').html('');
            getJsonAsyncWithBaseUrl("project/getOpenTaskForProject?project_id="+projectId, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.status == true) {
                        var _html = '';
                        if (result.data.data !=  0) {
                            var rawData = result.data.data;
                            for(var a in rawData){
                                _html += '<tr>' +
                                    '<td><div class="view_input"><p>'+rawData[a].open_item_description+'</p><label class="margin0">Description</label>' +
                                    '</div></td><td><div class="view_input"><p>'+rawData[a].name+'</p>' +
                                    '<label class="margin0">User Name</label></div></td><td>' +
                                    '<div class="view_input"><p>'+rawData[a].open_item_status+'</p><label class="margin0">Status</label>' +
                                    '</div></td>' +
                                    '<td>' +
                                    '<a onclick="editOpenItem(this, '+rawData[a].id_project_item+')" class="milestone-delete"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;' +
                                    '<a onclick="removeOpenItem(this, '+rawData[a].id_project_item+')" class="milestone-delete"><span class="fa fa-trash-o"></span></a></td>' +
                                    '</tr>';
                            }
                            $('.add-openitem-table > tbody').html(_html);
                        }else{
                            //
                            $('.add-openitem-table tbody').html(_html);
                        }
                    }
                }
            });
        }
        function editOpenItem(This, openTaskId){
            getJsonAsyncWithBaseUrl("Project/getProjectUsers/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var users = result.data.data;
                        TssLib.populateSelect($('#open_item_user_id'), {success: true, data: users}, 'email', 'id_user');
                    }
                }
            });
            var $modal = TssLib.openModel({ width: '600' }, $('#edit-openTask-modal').html());

            getJsonAsyncWithBaseUrl("project/getOpenTaskById?opentask_id="+openTaskId, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        if (result.data.status == true) {
                            var rawData = result.data.data[0];
                            TssLib.renderData($modal.find('#linen-editOpenTask-form'), rawData);
                            TssLib.ajaxForm({
                                jsonContent: true,
                                form: $('#linen-editOpenTask-form'), callback: function (result) {
                                    if (result.data.status) {
                                        getOpenItemList(project_id);
                                        TssLib.closeModal();
                                        $('#open_item_description').val('');
                                        $("#open_item_user_id").select2("val", "");
                                        $("#open_item_status").select2("val", "");
                                        TssLib.notify('Updated successfully ', null, 5);
                                    }
                                },
                                before: function (formObj) {
                                    if($('#open_item_description').val() == '' || $('#open_item_user_id').val() == '' || $('#open_item_status').val() == '' ){  TssLib.notify('Please fill All fields ', 'warn', 5); return false;}
                                    $(formObj).data('additionalData', { 'opentask_id':openTaskId, 'project_id':project_id  });
                                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/updateProjectOpenItem');
                                    return true;
                                }
                            });
                        }else{
                            TssLib.notify(result.data.message , 'warn', 5);
                        }
                    }
                }
            });
        }
        function removeMilestone(This, id){
            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                $('#oasis-confirm').closest('.modal').modal('hide');
                postJsonAsyncWithBaseUrl("project/deleteMilestone?milestone_id="+id, {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data.status == true) {
                            getMilestoneList(project_id);
                            TssLib.notify('Milestone removed ', null, 5);

                        }
                    }
                });
                /*$('body').find('.oasis-confirm').removeClass('in');*/
            }, 'Yes', 'No');
        }
        function removeOpenItem(This, id){
            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                $('#oasis-confirm').closest('.modal').modal('hide');
                postJsonAsyncWithBaseUrl("project/deleteOpenItem?open_item_id="+id, {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data.status == true) {
                            getOpenItemList(project_id);
                            TssLib.notify('Open Item removed ', null, 5);
                        }
                    }
                });

            }, 'Yes', 'No');
        }
        postJsonAsyncWithBaseUrl("client/getClients", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var clients = result.data.clients;
                    var departments = result.data.departments;
                    TssLib.populateSelect($('#client_id'), {success: true, data: clients}, 'client_name', 'id_client');
                    TssLib.populateSelect($('#department_id'), { success: true, data: departments }, 'department_name', 'id_department');
                    if(project_id != 0){getProjectDetails(project_id)};
                }
            }
        });
        function getProjectDetails(project_id){
            getJsonAsyncWithBaseUrl("Project/getDetailOfProject/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var projectDetails = result.data.data[0];
                        TssLib.renderData($('#linen-createSchedule-form'), projectDetails);
                    }
                }
            });
        }
        function editDepartment(This, id, projectId){
            /*Deptt*/
            var $modal = TssLib.openModel({ width: '600' }, $('#add-user-modal').html());
            postJsonAsyncWithBaseUrl("client/getClients", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var departments = result.data.departments;
                        TssLib.populateSelect($('#modal_department_id'), { success: true, data: departments }, 'department_name', 'id_department');
                        getJsonAsyncWithBaseUrl("project/getUserForProject?project_id="+projectId, {}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data.status == true) {
                                    var $modal = TssLib.openModel({ width: '600' }, $('#edit-project-modal').html());
                                    var rawData = result.data.data, departmentId = '', userIdArray = [], estimation = '',  estimation_percentage= '';
                                    for(var a in rawData) {
                                        if (rawData[a].id_project_department == id) {
                                            estimation = rawData[a].estimation_time;
//                                            estimation_percentage = rawData[a].estimation_percentage;
                                            departmentId = rawData[a].id_department;
                                            userIdArray = rawData[a].userid.split(',');
                                        }
                                    }

                                    /*Employee*/
                                    getJsonAsyncWithBaseUrl("Project/getDepartmentUsers/"+departmentId, {}, {
                                        jsonContent: true,
                                        callback: function (result) {
                                            if (result.data != null) {
                                                TssLib.populateSelect($('#modal_employee_id'), { success: true, data: result.data.data }, 'email', 'id_user');
                                                $("#modal_department_id").select2("val", departmentId);
                                                $modal.find('#modal_estimation_time').val(estimation);
//                                                $modal.find('#modal_estimation_percentage').val(estimation_percentage);
                                                $modal.find('#modal_employee_id').select2("val",userIdArray);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });

            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {
                    if (result.data.status) {
                        TssLib.closeModal();
                        getDepartmentList(project_id);
                        TssLib.notify('users Updated successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message , 'warn', 5);
                    }
                },
                before: function (formObj) {
//                    $(formObj).data('additionalData', { 'project_id':project_id, 'estimation_percentage':$('#modal_estimation_percentage').val()});
                    $(formObj).data('additionalData', { 'project_id':project_id, 'estimation_percentage':0});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/updateProjectEmployee');
                    return true;
                }
            });
        }
        function deleteTask(id_project_task){
            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                $('#oasis-confirm').closest('.modal').modal('hide');
                postJsonAsyncWithBaseUrl("project/deleteTask?project_task_id="+id_project_task, {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data.status == true) {
                            $("#tasks-list").trigger('reloadGrid');
                            TssLib.notify('Task removed ', null, 5);
                        }else{
                            TssLib.notify(result.data.message, 'warn', 5);
                        }
                    }
                });
            }, 'Yes', 'No');
        }
        function DeleteProjectTask(id_project_task){
            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                $('#oasis-confirm').closest('.modal').modal('hide');
                postJsonAsyncWithBaseUrl("project/deleteProjectTask?project_task_id="+id_project_task, {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data.status == true) {
                            $("#tasks-list").trigger('reloadGrid');
                            TssLib.notify('Task removed ', null, 5);
                            $('#Project-tasks-list').trigger('reloadGrid');
                        }else{
                            TssLib.notify(result.data.message, 'warn', 5);
                        }
                    }
                });
            }, 'Yes', 'No');
        }

        function editTask(id, taskId) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            var rowData = $taskGrid.jqGrid('getRowData', id);
            $(".msk_time").mask("Hh:Mm");

            if (TssLib.isBlank(id)) {
                var _html = '';
                _html += '' +
                    '<div class="col-sm-12 clearfix clearboth padding0">' +
                    '<div class="col-sm-6">' +
                    '<div class="form-group"><div class="input_container mt10">' +
                    '<input type="text" name="module_name"/>' +
                    '</div>' +
                    '<label class="control-label"><span class="req-str">*</span>Module Name</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<div class="form-group"><div class="input_container mt10">' +
                    '<input type="text" name="task_type"/>' +
                    '</div>' +
                    '<label class="control-label"><span class="req-str">*</span>Task Type</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-sm-12 clearfix clearboth">' +
                    '<div class="form-group"><div class="input_container mt10">' +
                    '<select id="department_id" class="rval ">' +
                    '<option value="">Select</option>' +
                    '</select>' +
                    '</div>' +
                    '<label class="control-label"><span class="req-str">*</span>Department Name</label>' +
                    '</div>';
                $modal.find('.panel-title').text('Add Task');
                $modal.find('[div-submit="true"]').text('Save');
                $(".moduleDiv").html('');
                $(".moduleDiv").append(_html);
                getJsonAsyncWithBaseUrl("Project/getProjectDepartments/"+project_id, {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            var  task = result.data.data;
                            TssLib.populateSelect($('#department_id'), { success: true, data: task }, 'department_name', 'department_id');

                            TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                        }
                    }
                });
                $.get("<?=site_url('Project/getDepartmentByProject')?>/"+project_id+"/" , {}, function( data ) {
                    $modal.find('#getDepartmentList').html(data);
                    $(".msk_time").mask("Hh:Mm");
                });
            }else{
                $modal.find('.panel-title').text('Edit Task');
                $modal.find('[div-submit="true"]').text('Update');
                $(".moduleDiv").html('');
                if(rowData.pm_approved==1){
                    $modal.find('.pmdiv').hide();
                    $globalDs = true;
                }else{
                    $modal.find('.pmdiv').show();
                    $globalDs = false;
                }
                $.post("<?=site_url('Project/getProjectDepartmentsEstTime')?>" , {'project_id': project_id, 'task_id': taskId}, function( data ) {
                    $modal.find('#getDepartmentList').html(data);
                    $(".msk_time").mask("Hh:Mm");
                });

            }

            TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);

            postJsonAsyncWithBaseUrl("Project/projectRequirement/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.data != null) {
                        TssLib.populateSelect($('#requirement_id'), {success: true, data: result.data.data}, 'requirement_name', 'id_requirements');
                        TssLib.preSelectValForSelectBox($modal.find('#requirement_id'),rowData.requirement_id);
                    }
                }
            });

            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {

                    if (TssLib.isBlank(id)) {
                        var addedTest = 'Added';
                    }else{
                        var addedTest = 'Updated';
                    }
                    if (result.data.status) {
                        $('#tasks-list').trigger('reloadGrid');
                        TssLib.closeModal();
                        $(".moduleDiv").html('');
                        TssLib.notify('Task '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    var start_date = $(formObj).find("#start_date").val(),
                        end_date = $(formObj).find("#end_date").val(),
                        department_id = $(formObj).find("#department_id").val(),
                        task_type = $(formObj).find("#task_type").val(),
                        description = encodeSpecialChar($(formObj).find("#description").val()),
                        use_case = $(formObj).find("#use_case").val(),
                        requirement_id = $(formObj).find("#requirement_id").val(),
                        project_module_id = $(formObj).find("#project_module_id").val();

                    var deptt = [];
                    var collection = $(".department");
                    if(collection.length > 0){
                        collection.each(function() {
                            var This = $(this);
                            var id = This.attr('data-id');
                            var value = This.val();
                            deptt.push({'id': id, 'est': value});
                        });
                    }

                    if(deptt.length<=0){ TssLib.notify("Department can't be blank", 'warn', 5); return false; }

                    if (TssLib.isBlank(id)) {
                        $(formObj).data('additionalData', { start_date:start_date ,end_date:end_date, department_id:department_id , department: deptt ,task_type:task_type, module_id: project_module_id, project_id: project_id, use_case: use_case, requirement_id: requirement_id});
                        $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/addProjectTask');
                    }else{
                        $(formObj).data('additionalData', { department: deptt , start_date:start_date ,end_date:end_date ,description: description, use_case: use_case, requirement_id: requirement_id});
                        $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/updateSingleProjectTask');
                    }
                    return true;
                }
            });
        }
        var $taskGrid = $('#tasks-list');
        function clearTaskUpload(){
            $('#requirement_name').val('');
//             $('#requirement_comments').val('');
            $('#req_end_date').val('');
            $('#documentUploadForm input').removeClass('err-bg');
            $('#documentUploadForm .input-group').removeClass('err-bg');
            $('#documentUploadForm').find('.error-msg').remove();
            $('#documentUploadForm').find("#removeFile").trigger('click');
        }
        function getTaskList(id){
            if(id == 0){ return false; }
            $taskGrid.jqGrid({
                //rowNum: -1,
                url: TssConfig.TT_SERVICE_URL + 'project/task_list',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                postData: {projectIds:id},
                colNames: ['id_task_workflow','id_project', 'Requirement Name','Module Name','Project Title', 'task_name','department_id','project_id','id_project_task','id_task_flow','start_date','end_date','Task','Start Date', 'End Date','Est.Time', 'Log Time','Add.Time','description','Created Date','Completed Date','Status','Action','project_task_name','use_case', 'pm_approved','requirement_id'],
                colModel: [
                    { name: 'id_task_workflow', index: 'id_task_workflow', hidden: true ,key:true},
                    { name: 'projectTitle', index: 'projectTitle', hidden: true },
                    { name: 'requirement_name', index: 'requirement_name',width: 20 },
                    { name: 'module_name', index: 'module_name',width: 25 },
                    { name: 'task_name', index: 'task_name', hidden: true },

                    { name: 'id_project_task', index: 'id_project_task',hidden: true },
                    { name: 'id_task_flow', index: 'id_task_flow',hidden: true },
                    { name: 'start_date', index: 'start_date',hidden: true,width: 20 },
                    { name: 'end_date', index: 'end_date',hidden: true ,width: 20 },
                    { name: 'id_project', index: 'id_project', hidden: true },
                    { name: 'department_id', index: 'department_id', hidden: true },
                    { name: 'id_project', index: 'id_project', hidden: true },
                    { name: 'project_task_name', index: 'project_task_name',width: 40,formatter: function (c, o, d) {
                        //return d.project_task_name;
                        //return '<a title="' + d.project_task_name + '" href="javascript:;" >' + d.project_task_name + '</a>';
                            return '<a href="javascript:;" onclick="openTaskMemeberModal('+ d.id_project_task +')">'+d.project_task_name+'</a>';
                    }
                    },
                    { name: 'task_start_date', index: 'task_start_date',width: 15 },
                    { name: 'task_end_date', index: 'task_end_date',width: 15 },
                    { name: 'estimated_time', index: 'estimated_time',width: 10,formatter: function (c, o, d) {
                        if(d.estimated_time == null){
                            return d.estimated_time;
                        }else{
                            var data = d.estimated_time.split(':');
                            return data[0] + ':' + data[1];
                        }
                    }
                    },
                    { name: 'totalLog', index: 'totalLog',width: 10,sortable: false,formatter: function (c, o, d) {
                        if(d.totalLog == '00:00'){
                            return d.totalLog;
                        }else{
                            var data = d.totalLog.split(':');
                            return data[0] + ':' + data[1];
                        }
                    }
                    },
                    { name: 'additional_time', index: 'additional_time',width: 10,sortable: false,formatter: function (c, o, d) {
                        if(d.additional_time == '00:00'){
                            return d.additional_time;
                        }else{
                            var data = d.additional_time.split(':');
                            return data[0] + ':' + data[1];
                        }
                    }
                    },
                    /*{ name: 'description', index: 'description',width: 28 ,formatter: function (c, o, d) {
                        return '<span title="' + d.description + '" href="javascript:;" >' + d.description + '</span>';
                    }
                    },*/
                    { name: 'description', index: 'description', hidden: true },
                    { name: 'original_created_date_time', index: 'original_created_date_time', hidden: true },
                    { name: 'completed_date', index: 'completed_date',width: 15 },
                    { name: 'current_status', index: 'Action',width: 15,  formatter: function(c,o,d){
                        var innerHtml = '';
                        if(c.toLowerCase() == 'new'){
                            innerHtml += '<span class="pull-left task-status task-inprogress">New</span>';
                        }
                        if(c.toLowerCase() == 'progress'){
                            innerHtml += "<span class='pull-left task-status task-waiting'>In Progress</span>";
                        }
                        if(c.toLowerCase() == 'completed'){
                            innerHtml += '<span class="pull-left task-status task-completed">Completed</span>';
                        }
                        if(c.toLowerCase() == 'hold'){
                            innerHtml += '<span class="pull-left task-status task-open">Hold</span><span class="pull-left task-chat"></span>';
                        }
                        return innerHtml;
                    }
                    },
                    { name: 'current_status', index: 'Action',width: 15,  formatter: function(c,o,d){
                        var _html = '';
                        _html += '<a href="javascript:;" onclick="editTask('+ o.rowId+', '+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Edit"><i class="fa fa-pencil"></i></a>';
                        if(c.toLowerCase() == 'new'){
                            _html += '<a href="javascript:;" onclick="deleteTask('+d.id_project_task+')" class="pull-left task-status btn-danger ml10" title="Delete"><i class="fa fa-trash-o"></i></a>';
                        }
                        if(d.logPresent == false){

                        }else
                            _html += ' -- ';
                        return _html;
                    }
                    },
                    { name: 'project_task_name', index: 'project_task_name', hidden: true },
                    { name: 'use_case', index: 'use_case', hidden: true },
                    { name: 'pm_approved', index: 'pm_approved', hidden: true },
                    { name: 'requirement_id', index: 'requirement_id', hidden: true },
                ],
                pager: 'tasks-list-page'
            }).navGrid('#tasks-list-page', {
                edit: false, add: true, del: false, search: false, refresh: true,
                addfunc: function () {
                    editTask();
                }
            });
            $taskGrid.trigger('reloadGrid');
        }
        function approveSelected(){
            var task_id = [];
            var colection = $ProjecttaskGrid.find(".selectedTask");
            if(colection.length > 0){
                colection.each(function() {
                    var This = $(this);
                    if(This.is(':checked')){
                        var id = This.attr('data-id');
                        task_id.push(id);
                    }
                });
                if(task_id.length>0){
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url('index.php/project/approveMultipleProjectTask')?>',
                        data: {task_id:task_id},
                        dataType: 'json',
                        async: true,
                        success: function (result) {
                            if(result.status){
                                $('#Project-tasks-list').trigger('reloadGrid');
                            }
                        }
                    });
                }
            }
        }
        var $ProjecttaskGrid = $('#Project-tasks-list');
        function getProjectTaskList(id){
            if(id == 0){ return false; }
            $ProjecttaskGrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'project/projectTaskList',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.pendingTaskSearch',
                postData: {projectIds:id},
                <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ ?>
                colNames: ['id_project_task','<input type="checkbox" id="checkAll" onclick="checkAllFun(event)" />','Requirement name', 'Task Name', 'task_name','Task Type','Estimated Time','Department','Start Date','End Date', 'Use Case','Action','id_project', 'module_name', 'project_module_id','id_department','project_id','Est. Time', 'Description','Created Date','project_task_name','requirement_id','start_date','end_date'],
                colModel: [
                    { name: 'id_project_task', index: 'id_project_task', hidden: true ,key:true},
                    { name: 'id_project_task', index: 'id_project_task', hidden: false,width:5 ,sortable: false,formatter: function (c, o, d) {
                        return '<input type="checkbox" class="selectedTask" data-id="' + d.id_project_task + '" name="selectedTask[' + d.id_project_task + ']" />';
                    }
                    },
                    { name: 'requirement_name', index: 'requirement_name',width:20},
                    { name: 'org_task_name', index: 'task_name',width: 40,formatter: function (c, o, d) {
                        return '<a title="' + d.task_name + '" href="javascript:;" >' + d.task_name + '</a>';
                    }
                    },
                    { name: 'task_name', index: 'task_name', hidden: true},
                    { name: 'task_type', index: 'task_type',width:20 },
                    { name: 'estimated_time', index: 'estimated_time',width: 20,sortable: false,formatter: function (c, o, d) {
                        if(d.estimated_time == null){
                            return d.estimated_time;
                        }else{
                            var data = d.estimated_time.split(':');
                            return data[0] + ':' + data[1];
                        }
                    }
                    },
                    { name: 'department_name', index: 'department_name',width:25,hidden: true  },
                    { name: 'from_date', index: 'from_date',width: 20 },
                    { name: 'to_date', index: 'to_date',width: 20 },
                    { name: 'use_case', index: 'use_case',hidden: true },
                    { name: 'status', index: 'status',width: 20,  formatter: function(c,o,d){
                        var _html = '';
                        var innerHtml = '';
                        <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ ?>
                        _html += '<a href="javascript:;" onclick="editProjectTask('+ d.id_project_task +', '+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Edit"><i class="fa fa-pencil"></i></a>';
                        _html += '<a href="javascript:;" onclick="ApproveTask('+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Approve"><i class="fa fa-check"></i></a>';
                        _html += '<a href="javascript:;" onclick="DeleteProjectTask('+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Delete"><i class="fa fa-trash-o"></i></a>';
                        <?php } ?>
                        return _html;
                    }
                    },
                    { name: 'id_project', index: 'id_project', hidden: true },
                    { name: 'module_name', index: 'module_name',hidden: true },
                    { name: 'project_module_id', index: 'project_module_id',hidden: true },
                    { name: 'id_department', index: 'id_department', hidden: true },
                    { name: 'project_id', index: 'project_id', hidden: true },
                    /*{ name: 'id_task_flow', index: 'id_task_flow',hidden: true },*/
                    { name: 'estimated_time', index: 'estimated_time',hidden: true},
                    { name: 'description', index: 'description',hidden: true},
                    /*{ name: 'original_created_date_time', index: 'original_created_date_time',hidden: true },*/
                    { name: 'completed_date', index: 'completed_date',hidden: true},
                    /*{ name: 'current_status', index: 'Action',hidden: true},*/
                    { name: 'project_task_name', index: 'project_task_name', hidden: true },
                    { name: 'requirement_id', index: 'requirement_id', hidden: true },
                    { name: 'start_date', index: 'start_date', hidden: true },
                    { name: 'end_date', index: 'end_date', hidden: true },
                ],
                <?php } else { ?>
                colNames: ['id_project_task','Requirement name', 'Task Name', 'task_name','Task Type','Estimated Time','Department','Start Date','End Date', 'Use Case','Action','id_project', 'module_name', 'project_module_id','id_department','project_id','Est. Time', 'Description','Created Date','project_task_name','start_date','end_date'],
                colModel: [
                    { name: 'id_project_task', index: 'id_project_task', hidden: true ,key:true},
                    { name: 'requirement_name', index: 'requirement_name',width:20},
                    { name: 'org_task_name', index: 'task_name',width: 40,formatter: function (c, o, d) {
                        return '<a title="' + d.task_name + '" href="javascript:;" >' + d.task_name + '</a>';
                    }
                    },
                    { name: 'task_name', index: 'task_name', hidden: true},
                    { name: 'task_type', index: 'task_type',width:20 },
                    { name: 'estimated_time', index: 'estimated_time',width: 20,formatter: function (c, o, d) {
                        if(d.estimated_time == null){
                            return d.estimated_time;
                        }else{
                            var data = d.estimated_time.split(':');
                            return data[0] + ':' + data[1];
                        }
                    }
                    },
                    { name: 'department_name', index: 'department_name',width:25 },
                    { name: 'start_date', index: 'start_date',width: 20 },
                    { name: 'end_date', index: 'end_date',width: 20 },
                    { name: 'use_case', index: 'use_case',hidden: true },
                    { name: 'status', index: 'status',width: 20,  formatter: function(c,o,d){
                        var _html = '';
                        var innerHtml = '';
                        <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ ?>
                        _html += '<a href="javascript:;" onclick="editProjectTask('+ d.id_project_task +', '+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Edit"><i class="fa fa-pencil"></i></a>';
                        _html += '<a href="javascript:;" onclick="ApproveTask('+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Approve"><i class="fa fa-check"></i></a>';
                        _html += '<a href="javascript:;" onclick="DeleteProjectTask('+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Delete"><i class="fa fa-trash-o"></i></a>';
                        <?php } ?>
                        return _html;
                    }
                    },
                    { name: 'id_project', index: 'id_project', hidden: true },
                    { name: 'module_name', index: 'module_name',hidden: true },
                    { name: 'project_module_id', index: 'project_module_id',hidden: true },
                    { name: 'id_department', index: 'id_department', hidden: true },
                    { name: 'project_id', index: 'project_id', hidden: true },
                    /*{ name: 'id_task_flow', index: 'id_task_flow',hidden: true },*/
                    { name: 'estimated_time', index: 'estimated_time',hidden: true},
                    { name: 'description', index: 'description',hidden: true},
                    /*{ name: 'original_created_date_time', index: 'original_created_date_time',hidden: true },*/
                    { name: 'completed_date', index: 'completed_date',hidden: true},
                    /*{ name: 'current_status', index: 'Action',hidden: true},*/
                    { name: 'project_task_name', index: 'project_task_name', hidden: true },
                    { name: 'start_date', index: 'start_date', hidden: true },
                    { name: 'end_date', index: 'end_date', hidden: true },
                ],
                <?php } ?>
                loadComplete: function() {
                    if(jQuery("#Project-tasks-list").getDataIDs().length<=0){
                        $('#approveButton').hide();
                    }else {
                        $('#approveButton').show();
                    }
                },
                pager: 'Project-tasks-list-page'

            }).navGrid('#Project-tasks-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true
            });
        }

        function checkAllFun(e) {
            e = e||event;/* get IE event ( not passed ) */
            e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
            var isChecked = $('#checkAll').prop('checked');
            $('.selectedTask').prop('checked', isChecked);
        }

        function editProjectTask(id, taskId) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-project-task-modal').html());
            var rowData = $ProjecttaskGrid.jqGrid('getRowData', id);

            $modal.find('.panel-title').text('Edit Task');
            $modal.find('[div-submit="true"]').text('Update');

            $(".moduleDiv").html('');

            var _html = '';
            _html += '' +
                '<div class="col-sm-12 clearfix clearboth padding0">' +
                '<div class="col-sm-6">' +
                '<div class="form-group"><div class="input_container mt10">' +
                '<input type="text" name="module_name"/>' +
                '</div>' +
                '<label class="control-label">Module Name</label>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-6">' +
                '<div class="form-group"><div class="input_container mt10">' +
                '<input type="text" name="task_type"/>' +
                '</div>' +
                '<label class="control-label"><span class="req-str">*</span>Task Type</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-12 clearfix clearboth">' +
                '<div class="form-group"><div class="input_container mt10">' +
                '<select id="id_department" name="id_department" class="rval ">' +
                '<option value="">Select</option>' +
                '</select>' +
                '</div>' +
                '<label class="control-label"><span class="req-str">*</span>Department Name</label>' +
                '</div>';

            $(".moduleDiv").append(_html);

            getJsonAsyncWithBaseUrl("Project/getProjectDepartments/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var  task = result.data.data;
                        TssLib.populateSelect($modal.find('#id_department'), { success: true, data: task }, 'department_name', 'department_id');
                        TssLib.preSelectValForSelectBox($modal.find('#id_department'),rowData.id_department);
                        TssLib.renderData($modal.find('#linen-create-Schedule-form'), rowData);
                        $('#linen-create-Schedule-form #id_department').val(rowData.id_department).trigger('chosen:updated');
                    }
                }
            });

            /*$.get("<?=site_url('Project/getDepartmentByProject')?>/"+project_id+"/" , {}, function( data ) {
             $modal.find('#getDepartmentList').html(data);
             $(".msk_time").mask("Hh:Mm");
             });*/
            $.post("<?=site_url('Project/getProjectDepartmentsEstTime')?>" , {'project_id': project_id, 'task_id': taskId}, function( data ) {
                $modal.find('#getDepartmentList').html(data);
                $(".msk_time").mask("Hh:Mm");
            });

            postJsonAsyncWithBaseUrl("Project/projectRequirement/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.data != null) {
                        TssLib.populateSelect($('#requirement_id'), {success: true, data: result.data.data}, 'requirement_name', 'id_requirements');
                        TssLib.preSelectValForSelectBox($modal.find('#requirement_id'),rowData.requirement_id);
                    }
                }
            });

            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-create-Schedule-form'), callback: function (result) {

                    var addedTest = 'Updated';
                    if (result.data.status) {
                        $ProjecttaskGrid.trigger('reloadGrid');
                        TssLib.closeModal();
                        $(".moduleDiv").html('');
                        TssLib.notify('Task '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify('Task name already exist ', 'warn', 5);
                    }
                },
                before: function (formObj) {
                    var start_date = $(formObj).find("#start_date").val(),
                        end_date = $(formObj).find("#end_date").val(),
                        department_id = $(formObj).find("#id_department").val(),
                        task_type = $(formObj).find("#task_type").val(),
                        description = encodeSpecialChar($(formObj).find("#description").val()),
                        use_case = $(formObj).find("#use_case").val(),
                        requirement_id = $(formObj).find("#requirement_id").val();
                    /*project_module_id = $(formObj).find("#project_module_id").val();*/

                    var deptt = [];
                    var collection = $(".department");
                    if(collection.length > 0){
                        collection.each(function() {
                            var This = $(this);
                            var id = This.attr('data-id');
                            var value = This.val();
                            deptt.push({'id': id, 'est': value});
                        });
                    }

                    if(deptt.length<=0){ TssLib.notify("Department can't be blank", 'warn', 5); return false; }

                    $(formObj).data('additionalData', {id_project_task:id, start_date:start_date ,end_date:end_date, department_id:department_id , department: deptt ,task_type:task_type, project_id: project_id, use_case: use_case, requirement_id: requirement_id});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/updatePendingProjectTask');

                    return true;
                }
            });
        }

        function ApproveTask(task_id)
        {
            //TssLib.confirm('Approve Confirmation', 'Are you sure to Approve?', function () {
            //$('#oasis-confirm').closest('.modal').modal('hide');
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url('index.php/project/approveProjectTask')?>',
                    data: {task_id:task_id},
                    dataType: 'json',
                    async: true,
                    success: function (result) {
                        if(result.status){
                            $('#Project-tasks-list').trigger('reloadGrid');
                        }else{
                            TssLib.notify(result.message, 'warn', 5);
                        }
                    }
                });
            //}, 'Yes', 'No');
        }

        function deleteRequirement($requirement_id)
        {
            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                $('#oasis-confirm').closest('.modal').modal('hide');
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url('index.php/Project_requirement/deleteRequirement')?>',
                    data: {requirement_id:$requirement_id},
                    dataType: 'json',
                    async: true,
                    success: function (result) {
                        if (result.status) {
                            $requirementDocGrid.trigger('reloadGrid');
                        }else{
                            TssLib.notify(result.message, 'warn', 5);
                        }
                    }
                });
            }, 'Yes', 'No');
        }

        function openTaskMemeberModal(taskId){
            var $modal = TssLib.openModel({width: 1024},$('#taskDetails').html());

            $.post("<?=site_url('Project/taskFullDetails')?>" , {'task_id':taskId }, function( data ) {
                $modal.find('#detailsContent').html(data);
            });
        }
    </Script>