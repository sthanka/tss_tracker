

    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                class="glyphicon glyphicon-remove"></i></a></div>
                    <h4 class="panel-title">Task overview</h4>
                </div>
                <div class="panel-body auto-height">
                    <div class="col-sm-12 clearfix border-bottom">
                        <div class="col-sm-2">
                            <div class="form-view">
                                <p name="Project"><?= $task_details[0]['project_name'] ?></p>
                                <label>Project</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?= $task_details[0]['task_name'] ?></div>
                                <label>Task</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <p name="Department"><?php if($task_details[0]['department_name'] == ''){ echo '-----'; }else{ echo $task_details[0]['department_name'];}; ?></p>
                                <label>Department</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?= $task_details[0]['allotted_time'] ?></div>
                                <label>Allotted Time</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?=($task_details[0]['actual_time']!='')?$task_details[0]['actual_time']:'00:00'?></div>
                                <label>Log Time</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-view">
                                <div name="Task" class="mychange-font"><?=($task_details[0]['remain_time']!='')?$task_details[0]['remain_time']:'00:00'?></div>
                                <label>Remaining Time</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix padding0">
                        <div class="">
                            <ul class="nav tsk-detail-tabs" id="tsk-detail-tabs" role="tablist">
                                <!--<h4>Add</h4>-->
                                <li role="presentation" class="active"><a href="#tskHome" id="tsk_home_li" aria-controls="home"
                                                                          role="tab" data-toggle="tab">Home</a></li>
                                <li role="presentation"><a href="#tskTimeline" aria-controls="profile" role="tab"
                                                           data-toggle="tab">Timeline</a></li>
                                <!--<li role="presentation" class="task-left-panel"><a href="#tskChecklist" onclick="loadCheckList(this, '<?/*= $task_details[0]['id_project_task']*/?>')" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Checklist</a></li>-->
                                <!--<li role="presentation"><a href="#tskUseCase" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Use Case</a></li>-->
                                <li role="presentation" class="task-left-panel"><a href="#tskAttachment" aria-controls="settings" role="tab"
                                                           data-toggle="tab">Attachment</a></li>

                                <li id='tskAskTimeBut' class="task-left-panel" role="presentation"><a href="#tskAskTime" onclick="getRequestMoreTimeList()" aria-controls="messages" role="tab" data-toggle="tab">Ask for Time</a></li>

                                <h4>Actions</h4>
<!--                                <div id="task_actions" class="nav" role="tablist"></div>-->
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tskHome" style="padding-top: 0px;">
                                    <div class="col-sm-12" style="padding-left: 0px;padding-top: 10px;">
                                        <h4 class="margin0">Task Description</h4>
                                        <p><?= $task_details[0]['description'] ?></p>
                                    </div>
                                    <div class="col-sm-12" style="padding-left: 0px;padding-top: 10px;">
                                        <h4 class="margin0">Attachments</h4>
                                        <div class="col-sm-12 pl0">
                                            <?php if(count($attachment)>1){ ?>
                                                <ul class="attachment-list-view">
                                                    <?php
                                                    foreach($attachment as $att){ ?>
                                                        <li><a href="<?= WEB_BASE_URL ?>uploads/<?= $att['attachment'] ?>" download ><?= $att['attachment'] ?></a><a href="<?= WEB_BASE_URL ?>uploads/<?= $att['attachment'] ?>" download  ><i class="fa fa-download"></i></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php }else{ ?>
                                                <ul class="attachment-list-view">
                                                    <li class="no-attachment">No Attachments</li>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="padding-left: 0px;padding-top: 10px;">
                                        <h4 class="margin0">Use case</h4>
                                        <ul class="u-list pl0" id="u-list_<?=$sub_task[0]['id_project_task']?>">
                                            <li><p><?=$task_details[0]['use_case']?></p></li>
                                        <?php /*  if(count($use_cases) > 0){
                                                    for($r=0;$r<count($use_cases);$r++){

                                            */?><!--
                                                <li>
                                                    <h5><?/*=$use_cases[$r]['user_name']*/?></h5>
                                                    <p><?/*=$use_cases[$r]['task_usecase']*/?></p>
                                                </li>
                                        <?php /*} } else { */?>
                                            <li class="no-usecase">No Use Case found</li>
                                        --><?php /*} */?>
                                        </ul>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskTimeline">
                                    <ul class="timele-history">
                                        <?php foreach($time_line as $line){ ?>

                                            <li class="timele-history-wrap">
                                                <div class="timele-history-list">
                                                    <div class="timele-history-list-date"><?=date('d-M-Y',strtotime($line['created_date_time']))?></div>
                                                    <div class="timele-history-list-content">
                                                        <div class="timele-history-list-head">
                                                            <span><?=date('h:i a',strtotime($line['created_date_time']))?></span>
                                                            <!--<span class="blue"><?/*= $line['user_name'] */?> </span>-->
                                                            <span class="blue"><?= $line['description'] ?> </span>
                                                            <!--<span>UX</span>--></div>
                                                        <!--<p><?/*=  $line['description'] */?></p>-->
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskAskTime">
                                    <div class="collapse-pane clearfix">
                                        <h4 class="margin0">Ask for More Time</h4>
                                        <a href="javascript:;" class="pull-right">+</a>
                                    </div>
                                    <div class="toggle-content clearfix">
                                    <div class="col-sm-12 pt10 pl0">
                                        <div class="col-sm-12 clearfix padding0">
                                            <p>Extra Time</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 clearfix padding0">
                                        <div class="col-sm-2 pl0">
                                            <label><span class="req-str">*</span>In Hours</label>

                                            <div class="form-group bt-default bootstrap-timepicker timepicker">
                                                <div class="bootstrap-timepicker timepicker">
                                                    <input type="text" class="form-control msk_time"  id="requested_time">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 clearfix pl0 pr0">
                                        <div class="form-group bt-default">
                                            <label><span class="req-str">*</span>Description</label>
                                            <textarea class="form-control rval" id="request_comments"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 clearfix text-center">
                                        <button class="button-common" onclick="saveMoreTime(this)">Save</button>
                                        <button class="button-color" onclick="clearMoreTime(this)">Reset</button>
                                    </div>
                                    </div>
                                    <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                        <div class="grid-details-table">
                                            <div class="grid-details-table-content">
                                                <div class="tbl_wrapper border0 ">
                                                    <table id="request-more-time-list" class="table-responsive" style="width: 800px;"></table>
                                                    <div id="request-more-time-list-page"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskTraiing"></div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskUseCase">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                        <h4 class="margin0 padding0">Use Case<a
                                                class="pull-right use-case-btn" ></a></h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content">
                                            <div class="col-sm-12 padding0">
                                                <div class="usecase-wrap">
                                                    <textarea id="user_case_text_<?=$task_details[0]['id_project_task']?>" ></textarea>
                                                    <div class="text-center padding5">
                                                        <button class="button-common" onclick="updateUseCase('','<?=$task_details[0]['id_project_task']?>','')" >Save</button>
                                                        <button class="button-color" onclick="useCaseEditorplay(this,'cancel')" >Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="u-list pl20 pl0" id="u-list_case_list_<?=$sub_task[0]['id_project_task']?>">
                                            <?php  if(count($use_cases) > 0){
                                                        foreach($use_cases as $k=>$v){

                                                ?>
                                                    <li>
                                                        <h5><?=$v['user_name']?></h5>
                                                        <p><?=$v['task_usecase']?></p>
                                                    </li>
                                            <?php } } else { ?>
                                                <li class="no-usecase">No Use Case found</li>
                                            <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskAttachment">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Attachments</h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content clearfix">
                                        <div class="col-sm-12 pb20 pl0">
                                            <div class="col-sm-12 padding0 attachemnt-parent-wrap" >
                                                <div class="attachemnt-wrap" >
                                                    <?php
                                                    $attachment_task_id = array();
                                                    array_push($attachment_task_id, $task_details[0]['id_project_task']);
                                                    for ($s = 0; $s < count($attachment_task_id); $s++) { ?>
                                                        <div class="tab-pane active" id="task_attachment_<?= $attachment_task_id[$s] ?>">
                                                            <form
                                                                class="NewErrorStyle form-horizontal NewErrorStyle animate-fld-bg align-error clearfix"
                                                                method="post" id="task_attachment_frm_<?= $attachment_task_id[$s] ?>"
                                                                action="<?= WEB_BASE_URL ?>index.php/Project/uploadTaskAttachment">
                                                                <div class="col-sm-6 pl0">
                                                                    <div class="fileupload fileupload-new input-group"
                                                                         data-provides="fileupload">
                                                                        <div class="form-control" data-trigger="fileupload"><i
                                                                                class="glyphicon glyphicon-file fileupload-exists"></i>
                                                                            <span class="fileupload-preview"></span></div>
                                                                            <span class="input-group-addon btn btn-default btn-file"><span
                                                                                    class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
                                                                                 <input type="file" name="task_attachment" id="task_attachment"
                                                                                        class="form-control rval">
                                                                            </span>
                                                                        <a href="#"
                                                                           class="input-group-addon btn btn-default fileupload-exists"
                                                                           data-dismiss="fileupload" id="remove_attachment_<?= $attachment_task_id[$s] ?>">Remove</a>
                                                                    </div>

                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="submit" class="button-common" name="btn" value="Upload">
                                                                </div>

                                                                <input type="hidden" name="project_task_id"
                                                                       value="<?= $attachment_task_id[$s] ?>">
                                                                <input type="hidden" name="user_id"
                                                                       value="<?= $this->session->userdata('user_id') ?>">
                                                            </form>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-sm-12 pb20 pl0">
                                            <?php if(count($attachment)>1){ ?>
                                                <ul class="attachment-list-view">
                                                    <?php
                                                    foreach($user_attachment as $att){ ?>
                                                        <li><a href="<?= WEB_BASE_URL ?>uploads/<?= $att['attachment'] ?>" download ><?= $att['attachment'] ?></a><a href="<?= WEB_BASE_URL ?>uploads/<?= $att['attachment'] ?>" download  ><i class="fa fa-download"></i></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php }else{ ?>
                                                <ul class="attachment-list-view">
                                                    <li class="no-attachment">No Attachments</li>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskReviews">..555555.</div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskChecklist">
                                    <h4 class="margin0">Checklist</h4>
                                    <div class="col-sm-12 padding0 clearfix">
                                        <div class="col-sm-12 padding0 pl0 tsk-chklist-wrapper">

                                        </div>
                                    </div>
                                    <!-- old code , checklist for all user in department whom task is alloted --<
                                    <!--<div class="col-sm-12 padding0">
                                        <?php /*if(count($checkList)>0){ */?>
                                            <h4 class="margin0">Previous Department Checklist</h4>
                                            <div class="col-sm-12 padding0 pl0 tsk-users-chklist-wrapper">
                                                <ul>
                                                    <?php /*foreach($checkList as $k=>$v){ */?>
                                                        <li><?/*= $v['checklist_name'] */?> -
                                                            <?/*= $v['checklist_value']=='2'?'Verified':$v['checklist_value']=='1'?'Not Verified':'Not Applicable' */?>
                                                            <span>By : </span><?/*= $v['first_name'].' '.$v['last_name'] */?></li>
                                                    <?php /*} */?>
                                                </ul>
                                            </div>
                                        <?php /*} */?>
                                    </div>-->
                                </div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskTraiing">.9999..</div>
                                <!--end of tab pane-->
                                <div role="tabpanel" class="tab-pane" id="tskLogTime">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Log Time</h4>
                                            <a href="javascript:;" class="pull-right">+</a>
                                        </div>
                                        <div class="toggle-content clearfix">
                                            <form id="manual_task_log_time" class="form-horizontal align-error" method="POST" >
                                                <div class="col-sm-12 pt10">
                                                    <div class="col-sm-12 pl0 clearfix">
                                                        <!--<div class="col-sm-6 padding10 pl0">-->
                                                            <div class="form-group bt-default">
                                                                <label>Task Type</label>
                                                                <select id="task_type" name="task_type" class="rval">
                                                                    <option>--select--</option>
                                                                </select>
                                                            </div>
                                                        <!--</div>
                                                        <div class="col-sm-6" id="taskTypeDropdownText"></div>-->
                                                    </div>
                                                    <div class="col-sm-12 pl0">
                                                        <div class="form-group bt-default">
                                                            <label>Description</label>
                                                            <textarea name="comments" id="comments" placeholder="Enter Comments" class="form-control rval" ></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 pl0">
                                                        <div class="form-group bt-default">
                                                            <label>Date</label>
                                                            <input class="form-control tssDatepicker rval" type="text" placeholder="dd/mm/yyyy" enddate="0" id="task_date" name="task_date">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 clearfix mt10 pl0">
                                                        <div class="col-sm-2 pl0">
                                                            <div class="form-group bt-default">
                                                                <div class="bootstrap-timepicker timepicker">
                                                                    <input id="start_time" name="start_time" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-1 text-center">
                                                            to
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group bt-default">
                                                                <div class="bootstrap-timepicker timepicker">
                                                                    <input id="end_time" name="end_time" type="text"  type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 clearfix padding20 pl0">
                                                        <input class="button-common" type="submit"  value="Save" >
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="grid-details-table">
                                                <div class="grid-details-table-content">
                                                    <div class="tbl_wrapper border0 ">
                                                        <table id="time-logs-info-table" class="table-responsive" style="width: 800px;"></table>
                                                        <div id="time-logs-info-table-page"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tskCmptTime">
                                    <div class="col-sm-12 pb20">
                                        <div class="collapse-pane clearfix">
                                            <h4 class="margin0">Complete Task</h4>
                                        </div>

                                        <div id="completed_task_div_view"  class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="col-sm-12 clearfix border-bottom">
                                                <div class="col-sm-12 clearfix pl0 pr0">
                                                    <div class="col-sm-2">
                                                        <div class="form-view">
                                                            <div name="Task" id="all_time" class="mychange-font"></div>
                                                            <label>Allotted Time</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-view">
                                                            <div name="Task" id="act_time" class="mychange-font"></div>
                                                            <label>Log Time</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-view">
                                                            <div name="Task" id="remain_time" class="mychange-font"></div>
                                                            <label>Remaining Time</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 clearfix pl0 pr0">
                                                    <div class="form-group bt-default">
                                                        <label><span class="req-str">*</span>Comments:</label>
                                                        <textarea class="form-control rval" id="userComment"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 clearfix">
                                                <p id="task_completed_node" style="color: red"></p>
                                            </div>
                                            <div class="col-sm-12 padding0 pl0 tsk-chklist-wrapper-cmt">

                                            </div>
                                            <div class="col-sm-12 clearfix" style="padding-top: 15px;">
                                                <a href='javascript:;' id="taskComplete" style="margin-left: 20px;" class='button-common module btn-danger' onclick='changeTaskStatus("<?=$task_details[0]['id_task_flow']?>","completed")'>Complete</a>
                                            </div>
                                        </div>
                                        <!--<div class="col-sm-12 clearfix clearboth pad-right-none pt20 pl0">
                                            <div class="grid-details-table">
                                                <div class="grid-details-table-content">
                                                    <div class="tbl_wrapper border0 ">
                                                        <table id="time-logs-info-table" class="table-responsive" style="width: 800px;"></table>
                                                        <div id="time-logs-info-table-page"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                                </div>
                                <!--end of tab pane-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var $refTblLoggrid = $('#time-logs-info-table');
        var $refTblgrid = $('#request-more-time-list');
        var logTbltype = 0;
        var getMoretimetype = 0;
        function logActionsBuild(id_task_workflow,c){

            var d = {};
            d.id_task_flow =  id_task_workflow;
            //$('#tsk-detail-tabs').html('');
            $('#tsk-detail-tabs > li.button-action-class,#tsk-detail-tabs .task-completed').remove();
            var _html = '';
            if(c.toLowerCase() == 'new'){
                _html += '<li class="button-action-class"><a href="javascript:;"   class="tskAccept-bg" onclick="changeTaskStatus(' + d.id_task_flow + ',\'progress\')">Start</a></li>'
                        + '<li class="button-action-class"><a href="javascript:;" class="tskRelease-bg" onclick="changeTaskStatus('+d.id_task_flow+',\'release\')">Release</a></li>';
            }
            if(c.toLowerCase() == 'progress'){
                _html += "<li class='button-action-class'><a href='#tskCmptTime' aria-controls='settings' role='tab' data-toggle='tab'  class='tskComplete-bg' onclick='getTaskCompleteContent("+d.id_task_flow+");loadCheckList(this, \"<?=$task_details[0]['id_project_task']?>\");'>Completed</a></li>"
                    +"<li class='button-action-class'><a href='javascript:;' class='tskAskHold-bg' onclick='changeTaskStatus("+d.id_task_flow+",\"hold\")'>Hold</a></li>"
                    +"<li class='button-action-class'><a href='javascript:;'  class='tskRelease-bg' onclick='changeTaskStatus("+d.id_task_flow+",\"release\")'>Release</a></li>"
                    +'<li class="button-action-class" role="presentation"><a onclick="getLogTimeList()" href="#tskLogTime" class="tskLogTime-bg" aria-controls="settings" role="tab" data-toggle="tab" >Log Time</a></li>';

            }
            if(c.toLowerCase() == 'approval_waiting'){

                $('#tskAskTimeBut').remove();
                $('#tskAskTime').remove();
                $('.task-left-panel').css('display','none');
                _html += '<span class="pull-left task-status task-completed">Waiting for approval</span>';
            }
            if(c.toLowerCase() == 'completed'){
                $('.task-left-panel').css('display','none');
                $('#tskAskTimeBut').remove();
                $('#tskAskTime').remove();
                _html += '<span class="pull-left task-status task-completed">Completed</span>';
            }
            if(c.toLowerCase() == 'release'){
                $('#tskAskTimeBut').remove();
                $('#tskAskTime').remove();
            }
            if(c.toLowerCase() == 'hold'){
               _html += '<li class="button-action-class"><a class="tskStart-bg" href="javascript:;" onclick="changeTaskStatus('+d.id_task_flow+',\'progress\')">Start</a></li>'

                    +'<li class="button-action-class"><a href="javascript:;"  class="tskRelease-bg" onclick="changeTaskStatus('+d.id_task_flow+',\'release\')">Release</a></li>'

            }
            $('#tsk-detail-tabs').append(_html);
        }
        logActionsBuild('<?=$task_details[0]['id_task_flow']?>','<?=$task_details[0]['task_status']?>');
        getJsonAsyncWithBaseUrl("Project/getTaskTypes", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var  projects = result.data.data;
                    TssLib.populateSelect($('#task_type'), { success: true, data: projects }, 'label', 'id_task_type');
                    /*$('#task_type').on('change', function(){
                        var selected = $(this).val(),
                            context = '';
                        if(selected!=''){
                            jQuery.each(taskTypeDropdown, function(index, item) {
                                if(selected == item.id_task_type){
                                    var isBillable = item.product_type=='billable'?'Is Productive':'Is not Productive';
                                    var product_type = item.is_billable=='billable'?'Is Billable':'Is not Billable';
                                    context = isBillable+' and '+product_type;
                                }
                            });
                            $('#taskTypeDropdownText').html(context);
                        }else { $('#taskTypeDropdownText').html(''); }
                    });*/
                }
            }
        });
        function changeTaskStatus(id_task_flow,status) {
            if(status=='progress'){
                changeTaskStatusConfirm(id_task_flow,status);
            }else{
                var txtstatus = status=='completed'?'complete':status;
                //TssLib.confirm('Task Status Confirmation', 'Are you sure to '+txtstatus+' this task?', function () {
                    $(this).closest('.modal').modal('hide');
                    changeTaskStatusConfirm(id_task_flow,status);
                //}, 'Yes', 'No');
            }
        }
        function changeTaskStatusConfirm(id_task_flow,status){
            var isValid = true;
            var jsondata = {};
            var userComment = $('#userComment').val();
            var sThisVal = [];
            $('.checkListClass').each(function () {
                if(this.checked){
                    sThisVal.push(this.checked ? {'id':$(this).attr('date-checklist-id'),'value':this.value} : "");
                }
            });
            if(userComment == '' && status=='completed'){
                TssLib.notify('Comments cannot be left blank.', 'warn', 5);
                isValid = false;
                return false;
            }
            if(sThisVal.length<=0 && status=='completed'){
                TssLib.notify('Checklist required.', 'warn', 5);
                isValid = false;
                return false;
            }
            if($('.checkListClass').length/2 != sThisVal.length){
                TssLib.notify('All checklist should be selected.', 'warn', 5);
                isValid = false;
                return false;
            }
            if(isValid){
                if(status=='completed'){
                    jsondata = {id_task_flow: id_task_flow, status: status, task_comment:userComment, 'data':sThisVal};
                }else{
                    jsondata = {id_task_flow: id_task_flow, status: status};
                }
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: WEB_BASE_URL + 'index.php/Project/changeTaskStatus',
                    dataType: 'json',
                    data: jsondata,
                    success: function (res) {
                        if (res.status) {
                            $taskGrid.trigger('reloadGrid');
                            callLoadEvents();
                            if(status=='completed'){
                                status = 'approval_waiting';
                                $('#taskComplete').hide();
                                $taskGrid.trigger('reloadGrid');
                                $('#tasks-list-page').trigger('reloadGrid');
                                $('#tsk_home_li').click();
                            }
                            logActionsBuild(id_task_flow,status);
                            TssLib.notify(res.data);
                        }
                        else {
                            TssLib.notify(res.data, 'warn', 5);
                        }
                    }
                });
            }
        }
        $(document).ready(function () {
            TssLib.datepickerBinder('#manual_task_log_time');
           //mask
            $(".msk_time").mask("Hh:Mm");
            $("#user_case_text_<?= $task_details[0]['id_project_task']?>").Editor();
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#manual_task_log_time'), callback: function (result) {
                    if (result.data.status) {
                        oldDate = formatAMPM(new Date());
                        $('#linen-createSchedule-form #start_time,#linen-createSchedule-form #end_time').timepicker('setTime',oldDate );
                        TssLib.notify('Time logged successfully ', null, 5);
                        $('#TIMER').hide().addClass('minimized');
                        localStorage.removeItem( 'tss_tracker');
                        $('#taskTimer').timer('remove');
                        if (typeof callLoadEvents == 'function') {
                            callLoadEvents();
                        }
                        $('#manual_task_log_time').find("#task_type").val("");
                        $('#comments').val("");
                        $('#task_date').val("");
                        $("#gbox_time-logs-info-table").parent().parent().parent().show();
                        var $refTblLoggrid = $('#time-logs-info-table');
                        $refTblLoggrid.trigger('reloadGrid');
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    if(validateTime($('#start_time').val(),$('#end_time').val()) <= 0 ){ TssLib.notify('start time should be less than end time', 'warn', 5); return false; }
                    $(formObj).data('additionalData', { 'duration':$('#taskTimer').data('seconds'), 'created_date_time': $('#task_date').val(),'task_flow_id':'<?=$task_details[0]['id_task_flow']?>','project_task_id':'<?=$task_details[0]['id_project_task']?>','project_id':'<?=$task_details[0]['id_project']?>','user_id':'<?=$this->session->userdata('user_id')?>'});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/logTime');
                    return true;
                }
            });
        });

        function clearMoreTime(){
            $('#requested_time').timer('remove');
            $('#request_comments').val("");
        }

        function useCaseEditorplay(e,mode) {
            $("#user_case_text_<?= $task_details[0]['id_project_task']?>").Editor("setText","");
        }

        <?php for($s = 0;$s < count($attachment_task_id);$s++){ ?>
        jQuery('#task_attachment_frm_<?=$attachment_task_id[$s]?>').ajaxForm(function (data) {
            var res = JSON.parse(data);
            if (res.status) {
                TssLib.notify('File Uploaded successfully ', null, 5);
                var html = '<li>' +
                    '<a href="' + res.data.attachment + '">' + res.data.attachment_name + '</a>' +
                    '<a href="' + res.data.attachment + '"><i class="fa fa-download"></i></a></li>';
                $('.attachment-list-view').prepend(html);
//                $('.attachment-tab-list-view').prepend(html);
                $('.no-attachment').remove();
                $('#remove_attachment_<?=$attachment_task_id[$s]?>').trigger("click");
            }
            else {
                TssLib.notify(res.data, 'warn', 5);
            }
        });
        <?php } ?>

        function loadCheckList(This, taskId){
            $.get("<?=site_url('Project/loadCheckList/')?>/"+taskId, function (data) {
                //$('.tsk-chklist-wrapper').html(data);
                $('.tsk-chklist-wrapper-cmt').html(data);
            });
        }

        function getLogTimeList(){

            var task_id = '<?= $task_details[0]['id_project_task']?>';
            if(logTbltype == 0){
                $refTblLoggrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/getLogTimeList?task_id='+task_id,
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    extSearchField: '.searchInput',
                    colNames: ['Id', 'project_task_id', 'Task Type', 'Description', 'Start Time', 'End Time', 'Total time','Date'],
                    colModel: [
                        { name: 'log_time_id', index: 'log_time_id', hidden: true ,key:true},
                        { name: 'project_task_id', index: 'project_task_id',  hidden: true},
                        { name: 'task_type_name', index: 'task_type_name' },
                        { name: 'comments', index: 'comments' },
                        { name: 'start_time', index: 'start_time' },
                        { name: 'end_time', index: 'end_time' },
                        { name: 'duration', index: 'duration' },
                        { name: 'created_date_time', index: 'created_date_time' },
                    ],
                    loadComplete: function () {
                        if(jQuery("#time-logs-info-table").getDataIDs().length==0){
                            $("#gbox_time-logs-info-table").parent().parent().parent().hide();
                        }else{
                            $(".grid-details-table").show();
                        }
                    },
                    pager: 'time-logs-info-table-page'
                }).navGrid('#time-logs-info-table-page', {
                    edit: false, add: false, del: true, search: false, refresh: true,
                    delfunc: function (id) {
                        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                            $(this).closest('.modal').modal('hide');
                            deleteLogTime(id);
                        }, 'Yes', 'No');
                    }
                });
                logTbltype++;
            }else{
                $refTblLoggrid.trigger('reloadGrid');
            }
        }

        function saveMoreTime(This){
            var time = $('#requested_time').val();
            var comment = $('#request_comments').val();
            var taskId = <?= $task_details[0]['id_project_task']?>;
            var taskWorkFlowId = <?=$task_details[0]['id_task_flow']?>;
            if(comment == '' || time==''){
                TssLib.notify('Fill all the fields', 'warn', 5);
                return;
            }
            $.ajax({
                async: false,
                type: 'POST',
                url: WEB_BASE_URL + 'index.php/Project/saveRequestMoreTime',
                dataType: 'json',
                data: {project_task_id: taskId, task_flow_id: taskWorkFlowId, duration: time, user_comments: comment},
                success: function (res) {
                    if (res.status) {
                        var $refTblgrid = $('#request-more-time-list');
//                        $(".grid-details-table").show();
                        $("#gbox_request-more-time-list").parent().parent().parent().show();
                        getRequestMoreTimeList();
                        $refTblgrid.trigger('reloadGrid');
                        clearMoreTime();
                        TssLib.notify('Saved successfully');
                    }
                    else {
                        TssLib.notify(res.data, 'warn', 5);
                    }
                }
            });
        }

        function getRequestMoreTimeList(){
            if(getMoretimetype == 0){
//                var $refTblgrid = $('#request-more-time-list');
                $refTblgrid.jqGrid({
                    url: TssConfig.TT_SERVICE_URL + 'Project/getRequestMoreTimeGrid?task=<?= $task_details[0]['id_project_task']?>&workflow=<?=$task_details[0]['id_task_flow']?>',
                    multiselect: false,
                    datatype: "json",
                    sortorder: "desc",
                    extSearchField: '.searchInput',
                    colNames: ['Id', 'Duration', 'User Comment', 'Approval By','Admin Comment', 'Status','created_date_time'],
                    colModel: [
                        { name: 'Id_request_more_time', index: 'Id_request_more_time', hidden: true ,key:true},
                        { name: 'duration', index: 'duration' },
                        { name: 'user_comments', index: 'user_comments' },
                        { name: 'assigned_to_name', index: 'assigned_to_name' },
                        { name: 'admin_comments', index: 'admin_comments' },
                        { name: 'status', index: 'status' },
                        { name: 'created_date_time', index: 'created_date_time', hidden: true },
                    ],
                    loadComplete: function () {
                        if(jQuery("#request-more-time-list").getDataIDs().length==0){
                            $("#gbox_request-more-time-list").parent().parent().parent().hide();
                        }else{
                            $(".grid-details-table").show();
                        }
                    },
                    pager: 'request-more-time-list-page'
                }).navGrid('#request-more-time-list', {
                    edit: false, add: false, del: false, search: false, refresh: true
                });
                getMoretimetype++;
            }else{
//                var $refTblgrid = $('#request-more-time-list');
                $refTblgrid.trigger('reloadGrid');
            }
        }

        $(".collapse-pane").click(function(){
            $(this).next(".toggle-content").fadeToggle(300);
        });

        function getTaskCompleteContent(id_task_flow)
        {
            $.ajax({
                async: false,
                type: 'POST',
                url: WEB_BASE_URL + 'index.php/Project/getTaskTimeDetails',
                dataType: 'json',
                data: {id_task_flow: id_task_flow},
                success: function (res) {
                    if (res.status) {
                        var data = res.data;
                        $('#all_time').text(data.allowted_time);
                        if(data.actual_time==''){
                            data.actual_time = '00:00';
                        }
                        $('#act_time').text(data.actual_time);
                        if(!data.remain_time || data.remain_time==''){
                            data.remain_time = '00:00';
                        }
                        $('#remain_time').text(data.remain_time);
                        if(!data.remain_seconds){
                            data.remain_seconds = 0;
                        }

                        if(data.remain_seconds>1800){
                            $('#task_completed_node').text('* Your logtime is less than to your allotted time ');
                        }
                    }
                }
            });
        }

    </script>
