<?php if ($this->session->userdata('user_id')) { ?>
    <div class="modal fade" id="oak_popup">
        <div class="modal-dialog model-mg-width">
            <div class="modal-content model-mg-content"></div>
        </div>
    </div>
    <!-- Modal Dialog  dekete confirmation-->
    <div class="modal fade" id="oasis-confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </div>
                        <h4 class="panel-title"></h4>
                    </div>
                    <div class="panel panel-default panel-body padding20 clearfix">
                        <p></p>
                    </div>
                    <div class="panel-footer col-sm-12 clearfix text-right">
                        <button type="button" class="button-common module btn-danger">Yes</button>
                        <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div >
    <div class="dark-modal pin" id="TIMER" style="display:none">

        <h3 class="dm-head show-min">
            <span class="tracker_task_name"></span>
            <a class="pull-right mr10" onclick="toggleScreen(this)"><i class="fa fa-thumb-tack"></i></a>
        </h3>
        <p class="dm-description tracker_description"></p>
        <p class="dm-description tracker_task"></p>
        <div class="timer-area show-min">
            <span>Task Time</span>
            <b id="taskTimer"></b>
            <a href="javascript:;" class="pause" onclick="playPause(this, true)">
                <i class="fa fa fa-pause"></i>
                <i class="fa fa fa-play"></i>
            </a>
        </div>
        <div class="row-5">
            <input type="button"  onclick="moreTimeRequest(this)" class="green-btn-full"  value="LOG TIME">
        </div>
    </div>
    <form id="task_log_time" class="form-horizontal align-error" method="POST" >
            <div class="dark-modal pin" id="MORE_TIME" style="display:none">
            <h3 class="dm-head show-min">
                <span class="tracker_task_name"></span>
                <a class="pull-right mr10" onclick="toggleScreen(this)"><i class="fa fa-thumb-tack"></i></a>
            </h3>
            <p class="dm-description tracker_description"></p>
            <p class="dm-description tracker_task"></p>


            <div class="clearfix clearboth">
                <div >
                    <div class="form-group">
                        <label class="control-label"><span class="req-str">*</span>Task Type :</label>
                        <div>
                            <select id="timer_task_type" name="task_type" class="form-control mb5 rval no-sel2" /></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix clearboth">
                <div >
                    <div class="form-group">
                        <label class="control-label"><span class="req-str">*</span>Comments :</label>
                        <div>
                            <textarea name="comments" placeholder="Enter Comments" rows="2" class="col-sm-12 clearfix clearboth rval"></textarea>
                        </div>
                    </div>
                </div>
             </div>

            <div class="input_container bootstrap-timepicker mt10 col-sm-6 pad-left pr5" >
                    <div class="start_time">
                        <input id="timer_start_time" name="start_time" type="text" class="form-control plr5">
                    </div>
                    <label>Start Time:</label>
            </div>
            <div class="input_container bootstrap-timepicker mt10 col-sm-6 pad-right" >
                    <div class="end_time">
                        <input id="timer_end_time" name="end_time" type="text" class="form-control plr5">
                    </div>
                    <label>End Time:</label>
            </div>

            <div class="input_container bootstrap-timepicker border0 col-sm-12 pad-right" >
                <label class="col-sm-6 pad-left pl0">Duration(DD:HH:MM):</label>
                <p class="time-duration col-sm-6 mb0 pad-right pl0">----</p>
            </div>
            <div class="btn-wrap">
                <input class="green-bg half-btn" type="submit"  value="Save" >
                <input class="red-bg half-btn" type="button"  onclick="cancelSendRequest(this)" value="Cancel" >
            </div>
        </div>
    </form>
</div>

<style>
.dark-modal select.no-sel2.form-control {    padding: 2px 4px !important;background:#E8E8E8!important}
.dark-modal textarea{padding: 2px 4px !important;background:#E8E8E8!important;    color: #495b79;font-size: 12px;font-weight: bold;}
</style>
<script src="<?= WEB_BASE_URL ?>scripts/modernizr.min.js"></script>
<script src="<?= WEB_BASE_URL ?>scripts/jquery.cookies.js"></script>
<script src="<?= WEB_BASE_URL ?>scripts/custom.js"></script>
<script>

// check timere open
    var tss_tracker =  {};
    var storageDetails = '';
    if(localStorage.getItem('tss_tracker')){
    var preDetails = JSON.parse(localStorage.getItem('tss_tracker'));
    if(preDetails.logType != 'add'){
            logTime();
        }else{
            localStorage.removeItem( 'tss_tracker');
        }
    }
    function toggleScreen(e){
        $(e).closest('.dark-modal').toggleClass('pin');
    }

    function logTime(e){
        var  logType = '';
        var rowData = {'task_type':'','comments':''};
        TssLib.renderData($('#task_log_time'), rowData);
        if(e == undefined){
            $('#taskTimer').timer('remove');
            storageDetails = JSON.parse(localStorage.getItem('tss_tracker'));
            var sda = new Date(storageDetails.startDate);
            var ada = new Date();
            $('#taskTimer').timer({seconds: Math.round(new Date(ada - sda)/1000),format: '%H:%M:%S',duration: '5s',repeat: true,callback: function(){ localDataSave() } });
        }else{
            if(localStorage.getItem('tss_tracker') != null){ TssLib.notify('Timer processing on other task ', null, 5); return false; }
            $('#taskTimer').timer('remove');
            var taskDetails = JSON.parse($(e).attr('task-object'));
            logType = $(e).attr('data-type');
            tss_tracker.logType = logType;
            tss_tracker = $.extend( {}, tss_tracker, taskDetails );
            tss_tracker.startDate = new Date();
            localStorage.setItem( 'tss_tracker', JSON.stringify(tss_tracker));
            $('#taskTimer').timer({seconds: 0,format: '%H:%M:%S',duration: '5s',repeat: true, callback: function(){ localDataSave() } });
        }
        storageDetails = JSON.parse(localStorage.getItem('tss_tracker'));
        $('.dark-modal').find('.tracker_task_name').html(storageDetails.project_name);
        $('.dark-modal').find('.tracker_description').html(storageDetails.description);
        $('.dark-modal').find('.tracker_task').html(storageDetails.task_name);
        $(e).closest('.grid-actions').hide();
        $(e).prev().removeClass('in');
        if(logType == 'add'){ moreTimeRequest(); }else{ $('#TIMER').show().removeClass('minimized'); }
    }
    function playPause(e){
        if(!$(e).hasClass('play')){
            $(e).addClass('play').removeClass('pause');
            $('#taskTimer').timer('pause')
        } else{
            $(e).addClass('pause').removeClass('play');
            $('#taskTimer').timer('resume');
        }
    }
    function localDataSave(){
       localDetails = JSON.parse(localStorage.getItem('tss_tracker'));
        var preDate = new Date(localDetails.startDate);
        var curDate = new Date();
        var timerSeconds = $('#taskTimer').data('seconds');
        var val = Math.round(curDate.getTime()/1000);
        val = (val-timerSeconds);
        localDetails.startDate = new Date(val*1000);
        localStorage.setItem( 'tss_tracker', JSON.stringify(localDetails));

   }
    function moreTimeRequest(e){
        //$('#timer_start_time,#timer_end_time').timepicker('remove');
        $('#timer_start_time,#timer_end_time').timepicker({ minuteStep: 1, defaultTime: formatAMPM(new Date()) });
        $('#MORE_TIME').slideDown();
        $('#TIMER').slideUp();
        var oldDate = '';
         if(e != undefined){
            storageDetails = JSON.parse(localStorage.getItem('tss_tracker'));
            oldDate = formatAMPM(new Date(storageDetails.startDate));
            //console.log(oldDate);
            $('#timer_start_time').timepicker('setTime',oldDate );
            $('#timer_end_time').timepicker('setTime',formatAMPM(new Date()));
         }else{
            $('#timer_start_time').timepicker('setTime',formatAMPM(new Date()));
            $('#timer_end_time').timepicker('setTime',formatAMPM(new Date()));
         }
         buildDuration(validateTime($('#timer_start_time').val(),$('#timer_end_time').val()));
         $('#timer_start_time').timepicker().on('changeTime.timepicker', function(e) {
            var midTime = validateTime(e.time.value,$('#timer_end_time').val());
            buildDuration(midTime);
         });
         $('#timer_end_time').timepicker().on('changeTime.timepicker', function(e) {
            var midTime = validateTime($('#timer_start_time').val(),e.time.value);
            buildDuration(midTime);
         });
         getJsonAsyncWithBaseUrl("Project/getTaskTypes", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var  projects = result.data.data;
                   TssLib.populateSelect($('#timer_task_type'), { success: true, data: projects }, 'name', 'id_task_type');
                }
            }
        });
    }
    function buildDuration(duration){
        duration = duration/1000;
        $('.time-duration').html(sformat(duration));
    }
    function sformat( s ) {
      var fm = [
                        Math.floor(Math.floor(s/60)/60)%60,                      //HOURS
                        Math.floor(s/60)%60,                                    //MINUTES
                        s%60                                                   //SECONDS
                  ];
      return $.map(fm,function(v,i) { return ( (v < 10) ? '0' : '' ) + v; }).join( ':' );
    }
    function formatAMPM(date) {
          var hours = date.getHours();
          var minutes = date.getMinutes();
          var ampm = hours >= 12 ? 'PM' : 'AM';
          hours = hours % 12;
          hours = hours ? hours : 12; // the hour '0' should be '12'
          minutes = minutes < 10 ? '0'+minutes : minutes;
          var strTime = hours + ':' + minutes + ' ' + ampm;
          return strTime;
    }
    function cancelSendRequest(e){
        TssLib.confirm('Delete Confirmation', 'Are you sure to cancel ?', function () {
            $(this).closest('.modal').modal('hide');
           /*$('#TIMER').slideDown();
            $('#MORE_TIME').slideUp();*/
            $('#TIMER').hide().addClass('minimized');
            $('#MORE_TIME').hide().addClass('minimized').find('.has-error').removeClass('has-error');
            $('#taskTimer').timer('remove');
            localStorage.removeItem( 'tss_tracker');
            TssLib.notify('Cancel successfully ', null, 5);
            //location.reload('#task');
            /*$('#timer_start_time').timepicker('remove');
            $('#timer_end_time').timepicker('remove');*/
        }, 'Yes', 'No');
    }
    function validateTime(sTime,eTime){
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            var sDate = yyyy+'/'+mm+'/'+dd+' '+sTime;
            var eDate = yyyy+'/'+mm+'/'+dd+' '+eTime;
            sDate = Date.parse(sDate);
            eDate = Date.parse(eDate);
            return eDate-sDate;
        }
    $(document).ready(function () {
            $.mask.definitions['H'] = "[0-9]";
            $.mask.definitions['h'] = "[0-9]";
            $.mask.definitions['M'] = "[0-5]";
            $.mask.definitions['m'] = "[0-9]";
            $.mask.definitions['P'] = "[AaPp]";
            $.mask.definitions['p'] = "[Mm]";
        TssLib.ajaxForm({
            jsonContent: true,
            form: $('#task_log_time'), callback: function (result) {
                if (result.data.status) {
                    TssLib.notify('Time logged successfully ', null, 5);
                    $('#TIMER').hide().addClass('minimized');
                    $('#MORE_TIME').hide().addClass('minimized');
                    localStorage.removeItem( 'tss_tracker');
                    $('#taskTimer').timer('remove');
                    //location.reload('#task');
                    if (typeof callLoadEvents == 'function') {
                      callLoadEvents();
                    }
                }else{
                    TssLib.notify(result.data.message, 'warn', 5);
                }
            },
            before: function (formObj) {
                var timeValidate = validateTime($('#timer_start_time').val(),$('#timer_end_time').val());
//                console.log('timeValidate',timeValidate);
                if(timeValidate <= 0 ){ TssLib.notify('start time should be less than end time', 'warn', 5); return false; }
                $(formObj).data('additionalData', { 'duration':$('#taskTimer').data('seconds'),'task_flow_id':storageDetails.id_task_flow,'project_task_id':storageDetails.id_project_task,'project_id':storageDetails.id_project,'user_id':storageDetails.assigned_to});
                $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/logTime');
                return true;
            }
        });
        $('.dark-modal').mouseenter(function(){
            $(this).removeClass('minimized');
        }).mouseleave(function(){
            $(this).addClass('minimized');
        })

        //For hiding schoolpost
        var z = ('@HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString()');
        var aa = window.location.href.split('/');
        var x = null;
        $.each(aa, function (i, o) {
            if (o == z)
                x = aa[i + 1];
        });
        if (x != null && x.toLowerCase() == "index") {
            $('#header').find('.school-post').hide();
        }
        $('.schoolpost-logo').attr('href', TssConfig.SITE_URL + "module");
    });
</script>
    <script src="<?= WEB_BASE_URL ?>scripts/jquery.jqGrid.src.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/i18n/grid.locale-en.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/jquery.jsonSuggest-2.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= WEB_BASE_URL ?>scripts/app.js"></script>
    <?php if ($this->session->userdata('message')) { ?>
    <script>
        $(document).ready(function () {
            TssLib.notify('<?=$this->session->userdata('message')?>', null, 5);
        });
    </script>
    <?php $this->session->unset_userdata('message');
} ?>
<script>
    var _menusUl = $('#left-panel-main-menus-ul'), _searchLi, _menuSearchBox, _inputSearch;
    <?php if($this->session->userdata('user_type_id')==2 || $this->session->userdata('is_manager')==1){ ?>
    //var jsonString = '[{"CampusCode":null,"Id":5244,"Name":"Admin Users","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Admin Users","FunctionalName":null,"Id":7762,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/AdminUsers"}]},{"CampusCode":null,"Id":5253,"Name":"Audit Logs","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Audit Logs","FunctionalName":null,"Id":8856,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/AuditLogs"}]},{"CampusCode":null,"Id":36,"Name":"Dashboard","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Dashboard","FunctionalName":null,"Id":57,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/Index"}]},{"CampusCode":null,"Id":1148,"Name":"Employee Management","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Employee Management","FunctionalName":null,"Id":336,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/EmployeeRoles"}]},{"CampusCode":null,"Id":1150,"Name":"Module","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Module","FunctionalName":null,"Id":346,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/Module"}]},{"CampusCode":null,"Id":1151,"Name":"Non Employee Roles","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Non Employee Roles","FunctionalName":null,"Id":348,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/NonEmployeeRoles"}]},{"CampusCode":null,"Id":4220,"Name":"Push Report Config","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"CRM MD Report Config","FunctionalName":null,"Id":9022,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/CRMMDReportConfig"},{"DisplayInMenu":true,"DisplayName":"Mail Alert Config","FunctionalName":null,"Id":7726,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/MailAlertConfig"},{"DisplayInMenu":true,"DisplayName":"Push Email Config","FunctionalName":null,"Id":5596,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/PushReportEmailConfig"}]},{"CampusCode":null,"Id":1149,"Name":"Roles And Permissions","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Roles And Permissions","FunctionalName":null,"Id":337,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/RolesAndPermissions"}]},{"CampusCode":null,"Id":5222,"Name":"User Login","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":false,"DisplayName":"User Login","FunctionalName":null,"Id":6608,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/BlockedUsers"}]},{"CampusCode":null,"Id":2182,"Name":"WorkFlow","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"WorkFlow","FunctionalName":null,"Id":1412,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/WorkFlow"},{"DisplayInMenu":true,"DisplayName":"Vacancy Approval Exception","FunctionalName":null,"Id":9020,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/VacancyAprvlException"}]},{"CampusCode":null,"Id":5251,"Name":"Year Ending","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Financial Year Ending","FunctionalName":null,"Id":8847,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/FinancialYearEnding"},{"DisplayInMenu":true,"DisplayName":"Lead Year Ending","FunctionalName":null,"Id":8848,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/LeadYearEnding"}]},{"CampusCode":null,"Id":5221,"Name":"Blocked Users","ShortName":null,"SortOrder":0,"SubMenus":[{"DisplayInMenu":true,"DisplayName":"Blocked Users","FunctionalName":null,"Id":6600,"IsWorkflowAvailable":false,"MetaTag":null,"MethodName":null,"Operation":null,"OperationId":0,"Selected":false,"ServiceName":null,"SortOrder":0,"Url":"Admin/BlockedUsers"}]}]';
    var jsonString = '[{"name":"Projects","icon":"fa fa-cubes","url":"<?=site_url('project')?>","submenu":""},{"name":"Project Plan","icon":"fa fa-cubes","url":"<?=site_url('Project/projectPlan')?>","submenu":""},{"name":"Week Workload","icon":"fa fa-clock-o","url":"<?=site_url('Project/weekWorkload')?>","submenu":""},{"name":"Project Details","icon":"fa fa-tachometer","url":"<?=site_url('Project/projectDetails')?>","submenu":""},{"name":"Day Monitoring","icon":"fa fa-clock-o","url":"<?=site_url('Project/dayMonitoring')?>","submenu":""},{"name":"Users Workload","icon":"fa fa-truck","url":"<?=site_url('Project/usersWorkload')?>","submenu":""},{"name":"Users","icon":"fa fa-users","url":"<?=site_url('user/userList')?>","submenu":""},{"name":"Reports","icon":"fa fa-file-excel-o","url":"<?=site_url('reports')?>","submenu":[{"name":"Reports","icon":"fa fa-file-excel-o","url":"<?=site_url('reports')?>","submenu":""},{"name":"Project Status Report","icon":"fa fa-file-excel-o","url":"<?=site_url('reports/projectStatus')?>","submenu":""},{"name":"Requirement Status Report","icon":"fa fa-file-excel-o","url":"<?=site_url('reports/getProjectRequirementStatus')?>","submenu":""},{"name":"Team Workload Report","icon":"fa fa-file-excel-o","url":"<?=site_url('reports/getTeamWorkloadReport')?>","submenu":""},{"name":"Week Workload Report","icon":"fa fa-file-excel-o","url":"<?=site_url('reports/getWeekWorkloadReport')?>","submenu":""}]},{"name":"Masters","icon":"fa fa-cubes","url":"<?=site_url('masters')?>","submenu":[{"name":"Departments","icon":"fa fa-university","url":"<?=site_url('department')?>","submenu":""},{"name":"Clients","icon":"fa fa-black-tie","url":"<?=site_url('client')?>","submenu":""},{"name":"TasK Types","icon":"fa fa-list","url":"<?=site_url('Task_types')?>","submenu":""},{"name":"Check List","icon":"fa fa-check","url":"<?=site_url('Check_list')?>","submenu":""},{"name":"Meeting","icon":"fa fa-briefcase","url":"<?=site_url('Task_types')?>/meeting","submenu":""},{"name":"Call","icon":"fa fa-phone","url":"<?=site_url('Task_types')?>/call","submenu":""},{"name":"Interview","icon":"fa fa-users","url":"<?=site_url('Task_types')?>/interview","submenu":""}]},{"name":"User Task","icon":"fa fa-tasks","url":"<?=site_url('Project')?>/userTask","submenu":""},{"name":"Attendance","icon":"fa fa-upload","url":"<?=site_url('Attendance')?>","submenu":""},{"name":"User Work Request","icon":"fa fa-list-alt","url":"<?=site_url('Project/userWorkApproval')?>","submenu":""},{"name":"Request More Time","icon":"fa fa-list-alt","url":"<?=site_url('Project/requestMoreTimeApproval')?>","submenu":""},{"name":"Holiday","icon":"fa fa-list-alt","url":"<?=site_url('Holiday')?>","submenu":""}]';

    <?php } else if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==0){ ?>
    var jsonString = '[{"name":"Dashboard","icon":"fa fa-calendar","url":"<?=site_url('Project/fullCalendar')?>", "submenu":""}, {"name":"My Task","icon":"fa fa-tasks","url":"<?=site_url('Project')?>/userTask", "submenu":""}, {"name":"Work Request","icon":"fa fa-list-alt","url":"<?=site_url('Project/userWorkRequest')?>", "submenu":""}, {"name":"Holiday","icon":"fa fa-list-alt","url":"<?=site_url('Holiday/user_list')?>", "submenu":""}]';
    <?php } else if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==1){ ?>
    /*{"name":"My Task","icon":"fa fa-tasks","url":"<?=site_url('Project')?>/userTask", "submenu":""},*/
    var jsonString = '[{"name":"Projects","icon":"fa fa-cubes","url":"<?=site_url('project')?>", "submenu":""},{"name":"Dashboard","icon":"fa fa-calendar","url":"<?=site_url('Project/fullCalendar')?>", "submenu":""}, {"name":"Week Workload","icon":"fa fa-clock-o","url":"<?=site_url('Project/weekWorkload')?>", "submenu":""}, {"name":"Users Task","icon":"fa fa-clock-o","url":"<?=site_url('Project/MemberTask')?>", "submenu":""},  {"name":"Work Request","icon":"fa fa-list-alt","url":"<?=site_url('Project/userWorkRequest')?>", "submenu":""}, {"name":"Holiday","icon":"fa fa-list-alt","url":"<?=site_url('Holiday/user_list')?>", "submenu":""},{"name":"Request More Time","icon":"fa fa-list-alt","url":"<?=site_url('Project/requestMoreTimeApproval')?>", "submenu":""}]';
    <?php } ?>
    var _subM = '';

    $(function(){
        _menusUl.html('');
        _searchLi = $('<li/>', { class: 'search-menu', style: 'height:43px', html: '<a href="javascript:;" class="showSearch"><i class="fa fa-search "></i></a>' });
        _menuSearchBox = $('<div/>', { class: 'searchbox' });
        _inputSearch = $('<input />', { id: 'menuSearchDemo', type: 'text', placeholder: 'Search' });
        $(_menuSearchBox).append(_inputSearch).append('<i class="fa fa-search pull-right"></i>');
        $(_searchLi).append(_menuSearchBox);
        $(_menusUl).append(_searchLi);
        var leftMenuData = JSON.parse(jsonString);
        $.each(leftMenuData, function() { _subM = '';
            if(this.submenu == ""){
                $('<li class="navMenuList"><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+this.name+'" href="'+this.url+'"><i style="width:18px;overflow:hidden" class="'+this.icon+'"></i><span class="menuTitle">' + this.name + '</span></a></li>').appendTo($(_menusUl));}
            else{
                _subM += '<li class="nav-parent" ><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+this.name+'" href="javascript:;"><i style="width:18px;overflow:hidden" class="'+this.icon+'"></i><span>' + this.name + '</span></a><ul class="children">';
                //$('<li class="nav-parent navMenuList" ><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+this.name+'" href="javascript:;"><i style="width:18px;overflow:hidden" class="'+this.icon+'"></i><span>' + this.name + '</span></a></li>').appendTo($(_menusUl));
                $.each(this.submenu, function(i, o){
                    _subM +='<li class="navMenuList"><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+o.name+'" href="'+o.url+'"><i style="width:18px;overflow:hidden" class="fa fa-caret-right"></i><span class="menuTitle">' + o.name + '</span></a></li>';
                });
                _subM +='</ul></li>';
                $(_subM).appendTo($(_menusUl));
                preSelectMenu(_subM);
            }
        });
        <?php if($this->session->userdata('user_type_id')==3 ){ ?>
        postJsonAsyncWithBaseUrl("User/getUserNavigation", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var leftMenuData = result.data.data;
                    var _menu = '',
                        subM = '';
                    $.each(leftMenuData, function() {
                        if(this.submenu == ""){
                            if(this.key=='departments' || this.key=='clients' || this.key=='task-types' || this.key=='check-list' || this.key=='meeting' || this.key=='call'){
                                _menu = '<li class="nav-parent" ><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="Masters" href="javascript:;"><i style="width:18px;overflow:hidden" class="fa fa-cubes"></i><span>Masters</span></a><ul class="children">';
                                subM +='<li class="navMenuList 1"><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+this.name+'" href="<?=WEB_BASE_URL?>index.php/'+this.url+'"><i style="width:18px;overflow:hidden" class="fa fa-caret-right"></i><span class="menuTitle">' + this.name + '</span></a></li>';
                            }
                            else{
                                $('<li class="navMenuList"><a  class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+this.name+'" href="<?=WEB_BASE_URL?>index.php/'+this.url+'"><i style="width:18px;overflow:hidden" class="'+this.icons+'"></i><span class="menuTitle">' + this.name + '</span></a></li>').appendTo($(_menusUl));
                            }
                        }
                        else{
                            $('<li class="nav-parent navMenuList" ><a class="nav-tooltip" data-toggle="tooltip" data-placement="right" title="'+this.name+'" href="javascript:;"><i style="width:18px;overflow:hidden" class="'+this.icons+'"></i><span>' + this.name + '</span></a></li>').appendTo($(_menusUl));
                            $.each(this.submenu, function(i, o){
                                showLog(o);
                            });
                        }
                    });
                    if(_menu!=''){
                        subM +='</ul></li>';
                        $(_menu+subM).appendTo($(_menusUl));
                        preSelectMenu(subM);
                    }
                }
            }});
        <?php } ?>
        menuFilter();
        $('.nav-tooltip').tooltip({ container: 'body' });//Added to overie the default tooltip
    })

    function menuFilter(){
        $('#menuSearchDemo').keyup(function(){

            var that = this, $allListElements = $('.navMenuList');

            var $matchingListElements = $allListElements.filter(function(i, li){
                var listItemText = $(li).find('.menuTitle').text().toUpperCase(), searchText = that.value.toUpperCase();
                return ~listItemText.indexOf(searchText);
            });

            $allListElements.hide();
            $matchingListElements.show();

        });
    }

    function preSelectMenu(list) {
        var url = decodeURIComponent(window.location.href);
        var objectLen = $("a[href$='" + url + "']").closest('li').length;
        if (objectLen == 0) {
            url = $.cookie("HighlightsUrl");
        } else {
            $.cookie("HighlightsUrl", url);
        }
        $("a[href$='" + url + "']").closest('li').parent().parent().addClass('nav-active');
        $("a[href$='" + url + "']").closest('li').parent().css('display','block');
        $("a[href$='" + url + "']").closest('li').addClass('active');
        $("a[href$='" + url + "']").closest('.nav-parent').find('a').trigger('click');
        _menuLeft();
    }


</script>
<!--@RenderSection("FooterCss", required: false)-->
<!--@RenderSection("Footerscripts", required: false)-->

</body>
<!-- InstanceEnd --></html>
<?php } else { ?>
    <script src="<?= WEB_BASE_URL ?>scripts/app.js"></script>
    </body>
    </html>
<?php } ?>
