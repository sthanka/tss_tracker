<?php
/**
 * Created by PhpStorm.
 * User: rameshpaul
 * Date: 16/5/16
 * Time: 12:17 PM
 */


class Department_model extends CI_Model {


    public function getDepartmentsGrid($paramArr){
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'`order`';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'ASC';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (d.department_name LIKE '%".$whereParam."%' || d.order='".$whereParam."' )";}
        $whereClause = "where true ".$whereParam;

        if($sortField == 'order')
            $sortField = 'd.order';

        $SQL = "SELECT d.*  from `department` d $whereClause
          order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT count(*)  from `department` d
          $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }
    public function addDepartment($data){
        $this->db->insert('department', $data);
        return $this->db->insert_id();
    }

    public function updateDepartment($id,$data){
        $this->db->where('id_department', $id);
        return $this->db->update('department', $data);
    }
    public function getDepartments(){
        $this->db->select('*');
        $this->db->from('department');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function checkDepartmentName($id,$name){
        $this->db->select('*');
        if($id != ''){
            $this->db->where('id_department !=', $id);
        }
        $this->db->where('department_name', $name);
        $this->db->from('department');
        $query = $this->db->get();
        return $query->result_array();

    }
    public function deleteDepartmentById($department_id){
        $this->db->where('id_department', $department_id);
        $this->db->delete('department');
        return 1;
    }
    public function checkProjectAssigned($depttId){
        $this->db->select('id_project_department');
        $this->db->from('project_department');
        $this->db->where('department_id',$depttId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }
    public function checkUserAssigned($depttId){
        $this->db->select('id_user_department');
        $this->db->from('user_department');
        $this->db->where('department_id',$depttId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }
    public function checkTaskTypeAssigned($depttId){
        $this->db->select('id_task_type_xref_department');
        $this->db->from('task_type_xref_department');
        $this->db->where('department_id',$depttId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }
    public function checkChecklistAssigned($depttId){
        $this->db->select('checklist_deparment_id');
        $this->db->from('department_checklist');
        $this->db->where('department_id',$depttId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }

}