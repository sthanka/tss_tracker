<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Reports</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Reports
                    </h3>
                </div>
                <div class="padding0">

                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <form id="reports_frm" class="NewErrorStyle" name="reports_frm" method="post"
                              action="<?= WEB_BASE_URL ?>index.php/Reports/generateReport">
                            <div class="col-sm-10 padding0">
                                <div class="col-sm-6">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="start_date" value="<?=date('d/m/Y',strtotime("-1 days"))?>"
                                                       name="start_date" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>

                                        <label class="control-label col-sm-12">Start Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container">
                                            <div class="input_container">
                                                <input type="text" placeholder="dd/mm/yyyy" id="end_date" value="<?=date('d/m/Y')?>"
                                                       name="end_date" class="rval form-control tssDatepicker">
                                            </div>
                                        </div>
                                        <label class="control-label col-sm-12">End Date:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="projectDropdown" name="project_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Project:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="departmentDropdown" name="department[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Department:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="employeeDropdown" name="user_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Employee:</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <!--<button class="button button-common module mt20"  onclick="resultType(0);" > Search </button>
                                    <button class="button button-common module mt20"  onclick="resultType(1);" > Excel </button>-->
                                    <input type="button" class="button button-common module mt20"
                                           onclick="validateReportsForm(0);" value="Search">
                                    <input type="button" class="button button-common module mt20"
                                           onclick="validateReportsForm(1);" value="Excel">
                                </div>
                            </div>
                            <input type="hidden" name="type" id="type" value="0">
                        </form>

                    </div>
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="grid-details-table-content clearfix padding0">
                            <div class="tbl_wrapper border0">
                                <table id="project-list" class="table-responsive"></table>
                                <div id="project-list-page"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#project-list');

        $(function () {
            $('#log_start_date').val(new Date().format('d/m/Y'));

            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Reports/getUserLogTime',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['Project Name', 'User Name', 'Task Name', 'Task Type', 'Start Time', 'End Time', 'Duration', 'Date'],
                colModel: [
                    {name: 'project_name', index: 'project_name', hidden: false, key: false},
                    {name: 'user_name', index: 'user_name', hidden: false, key: false},
                    {name: 'task_name', index: 'task_name', hidden: false, key: true},
                    {name: 'task_type', index: 'task_type', hidden: false, key: true},
                    {name: 'start_time', index: 'start_time', hidden: false, key: true},
                    {name: 'end_time', index: 'end_time', hidden: false, key: true},
                    {name: 'duration', index: 'duration', hidden: false, key: true},
                    {name: 'date', index: 'date', hidden: false, key: true}
                ],
                pager: 'project-list-page'
            }).navGrid('#project-list-page', {
                edit: false, add: false, del: false, search: false, refresh: true
            });

            $('#projectDropdown').on('change', function () {
                if ( $( "#projectDropdown" ).val()  == null ) { return false; }
                loadDepartments();
            });
            $('#departmentDropdown').on('change', function () {
                if (  $( "#departmentDropdown" ).val()  == null ) { return false; }
                loadUsers();
            });
            postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var projects = result.data;
                        TssLib.populateSelect($('#projectDropdown'), {
                            success: true,
                            data: projects
                        }, 'project_name', 'id_project');
                        TssLib.selectAllOptions('#projectDropdown');
                        loadDepartments();
                    }
                }
            });

        });
        function loadDepartments() {
            var project = $('#projectDropdown').val();
            project = project.join('-');
            postJsonAsyncWithBaseUrl("Project/getProjectDepartments/" + project, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var departments = result.data.data;
                        TssLib.populateSelect($('#departmentDropdown'), {
                            success: true,
                            data: departments
                        }, 'department_name', 'id_department');
                        TssLib.selectAllOptions('#departmentDropdown');
                        loadUsers();
                    }
                }
            });
        }
        function loadUsers() {
            var deptt = $('#departmentDropdown').val();
            deptt = deptt.join(',');
            var project = $('#projectDropdown').val();
            project = project.join(',');
            postJsonAsyncWithBaseUrl("Project/getUserByDepartmentProject", {'project_id':project , 'department_id': deptt}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var user = result.data.data;
                        TssLib.populateSelect($('#employeeDropdown'), {
                            success: true,
                            data: user
                        }, 'email', 'id_user');
                        TssLib.selectAllOptions('#employeeDropdown');

                        validateReportsForm(0);
                    }
                }
            });
        }
        function resultType(type) {
            $('#type').val(type);
        }

        function validateReportsForm(type) {
            $('#start_date').removeClass('err-bg');
            $('#end_date').removeClass('err-bg');
            $('#employeeDropdown').removeClass('err-bg');
            $('.error-msg').remove();

            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var project = $('#projectDropdown').val();
            var department = $('#departmentDropdown').val();
            var user_id = $('#employeeDropdown').val();
            var flag = 0;

            if (start_date == '') {
                $('#start_date').addClass('err-bg');
                $('#start_date').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (end_date == '') {
                $('#end_date').addClass('err-bg');
                $('#end_date').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (project == '') {
                $('#projectDropdown').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (department == '') {
                $('#departmentDropdown  ').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }
            if (user_id == '') {
                $('#employeeDropdown').addClass('err-bg');
                $('.multi-select-container').before('<span class="error-msg">Required</span>');
                flag++;
            }

            if (flag == 0) {
                if (type == 0) {
                    var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
                    myPostData['project'] = project;
                    myPostData['department'] = department;
                    myPostData['user_id'] = user_id;
                    myPostData['start_date'] = start_date;
                    myPostData['end_date'] = end_date;
                    //console.log(user_id);
                    $refTblgrid.setGridParam({'postData': myPostData});
                    $('#project-list').trigger("reloadGrid");

                    return false;
                }
                else {
                    $('#reports_frm').submit();
                }
            }
        }

        $(function(){

            /*setTimeout(function(){
                validateReportsForm(0);
            }, 1000);*/

        });
    </script>