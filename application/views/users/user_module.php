
<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!-- Modal Dialog  dekete confirmation-->
<div class="modal fade" id="oasis-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel panel-default panel-body padding20 clearfix">
                    <p></p>
                </div>
                <div class="panel-footer col-sm-12 clearfix text-right">
                    <button type="button" class="button-common module btn-danger">Yes</button>
                    <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Users Module List</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Users List</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="project-list" class="table-responsive"></table>
                        <div id="project-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-user-modal" class="modal-wrapper" style="display:none">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#" ><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add User Module</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="module_name" id="module_name"  />
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Module Name :</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <input type="text" class="form-control rval" name="module_url"  id="module_url" />
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Module URL:</label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control " name="id_module" id="id_module" />
                        </div>
                    </div>

                    <div class="panel-footer text-center">
                        <button class="button-common" div-submit="true">Save</button>
                        <button class="button-color" data-dismiss="modal" onclick="cancelAddUser(this)">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var departmentNumber = 0;
        var reference = "Allotted";
        TssLib.docReady(function () {
            var $refTblgrid = $('#project-list');
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'user/getUsersModuleGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['id_module','Module Name','Module_key','Module URL','created_date'],
                colModel: [
                    { name: 'id_module', index: 'id_module', hidden: true ,key:true},
                    { name: 'module_name', index: 'module_name', hidden: false },
                    { name: 'module_key', index: 'module_key', hidden: true },
                    { name: 'module_url', index: 'module_url', hidden: false },
                    { name: 'created_date', index: 'created_date', hidden: true },

                ],
                pager: 'project-list-page'
            }).navGrid('#project-list-page', {
                edit: true, add: true, del: false, search: false, refresh: true
                ,addfunc: function () {
                    addEditCreateSchedule();
                }, editfunc: function (id) {
                    var rowData = $refTblgrid.jqGrid('getRowData', id);
                    addEditCreateSchedule(rowData);
                }
            });
        });

        function addEditCreateSchedule(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-user-modal').html());
            if (TssLib.isBlank(rowData)) {
                $modal.find('.panel-title').text('Add User Module');
                $modal.find('[div-submit="true"]').text('Save');
            } else {
                TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                $modal.find('.panel-title').text('Edit User Module');
                $modal.find('[div-submit="true"]').text('Update');
            }
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {
                    if (result.data.status) {
                        $('#project-list').trigger('reloadGrid');
                        TssLib.closeModal();
                        if (TssLib.isBlank(rowData)) {
                            TssLib.notify('Module added successfully ', null, 5);
                        }else{
                            TssLib.notify('Module updated successfully ', null, 5);
                        }
                    }else{
                        TssLib.notify(result.data.message , 'warn', 5);
                    }
                },
                before: function (formObj) {
//                    $(formObj).data('additionalData', { 'department_id': selectedDepartments});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'User/addUserModule');
                    return true;
                }
            });
        }
        function dateFormat(c, o, d) {
            if (TssLib.isBlank(c))
                return '';
            else
                return c.DateWCF().format('d/m/Y');
        }
        function toggleAddDepartment(index, lead){
            var _html = '';
            postJsonAsyncWithBaseUrl("client/getClients", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        departmentsData = result.data.departments;
                        buildList(departmentsData, index, lead);
                    }
                }});
        }
        var selectedDepartments = [];
        function buildList(departmentsData, selectedData, selectedLead){
            var _html = '';
            departmentNumber = departmentNumber+1;
            _html += '<div  class="col-sm-12 padding0 clearfix department-class multiple-department-class-'+departmentNumber+'">';
            _html += '<div class="col-sm-6 pl0">';
            _html += '<div class="form-group ">';
            _html += '<div class="input_container">';
            _html += '<select class="form-control" class="multiple-department" id="department_id">';
            _html += '<option value="">Select</option>';
            for(var index in departmentsData){
                if(selectedData==departmentsData[index]['id_department']){
                    _html += '<option selected value="'+departmentsData[index]['id_department']+'">'+departmentsData[index]['department_name']+'</option>';
                }else{
                    _html += '<option value="'+departmentsData[index]['id_department']+'">'+departmentsData[index]['department_name']+'</option>';
                }
            }
            _html += '</select>';
            _html += '</div>';
            _html += '<label class="control-label"><span class="req-str">*</span> Department :</label>';
            _html += '</div>';
            _html += '</div>';

            _html += '<div class="col-sm-3">';
            _html += '<div class="form-group">';
            _html += '<div class="input_container">';
            if(selectedLead!=0){
                _html += '<input checked type="checkbox"  name="is_lead" class="department_is_lead" />';
            }else{
                _html += '<input type="checkbox"  name="is_lead" class="department_is_lead" />';
            }
            _html += '</div>';
            _html += '<label class="control-label">Team lead :</label>';
            _html += '</div>';
            _html += '</div>';

            _html += '<div class="col-sm-3">';
            _html += '<a href="javascript:;" onclick="deleteDepartment(this)"> <i class="fa fa-trash-o mt20"></i> <a>';
            _html += '</div>';

            _html += '</div>';
            $('#add-user-department').append(_html);
            $('select').select2();
        }
        function cancelAddUser(This){
            //Cancel all the department cretaed and close the moda
            selectedDepartments = [];
            departmentNumber = 0;
            $('#add-user-department').html('');
//                    $('#add-user-department').html(_html);
        }
        function deleteDepartment(This){
            $(This).parent().parent().remove();
            departmentNumber = departmentNumber-1;
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--<a href="javascript:;" class="footer-logo clearfix"><img src="<?/*=WEB_BASE_URL*/?>images/people-combine-logo.png" /></a>-->
</div>
<!--Render Body End-->