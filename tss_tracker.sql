/*
Navicat MySQL Data Transfer

Source Server         : 4 Server
Source Server Version : 50547
Source Host           : 192.168.0.4:3306
Source Database       : tss_tracker

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-05-02 19:31:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `client`
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `creared_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', 'naresh', null);
INSERT INTO `client` VALUES ('2', 'rakesh', null);
INSERT INTO `client` VALUES ('3', 'bhasha', null);
INSERT INTO `client` VALUES ('4', 'venkatesh', null);
INSERT INTO `client` VALUES ('5', 'mohan', null);

-- ----------------------------
-- Table structure for `job_role`
-- ----------------------------
DROP TABLE IF EXISTS `job_role`;
CREATE TABLE `job_role` (
  `id_job_role` int(11) NOT NULL AUTO_INCREMENT,
  `job_role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_job_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_role
-- ----------------------------
INSERT INTO `job_role` VALUES ('1', 'Ceo');
INSERT INTO `job_role` VALUES ('2', 'Project Manage');
INSERT INTO `job_role` VALUES ('3', 'Seniour Software Engineer');
INSERT INTO `job_role` VALUES ('4', 'Junior Software Engineer');

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `project_start_date` varchar(255) DEFAULT NULL,
  `project_end_date` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('1', 'Q-lana', '1', '/Date(1462147200000)/', '/Date(1463529600000)/', 'Q-lana is bank loan application process', 'inprogress', null);
INSERT INTO `project` VALUES ('2', 'miles', '2', '/Date(1462147200000)/', '/Date(1463529600000)/', 'Miles is school and instution project1', 'inprogress', null);
INSERT INTO `project` VALUES ('4', 'sdsdf', '1', '/Date(1462147200000)/', '/Date(1463529600000)/', 'sdfsdf', 'inprogress', null);
INSERT INTO `project` VALUES ('5', 'xvxcv', '2', '/Date(1463616000000)/', '/Date(1464220800000)/', 'xcvxcv', 'inprogress', null);
INSERT INTO `project` VALUES ('6', 'zxcxzc', '1', '/Date(1463616000000)/', '/Date(1464134400000)/', 'zxc', 'inprogress', null);

-- ----------------------------
-- Table structure for `project_task`
-- ----------------------------
DROP TABLE IF EXISTS `project_task`;
CREATE TABLE `project_task` (
  `id_task` int(45) NOT NULL AUTO_INCREMENT,
  `project_id` int(45) DEFAULT NULL,
  `module_name` varchar(45) DEFAULT NULL,
  `task_name` varchar(45) DEFAULT NULL,
  `sub_task_name` varchar(45) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `esimatated_time` datetime DEFAULT NULL,
  `status` int(45) DEFAULT NULL,
  PRIMARY KEY (`id_task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_task
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `gmail_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `user_image` text,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `job_role_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '3', '110567892914406443418', 'basha', 'shaik', 'male', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'bhasha.s@thresholdsoft.com', '8341532551', '3', '1');

-- ----------------------------
-- Table structure for `user_type`
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `id_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES ('1', 'Super admin');
INSERT INTO `user_type` VALUES ('2', 'Admin');
INSERT INTO `user_type` VALUES ('3', 'user');
