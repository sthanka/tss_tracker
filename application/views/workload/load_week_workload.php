<?php $responsiveClass = ($this->session->userdata('is_lead')==1) ? 'col-sm-9' : 'col-sm-12'; ?>
<div class="col-sm-12 padding0 clearfix">
    <div class="week-but mb5 text-center <?=$responsiveClass?>">
                <span class="" >
                     <?php if($last_week){ ?>
                         <span  class="fa fa-angle-left btn-red" onclick="getWeekWorkLoad(<?=$last_week?>);" ></span>
                     <?php }  ?>
                </span>
        <span class="input-middle"><?=date('M d',$week)?>&nbsp;&nbsp;  - &nbsp;&nbsp;<?=date('M d',strtotime(date('d-m-Y', $week).'+ 6 days'))?></span>
                <span class="">
                    <?php if($next_week){ ?>
                        <span class="fa fa-angle-right btn-red" onclick="getWeekWorkLoad(<?=$next_week?>);"></span>
                    <?php } ?>
                    </span>

        <input type="hidden" id="cur_week_time" value="<?=$week?>">
    </div>
    <div class="col-sm-3"></div>
</div>

<div class="col-sm-12 padding0">
    <div class="fixed-table clearfix <?=$responsiveClass?>">
        <table class="task-assigning-table mr15" >
            <thead>
            <th> <span class="th-box">TEAM</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week)))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d',strtotime(date('d-m-Y' , $week)))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d',strtotime(date('d-m-Y' , $week)))])){ echo 'title="'.$holidays[date('Y-m-d',strtotime(date('d-m-Y' , $week)))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week)))?> Monday</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 1 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+1 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+1 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+1 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 1 days'))?> Tuesday</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 2 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+2 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+2 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+2 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 2 days'))?> Wednesday</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 3 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+3 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+3 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+3 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 3 days'))?> Thursday</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 4 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+4 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+4 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+4 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 4 days'))?> Friday</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 5 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+5 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+5 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+5 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 5 days'))?> Saturday</span></th>
            <th class="<?= date('d-m-Y') == date('d-m-Y',strtotime(date('d-m-Y' , $week).'+ 6 days'))?'current-date':'' ?> <?php if(isset($holidays[date('Y-m-d' ,strtotime('+6 day', $week))])){ echo 'holiday'; } ?>" <?php if(isset($holidays[date('Y-m-d' ,strtotime('+6 day', $week))])){ echo 'title="'.$holidays[date('Y-m-d' ,strtotime('+6 day', $week))]['title'].'"'; } ?>><span class="th-box"><?=date('d',strtotime(date('d-m-Y', $week).'+ 6 days'))?> Sunday</span></th>
            </thead>
        </table>
    </div>
    <div class="fixed-container">


        <div class="<?=$responsiveClass?> task-assigning-table-prnt pr0">

            <table class="task-assigning-table mr15">
                <thead><th>&nbsp;</th></thead>
                <tbody>
                <?php for($s=0;$s<count($users);$s++){ ?>
                    <tr>
                        <td>
                            <div class="user-inv-tsk-box">
                                <span title="<?=$users[$s]['first_name'].' '.$users[$s]['last_name']?>" class="user-inv-tsk-name"><span><?=$users[$s]['first_name'].' '.$users[$s]['last_name']?></span><span><?=$users[$s]['department_name']?></span></span>
                                <span>Last Week :<span class="last-week-good"><?=$users[$s]['last_week_work_load']?>%</span></span>
                                <span>Work Load :<span class="work-load-bad"><?=$users[$s]['current_week_work_load']?>%</span></span>
                                <span class="user-inv-tsk-good"><?php if($users[$s]['percentage'] > 60){ echo 'Good';}else if($users[$s]['percentage'] < 30){  echo 'Bad'; }else{  echo 'Average'; } ?></span>
                            </div>
                        </td>
                        <?php if($this->session->userdata('is_lead')==1){ $type = 1; } else { $type = 0; } ?>
                        <?php  for($st=0;$st<=6;$st++){ $sr = $st+1; $stha=0;
                            for($sth=0;$sth<count($users[$s]['work_load']);$sth++){
                                if(isset($users[$s]['work_load'][$sth]) && $users[$s]['work_load'][$sth]['day_id']==$sr){  $stha = $sr;
                                    ?>
                                    <td class="<?php if(isset($holidays[date('Y-m-d' ,strtotime('+'.$st.' day', $week))])){ echo 'holiday'; } ?>" >
                                        <?php
                                        $load = $this->Project_modal->getUserDayLoad(array('user_id' => $users[$s]['id_user'],'date' => date('Y-m-d',strtotime('+'.$st.' day', $week))));
                                        $original_load = $this->Project_modal->getUserDayOriginalLoad(array('user_id' => $users[$s]['id_user'],'date' => date('Y-m-d',strtotime('+'.$st.' day', $week))));
                                        //echo "<pre>"; print_r($load); exit;
                                        ?>
                                        <div onclick="getDayTaskList(<?=$users[$s]['id_user']?>,<?=strtotime('+'.$st.' day', $week)?>,<?=$project_id?>,<?=$type?>);" data-day="<?=strtotime('+'.$st.' day', $week)?>" data-day-name="<?=date('l', strtotime('+'.$st.' day', $week))?>" data-user-id="<?=$users[$s]['id_user']?>" data-user-name="<?=$users[$s]['first_name'].' '.$users[$s]['last_name']?>" data-engaged-time="<?=$original_load[0]->alloted_time?>"
                                             class="inv-tsk-box <?php if(strtotime('+'.$st.' day', $week)<strtotime(date('Y-m-d'))){ echo "previous-day"; }else if(strtotime('+'.$st.' day', $week)==strtotime(date('Y-m-d'))){ echo "current-day"; } else{ echo "coming-day"; } ?>
                                         <?php if($users[$s]['work_load'][$sth]['day_id']<date('N')){ echo 'un-droppable1'; } else{
                                                 sscanf($load[0]->alloted_time, "%d:%d", $hours, $minutes);
                                                 $secs = $hours * 3600 + $minutes * 60;

                                                 sscanf("08:00", "%d:%d", $hours, $minutes);
                                                 $secs1 = $hours * 3600 + $minutes * 60;
                                                 if($secs>=$secs1){ echo 'un-droppable'; }
                                             } ?>
                                         ">
                                            <span>Estimated :<span><?=$load[0]->alloted_time?></span></span>
                                            <span>Actual :<span><?=$users[$s]['work_load'][$sth]['actual_time']?></span></span>
                                            <span>Projects :<span><?=$users[$s]['work_load'][$sth]['project_count']?></span></span>
                                            <span>Tasks :<span class="red-color"><?=$users[$s]['work_load'][$sth]['completed_task']?>/<?=$users[$s]['work_load'][$sth]['total_task']?></span></span>
                                        </div>
                                    </td>
                                <?php } }

                            if($stha==0)
                            { ?>
                                <td class="<?php if(isset($holidays[date('Y-m-d' ,strtotime('+'.$st.' day', $week))])){ echo 'holiday'; } ?>" ><div data-day="<?=strtotime('+'.$st.' day', $week)?>" data-day-name="<?=date('l', strtotime('+'.$st.' day', $week))?>" data-user-id="<?=$users[$s]['id_user']?>" data-user-name="<?=$users[$s]['first_name'].' '.$users[$s]['last_name']?>" data-engaged-time='00:00' class='inv-tsk-box total-day'></div></td>
                            <?php }

                        }
                        ?>
                    </tr>
                <?php }  ?>
                <?php if(!count($users)){ ?>
                    <tr>
                        <td colspan="6" >
                            <div class="text-center padding5">No Team Members Found</div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if($this->session->userdata('is_lead')==1){ ?>
        <div class="col-sm-3 padding0">
            <div class="unassigned-task-wrap">
                <h4 class="umain-heading">Task list
                </h4>
                <div class="col-sm-12 padding5 bg-white">
                    <div class="filters-wrap">
                        <!--<a onclick="showUnAssignedTaskshowByProjects(this)">Filters</a>
                            <a href="javascript:;" onclick="getUnassignedTaskList(<?/*=$project_id*/?>,0);">Clear All</a>-->
                        <a href="javascript:;" class="color-green" onclick="getUnassignedTaskList(<?=$project_id?>,'unassigned',this);">Unassigned <span id="unassignedcounts">(<?=$unassigned_task_counts['unassigned'] ?>)</span></a>
                        <a href="javascript:;" onclick="getUnassignedTaskList(<?=$project_id?>,'assigned',this);">Assigned <span id="assignedcounts">(<?=$unassigned_task_counts['assigned'] ?>)</span></a>
                        <a href="javascript:;" onclick="getUnassignedTaskList(<?=$project_id?>,'completed',this);">Completed <span id="completedcounts">(<?=$unassigned_task_counts['completed'] ?>)</span></a>
                        <a href="javascript:;" onclick="getUnassignedTaskList(<?=$project_id?>,'approval_waiting',this);">Pending <span id="pendingcounts">(<?=$unassigned_task_counts['pending'] ?>)</span></a>
                    </div>
                    <div class="">
                        <span class="tsk-green-legend">Alloated</span>
                        <span class="tsk-orange-legend">Not alloated</span>
                    </div>
                    <div class="">
                        <input type="text" id="filter-unassigned-task-box" placeholder="Search task"/>
                    </div>
                    <div class="col-sm-12 padding0" id="unasign-t">

                        <?php for($s=0;$s<count($unassigned_task);$s++){
                            $est_time = sec_to_time(time_to_sec($unassigned_task[$s]['estimated_time'])+time_to_sec($unassigned_task[$s]['additional_time']));
                            $allowted_time = $this->Project_modal->getTaskAllotedTime($unassigned_task[$s]['project_task_id'],$est_time);

                            if($allowted_time[0]['remains']!='00:00:00' && $allowted_time[0]['remains']!=''){
                                $allotedClass = $allowted_time[0]['alloted_time']=='00:00:00'?'not-alloted-task':'alloted-task';
                                ?>
                                <div class="unassigned-task-box <?=$allotedClass?>" data-title="<?=$unassigned_task[$s]['task_name']?>">
                                    <input type="hidden" name="project_task_id" value="<?=$unassigned_task[$s]['project_task_id']?>">
                                    <input type="hidden" name="project_id" value="<?=$unassigned_task[$s]['project_id']?>">
                                    <h4 name="project-title">
                                        <a class="task_name" href="javascript:;" >
                                            <?=$unassigned_task[$s]['task_name']?>
                                            <?/*=$unassigned_task[$s]['project_name']*/?> </a>
                                        <a onclick="getEditTask('<?=$unassigned_task[$s]['project_name']?>','<?=$unassigned_task[$s]['task_name']?>',<?=$unassigned_task[$s]['project_task_id']?>,<?=$unassigned_task[$s]['project_id']?>,'<?=$unassigned_task[$s]['estimated_time']?>','<?=$allowted_time[0]['remains']?>')" href="javascript:;" class="float-right"><span class="fa fa-pencil"></span></a>
                                    </h4>
                                    <!--                                    <div name="task_name" class="task_name">--><?//=$unassigned_task[$s]['task_name']?><!--</div>-->
                                    <div class="est-alot-rem">
                                        <span> Estimated :
                                            <span  name="estimated-time">
                                                <span class="estDefalt"><?=$unassigned_task[$s]['estimated_time']?></span>
                                                <!--<a class="editEstimatedTimeEditOption" href="javascript:;" onclick="showEstimatedTimeEditOption(this)" ><span class="fa fa-pencil"></span></a>
                                                <span class="estEdit" style="display: none;">
                                                    <input  style="width: 50px" class="msk_time" value="<?/*=$unassigned_task[$s]['estimated_time']*/?>" type="text" class="form-control rval pInt" id="estimation_time" name="estimation_time">
                                                    <a class="p10" href="javascript:;" onclick="SaveEstimatedTimeEditOption(this, '<?/*=$unassigned_task[$s]['id_task_flow']*/?>' )" ><i class="fa fa-check"></i></a>
                                                </span>-->
                                            </span>
                                        </span>
                                        <span><span>Additional:</span><span><?=$unassigned_task[$s]['additional_time']?></span></span>
                                        <span><span>Alloted:</span><span><?=$allowted_time[0]['alloted_time']?></span></span>
                                        <span><span>Remains:</span><span name="remains"><?=$allowted_time[0]['remains']?></span></span>
                                    </div>
                                </div>
                            <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div>
    </div>
</div>

<script>
    $(function() {
        minScroll();
        $(".unassigned-task-wrap,.actions-view-wrapper").mCustomScrollbar({
            theme:"dark",
            autoHideScrollbar: true,
            autoExpandScrollbar: true
        });

        $( ".unassigned-task-box" ).draggable({
            appendTo: 'body',
            containment: 'window',
            scroll: false,
            cursor: 'move',
            helper: 'clone'
        });
        $(".unassigned-task-box").bind("drag", function(event, ui) {
            ui.helper.css("border", "1px solid #000");
        });

        taskFilter();

        var _dragDropSelector = $( ".task-assigning-table tr td .inv-tsk-box" );
        _dragDropSelector.droppable({
            hoverClass: "ui-drop-hover",
            drop: function(event, ui) {

                /*if($(this).hasClass('un-droppable')){ alert('User has full day work'); return false; }
                 else if(($(this).attr('data-day')-$('#cur_day').val())<-172800){ alert('please select future days.'); return false; }*/
                /*if(($(this).attr('data-day')-$('#cur_day').val())<-172800){ alert('please select future days.'); return false; }*/
                if(($(this).attr('data-day')-$('#cur_day').val())<-604800){ alert('please select future days.'); return false; }
                //above functionality for allowing user to assing task for yesterday.//86400 means a day
                /*else if($(this).attr('data-day')<$('#cur_day').val()){ alert('please select feature days.'); return false; }*/


                $( this ).removeClass( "ui-drop-hover" );
                var $modal = TssLib.openModel({width: 850}, $('#assigningTaskTo').html());
                //mask
                $(".msk_time").mask("Hh:Mm");



                var projectTitle = ui.draggable.find('[name="project-title"]').text(),
                    task_name =  ui.draggable.find('[name="task_name"]').text(),
                    estimatedTime =  ui.draggable.find('[name="estimated-time"]').text(),
                    project_task_id =  ui.draggable.find('[name="project_task_id"]').val(),
                    project_id =  ui.draggable.find('[name="project_id"]').val(),
                    remains = ui.draggable.find('[name="remains"]').text();
                $modal.find('[name="project-title"]').text(projectTitle);
                $modal.find('[name="task-name"]').text(task_name);
                $modal.find('[name="estimated_time"]').text(estimatedTime);
                $modal.find('[name="remains"]').text(remains);
                $modal.find('#c_rem_time').val(remains);

                $modal.find('#project_task_id').val(project_task_id);
                $modal.find('#estimated_time').val(estimatedTime);
                $modal.find('#project_id').val(project_id);

                getPrevTasks($(this).attr('data-user-id'),$(this).attr('data-day'),project_id);


                var _droppableDiv = $(this);
                //alert($(this).attr('data-user-id'));
                $modal.find('#log_day').val($(this).attr('data-day'));
                $modal.find('#assigned_to').val($(this).attr('data-user-id'));
                $modal.find('#user_name').val($(this).attr('data-user-name'));
                $modal.find('#engagedHours').val($(this).attr('data-engaged-time'));
                $modal.find('#day').val($(this).attr('data-day-name'));


                TssLib.ajaxForm({
                    form: $modal.find('#assign_t_user'), callback: function (result) {
                        if (result.data.status) {
                            TssLib.closeModal();
                            loadWeekWorkload(0);
                        }
                        else {
                            TssLib.notify(result.data.message, 'warn', 5);
                        }
                    },
                    before: function (formObj) {
                        $(formObj).data('additionalData', {
                            'log_start_date': $modal.find('[name="log_start_date"]').val(),
                        });
                        $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/addTaskToUser');

                        var user_exiting_time = $('#engagedHours').val();

                        var t1 = $('#allotedHours').val();

                        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(t1);
                        if(!isValid){
                            TssLib.notify('Invalid Time Format', 'error', 5);
                            return false;
                        }

//                        if (parseInt(hmsToSecondsOnly(user_exiting_time) + hmsToSecondsOnly(t1))>28800) {
                        if (parseInt(hmsToSecondsOnly(user_exiting_time) + hmsToSecondsOnly(t1))>36000) {

                            /*TssLib.notify('User alloted time exceeds', 'error', 5);
                             return false;*/
                            /*TssLib.confirm('Delete Confirmation', 'Are you sure to exceeds the alloted time?', function () {
                             $(this).closest('.modal').modal('hide');
                             t1 = t1.replace(/_/g, 0);
                             var t2 = $('#c_rem_time').val();



                             if ((hmsToSecondsOnly(t2) < hmsToSecondsOnly(t1)) || t1 == "00:00") {

                             TssLib.notify('Check allotted Time', 'error', 5);
                             return false;
                             }

                             return true;
                             }, 'Yes', 'No', function(){ return false; });*/
                            var r = confirm("Are you sure to exceeds the alloted time?");
                            if (r == true) {
                                t1 = t1.replace(/_/g, 0);
                                var t2 = $('#c_rem_time').val();



                                if ((hmsToSecondsOnly(t2) < hmsToSecondsOnly(t1)) || t1 == "00:00") {

                                    TssLib.notify('Check allotted Time', 'error', 5);
                                    return false;
                                }

                                return true;
                            } else {
                                return false;
                            }
                        }else{
                            t1 = t1.replace(/_/g, 0);
                            var t2 = $('#c_rem_time').val();



                            if ((hmsToSecondsOnly(t2) < hmsToSecondsOnly(t1)) || t1 == "00:00") {

                                TssLib.notify('Check allotted Time', 'error', 5);
                                return false;
                            }

                            return true;
                        }

                    }
                });


                /*_droppableDiv.html('');
                 var _spanEstimated = $('<span/>', {html:'Estimated : <span>'+ estimatedTime+'</span>' }).appendTo(_droppableDiv),
                 _spanActual = $('<span/>', {html:'Actual :  <span>04</span>' }).appendTo(_droppableDiv),
                 _spanProjects = $('<span/>', {html:'Projects :  <span>01</span>' }).appendTo(_droppableDiv),
                 _spanTasks = $('<span/>', {html:'Tasks :  <span>06</span>' }).appendTo(_droppableDiv),
                 _btttomGrayBg= $('<span/>',{class:'bottom-gray-bg',html:'<span></span>'}).appendTo(_droppableDiv);*/

            },
            over:function(event, ui) {
            },
            out: function( event, ui ) {
            }
        });

    });
    $(window).load(function(){

        $(".task-assigning-table-prnt").mCustomScrollbar({
            theme:"dark",
            scrollButtons:{
                enable:false
            },
            mouseWheel:{ preventDefault: true },
            scrollbarPosition: 'inside',
            autoExpandScrollbar:true,
            theme: 'dark'
        });
    });
    /*   $(".task-assigning-table-prnt").mCustomScrollbar({
     theme:"dark",
     autoHideScrollbar: true,
     autoExpandScrollbar: true
     });*/


</script>