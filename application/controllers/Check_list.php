<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_list extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_id')){
            redirect(WEB_BASE_URL);
        }
        $this->load->model('Project_modal');
        $this->load->model('Check_list_modal');
    }
    public function index(){
        $data['content'] = 'check_list/index';
        $this->load->view('partials/landing',$data);
    }

    public function getCheckListGrid()
    {
        $params = $this->input->get();
        $clientList = $this->Check_list_modal->getCheckListGrid($params);
        echo json_encode($clientList);
    }

    public function addChecklist()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $id = $data['id_checklist'];

        $result = $this->Check_list_modal->checkChecklistName($id, $data['checklist_name']);
        if (empty($result)) {
            if($id == null && $id == '') {
                $dep = $this->Check_list_modal->addChecklist($data);
                $dataStatus = false;
            }else{
                $dep = $this->Check_list_modal->updateChecklist($id,$data);
                $dataStatus = true;
            }
            echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$dataStatus));
        } else {
            echo json_encode(array('status' => false, 'message' => 'Checklist name exist'));
        }
    }

    public function deleteChecklist(){
        $id = $this->input->get('id');
        if($id){
            if(!$this->Check_list_modal->checkTaskCheckListUsed($id)){
                $this->Check_list_modal->deleteChecklist($id);
                echo json_encode(array('status'=>true,'error'=>false,'data'=>[]));
            }else{
                echo json_encode(array('status'=>false,'error'=>true,'message'=>'Already in use.'));
            }
        }else{
            echo json_encode(array('status'=>false,'error'=>true,'message'=>'Id not found'));
        }

    }
}