<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workload extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Workload_model');
        $this->load->model('Project_modal');
        if (!$this->session->userdata('user_id')) {
            redirect(WEB_BASE_URL);
        }
    }

    public function index()
    {

        if ($this->session->userdata('user_id')) {
            $data['content'] = 'workload/workload';
            $data['projectList'] = $this->Workload_model->getDepartmentProject();
            //echo "<pre>"; print_r($data); exit;
            $this->load->view('partials/landing', $data);
        }

    }

    public function workloadTaskList($project_id = '')
    {
        //$this->load->model('Project_modal');
        //$data['content'] = 'workload/workload';

        $params = $this->input->post();
        $taskListData = array();


        if (isset($params['projectIds']) && !empty($params['projectIds'])) {
            $department_ids =  $project_ids = array();
            if(isset($params['projectIds']))
                $project_ids = $params['projectIds'];
            if(isset($params['departmentIds']))
                $department_ids = $params['departmentIds'];

            $taskListData = $this->Workload_model->getProjectTask(array('projectIds'=>$project_ids,'departmentIds'=>$department_ids));
            $timeWorkload = $this->Workload_model->getTimeWorkLoad($project_ids);

            //total department users
            $total_members_taskListData = $this->Workload_model->getDeptProjectTask($project_ids);
            $total_members_timeWorkload = $this->Workload_model->getTimeWorkLoad($project_ids);


            if ($this->session->userdata('user_type_id') != 3) {
                $data['projectTeamList'] = $this->Project_modal->getProjectTeam(array('project_ids'=>$project_ids));
            } else if ($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1) {
                $department_id = $this->session->userdata('department_id');
                $data['projectTeamList'] = $this->Project_modal->getProjectTeam(array('project_ids' => $project_ids, 'department_id' => $department_id));
            } else if ($this->session->userdata('user_type_id') == 3) {
                $department_id = $this->session->userdata('department_id');
                //$data['week_tasks'] = $this->Project_modal->getProjectTeam(array('project_id' => $project_id,'department_id'=>$department_id));
            } else {
                redirect(WEB_BASE_URL);
            }


            $taskWorkload = [];
            $deptTaskWorkload = [];

            foreach ($taskListData as $item) {
                // echo "<pre>"; print_r($item); exit;
                foreach ($timeWorkload as $time) {

                    //echo "<pre>"; print_r($time); exit;
                    if ($item['id_task_workflow'] == $time['task_workflow_id']) {
                        $item['task'][] = $time;
                    }

                }

                $taskWorkload[$item['project_name']][] = $item;
            }


            foreach ($total_members_taskListData as $item) {
                // echo "<pre>"; print_r($item); exit;
                foreach ($total_members_timeWorkload as $time) {

                    //echo "<pre>"; print_r($time); exit;
                    if ($item['id_task_workflow'] == $time['task_workflow_id']) {
                        $item['task'][] = $time;
                    }

                }
                $deptTaskWorkload[] = $item;
            }
            //echo "<pre>"; print_r($deptTaskWorkload); exit;
            if ($this->session->userdata('user_type_id') == 3) {

            }

            //echo "<pre>"; print_r($deptTaskWorkload); exit;
            $data['projectTaskList'] = $taskWorkload;
            $data['deptProjectTaskList'] = $deptTaskWorkload;
            //echo "<pre>"; print_r($deptTaskWorkload); exit;
            //echo "<pre>"; print_r($deptTaskWorkload); exit;

            $data['startDate'] = strtotime('monday this week');
            $this->load->view('workload/workload_tasklist', $data);
        }


    }

    public function ajaxWorkloadSave()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if ($data) {
            $_POST = $data;
        }
        $data = $this->input->post();
       // echo "<pre>"; print_r($data); exit;

        foreach ($data['data'] as $item) {

            $task_workflow_id = $this->Workload_model->createTaskWorkFlow($item);
            $task_bach = array();
            $day = 1;
            foreach ($item['task'] as $task) {
                $obj = [];
                $obj['task_workflow_id'] = $task_workflow_id;
                $obj['day_id'] = $day;
                $obj['time'] = $task['allotted_time'];
                $obj['date'] = $task['date'];
                $obj['project_id'] = $task['project_id'];
                $task_bach[] = $obj;
                $day++;
            }
            $this->Workload_model->createTimeWorkFlow($task_bach);
        }
        echo 1;
    }
}
