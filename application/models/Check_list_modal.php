<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Check_list_Modal extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    public function getCheckListGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = $paramArr['sidx'] != ''?$paramArr['sidx']:'id_checklist';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and cl.checklist_name LIKE '%".$whereParam."%' "; }

        $whereClause = "where true ".$whereParam;

        $SQL = "SELECT cl.*,GROUP_CONCAT(dcl.department_id) as dept from checklist cl
                LEFT JOIN department_checklist dcl on cl.id_checklist = dcl.checklist_id
                $whereClause GROUP BY cl.id_checklist order by $sortField $sortOrder $optLimit";

        $SQLCount = "SELECT count(*)  from checklist cl LEFT JOIN department_checklist dcl on cl.id_checklist = dcl.checklist_id
                $whereClause GROUP BY cl.id_checklist";
        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function addChecklist($data){
        $department_id = $data['department_id'];
        unset($data['department_id']);
        $this->db->insert('checklist', $data);
        $checklist_id =  $this->db->insert_id();
        $departmentArry = array();
        if(!is_array($department_id)){ $department_id = explode(',',$department_id);}
        foreach($department_id as $item ){
            $departmentArry[] = array('checklist_id'=>$checklist_id,'department_id'=>$item);
        }
        $this->db->insert_batch('department_checklist', $departmentArry);

    }

    public function updateChecklist($id,$data){
        $department_id = $data['department_id'];
        unset($data['department_id']);

        $this->db->where('id_checklist', $id);
        $this->db->update('checklist', $data);

        $this->db->where('checklist_id', $id);
        $this->db->delete('department_checklist');

        $departmentArry = array();
        if(!is_array($department_id)){ $department_id = explode(',',$department_id);}
        foreach($department_id as $item ){
            $departmentArry[] = array('checklist_id'=>$id,'department_id'=>$item);
        }
        $this->db->insert_batch('department_checklist', $departmentArry);

    }

    public function checkChecklistName($id,$name){
        $this->db->select('*');
        if($id != ''){
            $this->db->where('id_checklist !=', $id);
        }
        $this->db->where('checklist_name', $name);
        $this->db->from('checklist');
        $query = $this->db->get();
        return $query->result_array();

    }

    public function checkTaskCheckListUsed($id){
        $this->db->select('*');
        $this->db->from('user_checklist');
        $this->db->where('checklist_id', $id);

        $query = $this->db->get();
        return count($query->result())>0?true:false;
    }

    public function deleteChecklist($id){
        $this->db->delete('department_checklist', array('checklist_id'=>$id));
        $this->db->delete('checklist', array('id_checklist'=>$id));
        return true;
    }

}