<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Task list</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Task list
                    </h3>
                </div>
                <div class="padding0">
                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <div class="col-sm-12 padding0">
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container mt15">
                                        <select id="projectDropdown" name="project_id[]"
                                                class="form-control rval" multiple="multiple"
                                                includeselectalloption="true" enablefiltering="true">
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-12">Project:</label>
                                </div>
                            </div>
                            <?php if($this->session->userdata('user_type_id') == 2){  ?>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="departmentDropdown" name="department[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Department:</label>
                                    </div>
                                </div>
                            <?php } ?>
                            <!--<div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container mt15">
                                        <select id="moduleDropdown" name="department[]"
                                                class="form-control rval" multiple="multiple"
                                                includeselectalloption="true" enablefiltering="true">
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-12">Modules:</label>
                                </div>
                            </div>-->
                            <?php if($this->session->userdata('user_type_id') == 2 || $this->session->userdata('is_lead') == 1){  ?>
                                <div class="col-sm-3">
                                    <div class="form-group padding0">
                                        <div class="multi-select-container mt15">
                                            <select id="employeeDropdown" name="user_id[]"
                                                    class="form-control rval" multiple="multiple"
                                                    includeselectalloption="true" enablefiltering="true">
                                            </select>
                                        </div>
                                        <label class="control-label col-sm-12">Employee:</label>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container">
                                        <div class="input_container">
                                            <input type="text" placeholder="dd/mm/yyyy" id="sdate" value="<?=date('d/m/Y')?>"
                                                   name="sdate" class="rval form-control tssDatepicker">
                                        </div>
                                    </div>

                                    <label class="control-label col-sm-12">Start Date:</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container">
                                        <div class="input_container">
                                            <input type="text" placeholder="dd/mm/yyyy" id="edate"  value="<?=date('d/m/Y')?>"
                                                   name="edate" class="rval form-control tssDatepicker">
                                        </div>
                                    </div>

                                    <label class="control-label col-sm-12">End Date:</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container mt15">
                                        <select id="statusDropdown" name="status[]"
                                                class="form-control rval" enablefiltering="true">
                                            <option value="select">Select</option>
                                            <option value="new">New</option>
                                            <option value="progress">Progress</option>
                                            <option value="completed">Completed</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-12">Status:</label>
                                </div>
                            </div>
                            <script>
                                var level = 0;
                                <?php if($this->session->userdata('user_type_id') == 2){
                                    echo 'level=0;';
                                }else if($this->session->userdata('is_lead') == 1){
                                    echo 'level=1;';
                                }else {
                                    echo 'level=2;';
                                } ?>
                            </script>
                            <div class="col-sm-3">
                                <!--<button class="button button-common module mt20"  onclick="resultType(0);" > Search </button>
                                <button class="button button-common module mt20"  onclick="resultType(1);" > Excel </button>-->
                                <input type="button" class="button button-common module mt20"
                                       onclick="validateReportsForm(level);" value="Search">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth bg-white padding0">
                        <div class="grid-details-table">
                            <div class="grid-details-table-header">
                                <h3>&nbsp;</h3>
                                <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="grid-details-table-content clearfix padding0">
                                <div class="tbl_wrapper border0">
                                    <table id="project-list" class="table-responsive"></table>
                                    <div id="project-list-page"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="taskDetails" class="modal-wrapper" >
        <div id="detailsContent">

        </div>
    </div>
    <script>
        var $refTblgrid = $('#project-list');
        var $counter = 0;
        $(function(){
            var d = new Date();
            d.setDate(d.getDate() - 1);
            $('#date').val(d.format('d/m/Y'));
            TssLib.datepickerBinder('#reports_frm');

            postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var projects = result.data;
                        TssLib.populateSelect($('#projectDropdown'), {
                            success: true,
                            data: projects
                        }, 'project_name', 'id_project');
                        TssLib.selectAllOptions('#projectDropdown');
                        <?php if($this->session->userdata('user_type_id') == 2){ ?>
                        loadDepartments();
//                        loadModules();
                        <?php }else if($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1){ ?>
                        loadUsersWithoutDeptt();
//                        loadModules();
                        <?php }else{ ?>
//                        loadModules();
                        loadJQGrid(2);
                        <?php } ?>
                    }
                }
            });

            /*$refTblgrid.jqGrid({
             url: TssConfig.TT_SERVICE_URL + 'Project/userTaskGrid',
             multiselect: false,
             datatype: "json",
             mtype:"POST",
             sortorder: "desc",
             extSearchField: '.searchInput',
             colNames: ['Project Name', 'User Name', 'Task Name', 'Allotted Time', 'Additional Time','Log time', 'Status', 'project_task_id', 'user_id'],
             colModel: [
             {name: 'project_name', index: 'project_name', hidden: false},
             {name: 'user_name', index: 'user_name', hidden: false, key: false},
             {name: 'task_name', index: 'task_name', hidden: false, key: false},
             {name: 'allotted_time', index: 'allotted_time', hidden: false, key: false},
             {name: 'additional_time', index: 'additional_time', hidden: false, key: false},
             {name: 'logtime', index: 'logtime', hidden: false, key: false, sortable: false},
             {name: 'status', index: 'status', hidden: false, key: false},
             {name: 'project_task_id', index: 'project_task_id', hidden: true, key: false},
             {name: 'user_id', index: 'user_id', hidden: true, key: false}
             ],
             pager: 'project-list-page'
             }).navGrid('#project-list-page', {
             edit: false, add: false, del: false, search: false, refresh: true
             });*/

            <?php if($this->session->userdata('user_type_id') == 2){ ?>

            $('#projectDropdown').on('change', function () {
                if ( $( "#projectDropdown" ).val()  == null ) { return false; }
                loadDepartments();
//                loadModules();
            });

            $('#departmentDropdown').on('change', function () {
                if (  $( "#departmentDropdown" ).val()  == null ) { return false; }
                loadUsers();
            });
            <?php }else if($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1){ ?>

            $('#projectDropdown').on('change', function () {
                if ( $( "#projectDropdown" ).val()  == null ) { return false; }
                loadUsersWithoutDeptt();
//                loadModules();
            });
            <?php }else{ ?>

            $('#projectDropdown').on('change', function () {
                if ( $( "#projectDropdown" ).val()  == null ) { return false; }
//                loadModules();
            });
            <?php } ?>
        });
        function loadJQGrid(type){
            $('.tssDatepicker ').removeClass('err-bg');
            if(type == 0){
                $('.tssDatepicker ').removeClass('err-bg');
                $('#projectDropdown').removeClass('err-bg');
                $('#employeeDropdown').removeClass('err-bg');
                /*$('#moduleDropdown').removeClass('err-bg');*/
                $('#departmentDropdown').removeClass('err-bg');
                $('.error-msg').remove();

                sdate = $('#sdate').val();
                edate = $('#edate').val();
                project = $('#projectDropdown').val();
                user_id = $('#employeeDropdown').val();
                /*module_id = $('#moduleDropdown').val();*/
                department_id = $('#departmentDropdown').val();
                status = $('#statusDropdown').val();
                flag = 0;

                /*if (sdate == '') {
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (edate == '') {
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }*/
                if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (project == '') {
                    $('#projectDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (user_id == '') {
                    $('#employeeDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                /*if (module_id == '') {
                 $('#moduleDropdown').addClass('err-bg');
                 $('.multi-select-container').before('<span class="error-msg">Required</span>');
                 flag++;
                 }*/
                if (department_id == '') {
                    $('#departmentDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                postData = {
                    'project' : function(){ return project; },
                    'user_id' : function(){ return user_id; },
                    /*'module_id' : function(){ return module_id; },*/
                    'department_id' : function(){ return department_id; },
                    'sdate' : function(){ return sdate; },
                    'edate' : function(){ return edate; },
                    status: function(){ return status; },
                };
            }else if(type == 1){
                $('#date').removeClass('err-bg');
                $('#projectDropdown').removeClass('err-bg');
                $('#employeeDropdown').removeClass('err-bg');
                /*$('#moduleDropdown').removeClass('err-bg');*/
                $('.error-msg').remove();

                sdate = $('#sdate').val();
                edate = $('#edate').val();
                project = $('#projectDropdown').val();
                user_id = $('#employeeDropdown').val();
                /*module_id = $('#moduleDropdown').val();*/
                status = $('#statusDropdown').val();
                var flag = 0;

                /*if (sdate == '') {
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (edate == '') {
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }*/
                if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (project == '') {
                    $('#projectDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (user_id == '') {
                    $('#employeeDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                /*if (module_id == '') {
                 $('#moduleDropdown').addClass('err-bg');
                 $('.multi-select-container').before('<span class="error-msg">Required</span>');
                 flag++;
                 }*/
                postData = {
                    'project' : function(){ return project; },
                    'user_id' : function(){ return user_id; },
                    /*'module_id' : function(){ return module_id; },*/
                    'sdate' : function(){ return sdate; },
                    'edate' : function(){ return edate; },
                    status: function(){ return status; },
                };
            }else if(type == 2){
                $('#date').removeClass('err-bg');
                $('#projectDropdown').removeClass('err-bg');
                $('.error-msg').remove();

                sdate = $('#sdate').val();
                edate = $('#edate').val();
                /*module_id = $('#moduleDropdown').val();*/
                project = $('#projectDropdown').val();
                status = $('#statusDropdown').val();
                flag = 0;

                /*if (sdate == '') {
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (edate == '') {
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }*/
                if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (project == '') {
                    $('#projectDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                postData = {
                    'project' : function(){ return project; },
                    /*'module_id' : function(){ return module_id; },*/
                    'sdate' : function(){ return sdate; },
                    'edate' : function(){ return edate; },
                    status: function(){ return status; },
                };
            }

            if (flag == 0) {
                if($counter==0){
                    $refTblgrid.jqGrid({
                        url: TssConfig.TT_SERVICE_URL + 'Project/userTaskGrid',
                        multiselect: false,
                        datatype: "json",
                        mtype:"POST",
                        sortorder: "desc",
                        postData: postData,
                        extSearchField: '.searchInput',
//                        colNames: ['Project Name', 'User Name', 'Task Name', 'Module Name', 'Allotted Time', 'Additional Time','Log time', 'Date', 'Status', 'project_task_id', 'user_id'],
                        colNames: ['Project Name', 'User Name', 'Task Name', 'Allotted Time', 'Additional Time','Log time', 'Date', 'Status', 'project_task_id', 'user_id'],
                        colModel: [
                            {name: 'project_name', index: 'project_name', hidden: false},
                            {name: 'user_name', index: 'user_name', hidden: false, key: false},
                            /*{name: 'task_name', index: 'task_name', hidden: false, key: false},*/
                            {
                                name: 'task_name', index: 'task_name', formatter: function (c, o, d) {
                                //return d.project_task_name;
                                //return '<a title="' + d.project_task_name + '" href="javascript:;" >' + d.project_task_name + '</a>';
                                <?php if($this->session->userdata('user_type_id') == 2){ ?>
                                    return '<a href="javascript:;" onclick="openTaskMemeberModal(' + d.project_task_id + ')">' + d.task_name + '</a>';
                                <?php } else if($this->session->userdata('user_type_id') == 3 && ($this->session->userdata('is_lead') == 1 || $this->session->userdata('is_manager') == 1)){ ?>
                                    return '<a href="javascript:;" onclick="openTaskMemeberModal(' + d.project_task_id + ')">' + d.task_name + '</a>';
                                <?php } else { ?>
                                    return d.task_name;
                                <?php } ?>
                            }
                            },
                            /*{name: 'module_name', index: 'module_name', hidden: false, key: false},*/
                            {name: 'allotted_time', index: 'allotted_time', hidden: false, key: false},
                            {name: 'additional_time', index: 'additional_time', hidden: false, key: false},
                            {name: 'logtime', index: 'logtime', hidden: false, key: false, sortable: false},
                            {name: 'created_date_time', index: 'created_date_time', hidden: false, key: false, sortable: false},
                            {name: 'status', index: 'status', hidden: false, key: false},
                            {name: 'project_task_id', index: 'project_task_id', hidden: true, key: false},
                            {name: 'user_id', index: 'user_id', hidden: true, key: false}
                        ],
                        pager: 'project-list-page'
                    }).navGrid('#project-list-page', {
                        edit: false, add: false, del: false, search: false, refresh: true
                    });
                    $counter++;
                    validateReportsForm(level)
                }
            }

        }
        function loadDepartments() {
            var project = $('#projectDropdown').val();
            project = project.join('-');
            postJsonAsyncWithBaseUrl("Project/getProjectDepartments/" + project, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var departments = result.data.data;
                        TssLib.populateSelect($('#departmentDropdown'), {
                            success: true,
                            data: departments
                        }, 'department_name', 'id_department');
                        TssLib.selectAllOptions('#departmentDropdown');
                        loadUsers();
                    }
                }
            });
        }
        /*function loadModules() {
         var project = $('#projectDropdown').val();
         project = project.join('-');
         postJsonAsyncWithBaseUrl("Project/getProjectModuleList/" + project, {}, {
         jsonContent: true,
         callback: function (result) {
         if (result.data != null) {
         var departments = result.data.data;
         TssLib.populateSelect($('#moduleDropdown'), {
         success: true,
         data: departments
         }, 'module_name', 'id_project_module');
         TssLib.selectAllOptions('#moduleDropdown');
         }
         }
         });
         }*/
        function loadUsers() {
            var deptt = $('#departmentDropdown').val();
            deptt = deptt.join(',');
            var project = $('#projectDropdown').val();
            project = project.join(',');
            postJsonAsyncWithBaseUrl("Project/getUserByDepartmentProject", {'project_id':project , 'department_id': deptt}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var user = result.data.data;
                        TssLib.populateSelect($('#employeeDropdown'), {
                            success: true,
                            data: user
                        }, 'email', 'id_user');
                        TssLib.selectAllOptions('#employeeDropdown');
                        var level = 0;
                        <?php if($this->session->userdata('user_type_id') == 2){
                        echo 'level=0;';
                    }else if($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1){
                        echo 'level=1;';
                    }else {
                        echo 'level=2;';
                    } ?>
                        loadJQGrid(level);
//                        validateReportsForm(level);
                    }
                }
            });
        }
        function loadUsersWithoutDeptt(){
            var project = $('#projectDropdown').val();
            if(!project){
                project =0;
            }
            else
                project = project.join(',');

            var postData = {};
            <?php if($this->session->userdata('department_id')){ ?>
            postData = {'project_id':project , 'department_id': '<?=$this->session->userdata('department_id')?>'};
            <?php } else { ?>
            postData = {'project_id':project }
            <?php } ?>

            postJsonAsyncWithBaseUrl("Project/getUserByDepartmentProject", postData, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var user = result.data.data;
                        TssLib.populateSelect($('#employeeDropdown'), {
                            success: true,
                            data: user
                        }, 'email', 'id_user');
                        TssLib.selectAllOptions('#employeeDropdown');

                        var level = 0;
                        <?php if($this->session->userdata('user_type_id') == 2){
                        echo 'level=0;';
                    }else if($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1){
                        echo 'level=1;';
                    }else {
                        echo 'level=2;';
                    } ?>
//                        validateReportsForm(level);
                        loadJQGrid(level);
                    }
                }
            });
        }
        function validateReportsForm(type) {
            var postData = {};
            var sdate, edate, project, user_id, module_id, department_id,flag, status;
            $('.tssDatepicker ').removeClass('err-bg');
            if(type == 0){
                $('#date').removeClass('err-bg');
                $('#projectDropdown').removeClass('err-bg');
                $('#employeeDropdown').removeClass('err-bg');
                /*$('#moduleDropdown').removeClass('err-bg');*/
                $('#departmentDropdown').removeClass('err-bg');
                $('.error-msg').remove();

                sdate = $('#sdate').val();
                edate = $('#edate').val();
                project = $('#projectDropdown').val();
                user_id = $('#employeeDropdown').val();
                /*module_id = $('#moduleDropdown').val();*/
                department_id = $('#departmentDropdown').val();
                status = $('#statusDropdown').val();
                flag = 0;

                /*if (sdate == '') {
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (edate == '') {
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }*/
                if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (project == '') {
                    $('#projectDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (user_id == '') {
                    $('#employeeDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                /*if (module_id == '') {
                 $('#moduleDropdown').addClass('err-bg');
                 $('.multi-select-container').before('<span class="error-msg">Required</span>');
                 flag++;
                 }*/
                if (department_id == '') {
                    $('#departmentDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                postData = {
                    'project' : function(){ return project; },
                    'user_id' : function(){ return user_id; },
                    /*'module_id' : function(){ return module_id; },*/
                    'department_id' : function(){ return department_id; },
                    'sdate' : function(){ return sdate; },
                    'edate' : function(){ return edate; },
                    status: function(){ return status; },
                };
            }else if(type == 1){
                $('#date').removeClass('err-bg');
                $('#projectDropdown').removeClass('err-bg');
                $('#employeeDropdown').removeClass('err-bg');
                /*$('#moduleDropdown').removeClass('err-bg');*/
                $('.error-msg').remove();

                sdate = $('#sdate').val();
                edate = $('#edate').val();
                project = $('#projectDropdown').val();
                user_id = $('#employeeDropdown').val();
                /*module_id = $('#moduleDropdown').val();*/
                status = $('#statusDropdown').val();
                var flag = 0;

                /*if (sdate == '') {
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (edate == '') {
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }*/
                if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (project == '') {
                    $('#projectDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (user_id == '') {
                    $('#employeeDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                /*if (module_id == '') {
                 $('#moduleDropdown').addClass('err-bg');
                 $('.multi-select-container').before('<span class="error-msg">Required</span>');
                 flag++;
                 }*/
                postData = {
                    'project' : function(){ return project; },
                    'user_id' : function(){ return user_id; },
                    /*'module_id' : function(){ return module_id; },*/
                    'sdate' : function(){ return sdate; },
                    'edate' : function(){ return edate; },
                    status: function(){ return status; },
                };
            }else if(type == 2){
                $('#date').removeClass('err-bg');
                $('#projectDropdown').removeClass('err-bg');
                /*$('#moduleDropdown').removeClass('err-bg');*/
                $('.error-msg').remove();

                sdate = $('#sdate').val();
                edate = $('#edate').val();
                project = $('#projectDropdown').val();
                /*module_id = $('#moduleDropdown').val()*/;
                status = $('#statusDropdown').val();
                flag = 0;

                /*if (sdate == '') {
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (edate == '') {
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }*/
                if((sdate == '' && edate != '') || (sdate != '' && edate == '')){
                    $('#sdate').addClass('err-bg');
                    $('#sdate').before('<span class="error-msg">Required</span>');
                    $('#edate').addClass('err-bg');
                    $('#edate').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                if (project == '') {
                    $('#projectDropdown').addClass('err-bg');
                    $('.multi-select-container').before('<span class="error-msg">Required</span>');
                    flag++;
                }
                postData = {
                    'project' : function(){ return project; },
                    /*'module_id' : function(){ return module_id; },*/
                    'sdate' : function(){ return sdate; },
                    'edate' : function(){ return edate; },
                    status: function(){ return status; },
                };
            }
            type = 0;

            if (flag == 0) {
                if (type == 0) {
                    var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
                    myPostData = postData;
                    $refTblgrid.setGridParam({'postData': myPostData});
                    $('#project-list').trigger("reloadGrid");

                    return false;
                }
                else {
                    $('#reports_frm').submit();
                }
            }
        }

        function openTaskMemeberModal(taskId){
            var $modal = TssLib.openModel({width: 1024},$('#taskDetails').html());

            $.post("<?=site_url('Project/taskFullDetails')?>" , {'task_id':taskId }, function( data ) {
                $modal.find('#detailsContent').html(data);
            });
        }
    </script>