<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Project_modal');
	}

	public function index()
	{
		if($this->session->userdata('user_id'))
		{
            if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==1){ redirect(WEB_BASE_URL.'index.php/Project/weekWorkload'); }
            if($this->session->userdata('user_type_id')==2 ){redirect(WEB_BASE_URL.'index.php/Project/'); }
            if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')!=1){ redirect(WEB_BASE_URL.'index.php/Project/fullCalendar'); }
		}
		else
		{
			$data['content'] = 'index';
			$data['left_menu'] = '';
			$this->load->view('partials/landing',$data);
		}

	}
    public function userList(){
        $data['content'] = 'users/index';
        $this->load->view('partials/landing',$data);
    }
    public function getUsersGrid(){
        $params = $this->input->get();
        $list = $this->User_model->getUsersGrid($params);
		foreach($list['rows'] as $key=>$val){
//			print_r($val);exit();
			$data = $this->User_model->isUserLead(array('user_id'=>$val->id_user, 'is_lead'=>'1'));
			if(count($data)>0){
				if($data[0]['is_lead']==1)
					$val->is_lead = $data[0]['is_lead'];
			}else{
				$val->is_lead = 0;
			}
		}
        echo json_encode($list);
    }

	public function getUserById(){
		$params = $this->input->get();
		$list = $this->User_model->getUser($params);
		echo json_encode($list);
	}

	public function getUsersByType(){
		$params = $this->input->get();
		$usersList = $this->User_model->getUsersByType($params);
		echo json_encode($usersList);
	}

    public function getAllUsers(){
        $usersList = $this->User_model->getAllUsers(array('user_type_id' => 3));
        echo json_encode($usersList);
    }
    public function getDepUsers(){
        $depId = $this->session->userdata('department_id');
        $usersList = $this->User_model->getDepUsers($depId);
        echo json_encode($usersList);
    }
	public function login()
	{//echo "<pre>"; print_r($_POST); exit;
		if(isset($_POST) && !empty($_POST))
		{
			if(!isset($_POST['domain']) || $_POST['domain']!='thresholdsoft.com'){
				echo json_encode(array('response' => 0,'data' => 'Unauthorised user')); exit;
			}

			$data = $_POST;
			$check_user = $this->User_model->getUser(array('email' => $data['emails'][0]['value'],'status' => 1));
			//echo "<pre>"; print_r($check_user); exit;
			if(empty($check_user)){
				/*$user_data = array(
					'user_type_id' => 3,
					'email' => $data['emails'][0]['value'],
					'user_image' => $data['image']['url'],
					'gmail_id' => $data['id']
				);

				$user_id = $this->User_model->addUser($user_data);
				$user_type_id = 3;
				$user_name = $data['displayName'];*/
				echo json_encode(array('response' => 0,'data' => 'Unauthorised user')); exit;
			}
			else
			{
				$user_id = $check_user[0]['id_user'];
				$user_type_id = $check_user[0]['user_type_id'];
				$is_manager = $check_user[0]['is_manager'];

				$this->User_model->updateUser(array(
					'id_user' => $check_user[0]['id_user'],
					'user_image' => $data['image']['url']
				));
				$user_name = $check_user[0]['first_name'].' '.$check_user[0]['last_name'];
			}

			$login = array(
				'username' => $user_name,
				'email' => $data['emails'][0]['value'],
				'user_id' => $user_id,
				'user_type_id' => $user_type_id,
				'user_image' => $data['image']['url'],
				'department_id' => '',
				'is_lead' => '',
				'is_manager' => isset($is_manager)?$is_manager:0
			);

			if(!empty($check_user) && $user_type_id==3){
				$departments = $this->User_model->getUserDepartments(array('user_id' => $user_id));
				if(!empty($departments))
				{
					if(count($departments)==1)
					{
						$login['department_name'] = $departments[0]['department_name'];
						$login['department_id'] = $departments[0]['department_id'];
						$login['is_lead'] = $departments[0]['is_lead'];

						$login['department_id_array'] = 0;
						$login['is_lead_array'] = 0;
					}
					else
					{
						$login['department_name'] = $departments[0]['department_name'];
						$login['department_id'] = $departments[0]['department_id'];
						$login['is_lead'] = $departments[0]['is_lead'];

						$login['department_name_array'] = array_map(function($i){ return $i['department_name']; },$departments);
						$login['department_id_array'] = array_map(function($i){ return $i['department_id']; },$departments);
						$login['is_lead_array'] = array_map(function($i){ return $i['is_lead']; },$departments);
					}
				}

				/*$login['department_id'] = $check_user[0]['department_id'];
				$login['is_lead'] = $check_user[0]['is_lead'];*/
			}

			$this->session->set_userdata($login);
			echo json_encode(array('response' => 1,'data' => '')); exit;
		}
	}
	public function login1()
	{//echo "<pre>"; print_r($_POST); exit;
		if(isset($_POST) && !empty($_POST))
		{
			if(!isset($_POST['domain']) || $_POST['domain']!='thresholdsoft.com'){
				echo json_encode(array('response' => 0,'data' => 'Unauthorised user')); exit;
			}

			$data = $_POST;
			$check_user = $this->User_model->getUser(array('email' => $data['emails'][0]['value']));
			if(empty($check_user)){
				/*$user_data = array(
					'user_type_id' => 3,
					'email' => $data['emails'][0]['value'],
					'user_image' => $data['image']['url'],
					'gmail_id' => $data['id']
				);

				$user_id = $this->User_model->addUser($user_data);
				$user_type_id = 3;
				$user_name = $data['displayName'];*/
				echo json_encode(array('response' => 0,'data' => 'Unauthorised user')); exit;
			}
			else
			{
				$user_id = $check_user[0]['id_user'];
				$user_type_id = $check_user[0]['user_type_id'];

				$this->User_model->updateUser(array(
					'id_user' => $check_user[0]['id_user'],
					'user_image' => $data['image']['url']
				));
				$user_name = $check_user[0]['first_name'].' '.$check_user[0]['last_name'];
			}

			$login = array(
				'username' => $user_name,
				'email' => $data['emails'][0]['value'],
				'user_id' => $user_id,
				'user_type_id' => $user_type_id,
				'user_image' => $data['image']['url'],
				'department_id' => '',
				'is_lead' => ''
			);

			if(!empty($check_user)){
				$login['department_id'] = $check_user[0]['department_id'];
				$login['is_lead'] = $check_user[0]['is_lead'];
			}

			$this->session->set_userdata($login);
			echo json_encode(array('response' => 1,'data' => '')); exit;
		}
	}

	public function addUser(){
		$data = json_decode(file_get_contents("php://input"), true);
		if($data){ $_POST = $data; }
		$data = $this->input->post();
        if($data['id_user']){
            $email = explode('@',$data['email']);
            $email = explode('.',$email[1]);
            if($email[0] != 'thresholdsoft'){ echo json_encode(array('status' => false, 'message' => 'not valid Email')); return false;}
        }
		$id = $data['id_user'];
		$data['user_type_id'] = 3;
		if($id == null && $id == '') { unset($data['id_user']); }
		$result = $this->User_model->checkEmail($data);
		if (empty($result)) {
			if($id == null && $id == '') {
				$userRes = $this->User_model->addUser($data);
			}else{
				$userRes = $this->User_model->updateUser($data);
			}
			echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$userRes));
		} else {
			echo json_encode(array('status' => false, 'message' => 'User email already exist '));
		}
	}
	public function addUserData(){
		$data = json_decode(file_get_contents("php://input"), true);
		if($data){ $_POST = $data; }
		$data = $this->input->post();
        if($data['id_user']){
            $email = explode('@',$data['email']);
            $email = explode('.',$email[1]);
            if($email[0] != 'thresholdsoft'){ echo json_encode(array('status' => false, 'message' => 'not valid Email')); return false;}
        }
		$id = $data['id_user'];
		$data['user_type_id'] = 3;
		if($id == null && $id == '') { unset($data['id_user']); }
		$result = $this->User_model->checkEmail($data);
		if (empty($result)) {
			if($id == null && $id == '') {
				$userRes = $this->User_model->addUserData($data);
			}else{
				$userRes = $this->User_model->updateUserData($data);
			}
			if(isset($data['modules'])){
				$this->User_model->deleteUserModuleData($userRes);
				$userRes = $this->User_model->addUserModuleData($data, $userRes);
			}
			echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$userRes));
		} else {
			echo json_encode(array('status' => false, 'message' => 'User email already exist '));
		}
	}

	public function logout() {

		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('user_type_id');

		redirect(WEB_BASE_URL);
	}

	public function profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['user'] = $this->User_model->getUser(array('id_user' => $this->session->userdata('user_id')));
			$data['job_role'] = $this->User_model->getJobRole();
			$data['content'] = 'profile';
			$this->load->view('partials/landing',$data);
		}
		else{
			redirect(WEB_BASE_URL);
		}

	}

	public function updateProfile()
	{
		if($this->session->userdata('user_id'))
		{
			$data = array(
				'id_user' => $this->session->userdata('user_id'),
				'first_name' => $_POST['first_name'],
				'last_name' => $_POST['last_name'],
				'gender' => $_POST['gender'],
				'phone_number' => $_POST['phone_number'],
				'job_role_id' => $_POST['job_role']
			);
			$this->User_model->updateUser($data);
			$this->session->set_userdata('message','Profile updated successfully.');
			redirect(WEB_BASE_URL.'index.php/User/profile');
		}
		else{
			redirect(WEB_BASE_URL);
		}
	}
    public function changeDepartment(){
        $data = json_decode(file_get_contents("php://input"), true);
        if ($data) {
            $_POST = $data;
        }
        $data = $this->input->post();
        if (empty($data)) {
            $result = array('status' => FALSE, 'error' => 'Invalid Data', 'data' => '');
            echo json_encode($result); exit;
        }
        $result = $this->User_model->updateUserDepartment($data);
        echo json_encode(array('status' => true, 'message' => '', 'data' => $result));
    }

	public function deleteUserById()
	{
		$data = json_decode(file_get_contents("php://input"), true);
		if($data){ $_POST = $data; }
		$data = $this->input->post();
		if($this->User_model->checkUserAssigned($data['user_id'])){
			$status = $this->User_model->deleteUserById($data['user_id']);
			if($status){
				echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'User deleted'));
			}else{
				echo json_encode(array('status'=>false,'error'=>true,'message'=>''));
			}
		}else{
			echo json_encode(array('status'=>false,'error'=>true,'message'=>'User already assigned to a project', 'data'=>[]));
		}

	}

	public function userModule(){
		$data['content'] = 'users/user_module';
		$this->load->view('partials/landing',$data);
	}

	public function getUsersModuleGrid(){
		$params = $this->input->get();
		$list = $this->User_model->getUsersModuleGrid($params);
		echo json_encode($list);
	}

	public function addUserModule(){
		$data = json_decode(file_get_contents("php://input"), true);
		if($data){ $_POST = $data; }
		$data = $this->input->post();
//		echo '<pre>'; print_r($data); exit;

		$id = $data['id_module'];
		if($id == null && $id == '') { unset($data['id_module']); }
		$result = $this->User_model->checkDuplicateUserModule($data);
//		echo '<pre>'; var_dump($result); exit;

		if ($result==false) {
			$data['module_key'] = $this->createUserModuleKey($data);
			if($id == null && $id == '') {
				$userRes = $this->User_model->addUserModule($data);
			}else{
				$userRes = $this->User_model->updateUserModule($id , $data);
			}
			echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$userRes));
		} else {
			echo json_encode(array('status' => false, 'message' => 'Module name already exist '));
		}
	}

	private function createUserModuleKey($data){
		if(isset($data['module_name'])){
			if(preg_match('/\//',$data['module_name'])){
				$a = explode('/', $data['module_name']);
				$data['module_name'] = $a[count($a)];
			}
			return strtolower(str_replace(" ","-",$data['module_name']));
		}
	}
	public function getAllUserModule(){
		$data = json_decode(file_get_contents("php://input"), true);
		if($data){ $_POST = $data; }
		$data = $this->input->post();

		$data['module'] = $this->User_model->getAllUserModule();
		if($data['user_id'] != 0){
			foreach($data['module'] as $k=>$v){
				$isChecked = $this->User_model->getUserSpecificModule(array('id_module' => $v->id_module, 'user_id'=> $data['user_id']));
				if(count($isChecked)>0){
					$isChecked = $isChecked[0]->allow_access;
					if($isChecked == 1){
						$data['module'][$k]->checked = true;
					}else{
						$data['module'][$k]->checked = false;
					}
				}else{
					$data['module'][$k]->checked = false;
				}
			}
		}
		$data=$this->load->view('modals/userModules',$data, TRUE);
		$this->output->set_output($data);
	}

	public function getUserNavigation(){
		if($this->session->userdata('user_id')){
			$userId = $this->session->userdata('user_id');
			$res = $this->User_model->getUserNavigation(array('user_id' => $userId));
			echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$res));
		}
	}

	public function getManagerList(){
		$usersList = $this->User_model->getUser(array('manager' => 1, 'status' => 1));
		echo json_encode(array('status'=>TRUE,'error'=>false,'message'=>'','data'=>$usersList));
	}
}
