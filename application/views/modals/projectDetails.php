<?php
$temp = [];
?>
<style>
    .highcharts-container{width:100% !important; height:100% !important;}
</style>
<div class="col-sm-12 clearfix pr0">
    <div class="project-details-head clearfix">
        <ul>
            <li><span class="blue"><?=$projectDetails['project_name']?></span><span> Name</span></li>
            <li><span><?=$projectDetails['project_start_date']?></span><span>Start Date</span></li>
            <li><span><?=$projectDetails['project_end_date']?></span><span>Delivery Date</span></li>
            <li><span><?=$projectDetails['balanceDays']?>/<b><?=$projectDetails['totalDays']?></b></span><span># Days</span></li>
            <li><span><?=$projectDetails['member']?> Members</span><span>Team</span></li>
            <li></li>
            <!--<li><span> <img src="<?/*= WEB_BASE_URL */?>images/project-overview-graph1.jpg"/></span></li>-->
        </ul>
    </div>
    <div class="project-details-grpah-widget pt10 clearfix">
        <div class="col-70">
            <?php foreach($projectDepartmentStatus as $departmentStatus){
                /*foreach($departmentStatus as $status){*/ ?>
                <div class="col-25">
                    <div class="prj-inv-graph" >
                        <div class="prj-inv-graph-head">
                            <a href="javascript:;">
                                <h4><?= $departmentStatus['department_name'] ?></h4>
                                <Span><?= $departmentStatus['actual_time']!=''?$departmentStatus['actual_time']:'00:00' ?> Hours</Span>
                            </a>
                        </div>
                        <div class="prj-inv-graph-content">
                            <div data-graphdata = '<?=json_encode($departmentStatus)?>' class="projectUxdesignInfo" style="height: 135px;margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            <?php   } ?>
        </div>
        <div class="col-30">
            <div class="right-overall-graph ml5">
                <h4 class="heading padding5">Overall Progress</h4>
                <div id="rightOverallGraph" data-graphdata='<?=json_encode($projectProcessStatus) ?>' style="height: 130px;margin: 0 auto"></div>
            </div>
        </div>
    </div>
    <div class="project-details-grpah-widget pt10 clearfix">
        <div class="col-100">
            <div class="right-overall-graph ml5">
                <h4 class="heading padding5">Task Progress</h4>
                <div id="rightCompleteGraph" data-graphdata='<?=json_encode($projectYearlyStatus) ?>' style="min-height: 300px;margin: 0 auto"></div>
            </div>
        </div>
    </div>
    <div class="project-details-grpah-widget pt10 clearfix">
        <div class="col-70">
            <?php
            foreach($projectDepartmentStatus as $departmentStatus){
                $departmentStatus['over_all_estimated'] = $departmentStatus['over_all_estimated_time'];
                $departmentStatus['estimated_time'] = $departmentStatus['estimated_time']!=''?$departmentStatus['estimated_time']:'00:00';
                $departmentStatus['additional_time'] = $departmentStatus['additional_time']!=''?$departmentStatus['additional_time']:'00:00';
                $departmentStatus['allowted_time'] = $departmentStatus['allowted_time']!=''?$departmentStatus['allowted_time']:'00:00';
                $departmentStatus['actual_time'] = $departmentStatus['actual_time']!=''?$departmentStatus['actual_time']:'00:00';
                $departmentStatus['unallotted_time'] = sec_to_time((time_to_sec($departmentStatus['estimated_time'])+time_to_sec($departmentStatus['additional_time']))-time_to_sec($departmentStatus['allowted_time']));
                if(time_to_sec($departmentStatus['allowted_time']) > time_to_sec($departmentStatus['actual_time']))
                    $departmentStatus['remain_logtime'] = sec_to_time(time_to_sec($departmentStatus['allowted_time'])-time_to_sec($departmentStatus['actual_time']));
                else{
                    if(time_to_sec($departmentStatus['actual_time'])-time_to_sec($departmentStatus['allowted_time']) == 0){
                        $departmentStatus['remain_logtime'] = sec_to_time(time_to_sec($departmentStatus['actual_time'])-time_to_sec($departmentStatus['allowted_time']));
                    }else{
                        $departmentStatus['remain_logtime'] = '-'.sec_to_time(time_to_sec($departmentStatus['actual_time'])-time_to_sec($departmentStatus['allowted_time']));
                    }
                }
                /*foreach($departmentStatus as $status){*/ ?>
                <div class="col-25">
                    <div class="prj-inv-graph" >
                        <div class="prj-inv-graph-head">
                            <a href="javascript:;">
                                <h4>Department : <?= $departmentStatus['department_name'] ?></h4>
                            </a>
                        </div>
                        <div class="prj-inv-graph-content" style="font-size: 13px;padding: 5px;">
                        <div><Span>Total Eastimated : <?= $departmentStatus['over_all_estimated']?>(H)</Span></div>
                        <div><Span>Department Hold : <?= $departmentStatus['estimated_time']?>(H)</Span></div>
                        <div><Span>Additional : <?= $departmentStatus['additional_time']?>(H)</Span></div>
                        <div><Span>Allotted : <?= $departmentStatus['allowted_time']?>(H)</Span></div>
                        <div><Span>UnAllotted : <?= $departmentStatus['unallotted_time']?>(H)</Span></div>
                        <div><Span>Logged : <?= $departmentStatus['actual_time']?>(H)</Span></div>
                        <div><Span>Remain Logtime : <?= $departmentStatus['remain_logtime']?>(H)</Span></div>
                        </div>
                    </div>
                </div>
            <?php   } ?>
        </div>
    </div>
</div>
<div class="col-sm-12 clearfix mt10 pr0">
    <div class="col-50">
        <div class="open-items-wrapper mr5">
            <h4 class="margin0 heading padding5"><i class="fa fa-folder-open-o mr5"></i>Open Items<span class="red bold ml10">(<?=count($projectOpenTask)?>)</span><a class="pull-right" href="<?=WEB_BASE_URL.'index.php/Project/projectCreation/'.$projectId?>"><i class="fa fa-plus"></i></a></h4>
            <table class="table table-responsive open-items-table mb0" width="100%" id="openItemsTable" style="min-height: 50px;">
                <tbody>
                <?php foreach($projectOpenTask as $k=>$v) { ?>
                    <tr>
                        <td><?= $v->open_item_description ?></td>
                        <td><?= $v->name ?></td>
                        <td><?php if($v->open_item_status == 'ontrack'){
                                echo '<span class="green">'.ucfirst($v->open_item_status).'</span>';
                            }else if($v->open_item_status == 'delayed'){
                                echo '<span class="red">'.ucfirst($v->open_item_status).'</span>';
                            }else if($v->open_item_status == 'concern'){
                                echo '<span class="orange">'.ucfirst($v->open_item_status).'</span>';
                            } ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-50">
        <div class="milestones-wrapper ml5">
            <h4 class="margin0 heading padding5"><i class="fa fa-clock-o mr5"></i>Milestones<a class="pull-right" href="<?=WEB_BASE_URL.'index.php/Project/projectCreation/'.$projectId?>"><i class="fa fa-plus"></i></a></h4>
            <table class="table table-responsive milestones-table mb0" width="100%" style="min-height: 50px;">
                <tbody>
                <?php foreach($projectMilestone as $k=>$v) { ?>
                    <tr>
                        <td><?= $v->milestone_name ?></td>
                        <td><?php if($v->milestone_status == 'ontrack'){
                                echo '<span class="green">'.ucfirst($v->milestone_status).'</span>';
                            }else if($v->milestone_status == 'delayed'){
                                echo '<span class="red">'.ucfirst($v->milestone_status).'</span>';
                            }else if($v->milestone_status == 'concern'){
                                echo '<span class="orange">'.ucfirst($v->milestone_status).'</span>';
                            } ?></td>
                        <td><?= $v->milestone_start_date ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-sm-12 mt10 pr0">
    <div class="project-resrc-workload">
        <h4 class="heading padding5"><i class="fa fa-users mr5"></i>Resources Workload (Hours)</h4>
        <!--        <div data-chart='--><?php //echo json_encode($temp);?><!--' id="ProjectresourceWorkloadData">-->
        <div data-chart='<?php echo json_encode($projectWorkload);?>' id="ProjectresourceWorkloadData">
            <div class="col-sm-12" id="ProjectresourceWorkload" style="width: 100%; height: 210px; margin: 0 auto"></div>
        </div>
    </div>
</div>
<div class="col-sm-12 padding0" >
    <div class="departmnt-task clearfix">
        <ul role="tablist" class="nav nav-tabs nav-tabs-top custom-nav-tabs check-tabs pull-right">
            <li class="active" ><a data-toggle="tab" href="#departments" ><p class="tab-list">Task List</p></a></li>
            <li><a data-toggle="tab" href="#milestones"><p class="tab-list">Task Overview</p></a></li>
        </ul>
    </div>
</div>
<div class="tab-content pb0 mt0">
    <div id="departments" class="tab-pane padding10 active">
        <!--        <h4 class="section-heading">Department / Team</h4>-->
        <div class="col-sm-12 padding0">
            <div class="col-sm-12 pt10 pr0">
                <div id="taskList" class="tab-pane">
                    <div class="grid-details-table">
                        <div class="grid-details-table-header">
                            <h3>Task List</h3>
                            <input type="text" class="searchInput pull-right"  />
                            <a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                        </div>
                        <div class="grid-details-table-content clearfix padding0">
                            <div class="tbl_wrapper border0">
                                <table id="tasks-list" class="table-responsive"></table>
                                <div id="tasks-list-page"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="milestones" class="tab-pane padding10">
        <!--        <h4 class="section-heading">Depatment Task</h4>-->
        <div class="col-sm-12 pt20 pr0">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Depatment Task List</h3>
                    <input type="text" id="filter-task-box" class="pull-right" style="width: 0px;padding-left: 5px;overflow:hidden; display: none; height: 28px;"/>
                    <a class="custumSearchInput gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
            </div>
            <div class="grid-details-table-content clearfix padding0">
                <div class="tbl_wrapper border0">
                    <table id="department-list" class="table-responsive department-list" style="border: 1px solid #eee;">
                        <thead></thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!--<div id="add-client-modal" class="modal-wrapper" style="display:none">
    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    <h4 class="panel-title">Add Client</h4>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input_container">
                                    <input type="text" class="form-control rval" name="task_name"  id="task_name"/>
                                </div>
                                <label class="control-label"><span class="req-str">*</span>Task Name :</label>
                            </div>
                        </div>
                    </div>
                    <?php /*if($this->session->userdata('is_manager') == 1){ */?>
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container">
                                    <input id="start_date" name="start_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                </div>
                                <label class="control-label"><span class="req-str">*</span>Start Date :</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container">
                                    <input id="end_date" name="end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                </div>
                                <label class="control-label"><span class="req-str">*</span>End Date :</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input_container">
                                    <input type="text" class="form-control rval msk_time" name="estimated_time"  id="estimated_time"  />
                                </div>
                                <label class="control-label">Estimated Time :</label>
                            </div>
                        </div>
                    </div>
                    <div class="moduleDiv">

                    </div>
                    <div class="col-sm-12 clearfix">
                        <div id="getDepartmentList"></div>
                    </div>
                    <?php /*} */?>
                    <div class="col-sm-12 clearfix">
                        <div class="form-group bt-default">
                            <div class="change-padd">
                                <label>Description</label>
                                <textarea name="description" id="description" class="form-control rval"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix">
                        <div class="form-group bt-default">
                            <div class="change-padd">
                                <label><span class="req-str">*</span>Use Case</label>
                                <textarea name="use_case" id="use_case" class="form-control rval"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control rval" name="id_project_task" />
                    <input type="hidden" class="form-control rval" name="id_task_flow" />
                    <input type="hidden" class="form-control rval" name="id_project" />
                    <input type="hidden" class="form-control rval" name="department_id" />
                </div>
                <div class="panel-footer text-center">
                    <button class="button-common" div-submit="true">Save</button>
                    <button class="button-color" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>-->
<div id="add-client-modal" class="modal-wrapper" style="display:none">
    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    <h4 class="panel-title">Add Client</h4>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input_container">
                                    <input type="text" class="form-control rval" name="task_name"  id="task_name"/>
                                </div>
                                <label class="control-label"><span class="req-str">*</span>Task Name :</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth padding0 pmdiv">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container">
                                    <input id="start_date" name="start_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                </div>
                                <label class="control-label"><span class="req-str">*</span>Start Date :</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container">
                                    <input id="end_date" name="end_date" type="text" placeholder="dd/mm/yyyy" class="rval form-control tssDatepicker">
                                </div>
                                <label class="control-label"><span class="req-str">*</span>End Date :</label>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input_container">
                                    <input type="text" class="form-control rval msk_time" name="estimated_time"  id="estimated_time"  />
                                </div>
                                <label class="control-label">Estimated Time :</label>
                            </div>
                        </div>
                    </div>-->
                    <div class="moduleDiv pmdiv">

                    </div>
                    <div class="col-sm-12 clearfix">
                        <div id="getDepartmentList" style="padding-top: 12px;font-size: 12px;"></div>
                    </div>
                    <div class="col-sm-12 clearfix pl0">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container mt10">
                                    <select name="requirement_id" id="requirement_id" class="rval"></select>
                                </div>
                                <label class="control-label">Requirement</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix">
                        <div class="form-group bt-default">
                            <label>Description</label>
                            <textarea name="description" id="description" class="form-control rval"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix">
                        <div class="form-group bt-default">
                            <div class="change-padd">
                                <label><span class="req-str">*</span>Use case</label>
                                <textarea name="use_case" id="use_case" class="form-control rval"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control rval" name="id_project_task" />
                    <input type="hidden" class="form-control rval" name="id_task_flow" />
                    <input type="hidden" class="form-control rval" name="id_project" />
                    <input type="hidden" class="form-control rval" name="department_id" />
                </div>
                <div class="panel-footer text-center">
                    <button class="button-common" div-submit="true">Save</button>
                    <button class="button-color" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="taskDetails" class="modal-wrapper" >
    <div id="detailsContent">

    </div>
</div>
<script>
    TssLib.docReady(function () {
        getTaskList();
        getDepartmentTaskList();
        $('.custumSearchInput i').on('click',function(){
            var a = $(this).parent().parent().find('#filter-task-box');
            if(a.css('display') == 'none'){
                a.css({'width':'140px' ,'display':'block', 'height': '28px;', 'transition': 'width 2s linear'});
            }else{
                a.css({'width':'0px' , 'overflow': 'hidden', 'display':'none', 'transition': 'width 2s linear'});
            }
        });
        $('#filter-task-box').on('blur',function(){
            $(this).parent().parent().find('#filter-task-box').css({'width':'0px' , 'overflow': 'hidden', 'display':'none', 'transition': 'width 2s linear'});
        });
    });
    $('#rightOverallGraph').each(function () {
        var processData = '';
        processData = jQuery.parseJSON($(this).attr('data-graphdata'));
        var _finalObj = [];
        $.each(processData, function( index, value ) {
            var color = '';
            if(value.status == 'new'){ color = '#47b700';}else if(value.status == 'progress'){ color = '#9a9a9a'; }else if(value.status == 'completed'){ color = '#f36523';}
            _finalObj.push({name:value.status,y:parseInt(value.count),color:color});
        });
        processData = _finalObj;
        $(this).highcharts({
            chart: {
                plotBorderWidth: 0,
                plotShadow: false,
                spacing:[0,0,10,0]
            },
            title: {
                text: false,
                align: 'center',
                verticalAlign: 'middle',
                y: 40
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    showInLegend: true,
                    padding:0,
                    margin:0,
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '100%']
                }
            },
            legend: {
                align: "center",
                verticalAlign: "bottom",
                margin: 0,
                padding: 0,
                spacing: [0, 0, 0, 0]
            },
            series: [{
                type: 'pie',
                innerSize: '0',
                data: processData
            }]
        });
    });

    $('#rightCompleteGraph').each(function () {
        var processData = '';
        processData = jQuery.parseJSON($(this).attr('data-graphdata'));
        var _rowObj = [];
        var _finalObj = [];
        var approval = [];
        var progress = [];
        var completed = [];
        var newtask = [];
        $.each(processData, function( index, value ) {
            _rowObj.push(value.range.start);
            approval.push(parseInt(value.data.approval_waiting_count));
            progress.push(parseInt(value.data.progress_count));
            completed.push(parseInt(value.data.completed_count));
            newtask.push(parseInt(value.data.new_count));
        });

        $(this).highcharts({
            title: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'Date Range'
                },
                categories: _rowObj
            },
            yAxis: {
                title: {
                    text: 'No. of Tasks'
                },
                allowDecimals: false,
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
            },
            series: [{
                name: 'Approval waiting',
                data: approval
            }, {
                name: 'Progress',
                data: progress
            }, {
                name: 'Completed',
                data: completed
            }, {
                name: 'New',
                data: newtask
            }]
        });
    });

    $('.projectUxdesignInfo').each(function () {
        graphData = jQuery.parseJSON($(this).attr('data-graphdata'));
        /*graphData = [{name: 'Incomplete', y: ((graphData.actual_time)-(graphData.duration))/(60*60),color: "#9a9a9a" }, {name: 'Complete', y: graphData.duration/(60*60), color: "#47b700"}]*/
        graphData = [
            {name: 'Eastimated Time', y: (graphData.estimated_time_sec)/(60*60),color: "#9a9a9a" },
            {name: 'Additional Time', y: (graphData.additional_time_sec)/(60*60), color: "#47b700"},
            {name: 'Allotted Time', y: (graphData.allowted_time_sec)/(60*60), color: "#bc7878"},
            {name: 'Logged Time', y: (graphData.actual_time_sec)/(60*60), color: "#f24242"},
        ]
        $(this).highcharts({
            chart: {
                type: 'pie',
                style: {
                    fontFamily: "Lato,sans-serif"
                }
            },
            title: true,
            tooltip: {
                pointFormat:  '{name} <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    innerSize: 70,
                    depth: 0,
                    showInLegend:false,
                    dataLabels: {
                        enabled: false
                    }
                }
            },

            series: [{
                size: '100%',
                innerSize: '40%',
                data: graphData
            }]
        });
    });

    function getDepartmentTaskList(){
        var project_id = $('#projectDropdown').val();
        getJsonAsyncWithBaseUrl('Project/ProjectTaskDetails?project_id='+project_id, {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data.response) {
                    var data = result.data.data, head = '', body = '', count =0;
                    for(var index in data){
                        var main = data[index];
                        if(count == 0){
                            head += '<tr>';
                            for(var ind in main){
                                if(ind != 'id_project_task'){
                                    var heading = ind;
                                    if(ind == 'task_name'){ head += '<th width="15%">Task Name</thwidth>'; }
                                    else if(ind == 'description') { head += '<th width="15%">Description</th>'; }
                                    else{ head += '<th>'+heading+'</th>'; }
                                }
                            }
                            count++;
                            head += '</tr>';
                        }
                        body += '<tr>';
                        for(var ind in main){
                            if(ind != 'id_project_task'){
                                if(ind == 'task_name'){
                                    body += '<td class="taskname">'+main[ind]+'</td>';
                                }else{
                                    body += '<td >'+main[ind]+'</td>';
                                }
                            }
                        }
                        body += '</tr>';
                    }
                    $('#department-list > thead').html(head);
                    $('#department-list > tbody').html(body);
                    searchDepartmentTask();
                }else{
                    $('#department-list > thead').html('');
                    $('#department-list > tbody').html('');
                }
            }
        });
    }
    function searchDepartmentTask(){
        $('#filter-task-box').keyup(function(){
            var value = $(this).val();
            var re = new RegExp( value, "gi" );
            $("table tr").each(function(index) {
                if (index != 0) {

                    $row = $(this);

                    var id = $row.find("td:first").text();

                    if (!id.match(re)) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                }
            });
        });
    }
    function getTaskList(){
        var $taskGrid = $('#tasks-list');
        $taskGrid.jqGrid({
            //rowNum: -1,
            url: TssConfig.TT_SERVICE_URL + 'project/task_list',
            multiselect: false,
            datatype: "json",
            sortorder: "desc",
            extSearchField: '.searchInput',
            postData: {projectIds:$('#projectDropdown').val()},
            colNames: ['id_task_workflow','id_project', 'Requirement Name','Module Name','Project Title', 'task_name','department_id','project_id','id_project_task','id_task_flow','start_date','end_date','Task','Start Date', 'End Date','Est. Time', 'Tot Log','Add. time','description','Created Date','Completed Date','Status','Action','project_task_name','use_case', 'pm_approved','requirement_id'],
            colModel: [
                { name: 'id_task_workflow', index: 'id_task_workflow', hidden: true ,key:true},
                { name: 'projectTitle', index: 'projectTitle', hidden: true },
                { name: 'requirement_name', index: 'requirement_name',width: 30 },
                { name: 'module_name', index: 'module_name',width: 30 },
                { name: 'task_name', index: 'task_name', hidden: true },
                { name: 'id_project_task', index: 'id_project_task',hidden: true },
                { name: 'id_task_flow', index: 'id_task_flow',hidden: true },
                { name: 'start_date', index: 'start_date',hidden: true,width: 20 },
                { name: 'end_date', index: 'end_date',hidden: true ,width: 20 },
                { name: 'id_project', index: 'id_project', hidden: true },
                { name: 'department_id', index: 'department_id', hidden: true },
                { name: 'id_project', index: 'id_project', hidden: true },
                /*{ name: 'project_task_name', index: 'project_task_name',width: 40,formatter: function (c, o, d) {
                    return '<a title="' + d.project_task_name + '" href="javascript:;" >' + d.project_task_name + '</a>';
                }
                },*/
                { name: 'project_task_name', index: 'project_task_name',width: 40,formatter: function (c, o, d) {
                    //return d.project_task_name;
                    //return '<a title="' + d.project_task_name + '" href="javascript:;" >' + d.project_task_name + '</a>';
                    return '<a href="javascript:;" onclick="openTaskMemeberModal('+ d.id_project_task +')">'+d.project_task_name+'</a>';
                }
                },
                { name: 'task_start_date', index: 'task_start_date',width: 15 },
                { name: 'task_end_date', index: 'task_end_date',width: 15 },
                { name: 'estimated_time', index: 'estimated_time',width: 10,formatter: function (c, o, d) {
                    if(d.estimated_time == null){
                        return d.estimated_time;
                    }else{
                        var data = d.estimated_time.split(':');
                        return data[0] + ':' + data[1];
                    }
                }
                },
                { name: 'totalLog', index: 'totalLog',width: 10,sortable: false,formatter: function (c, o, d) {
                    if(d.totalLog == '00:00'){
                        return d.totalLog;
                    }else{
                        var data = d.totalLog.split(':');
                        return data[0] + ':' + data[1];
                    }
                }
                },

                { name: 'additional_time', index: 'additional_time',width: 10,sortable: false,formatter: function (c, o, d) {
                    if(d.additional_time == '00:00'){
                        return d.additional_time;
                    }else{
                        var data = d.additional_time.split(':');
                        return data[0] + ':' + data[1];
                    }
                }
                },
                /*{ name: 'description', index: 'description',width: 28 ,formatter: function (c, o, d) {
                 return '<span title="' + d.description + '" href="javascript:;" >' + d.description + '</span>';
                 }
                 },*/
                { name: 'description', index: 'description', hidden: true },
                { name: 'original_created_date_time', index: 'original_created_date_time', hidden: true },
                { name: 'completed_date', index: 'completed_date',width: 20 },
                { name: 'current_status', index: 'Action',width: 15,  formatter: function(c,o,d){
                    var innerHtml = '';
                    if(c.toLowerCase() == 'new'){
                        innerHtml += '<span class="pull-left task-status task-inprogress">New</span>';
                    }
                    if(c.toLowerCase() == 'progress'){
                        innerHtml += "<span class='pull-left task-status task-waiting'>In Progress</span>";
                    }
                    if(c.toLowerCase() == 'completed'){
                        innerHtml += '<span class="pull-left task-status task-completed">Completed</span>';
                    }
                    if(c.toLowerCase() == 'hold'){
                        innerHtml += '<span class="pull-left task-status task-open">Hold</span><span class="pull-left task-chat"></span>';
                    }
                    return innerHtml;
                }
                },
                { name: 'current_status', index: 'Action',width: 10,  formatter: function(c,o,d){
                    var _html = '';
                    _html += '<a href="javascript:;" onclick="editTask('+ o.rowId+', '+ d.id_project_task +')" class="pull-left task-status btn-danger ml10" title="Edit"><i class="fa fa-pencil"></i></a>';
                    if(c.toLowerCase() == 'new'){
                         _html += '<a href="javascript:;" onclick="deleteTask('+d.id_project_task+')" class="pull-left task-status btn-danger ml10" title="Delete"><i class="fa fa-trash-o"></i></a>';
                    }else
                        _html += ' -- ';
                    return _html;
                }
                },
                { name: 'project_task_name', index: 'project_task_name', hidden: true },
                { name: 'use_case', index: 'use_case', hidden: true },
                { name: 'pm_approved', index: 'pm_approved', hidden: true },
                { name: 'requirement_id', index: 'requirement_id', hidden: true },
            ],
            pager: 'tasks-list-page'
        }).navGrid('#tasks-list-page', {
            edit: false, add: true, del: false, search: false, refresh: true,
            addfunc: function () {
                editTask();
            }
        });
        var alarteed = [], assigne = []; var users = [];
        var ProjectresourceWorkloadData = jQuery.parseJSON($('#ProjectresourceWorkloadData').attr("data-chart"));
        $.each( ProjectresourceWorkloadData, function( key, value ) {
            alarteed.push(parseFloat(value.total));
            assigne.push(parseFloat(value.loggedTime));
            users.push(value.name);
        });
        $('#ProjectresourceWorkload').each(function () {
            $(this).highcharts({
                chart: {
                    type: 'column',
                },
                title: false,
                xAxis: {
                    categories: users
                },
                yAxis: [{
                    min: 0,
                    title: false,
                    labels: {
                        enabled: true
                    }
                }, {
                    title: false,
                    opposite: true
                }],
                legend: {
                    enabled: true
                },

                tooltip: {
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                    }
                },
                series: [{
                    name: 'Alloted',
                    color: '#0072BB',
                    data: alarteed,
                    pointPadding: 0.1
                }, {
                    name: 'Actual',
                    color: '#5CBA5C',
                    data: assigne,
                    pointPadding: 0.2
                }]
            });
        });
        TssLib.animateSearchInHead('#taskList');
    }
    function editTask(id, taskId) {
        var project_id = $('#projectDropdown').val();
        var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
        var $taskGrid = $('#tasks-list');
        var rowData = $taskGrid.jqGrid('getRowData', id);
        $(".msk_time").mask("Hh:Mm");

        if (TssLib.isBlank(id)) {
            var _html = '';
            _html += '' +
                '<div class="col-sm-12 clearfix clearboth padding0">' +
                '<div class="col-sm-6">' +
                '<div class="form-group"><div class="input_container mt10">' +
                '<input type="text" name="module_name"/>' +
                '</div>' +
                '<label class="control-label"><span class="req-str">*</span>Module Name</label>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-6">' +
                '<div class="form-group"><div class="input_container mt10">' +
                '<input type="text" name="task_type"/>' +
                '</div>' +
                '<label class="control-label"><span class="req-str">*</span>Task Type</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-12 clearfix clearboth">' +
                '<div class="form-group"><div class="input_container mt10">' +
                '<select id="department_id" class="rval ">' +
                '<option value="">Select</option>' +
                '</select>' +
                '</div>' +
                '<label class="control-label"><span class="req-str">*</span>Department Name</label>' +
                '</div>';
            $modal.find('.panel-title').text('Add Task');
            $modal.find('[div-submit="true"]').text('Save');
            $(".moduleDiv").html('');
            $(".moduleDiv").append(_html);
            getJsonAsyncWithBaseUrl("Project/getProjectDepartments/"+project_id, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var  task = result.data.data;
                        TssLib.populateSelect($('#department_id'), { success: true, data: task }, 'department_name', 'department_id');

                        TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                    }
                }
            });
            $.get("<?=site_url('Project/getDepartmentByProject')?>/"+project_id+"/" , {}, function( data ) {
                $modal.find('#getDepartmentList').html(data);
                $(".msk_time").mask("Hh:Mm");
            });
        }else{
            $modal.find('.panel-title').text('Edit Task');
            $modal.find('[div-submit="true"]').text('Update');
            $(".moduleDiv").html('');
            if(rowData.pm_approved==1){
                $modal.find('.pmdiv').hide();
                $globalDs = true;
            }else{
                $modal.find('.pmdiv').show();
                $globalDs = false;
            }
            $.post("<?=site_url('Project/getProjectDepartmentsEstTime')?>" , {'project_id': project_id, 'task_id': taskId}, function( data ) {
                $modal.find('#getDepartmentList').html(data);
                $(".msk_time").mask("Hh:Mm");
            });

        }

        TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);

        postJsonAsyncWithBaseUrl("Project/projectRequirement/"+project_id, {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data.data != null) {
                    TssLib.populateSelect($('#requirement_id'), {success: true, data: result.data.data}, 'requirement_name', 'id_requirements');
                    TssLib.preSelectValForSelectBox($modal.find('#requirement_id'),rowData.requirement_id);
                }
            }
        });

        TssLib.ajaxForm({
            jsonContent: true,
            form: $('#linen-createSchedule-form'), callback: function (result) {

                if (TssLib.isBlank(id)) {
                    var addedTest = 'Added';
                }else{
                    var addedTest = 'Updated';
                }
                if (result.data.status) {
                    $('#tasks-list').trigger('reloadGrid');
                    TssLib.closeModal();
                    $(".moduleDiv").html('');
                    TssLib.notify('Task '+addedTest+' successfully ', null, 5);
                }else{
                    TssLib.notify(result.data.message, 'warn', 5);
                }
            },
            before: function (formObj) {
                var start_date = $(formObj).find("#start_date").val(),
                    end_date = $(formObj).find("#end_date").val(),
                    department_id = $(formObj).find("#department_id").val(),
                    task_type = $(formObj).find("#task_type").val(),
                    description = encodeSpecialChar($(formObj).find("#description").val()),
                    use_case = $(formObj).find("#use_case").val(),
                    requirement_id = $(formObj).find("#requirement_id").val(),
                    project_module_id = $(formObj).find("#project_module_id").val();

                var deptt = [];
                var collection = $(".department");
                if(collection.length > 0){
                    collection.each(function() {
                        var This = $(this);
                        var id = This.attr('data-id');
                        var value = This.val();
                        deptt.push({'id': id, 'est': value});
                    });
                }

                if(deptt.length<=0){ TssLib.notify("Department can't be blank", 'warn', 5); return false; }

                if (TssLib.isBlank(id)) {
                    $(formObj).data('additionalData', { start_date:start_date ,end_date:end_date, department_id:department_id , department: deptt ,task_type:task_type, module_id: project_module_id, project_id: project_id, use_case: use_case, requirement_id: requirement_id});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/addProjectTask');
                }else{
                    $(formObj).data('additionalData', { department: deptt , start_date:start_date ,end_date:end_date ,description: description, use_case: use_case, requirement_id: requirement_id});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/updateSingleProjectTask');
                }
                return true;
            }
        });
    }
    function deleteTask(id_project_task){
        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
            $('#oasis-confirm').closest('.modal').modal('hide');
            postJsonAsyncWithBaseUrl("project/deleteTask?project_task_id="+id_project_task, {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data.status == true) {
                        $("#tasks-list").trigger('reloadGrid');
                        TssLib.notify('Task removed ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                }
            });
        }, 'Yes', 'No');
    }
    function openTaskMemeberModal(taskId){
        var $modal = TssLib.openModel({width: 1024},$('#taskDetails').html());

        $.post("<?=site_url('Project/taskFullDetails')?>" , {'task_id':taskId }, function( data ) {
            $modal.find('#detailsContent').html(data);
        });
    }
</script>