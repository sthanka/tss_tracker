<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Project Details</h3>
        </div>
        <div class="col-sm-12 clearboth clearfix pb20 bg-white ml15">
            <div class="col-sm-9 padding0">
                <div class="col-sm-4">
                   <div class="form-group padding0">
                   <div class="multi-select-container">
                       <select id="projectDropdown" class="form-control" enablefiltering="true">
                       </select>
                   </div>
                   <label class="control-label col-sm-12">Project:</label>
                    </div>
               </div>
                <div class="col-sm-4">
                    <button onclick="applyProjectDetailsFilter(this)" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                </div>
            </div>
        </div>

        <div id="projectResult" class="col-sm-12 pl0 pr0"></div>
    </div>
</div>
<script>
    var project_id = 0;
    TssLib.docReady(function () {
        postJsonAsyncWithBaseUrl("project/get_projects", {}, {
            jsonContent: true,
            callback: function (result) {
                if (result.data != null) {
                    var projects = result.data;
                    //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                    TssLib.populateSelect($('#projectDropdown'), {
                        success: true,
                        data: projects
                    }, 'project_name', 'id_project');
                    $("#projectDropdown").select2("val", $("#projectDropdown option:eq(1)").val());
                    applyProjectDetailsFilter(this);
                }
            }
        });
    });
    
    function applyProjectDetailsFilter(){
        var project = $('#projectDropdown').val();
        if(project != ''){
            $.get("<?=site_url('Project/getProjectDetialsById/')?>?project_id="+project, function (data) {
                $('#projectResult').html(data);
            });
        }
    }

    function addEditCreateSchedule(rowData) {
        var $modal = TssLib.openModel({ width: '600' }, $('#add-project-modal').html());
        if (TssLib.isBlank(rowData)) {

        } else {
            TssLib.renderData($('#linen-createSchedule-form'), rowData);
        }
        TssLib.ajaxForm({
            jsonContent: true,
            form: $('#linen-createSchedule-form'), callback: function (data) {
                if (data.success) {

                }
                TssLib.closeModal();
            }
        });
    }
    function getUseCase(e){
        $('body').find('[tab-context="use-case"]').trigger('click');
    }

</script>