<table class="day-monitoring-table table-responsive">
    <tbody>
        <?php
//        echo "<pre>"; print_r($data); exit;
        if(count($data)>0){
        foreach($data as $k=>$v){ //if($v['id_user']==43){ echo "<pre>"; print_r($v); exit; }
            ?>
        <tr>
            <td><span><?=$v['name']?></span><span><?/*= $v['departmentName']*/?></span></td>
            <td><span><?=$v['projectCounts']?></span><span>Projects</span></td>
            <td><span><?=$v['taskCount']?></span><span>Tasks</span></td>
            <td><span></span><span></span></td>
            <td><span><?=sec_to_time(time_to_sec($v['actual']))?></span><span>Act.Hr</span></td>
            <?php
                $workStatus = array('working'=>'working', 'free' =>'no task', 'meeting'=>'meeting');
                $StatusColor = array('working'=>'#5cba5c', 'free' =>'#ff150e', 'meeting'=>'#0072bb');
                $v['logged_time'] = array_reverse($v['logged_time']);
                //echo "<pre>"; print_r($v); exit;
                foreach($v['logged_time'] as $k1=>$v1){

                    if (strpos($v1['duration'], '.') !== false) {
                        $v1['duration'] = explode('.', $v1['duration']);
                        $v1['duration'] = $v1['duration'][0] + (($v1['duration'][1] / 60));
                        //$v1['duration'] = (float)str_replace(':','.',$v1['duration']);
                    }

                    $graphData[] = array(
                        'name'=> $v1['taskName'].' '.$workStatus[$v1['workType']],
                        'data'=> [(float)round($v1['duration'],2)],
                        'color'=> $StatusColor[$v1['workType']]
                    );

                    if($v1['isToday']==true){
                        $min = '0';
                        $max = $v1['totalDuration'];
                    }else{
                        $min = '0';
                        $max = '11';
                    }
                }
            //echo "<pre>"; print_r($graphData); exit;
            ?>
            <td><div data-chart='<?php echo json_encode($graphData); ?>' id="dayMonitoringGraph-notask" class="dayMonitoringGraph" style="height: 70px; margin: 0 auto"></div></td>
            <td>
                <!--<span>+02</span>-->
            </td>
            <td>
                <!--<span class="no-task-bg">NO TASK</span>-->
            </td>
        </tr>
        <?php }
        }else{
        ?>
    <div class="col-sm-12"><center>No Data found</center></div>
        <?php } ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('.dayMonitoringGraph').each(function () {
            var chartData = jQuery.parseJSON($(this).attr('data-chart'));
            console.log(chartData);
            $(this).highcharts({
                chart: {
                    type: 'bar',
                    spacing: [0, 0, 0, 0],
                    padding: 0,

                },
                title: false,
                xAxis: {
                    title: false,
                    gridLineColor: '#ccc',
                    lineColor: '#ccc',
                    lineWidth: 1,
                    tickLength: 0,
                    labels: {
                        enabled: false
                    },
                    categories: ['Log Time (hours)']
                },
                yAxis: {
                    title: false,
                    offset: -15,
                    gridLineColor: '#ccc',
                    lineColor: '#ccc',
                    lineWidth: 1,
                    min:'<?= $min ?>',
                    max:'<?= $max ?>',
                    allowDecimals:true,
                    tickInterval:1,
                    labels: {
                        padding: 0,
                        margin: 0,
                        spacing: [0, 0, 0, 0],
                        formatter:function(){
                            //console.log(this.value);
                            this.value = this.value+9;
                            if(this.value==0)
                                return '12 ';
                            else if(this.value==24)
                                return '12 ';
                            else if(this.value==12)
                                return this.value+' ';
                            else if(this.value>12)
                                return (this.value-12)+' ';
                            else
                                return this.value+' ';
                        },
                        style: {
                            "textOverflow": "none"
                        }
                    },
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: chartData
            });
        });
    });

</script>