<div class="modal fade" id="oak_popup">
    <div class="modal-dialog model-mg-width">
        <div class="modal-content model-mg-content"></div>
    </div>
</div>
<!--@RenderBody()-->
<!--Render Body Start-->
<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Work Request</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Work request list</h3>
                    <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                </div>
                <div class="grid-details-table-content clearfix padding0">
                    <div class="tbl_wrapper border0">
                        <table id="task-type-list" class="table-responsive"></table>
                        <div id="task_type-list-page"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-client-modal" class="modal-wrapper" style="display:none; width: 200px;">
            <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                            <h4 class="panel-title">Add Task Types</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12 clearfix clearboth padding0">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval tssDatepicker" enddate="0d" name="date" id="date"/>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Date:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group padding0">
                                            <div class="input_container" >
                                                <select id="approved_by" name="approved_by" class="form-control rval"></select>
                                            </div>
                                            <label class="control-label col-sm-12">Project Manager:</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group bt-default">
                                        <label><span class="req-str">*</span>Comment</label>
                                        <textarea name="description" id="description" class="form-control rval"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="id_work_request" value=""/>
                        </div>
                        <div class="panel-footer text-center">
                            <button class="button-common" div-submit="true">Save</button>
                            <button class="button-color" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#task-type-list');
        TssLib.docReady(function () {
            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Project/getWorkRequestGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
                colNames: ['id_work_request', 'Requested for' , 'Requested on' , 'Description' , 'Status' , 'Approved By' , 'Admin Comments' ],
                colModel: [
                    { name: 'id_work_request', index: 'id_work_request', hidden: true ,key:true},
                    { name: 'date', index: 'date', width: 8 },
                    { name: 'created_date', index: 'created_date', width: 8 },
                    { name: 'description', index: 'description', width: 25 },
                    { name: 'status', index: 'status', width: 15 },
                    { name: 'name', index: 'name', width: 20, formatter: function(c,o,d){
                        if(d.status == 'pending'){
                            return '';
                        }else{
                            return d.name;
                        }
                    } },
                    { name: 'admin_comments', index: 'admin_comments', width: 25, formatter: function(c,o,d){
                        if(d.status == 'pending'){
                            return '';
                        }else{
                            return d.admin_comments;
                        }
                    } },
                ],
                pager: 'task_type-list-page'
            }).navGrid('#task_type-list-page', {
                edit: false, add: true, del: true, search: false, refresh: true,
                addfunc: function () {
                    addEditCreateTaskType();
                }, delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        postJsonAsyncWithBaseUrl("Project/deleteWorkRequestById", {'id_work_request': id}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data != null) {
                                    if(result.data.status){
                                        TssLib.notify('Deleted successfully', null, 5);
                                        $('#task-type-list').trigger('reloadGrid');
                                    }else{
                                        TssLib.notify(result.data.message, 'warn', 5);
                                    }
                                }
                            }});
                    }, 'Yes', 'No');
                }
            });
        });
        function addEditCreateTaskType(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-client-modal').html());
            if (TssLib.isBlank(rowData)) {
                $modal.find('[div-submit="true"]').text('Save');
            } else {
                TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                $modal.find('[div-submit="true"]').text('Update');
            }
            $modal.find('.panel-title').text('Work Request');
            getJsonAsyncWithBaseUrl("User/getManagerList/", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var  user = result.data.data;
                        TssLib.populateSelect($('#approved_by'), { success: true, data: user }, 'email', 'id_user');
                    }
                }
            });
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {

                    if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                    if (result.data.status) {
                        $refTblgrid.trigger('reloadGrid');
                        TssLib.closeModal();
                        TssLib.notify('Request '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    $(formObj).data('additionalData', { date: $('#date').val()});
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Project/addWorkRequest');
                    return true;
                }
            });
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->