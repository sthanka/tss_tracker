<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
        <div class="contentHeader">
            <h3>Profile</h3>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <form method="post" action="<?=WEB_BASE_URL?>index.php/User/updateProfile">
             <div class="panel-body align-error">
                <div class="col-sm-12 clearfix clearboth padding0">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input_container">
                                <input type="text" name="first_name" id="first_name" value="<?=$user[0]['first_name']?>" class="form-control"/>
                            </div>
                            <label class="control-label">First Name :</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input_container">
                                <input type="text" name="last_name" id="last_name" value="<?=$user[0]['last_name']?>" class="form-control"/>
                            </div>
                            <label class="control-label">Last Name :</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 clearfix clearboth padding0">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input_container">
                                <input type="text" class="form-control" name="email" id="email" value="<?=$user[0]['email']?>" disabled />
                            </div>
                            <label class="control-label">Email :</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input_container">
                                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" name="phone_number" id="phone_number" value="<?=$user[0]['phone_number']?>" />
                            </div>
                            <label class="control-label">Phone number :</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 clearfix clearboth padding0">
                    <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input_container">
                            <select name="gender" id="gender">
                                <option <?php if($user[0]['gender']=='male'){ echo "selected='selected'"; } ?> value="male">Male</option>
                                <option <?php if($user[0]['gender']=='female'){ echo "selected='selected'"; } ?> value="female">Female</option>
                            </select>
                        </div>
                        <label class="control-label">Gender :</label>
                    </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input_container">
                                <select name="job_role" id="job_role">
                                    <option value="0">Select Job Role</option>
                                    <?php for($s=0;$s<count($job_role);$s++){ ?>
                                        <option <?php if($user[0]['job_role_id']==$job_role[$s]['id_job_role']){ echo "selected='selected'"; } ?> value="<?=$job_role[$s]['id_job_role']?>"><?=$job_role[$s]['job_role']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <label class="control-label">Job role :</label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="panel-footer text-center">
                <button class="button-common" type="submit">Save</button>
            </div>
            </form>
        </div>

    </div>
</div>

