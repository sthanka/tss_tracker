<div  id="chk-disabled">
<?php
if(count($checklist)>0){
foreach($checklist as $k=>$v){ ?>
    <div class="col-sm-12 clearfix">
        <div class="col-sm-8 pt10">
            <label class="control-label"><span class="req-str">*</span><?=$v->checklist_name ?></label>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="padding5"><input type="radio" class="checkListClass"
                                               date-checklist-id="<?php echo $v->id_checklist; ?>"
                                               id="checklist-<?=$v->id_checklist ?>"
                                               name="checklist-<?=$v->id_checklist ?>" value="1" /> Verified</label>
                <label class="padding5"><input type="radio" class="checkListClass"
                                               date-checklist-id="<?php echo $v->id_checklist; ?>"
                                               id="checklist-<?=$v->id_checklist ?>"
                                               name="checklist-<?=$v->id_checklist ?>" value="0" /> N/A</label>
        </div>
        </div>
    </div>
<?php } }else{ ?>
    No checklist found
<?php } ?>
</div>
<script>
    <?php if($status!='progress'){ ?>
    $(function() {
        //$("#chk-disabled").attr('disabled', 'disabled');
        //$("#chk-disabled").children().attr("disabled","disabled");
        $("#chk-disabled input[type=radio]").attr('disabled', false);
    });
    <?php } ?>
</script>
