<?php

$last_date = $project_week[(count($project_week))-1]['week_end_date'];
//echo "<pre>"; print_r($last_date);
$last_date = date('Y-m-d',strtotime('+1 days',strtotime($last_date)));
$next_month = $last_date;
if($project_details[0]['status']=='in-progress' || $project_details[0]['status']=='reopen')
{
    $next_month = date('Y-m-d');
}
else if($project_details[0]['status']=='completed'){
    $next_month = $project_details[0]['completed_date'];
}


$next_weeks = array();
$s=0;
//echo $last_date.'--'.$next_month.'<br>';
while(strtotime($last_date)<=strtotime($next_month)){
    $ts = strtotime($last_date);
    if($s==0){
        $start_s = $ts;
    }
    else
        $start_s = (date('w', $ts) == 0) ? $ts : strtotime('monday this week', $ts);

    $start_date = date('Y-m-d', $start_s);
    $end_date = date('Y-m-d', strtotime('sunday this week', $start_s));
    if(strtotime($start_date)==strtotime($end_date))
    {
        $start_date = date('Y-m-d',strtotime('+1 day', strtotime($start_date)));
        $start_date = date('Y-m-d',strtotime('monday this week', strtotime($start_date)));
        $end_date = date('Y-m-d', strtotime('sunday this week', strtotime($start_date)));
    }
    if(strtotime($next_month)<strtotime($end_date))
        $end_date = $next_month;
    //echo $start_date.'---'.$end_date; exit;
    $next_weeks[] = array(
        'project_id' => $project_id,
        'week_start_date' => $start_date,
        'week_end_date' => $end_date,
    );

    $last_date = date('Y-m-d',strtotime('+7 day', strtotime($last_date)));

    $s++;
}
//echo "<pre>"; print_r($next_weeks); exit;
?>


            <?php $responsiveClass = 'col-sm-12'; ?>
            <div class="col-sm-12 padding0 clearfix">
                <div class="week-but mb5 text-center <?=$responsiveClass?>">
            </div>

            <div class="col-sm-12 padding0">
                <div class="">

                    <div class="<?=$responsiveClass?>" style="overflow-x: scroll">

                        <table class="task-assigning-table mr15 table-bordered text-center project-plan-wrapper project-plan-table">
                            <thead>
                            <th> <span class="th-box">TEAM</span></th>
                            <?php for($s=0;$s<count($project_week);$s++) {
                                $holidays = $this->Project_modal->getHolidaysByDate(array('start_date' => $project_week[$s]['week_start_date'],'end_date' => $project_week[$s]['week_end_date']));
                                $project_week[$s]['holiday'] = count($holidays);
                                /*if(!empty($holidays))
                                    $project_week[$s]['holiday_name'] = join(',',array_map(function($i){ return $i['title']; },$holidays));*/
                                if(!empty($holidays)){
                                    $holidayDate = [];
                                    $holidayDate = array_map(function($i){ return date('w',strtotime($i['date'])); },$holidays);
                                    if(count($holidays)>1){
                                        foreach($holidayDate as $k){
                                            if($k==0 || $k==6){
                                                $project_week[$s]['holiday']--;
                                            }
                                        }
                                    }else if(count($holidays)==1){
                                        if($holidayDate[0]==0 || $holidayDate[0]==6)
                                            $project_week[$s]['holiday']--;
                                    }
                                    $project_week[$s]['holiday_name'] = join(',',array_map(function($i){ return $i['title']; },$holidays));
                                }
                                ?>
                                <th>
                                <span class="th-box" >
                                    <i style="" title="<?=date('d-m-Y',strtotime($project_week[$s]['week_start_date']))?> to <?=date('d-m-Y',strtotime($project_week[$s]['week_end_date']))?>">W<?=$s+1?></i>
                                    <?php if(isset($project_week[$s]['holiday_name'])){ ?>
                                        <i style="" title="<?=$project_week[$s]['holiday_name']?>" class="fa fa-h-square icon-h"></i>
                                    <?php } ?>
                                </span>

                                </th>
                            <?php } $records=$s; ?>
                            <!--next month Weeks after laste dates-->
                            <?php for($s=0;$s<count($next_weeks);$s++) {
                                $holidays = $this->Project_modal->getHolidaysByDate(array('start_date' => $next_weeks[$s]['week_start_date'],'end_date' => $next_weeks[$s]['week_end_date']));
                                $next_weeks[$s]['holiday'] = count($holidays);
                                if(!empty($holidays))
                                    $next_weeks[$s]['holiday_name'] = join(',',array_map(function($i){ return $i['title']; },$holidays));
                                ?>
                                <!--<th class="current-date">
                                <span class="th-box ">
                                    W<?/*=$s+$records+1*/?>

                                </span>
                                </th>-->
                                <th class="current-date">
                                <span class="th-box ">
                                    <i style="" title="<?=date('d-m-Y',strtotime($next_weeks[$s]['week_start_date']))?> to <?=date('d-m-Y',strtotime($next_weeks[$s]['week_end_date']))?>">W<?=$s+$records+1?></i>
                                    <?php if(isset($next_weeks[$s]['holiday_name'])){ ?>
                                        <i style="" title="<?=$next_weeks[$s]['holiday_name']?>" class="fa fa-h-square icon-h"></i>
                                    <?php } ?>
                                </span>
                                </th>
                            <?php } ?>

                            <th><span class="th-box">Total</span></th>
                            </thead>
                            <tbody>
                            <?php for($s=0;$s<count($users);$s++){
                                $total_time = 0; $total__l_time = $total__0_time = 0;

                                ?>
                                <tr>
                                    <td>
                                        <div class="">
                                            <span title="<?=$users[$s]['first_name'].' '.$users[$s]['last_name']?>" class="user-inv-tsk-name">
                                                <?=$users[$s]['first_name'].' '.$users[$s]['last_name']?>
                                            </span>
                                        </div>
                                    </td>
                                    <?php $toc=0; for($r=0;$r<count($project_week);$r++) {
                                        $user_work = $this->Report_model->getUserLogTime(array('project_id' => $project_id,'user_id' => $users[$s]['id_user'],'start_date' => $project_week[$r]['week_start_date'],'end_date' => $project_week[$r]['week_end_date']));
                                        $user_other_work = $this->Report_model->getUserLogTime(array('project_id_not' => $project_id,'user_id' => $users[$s]['id_user'],'start_date' => $project_week[$r]['week_start_date'],'end_date' => $project_week[$r]['week_end_date']));
                                        //$user_week_work = $this->Report_model->getWeekWorkTime(array('user_id' => $users[$s]['id_user'],'start_date' => $project_week[$r]['week_start_date'],'end_date' => $project_week[$r]['week_end_date']));
                                        if(!empty($user_work)){
                                            $lt = $user_work[0]['time'];
                                        }
                                        if($lt==''){ $lt = '00:00'; }
                                        /*if(!empty($user_week_work)){
                                            $wlt = $user_week_work[0]['time'];
                                        }
                                        if($wlt==''){ $wlt = '00:00'; }*/

                                        if(!empty($user_other_work)){
                                            $olt = $user_other_work[0]['time'];
                                        }
                                        if($olt==''){ $olt = '00:00'; }

                                        $total_week_estimation = 6;
                                        //echo $project_week[$r]['week_start_date'].'<br>';
                                        $week_estimation = getWorkingDays($project_week[$r]['week_start_date'],$project_week[$r]['week_end_date']);
                                        //echo $current_week_estimation;
                                        //$current_week_estimation = date('w',strtotime($project_week[$r]['week_start_date']));

                                        //$week_estimation = $total_week_estimation-$current_week_estimation;
                                        //echo $week_estimation; exit;
                                        if($week_estimation > 0) $week_estimation = $week_estimation*8; else $week_estimation = 0;
                                        //echo "<pre>"; print_r($week_estimation);
                                        $other_projects_estimation_details = $this->Report_model->getUserOtherProjectsEstimation(array('project_id' => $project_id,'user_id' => $users[$s]['id_user'],'week_start_date' => $project_week[$r]['week_start_date'],'week_end_date' => $project_week[$r]['week_end_date']));

                                       $other_projects_estimation_time = 0;
                                       if(!empty($other_projects_estimation_details)) $other_projects_estimation_time = $other_projects_estimation_details[0]['assigned_log'];

                                        $week_max_estimation_time = '00:00:00';

                                        if(isset($plan[$users[$s]['id_user']][$project_week[$r]['id_project_week']]['time']))
                                        {
                                            $c_t = $plan[$users[$s]['id_user']][$project_week[$r]['id_project_week']]['time'];

                                            $current_total_estimation_time = time_to_sec($week_estimation-($project_week[$r]['holiday']*8)) - time_to_sec($other_projects_estimation_time)-time_to_sec($c_t);
                                            $week_max_estimation_time = sec_to_time($current_total_estimation_time + time_to_sec($c_t));
                                        }
                                        else
                                        {
                                            $c_t = time_to_sec($week_estimation-($project_week[$r]['holiday']*8)) - time_to_sec($other_projects_estimation_time);
                                            $c_t = sec_to_time($c_t);
                                            $week_max_estimation_time = $c_t;
                                        }

                                        $total__l_time = $total__l_time + time_to_sec($lt);
                                        $total__0_time = $total__0_time + time_to_sec($olt);
                                        ?>
                                        <td>
                                            <div class="project_plan_EW">
                                               <span><input type="text"  <?php if(strtotime(date('d-m-Y')) > strtotime($project_week[$r]['week_end_date'])) echo 'disabled';?>  value="<?=$c_t?>" style="width: 40%" name="<?=$users[$s]['id_user']?>_<?=$project_week[$r]['id_project_week']?>_<?= $week_max_estimation_time?>" class="msk_time" id="" ></span>
                                               <span><b><?=$lt?></b></span>
                                               <span style="color: #5CB9EC" title="<?=$user_other_work[0]['project_name']?>"><b><?=$olt?></b></span>
                                            </div>
                                        </td>

                                    <?php $toc = $r; $total_time = $total_time + time_to_sec($c_t); } ?>

                                    <!--Next month weeks data-->
                                    <?php for($r=0;$r<count($next_weeks);$r++) {
                                        $user_work = $this->Report_model->getUserLogTime(array('project_id' => $project_id,'user_id' => $users[$s]['id_user'],'start_date' => $next_weeks[$r]['week_start_date'],'end_date' => $next_weeks[$r]['week_end_date']));
                                        $user_other_work = $this->Report_model->getUserLogTime(array('project_id_not' => $project_id,'user_id' => $users[$s]['id_user'],'start_date' => $next_weeks[$r]['week_start_date'],'end_date' => $next_weeks[$r]['week_end_date']));
                                        if(!empty($user_work)){
                                            $lt = $user_work[0]['time'];
                                        }
                                        if($lt==''){ $lt = '00:00'; }

                                        if(!empty($user_other_work)){
                                            $olt = $user_other_work[0]['time'];
                                        }
                                        if($olt==''){ $olt = '00:00'; }


                                        $total_week_estimation = 6;
                                        //echo $next_weeks[$r]['week_start_date'].'<br>';
                                        $week_estimation = getWorkingDays($next_weeks[$r]['week_start_date'],$next_weeks[$r]['week_end_date']);
                                        //echo $next_weeks[$r]['week_start_date'].'=='.$next_weeks[$r]['week_end_date'].'=='.$week_estimation;
                                        //$current_week_estimation = date('w',strtotime($next_weeks[$r]['week_start_date']));

                                        //$week_estimation = $total_week_estimation-$current_week_estimation;
                                        //echo $week_estimation; exit;
                                        if($week_estimation > 0) $week_estimation = $week_estimation*8; else $week_estimation = 0;
                                        //echo "<pre>"; print_r($week_estimation);
                                        $other_projects_estimation_details = $this->Report_model->getUserOtherProjectsEstimation(array('project_id' => $project_id,'user_id' => $users[$s]['id_user'],'week_start_date' => $next_weeks[$r]['week_start_date'],'week_end_date' => $next_weeks[$r]['week_end_date']));

                                        $other_projects_estimation_time = 0;
                                        if(!empty($other_projects_estimation_details)) $other_projects_estimation_time = $other_projects_estimation_details[0]['assigned_log'];

                                        $week_max_estimation_time = '00:00:00';


                                            $c_t = time_to_sec($week_estimation-($next_weeks[$r]['holiday']*8)) - time_to_sec($other_projects_estimation_time);
                                            $c_t = sec_to_time($c_t);
                                            $week_max_estimation_time = $c_t;


                                        $total__l_time = $total__l_time + time_to_sec($lt);
                                        $total__0_time = $total__0_time + time_to_sec($olt);

                                        ?>
                                        <td>
                                            <div class="project_plan_CW">
                                                <span><input type="text" disabled  value="<?=$c_t?>" style="width: 40%" name="" class="msk_time" id="" ></span>
                                                <span><b><?=$lt?></b></span>
                                                <span style="color: #5CB9EC"  title="<?=$user_other_work[0]['project_name']?>"><b><?=$olt?></b></span>
                                            </div>
                                            <!--<input type="text" class="msk_time" name="time" value="<?/*=$c_t*/?>" id="allotedHours"/></span>-->
                                        </td>

                                        <?php  $total_time = $total_time + time_to_sec($c_t); } ?>

                                    <td>
                                        <div class="project_plan_TD">
                                            <span><?=sec_to_time($total_time)?></span> /
                                            <span><?=sec_to_time($total__l_time)?></span> /
                                            <span style="color: #5CB9EC"> <?=sec_to_time($total__0_time)?></span>
                                        </div>
                                    </td>
                                </tr>
                            <?php }  ?>
                            <?php if(!count($users)){ ?>
                                <tr>
                                    <td colspan="6" >
                                        <div class="text-center padding5">No Team Members Found</div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div>
                </div>
            </div>
            <input type="hidden" name="project_id" value="<?=$project_id?>">


        </div>

        <script>
            TssLib.docReady(function () {
                $(".msk_time").mask("Hh:Mm");

                /*$(".msk_time").onchange(function(){
                    console.log(0);
                    getUpdateData(this);
                });*/
            });




        </script>