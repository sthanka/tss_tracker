
    <div class="modal fade" id="oak_popup">
        <div class="modal-dialog model-mg-width">
            <div class="modal-content model-mg-content"></div>
        </div>
    </div>
    <!-- Modal Dialog  dekete confirmation-->
    <div class="modal fade" id="oasis-confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        </div>
                        <h4 class="panel-title"></h4>
                    </div>
                    <div class="panel panel-default panel-body padding20 clearfix">
                        <p></p>
                    </div>
                    <div class="panel-footer col-sm-12 clearfix text-right">
                        <button type="button" class="button-common module btn-danger">Yes</button>
                        <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--@RenderBody()-->
    <!--Render Body Start-->
    <div class="mainpanel" id="budgetTemplateTbl_wrapper">
        <?php if($this->session->userdata('user_type_id') == 2 || ($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1) ){ ?>
        <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important"><!-- InstanceBeginEditable name="EditRegion3" -->
            <div class="contentHeader">
                <h3>Projects</h3>
            </div>
            <div class="col-sm-12 clearfix clearboth pad-right-none">
                <div class="grid-details-table">
                    <div class="grid-details-table-header">
                        <h3>Project List</h3>
                        <h3 class="pull-right dropdown">
                        <!--<a href="javascript:;" class="pull-right border-left"><i class="fa fa-trash-o"></i></a>-->
                            <?php if($this->session->userdata('user_type_id') == 2 or ($this->session->userdata('user_type_id')==3 and $this->session->userdata('is_manager')==1)){ ?>
                                <a href="<?= site_url('Project/projectCreation')?>" class="pull-right border-left"><i class="fa fa-plus"></i></a>
                            <?php } ?>
                    </h3>
                        <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="grid-details-table-content clearfix padding0">
                        <div class="tbl_wrapper border0">
                            <table id="project-list" class="table-responsive"></table>
                            <div id="project-list-page"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="add-project-modal" class="modal-wrapper" style="display:none">
                <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                                <h4 class="panel-title">Add Project</h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" class="form-control rval" name="project_name"  />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Project Name :</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <select id="client_id" name="client_id" type="text" class="form-control rval"/> </select>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Client Name :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth padding0">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" id="project_start_date" name="project_start_date" class="rval form-control tssDatepicker" />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Start Date :</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input_container">
                                                <input type="text" id="project_end_date" name="project_end_date" class="rval form-control tssDatepicker" />
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>End Date :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 clearfix clearboth">
                                    <div class="form-group">
                                        <div class="input_container">
                                            <textarea class="form-control rval" name="description"></textarea>
                                        </div>
                                        <label class="control-label"><span class="req-str">*</span>Description :</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 padding0 clearfix clearboth" >
                                    <div class="col-sm-12">
                                        <div class="form-group margin0">
                                            <div class="input_container">
                                                <select class="form-control rval" multiple name="department" id="department"></select>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Department :</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 padding0 clearfix clearboth status-drp">
                                    <div class="col-sm-12">
                                        <div class="form-group margin0">
                                            <div class="input_container">
                                                <select class="form-control rval" name="status" id="status">
                                                    <option value="in-progress">In Progress</option>
                                                    <option value="open">Open</option>
                                                    <option value="completed">Done</option>
                                                    <option value="reopen">Reopen</option>
                                                </select>
                                            </div>
                                            <label class="control-label"><span class="req-str">*</span>Status :</label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control rval" name="id_project" />
                            </div>
                            </div>
                            <div class="panel-footer text-center">
                                <button class="button-common" div-submit="true">Save</button>
                                <button class="button-color" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
            <script>
                var reference = "Allotted";
                TssLib.docReady(function () {
                    var $refTblgrid = $('#project-list');
                    var isDelLog = "<?php if($this->session->userdata('user_type_id') == 2){ echo true;}else{ echo false;} ?>";

                    $refTblgrid.jqGrid({
                        url: TssConfig.TT_SERVICE_URL + 'project/project_list',
                        multiselect: false,
                        datatype: "json",
                        sortorder: "desc",
                        extSearchField: '.searchInput',
                        colNames: ['Id', 'name','Project', 'Start Date', 'End Date', 'Status', 'pname','description','client_id','departments'],
                        colModel: [
                            { name: 'id_project', index: 'id_project', hidden: true ,key:true},
                            { name: 'project_name', index: 'pname', hidden: true },
                            {
                                name: 'project_name', index: 'project_name', formatter: function (c, o, d) {
                                return '<a href="' + TssConfig.TT_SERVICE_URL + 'project/projectCreation/'+ d.id_project +'">' + c + '</a>';
                            }
                            },
                            { name: 'project_start_date', index: 'project_start_date' },
                            { name: 'project_end_date', index: 'project_end_date' },
                            { name: 'status', index: 'status' },
                            { name: 'project_name', index: 'project_name', hidden: true },
                            { name: 'description', index: 'description', hidden: true },
                            { name: 'client_id', index: 'client_id', hidden: true },
                            { name: 'departments', index: 'departments', hidden: true }

                        ],
                        pager: 'project-list-page'
                    }).navGrid('#project-list-page', {
                        edit: isDelLog, add: isDelLog, del: isDelLog, search: false, refresh: true,
                        addfunc: function () {
                            //addEditCreateSchedule();
                            window.location.href = TssConfig.TT_SERVICE_URL + 'Project/projectCreation';
                        }, editfunc: function (id) {
                            var rowData = $refTblgrid.jqGrid('getRowData', id);
                            //console.log(id);
                            window.location.href = TssConfig.TT_SERVICE_URL + 'Project/projectCreation/'+id;
                            //addEditCreateSchedule(rowData);
                        }, delfunc: function (id) {
                            TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                                $(this).closest('.modal').modal('hide');
                                getJsonAsyncWithBaseUrl("project/delete/"+id, {}, {
                                    jsonContent: true,
                                    callback: function (result) {
                                        if (result.data.status ) {
                                            $('#project-list').trigger('reloadGrid');
                                            TssLib.notify('Deleted successfully ', null, 5);
                                        }
                                    }
                                });
                            }, 'Yes', 'No');
                        }
                    });

                });
                function dateFormat(c, o, d) {
                    if (TssLib.isBlank(c))
                        return '';
                    else
                        return c.DateWCF().format('d/m/Y');
                }
                function addEditCreateSchedule(rowData) {
                    var $modal = TssLib.openModel({ width: '600' }, $('#add-project-modal').html());
                    postJsonAsyncWithBaseUrl("client/getClients", {}, {
                        jsonContent: true,
                        callback: function (result) {
                            if (result.data != null) {
                                departmentsData = result.data;
                                TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                                TssLib.populateSelect($modal.find('#department'), { success: true, data: departmentsData.departments }, 'department_name', 'id_department');
                                if (TssLib.isBlank(rowData)) {
                                    $modal.find('.panel-title').text('Add Project');
                                    $modal.find('[div-submit="true"]').text('Save');
                                    $modal.find('.status-drp').hide();
                                } else {
                                    //console.log('departmentsData',rowData.departments.split(","));
                                    TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                                    TssLib.preSelectValForSelectBox($modal.find('#client_id'), rowData.client_id);
                                    $modal.find('#client_id').trigger('change');
                                    $modal.find('#department').val(rowData.departments.split(","));
                                    $modal.find('.panel-title').text('Edit Category');
                                    $modal.find('[div-submit="true"]').text('Update');
                                    $modal.find('.status-drp').hide();
                                }
                            }
                            else {

                            }
                        }
                    });
                    TssLib.ajaxForm({
                        jsonContent: true,
                        form: $('#linen-createSchedule-form'), callback: function (result) {
                            if (result.data.status) {
                                $('#project-list').trigger('reloadGrid');
                                TssLib.closeModal();
                                TssLib.notify('project added successfully ', null, 5);
                            }else{
                                TssLib.notify('project name already exist ', 'warn', 5);
                            }
                        },
                        before: function (formObj) {
                            var project_start_date = $(formObj).find("#project_start_date").val(),
                                project_end_date = $(formObj).find("#project_end_date").val(),
                                startDateParts = project_start_date.split('/'),
                                endDateParts = project_end_date.split('/');
                            if(new Date(startDateParts[2], parseInt(startDateParts[1], 10) - 1, startDateParts[0], '12', '00').getTime() > new Date(endDateParts[2], parseInt(endDateParts[1], 10) - 1, endDateParts[0], '12', '00').getTime()){ TssLib.notify('start date should greater than end date', 'warn', 5); return false; }
                            $(formObj).data('additionalData', { 'project_end_date': project_end_date, 'project_start_date':project_start_date});
                            $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'project/add_project');
                            return true;
                        }
                    });
                }

            </script>
            <!-- InstanceEndEditable -->
        <?php } else {?>
         <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
             <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Task List
                    </h3>
                </div>
                <div class="grid-details-table-content padding0">
                     <div class="col-sm-12 clearboth clearfix pb20">
                        <div class="col-sm-2">
                            <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select id="project_filter"  class="form-control" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Project:</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group padding0">
                                <div class="multi-select-container">
                                    <select class="form-control" id="project_status" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                        <option value="pending">Pending</option>
                                        <option value="accepted">Accepted</option>
                                        <option value="progress">In-progress</option>
                                        <option value="completed">Completed</option>
                                        <option value="hold">Hold</option>
                                    </select>
                                </div>
                                <label class="control-label col-sm-12">Status:</label>
                            </div>
                        </div>
                         <?php if($this->session->userdata('user_type_id') == 3 && $this->session->userdata('is_lead') == 1 ){ ?>
                         <div class="col-sm-2">
                             <div class="form-group padding0">
                                 <div class="multi-select-container">
                                     <select class="form-control" id="project_users" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                     </select>
                                 </div>
                                 <label class="control-label col-sm-12">Users:</label>
                             </div>
                         </div>
                         <?php } ?>
                         <div class="col-sm-2">
                             <div class="form-group padding0">
                                 <div class="input_container">
                                     <input type="text" class="rval form-control tssDatepicker" name="project_start_date" id="project_start_date" placeholder="yyyy-mm-dd">
                                 </div>
                                 <label class="control-label col-sm-12">Start Date:</label>
                             </div>
                         </div>
                         <div class="col-sm-2">
                             <div class="form-group padding0">
                                 <div class="input_container">
                                     <input type="text" class="rval form-control tssDatepicker" name="project_end_date" id="project_end_date" placeholder="yyyy-mm-dd">
                                 </div>
                                 <label class="control-label col-sm-12">End Date:</label>
                             </div>
                         </div>
                        <div class="col-sm-1">
                            <button onclick="filterSearch()" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                        </div>
                    </div>

                    <div class="grid-details-table-content clearfix padding0">
                        <div class="tbl_wrapper border0">
                            <table id="all-tasks-list" class="table-responsive"></table>
                            <div id="tasks-list-page"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         </div>
        <script>
            var reference = "Allotted";
            var $taskGrid = $('#all-tasks-list');
            TssLib.docReady(function () {

                $taskGrid.jqGrid({
                    rowNum: -1,
                    url: TssConfig.TT_SERVICE_URL + 'project/task_list',
                    multiselect: false,
                    viewrecords: false,
                    pgbuttons: false,     // disable page control like next, back button
                    pgtext: null,         // disable pager text like 'Page 0 of 10'
                    datatype: 'json',
                    cmTemplate: { sortable: false },
                    colNames: ['id_task_workflow','id_project','Project Title','Code', 'Task','Start Date', 'End Date','Est Time','Actual Time','Assigned By','Assigned To','Status'],
                    colModel: [
                        { name: 'id_task_workflow', index: 'id_task_workflow', hidden: true },
                        { name: 'projectTitle', index: 'projectTitle', hidden: true },
                        { name: 'id_project', index: 'id_project', hidden: true },
                        { name: 'code', index: 'code',width: 10 },
                        { name: 'parent_name', index: 'parent_name',width: 50,formatter: function (c, o, d) {
                                 if(d.projectTitle != 1)
                                    return '<a href="' + TssConfig.TT_SERVICE_URL + 'project/taskDetails/'+ d.id_project_task +'">' + d.task_name + '</a>';
                                 return c;
                            }
                        },
                        { name: 'start_date', index: 'StartDate',width: 25 },
                        { name: 'end_date', index: 'EndDate',width: 25 },
                        { name: 'estimated_time', index: 'EstTime',width: 20 },
                        { name: 'actual_time', index: 'actual_time',width: 20 },
                        { name: 'assigned_by', index: 'AssiBy',width: 80,hidden: true  },
                        { name: 'email', index: 'AssiTo',width: 80},
                        { name: 'current_status', index: 'Action',width: 40,  formatter: function(c,o,d){

                            var _html = '';

                            if(c.toLowerCase() == 'pending'){
                                _html += '<span class="pull-left task-status task-inprogress">Pending</span>';
                                if(d.assigned_to==<?=$this->session->userdata('user_id')?>) {
                                    _html+='<a class="link pull-right" onclick="showTimer(this)"><i class="fa fa-ellipsis-v font-18"></i></a>';
                                    _html += '<ul class="grid-actions">'
                                        + '<li><a href="javascript:;" onclick="changeTaskStatus(' + d.id_task_workflow + ',\'accepted\')">Accept</a></li>'
                                        + '<li><a href="javascript:;" onclick="changeTaskStatus(' + d.id_task_workflow + ',\'reject\')">Reject</a></li>'
                                        + '<li><a href="javascript:;" onclick="changeTaskStatus('+d.id_task_workflow+',\'release\')">Release</a></li>'
                                        + '</ul>';
                                }
                                else{
                                    //_html+='<a class="link pull-right" onclick="showTimer(this)"><i class="fa fa-ellipsis-v font-18"></i></a>';
                                }

                            }
                            if(c.toLowerCase() == 'accepted'){
                                _html += '<span class="pull-left task-status task-open">Accepted</span><span class="pull-left task-chat"></span><a class="link pull-right" onclick="showTimer(this)"><i class="fa fa-ellipsis-v font-18"></i></a>';
                                _html += '<ul class="grid-actions">'
                                    +'<li><a href="javascript:;" onclick="changeTaskStatus('+d.id_task_workflow+',\'progress\')">Start</a></li>'
                                    +'<li><a href="javascript:;" onclick="changeTaskStatus('+d.id_task_workflow+',\'hold\')">Hold</a></li>'
                                    +'<li><a href="javascript:;" onclick="changeTaskStatus('+d.id_task_workflow+',\'release\')">Release</a></li>'
                                    +'</ul>';
                            }
                            if(c.toLowerCase() == 'progress'){
                                _html += "<span class='pull-left task-status task-waiting'>In Progress</span><a class='link pull-right' onclick='showTimer(this)'><i class='fa fa-ellipsis-v font-18'></i></a>";
                                _html += "<ul class='grid-actions'>"
                                    +"<li><a href='javascript:;' onclick='changeTaskStatus("+d.id_task_workflow+",\"completed\")'>Completed</a></li>"
                                    +"<li><a href='javascript:;' onclick='changeTaskStatus("+d.id_task_workflow+",\"hold\")'>Hold</a></li>"
                                    +"<li><a href='javascript:;' onclick='changeTaskStatus("+d.id_task_workflow+",\"release\")'>Release</a></li>"
                                    +"<li><a href='javascript:;' onclick='logTime(this)' data-type='timer' task-object='"+JSON.stringify(d)+"'  >Log Time</a></li>"
                                    +"</ul>";
                            }
                            if(c.toLowerCase() == 'completed'){
                                _html += '<span class="pull-left task-status task-completed">Completed</span>';
                            }
                            if(c.toLowerCase() == 'hold'){
                                _html += '<span class="pull-left task-status task-open">Hold</span><span class="pull-left task-chat"></span><a class="link pull-right" onclick="showTimer(this)"><i class="fa fa-ellipsis-v font-18"></i></a>';
                                _html += '<ul class="grid-actions">'
                                    +'<li><a href="javascript:;" onclick="changeTaskStatus('+d.id_task_workflow+',\'progress\')">Start</a></li>'
                                   <?php if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')==0){ ?>
                                    +'<li><a href="javascript:;" onclick="changeTaskStatus('+d.id_task_workflow+',\'release\')">Release</a></li>'
                                   <?php } ?>
                                    +'</ul>';
                            }

                            return _html;
                        }
                        }
                    ],
                    gridComplete: function(){

                    },
                    loadComplete: function() {

                        var ids = $(this).jqGrid("getDataIDs"), l = ids.length, i, rowid, status;

                        var projectIndex = 0;
                        for (i = 0; i < l; i++) {
                            rowid = ids[i];
                            // get data from some column "readStatus"
                            var projectTitle = $(this).jqGrid("getCell", rowid, "projectTitle");
                            // or get data from some
                            //var rowData = $(this).jqGrid("getRowData', rowid);

                            // now you can set css on the row with some
                            if(projectTitle == 1){
                                $('#' + $.jgrid.jqID(rowid)).addClass('ProjectTitleRow');
                                projectIndex++;
                            } else{
                                $('#' + $.jgrid.jqID(rowid)).removeClass('ProjectTitleRow');
                            }
                        }
                        var currentValue = $(this).getGridParam('rowNum');
                        $(this).setGridParam({rowNum:currentValue+projectIndex});
                        setTimeout(function(){

                            resizeGrids();
                        }, 600);

                    }
                }).navGrid('#tasks-list-page', {
                    edit: false, add: false, del: false, search: false, refresh: true,
                    addfunc: function () {
                        addEditCreateSchedule();
                    }, editfunc: function (id) {
                        var rowData = $taskGrid.jqGrid('getRowData', id);
                        addEditCreateSchedule(rowData);
                    }, delfunc: function (id) {
                        TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                            $(this).closest('.modal').modal('hide');
                            TssLib.notify('Deleted successfully ', null, 5);
                        }, 'Yes', 'No');
                    }
                });
                postJsonAsyncWithBaseUrl("project/get_projects", {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            var  projects = result.data;
                            //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                            TssLib.populateSelect($('#project_filter'), { success: true, data: projects }, 'project_name', 'id_project');
                        }
                    }
                });
                postJsonAsyncWithBaseUrl("User/getDepUsers", {}, {
                    jsonContent: true,
                    callback: function (result) {
                        if (result.data != null) {
                            var  projects = result.data;
                            //TssLib.populateSelect($modal.find('#client_id'), { success: true, data: departmentsData.clients }, 'client_name', 'id_client');
                            TssLib.populateSelect($('#project_users'), { success: true, data: projects }, 'email', 'id_user');
                        }
                    }
                });
            });
            function filterSearch(){

                $taskGrid.jqGrid("setGridParam", {
                    rowNum: -1,
                    search: false,
                    postData:{
                        'projectIds':$('#project_filter').val(),
                        'projectStatus':$('#project_status').val(),
                        'project_users':$('#project_users').val(),
                        'project_start_date':$('#project_start_date').val().split("/").reverse().join("-"),
                        'project_end_date':$('#project_end_date').val().split("/").reverse().join("-")
                    }
                }).trigger("reloadGrid");
                //postData:{department_ids:$('#department_filter').val(), project_ids:$('#project_filter').val() },
                $('#all-tasks-list').trigger('reloadGrid');
            }

            function changeTaskStatus(task_id,status)
            {
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: WEB_BASE_URL+'index.php/Project/changeTaskStatus',
                    dataType: 'json',
                    data: {id_task_workflow:task_id,status:status},
                    success:function(res){
                        $taskGrid.jqGrid("setGridParam", {
                            rowNum: -1,
                            search: false
                        });
                        $('#all-tasks-list').trigger('reloadGrid');
                        if(res.status)
                        {

                            TssLib.notify('Task status updated Successfully');
                        }
                        else{
                            TssLib.notify(res.data, 'warn', 5);
                        }
                    }
                });
            }

        </script>
        <?php } ?>


        <!--<a href="javascript:;" class="footer-logo clearfix"><img src="<?/*=WEB_BASE_URL*/?>images/people-combine-logo.png" /></a>-->
    </div>
    <!--Render Body End-->


