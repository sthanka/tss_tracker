<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project_modal extends CI_Model
{
    public $key = '#@Tss_Tracker$#';

    function __construct(){
        $this->db->query('SET SQL_BIG_SELECTS=1');
    }

    function next_result()
    {
        if (is_object($this->conn_id))
        {
            return mysqli_next_result($this->conn_id);
        }
    }
    public function getProjectsGrid($paramArr)
    {

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'id_project';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if($sortField=='project_start_date'){
            $sortField = " STR_TO_DATE(project_start_date,'%e/%c/%Y') ";
        }else if($sortField=='project_end_date'){
            $sortField = " STR_TO_DATE(project_end_date,'%e/%c/%Y') ";
        }

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and p.project_name LIKE '%".$whereParam."%' or STR_TO_DATE(p.project_start_date, '%e/%c/%Y')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') or STR_TO_DATE(p.project_end_date, '%e/%c/%Y')=STR_TO_DATE('".$whereParam."','%e-%b-%Y')"; }
        $whereClause = "where true ".$whereParam;

        if($this->session->userdata('user_type_id')==3)
        {
            $user_id = $this->session->userdata('user_id');
            $SQL = "SELECT project.*, DATE_FORMAT(STR_TO_DATE(project.project_start_date, '%e/%c/%Y'), \"%d-%b-%Y\") project_start_date, DATE_FORMAT(STR_TO_DATE(project.project_start_date, '%e/%c/%Y'), \"%d-%b-%Y\") project_end_date, GROUP_CONCAT(pd.department_id) as departments from project_employees
            INNER JOIN project on project.id_project = project_employees.project_id
            LEFT JOIN project_department pd ON project.id_project = pd.project_id
            where project_employees.employee_id = $user_id

            GROUP BY project.id_project  order by $sortField $sortOrder $optLimit";

            $SQLCount = "select count(*) from project_employees
                        INNER JOIN project on project.id_project = project_employees.project_id
                        LEFT JOIN project_department pd ON project.id_project = pd.project_id
                        where project_employees.employee_id = $user_id GROUP BY project.id_project";
        }else{
            $SQL = "SELECT p.*, DATE_FORMAT(STR_TO_DATE(p.project_start_date, '%e/%c/%Y'), \"%d-%b-%Y\") as project_start_date, DATE_FORMAT(STR_TO_DATE(p.project_end_date, '%e/%c/%Y'), \"%d-%b-%Y\") as project_end_date, GROUP_CONCAT(pd.department_id) as departments FROM project p
                        LEFT JOIN project_department pd ON p.id_project = pd.project_id
                        $whereClause GROUP BY p.id_project order by $sortField $sortOrder $optLimit";

            $SQLCount = "select count(*) from project p
                        LEFT JOIN project_department pd ON p.id_project = pd.project_id
                        $whereClause GROUP BY p.id_project";
        }

        $result = $this->db->query($SQL);

        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();


        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getProjectTaskListGrid($paramArr,$type='list')
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'pt.id_project_task';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
        if(!empty($whereParam))  {
            $whereParam = specialCharReplace($whereParam);
            if($whereParam == 'In progress' || $whereParam == 'in progress') $whereParam = 'progress';
            if(preg_match_all('/:/', $whereParam)){
                if(preg_match_all('/-/', $whereParam)){
                    $whereParam = " AND DATE_FORMAT(pt.created_date_time, '%Y-%m-%d %H:%i:00')=STR_TO_DATE('".$whereParam."','%e-%b-%Y %H:%i:00') ";
                }else{
                    $whereParam = " AND pt.estimated_time like '%".$whereParam."%' ";
                }
            }else if(preg_match_all('/\d\/\d/', $whereParam)){
                $whereParam = "AND (DATE_FORMAT(pt.start_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%d/%m/%Y')
                 OR DATE_FORMAT(pt.end_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%d/%m/%Y')
                 OR DATE_FORMAT(pt.completed_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%d/%m/%Y')
                 )";
            }else{
                $whereParam = "and (pt.task_name LIKE '%".$whereParam."%'
                 OR pt.description like '%".$whereParam."%'
                 OR pt.task_type like '%".$whereParam."%'
                 OR r.requirement_name like '%".$whereParam."%'
                 )";
            }
        }
        if(isset($paramArr['projectIds'])){
            if(!empty($paramArr['projectIds'])){
                $whereParam .= ' AND pt.project_id IN ('.$paramArr['projectIds'].') ';
            }
        }
        if($sortField=='task_start_date'){
            $sortField = " STR_TO_DATE(task_start_date,\"%d-%b-%Y\") ";
        }else if($sortField=='task_end_date'){
            $sortField = " STR_TO_DATE(task_end_date,\"%d-%b-%Y\") ";
        }else if($sortField=='from_date'){
            $sortField = " pt.start_date ";
        }else if($sortField=='to_date'){
            $sortField = " pt.end_date ";
        }

        $whereClause = "where true AND pt.pm_approved=0 ".$whereParam;
        $SQL = "select *,DATE_FORMAT(pt.start_date, '%d-%b-%Y') as from_date,DATE_FORMAT(pt.end_date, '%d-%b-%Y ') as to_date, DATE_FORMAT(pt.start_date, '%e/%c/%Y') as start_date,DATE_FORMAT(pt.end_date, '%e/%c/%Y') as end_date, pm.module_name, pt.departments as id_department, pt.description as description, pt.requirement_id from project_task pt
                left join project p on pt.project_id=p.id_project
                left join requirements r on pt.requirement_id=r.id_requirements
                left JOIN department d on FIND_IN_SET(pt.departments, d.id_department)
                LEFT JOIN `project_module` `pm` ON `pt`.`project_module_id`=`pm`.`id_project_module`
          $whereClause GROUP BY id_project_task
          order by $sortField $sortOrder $optLimit";
        //echo $SQL; exit;

        $SQLCount = "select * from project_task pt
                  left join project p on pt.project_id=p.id_project
                  left join requirements r on pt.requirement_id=r.id_requirements
                  left JOIN department d on FIND_IN_SET(pt.departments, d.id_department)
                  LEFT JOIN `project_module` `pm` ON `pt`.`project_module_id`=`pm`.`id_project_module`
              $whereClause GROUP BY id_project_task
              order by $sortField $sortOrder ";

        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'records'=>$total_count,'page'=>$page);
        } else {
            return array();
        }
    }

    public function getTaskGrid($paramArr,$type='list')
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'id_task_flow';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
        if(!empty($whereParam))  {
            $whereParam = specialCharReplace($whereParam);
            if($whereParam == 'In progress' || $whereParam == 'in progress') $whereParam = 'progress';
            if(preg_match_all('/:/', $whereParam)){
                if(preg_match_all('/-/', $whereParam)){
                    $whereParam = " AND DATE_FORMAT(p.created_date_time, '%Y-%m-%d %H:%i:00')=STR_TO_DATE('".$whereParam."','%e-%b-%Y %H:%i:00') ";
                }else{
                    $whereParam = " AND p.estimated_time like '%".$whereParam."%' ";
                }
            }else{
                $whereParam = "and (p.task_name LIKE '%".$whereParam."%'
                 OR p.description like '%".$whereParam."%'
                 OR DATE_FORMAT(p.start_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y')
                 OR DATE_FORMAT(p.end_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y')
                 OR DATE_FORMAT(p.completed_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y')
                 OR tw.task_status LIKE '%".$whereParam."%' )";
            }
        }
        if(isset($paramArr['projectIds'])){
            if(!empty($paramArr['projectIds'])){
                $whereParam .= ' AND p.project_id IN ('.$paramArr['projectIds'].') ';
            }
        }
        if($sortField=='task_start_date'){
            $sortField = " STR_TO_DATE(task_start_date,\"%d-%b-%Y\") ";
        }else if($sortField=='task_end_date'){
            $sortField = " STR_TO_DATE(task_end_date,\"%d-%b-%Y\") ";
        }else if($sortField=='Action'){
            $sortField = " tw.task_status ";
        }

        $whereClause = "where true AND p.pm_approved=1 ".$whereParam;
        $SQL = "SELECT
                r.requirement_name,
                tw.id_task_flow,tm.allowted_time as current_estimated_time, DATE_FORMAT(p.created_date_time,\"%d-%b-%Y %H:%i\") as original_created_date_time,
                 DATE_FORMAT(p.start_date,\"%d/%m/%Y\")as start_date ,DATE_FORMAT(p.end_date,\"%d/%m/%Y\")as end_date,DATE_FORMAT(p.start_date,\"%d-%b-%Y\")as task_start_date ,DATE_FORMAT(p.end_date,\"%d-%b-%Y\")as task_end_date,DATE_FORMAT(p.completed_date,'%d-%b-%Y') completed_date,
                CONCAT(u.first_name, \" \", u.last_name) as user_name,tw.department_id ,u.email,
                p.id_project_task,pm.id_project_module,pm.module_name,p.status as current_status,
                tm.assigned_to,p.estimated_time,p.task_name,p.task_name project_task_name, pp.project_name,pp.id_project,p.description,lts.actual_time, p.departments as department_id, p.use_case, p.pm_approved, p.requirement_id, IFNULL(tw.additional_time,'00:00:00') additional_time
            FROM `task_flow` `tw`
                LEFT JOIN `project_task` `p` ON `tw`.`project_task_id`=`p`.`id_project_task`
                LEFT JOIN `requirements` `r` ON p.requirement_id=r.id_requirements
                LEFT JOIN `project` `pp` ON `pp`.`id_project` = `p`.`project_id`
                LEFT JOIN `project_module` `pm` ON `p`.`project_module_id`=`pm`.`id_project_module`
                LEFT JOIN `task_member` `tm` ON `tw`.`id_task_flow`=`tm`.`task_flow_id`
                LEFT JOIN `user` `u` ON `tm`.`assigned_to`=`u`.`id_user`
                LEFT JOIN `user_department` `ud` ON `ud`.`user_id`=`u`.`id_user`
                LEFT JOIN (select SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) as actual_time,project_task_id,user_id from
                 log_time lts GROUP BY project_task_id) lts ON `lts`.`project_task_id` = `tw`.`project_task_id` and
                `tm`.`assigned_to`=`lts`.`user_id`
          $whereClause GROUP BY tw.project_task_id
          order by $sortField $sortOrder $optLimit";

        //echo '<pre>'; print_r($SQL); exit;

        $SQLCount = "SELECT count(*)
          FROM `task_flow` `tw`
            LEFT JOIN `project_task` `p` ON `tw`.`project_task_id`=`p`.`id_project_task`
            LEFT JOIN `project` `pp` ON `pp`.`id_project` = `p`.`project_id`
            LEFT JOIN `project_module` `pm` ON `p`.`project_module_id`=`pm`.`id_project_module`
            LEFT JOIN `task_member` `tm` ON `tw`.`id_task_flow`=`tm`.`task_flow_id`
            LEFT JOIN `user` `u` ON `tm`.`assigned_to`=`u`.`id_user`
            LEFT JOIN `user_department` `ud` ON `ud`.`user_id`=`u`.`id_user`
            LEFT JOIN (select SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) as actual_time,project_task_id,user_id from
             log_time lts GROUP BY project_task_id) lts ON `lts`.`project_task_id` = `tw`.`project_task_id` and
            `tm`.`assigned_to`=`lts`.`user_id`
          $whereClause GROUP BY id_project_task";
        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($SQL);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            foreach($custlist as $k=>$v){
                $logPresent = $this->checkLogExist(array('task_flow_id'=> $v-> id_task_flow));
                if($logPresent){
                    $custlist[$k]->logPresent = true;
                    $logtime = $this->getTotalLogTime(array('project_task_id' => $v->id_project_task));
                    $custlist[$k]->totalLog = $logtime[0]['actual_time'];
                }else{
                    $custlist[$k]->logPresent = false;
                    $custlist[$k]->totalLog = '00:00';
                }
            }
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'records'=>$total_count,'page'=>$page);
        } else {
            return array();
        }
    }

    public function taskListGrid($paramArr)
    {
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sortField'])?$paramArr['sortField']:'id_project_task';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['parent_id'])?'pt.id_project_task='.$paramArr['parent_id']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (".$whereParam.")"; }
        if($this->session->userdata('user_type_id')==3)
        {   if($this->session->userdata('is_lead')!=1)
                $whereParam = $whereParam." and (tw.assigned_to=".$this->session->userdata('user_id').")";
            else
                $whereParam = $whereParam." and (ud.department_id=".$this->session->userdata('department_id').")";
        }
        $whereClause = "where true ".$whereParam;
        $whereClause = $whereClause.' and tw.status!="forward"';
        $whereClause = $whereClause.' and tw.status!="reject"';
        $whereClause = $whereClause.' and tw.status!="release"';
        $groupBy = ' GROUP BY tw.id_task_workflow ';

        $SQL = "SELECT CONCAT(tu.first_name,' ',tu.last_name) as assigned_to_user,CASE
                WHEN tw.status ='progress' THEN 'In progress' ELSE tw.status END as current_status,tw.estimated_time as current_estimated_time,pt.*,DATE_FORMAT(pt.start_date,'%d-%m-%Y') as s_date,DATE_FORMAT(pt.end_date,'%d-%m-%Y') as e_date,tw.assigned_to,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as actual_time
                FROM task_workflow tw
                left JOIN project_task pt on tw.project_task_id=pt.id_project_task
                left join user tu on tw.assigned_to=tu.id_user
                left join user_department ud on ud.user_id=tu.id_user
                left join log_time lt on tw.project_task_id=lt.project_task_id and lt.user_id=tu.id_user
                $whereClause $groupBy order by $sortField $sortOrder $optLimit ";
        //echo $SQL; exit;
        $SQLCount = "select count(*) FROM task_workflow tw
                       left JOIN project_task pt on tw.project_task_id=pt.id_project_task
                left join project p on pt.project_id=p.id_project
                left join user tu on tw.assigned_to=tu.id_user $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        //echo "<pre>"; print_r($SQL); exit;
        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getTaskList($data)
    {
        $SQL =  'select tt.name as task_type,l.log_time_id,l.project_task_id,l.comments,STR_TO_DATE( l.start_time, "%l:%i %p" )as start_time,STR_TO_DATE( l.end_time, "%l:%i %p" ) as end_time,pt.task_name,p.project_name,date(l.created_date_time) as date
                from log_time l
                left join user u on l.user_id=u.id_user
                left join user_department ud on u.id_user=ud.user_id
                left join task_type tt on l.task_type=tt.id_task_type
                left join project_task pt on l.project_task_id=pt.id_project_task
                left join project p on pt.project_id=p.id_project where l.user_id='. $this->session->userdata('user_id').'
                and ud.department_id='.$this->session->userdata('department_id').'
                AND Date(l.created_date_time) between STR_TO_DATE(\''.$data['start_date'].'\', \'%c/%e/%Y\') AND STR_TO_DATE(\''.$data['end_date'].'\', \'%c/%e/%Y\')';
        $result = $this->db->query($SQL);
        $result = $result->result_array();
        //echo $this->db->last_query(); exit ;
        $finalArr = [];
        //past
        foreach($result as $row) {
            //echo '<pre>'; print_r($row); exit;
            if($row['project_name']==''){
                $tem['title'] = $row['task_type'];
                $tem['types'] = 'break';
                $tem['color'] = '#F26422';
            }
            else{
                $tem['title'] = $row['project_name'];
                $tem['types'] = 'present';
                $tem['color'] = '#5CBA5C';
            }
            $tem['task'] = ($row['task_name'] == '') ? 'break' : $row['task_name'];
            $tem['description'] = $row['comments'];
            $tem['start'] = $row['date'].' '.$row['start_time'];
            $tem['end'] = $row['date'].' '. $row['end_time'];
            $tem['taskId'] = $row['log_time_id'];
            $tem['project_task_id'] = $row['project_task_id'];
            $tem['id_task_week_flow'] = 0;
            $finalArr[] = $tem;
        }

        $SQL = 'select twf.id_task_week_flow,p.project_name,pt.id_project_task,p.id_project,pt.task_name,pt.description,DATE_FORMAT(twf.date,"%Y-%m-%d") as date,twf.time,tf.project_task_id from task_week_flow twf
                left join task_flow tf on twf.task_flow_id=tf.id_task_flow
                left join project_task pt on tf.project_task_id=pt.id_project_task
                left join project p on pt.project_id=p.id_project
                where tf.department_id='.$this->session->userdata('department_id').' and twf.date >= "'.date('Y-m-d', strtotime("+1 days")) .'" and twf.user_id='.$this->session->userdata('user_id').'
                ORDER BY twf.date asc';
//        echo $SQL; exit;
        $result = $this->db->query($SQL);
        $result = $result->result_array();
        $time_extender = $start_time = $end_time = 0; $date = '';
        //echo '<pre>'; print_r($result); exit;
        //future
        $break_time = false;
        //date seperated
        /*foreach($result as $row) {

        }*/
        foreach($result as $row) {
            //echo '<pre>'; print_r($row); exit;
            if($date!=$row['date']){
                $time_extender = 0;
                $break_time  = false;
            }

            if($time_extender==0) {
                $start_time = '09:00:00';
            }
            else{
                $start_time = sec_to_time($time_extender);
            }

            $end_time = sec_to_time(time_to_sec($start_time)+time_to_sec($row['time']));
            $time_extender = time_to_sec($end_time);
            if(strtotime($end_time) >= strtotime('13:00:00') && !$break_time){
                $break_time  = true;
                $date = $row['date'];
                $tem['color'] = '#0072BB';
                $tem['types'] = 'future';
                $tem['projectId'] = $row['id_project'];
                //echo $start_time.'--'.$end_time.'<br>';
                $tem['title'] = $row['project_name'];
                $tem['task'] = $row['task_name'];
                $tem['description'] = $row['description'];
                $tem['start'] = $row['date'] .' '.$start_time;
                $tem['end'] = $row['date'].' '.'13:00:00';
                $tem['taskId'] = $row['id_project_task'];
                $tem['project_task_id'] = 0;
                $tem['id_task_week_flow'] = $row['id_task_week_flow'];
                $finalArr[] = $tem;
                /* lunch break label*/
                $tem['color'] = '#ff150e';
                $tem['types'] = '';
                $tem['project_task_id'] =  $tem['projectId'] = 0;
                $tem['title'] = 'Lunch Time';
                $tem['task'] = '';
                $tem['start'] = $row['date'] .' '.'13:00:00';
                $tem['end'] = $row['date'].' '.'14:00:00';
                $finalArr[] = $tem;
                 /* end lunch break label*/

                $remaining_time = sec_to_time(time_to_sec($row['time']) - (time_to_sec('13:00:00')-time_to_sec($start_time)));
                $start_time = '14:00:00';
                $end_time =sec_to_time(time_to_sec($start_time) + time_to_sec($remaining_time));
                $time_extender = time_to_sec($end_time);
            }

            $date = $row['date'];
            $tem['color'] = '#0072BB';
            $tem['types'] = 'future';
            $tem['projectId'] = $row['id_project'];
            //echo $start_time.'--'.$end_time.'<br>';
            $tem['title'] = $row['project_name'];
            $tem['task'] = $row['task_name'];
            $tem['description'] = $row['description'];
            $tem['start'] = $row['date'] .' '.$start_time;
            $tem['end'] = $row['date'].' '.$end_time;
            $tem['taskId'] = $row['id_project_task'];
            $tem['project_task_id'] = 0;
            $tem['id_task_week_flow'] = $row['id_task_week_flow'];
            if($start_time!=$end_time)
                $finalArr[] = $tem;
        }
        //echo "<pre>"; print_r($finalArr); exit;
        //For holidays list
        /*$SQL =  'select id_holiday, DATE_FORMAT(date, \'%Y-%m-%d\') date, title, description
                from holidays
                WHERE Date(date) between STR_TO_DATE(\''.$data['start_date'].'\', \'%c/%e/%Y\') AND STR_TO_DATE(\''.$data['end_date'].'\', \'%c/%e/%Y\')';
        $result = $this->db->query($SQL);
        $result = $result->result_array();

        foreach($result as $k=>$v){
            $tem = [];
            $tem['color'] = '#7D9EA9';
            $tem['types'] = 'holiday';
            $tem['projectId'] = '';
            $tem['title'] = $v['title'];
            $tem['task'] = 'holiday';
            $tem['description'] = $v['description'];
            $tem['start'] = $v['date'] .' 09:00:00';
            $tem['end'] = $v['date'].' 20:00:00';
            $tem['taskId'] = 0;
            $tem['project_task_id'] = 0;
            $tem['id_task_week_flow'] = 0;
            $finalArr[] = $tem;
        }*/

        return $finalArr;
    }
    public function getProjects()
    {
        $where = '';

        if($this->session->userdata('user_type_id') != 2 ){ $where = ' WHERE pe.employee_id='.$this->session->userdata('user_id'); }

        $SQL =  'SELECT p.* from project p LEFT JOIN project_employees pe ON p.id_project = pe.project_id '.$where.' GROUP BY p.id_project ORDER BY p.id_project DESC';
        $result = $this->db->query($SQL);
        if(isset($data['user_id']) && ($this->session->userdata('user_type_id')!=3)){
            $this->db->from('project');
        }
        //echo $this->db->last_query().'---';
        return $result->result_array();
    }
    public function getProjectList($data='')
    {
        $this->db->select('id_project as project_id,project_name');
        $this->db->from('project');
        if(isset($data['status']))
            $this->db->where('status', $data['status']);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getDepartmentList($data='')
    {
        $this->db->select('*');
        $this->db->from('department');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getProjectDetailsById($projectId)
    {
            $where = 'where id_project='.$projectId;

            $SQL =  "SELECT p.*,
                    DATE_FORMAT(STR_TO_DATE(p.project_start_date, '%e/%c/%Y %H:%i'), '%d-%b-%Y') as project_start_date,
                    DATE_FORMAT(STR_TO_DATE(p.project_end_date, '%e/%c/%Y %H:%i'), '%d-%b-%Y') as project_end_date,
                    datediff(DATE_FORMAT(STR_TO_DATE(p.project_end_date, '%e/%c/%Y %H:%i'), '%Y-%m-%d %H:%m:%s') , DATE_FORMAT(STR_TO_DATE(p.project_start_date, '%e/%c/%Y %H:%i'), '%Y-%m-%d %H:%m:%s') ) as totalDays,
                    datediff(DATE_FORMAT(STR_TO_DATE(p.project_end_date, '%e/%c/%Y %H:%i'), '%Y-%m-%d %H:%m:%s') , date(now()) ) as balanceDays,
                    count(pe.id_employee) as member"
                . " from project p LEFT JOIN project_employees pe ON p.id_project = pe.project_id ".$where." GROUP BY p.id_project ORDER BY p.id_project DESC";
            $result = $this->db->query($SQL);
            if(isset($data['user_id']) && ($this->session->userdata('user_type_id')!=3)){
                $this->db->from('project');
            }
            return $result->result_array();

    }
    public function getProjectWorkloadPercentage($projectId){
        $final = [];
        $userlist = [];
        $qry = "select u.id_user, CONCAT_WS(' ',u.first_name, u.last_name) as name, u.email
                from project_employees pe
                LEFT JOIN user u ON pe.employee_id=u.id_user
                where pe.project_id=".$projectId."
                 order by pe.employee_id";
        //echo $qry; exit;
        $res_all = $this->db->query($qry);
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                foreach($res_all as $k=>$v){
                    $userlist[$v->id_user] = $v;
                    //SUM(TIME_TO_SEC(STR_TO_DATE(IFNULL(tw.estimated_time,0), '%H:%i:%s %p')) + TIME_TO_SEC(STR_TO_DATE(IFNULL(tw.additional_time,0), '%H:%i:%s %p'))) total
                    $qryAssignedTask = "select tw.id_task_flow,tm.assigned_to,pt.id_project_task, concat_ws(' ',u.first_name,u.last_name) as name,u.email,
                        SUM(TIME_TO_SEC(tm.allowted_time)) as total
                        from task_flow tw
                        left join project_task pt on tw.project_task_id=pt.id_project_task
                        left join task_member tm on tw.id_task_flow=tm.task_flow_id
                        left join user u ON tm.assigned_to=u.id_user
                        where tm.assigned_to=".$v->id_user."
                        and pt.project_id=".$projectId."
                        group by tm.assigned_to";
                    //echo $qryAssignedTask; exit;
                    $qryActualTask = "select lt.user_id, TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(lt.duration))),'%H:%i') as logged_duration,SUM(TIME_TO_SEC(lt.duration)) as duration, concat_ws(' ',u.first_name,u.last_name) as name, u.email, lt.project_task_id as task_id
                        from log_time lt
                        left join user u ON lt.user_id=u.id_user
                        WHERE lt.user_id=".$v->id_user."
                        and lt.project_id=".$projectId;
                    //echo $qryActualTask; exit;
                    //echo round( 1860/3600,2); exit;
                    $qryAssignedTask = $this->db->query($qryAssignedTask)->result();
                    $qryActualTask = $this->db->query($qryActualTask)->result();
                    if(count($qryAssignedTask)>0){
                        $final[$qryAssignedTask[0]->assigned_to] = array(
                            'id_user' => $qryAssignedTask[0]->assigned_to,
                            'name' => $qryAssignedTask[0]->name,
                            'email' => $qryAssignedTask[0]->email,
                            'total' => number_format($qryAssignedTask[0]->total/3600, 2, '.', ''),
                            'loggedTime' => 0,
                            'loggedTimePercentage' =>0
                        );
                    }else{
                        $final[$v->id_user] = array(
                            'id_user' => $v->id_user,
                            'name' => $v->name,
                            'email' => $v->email,
                            'total' => 0,
                            'loggedTime' => 0,
                            'loggedTimePercentage' =>0
                        );
                    }
                    if(count($qryActualTask)>0 && isset($qryActualTask[0]->user_id)){
                        $final[$qryActualTask[0]->user_id] = array(
                            'id_user' => $qryActualTask[0]->user_id,
                            'name' => $qryActualTask[0]->name,
                            'email' => $qryActualTask[0]->email,
                            'total' => round(isset($qryAssignedTask[0])?($qryAssignedTask[0]->total/3600):0,2),
                            //'loggedTime' =>round( $qryActualTask[0]->duration/3600,2),
                            'loggedTime' =>(float)str_replace(':','.',$qryActualTask[0]->logged_duration),
                            'loggedTimePercentage' => (isset($qryAssignedTask[0]->duration) && $qryAssignedTask[0]->duration>0)?(((int)$qryActualTask[0]->duration / $qryAssignedTask[0]->total) * 100):0,
                        );
                    }else if(isset($qryActualTask[0]->user_id)){
                        $final[$qryActualTask[0]->id_user] = array(
                            'id_user' => $qryActualTask[0]->user_id,
                            'name' => $qryActualTask[0]->name,
                            'email' => $qryActualTask[0]->email,
                            'total' => isset($qryAssignedTask[0])?($qryAssignedTask[0]->total/3600):0,
                            'loggedTime' => 0,
                            'loggedTimePercentage' => 0,
                        );
                    }
                }
                return array_values($final);
            }else{
                return [];
            }
        }
    }
    public function getDetailOfProject($projectId)
    {
        $where = 'where id_project='.$projectId;

        $SQL =  "SELECT p.*,
                    p.project_start_date,
                    p.project_end_date,
                    datediff(DATE_FORMAT(STR_TO_DATE(p.project_end_date, '%e/%c/%Y %H:%i'), '%Y-%m-%d %H:%m:%s') , DATE_FORMAT(STR_TO_DATE(p.project_start_date, '%e/%c/%Y %H:%i'), '%Y-%m-%d %H:%m:%s') ) as totalDays,
                    datediff(DATE_FORMAT(STR_TO_DATE(p.project_end_date, '%e/%c/%Y %H:%i'), '%Y-%m-%d %H:%m:%s') , date(now()) ) as balanceDays,
                    count(pe.id_employee) as member"
            . " from project p LEFT JOIN project_employees pe ON p.id_project = pe.project_id ".$where." GROUP BY p.id_project ORDER BY p.id_project DESC";
        $result = $this->db->query($SQL);
        if(isset($data['user_id']) && ($this->session->userdata('user_type_id')!=3)){
            $this->db->from('project');
        }
        return $result->result_array();

    }
    public function addProject($data)
    {
        $sdata = array(
            'project_name'=>$data['project_name'],
            'client_id'=>$data['client_id'],
            'project_start_date'=>$data['project_start_date'],
            'project_end_date'=>$data['project_end_date'],
            'description'=>$data['description'],
            'project_manager'=>$data['project_manager'],
            'status'=>$data['status']
        );
        $this->db->insert('project', $sdata);
        $project_id = $this->db->insert_id();
        if(isset($data['department'])){
            $departmentArry = array();
            if(!is_array($data['department'])){ $data['department'] = explode(',',$data['department']);}
            foreach($data['department'] as $item ){
                $departmentArry[] = array('project_id'=>$project_id,'department_id'=>$item);
            }
            $this->db->insert_batch('project_department', $departmentArry);
        }
        return $project_id;
    }
    public function updateProject($id,$data)
    {
        $sdata = array(
            'project_name'=>$data['project_name'],
            'client_id'=>$data['client_id'],
            'project_start_date'=>$data['project_start_date'],
            'project_end_date'=>$data['project_end_date'],
            'description'=>$data['description'],
            'project_manager'=>$data['project_manager'],
            'status'=>$data['status']
        );
        $this->db->where('id_project', $id);
        $this->db->update('project', $sdata);
        if(isset($data['department'])){
            $departmentArry = array();
            if(!is_array($data['department'])){ $data['department'] = explode(',',$data['department']);}
            foreach($data['department'] as $item ){
                $departmentArry[] = array('project_id'=>$id,'department_id'=>$item);
            }
            $this->db->insert_batch('project_department', $departmentArry);
        }
        return $id;
    }
    public function addProjectDeptTeam($data,$mode = 1){
        $employeeArry = array();
        if(!is_array($data['employee_id'])){ $data['employee_id'] = explode(',',$data['employee_id']);}
        foreach($data['employee_id'] as $item ){
            $employeeArry[] = array('project_id'=>$data['project_id'],'employee_id'=>$item);
        }
        $this->db->insert_batch('project_employees', $employeeArry);
        if($mode){
            $this->db->insert('project_department',  array('project_id'=>$data['project_id'],'department_id'=>$data['department_id'],'estimation_time'=>$data['estimation_time'],'estimation_percentage'=>$data['estimation_percentage']));
        }else{
            $this->db->where('department_id', $data['department_id']);
            $this->db->where('project_id', $data['project_id']);
            $this->db->update('project_department',  array('estimation_time'=>$data['estimation_time'], 'estimation_percentage'=>$data['estimation_percentage']));
        }
    }
    public function addProjectMilestone($data){
        $this->db->insert('project_milestones', $data);
        return $this->db->insert_id();
    }
    public function updateProjectMilestone($data, $id){
        $this->db->where('id_milestone',$id);
        return $this->db->update('project_milestones', $data);
    }
    public function addProjectOpenItem($data){
        $this->db->insert('project_open_items', $data);
        return $this->db->insert_id();
    }
    public function updateProjectOpenItem($data, $id){
        $this->db->where('id_project_item',$id);
        return $this->db->update('project_open_items', $data);
    }
    public function delete($id)
    {
        $this->db->delete('project_department', array('project_id' => $id));
        $this->db->where('id_project', $id);
        return $this->db->delete('project');
    }
    public function checkProjectName($id,$name){
        $this->db->select('*');
        if($id != ''){
            $this->db->where('id_project !=', $id);
        }
        $this->db->where('project_name', $name);
        $this->db->from('project');
        $query = $this->db->get();
        return $query->result_array();

    }
    public function overview($id)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('id_project', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function addMember($data)
    {
        $this->db->select('*');
        $this->db->from('project_employees');
        $this->db->where('project_id', $data['project_id']);
        $this->db->where('employee_id', $data['employee_id']);
        $query = $this->db->get();
        $row = $query->result_array();
        if(!empty($row)){
            return 0;
        }else{
            $this->db->insert('project_employees', $data);
            return $this->db->insert_id();
        }

    }
    public function deleteOpenItem($id){
        $this->db->delete('project_open_items', array('id_project_item' => $id));
        return true;
    }
    public function projectUserDelete($id){
        $this->db->where('id_employee', $id);
        return $this->db->delete('project_employees');
    }
    public function getProjectUsers($id)
    {
        $this->db->select('*');
        $this->db->from('project_employees pe');
        $this->db->where('project_id', $id);
        $this->db->join('user u', 'pe.employee_id = u.id_user','left');
        $query = $this->db->get();
        return $query->result_array();
    }
    /*
   * excel upload
   */
    /**
     * @param $data
     * @return mixed
     */
    public function createExcelProject($data)
    {
        //check project by project name
        $this->db->select('*');
        $this->db->where('project_name',$data['project_name']);
        if(isset($data['project_id']))
            $this->db->where('id_project',$data['project_id']);
        $this->db->from('project');
        $query = $this->db->get();
        $projectList = $query->row();
        if($projectList){
            return $projectList->id_project;
        }else{
            //$this->db->insert('project',array('project_name'=>$data['project_name']));
            return false;
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createExcelModule($data)
    {
        //check project by project name
        $this->db->select('*');
        $this->db->where('module_name',$data['module_name']);
        $this->db->where('project_id',$data['project_id']);
        $this->db->from('project_module');
        $query = $this->db->get();

        $moduleList = $query->row();
        if($moduleList){
            return $moduleList->id_project_module;
        }else{
            $this->db->insert('project_module',array('module_name'=>$data['module_name'],'project_id'=>$data['project_id']));
            return $this->db->insert_id();
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function getDepartmentEstimationTime($data)
    {
        //echo '<pre>'; print_r($data); exit;
        $projectId = isset($data['project_id']) ? $data['project_id'] : 0;
        $departmentId = isset($data['department_id']) ? $data['department_id'] : 0;
        $taskId = isset($data['task_id']) ? $data['task_id'] : 0;

        // get project department details
        $this->db->select('*');
        $this->db->where(array('project_id'=>$projectId,'department_id'=> $departmentId));
        $this->db->from('project_department');
        $query = $this->db->get();
        $projectDepartmentDetails = $query->row();
        //echo "<pre>"; print_r($projectDepartmentDetails); exit;
        if(empty($projectDepartmentDetails)) return false;
        $estimationPercentage = $projectDepartmentDetails->estimation_percentage;
        if(empty($estimationPercentage)) return false;

        // get task details
        $this->db->select('*');
        $this->db->where(array('id_project_task'=>$taskId));
        $this->db->from('project_task');
        $query = $this->db->get();
        $taskDetails = $query->row();
        if(empty($taskDetails)) return false;
        $taskEstimatedTime = $taskDetails->estimated_time;
        if(empty($taskEstimatedTime)) return false;
        list($estimationHours,$estimationMinutes) = explode(':',$taskEstimatedTime);
        $totalEstimationMinutes = $estimationHours*60 + $estimationMinutes;
        $departmentEstimationMinutes = (int) ($totalEstimationMinutes * $estimationPercentage/100);
        //echo $departmentEstimationMinutes; exit;
        return floor($departmentEstimationMinutes/60).':'.($departmentEstimationMinutes % 60).':00';
    }
    public function createExcelTask($data,$type='task')
    {   //echo "<pre>"; print_r($data); exit;
        //getting task start date sprint
        /*$this->db->select('*');
        $this->db->from('sprint');
        $this->db->where('start_date',$data['start_date']);
        $query = $this->db->get();
        $sprint_data = $query->result_array();*/
//echo "<pre>"; print_r($data); exit;

        $department_id = $data['department_id'];

        $this->db->select('*');
        //$this->db->where(array('project_id'=>$data['project_id'],'project_module_id'=>$data['module_id'],'requirement_id' => $requirement_id,'task_name'=>$data['task_name']));
        $this->db->where(array('project_id'=>$data['project_id'],'requirement_id' => $data['requirement_id'],'task_name'=>$data['task_name']));
        $this->db->from('project_task');
        $query = $this->db->get();
        $taskList = $query->row();
//die($this->db->last_query());
        /*$taskData = array(
            'project_id'=>$data['project_id'],
            'estimated_time'=>$data['estimated_time'],
            'project_module_id'=>$data['module_id'],
            'task_name'=>$data['task_name'],
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'status'=>'new',
            'description'=>$data['description'],
            'task_type'=>$data['task_type'],
        );*/
        $taskData = array(
            'project_id'=>$data['project_id'],
            'project_module_id'=>$data['module_id'],
            'requirement_id'=>$data['requirement_id'],
            'task_name'=>$data['task_name'],
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'status'=>'new',
            'description'=>$data['description'],
            'task_type'=>$data['task_type'],
            'use_case'=>$data['use_case'],
            'departments' => join(',',($data['department_id'])),
        );
        if($taskList){
            if($type!='sub') {
                //$this->db->where('id_project_task', $taskList->id_project_task);
                //$this->db->update('project_task', $taskData);

                //echo $this->db->last_query(); exit;
                /*$departmentEstimationTime = $this->getDepartmentEstimationTime(array('project_id' => $data['project_id'],'department_id' => $department_id[0],'task_id'=> $taskList->id_project_task));
                $this->db->where('id_project_task', $taskList->id_project_task);
                $this->db->update('project_task', array('department_estimated_time' => $departmentEstimationTime));*/

                //echo '<pre>'; print_r($taskList); exit;

                //changed to when project manager approved then task flow will be created
                /*foreach($department_id as $id){
                    $taskWorkData = array(
                        'estimated_time' => $departmentEstimationTime,
                        'start_date'=>$data['start_date'],
                        'end_date'=>$data['end_date'],
                        'actual_end_date'=>$data['end_date']
                    );
                    $this->db->where('project_task_id', $taskList->id_project_task);
                    $this->db->where('department_id', $id);
                    $this->db->update('task_flow', $taskWorkData);
                }*/
            }
            return array('task_id'=>$taskList->id_project_task,'message'=>'Updated Task');
        }else{
            $this->db->insert('project_task',$taskData);
            $task_id = $this->db->insert_id();
            /*$departmentEstimationTime = $this->getDepartmentEstimationTime(array('project_id' => $data['project_id'],'department_id' => $department_id[0],'task_id'=> $task_id));
            $this->db->where('id_project_task', $task_id);
            $this->db->update('project_task', array('department_estimated_time' => $departmentEstimationTime));*/
            if($type!='sub'){
                ////changed to when project manager approved then task flow will be created
                /*foreach($department_id as $id){
                    $taskWorkData = array(
                        'project_task_id'=>$task_id,
                        'estimated_time'=> $departmentEstimationTime,
                        'start_date'=>$data['start_date'],
                        'end_date'=>$data['end_date'],
                        'actual_end_date'=>$data['end_date'],
                        'department_id'=>$id
                    );
                    $this->db->insert('task_flow',$taskWorkData);
                    $this->db->insert_id();
                }*/
            }
            return array('task_id'=>$task_id,'message'=>'Created Task');
        }
    }

    public function createTask($data,$type='task')
    {

        $department_id = $data['department_id'];

        $taskData = array(
            'project_id'=>$data['project_id'],
            'project_module_id'=>$data['module_id'],
            'task_name'=>$data['task_name'],
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'status'=>'new',
            'description'=>$data['description'],
            'use_case'=>$data['use_case'],
            'departments'=>$department_id,
            'requirement_id'=>$data['requirement_id'],
            'task_type'=>$data['task_type'],
        );
        $this->db->insert('project_task',$taskData);
        $task_id = $this->db->insert_id();
        /*$taskWorkData = array(
            'project_task_id'=>$task_id,
            'estimated_time'=> 0,
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'actual_end_date'=>$data['end_date'],
            'department_id' => $department_id
        );
        $this->db->insert('task_flow',$taskWorkData);
        $this->db->insert_id();*/

        return array('task_id'=>$task_id,'message'=>'Created Task');
    }

    public function getProjectDepartmentsEstTime($data) {
        $this->db->select('*,TIME_FORMAT(td.time,"%H:%i") as esti_time');
        $this->db->from('task_department td');
        $this->db->join('department d','td.department_id=d.id_department','left');
        if(isset($data['project_id']))
            $this->db->where('td.project_id', $data['project_id']);

        if(isset($data['task_id']))
            $this->db->where('td.task_id', $data['task_id']);

        if(isset($data['department_id']))
            $this->db->where('td.department_id', $data['department_id']);
        $this->db->order_by('d.order','ASC');
        $result = $this->db->get()->result_array();

        foreach($result as $k=>$v){
            $this->db->select('IFNULL(TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(IFNULL(tm.allowted_time, "00:00")))), \'%H:%i\'), "00:00") as allotted_time,
        IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(IFNULL(tm.allowted_time, "0")))-SUM(TIME_TO_SEC(IFNULL(tm.additional_time, "0")))), \'%H:%i\'), "00:00") est_time,
        IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(IFNULL(tm.additional_time, "0")))), \'%H:%i\'), "00:00") add_time, tf.task_status, pt.status status');
            $this->db->from('task_member tm');
            $this->db->join('task_flow tf','tm.task_flow_id=tf.id_task_flow','left');
            $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
            if(isset($data['task_id']))
                $this->db->where('tf.project_task_id', $data['task_id']);
            if(isset($data['task_id']))
                $this->db->where('tf.department_id', $v['department_id']);

            $res = $this->db->get()->result_array();

            $result[$k]['task_status'] = '';
            $result[$k]['est_time'] = 0;
            $result[$k]['add_time'] = 0;
            foreach($res as $k1=>$v1){
                $result[$k]['est_time'] = sec_to_time(time_to_sec($result[$k]['est_time']) + time_to_sec($v1['est_time']));
                $result[$k]['add_time'] = sec_to_time(time_to_sec($result[$k]['add_time']) + time_to_sec($v1['add_time']));
                $result[$k]['task_status'] = $v1['task_status']?$v1['task_status']:'Not yet assigned';
            }
        }
        return $result;
    }

    public function deletetaskDepartment($data){
        if(isset($data['project_id']))
            $this->db->where('project_id' , $data['project_id']);

        if(isset($data['task_id']))
            $this->db->where('project_id' , $data['task_id']);

        if(isset($data['department_id']))
            $this->db->where('department_id' , $data['department_id']);

        $this->db->delete('task_department');
    }

    public function createExceltaskDepartment($data)
    {
        $this->db->select('*');
        $this->db->where(array('task_id'=>$data['task_id'],'department_id'=>$data['department_id']));
        $this->db->from('task_department');
        $query = $this->db->get();
        $list = $query->result_array();
        if(empty($list)){
            $this->db->insert('task_department', $data);
            $task_department_id = $this->db->insert_id();
        }
        else{
            $this->db->where('id_task_department', $list[0]['id_task_department']);
            $this->db->update('task_department',$data);
            $task_department_id = $list[0]['id_task_department'];
        }
        return array('task_id'=>$task_department_id,'message'=>'add task time for department');
    }

    public function getTaskDepartment($data)
    {
        $this->db->select('*');
        $this->db->where(array('task_id'=>$data['task_id'],'department_id'=>$data['department_id']));
        $this->db->from('task_department');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addSprint($data)
    {
        $this->db->insert('sprint', $data);
        return $this->db->insert_id();
    }

    public function updateSprint($data)
    {
        $this->db->where('id_sprint', $data['id_sprint']);
        $this->db->update('sprint', $data);
        return 1;
    }

    public function getSprint($data)
    {
        $this->db->select('*');
        $this->db->from('sprint');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['id_sprint']))
            $this->db->where('id_sprint',$data['id_sprint']);
        if(isset($data['name']))
            $this->db->where('name',$data['name']);
        if(isset($data['sprint_id_not']) && $data['sprint_id_not']!='' && $data['sprint_id_not']!=0)
            $this->db->where('id_sprint!=',$data['sprint_id_not']);
        if(isset($data['week_day'])) {
            //$this->db->where('YEARWEEK(`start_date`, 1) = YEARWEEK("' . $data['week_day'] . '", 1)');
            $this->db->where('"'.$data["week_day"].'" BETWEEN start_date AND end_date');
        }

        $this->db->order_by('id_sprint','desc');
        $query = $this->db->get();
        //echo $this->db->last_query().'---';
        return $query->result_array();
    }

    public function getUnassignedProjectTask($data)
    {   //echo "<pre>"; print_r($data);
        $this->db->select('p.*,pm.id_project_module,pm.module_name');
        $this->db->from('project_task p');
        $this->db->join('project_module pm','p.project_module_id=pm.id_project_module','left');
        $this->db->join('sprint s','p.sprint_id=s.id_sprint','left');
        if(isset($data['sprint_id']))
            $this->db->where('p.sprint_id',$data['sprint_id']);
        //$this->db->where('p.id_project_task in (select parent_id from project_task where project_id='.$data['project_id'].' and parent_id!=0 and (`user_id` IS NULL or user_id=0))');
        if(isset($data['project_id']))
            $this->db->where('p.project_id',$data['project_id']);
        if(isset($data['user_id']) && ($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')!=1))
            $this->db->where('p.user_id',$data['user_id']);

        if(isset($data['week_date'])) {
            //$this->db->where('(s.start_date <= CURDATE() and CURDATE() <= s.end_date)');
            $this->db->where('YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1)');
        }

        //$this->db->where('(`p.user_id` IS NULL or p.user_id=0)');
        $this->db->where('p.id_project_task not in (

        SELECT pw.project_task_id
            FROM `project_task` `p`
                LEFT JOIN `task_workflow` `pw` ON `p`.`id_project_task`=`pw`.`project_task_id`
                LEFT JOIN `user` `u` ON `pw`.`assigned_to`=`u`.`id_user`
                LEFT JOIN `sprint` `s` ON `s`.`id_sprint`=p.sprint_id WHERE YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1)
                AND `p`.`project_id` = '.$data['project_id'].'

        )');
        $query = $this->db->get('');
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function getAssignedProjectTask($data)
    {
        $this->db->select('tw.id_task_workflow,tw.estimated_time as current_estimated_time,CONCAT(u.first_name," ",u.last_name) as user_name,u.email,p.*,p.task_name as parent_name,p.id_project_task as parent_id,pm.id_project_module,pm.module_name');
        $this->db->from('task_workflow tw');
        $this->db->join('project_task p','tw.project_task_id=p.id_project_task','left');
        $this->db->join('project_module pm','p.project_module_id=pm.id_project_module','left');
        $this->db->join('sprint s','p.sprint_id=s.id_sprint','left');
        $this->db->join('user u','tw.assigned_to=u.id_user','left');
        if(isset($data['sprint_id']))
            $this->db->where('p.sprint_id',$data['sprint_id']);
        //$this->db->where('p.id_project_task in (select parent_id from project_task where project_id='.$data['project_id'].' and parent_id!=0 and (`user_id` IS NOT NULL or user_id!=0))');
        if(isset($data['project_id']))
            $this->db->where('p.project_id',$data['project_id']);
        if(isset($data['user_id']) && ($this->session->userdata('user_type_id')==3))
        {
            if($this->session->userdata('is_lead')!=1)
                $this->db->where('tw.assigned_to',$data['user_id']);
            else
                $this->db->where('u.department_id',$this->session->userdata('department_id'));
        }

        if(isset($data['week_date'])) {
            //$this->db->where('(s.start_date <= CURDATE() and CURDATE() <= s.end_date)');
            $this->db->where('YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1)');
        }

        $this->db->where('tw.status','pending');
        $query = $this->db->get('');
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectTaskByStatus($data)
    {
        $this->db->select('tw.id_task_workflow,tw.estimated_time as current_estimated_time,CONCAT(u.first_name," ",u.last_name) as user_name,u.email,p.*,p.task_name as parent_name,p.id_project_task as parent_id,pm.id_project_module,pm.module_name');
        $this->db->from('task_workflow tw');
        $this->db->join('project_task p','tw.project_task_id=p.id_project_task','left');
        $this->db->join('project_module pm','p.project_module_id=pm.id_project_module','left');
        $this->db->join('sprint s','p.sprint_id=s.id_sprint','left');
        $this->db->join('user u','tw.assigned_to=u.id_user','left');
        if(isset($data['sprint_id']))
            $this->db->where('p.sprint_id',$data['sprint_id']);
        //$this->db->where('p.id_project_task in (select parent_id from project_task where project_id='.$data['project_id'].' and parent_id!=0 and (`user_id` IS NOT NULL or user_id!=0))');
        if(isset($data['project_id']))
            $this->db->where('p.project_id',$data['project_id']);
        if(isset($data['user_id']) && ($this->session->userdata('user_type_id')==3))
        {
            if($this->session->userdata('is_lead')!=1)
                $this->db->where('tw.assigned_to',$data['user_id']);
            else
                $this->db->where('u.department_id',$this->session->userdata('department_id'));
        }
        if(isset($data['week_date'])) {
            //$this->db->where('(s.start_date <= CURDATE() and CURDATE() <= s.end_date)');
            $this->db->where('YEARWEEK(s.start_date, 1) = YEARWEEK(CURDATE(), 1)');
        }
        if(isset($data['status']))
            $this->db->where('tw.status',$data['status']);
        $query = $this->db->get('');
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskDetails($data)
    {
        $this->db->select('p.*, pr.*, p.description, CONCAT_WS(" ",u.first_name,u.last_name) as name,d.department_name,`tm`.task_status as task_status,tw.id_task_flow');
        $this->db->from('project_task p');
        $this->db->join('project pr','p.project_id=pr.id_project','left');
        $this->db->join('task_flow tw','p.id_project_task=tw.project_task_id','left');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        $this->db->join('user u','tm.assigned_to=u.id_user','left');
        $this->db->join('user_department ud','ud.user_id=u.id_user','left');
        $this->db->join('department d','d.id_department=ud.department_id','left');
        $this->db->where('p.id_project_task',$data['id_project_task']);
        if(isset($data['id_task_flow']))
            $this->db->where('tw.id_task_flow',$data['id_task_flow']);
        if(isset($data['user_id']))
            $this->db->where('tm.assigned_to',$data['user_id']);
        if(isset($data['department_id']))
            $this->db->where('ud.department_id',$data['department_id']);
        if(isset($data['tw_department_id']))
            $this->db->where('ud.department_id',$data['tw_department_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }
    public function checkUserAssignedToTask($data){
        $this->db->select('*');
        $this->db->from('task_week_flow');
        $this->db->where('user_id',$this->session->userdata('user_id'));
        $this->db->where('task_flow_id',$data['task_flow_id']);
        $this->db->where("DATE(date)='".$data['created_date_time']."'");
        $query = $this->db->get()->result();
//        die($this->db->last_query());
        if(count($query)>0){
            return true;
        }else{
            return false;
        }
    }
    public function checkApprovedHoliday($data){
        $this->db->select('status');
        $this->db->from('work_request');
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('date',$data['date']);
        $result = $this->db->get()->result();

        if(count($result)>0){
            return $result[0]->status;
        }else{
            return false;
        }

    }
    public function checkLogPresentByDate($data){
        $this->db->select('log_time_id');
        $this->db->from('log_time');
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('created_date_time',$data['date']);
        $result = $this->db->get()->result();

        if(count($result)>0){
            return true;
        }else{
            return false;
        }

    }
    public function checkLogTime($data){
        $startDate = $data['start_time'];
        $endDate = $data['end_time'];

        if(isset($data['created_date_time'])){
            $data['created_date_time'] = $data['created_date_time'];
        }else{
            $data['created_date_time'] = date('Y-m-d');
        }

        $this->db->select('*');
        $this->db->from('log_time');
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('created_date_time',$data['created_date_time']);
        $query = $this->db->get()->result_array();

        $startDates = [];
        $error = 0;
        foreach($query as $row){
            $current_time = $row['start_time'];
            $sunrise = $startDate;
            $sunset = $endDate;
            $date1 = DateTime::createFromFormat('H:i a', $current_time);
            $date2 = DateTime::createFromFormat('H:i a', $sunrise);
            $date3 = DateTime::createFromFormat('H:i a', $sunset);
            if ($date1 > $date2 && $date1 < $date3)
            {
                $error++;
            }
        }
        if($error != 0){
            return 'timelogged';
        }
        $startDate = $data['created_date_time'].' '.$startDate;
        $endDate = $data['created_date_time'].' '.$endDate;

        $this->db->select('*');
        $this->db->from('log_time lt');
        $this->db->where('lt.user_id',$data['user_id']);
        $this->db->where('STR_TO_DATE("'.$startDate.'", "%Y-%c-%e %H:%i") between STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(lt.created_date_time , "%Y-%m-%d"), lt.start_time), "%Y-%c-%e %H:%i") AND STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(lt.created_date_time , "%Y-%m-%d"), lt.end_time), "%Y-%c-%e %H:%i")');
        $result = $this->db->get()->result();
        if(count($result)>0){
            return 'timelogged';
        }

        $this->db->select('*');
        $this->db->from('log_time lt');
        $this->db->where('lt.user_id',$data['user_id']);
        $this->db->where('STR_TO_DATE("'.$endDate.'", "%Y-%c-%e %H:%i") between STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(lt.created_date_time , "%Y-%m-%d"), lt.start_time), "%Y-%c-%e %H:%i") AND STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(lt.created_date_time , "%Y-%m-%d"), lt.end_time), "%Y-%c-%e %H:%i")');
        $result = $this->db->get()->result();
        if(count($result)>0){
            return 'timelogged';
        }

        /*Exceeds condition EX: log time 3-4, but later adding log as 2-6*/
        $this->db->select('*');
        $this->db->from('log_time lt');
        $this->db->where('lt.user_id',$data['user_id']);
        $this->db->where('Date(lt.created_date_time)="'.$data['created_date_time'].'"');
        $query = $this->db->get()->result_array();

        foreach($query as $row){
            $this->db->select('*');
            $this->db->from('log_time lt');
            $this->db->where('lt.user_id',$data['user_id']);
            $this->db->where('STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(lt.created_date_time , "%Y-%m-%d"), lt.start_time), "%Y-%c-%e %H:%i") BETWEEN STR_TO_DATE("'.$startDate.'", "%Y-%c-%e %H:%i") AND STR_TO_DATE("'.$endDate.'", "%Y-%c-%e %H:%i")');
            $result = $this->db->get()->result();

            if(count($result)>0){
                return 'timelogged';
            }

            $this->db->select('*');
            $this->db->from('log_time lt');
            $this->db->where('lt.user_id',$data['user_id']);
            $this->db->where('STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(lt.created_date_time , "%Y-%m-%d"), lt.end_time), "%Y-%c-%e %H:%i") BETWEEN STR_TO_DATE("'.$startDate.'", "%Y-%c-%e %H:%i") AND STR_TO_DATE("'.$endDate.'", "%Y-%c-%e %H:%i")');
            $result = $this->db->get()->result();
            if(count($result)>0){
                return 'timelogged';
            }
        }

    }
    public function logTime($data){
        if(isset($data['hour']))
            unset($data['hour']);
        if(isset($data['meridian']))
            unset($data['meridian']);
        if(isset($data['minute']))
            unset($data['minute']);
        if(isset($data['task_date']))
            unset($data['task_date']);

        $login  = DateTime::createFromFormat('h:i a', $data['start_time'])->getTimestamp();
        $logout = DateTime::createFromFormat('h:i a', $data['end_time'])->getTimestamp();
        $data['start_time'] = (new DateTime($data['start_time']))->format('h:i a');
        $data['end_time'] = (new DateTime($data['end_time']))->format('h:i a');
        $interval = $logout-$login;
        $data['duration'] =gmdate("H:i:s", $interval);
        $this->db->insert('log_time', $data);
        return $this->db->insert_id();
    }
    public function checkLogExist($data){
        $this->db->select('log_time_id');
        $this->db->from('log_time');
        if(isset($data['task_flow_id']))
            $this->db->where('task_flow_id',$data['task_flow_id']);

        $query = $this->db->get()->result();
//        die($this->db->last_query());
        if(count($query)>0){
            return true;
        }else{
            return false;
        }
    }
    public function deleteLogTime($id){
        $this->db->where('log_time_id', $id);
        return $this->db->delete('log_time');
    }

    /**
     * @param $data
     *
     */
    public function getProjectTask($data){
        $this->db->select('*');
        $this->db->from('project_task pt');
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);
        if(isset($data['id_sprint']))
            $this->db->where('pt.id_sprint',$data['id_sprint']);
        if(isset($data['task_name']))
            $this->db->where('pt.task_name',$data['task_name']);
        if(isset($data['id_project_task']))
            $this->db->where('pt.id_project_task',$data['id_project_task']);
        $this->db->join('project_module pm', 'pm.id_project_module = pt.project_module_id');
        $this->db->order_by('pt.id_project_task','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function getTaskCheckList($data)
    {
        $this->db->select('*');
        $this->db->from('user_checklist');
        if(isset($data['task_id']))
            $this->db->where('task_id',$data['task_id']);
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTaskUseCase($data)
    {
        $this->db->select('pt.*,DATE_FORMAT(pt.created_date_time, \'%d/%m/%Y\') as created_date_time,u.id_task_usecase,u.task_usecase,CONCAT(us.first_name," ",us.last_name) as user_name');
        $this->db->from('task_usecase u');

        $this->db->join('user us','u.user_id=us.id_user','left');
        $this->db->join('user_department ud','us.id_user=ud.user_id','left');
        $this->db->join('project_task pt','u.project_task_id=pt.id_project_task','left');
        if(isset($data['parent_id']))
            $this->db->where('pt.id_project_task',$data['parent_id']);
        if(isset($data['project_task_id']))
            $this->db->where('u.project_task_id',$data['project_task_id']);
        if(isset($data['user_id'])){
            if(count($data['user_id'])){
                $this->db->where_in('u.user_id', $data['user_id']);
            }else{
                $this->db->where('u.user_id',$data['user_id']);
            }
        }
        if(isset($data['project_id'])){
            $this->db->where('pt.project_id',$data['project_id']);
        }
        if(isset($data['date_filter'])){
            $this->db->where("DATE_FORMAT(u.created_date_time, '%d/%m/%Y')=DATE_FORMAT(STR_TO_DATE('".$data['date_filter']."', '%d/%m/%Y'), '%d/%m/%Y')");
        }
        if(isset($data['department_id'])){
            $this->db->where('ud.department_id',$data['department_id']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskUseCaseByUser($data)
    {
        $this->db->select('pt.*,u.id_task_usecase,u.task_usecase');
        $this->db->from('project_task pt');
        $this->db->join('task_usecase u','u.project_task_id=pt.id_project_task and u.user_id='.$data['user_id'],'left');

        if(isset($data['parent_id']))
            $this->db->where('pt.parent_id',$data['parent_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllCheckList($data){
        $this->db->select('*');
        $this->db->from('user_checklist uc');
        $this->db->join('checklist c','uc.checklist_id=c.id_checklist','left');
        $this->db->join('user u','uc.user_id=u.id_user','left');
        $this->db->join('user_department ud','u.id_user=ud.user_id','left');
        if(count($data)>0){
            $this->db->join('project_task pt','pt.id_project_task=uc.task_id','left');
            if(isset($data['user_id'])){
                if(count($data['user_id'])){
                    $this->db->where_in('u.id_user', $data['user_id']);
                }else{
                    $this->db->where('u.id_user',$data['user_id']);
                }
            }
            if(isset($data['project_task_id'])){
                $this->db->where('uc.task_id',$data['project_task_id']);
            }
            if(isset($data['department_id'])){
                $this->db->where('ud.department_id',$data['department_id']);
            }
        }else{
            $this->db->where('uc.task_id',$data);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addTaskUseCase($data)
    {
        $this->db->insert('task_usecase', $data);
        return $this->db->insert_id();
    }

    public function updateTaskUseCase($data)
    {
        $this->db->where('id_task_usecase', $data['id_task_usecase']);
        $this->db->update('task_usecase', $data);
        return 1;
    }

    public function getTaskAttachments($data)
    {
        $this->db->select('pt.*,a.user_id,a.id_attachment,a.attachment,a.created_date_time,CONCAT(ua.first_name," ",ua.last_name) as user_name');
        $this->db->from('attachment a');
        /*$this->db->join('project_task pt','a.project_task_id=pt.id_project_task','left');*/
        $this->db->join('user ua','a.user_id=ua.id_user','left');
        $this->db->join('user_department ud','ua.id_user=ud.user_id','left');
        $this->db->join('project_task pt','a.project_task_id=pt.id_project_task','left');
        if(isset($data['parent_id']))
            $this->db->where('pt.id_project_task',$data['parent_id']);

        if(isset($data['user_id'])){
            if(count($data['user_id'])){
                $this->db->where_in('ua.id_user', $data['user_id']);
            }else{
                $this->db->where('ua.id_user',$data['user_id']);
            }
        }
        if(isset($data['project_task_id'])){
            $this->db->where('a.project_task_id',$data['project_task_id']);
        }
        if(isset($data['date_filter'])){
            $this->db->where("DATE_FORMAT(a.created_date_time, '%d/%m/%Y')=DATE_FORMAT(STR_TO_DATE('".$data['date_filter']."', '%d/%m/%Y'), '%d/%m/%Y')");
        }
        if(isset($data['department_id'])){
            $this->db->where('ud.department_id',$data['department_id']);
        }

        $this->db->order_by('a.id_attachment','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addAttachment($data)
    {
        $this->db->insert('attachment', $data);
        return $this->db->insert_id();
    }

    public function getTimeLine($data)
    {
        $this->db->select('t.*,CONCAT(u.first_name," ",u.last_name) as user_name,d.department_name');
        $this->db->from('timeline t');
        $this->db->join('user u','t.created_by=u.id_user','left');
        $this->db->join('user_department ud','u.id_user=ud.user_id','left');
        $this->db->join('department d','ud.department_id=d.id_department','left');

        /*if(isset($data['module_type']) && $data['module_type']=='task') {
            $this->db->where('t.module_id in (select id_project_task from project_task )');
        }else {*/
            if(isset($data['module_type'])){$this->db->where('t.module_type',$data['module_type']);}
            if(isset($data['module_id'])){ $this->db->where('t.module_id',$data['module_id']); }
            if(isset($data['department_id'])){ $this->db->where('ud.department_id',$data['department_id']); }
       /* }*/

        $this->db->order_by('t.id_timeline','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addTimeLine($data)
    {
        $this->db->insert('timeline', $data);
        return $this->db->insert_id();
    }

    public function taskLogListGrid($paramArr)
    {   //echo "<pre>"; print_r($paramArr); exit;
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sortField'])?$paramArr['sortField']:'log_time_id';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['task_id'])?'pt.id_project_task='.$paramArr['task_id']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (".$whereParam.")"; }
        $whereClause = "where true ".$whereParam;
        if($this->session->userdata('user_type_id')==3 && $this->session->userdata('is_lead')!=1 ){
            $whereClause = $whereClause.' and lt.user_id='.$this->session->userdata('user_id');
        }

        $SQL = "select pt.*,lt.*,TIME_FORMAT( duration, '%H:%i' ) as a_time,DATE_FORMAT(lt.created_date_time, '%d/%m/%Y') as logDate ,CONCAT(u.first_name,' ',u.last_name) as user from log_time lt
                left join project_task pt on lt.project_task_id=pt.id_project_task
                left join user u on lt.user_id=u.id_user
                $whereClause order by $sortField $sortOrder $optLimit";

        //echo $SQL; exit;
        $result = $this->db->query($SQL);

        $SQLCount = "select count(*) from log_time lt
                       left join project_task pt on lt.project_task_id=pt.id_project_task
                left join user u on lt.user_id=u.id_user
                $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();

        $total_count = $resultCount['count(*)'];
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function TouchPointsGrid($paramArr)
    {
        //echo "<pre>"; print_r($paramArr); exit;
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sortField'])?$paramArr['sortField']:'id_task_workflow';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['parent_id'])?'pt.id_project_task='.$paramArr['parent_id']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (".$whereParam.")"; }
        $whereClause = "where true ".$whereParam;
        if($this->session->userdata('user_type_id')==3) {
            if ($this->session->userdata('is_lead') != 1) {
                $whereClause = $whereClause . ' and tw.assigned_to=' . $this->session->userdata('user_id');
            } else {
                $whereClause = $whereClause . ' and tu.department_id=' . $this->session->userdata('department_id');
            }
        }

        $SQL = "select tw.id_task_workflow,pt.task_name,tw.status,DATE_FORMAT(tw.created_date_time,'%d-%m-%Y') as date,CONCAT(tu.first_name,\" \",tu.last_name) as user_name,d.department_name from task_workflow tw
                left join project_task pt on tw.project_task_id=pt.id_project_task
                left join user tu on tw.assigned_to=tu.id_user
                left join department d on tu.department_id=d.id_department
                $whereClause order by $sortField $sortOrder $optLimit";

        //echo $SQL; exit;
        $result = $this->db->query($SQL);

        $SQLCount ="select count(*) from task_workflow tw
                left join project_task pt on tw.project_task_id=pt.id_project_task
                left join user tu on tw.assigned_to=tu.id_user
                left join department d on tu.department_id=d.id_department
                $whereClause order by $sortField $sortOrder ";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();

        $total_count = $resultCount['count(*)'];
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getAttachmentsCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('attachment');
        if(isset($data['project_task_id']))
            $this->db->where('project_task_id',$data['project_task_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function updateTask($data)
    {
        $this->db->where('id_project_task', $data['id_project_task']);
        $this->db->update('project_task', $data);
        return 1;
    }

    public function updateTaskWorkFlow($data)
    {
        $this->db->where('id_task_workflow', $data['id_task_workflow']);
        $this->db->update('task_workflow', $data);
        //echo $this->db->last_query(); exit;
        return 1;
    }

    public function getProjectTeam($data){

        $this->db->select('*');
        $this->db->join('user u','u.id_user=pm.employee_id','left');
        if(isset($data['email']))
            $this->db->where('u.email',$data['email']);
        if(isset($data['project_id']))
             $this->db->where('pm.project_id',$data['project_id']);
        if(isset($data['project_ids']))
            $this->db->where_in('pm.project_id',$data['project_ids']);
            $this->db->group_by('email');
        if(isset($data['department_id']))
            $this->db->where('u.department_id',$data['department_id']);

        $this->db->from('project_employees pm');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskWorkFlow($data)
    {
        $this->db->select('*');
        $this->db->from('task_workflow tw');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        if(isset($data['id_task_workflow']))
            $this->db->where('tw.id_task_workflow',$data['id_task_workflow']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addTaskWorkFlow($data)
    {
        $this->db->insert('task_workflow', $data);
        return $this->db->insert_id();
    }

    public function getUser($data)
    {
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('user_department ud','ud.user_id=u.id_user','left');
        if(isset($data['id_user']))
            $this->db->where('u.id_user',$data['id_user']);
        if(isset($data['department_id']))
            $this->db->where('ud.department_id',$data['department_id']);
        if(isset($data['is_lead']))
            $this->db->where('u.is_lead',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUserByDepartmentProject($data)
    {
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('user_department ud','ud.user_id=u.id_user','left');
        $this->db->join('project_employees pe','u.id_user=pe.employee_id','left');
        if(isset($data['id_user']))
            $this->db->where('u.id_user',$data['id_user']);
        if(isset($data['department_id'])){
            if(preg_match('/,/' , $data['department_id'])){
                $data['department_id'] = explode(',',$data['department_id']);
                $this->db->where_in('ud.department_id',$data['department_id']);
            }else{
                $this->db->where('ud.department_id',$data['department_id']);
            }
//            $this->db->where_in("ud.department_id",'join(", ", '.$data['department_id'].')');
            //$whereParam .= ' AND `a`.`user_id` in (' . join(', ', ($user_id)) . ')';
        }
        if(isset($data['project_id'])){
            if(preg_match('/,/' , $data['project_id'])){
                $data['project_id'] = explode(',',$data['project_id']);
                $this->db->where_in('pe.project_id',$data['project_id']);
            }else{
                $this->db->where('pe.project_id',$data['project_id']);
            }
//            $this->db->where_in('pe.project_id',"join(', ', ".$data['project_id'].")");
        }
        $this->db->where('u.user_type_id','3');
        $this->db->group_by('u.id_user');

        $query = $this->db->get();

        return $query->result_array();
    }

    public function getUserById($data)
    {
        $this->db->select('*');
        $this->db->from('user u');
        //$this->db->join('user_department ud','ud.user_id=u.id_user','left');
        if(isset($data['id_user']))
            $this->db->where('u.id_user',$data['id_user']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompletedSubtaskDept($parent_id,$status,$dept)
    {
        $this->db->select('tw.*,tw.estimated_time as current_estimated_time,pt.*,u.department_id');
        $this->db->from('task_workflow tw');
        $this->db->join('user u','tw.assigned_to=u.id_user','left');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        if($dept!=0)
            $this->db->where('u.department_id',$dept);
        $this->db->where('tw.status',$status);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getLastSubtaskDept($parent_id,$status)
    {
        $this->db->select('tw.*,tw.estimated_time as current_estimated_time,pt.*,u.department_id');
        $this->db->from('task_workflow tw');
        $this->db->join('user u','tw.assigned_to=u.id_user','left');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->where('pt.parent_id',$parent_id);
        $this->db->order_by('tw.id_task_workflow','desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getSubTaskByParent($task_id)
    {
        $this->db->select('*');
        $this->db->from('project_task');
        $this->db->where('id_project_task',$task_id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectDepartments($project_id)
    {
        $this->db->select('d.id_department, pd.department_id, d.department_name, pd.estimation_time, pd.estimation_percentage');
        $this->db->join('department d','pd.department_id=d.id_department','left');
        $this->db->from('project_department pd');
        if(preg_match('/-/' , $project_id)){
            $project_id = explode('-',$project_id);
            $this->db->where_in('pd.project_id',$project_id);
        }else{
            $this->db->where('pd.project_id',$project_id);
        }
        $this->db->group_by('pd.department_id');
        $this->db->order_by('d.order','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addTouchPoints($data)
    {
        $this->db->insert('touch_points', $data);
        return $this->db->insert_id();
    }

    public function getTouchPoints($data)
    {
        $this->db->select('tw.*,pt.task_name,CONCAT(tu.first_name," ",tu.last_name) as user_name,d.department_name');
        $this->db->from('task_workflow tw');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('user tu','tw.assigned_to=tu.id_user','left');
        $this->db->join('user_department ud','ud.user_id=tu.id_user','left');
        $this->db->join('department d','ud.department_id=d.id_department','left');

        if($this->session->userdata('user_type_id')==3)
        {   if($this->session->userdata('is_lead')!=1)
                $this->db->where('tw.assigned_to',$this->session->userdata('user_id'));
            else
                $this->db->where('ud.department_id',$this->session->userdata('department_id'));
        }
        if(isset($data['department_id']))
            $this->db->where('ud.department_id',$data['department_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTimeLogByTaskWorkflowId($data)
    {
        $this->db->select('TIME_TO_SEC(TIMEDIFF(end_time,start_time)) as time');
        $this->db->from('log_time');
        if(isset($data['project_task_id']))
            $this->db->where('project_task_id',$data['project_task_id']);
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        if(isset($data['task_flow_id']))
            $this->db->where('task_flow_id',$data['task_flow_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTimeLogByTask($data)
    {
        $this->db->select('*');
        $this->db->from('log_time');
        if(isset($data['project_task_id']))
            $this->db->where('project_task_id',$data['project_task_id']);
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['project_task_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectTeamLead($data)
    {
        $this->db->select('*');
        $this->db->from('project_employees pe');
        $this->db->join('user u','u.id_user=pe.employee_id','left');
        if(isset($data['department_id']))
            $this->db->where('u.department_id',$data['department_id']);
        if(isset($data['is_lead']))
            $this->db->where('u.is_lead',1);
        if(isset($data['project_id']))
            $this->db->where('pe.project_id',$data['project_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function CheckTaskExsitsInDept($data)
    {
        $this->db->select('*');
        $this->db->from('task_workflow tw');
        $this->db->join('user u','u.id_user=tw.assigned_to','left');
        if(isset($data['department_id']))
            $this->db->where('u.department_id',$data['department_id']);
        if(isset($data['task_id']))
            $this->db->where('tw.project_task_id',$data['task_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function logDetailsGrid($paramArr){
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sortField'])?$paramArr['sortField']:'pt.id_project_task';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['whereParam'])?$paramArr['whereParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (".$whereParam.")"; }
        $whereClause = "where true ".$whereParam;
        if(isset($paramArr['task_users'])){
            if(!empty($paramArr['task_users'])){
                $whereClause  .=  ' and tw.assigned_to = '.$paramArr['task_users'];
            }

        }
        if(isset($paramArr['log_start_date'])){
            if(!empty($paramArr['log_start_date'])){
                $whereClause  .=  ' and DATE(lt.created_date_time) >= "'.$paramArr['log_start_date'].'"';
            }

        }
        if(isset($paramArr['log_end_date'])){
            if(!empty($paramArr['log_end_date'])){
                $whereClause  .=  ' and DATE(lt.created_date_time) <= "'.$paramArr['log_end_date'].'"';
            }

        }

        $SQL = "select pt.task_name,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( tw.estimated_time ) ) ), '%H:%i' ),'00:00') as estimated_time,
                IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as duration,
                u.email,pr.project_name, DATE_FORMAT(lt.created_date_time,'%m-%d-%Y') as created_date_time ,pt.id_project_task
                from task_workflow tw
                left join project_task pt on tw.project_task_id=pt.id_project_task
                left JOIN log_time lt on pt.id_project_task=lt.project_task_id
                left JOIN `user` u on tw.assigned_to=u.id_user
                left JOIN `project` pr on pt.project_id =pr.id_project
                $whereClause GROUP BY pt.id_project_task order by $sortField $sortOrder $optLimit";

        $SQLCount = "select pt.task_name,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( tw.estimated_time ) ) ), '%H:%i' ),'00:00') as estimated_time,
                    IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), '%H:%i' ),'00:00') as duration,
                    u.email,pr.project_name, lt.created_date_time,pt.id_project_task
                    from task_workflow tw
                    left join project_task pt on tw.project_task_id=pt.id_project_task
                    left JOIN log_time lt on pt.id_project_task=lt.project_task_id
                    left JOIN `user` u on tw.assigned_to = u.id_user
                    left JOIN `project` pr on pt.project_id =pr.id_project
                    $whereClause GROUP BY  pt.id_project_task";
        //echo '<pre>'; print_r($SQL); exit;

        $result = $this->db->query($SQL);

        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();


        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function filterDayWiseMonitoring($data){
        $final = [];
        $whereCondition = '';
        $whereCond = '';
        $joinStatement = '';

        $whereCondition = '';
        if(isset($data['department']) || isset($data['user']) || isset($data['project'])){
            $whereCondition = ' where ';
        }

        if(isset($data['user'])){
            $whereCondition .= ' u.id_user IN ('.$data['user'].') ';
            $whereCond = 1;
        }

        if(isset($data['department'])){
            if($whereCond == 0){
                $whereCondition .= ' ud.department_id IN ('.$data['department'].') ';
                $whereCond = 1;
            }else{
                $whereCondition .= ' AND ud.department_id IN ('.$data['department'].') ';
            }
        }
        if(isset($data['project'])){
            $joinStatement .= ' LEFT JOIN project_employees pe ON u.id_user=pe.employee_id
                                LEFT JOIN project p ON pe.project_id=p.id_project';
            if($whereCond == 0){
                $whereCondition .= ' pe.project_id IN ('.$data['project'].') ';
            }else{
                $whereCondition .= ' AND pe.project_id IN ('.$data['project'].') ';
            }
        }

        $qry = 'select CONCAT_WS(\' \',u.first_name,u.last_name) name,u.id_user emp_id
                from user u
                left join user_department ud on u.id_user=ud.user_id
                    '.$joinStatement.' '.$whereCondition;

        $res_all = $this->db->query($qry);
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                foreach($res_all as $k=>$v){
                    $userList[$v->emp_id] = $v;
                }
            }else{
                return false;
            }
        }

        if(isset($data['date'])){
            $whereCondition = 'WHERE Date(lt.created_date_time)=Date("'.$data['date'].'") ';
        }

        foreach($userList as $k=>$v){
            $allWhereCond = '';
            if(strlen($whereCondition)>0){
                $allWhereCond .= $whereCondition.' AND u.id_user='.$k;
            }
            $qry = 'select lt.created_date_time,
                     lt.start_time,
                      lt.end_time,
                       lt.duration,
                         lt.comments,pt.task_name from
                log_time lt
                left JOIN project_task pt on pt.id_project_task = lt.project_task_id ||  pt.id_project_task is NULL
                WHERE Date(lt.created_date_time)=Date(\''.$data["date"].'\')
                AND lt.user_id='.$v->emp_id.' order by str_to_date(lt.start_time,\'%l:%i %p\') asc';
            $res_all = $this->db->query($qry);
             //die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    foreach($res_all as $k1=>$v1){

                        $countData = $this->get_counts($v->emp_id, $data['date']);
                        $final[$v->emp_id][] = array(
                            'id_user' => $v->emp_id,
                            'name' => $v->name,
                            'projectCounts' => $countData['projectCounts'],
                            'task_name'=>$v1->task_name,
                            'taskCount' => $countData['taskCount'],
                            'estimated_time' => $countData['actualTime'],
                            'created_date_time' => $v1->created_date_time,
                            'start_time' => $v1->start_time,
                            'end_time' => $v1->end_time,
                            'duration' => $v1->duration,
                            'comments' => $v1->comments,
                            'status' => '',
                        );
                    }
                }else{
                    $countData = $this->get_counts($v->emp_id, $data['date']);
                    $final[$v->emp_id][] = array(
                        'id_user' => $v->emp_id,
                        'name' => $v->name,
                        'projectCounts' => $countData['projectCounts'],
                        'task_name'=>'',
                        'taskCount' => $countData['taskCount'],
                        'estimated_time' => $countData['actualTime'],
                        'created_date_time' => '',
                        'start_time' => 0,
                        'end_time' => 0,
                        'duration' => 0,
                        'comments' => '',
                        'status' => '',
                    );
                }
            }
        }
//        echo '<pre>';print_r($final);die;
        return $final;
    }
    function get_counts($userId, $date){
        $qry = 'select p.id_project
            from project p
            left join project_employees pe ON p.id_project=pe.project_id
            left join project_task pt ON p.id_project=pt.project_id
            left join task_flow tf on pt.id_project_task=tf.project_task_id
            left join task_week_flow twf on twf.task_flow_id=tf.id_task_flow
            where
            twf.user_id='.$userId.' AND twf.date=\''.$date.'\'
            group by p.id_project';
        $countRes = $this->db->query($qry)->result();
        $projectCounts = count($countRes);

        $qry = 'select pt.id_project_task
            from project p
            left join project_employees pe ON p.id_project=pe.project_id
            left join project_task pt ON p.id_project=pt.project_id
            left join task_flow tf on pt.id_project_task=tf.project_task_id
            left join task_week_flow twf on twf.task_flow_id=tf.id_task_flow
            where
            twf.user_id='.$userId.' AND twf.date=\''.$date.'\'
            group by p.id_project';
        $countRes = $this->db->query($qry)->result();
        $taskCount =  count($countRes);
        $total = 0;
        $qry = 'select TIME_TO_SEC(STR_TO_DATE(CONCAT_WS(" ", DATE_FORMAT(\''.$date.'\' , "%Y-%m-%d"), twf.time), "%Y-%c-%e %H:%i:%s")) as actualTime
            from project p
            left join project_employees pe ON p.id_project=pe.project_id
            left join project_task pt ON p.id_project=pt.project_id
            left join task_flow tf on pt.id_project_task=tf.project_task_id
            left join task_week_flow twf on twf.task_flow_id=tf.id_task_flow
            where
            twf.user_id='.$userId.' AND twf.date=\''.$date.'\'
            group by p.id_project';
        $countRes = $this->db->query($qry)->result();
        foreach($countRes as $k => $v){
//            print_r($v->tim);die;
            $total += $v->actualTime;
        }

        return array('projectCounts' => $projectCounts, 'taskCount' => $taskCount, 'actualTime'=> ($total/3600));
    }

    //added by sthanka
    public function getUserTasks($data=array())
    {
        $this->db->select('tw.id_task_flow,tw.project_task_id,pt.project_id,p.project_name,pt.task_name, IFNULL(tw.estimated_time,"00:00:00") estimated_time,IFNULL(tw.additional_time,"00:00:00") as additional_time,"00:00:00" as alloted_time,"00:00:00" as remains, , tw.task_status as status');
        $this->db->from('task_flow tw');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        $this->db->where('tw.department_id', $this->session->userdata('department_id'));
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);
        $this->db->group_by('tw.project_task_id');
        if(isset($data['status']) && is_array($data['status']))
            $this->db->where_in('tw.task_status',$data['status']);
        if(isset($data['order']))
            $this->db->order_by($data['order'],'desc');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getUsers($data)
    {
        $this->db->select('*');
        $this->db->from('user u');
        $this->db->join('user_department ud','ud.user_id=u.id_user','left');
        $this->db->join('department d','ud.department_id=d.id_department','left');
        if(isset($data['project_id']) && !empty($data['project_id'])){
            $this->db->join('project_employees pd','u.id_user=pd.employee_id','left');
            $this->db->where('pd.project_id',$data['project_id']);
        }
        if(isset($data['project_id_array'])){
            $this->db->join('project_employees pd','u.id_user=pd.employee_id','left');
            $this->db->where_in('pd.project_id',$data['project_id_array']);
            $this->db->group_by('pd.employee_id');
        }
        if(isset($data['department_id']) && $data['department_id']!='' && !empty($data['department_id'])){
            $this->db->where('ud.department_id',$data['department_id']);
        }

        if(isset($data['is_lead']))
            $this->db->where('ud.is_lead',$data['is_lead']);
        if(isset($data['user_id']))
            $this->db->where('u.id_user',$data['user_id']);

        $this->db->where('u.user_type_id',3);
        $this->db->where('u.status',1);
        $this->db->group_by('u.id_user');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getUserTaskWorkload($data)
    {
        //getting project count
        //echo "<pre>"; print_r($data); exit;
        $sql = "SELECT twf.day_id,count(DISTINCT(pt.project_id)) as projects from task_flow tw
                LEFT JOIN project_task pt on tw.project_task_id=pt.id_project_task
                LEFT JOIN task_week_flow twf on tw.id_task_flow=twf.task_flow_id
                LEFT JOIN task_member tm on tw.id_task_flow=tm.task_flow_id
                WHERE tm.assigned_to=".$data['assigned_to']." ";
        if(isset($data['project_id'])){
            $sql.=" AND pt.project_id=".$data['project_id']." ";
        }
        if(isset($data['start_date']) && isset($data['end_date'])){
            $sql.=' AND (twf.date>="'.$data['start_date'].'" and twf.date<="'.$data['end_date'].'") ';
        }
        $sql.= " AND twf.day_id is not null
                GROUP BY twf.day_id";
        $projects = $this->db->query($sql);
        $projects = $projects->result_array();

        //echo "<pre>";print_r($sql); exit;
        //getting total task count
        $sql1 = "SELECT twf.day_id,count(DISTINCT(tw.id_task_flow)) as tasks  from task_flow tw
                LEFT JOIN project_task pt on tw.project_task_id=pt.id_project_task
                LEFT JOIN task_week_flow twf on tw.id_task_flow=twf.task_flow_id
                LEFT JOIN task_member tm on tw.id_task_flow=tm.task_flow_id
                WHERE tm.assigned_to=".$data['assigned_to']." ";
        if(isset($data['project_id'])){
            $sql1.=" AND pt.project_id=".$data['project_id']." ";
        }
        if(isset($data['start_date']) && isset($data['end_date'])){
            $sql1.=' AND (twf.date>="'.$data['start_date'].'" and twf.date<="'.$data['end_date'].'") ';
        }
        $sql1.=" AND twf.day_id is not null
                GROUP BY twf.day_id";
        //echo $sql1; exit;
        $total_tasks = $this->db->query($sql1);
        $total_tasks = $total_tasks->result_array();

        //getting completed tasks
        $sql2 = "SELECT twf.day_id,count(DISTINCT(tw.id_task_flow)) as tasks  from task_flow tw
                LEFT JOIN project_task pt on tw.project_task_id=pt.id_project_task
                LEFT JOIN task_week_flow twf on tw.id_task_flow=twf.task_flow_id
                LEFT JOIN task_member tm on tw.id_task_flow=tm.task_flow_id
                WHERE tm.assigned_to=".$data['assigned_to']." AND tm.task_status='completed' ";
        if(isset($data['project_id'])){
            $sql2.=" AND pt.project_id=".$data['project_id']." ";
        }
        if(isset($data['start_date']) && isset($data['end_date'])){
            $sql2.=' AND (twf.date>="'.$data['start_date'].'" and twf.date<="'.$data['end_date'].'") ';
        }
        $sql2.="AND twf.day_id is not null
                GROUP BY twf.day_id";

        $completed_tasks = $this->db->query($sql2);
        $completed_tasks = $completed_tasks->result_array();

        $this->db->select('twf.day_id,IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( twf.time ) ) ), "%H:%i" ),"00:00") as alloted_time,IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(duration))), "%H:%i" ),"00:00") as actual_time');
        $this->db->from('task_flow tw');
        $this->db->join('task_week_flow twf','tw.id_task_flow=twf.task_flow_id','left');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('log_time lt','tw.project_task_id=lt.project_task_id and twf.date=DATE_FORMAT(lt.created_date_time,"%Y-%m-%d") and  lt.user_id="'.$data['user_id'].'"','left');
        if(isset($data['assigned_to'])) {
            $this->db->where('tm.assigned_to', $data['assigned_to']);
            $this->db->where('twf.user_id', $data['assigned_to']);
        }
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);
        if(isset($data['start_date']) && isset($data['end_date'])){
            $this->db->where('(twf.date>="'.$data['start_date'].'" and twf.date<="'.$data['end_date'].'")');
        }
        $this->db->where('twf.day_id is not null');

        //to avoid completed and holded task week load
        //$this->db->where('tm.task_status !="completed" and tm.task_status !="hold"');

        $this->db->group_by('twf.day_id');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $res =  $query->result_array();

        return array('projects' => $projects,'res' => $res, 'total_tasks' => $total_tasks,'completed_tasks' => $completed_tasks);
    }

    public function addTaskTimeLog($data)
    {
        $this->db->insert('task_time_log', $data);
        return $this->db->insert_id();
    }

    public function getTaskTypes($type){
        $this->db->from('task_type tt');
        $this->db->select(' tt.*, CONCAT_WS(\' \',tt.name,CONCAT(\'(\',CONCAT_WS(\' ,\',if(tt.product_type=\'billable\',\'productive\',\'non-productive\'), if(tt.is_billable=\'1\',\'billable\',\'non-billable\')),\')\')) label');
        if($type == 'task'){
            $this->db->where('txd.department_id',$this->session->userdata('department_id'));
            $this->db->join('task_type_xref_department txd','tt.id_task_type=txd.task_type_id' , 'left');
        }
        else{
//            $this->db->where('tt.department_id',0);
        }

        $this->db->where('tt.task_type_key',$type);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskTypesByProjectId($type , $deptId){
        $this->db->from('task_type tt');
        $this->db->select(' tt.*');
        if($type == 'task'){
            $this->db->where('txd.department_id',$deptId);
        }
        else{
            $this->db->where('tt.department_id',0);
        }
        $this->db->join('task_type_xref_department txd','tt.id_task_type=txd.task_type_id' , 'left');
        $this->db->where('tt.task_type_key',$type);
        $query = $this->db->get();
        return $query->result();
    }

    public function getTaskAllotedTime($task_id,$estimation_time)
    {
        $this->db->select('IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( tm.allowted_time ) ) ), "%H:%i:%s" ),"00:00:00") as alloted_time,
            IFNULL(TIME_FORMAT( SEC_TO_TIME(TIME_TO_SEC("'.$estimation_time.'")-SUM( TIME_TO_SEC( tm.allowted_time ) )), "%H:%i:%s" ),"'.$estimation_time.'") as remains');
        $this->db->from('task_flow tw');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        if(isset($data['assigned_to']))
            $this->db->where('tm.assigned_to',$data['assigned_to']);

        $this->db->where('tw.project_task_id',$task_id);
        $this->db->where('tw.department_id',$this->session->userdata('department_id'));

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getChecklist($task_id)
    {
        $this->db->select('*');
        $this->db->join('department_checklist dcl','cl.id_checklist = dcl.checklist_id','left');
        $this->db->from('checklist cl');
        $this->db->where('dcl.department_id', $this->session->userdata('department_id'));
        $checklist = $this->db->get()->result();
        foreach ($checklist as $k => $v) {
            $this->db->select('*');
            $this->db->from('user_checklist uc');
            $this->db->where('uc.checklist_id', $v->id_checklist);
            $this->db->where('uc.task_id', $task_id);
            $this->db->where('uc.user_id', $this->session->userdata('user_id'));
            $userChecklist = $this->db->get()->result();
            if(count($userChecklist)>0){
                $checklist[$k]->checklist_value = $userChecklist[0]->checklist_value;
            }else{
                $checklist[$k]->checklist_value = '';
            }
        }
        return $checklist;
    }

    public function saveCheckListStatus($data)
    {
        if(isset($data['taskId'])){
            //remove all contents
            $this->db->delete('user_checklist', array(
                'user_id' => $this->session->userdata('user_id'),
                'task_id' => $data['taskId'],
            ));
            foreach($data['data'] as $k=>$v){
                if($v!=''){
                    $this->db->insert('user_checklist' , array(
                        'checklist_id' => $v['id'],
                        'checklist_value' => $v['value'],
                        'user_id' => $this->session->userdata('user_id'),
                        'task_id' => $data['taskId'],
                    ));
                }
            }
            return true;
        }else{
            return 'noid';
        }
    }

    public function getUserDayTasks($data)
    {
        $this->db->select('p.project_name,twf.id_task_week_flow,pt.task_name,pt.description,TIME_FORMAT(twf.time,"%H:%i") as time,tm.task_status, (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(STR_TO_DATE(lt.duration, \'%H:%i:\')))) fROM log_time lt WHERE lt.task_flow_id=tw.id_task_flow) duration');
        $this->db->from('task_flow tw');
        $this->db->join('task_week_flow twf','tw.id_task_flow=twf.task_flow_id','left');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        if(isset($data['user_id'])) {
            $this->db->where('tm.assigned_to', $data['user_id']);
            $this->db->where('twf.user_id', $data['user_id']);
        }
        if(isset($data['day_id']))
            $this->db->where('twf.date',$data['day_id']);
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);

        $this->db->where('twf.time is not null');
//        $this->db->where('twf.time!=""');
        $this->db->where('trim(coalesce(`twf`.`time`, \'\')) <>\'\'');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getUserDayTasksForFuture($data)
    {
        $this->db->select('p.project_name,twf.id_task_week_flow,pt.task_name,twf.time,tm.task_status');
        $this->db->from('task_flow tw');
        $this->db->join('task_week_flow twf','tw.id_task_flow=twf.task_flow_id','left');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        if(isset($data['user_id'])) {
            $this->db->where('tm.assigned_to', $data['user_id']);
            $this->db->where('twf.user_id', $data['user_id']);
        }
//        if(isset($data['day_id']))
//            $this->db->where('twf.date',$data['day_id']);
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);
        if(isset($data['id_task_week_flow']))
            $this->db->where('twf.id_task_week_flow',$data['id_task_week_flow']);

        $this->db->where('pt.id_project_task',$data['taskId']);
//        $this->db->where('twf.time is not null');
//        $this->db->where('twf.time!=""');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getOtherActivityTimeGrid($paramArr){

        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'log_time_id';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['whereParam'])?$paramArr['whereParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if($sortField=='start_time'){
            $sortField = " STR_TO_DATE(CONCAT_WS(\" \", DATE_FORMAT(lt.created_date_time , \"%Y-%m-%d\"), lt.start_time), \"%Y-%c-%d %h:%i %p\") ";
        }else if($sortField=='end_time'){
            $sortField = " STR_TO_DATE(CONCAT_WS(\" \", DATE_FORMAT(lt.created_date_time , \"%Y-%m-%d\"), lt.end_time), \"%Y-%c-%d %h:%i %p\") ";
        }else if($sortField=='created_date_time'){
            $sortField = " STR_TO_DATE(lt.created_date_time , \"%Y-%b-%d\") ";
        }

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (".$whereParam.")"; }
        if(isset($paramArr['task_type'])){ $whereParam .= " AND tt.task_type_key='".$paramArr['task_type']."' "; }
//        $whereClause = "where true AND lt.user_id=".$this->session->userdata('user_id')." AND DATE(lt.created_date_time)=DATE_FORMAT(now() , \"%Y-%m-%d\") ".$whereParam;
        $whereClause = "where true AND lt.user_id=".$this->session->userdata('user_id')." ".$whereParam;

        $SQL = 'SELECT `lt`.`log_time_id`, `lt`.`project_task_id`,  `lt`.`comments`, `lt`.`start_time`, `lt`.`end_time`, `lt`.`duration`, `lt`.`created_date_time`, tt.name
                FROM `log_time` `lt`
                LEFT JOIN task_type tt ON lt.task_type=tt.id_task_type
                  '.$whereClause.' order by '.$sortField.' '.$sortOrder.' '.$optLimit;

        $result = $this->db->query($SQL);

//        echo $this->db->last_query(); exit;

        $SQLCount = 'SELECT count(*)
                FROM `log_time` `lt`
                LEFT JOIN `project_task` `pt` ON `lt`.`project_task_id`=`pt`.`id_project_task`
                LEFT JOIN `project` `p` ON `pt`.`project_id`=`p`.`id_project`
                LEFT JOIN task_type tt ON lt.task_type=tt.id_task_type
                '.$whereClause;

        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if($result->num_rows() > 0) {
            $list = $result->result();
            foreach($list as $k=>$v){
                $v->created_date_time = (new DateTime($v->created_date_time))->format('d-M-Y');
                $v->duration = (new DateTime($v->duration))->format('H:i');
            }
            return array('rows'=>$list,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }

    }

    public function getUserLogTime($paramArr){
        $task_id = 0;
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'log_time_id';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if($sortField=='start_time'){
            $sortField = " STR_TO_DATE(CONCAT_WS(\" \", DATE_FORMAT(lt.created_date_time , \"%Y-%m-%d\"), lt.start_time), \"%Y-%c-%d %h:%i %p\") ";
        }else if($sortField=='end_time'){
            $sortField = " STR_TO_DATE(CONCAT_WS(\" \", DATE_FORMAT(lt.created_date_time , \"%Y-%m-%d\"), lt.end_time), \"%Y-%c-%d %h:%i %p\") ";
        }else if($sortField=='created_date_time'){
//            $sortField = " STR_TO_DATE(lt.created_date_time , \"%Y-%b-%d\") ";
            $sortField = " Date(lt.created_date_time) ";
        }


        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam);
            $whereParam = " AND ( `pt`.`task_name` LIKE '%".$whereParam."%' || `p`.`project_name` LIKE '%".$whereParam."%' || `u`.`first_name` LIKE '%".$whereParam."%' || `u`.`last_name` LIKE '%".$whereParam."%' || `tt`.`name` LIKE '%".$whereParam."%' || `lt`.`comments` LIKE '%".$whereParam."%' ) ";
        }
        if(isset($paramArr['task_id'])){ $whereParam .= ' AND pt.id_project_task='.$paramArr['task_id'].' '; }
        $whereClause = "where true ".$whereParam;
        if(isset($paramArr['view']) && $paramArr['view']==1){

        }
        else if(isset($paramArr['view']) && $paramArr['view']==2){
            $whereClause .=" AND ud.department_id=".$this->session->userdata('department_id')." ";
        }
        else{
            $whereClause .= " AND lt.user_id=".$this->session->userdata('user_id')." ";
        }

        $SQL = 'SELECT `lt`.`log_time_id`, `lt`.`project_task_id`,  tt.name task_type_name, `lt`.`comments`, `lt`.`start_time`, `lt`.`end_time`, `lt`.`duration`, `lt`.`created_date_time`, `pt`.`task_name`, `pt`.`start_date`, `pt`.`end_date`, `p`.`project_name`,CONCAT(u.first_name," ",u.last_name) as user_name
                FROM `log_time` `lt`
                LEFT JOIN `user` `u` ON lt.user_id=u.id_user
                LEFT JOIN `user_department` `ud` ON u.id_user=ud.user_id
                LEFT JOIN `project_task` `pt` ON `lt`.`project_task_id`=`pt`.`id_project_task`
                LEFT JOIN `project` `p` ON `pt`.`project_id`=`p`.`id_project`
                LEFT JOIN task_type tt ON lt.task_type=tt.id_task_type
                  '.$whereClause.' order by '.$sortField.' '.$sortOrder.' '.$optLimit;

        $result = $this->db->query($SQL);

        //echo $this->db->last_query(); exit;

        $SQLCount = 'SELECT count(*)
                FROM `log_time` `lt`
                LEFT JOIN `user` `u` ON lt.user_id=u.id_user
                LEFT JOIN `user_department` `ud` ON u.id_user=ud.user_id
                LEFT JOIN `project_task` `pt` ON `lt`.`project_task_id`=`pt`.`id_project_task`
                LEFT JOIN `project` `p` ON `pt`.`project_id`=`p`.`id_project`
                LEFT JOIN task_type tt ON lt.task_type=tt.id_task_type
                '.$whereClause;

        $queryCount = $this->db->query($SQLCount);
        //echo $this->db->last_query(); exit;
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if($result->num_rows() > 0) {
            $list = $result->result();
            foreach($list as $k=>$v){
                $v->created_date_time = (new DateTime($v->created_date_time))->format('d-M-Y');
                $v->duration = (new DateTime($v->duration))->format('H:i');
            }
            return array('rows'=>$list,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }

    }

    public function getUserForProject($projectId)
    {
        $this->db->select('`pd`.*, `d`.`id_department`, `d`.`department_name`,GROUP_CONCAT(distinct CONCAT(u.first_name, " ", u.last_name)) As emp_name,GROUP_CONCAT(distinct pe.employee_id) As userid');
        $this->db->from('project_department pd');
        $this->db->join('user_department ud','pd.department_id=ud.department_id','left');
        $this->db->join('project_employees pe','pe.project_id=pd.project_id and ud.user_id=pe.employee_id');
        $this->db->join('department d','pd.department_id=d.id_department and ud.department_id=d.id_department','left');
        $this->db->join('user u','ud.user_id=u.id_user and pe.employee_id=u.id_user');
        $this->db->where('pd.project_id',$projectId);
        $this->db->group_by('pd.department_id');
        $this->db->order_by('pd.id_project_department');

        $list = $this->db->get()->result();
        //echo $this->db->last_query(); exit;
        if(count($list)>0){
            foreach($list as $key=>$val){
                $list[$key]->project_effort = 0;
                $logtime = $this->getLogTimeByDepartment(array('department_id'=>$val->department_id, 'project_id'=> $projectId));
                if(count($logtime)>0){
                    $list[$key]->project_effort = $logtime[0]['log_time'];
                }
            }
            return $list;
        }else{
            return [];
        }
    }

    public function getMilestoneById($milestoneId){
        $this->db->select('pm.id_milestone, pm.milestone_name, DATE_FORMAT(pm.milestone_start_date,"%d/%m/%Y") as milestone_start_date , DATE_FORMAT(pm.milestone_end_date,"%d/%m/%Y") as milestone_end_date, pm.milestone_status');
        $this->db->from('project_milestones pm');
        $this->db->where('pm.id_milestone',$milestoneId);

        $list = $this->db->get()->result();
        if(count($list)>0){
            return $list;
        }else{
            return [];
        }
    }

    public function getMilestoneForProject($projectId){
        $this->db->select('pm.id_milestone, pm.milestone_name, DATE_FORMAT(pm.milestone_start_date,"%d-%b-%Y") as milestone_start_date , DATE_FORMAT(pm.milestone_end_date,"%d-%b-%Y") as milestone_end_date, pm.milestone_status');
        $this->db->from('project_milestones pm');
        $this->db->where('pm.project_id',$projectId);

        $list = $this->db->get()->result();
        if(count($list)>0){
            return $list;
        }else{
            return [];
        }
    }

    public function getOpenTaskById($opentask_id){
        $this->db->select('poi.id_project_item, poi.open_item_description , poi.open_item_status , poi.created_date_time , poi.open_item_user_id');
        $this->db->from('project_open_items poi');
//        $this->db->join('user u','poi.open_item_user_id=u.id_user', 'left');
        $this->db->where('poi.id_project_item',$opentask_id);

        $list = $this->db->get()->result();
        if(count($list)>0){
            return $list;
        }else{
            return [];
        }
    }

    public function getOpenTaskForProject($projectId){
        $this->db->select('poi.id_project_item, poi.open_item_description , poi.open_item_status , CONCAT_WS(\' \',u.first_name, u.last_name) as name, poi.created_date_time');
        $this->db->from('project_open_items poi');
        $this->db->join('user u','poi.open_item_user_id=u.id_user', 'left');
        $this->db->where('poi.project_id',$projectId);

        $list = $this->db->get()->result();
        if(count($list)>0){
            return $list;
        }else{
            return [];
        }
    }

    public function deleteMilestone($milestoneId){
        $this->db->delete('project_milestones', array('id_milestone' => $milestoneId));
        return true;
    }
    public function isTaskAssignedToUser($taskId){
        $this->db->select('id_project_task');
        $this->db->from('project_task pt');
        $this->db->join('task_flow tf', 'pt.id_project_task=tf.project_task_id','left');
        $this->db->join('task_week_flow twf', 'tf.id_task_flow=twf.task_flow_id','left');
        $this->db->where('pt.id_project_task',$taskId);
        $result = $this->db->get()->result();
        if(count($result)>0){
            return true;
        }else{
            return false;
        }
    }
    public function deleteProjectTask($data){
        $this->db->delete('project_task', array('id_project_task' => $data['project_task_id']));
        return true;
    }
    public function deleteTask($data){
        /*$this->db->delete('task_flow', array('project_task_id' => $project_task_id,'task_status'=>'new'));
        $this->db->delete('project_task', array('id_project_task' => $project_task_id,'status'=>'new'));*/
        $this->db->delete('task_week_flow', array('task_flow_id' => $data['task_flow_id']));
        $this->db->delete('task_member', array('task_flow_id' => $data['task_flow_id']));
        $this->db->delete('task_usecase', array('project_task_id' => $data['project_task_id']));
        $this->db->delete('task_flow', array('project_task_id' => $data['project_task_id']));
        $this->db->delete('project_task', array('id_project_task' => $data['project_task_id']));
        return true;
    }
    public function deleteTaskFlow($data){
        if(isset($data['task_flow_id']))
            $this->db->where('id_task_flow',$data['task_flow_id']);
        $this->db->delete('task_flow');
    }

    public function checkTotalPercentage($inputPercentage,$proId){
        $totalPercentage = 0;
        $this->db->select('pd.estimation_percentage');
        $this->db->from('project_department pd');
        $this->db->where('project_id',$proId);
        $result = $this->db->get()->result();
        foreach($result as $k=>$v){
            $totalPercentage += (int)$v->estimation_percentage;
        }
        if($totalPercentage+(int)$inputPercentage > 100){
            return false;
        }else{
            return true;
        }
    }
    public function deleteProjectDepartmenUsers($deptId,$proId){
        $this->db->select('pe.*');
        $this->db->from('project_employees pe');
        $this->db->join('user u','pe.employee_id=u.id_user','left');
        $this->db->join('user_department ud','ud.user_id=u.id_user','left');
        $this->db->join('project_department pd','ud.department_id=pd.department_id','left');
        $this->db->where('pe.project_id',  $proId);
        $this->db->where('ud.department_id', $deptId);
        $this->db->group_by('pe.employee_id');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $result = $query->result_array();
        //echo "<pre>"; print_r($result);
        foreach($result as $res){
            $this->db->where('id_employee', $res['id_employee']);
            $this->db->delete('project_employees');
            //echo $this->db->last_query().'--';
        }
        //exit;
        return true;
    }
    public function checkDepartmentExist($data){
        $this->db->select('*');
        $this->db->from('project_department');
        $this->db->where('project_id', $data['project_id']);
        $this->db->where('department_id', $data['department_id']);
        $query = $this->db->get();
        $row = $query->result_array();
        if(!empty($row)){
            return true;
        }else{
            return false;
        }
    }

    //added by sthanka
    function getTaskFlow($data)
    {
        $this->db->select('*,TIME_FORMAT(ADDTIME(IFNULL(tf.estimated_time,"00:00"),IFNULL(tf.additional_time,"00:00")),"%H:%i") as total_estimated_time');
        $this->db->from('task_flow tf');
        $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
        if(isset($data['project_task_id']))
            $this->db->where('tf.project_task_id', $data['project_task_id']);
        if(isset($data['department_id']))
            $this->db->where('tf.department_id', $data['department_id']);
        if(isset($data['id_task_flow']))
            $this->db->where('tf.id_task_flow', $data['id_task_flow']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
       return $query->result_array();
    }

    function getTimeFromTaskFlow($data)
    {
        $this->db->select('*,IFNULL(TIME_FORMAT(tf.estimated_time,"%H:%i"),"00:00") as estimated_time,IFNULL(TIME_FORMAT(tf.additional_time,"%H:%i"),"00:00") as additional_time');
        $this->db->from('task_flow tf');
        $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
        if(isset($data['project_task_id']))
            $this->db->where('tf.project_task_id', $data['project_task_id']);
        if(isset($data['department_id']))
            $this->db->where('tf.department_id', $data['department_id']);
        if(isset($data['id_task_flow']))
            $this->db->where('tf.id_task_flow', $data['id_task_flow']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function addTaskFlow($data)
    {
        $this->db->insert('task_flow', $data);
        return $this->db->insert_id();
    }

    function updateTaskFlow($data,$task_flow_id)
    {
        $this->db->where('id_task_flow',$task_flow_id);
        $this->db->update('task_flow', $data);
    }

    function getTaskMember($data)
    {
        $this->db->select('*');
        $this->db->from('task_member tm');
        if(isset($data['task_flow_id']))
            $this->db->where('tm.task_flow_id', $data['task_flow_id']);
        if(isset($data['assigned_to']))
            $this->db->where('tm.assigned_to', $data['assigned_to']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function addTaskMember($data)
    {
        $this->db->insert('task_member', $data);
        return $this->db->insert_id();
    }

    function updateTaskMember($data,$task_member_id)
    {
        $this->db->where('id_task_member',$task_member_id);
        $this->db->update('task_member', $data);
        return 1;

    }

    function updateTaskMemberByTaskFlowId($data,$task_flow_id)
    {
        $this->db->where('task_flow_id',$task_flow_id);
        $this->db->update('task_member', $data);
        return 1;
    }

    function updateTaskMemberByTask($data)
    {
        if(isset($data['id_task_member']))
            $this->db->where('id_task_member',$data['id_task_member']);
        if(isset($data['task_flow_id']))
            $this->db->where('task_flow_id',$data['task_flow_id']);
        if(isset($data['assigned_to']))
            $this->db->where('assigned_to',$data['assigned_to']);
        $this->db->update('task_member', $data);
        return 1;
    }

    function deleteTaskMember($data)
    {
        if(isset($data['id_task_member']))
            $this->db->where('id_task_member',$data['id_task_member']);
        if(isset($data['task_flow_id']))
            $this->db->where('task_flow_id',$data['task_flow_id']);
        $this->db->delete('task_member');
        //echo $this->db->last_query(); exit;
    }

    function addTaskWeekLoad($data)
    {
        $this->db->insert('task_week_flow', $data);
        return $this->db->insert_id();
    }

    function getTaskWeekFlow($data)
    {
        $this->db->select('*');
        $this->db->from('task_week_flow twf');
        if(isset($data['id_task_week_flow']))
            $this->db->where('twf.id_task_week_flow', $data['id_task_week_flow']);
        if(isset($data['task_flow_id']))
            $this->db->where('twf.task_flow_id', $data['task_flow_id']);
        if(isset($data['user_id']))
            $this->db->where('twf.user_id', $data['user_id']);
        if(isset($data['day_id']))
            $this->db->where('twf.day_id', $data['day_id']);
        if(isset($data['date']))
            $this->db->where('DATE_FORMAT(twf.date,"%Y-%m-%d")', $data['date']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function updateTaskWeekFlow($data)
    {
        $this->db->where('id_task_week_flow',$data['id_task_week_flow']);
        $this->db->update('task_week_flow',$data);
        return 1;
    }

    public function deleteTaskWeekFlow($data)
    {
        if(isset($data['id_task_week_flow']))
            $this->db->where('id_task_week_flow',$data['id_task_week_flow']);
        if(isset($data['task_flow_id']))
            $this->db->where('task_flow_id',$data['task_flow_id']);
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);

        $this->db->delete('task_week_flow');
    }


    public function getDepartmentDetails($data)
    {
        $this->db->select('*');
        $this->db->from('department d');
        if(isset($data['department_name']))
            $this->db->where('d.department_name', $data['department_name']);
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->result_array();
    }

    public function getProjectDepartmentDetails($data)
    {//echo "<pre>"; print_r($data); exit;
        $this->db->select('*');
        $this->db->from('project_department pd');
        $this->db->join('department d','pd.department_id=d.id_department','left');
        if(isset($data['department_id']))
            $this->db->where('pd.department_id', $data['department_id']);
        if(isset($data['project_id']))
            $this->db->where('pd.project_id', $data['project_id']);
        $this->db->order_by('d.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDepartmentProjects($data)
    {
        $this->db->select('*');
        $this->db->from('project_department pd');
        $this->db->join('project p','pd.project_id=p.id_project','left');
        if(isset($data['department_id']))
            $this->db->where('pd.department_id', $data['department_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*public function getProjectDepartmentStatus($projectId){
        $this->db->select('*');
        $this->db->from('project_department pd');
        $this->db->join('department d','d.id_department = pd.department_id','left');
        $this->db->where('project_id', $projectId);
        $query = $this->db->get();
        $rows = $query->result_array();
        $finalResult = [];
        foreach($rows as $row){
            $this->db->select('lt.*,u.first_name,u.last_name,SUM(TIME_TO_SEC(duration)) as actual_time,d.department_name ');
            $this->db->from('log_time lt');
            $this->db->join('user u','u.id_user =lt.user_id','left');
            $this->db->join('user_department ud','u.id_user = ud.user_id','left');
            $this->db->join('department d','d.id_department = ud.department_id','left');
            $this->db->where('lt.project_id', $row['project_id'] );
            $this->db->where('ud.department_id', $row['department_id']);
            $query = $this->db->get();
            $result = $query->result_array();
            $result[0]['project_id'] = $row['project_id'];
            $result[0]['department_name'] = $row['department_name'];
            $result[0]['actual_time'] = $row['estimation_time']*60*60;
            if($result[0]['duration'] == ''){ $result[0]['duration'] = 0; }else{
                $time = explode(':',$result[0]['duration']);
                $result[0]['duration'] = ($time[0]*60*60 + $time[0]*60);
            }
            $finalResult[] = $result;
        }
       return $finalResult;
    }*/

    public function getProjectDepartmentStatus($projectId){
        $finalResult = [];

        $this->db->select('*');
        $this->db->from('project_department pd');
        $this->db->where('project_id', $projectId);
        $this->db->group_by('pd.department_id');
        $query = $this->db->get();
        $rows = $query->result_array();
        foreach($rows as $key=>$val){
            $this->db->select('SUM(TIME_TO_SEC(IFNULL(t.estimated_time,0))+TIME_TO_SEC(IFNULL(t.additional_time,0))) tot_est, SEC_TO_TIME(SUM(TIME_TO_SEC(IFNULL(t.estimated_time,0))+TIME_TO_SEC(IFNULL(t.additional_time,0)))) act_tot_est, d.department_name');
            $this->db->from('task_flow t');
            $this->db->join('project_task p', 'p.id_project_task=t.project_task_id' );
            $this->db->join('department d', 't.department_id=d.id_department' );
            $this->db->where('p.project_id', $projectId );
            $this->db->where('t.department_id', $val['department_id'] );
            $query = $this->db->get();
            $res = $query->result_array();
            if(count($res)>0){
                $finalResult[$val['department_id']]['department_name'] = $res[0]['department_name'];
                $finalResult[$val['department_id']]['dept_est'] = $res[0]['tot_est'];
                $finalResult[$val['department_id']]['act_dept_est'] = $res[0]['act_tot_est'];
            }

            $this->db->select('u.first_name,u.last_name,SUM(TIME_TO_SEC(duration)) as actual_time,TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(duration))),"%H:%i") as actual_time_hrs,d.department_name ');
            $this->db->from('log_time lt');
            $this->db->join('user u','u.id_user =lt.user_id','left');
            $this->db->join('user_department ud','u.id_user = ud.user_id','left');
            $this->db->join('department d','d.id_department = ud.department_id','left');
            $this->db->where('lt.project_id', $projectId );
            $this->db->where('ud.department_id', $val['department_id']);
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            $result = $query->result_array();
            if(count($result)>0){
                $finalResult[$val['department_id']]['project_id'] = $projectId;
                $finalResult[$val['department_id']]['actual_log_time'] = is_null($result[0]['actual_time'])?'0':$result[0]['actual_time'];
                $finalResult[$val['department_id']]['actual_time_hrs'] = is_null($result[0]['actual_time'])?'0':$result[0]['actual_time_hrs'];
            }
        }
        return $finalResult;
    }


    public function getProjectDepartmentTime($projectId){
        $finalResult = [];

        $this->db->select('*');
        $this->db->from('project_department pd');
        $this->db->join('department d','pd.department_id=d.id_department','left');
        $this->db->where('project_id', $projectId);
        $this->db->group_by('pd.department_id');
        $query = $this->db->get();
        $rows = $query->result_array();
        //echo "<pre>"; print_r($rows); exit;
        foreach($rows as $key=>$val){
            $this->db->select('
            TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(td.time))),"%H:%i") as over_all_estimated_time');
            $this->db->from('project_task pt');
            $this->db->join('task_department td','pt.id_project_task=td.task_id','left');
            $this->db->where('pt.project_id', $projectId );
            $this->db->where('td.department_id', $val['department_id']);
            $this->db->where('pt.pm_approved', 1);
            $this->db->where('pt.id_project_task is not NULL');

            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            $res0 = $query->result_array();

            $this->db->select('
            TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(t.estimated_time))),"%H:%i") as estimated_time,
            TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(t.additional_time))),"%H:%i") as additional_time');
            $this->db->from('project_task pt');
            $this->db->join('task_flow t', 'pt.id_project_task=t.project_task_id','left');
            $this->db->where('pt.project_id', $projectId );
            $this->db->where('t.department_id', $val['department_id']);
            $query = $this->db->get();
            $res = $query->result_array();

            $this->db->select('TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(tm.allowted_time))),"%H:%i") as allowted_time');
            $this->db->from('project_task pt');
            $this->db->join('task_flow t', 'pt.id_project_task=t.project_task_id','left');
            $this->db->join('task_member tm', 't.id_task_flow=tm.task_flow_id', 'left' );
            $this->db->where('pt.project_id', $projectId );
            $this->db->where('t.department_id', $val['department_id'] );
            $query = $this->db->get();
            $res1 = $query->result_array();

            $this->db->select('TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(lt.duration))),"%H:%i") as actual_time');
            $this->db->from('project_task pt');
            $this->db->join('task_flow t', 'pt.id_project_task=t.project_task_id','left');
            $this->db->join('log_time lt', 'lt.task_flow_id=t.id_task_flow','left' );
            $this->db->where('pt.project_id', $projectId );
            $this->db->where('t.department_id', $val['department_id'] );
            $query = $this->db->get();
            $res2 = $query->result_array();

            if(count($res)>0){
                $finalResult[$val['department_id']]['department_name'] = $val['department_name'];
                $finalResult[$val['department_id']]['over_all_estimated_time'] = $res0[0]['over_all_estimated_time'];
                $finalResult[$val['department_id']]['estimated_time'] = $res[0]['estimated_time'];
                $finalResult[$val['department_id']]['estimated_time_sec'] = time_to_sec($res[0]['estimated_time']);
                $finalResult[$val['department_id']]['additional_time'] = $res[0]['additional_time'];
                $finalResult[$val['department_id']]['additional_time_sec'] = time_to_sec($res[0]['additional_time']);
                $finalResult[$val['department_id']]['allowted_time'] = $res1[0]['allowted_time'];
                $finalResult[$val['department_id']]['allowted_time_sec'] = time_to_sec($res1[0]['allowted_time']);
                $finalResult[$val['department_id']]['actual_time'] = $res2[0]['actual_time'];
                $finalResult[$val['department_id']]['actual_time_sec'] = time_to_sec($res2[0]['actual_time']);
            }

            /*$this->db->select('u.first_name,u.last_name,SUM(TIME_TO_SEC(duration)) as actual_time,TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(duration))),"%H:%i") as actual_time_hrs,d.department_name ');
            $this->db->from('log_time lt');
            $this->db->join('user u','u.id_user =lt.user_id','left');
            $this->db->join('user_department ud','u.id_user = ud.user_id','left');
            $this->db->join('department d','d.id_department = ud.department_id','left');
            $this->db->where('lt.project_id', $projectId );
            $this->db->where('ud.department_id', $val['department_id']);
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            $result = $query->result_array();
            if(count($result)>0){
                $finalResult[$val['department_id']]['project_id'] = $projectId;
                $finalResult[$val['department_id']]['actual_log_time'] = is_null($result[0]['actual_time'])?'0':$result[0]['actual_time'];
                $finalResult[$val['department_id']]['actual_time_hrs'] = is_null($result[0]['actual_time'])?'0':$result[0]['actual_time_hrs'];
            }*/
        }
        return $finalResult;
    }



    public function getProjectProcessStatus($projectId){
        $this->db->select('COUNT(*) as count,pt.status');
        $this->db->from('project_task  pt');
        $this->db->where('pt.project_id', $projectId);
        $this->db->group_by('pt.status');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $rows = $query->result_array();
        //echo $this->db->last_query();  exit;
        $statusTypes = ['progress','new','completed'];
        $avbTypes = [];
        foreach($rows as $key => $val){
            $avbTypes[] = $val['status'];
        }
        $diff = array_diff($statusTypes,$avbTypes);
        foreach($diff as $item){
            $rows[] = ['status'=>$item,'count'=>0];
        }
       return $rows;
    }

    public function getEmployeeProjects($data)
    {
        $this->db->select('*');
        $this->db->from('project_employees pe');
        $this->db->join('project p','pe.project_id = p.id_project','left');
        if(isset($data['user_id']))
            $this->db->where('pe.employee_id', $data['user_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }


    public function fullCalenderTaskList($paramArr,$type='list')
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sortField'])?$paramArr['sortField']:'id_task_flow';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        /*$whereParam = isset($paramArr['parent_id'])?'pt.parent_id='.$paramArr['parent_id']:NULL;*/
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = 0;
        if($page!=0){ $offset = ($page - 1) * $limit; }

        // if(!empty($limit)) $optLimit = "limit $offset,$limit";
        //else $optLimit = NULL;
        $user_id = $this->session->userdata('user_id');
        $department_id = $this->session->userdata('department_id');

        $this->db->join('project_task p','tw.project_task_id=p.id_project_task','left')
            ->join('project pp',' pp.id_project = p.project_id','left')
            ->join('project_module pm','p.project_module_id=pm.id_project_module','left')
            ->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left')
            ->join('task_week_flow twf','tw.id_task_flow=twf.task_flow_id and tm.assigned_to=twf.user_id','left')
            ->join('user u','tm.assigned_to=u.id_user','left')
            ->join('log_time lts','`lts`.`task_flow_id` = `tw`.`id_task_flow` and `tm`.`assigned_to`=`lts`.`user_id`','left')

            ->from('task_flow tw');

            $this->db->where('tw.department_id',$this->session->userdata('department_id'));
            $this->db->where('assigned_to',$user_id);
            //$this->db->where('twf.date=CURDATE()');
        if(isset($paramArr['task_status']) && $paramArr['task_status']=='pending') {
            $this->db->where('twf.date<"' . date('Y-m-d') . '"');

        }
        else if(!isset($paramArr['task_status']) || (isset($paramArr['task_status']) && $paramArr['task_status']=='today')){
            $this->db->where('twf.date="' . date('Y-m-d') . '"');
            unset($paramArr['task_status']);
        }

        if(!empty($whereParam)){ $whereParam = specialCharReplace($whereParam); $this->db->where("p.task_name LIKE '%".$whereParam."%'"); }

        if(isset($paramArr['departmentIds'])){
            if(!empty($paramArr['departmentIds'])) {
                $this->db->where_in('p.project_id',$paramArr['projectIds']);
            }

        }
        if(isset($paramArr['projectIds'])){
            if(!empty($paramArr['projectIds'])){
                $this->db->where_in('p.project_id',$paramArr['projectIds']);
            }

        }
        if(isset($paramArr['projectStatus'])){
            if(!empty($paramArr['projectStatus'])){
                $this->db->where_in('tw.task_status',$paramArr['projectStatus']);
            }
        }


        if(isset($paramArr['task_status']) && $paramArr['task_status']=='pending'){
            $this->db->where('(tm.task_status = "pending" OR tm.task_status= "new")');
        }else{
            if(isset($paramArr['task_status']))
              $this->db->where('tm.task_status',$paramArr['task_status']);
        }

        if(isset($paramArr['project_users'])){
            if(!empty($paramArr['project_users'])){
                $this->db->where_in('u.id_user',$paramArr['project_users']);
            }

        }
        if(isset($paramArr['project_start_date'])){
            if(!empty($paramArr['project_start_date'])){
                $this->db->where('DATE(p.start_date) >=',$paramArr['project_start_date']);
            }

        }
        if(isset($paramArr['project_end_date'])){
            if(!empty($paramArr['project_end_date'])){
                $this->db->where('DATE(p.end_date) <=',$paramArr['project_end_date']);
            }

        }

        if($type =='list'){
            $this->db->select('tw.id_task_flow,tm.allowted_time as estimated_time,
            p.start_date,p.end_date,p.completed_date,
        CONCAT(u.first_name, " ", u.last_name) as user_name,u.email,
        p.id_project_task,pm.id_project_module,pm.module_name,tm.task_status as current_status,
        tm.assigned_to,p.task_name,pp.project_name,pp.id_project,p.description,IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(lts.duration))),"") as actual_time');

            $this->db->group_by('id_project_task');
            //$this->db->order_by($sortField,$sortOrder);
            $this->db->order_by('twf.id_task_week_flow','desc');
            //$this->db->limit($limit,$offset);

            $listResult = $this->db->get();
            //echo $this->db->last_query(); exit;
            //echo "<pre>"; print_r($listResult); exit;
            $listResult = $listResult->result_array();
            //echo "<pre>"; print_r($listResult); exit;
            $projectArryResult = array();
            foreach($listResult as $item){
                $projectArryResult[$item['project_name']][] = $item;

            }
            //echo "<pre>"; print_r($projectArryResult); exit;
            $index = 1;
            $finalResult = array();

            foreach($projectArryResult as $key=>$item){
                $temp = array();
                $temp['projectTitle'] = 1;
                $temp['parent_name'] = $key;
                $temp['current_status'] = $key;
                $temp['code'] = $index;
                $finalResult[] = $temp;
                $subIndex = 1;
                foreach($item as $row){
                    //echo '<pre>'; print_r($row); exit;
                    $temp = $row;
                    $temp['code'] = $index.'.'.$subIndex;
                    $temp['projectTitle'] = 0;
                    if(!empty($temp['end_date']))
                        $temp['end_date'] = date('Y-m-d', strtotime($temp['end_date']));
                    if(!empty($temp['start_date']))
                        $temp['start_date'] = date('Y-m-d', strtotime( $temp['start_date']));
                    if(!empty($temp['actual_time']))
                        $temp['actual_time'] = date('H:i:s', strtotime( $temp['actual_time']));
                    if($temp['actual_time']==''){ $temp['actual_time']='00:00:00'; }

                $finalResult[] = $temp;
                    $subIndex++;
                }
                $index++;
            }
            //echo "<pre>"; print_r($finalResult); exit;

            return array('rows'=>$finalResult,'projectCount'=>count($projectArryResult));

        }

    }

    public function getTaskMembers($data)
    {
        $this->db->select('*');
        $this->db->from('task_member tm');
        if(isset($data['task_flow_id']))
            $this->db->where('tm.task_flow_id',$data['task_flow_id']);
        if(isset($data['assigned_to']))
            $this->db->where('tm.assigned_to',$data['assigned_to']);
        if(isset($data['status']))
            $this->db->where('tm.task_status',$data['status']);
        if(isset($data['status_not']))
            $this->db->where('tm.task_status!=',$data['status_not']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function updateProjectTask($data)
    {
        $this->db->where('id_project_task',$data['id_project_task']);
        unset($data['id_project_task']);
        $this->db->update('project_task',$data);
//        echo $this->db->last_query(); exit;
        return 1;
    }

    public function addProjectTask($data)
    {
        $this->db->insert('project_task',$data);
        return 1;
    }
    public function getWeekAllotedTime($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( twf.time ) ) ), "%H:%i:%s" ),"00:00:00") as time');
        $this->db->from('task_week_flow twf');
        $this->db->join('task_flow tf','twf.task_flow_id=tf.id_task_flow','left');
        $this->db->where('DATE_FORMAT(twf.date,"%Y-%m-%d")>="'.$data['start_date'].'" and DATE_FORMAT(twf.date,"%Y-%m-%d")<="'.$data['end_date'].'"');
        $this->db->where('twf.user_id',$data['user_id']);
        if(isset($data['department_id']) && !empty($data['department_id'])){
            $this->db->where('tf.department_id',$data['department_id']);
        }
        /*else{
            $this->db->where('tf.department_id',$this->session->userdata('department_id'));
        }*/


        $query = $this->db->get();
        //echo $this->db->last_query().'</br>';
        return $query->result_array();
    }

    public function getWeekLogTime($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), "%H:%i:%s" ),"00:00:00") as time');
        $this->db->from('log_time lt');
        $this->db->join('task_flow tf','lt.task_flow_id=tf.id_task_flow','left');
        $this->db->where('DATE_FORMAT(lt.created_date_time,"%Y-%m-%d")>="'.$data['start_date'].'" and DATE_FORMAT(lt.created_date_time,"%Y-%m-%d")<="'.$data['end_date'].'"');
        $this->db->where('lt.user_id',$data['user_id']);
        if(isset($data['department_id']) && !empty($data['department_id'])){
            $this->db->where('tf.department_id',$data['department_id']);
        }
        /*else{
            $this->db->where('tf.department_id',$this->session->userdata('department_id'));
        }*/

        $this->db->where('lt.project_task_id is not null');
        $query = $this->db->get();
        //echo $this->db->last_query().'</br>';
        return $query->result_array();
    }

    public function getProjectTaskDetails($data)
    {
        $this->db->select('*,pt.status as task_status');
        $this->db->from('project_task pt');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        if(isset($data['id_project_task']))
            $this->db->where('pt.id_project_task',$data['id_project_task']);
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);

        $this->db->where('pt.pm_approved',1);
        $query = $this->db->get();
        //echo $this->db->last_query().'</br>';
        return $query->result_array();
    }

    public function saveRequestMoreTime($data)
    {
        $data['employee_id'] = $this->session->userdata('user_id');
        $data['status'] = 'pending';
        if($this->db->insert('request_more_time', $data)){
            return $this->db->insert_id();
        }
    }

    public function getRequestMoreTimeGrid($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'rtm.created_date_time';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['whereParam'])?$paramArr['whereParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam); /*$whereParam = "and (".$whereParam.")";*/
        }

        $whereClause = "where true AND rtm.employee_id='".$this->session->userdata('user_id')."' AND rtm.project_task_id='".$paramArr['task']."' AND rtm.project_task_id='".$paramArr['task']."' " .$whereParam;

        $SQL = 'SELECT rtm.*,CONCAT(u.first_name," ",u.last_name) as assigned_to_name, DATE_FORMAT(rtm.created_date_time,"%d-%b-%Y") created_date_time
                FROM request_more_time rtm
                LEFT JOIN user u ON rtm.assigned_to=u.id_user
                '.$whereClause.'
                order by '.$sortField.' '.$sortOrder.' '.$optLimit;

        $result = $this->db->query($SQL);

        $SQLCount = 'SELECT count(*)
                FROM request_more_time rtm
                LEFT JOIN user u ON rtm.assigned_to=u.id_user
                '.$whereClause;
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }

    }


    public function updateEstimatedTimeForWeekWorkLoad($data)
    {
        $this->db->where('id_task_flow', $data['id_task_flow']);
        return $this->db->update('task_flow' ,array('estimated_time'=> $data['time'].':00'));
    }
     public function getMoreTimeRequestById($task_id){
         $this->db->select('rmt.*, u.*, rmt.status status, rmt.employee_id employee_id, DATE_FORMAT(rmt.created_date_time,"%d-%b-%Y") created_date_time');
         $this->db->from('request_more_time rmt');
         $this->db->join('user u','u.id_user = rmt.employee_id','left');
//         $this->db->join('user_department ud','ud.user_id = u.id_user','left');
         $this->db->where('rmt.project_task_id', $task_id);
//         $this->db->where('ud.department_id', $this->session->userdata('department_id'));
         $query = $this->db->get();
         //echo $this->db->last_query().'</br>';
         return $query->result_array();
     }

    public function getRequestMoreTime($data)
    {
        $this->db->select('*');
        $this->db->from('request_more_time r');
        if(isset($data['project_task_id']))
            $this->db->where('r.project_task_id', $data['project_task_id']);
        if(isset($data['employee_id']))
            $this->db->where('r.employee_id',$data['employee_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAdditionalTime($params){
        $this->db->select('additional_time');
        $this->db->from('task_flow');
        if($params['task_flow_id']){
            $this->db->where('id_task_flow', $params['task_flow_id']);
        }
        return $this->db->get()->result();
    }

    public function actionRequestedTime($data){
        if($data['action']=='0'){
            $this->db->where('Id_request_more_time', $data['Id']);
            $this->db->update('request_more_time', array(
                'status' => 'rejected',
                'admin_comments' => $data['comment'],
                'approved_by' => $this->session->userdata('user_id'),
            ));
        }else{
            $this->db->where('Id_request_more_time', $data['Id']);
            $this->db->update('request_more_time', array(
                'status' => 'approved',
                'admin_comments' => $data['comment'],
                'duration' => $data['time'],
                'approved_by' => $this->session->userdata('user_id'),
            ));

            $this->db->select('additional_time');
            $this->db->from('task_flow');
            $this->db->where('id_task_flow' , $data['taskflowId']);
            $result = $this->db->get()->result();
            if(count($result)>0){
                $old_additional_time = $result[0]->additional_time;
                if($old_additional_time!='00:00:00' && $old_additional_time==null){
                    //
                } else if($old_additional_time=='00:00:00' && $old_additional_time!=null){
                    //
                } else{
                    $new_time = time_to_sec($data['time'])+time_to_sec($old_additional_time);
                    $data['time'] = sec_to_time($new_time);
                }
            }
            $this->db->where('id_task_flow' , $data['taskflowId']);
            $this->db->update('task_flow', array(
                'additional_time' => $data['time'],
            ));

            $task_flow = $this->getRequestMoreTimeTaskFlow(array('Id_request_more_time' => $data['Id']));
            $task_member = $this->getTaskMembers(array('task_flow_id' => $task_flow[0]['task_flow_id'],'assigned_to' => $task_flow[0]['employee_id']));
            $this->updateTaskMemberByTask(array(
                'id_task_member' => $task_member[0]['id_task_member'],
                'allowted_time' => sec_to_time(time_to_sec($task_flow[0]['duration'])+time_to_sec($task_member[0]['allowted_time'])),
                'additional_time' => sec_to_time(time_to_sec($task_flow[0]['duration'])+time_to_sec($task_member[0]['additional_time']))
            ));
        }
        return true;
    }

    public function getProjectTaskGrid($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'id_project_task';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;
        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and pt.task_name LIKE '%".$whereParam."%' or p.project_name LIKE '%".$whereParam."%'"; }
        $whereParam .= ' AND `pt`.`project_id` = '.$paramArr['projectIds'];
        $whereClause = "where true AND pt.pm_approved=0 ".$whereParam;
        $sql = <<<abc
            SELECT
            `pt`.*, `p`.`project_name`, IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(lt.duration))), "00:00:00") as actual_time,
            DATE_FORMAT(pt.start_date,"%d/%m/%Y")as start_date ,DATE_FORMAT(pt.end_date,"%d/%m/%Y")as end_date,DATE_FORMAT(pt.start_date,"%d-%b-%Y")as task_start_date ,DATE_FORMAT(pt.end_date,"%d-%b-%Y")as task_end_date,DATE_FORMAT(pt.completed_date,'%d-%b-%Y') completed_date,
            'p.id_project','department_id','start_date','end_date','project_task_name'
            FROM `project_task` `pt`
            LEFT JOIN `project` `p` ON `pt`.`project_id`=`p`.`id_project`
            LEFT JOIN `log_time` `lt` ON `pt`.`id_project_task`=`lt`.`project_task_id`
            $whereClause
            GROUP BY `pt`.`id_project_task`
          order by $sortField $sortOrder $optLimit
abc;
        $SQLCount = <<<abc
            SELECT `pt`.*, `p`.*
            FROM `project_task` `pt`
            LEFT JOIN `project` `p` ON `pt`.`project_id`=`p`.`id_project`
            LEFT JOIN `log_time` `lt` ON `pt`.`id_project_task`=`lt`.`project_task_id`
            $whereClause
            GROUP BY pt.id_project_task
abc;
        $queryCount = $this->db->query($SQLCount);
        $total_count = $queryCount->num_rows();
        $result = $this->db->query($sql);
        if($result->num_rows() > 0) {
            $custlist = $result->result();
            foreach($custlist as $k=>$v){
                $v->start_date = date('d-M-Y',strtotime($v->start_date));
                $v->end_date = date('d-M-Y',strtotime($v->end_date));
                if($v->completed_date!='') {
                    $v->completed_date = date('d-M-Y', strtotime($v->completed_date));
                }
            }
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }

        $listResult = $this->db->get();

        $listResult = $listResult->result_array();
//        echo $this->db->last_query(); exit;
        $projectArryResult = array();
        foreach($listResult as $item){
            $projectArryResult[$item['project_name']][] = $item;
        }
        for($s=0;$s<count($listResult);$s++)
        {
            $listResult[$s]['start_date'] = date('d-M-Y',strtotime($listResult[$s]['start_date']));
            $listResult[$s]['end_date'] = date('d-M-Y',strtotime($listResult[$s]['end_date']));
            if($listResult[$s]['completed_date']!=''){
                $listResult[$s]['completed_date'] = date('d-M-Y',strtotime($listResult[$s]['completed_date']));
            }
        }
        return array('rows'=>$listResult,'projectCount'=>count($listResult));

    }

    function getDepartmentProjectTask($data)
    {
        $this->db->select('tf.*,pt.task_name,pt.description as task_description');
        $this->db->from('task_flow tf');
        $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        if(isset($data['id_project_task']))
            $this->db->where('pt.id_project_task',$data['id_project_task']);
        if(isset($data['department_id']))
            $this->db->where('tf.department_id',$data['department_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    function getLogtime($data)
    {
        $this->db->select('p.project_name,CONCAT(u.first_name, " ", u.last_name) as user_name,pt.task_name,tt.name as task_type,lt.comments as description,lt.start_time,lt.end_time,lt.duration,DATE_FORMAT(lt.created_date_time,"%d-%b-%Y") as date');
        $this->db->from('log_time lt');
        $this->db->join('user u','u.id_user=lt.user_id','left');
        $this->db->join('project p','p.id_project=lt.project_id','left');
        $this->db->join('project_task pt','pt.id_project_task=lt.project_task_id','left');
        $this->db->join('task_type tt','tt.id_task_type=lt.task_type','left');
        if(isset($data['user_id']))
            $this->db->where('lt.user_id',$data['user_id']);
        if(isset($data['user_id_arr']))
            $this->db->where('lt.user_id in ('.$data['user_id_arr'].')');
        if(isset($data['start_date']) && isset($data['end_date']))
            $this->db->where('DATE_FORMAT(lt.created_date_time,"%Y-%m-%d")>="'.$data['start_date'].'" && DATE_FORMAT(lt.created_date_time,"%Y-%m-%d")<="'.$data['end_date'].'"');
        if(isset($data['project_id']) && !is_array($data['project_id']))
            $this->db->where('lt.project_id',$data['project_id']);
        else if(isset($data['project_id']) && is_array($data['project_id'])){
            $this->db->where('(lt.project_id in ('.join(',',$data['project_id']).') or lt.project_id is null)');
        }


        if(isset($data['project_task_id']))
            $this->db->where('lt.project_task_id',$data['project_task_id']);

         $this->db->order_by('date_format(STR_TO_DATE(`start_time`,"%h:%i %p"),"%H:%i:%s")');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getUserLogTimeByProject($data)
    {
        $this->db->select('SUM(TIME_TO_SEC(lt.duration)) as actual_time');
        $this->db->from('log_time lt');
        if(isset($data['user_id_arr']))
            $this->db->where('lt.user_id in ('.$data['user_id_arr'].')');
        if(isset($data['project_id'])){
            $this->db->where('lt.project_id',$data['project_id']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    function getUserLogTimeGrid($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'log_time_id';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['whereParam'])?$paramArr['whereParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); /*$whereParam = "and (".$whereParam.")";*/ }
        $whereClause = "where true ".$whereParam;

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = array(0); }

        if(isset($paramArr['project'])){ $project_id = $paramArr['project']; }
        else{ $project_id = array(0); }

        if(isset($paramArr['start_date'])){ $start_date = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['start_date']))); }
        else{ $start_date = 0; }

        if(isset($paramArr['end_date'])){ $end_date = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['end_date'])));}
        else{ $end_date = 0; }
        array_push($project_id,'""');
        //echo "<pre>"; print_r($project_id); exit;

        if($sortField=='date')
            $sortField='lt.created_date_time';
        else if($sortField=='start_time')
            $sortField='str_to_date(lt.start_time,\'%l:%i %p\')';
        else if($sortField=='end_time')
            $sortField='str_to_date(lt.end_time,\'%l:%i %p\')';

        $SQL = 'SELECT `p`.`project_name`, CONCAT(u.first_name, " ", u.last_name) as user_name, `pt`.`task_name`, `tt`.`name` as `task_type`, `lt`.`comments` as `description`,lt.start_time,lt.end_time, `lt`.`duration`,
                DATE_FORMAT(lt.created_date_time, "%d-%b-%Y") as date
                FROM `log_time` `lt`
                LEFT JOIN `user` `u` ON `u`.`id_user`=`lt`.`user_id`
                LEFT JOIN `project` `p` ON `p`.`id_project`=`lt`.`project_id`
                LEFT JOIN `project_task` `pt` ON `pt`.`id_project_task`=`lt`.`project_task_id`
                LEFT JOIN `task_type` `tt` ON `tt`.`id_task_type`=`lt`.`task_type`
                WHERE `lt`.`user_id` in ('.join(', ',($user_id)).')
                AND (lt.project_id in ('.join(', ',($project_id)).') or lt.project_id is null)
                AND DATE_FORMAT(lt.created_date_time,"%Y-%m-%d") >= "'.$start_date.'" && DATE_FORMAT(lt.created_date_time,"%Y-%m-%d")<="'.$end_date.'"
                order by '.$sortField.' '.$sortOrder.' '.$optLimit;

        $result = $this->db->query($SQL);

        //echo $this->db->last_query(); exit;

        $SQLCount = "select count(*) from `log_time` `lt`
                LEFT JOIN `project` `p` ON `p`.`id_project`=`lt`.`project_id`
                LEFT JOIN `project_task` `pt` ON `pt`.`id_project_task`=`lt`.`project_task_id`
                LEFT JOIN `task_type` `tt` ON `tt`.`id_task_type`=`lt`.`task_type`
                WHERE `lt`.`user_id` in (".join(', ',($user_id)).")
                AND (lt.project_id in (".join(', ',($project_id)).") or lt.project_id is null)
                AND DATE_FORMAT(lt.created_date_time,'%Y-%m-%d') >= '".$start_date."' && DATE_FORMAT(lt.created_date_time,'%Y-%m-%d')<='".$end_date."'";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    function userTasksByStatus($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'tw.id_task_flow';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['whereParam'])?$paramArr['whereParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) { $whereParam = specialCharReplace($whereParam); $whereParam = "and (".$whereParam.")"; }
        $whereClause = "where true ".$whereParam;

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = 0; }

        $start_date = date('Y-m-d', strtotime("-21 days"));
        $end_date = date('Y-m-d', strtotime($start_date. "+30 days"));

        if(isset($paramArr['status'])){ $status = $paramArr['status']; }
        else{ $status = 0; }

        $SQL = 'select project_name,task_name,tw.estimated_time, tm.allowted_time as time,tm.task_status from task_flow tw
                left join task_member tm on tw.id_task_flow=tm.task_flow_id
                left join project_task pt on tw.project_task_id=pt.id_project_task
                left join project p on pt.project_id = p.id_project
                where DATE_FORMAT(tw.start_date,\'%Y-%m-%d\') between "'.$start_date.'" and "'.$end_date.'"
                and tm.task_status="'.$status.'" and tm.assigned_to="'.$user_id.'"
                order by '.$sortField.' '.$sortOrder.' '.$optLimit;
    //echo $SQL; exit;
        $result = $this->db->query($SQL);

        $SQLCount = 'select count(*) from task_flow tw
                left join task_member tm on tw.id_task_flow=tm.task_flow_id
                left join project_task pt on tw.project_task_id=pt.id_project_task
                left join project p on pt.project_id = p.id_project
                where DATE_FORMAT(tw.start_date,\'%Y-%m-%d\') between "'.$start_date.'" and "'.$end_date.'"
                and tm.task_status="'.$status.'" and tm.assigned_to="'.$user_id.'"
                and tm.task_status="'.$status.'"';

        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }
    function checkExtraTime($data){


        if(isset($data['hour']))
            unset($data['hour']);
        if(isset($data['meridian']))
            unset($data['meridian']);
        if(isset($data['minute']))
            unset($data['minute']);

        $login  = DateTime::createFromFormat('h:i a', $data['start_time'])->getTimestamp();
        $logout    = DateTime::createFromFormat('h:i a', $data['end_time'])->getTimestamp();
        $data['start_time'] = (new DateTime($data['start_time']))->format('h:i a');
        $data['end_time'] = (new DateTime($data['end_time']))->format('h:i a');
        $interval = $logout-$login;
        $data['duration'] =gmdate("H:i:s", $interval);

        /* get logged time*/
        $this->db->select('SEC_TO_TIME(sum(TIME_TO_SEC(duration))) as loggedTime,project_task_id');
        $this->db->from('log_time');
        $this->db->where('project_task_id',$data['project_task_id']);
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('created_date_time',$data['created_date_time']);
        $result = $this->db->get()->first_row();

        $logged_time = time_to_sec($result->loggedTime);
        $current_time = time_to_sec($data['duration']);

        $logged_time = sec_to_time($logged_time+$current_time);

        /*get allowted time*/

        $this->db->select('tm.allowted_time,tf.estimated_time, IFNULL(tf.additional_time,"00:00:00") additional_time');
        $this->db->from('task_member tm');
        $this->db->join('task_flow tf','tf.id_task_flow = tm.task_flow_id','left');
        $this->db->where('tf.project_task_id',$data['project_task_id']);
        $this->db->where('tm.assigned_to',$data['user_id']);
        //$this->db->get();
        $result = $this->db->get()->first_row();
//        $allowted_time = time_to_sec($result->allowted_time) + time_to_sec( $result->additional_time);
        $allowted_time = time_to_sec($result->allowted_time);
//        echo '<pre>'; print_r($allowted_time .' - '.time_to_sec($logged_time)); exit;

        if($allowted_time >= time_to_sec($logged_time))
            return true;
        return false;

    }

    public function getProjectMilestoneList($projectId){
        $this->db->select('*');
        $this->db->from('project_milestones');
        if(isset($projectId)){
            if(preg_match_all('/-/', $projectId)){
                $projectId = explode('-', $projectId);
                $this->db->where_in('project_id', $projectId);
            }else{
                $this->db->where('project_id',$projectId);
            }
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function getTotalLogTime($data)
    {
        $this->db->select('IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(duration))),"00:00") as actual_time');
        $this->db->from('log_time');
        if(isset($data['task_flow_id']))
            $this->db->where('task_flow_id',$data['task_flow_id']);
        if(isset($data['project_task_id']))
            $this->db->where('project_task_id',$data['project_task_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    function getProjectDepartmentId($data){
        $this->db->select('*');
        $this->db->from('project_department');
        if(isset($data['deptt']))
            $this->db->where('department_id',$data['deptt']);
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    function getProjectDepartmentData($id){
        $this->db->select('project_id, department_id, estimation_time, estimation_percentage');
        $this->db->from('project_department');
        if(isset($id))
            $this->db->where('id_project_department',$id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    function updateProjectDepartmentById($id , $data){
        $this->db->where('id_project_department', $id);
        $this->db->update('project_department', $data);
        return true;
    }

    function getProjectTaskDetailsByProjectTaskId($id){
        $this->db->select('*');
        $this->db->from('project_task');
        if(isset($id))
            $this->db->where('id_project_task',$id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    /*function deleteProjectDepartmentUsers($department_id,$project_id)
    {
        $this->db->where('project_id',$project_id);
        $this->db->where('employee_id',$department_id);
        $this->db->delete('project_employees');
    }*/

    function getUserDayLoad($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( twf.time ) ) ), "%H:%i:%s" ),"00:00:00") as alloted_time');
        $this->db->from('task_week_flow twf');
        if(isset($data['date'])){
            $this->db->where('DATE_FORMAT(twf.date, "%Y-%m-%d")=',$data['date']);
        }
        if(isset($data['user_id'])){
            $this->db->where('user_id',$data['user_id']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    function getUserDayOriginalLoad($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( twf.time ) ) ), "%H:%i:%s" ),"00:00:00") as alloted_time');
        $this->db->from('task_week_flow twf');
        $this->db->join('task_member tm','twf.task_flow_id=tm.task_flow_id and twf.user_id=tm.assigned_to','left');
        if(isset($data['date'])){
            $this->db->where('DATE_FORMAT(twf.date, "%Y-%m-%d")=',$data['date']);
        }
        if(isset($data['user_id'])){
            $this->db->where('user_id',$data['user_id']);
            $this->db->where('tm.assigned_to',$data['user_id']);
        }
        $this->db->where('tm.task_status!=','completed');
        $this->db->where('tm.task_status!=','hold');
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    function getTaskWiseUserAllottedTime($data)
    {

        $this->db->select('twf.user_id,CONCAT(u.first_name," ",u.last_name) as user_name,p.project_name,pt.task_name,twf.task_flow_id,tm.task_status,
            TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(twf.time))), \'%H:%i:%s\') as allotted_time,DATE_FORMAT(twf.date,"%d-%m-%Y") as date');
        $this->db->from('task_week_flow twf');
        $this->db->join('task_flow tw','twf.task_flow_id=tw.id_task_flow','left');
        $this->db->join('task_member tm','tm.task_flow_id=tw.id_task_flow','left');
        $this->db->join('project_task pt','tw.project_task_id=pt.id_project_task','left');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        $this->db->join('user u','tm.assigned_to=u.id_user','left');
        if (isset($data['user_id'])) {
            $this->db->where('twf.user_id', $data['user_id']);
            $this->db->where('tm.assigned_to', $data['user_id']);
        }
        if (isset($data['start_date']) && isset($data['end_date']))
            $this->db->where('DATE_FORMAT(twf.date,"%Y-%m-%d")>="' . $data['start_date'] . '" && DATE_FORMAT(twf.date,"%Y-%m-%d")<="' . $data['end_date'] . '"');

        $this->db->group_by('twf.task_flow_id');
        $this->db->order_by('twf.date,project_name','DESC');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getLogTimeByUser($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(IFNULL(lt.duration,"00:00")))), \'%H:%i\'),"00:00") as log_time');
        $this->db->from('log_time lt');
        if(isset($data['user_id']))
            $this->db->where('lt.user_id',$data['user_id']);
        if(isset($data['department_id'])){
            $this->db->join('user_department ud','lt.user_id=ud.user_id');
            $this->db->where('lt.project_id',$data['project_id']);
        }
        if(isset($data['project_task_id']))
            $this->db->where('lt.project_task_id',$data['project_task_id']);
        if(isset($data['task_flow_id']))
            $this->db->where('lt.task_flow_id',$data['task_flow_id']);
        if(isset($data['date']))
            $this->db->where('DATE_FORMAT(lt.created_date_time,"%Y-%m-%d")',date('Y-m-d',strtotime($data['date'])));

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getAllottedTimeOfUser($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(IFNULL(tm.allowted_time, "00:00")))), \'%H:%i\'), "00:00") as allotted_time,
        IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(IFNULL(tm.allowted_time, "0")))-SUM(TIME_TO_SEC(IFNULL(tm.additional_time, "0")))), \'%H:%i\'), "00:00") est_time,
        IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(IFNULL(tm.additional_time, "0")))), \'%H:%i\'), "00:00") add_time');
        $this->db->from('task_flow tf');
        $this->db->join('task_member tm','tf.id_task_flow=tm.task_flow_id','left');
        if(isset($data['user_id']))
            $this->db->where('tm.assigned_to',$data['user_id']);
        if(isset($data['project_task_id']))
            $this->db->where('tf.project_task_id',$data['project_task_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getLogTimeByDepartment($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(IFNULL(lt.duration,"00:00")))), \'%H:%i\'),"00:00") as log_time');
        $this->db->from('log_time lt');
        if(isset($data['user_id']))
            $this->db->where('lt.user_id',$data['user_id']);
        if(isset($data['department_id'])){
            $this->db->join('user_department ud','lt.user_id=ud.user_id');
            $this->db->where('ud.department_id',$data['department_id']);
            $this->db->where('lt.project_id',$data['project_id']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    function getMembersTaskGrid($paramArr)
    {
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='')?$paramArr['sidx']:'id_task_week_flow';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        /*if(!empty($whereParam)) $whereParam = "and (".$whereParam.")";
        $whereClause = "where true ".$whereParam;*/

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = array(0); }

        if(isset($paramArr['project'])){ $project_id = $paramArr['project']; }
        else{ $project_id = array(0); }

        if(isset($paramArr['sdate']) && $paramArr['sdate']!=''){ $sdata = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['sdate']))); }
        else{ $sdata = 0; }
        if(isset($paramArr['edate']) && $paramArr['edate']!=''){ $edata = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['edate']))); }
        else{ $edata = 0; }

        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam);
            $timeParams = "";
            if(preg_match_all('/:/', $whereParam)){
                $timeParams = " || twf.time like '%".$whereParam."%' || tw.additional_time like '%".$whereParam."%' ";
            }
            $whereParam = "and (p.project_name LIKE '%".$whereParam."%' || u.first_name LIKE '%".$whereParam."%' || u.last_name LIKE '%".$whereParam."%' || pt.task_name LIKE '%".$whereParam."%' || tw.task_status LIKE '%".$whereParam."%' $timeParams )";
        }
        if(!empty($user_id)) {
            $whereParam .= ' AND `twf`.`user_id` in (' . join(', ', ($user_id)) . ')';
        }
        if(!empty($project_id)) {
            $whereParam .= ' AND (pt.project_id in (' . join(', ', ($project_id)) . '))';
        }
        if($sdata!=0 && $edata!=0){
            $whereParam .= ' AND DATE_FORMAT(twf.date,"%Y-%m-%d") BETWEEN "'.$sdata.'" AND "'.$edata.'"';
        }
        $whereClause = "where true ".$whereParam;

        array_push($project_id,'""');
        //echo "<pre>"; print_r($project_id); exit;

        $SQL = 'SELECT twf.task_flow_id,tw.project_task_id,twf.user_id,pt.project_id,twf.date,`p`.`project_name`, CONCAT(u.first_name, " ", u.last_name) as user_name, `pt`.`task_name`, IFNULL(twf.time,"00:00:00") as allotted_time,tw.task_status as status
                FROM task_week_flow twf
                LEFT JOIN task_flow tw ON twf.task_flow_id=tw.id_task_flow
                LEFT JOIN task_member tm ON twf.task_flow_id=tw.id_task_flow
                LEFT JOIN project_task pt ON tw.project_task_id=pt.id_project_task
                LEFT JOIN project p ON pt.project_id=p.id_project
                LEFT JOIN user u ON twf.user_id=u.id_user
                '. $whereClause .' GROUP BY twf.task_flow_id
                order by '.$sortField.' '.$sortOrder.' '.$optLimit;
        $result = $this->db->query($SQL);

        //echo $this->db->last_query(); exit;

        $SQLCount = 'SELECT twf.task_flow_id,tw.project_task_id,twf.user_id,pt.project_id,twf.date,`p`.`project_name`, CONCAT(u.first_name, " ", u.last_name) as user_name, `pt`.`task_name`, TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(IFNULL(twf.time,"00:00:00")))), \'%H:%i:%s\') as allotted_time,tw.task_status as status
                FROM task_week_flow twf
                LEFT JOIN task_flow tw ON twf.task_flow_id=tw.id_task_flow
                LEFT JOIN task_member tm ON twf.task_flow_id=tw.id_task_flow
                LEFT JOIN project_task pt ON tw.project_task_id=pt.id_project_task
                LEFT JOIN project p ON pt.project_id=p.id_project
                LEFT JOIN user u ON twf.user_id=u.id_user
                '. $whereClause .' GROUP BY twf.task_flow_id';
        //echo $SQLCount; exit;
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->result();
        $total_count = count($resultCount);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function userTaskGrid($paramArr){
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = (isset($paramArr['sidx']) && trim($paramArr['sidx'])!='' && $paramArr['sidx'] != 'logtime')?$paramArr['sidx']:'id_task_week_flow';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(isset($paramArr['user_id'])){ $user_id = $paramArr['user_id']; }
        else{ $user_id = null; }

        if(isset($paramArr['project'])){ $project_id = $paramArr['project']; }
        else{ $project_id = null; }

        if(isset($paramArr['department_id'])){ $department_id = $paramArr['department_id']; }
        else{ $department_id = null; }

        if(isset($paramArr['sdate']) && $paramArr['sdate']!=''){ $sdata = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['sdate']))); }
        else{ $sdata = 0; }
        if(isset($paramArr['edate']) && $paramArr['edate']!=''){ $edata = date('Y-m-d',strtotime(str_replace('/','-',$paramArr['edate']))); }
        else{ $edata = 0; }

        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam);
            $timeParams = "";
            if(preg_match_all('/:/', $whereParam)){
                $timeParams = " || twf.time like '%".$whereParam."%' || tw.additional_time like '%".$whereParam."%' ";
            }
            $whereParam = " and (p.project_name LIKE '%".$whereParam."%' || u.first_name LIKE '%".$whereParam."%' || u.last_name LIKE '%".$whereParam."%' || pt.task_name LIKE '%".$whereParam."%' || tw.task_status LIKE '%".$whereParam."%' $timeParams )";
        }
        if(!is_null($user_id)) {
            $whereParam .= ' AND `twf`.`user_id` in (' . $user_id . ')';
        }else{
            $whereParam .= ' AND `twf`.`user_id` in (' . $this->session->userdata('user_id') . ')';
        }
        if(!is_null($project_id)) {
            $whereParam .= ' AND (pt.project_id in (' . $project_id . '))';
        }

        if(!is_null($department_id)) {
            $whereParam .= ' AND (td.department_id in (' . $department_id . ') || tw.department_id in (' . $department_id . '))';
        }
        if($sdata!=0 && $edata!=0){
            $whereParam .= ' AND DATE_FORMAT(twf.date,"%Y-%m-%d") BETWEEN "'.$sdata.'" AND "'.$edata.'"';
        }
        if(isset($paramArr['status']) && $paramArr['status'] != 'select' && $paramArr['status'] != '' ){ $whereParam .= ' AND tw.task_status="'.$paramArr['status'].'" '; }
        $whereClause = "where true ".$whereParam;

        $SQL = 'SELECT twf.task_flow_id,tw.project_task_id,twf.user_id,twf.date,  pt.project_id,`pt`.`task_name`, `p`.`project_name`, CONCAT(u.first_name, " ", u.last_name) as user_name,IFNULL(twf.time,"00:00:00") as allotted_time, tw.task_status as status, IFNULL(tw.additional_time,\' -- \') additional_time, DATE_FORMAT(tm.created_date_time, "%d-%b-%Y") created_date_time
                FROM task_week_flow twf
                    LEFT JOIN task_flow tw ON twf.task_flow_id=tw.id_task_flow
                    LEFT JOIN task_department td ON tw.project_task_id=td.task_id
                    LEFT JOIN task_member tm ON twf.task_flow_id=tm.task_flow_id
                    LEFT JOIN project_task pt ON tw.project_task_id=pt.id_project_task
                    LEFT JOIN project p ON pt.project_id=p.id_project
                    LEFT JOIN user u ON twf.user_id=u.id_user  ';
        $SQL .= $whereClause.' GROUP BY twf.task_flow_id
                order by '.$sortField.' '.$sortOrder.' '.$optLimit;

        $result = $this->db->query($SQL);
//        echo $this->db->last_query(); exit;

        $SQLCount = 'SELECT twf.task_flow_id
                    FROM task_week_flow twf
                        LEFT JOIN task_flow tw ON twf.task_flow_id=tw.id_task_flow
                        LEFT JOIN task_department td ON tw.project_task_id=td.task_id
                        LEFT JOIN task_member tm ON twf.task_flow_id=tm.task_flow_id
                        LEFT JOIN project_task pt ON tw.project_task_id=pt.id_project_task
                        LEFT JOIN project p ON pt.project_id=p.id_project
                        LEFT JOIN user u ON twf.user_id=u.id_user ';
        $SQLCount .= $whereClause.' GROUP BY twf.task_flow_id';
        //echo $SQLCount; exit;
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->result();
        //echo "<pre>"; print_r($resultCount); exit;
        $total_count = count($resultCount);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getTaskTimeByProject($data)
    {
        $this->db->select('p.project_name,tf.id_task_flow,pt.task_name,d.department_name,tf.estimated_time,IFNULL(tf.additional_time,"00:00:00") as additional_time,ADDTIME(IFNULL(tf.estimated_time,"00:00:00"),IFNULL(tf.additional_time,"00:00:00")) as total_time');
        $this->db->from('task_flow tf');
        $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
        $this->db->join('project p','pt.project_id=p.id_project','left');
        $this->db->join('department d','tf.department_id=d.id_department','left');
        if(isset($data['project_id']))
            $this->db->where('pt.project_id',$data['project_id']);
        $this->db->group_by('id_task_flow');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLogTimeByTaskFlow($data)
    {
        $this->db->select('CONCAT(CONCAT(u.first_name," ",u.last_name),\'-\',IFNULL(TIME_FORMAT( SEC_TO_TIME( SUM( TIME_TO_SEC( lt.duration ) ) ), "%H:%i" ),"00:00")) as members');
        $this->db->from('log_time lt');
        $this->db->join('user u','lt.user_id=u.id_user','left');
        if(isset($data['task_flow_id']))
            $this->db->where('lt.task_flow_id',$data['task_flow_id']);
        $this->db->group_by('lt.user_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUsersTaskDetailsByTaskId($data){
        $this->db->select('d.department_name,t.task_flow_id, t.task_status, t.created_date_time, t.allowted_time, t.additional_time, t.task_status status,
                tf.department_id, DATE_FORMAT(tf.start_date, \'%d-%b-%Y\') start_date, DATE_FORMAT(tf.end_date, \'%d-%b-%Y\') end_date, DATE_FORMAT(tf.actual_end_date, \'%d-%b-%Y\') actual_end_date,
                u.id_user, concat_ws(\' \', u.first_name, u.last_name) name, u1.id_user, concat_ws(\' \', u1.first_name, u1.last_name) approved_by, DATE_FORMAT(t.updated_date_time,"%d-%m-%Y") updated_date_time');
        $this->db->from('task_member t');
        $this->db->join('user u','t.assigned_to=u.id_user','left');
        $this->db->join('user u1','t.updated_by=u1.id_user','left');
        $this->db->join('user_department ud','ud.user_id=u.id_user','left');
        $this->db->join('task_flow tf','t.task_flow_id=tf.id_task_flow','left');
        $this->db->join('department d','tf.department_id=d.id_department','left');
        $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
        if(isset($data['project_task_id']))
            $this->db->where('pt.id_project_task',$data['project_task_id']);
        if(isset($data['department_id']))
            $this->db->where('ud.department_id',$data['department_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectModuleList($projectId){
        $this->db->select('*');
        $this->db->from('project_module');
        if(isset($projectId)){
            if(preg_match_all('/-/', $projectId)){
                $projectId = explode('-', $projectId);
                $this->db->where_in('project_id', $projectId);
            }else{
                $this->db->where('project_id',$projectId);
            }
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function getWorkRequestGrid($paramArr)
    {
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sidx'])?$paramArr['sidx']:'id_work_request';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if($sortField=='date'){
            $sortField = " STR_TO_DATE(w.date,'%Y-%m-%d 00:00:00') ";
        }else if($sortField=='name'){
            $sortField = " u.first_name ";
        }else if($sortField=='status'){
            $sortField = " w.status ";
        }else{
            $sortField = " id_work_request ";
        }

        if(!empty($whereParam))  {
            $whereParam = specialCharReplace($whereParam);
            if(preg_match_all('/-/', $whereParam)){
                $whereParam = " AND ( DATE_FORMAT(w.date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') || DATE_FORMAT(w.created_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y'))";
            }else{
                $whereParam = "and ( w.description like '%".$whereParam."%'
                 OR u.first_name LIKE '%".$whereParam."%'
                 OR u.last_name LIKE '%".$whereParam."%'
                 OR w.admin_comments like '%".$whereParam."%'
                 OR w.status  like '%".$whereParam."%' )";
            }
        }

        $whereClause = "where true AND user_id='".$this->session->userdata('user_id')."' ".$whereParam;
        $groupBy = ' GROUP BY w.id_work_request ';

        $SQL = "SELECT w.*, u.*, concat_ws(' ', u.first_name, u.last_name) name, DATE_FORMAT(w.date, '%d-%b-%Y') date, DATE_FORMAT(w.date, '%d/%m/%Y') actualDate, DATE_FORMAT(w.created_date, '%d-%b-%Y') created_date, w.status status
               from work_request w
                LEFT JOIN user u ON w.approved_by=u.id_user
                $whereClause $groupBy order by $sortField $sortOrder $optLimit ";
//        echo $SQL; exit;
        $SQLCount = "SELECT count(*)
               from work_request w
                LEFT JOIN user u ON w.approved_by=u.id_user $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        //echo "<pre>"; print_r($SQL); exit;
        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function addWorkRequest($data){
        $this->db->insert('work_request', $data);
        return $this->db->insert_id();
    }

    public function updateWorkRequest($id , $data){
        $this->db->where('id_work_request', $id);
        $this->db->update('work_request', $data);
        return $id;
    }

    public function deleteWorkRequestById($id){
        $this->db->where('id_work_request', $id);
        $this->db->delete('work_request');
        return true;
    }

    public function isWorkRequestStatusChange($id){
        $this->db->select('*');
        $this->db->from('work_request');
        $this->db->where('id_work_request', $id);
        $this->db->where('approved_by IS NOT NULL');

        $result = $this->db->get()->result();
        if(count($result) > 0){
            return $result[0]->status;
        }else{
            return 'notfound';
        }

    }

    public function getAllWorkRequestGrid($paramArr) {
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sidx'])?$paramArr['sidx']:'id_work_request';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if($sortField=='date'){
            $sortField = " STR_TO_DATE(w.date,'%Y-%m-%d 00:00:00') ";
        }else if($sortField=='name'){
            $sortField = " u.first_name ";
        }else if($sortField == 'requested_by'){
            $sortField = " u1.first_name ";
        }else if($sortField=='status'){
            $sortField = " w.status ";
        }else{
            $sortField = " id_work_request ";
        }

        if(!empty($whereParam))  {
            $whereParam = specialCharReplace($whereParam);
            if(preg_match_all('/-/', $whereParam)){
                    $whereParam = " AND (DATE_FORMAT(w.date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') OR DATE_FORMAT(w.created_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') )";
            }else{
                $whereParam = "and ( w.description like '%".$whereParam."%'
                 OR u.first_name LIKE '%".$whereParam."%'
                 OR u.last_name LIKE '%".$whereParam."%'
                 OR u1.first_name LIKE '%".$whereParam."%'
                 OR u1.last_name LIKE '%".$whereParam."%'
                 OR w.admin_comments like '%".$whereParam."%'
                 OR w.status  like '%".$whereParam."%' )";
            }
        }
        $whereClause = "where true AND w.approved_by=".$this->session->userdata('user_id')." ".$whereParam;
        $groupBy = ' GROUP BY w.id_work_request ';

        $SQL = "SELECT w.*, u.*, concat_ws(' ', u.first_name, u.last_name) name, concat_ws(' ', u1.first_name, u1.last_name) requested_by, DATE_FORMAT(w.date, '%d-%b-%Y') date, DATE_FORMAT(w.date, '%d/%m/%Y') actualDate, DATE_FORMAT(w.created_date, '%d-%b-%Y') created_date, w.status status
               from work_request w
                LEFT JOIN user u ON w.approved_by=u.id_user
                LEFT JOIN user u1 ON w.user_id=u1.id_user
                $whereClause $groupBy order by $sortField $sortOrder $optLimit ";
        //echo $SQL; exit;
        $SQLCount = "SELECT count(*)
               from work_request w
                LEFT JOIN user u ON w.approved_by=u.id_user
                LEFT JOIN user u1 ON w.user_id=u1.id_user
                $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        //echo "<pre>"; print_r($SQL); exit;
        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getWorkRequestById($data){
        $this->db->select('*, DATE_FORMAT(date, \'%d/%m/%Y\') date, date org_date');
        $this->db->from('work_request');
        if(isset($data['id_work_request']))
            $this->db->where('id_work_request', $data['id_work_request']);

        $result = $this->db->get()->result();
        if(count($result)>0){
            return $result[0];
        }else{
            return [];
        }
    }

    public function isTaskPresent($data)
    {
        $this->db->select('*');
        $this->db->where(array('project_id' => $data['project_id'], 'project_module_id' => $data['module_id'], 'task_name' => $data['task_name']));
        $this->db->from('project_task');
        $query = $this->db->get();
        $task = $query->row();
//        die($this->db->last_query());
        $res = $query->result_array();
        if(count($res)){
            return $res;
        }else{
            return false;
        }
    }

    public function insertNewTask($data)
    {
        $this->db->insert('project_task', $data);
        return $this->db->insert_id();
    }

    public function isTaskAlloted($data)
    {
        $this->db->select('*');
        $this->db->where(array('task_flow_id' => $data['task_flow_id'], 'user_id' => $data['user_id']));
        $this->db->from('task_week_flow');
        $query = $this->db->get();
//        $task = $query->row();
        $res = $query->result_array();
        if(count($res)){
            return $res;
        }else{
            return false;
        }
    }

    public function getProjectRequirement($data)
    {
        $this->db->select('*');
        $this->db->from('requirements');
        if(preg_match('/-/' , $data['project_id'])){
            $data['project_id'] = explode('-',$data['project_id']);
            $this->db->where_in('project_id',$data['project_id']);
        }else{
            $this->db->where('project_id',$data['project_id']);
        }
        $this->db->group_by('id_requirements');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function isUniqueRequirementName($data)
    {
        $this->db->select('*');
        $this->db->from('requirements');
        $this->db->where('project_id',$data['project_id']);
        $this->db->where('requirement_name',$data['requirement_name']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function isUniqueRequirementTask($data)
    {
        $this->db->select('*');
        $this->db->from('project_task');
        if(isset($data['requirement_id']))
            $this->db->where('requirement_id',$data['requirement_id']);
        if(isset($data['task_name']))
            $this->db->where('task_name',$data['task_name']);
        if(isset($data['id_project_task']))
            $this->db->where('id_project_task!='.$data['id_project_task']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function requirementTaskTotal($data)
    {
        $this->db->select('count(*) as total, SUM(TIME_TO_SEC(estimated_time)) estimated_time, id_project_task, end_date');
        $this->db->from('project_task');
        if(isset($data['requirement_id']))
            $this->db->where('requirement_id',$data['requirement_id']);
        if(isset($data['task_name']))
            $this->db->where('task_name',$data['task_name']);
        if(isset($data['id_project_task']))
            $this->db->where('id_project_task!='.$data['id_project_task']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRequrementListGrid($paramArr)
    {
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sidx']) && strlen($paramArr['sidx'])>0?$paramArr['sidx']:'id_requirements';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if(!empty($whereParam)) {
            $whereParam = specialCharReplace($whereParam);
            if(preg_match_all('/\d-\d/', $whereParam)){
                $whereParam = " AND (DATE_FORMAT(r.end_date, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%d-%m-%Y') OR DATE_FORMAT(r.created_date_time, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%d-%m-%Y') )";
            }else {
                $whereParam = "and ( r.requirement_name like '%" . $whereParam . "%' OR r.requirement_document LIKE '%" . $whereParam . "%')";
            }
        }

        if($sortField=='created_date_time'){
            $sortField = " r.created_date_time ";
        }else if($sortField=='end_date'){
            $sortField = " r.end_date ";
        }

        $whereClause = "where true AND r.project_id='".$paramArr['project_id']."' ".$whereParam;
        $groupBy = ' GROUP BY r.id_requirements ';

        $SQL = "SELECT r.*, DATE_FORMAT(r.end_date,'%d-%m-%Y') as end_date, DATE_FORMAT(r.created_date_time,'%d-%m-%Y') as created_date
                FROM requirements r
                $whereClause $groupBy order by $sortField $sortOrder $optLimit ";
        //echo $SQL; exit;
        $SQLCount = "SELECT count(*)
                FROM requirements r
                $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];

        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function addRequirementDoc($data)
    {
        $this->db->insert('requirements', $data);
        return $this->db->insert_id();
    }

    public function approveProjectTask($data)
    {
        $this->db->where('id_project_task',$data['id_project_task']);
        $this->db->update('project_task',$data);
    }

    public function getRequirementTask($data)
    {
        $this->db->select('*');
        $this->db->from('project_task');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['requirement_id']))
            $this->db->where('requirement_id',$data['requirement_id']);
        if(isset($data['task_name']))
            $this->db->where('task_name',$data['task_name']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function deleteRequirement($data)
    {
        $this->db->where('id_requirements',$data['requirement_id']);
        $this->db->delete('requirements');
    }

    public function getAllRequestMoreTimeGrid($paramArr) {
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sidx'])?$paramArr['sidx']:'Id_request_more_time';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        if($sortField=='date'){
            $sortField = " STR_TO_DATE(w.date,'%Y-%m-%d 00:00:00') ";
        }else if($sortField=='name'){
            $sortField = " u.first_name ";
        }else if($sortField == 'requested_by'){
            $sortField = " u1.first_name ";
        }else if($sortField=='status'){
            $sortField = " r.status ";
        }else if($sortField=='requirement_name'){
            $sortField = " re.requirement_name ";
        }else{
            $sortField = " Id_request_more_time ";
        }

        $whereClause = " where true ";
        $whereClause .= " AND r.assigned_to= ".$this->session->userdata('user_id')." ";

        if(!empty($whereParam) || $whereParam!='')  {
            $whereParam = specialCharReplace($whereParam);

            //$whereClause.= " AND (DATE_FORMAT(r.created_date_time, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') )";

            $whereClause.= "and ( p.project_name like '%".$whereParam."%'
                 OR re.requirement_name LIKE '%".$whereParam."%'
                 OR pt.task_name LIKE '%".$whereParam."%'
                 OR u.first_name LIKE '%".$whereParam."%'
                 OR u.last_name LIKE '%".$whereParam."%'
                 OR r.admin_comments like '%".$whereParam."%'
                 OR r.status  like '%".$whereParam."%' )";

        }


        //$whereClause = " r.approved_by=".$this->session->userdata('user_id')." ".$whereParam;
        $groupBy = ' GROUP BY Id_request_more_time ';

        $SQL = "select p.project_name,re.requirement_name,pt.task_name,CONCAT(u.first_name,' ',u.last_name) as user_name,CONCAT(u1.first_name,' ',u1.last_name) as assigned_to,r.*,TIME_FORMAT(tf.estimated_time,'%H:%i') as allotted_time,DATE_FORMAT(r.created_date_time, '%d-%m-%Y') as requested_on
                from request_more_time r
                left join project_task pt on r.project_task_id = pt.id_project_task
                left join task_flow tf on r.task_flow_id = tf.id_task_flow
                left join requirements re on pt.requirement_id=re.id_requirements
                left join project p on pt.project_id = p.id_project
                LEFT join user u on r.employee_id = u.id_user
                LEFT join user u1 on r.assigned_to = u1.id_user
                $whereClause $groupBy order by $sortField $sortOrder $optLimit ";
        //echo $SQL; exit;
        $SQLCount = "SELECT count(*)
                from request_more_time r
                left join project_task pt on r.project_task_id = pt.id_project_task
                left join task_flow tf on r.task_flow_id = tf.id_task_flow
                left join requirements re on pt.requirement_id=re.id_requirements
                left join project p on pt.project_id = p.id_project
                LEFT join user u on r.employee_id = u.id_user
                LEFT join user u1 on r.assigned_to = u1.id_user
                $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        //echo "<pre>"; print_r($SQL); exit;
        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function updateRequestMoreTime($data)
    {
        $this->db->where('Id_request_more_time',$data['Id_request_more_time']);
        $this->db->update('request_more_time',$data);
        return 1;
    }

    public function getTaskTimeDetailsByUserId($data)
    {
        $this->db->select('tw.id_task_flow,
        TIME_FORMAT(tm.allowted_time,"%H:%i") as allowted_time,
        IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(lts.duration))),"%H:%i"), "") as actual_time,
        TIME_FORMAT(SEC_TO_TIME(TIME_TO_SEC(tm.allowted_time) - SUM(TIME_TO_SEC(IFNULL(lts.duration,"00:00")))),"%H:%i") as remain_time,
        tm.task_status as current_status,(TIME_TO_SEC(tm.allowted_time) - SUM(TIME_TO_SEC(IFNULL(lts.duration,"00:00")))) as remain_seconds');
        $this->db->from('task_flow tw');
        $this->db->join('task_member tm','tw.id_task_flow=tm.task_flow_id','left');
        $this->db->join('log_time lts','lts.task_flow_id = tw.id_task_flow and tm.assigned_to=lts.user_id','left');
        if(isset($data['project_task_id']))
            $this->db->where('tw.project_task_id',$data['project_task_id']);
        if(isset($data['user_id']))
            $this->db->where('tm.assigned_to',$data['user_id']);
        if(isset($data['id_task_flow']))
            $this->db->where('tw.id_task_flow',$data['id_task_flow']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function isProjectEmployee($data){
        $this->db->select('*');
        $this->db->from('project_employees');
        if(isset($data['project_id']))
            $this->db->where('project_id',$data['project_id']);
        if(isset($data['user_id']))
            $this->db->where('employee_id',$data['user_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTaskCompleteTime($data)
    {
        $this->db->select('TIME_FORMAT(pt.estimated_time,"%H:%i") as task_time,IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(tf.estimated_time))),"%H:%i"),"00:00") as estimated_time,
			 IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(tf.additional_time))),"%H:%i"),"00:00") as additional_time,
             IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(tf.estimated_time))+SUM(TIME_TO_SEC(tf.additional_time))), "%H:%i"),"00:00") as total_time');
        $this->db->from('project_task pt');
        $this->db->join('task_flow tf','tf.project_task_id=pt.id_project_task','left');
        if(isset($data['project_task_id']))
            $this->db->where('pt.id_project_task',$data['project_task_id']);
        if(isset($data['department_id']))
            $this->db->where('tf.department_id',$data['department_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTaskLogTime($data)
    {
        $this->db->select('IFNULL(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(lt.duration))), "%H:%i"), "00:00") as actual_time');
        $this->db->from('log_time lt');
        $this->db->join('task_flow tf','tf.id_task_flow=lt.task_flow_id','left');
        if(isset($data['project_task_id']))
            $this->db->where('lt.project_task_id',$data['project_task_id']);
        if(isset($data['department_id']))
            $this->db->where('tf.department_id',$data['department_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRequestMoreTimeTaskFlow($data)
    {
        $this->db->select('*');
        $this->db->from('request_more_time r');
        $this->db->join('task_flow t','r.task_flow_id=t.id_task_flow','left');
        if(isset($data['Id_request_more_time']))
            $this->db->where('r.Id_request_more_time',$data['Id_request_more_time']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLogTimeByDeptGrid($paramArr) {
        $whereParam = '';
        $page = isset($paramArr['page'])?$paramArr['page']:NULL;
        $limit = isset($paramArr['rows'])?$paramArr['rows']:NULL;
        $sortField = isset($paramArr['sidx'])?$paramArr['sidx']:'Id_request_more_time';
        $sortOrder = isset($paramArr['sord'])?$paramArr['sord']:'desc';
        $whereParam = isset($paramArr['searchParam'])?$paramArr['searchParam']:NULL;
        $offset = ($page - 1) * $limit;
        if(!empty($limit)) $optLimit = "limit $offset,$limit";
        else $optLimit = NULL;

        /*if($sortField=='date'){
            $sortField = " STR_TO_DATE(w.date,'%Y-%m-%d 00:00:00') ";
        }else if($sortField=='name'){
            $sortField = " u.first_name ";
        }else if($sortField == 'requested_by'){
            $sortField = " u1.first_name ";
        }else if($sortField=='status'){
            $sortField = " r.status ";
        }else if($sortField=='requirement_name'){
            $sortField = " re.requirement_name ";
        }else{
            $sortField = " Id_request_more_time ";
        }*/

        $whereClause = " where true ";
        /*$whereClause .= " AND r.assigned_to= ".$this->session->userdata('user_id')." ";*/

        /*if(!empty($whereParam) || $whereParam!='')  {
            $whereParam = specialCharReplace($whereParam);

            //$whereClause.= " AND (DATE_FORMAT(r.created_date_time, '%Y-%m-%d')=STR_TO_DATE('".$whereParam."','%e-%b-%Y') )";

            $whereClause.= "and ( p.project_name like '%".$whereParam."%'
                 OR re.requirement_name LIKE '%".$whereParam."%'
                 OR pt.task_name LIKE '%".$whereParam."%'
                 OR u.first_name LIKE '%".$whereParam."%'
                 OR u.last_name LIKE '%".$whereParam."%'
                 OR r.admin_comments like '%".$whereParam."%'
                 OR r.status  like '%".$whereParam."%' )";

        }*/

        $groupBy = ' GROUP BY Id_request_more_time ';

        $SQL = "select p.project_name,re.requirement_name,pt.task_name,CONCAT(u.first_name,' ',u.last_name) as user_name,CONCAT(u1.first_name,' ',u1.last_name) as assigned_to,r.*,TIME_FORMAT(tf.estimated_time,'%H:%i') as allotted_time,DATE_FORMAT(r.created_date_time, '%d-%m-%Y') as requested_on
                from request_more_time r
                left join project_task pt on r.project_task_id = pt.id_project_task
                left join task_flow tf on r.task_flow_id = tf.id_task_flow
                left join requirements re on pt.requirement_id=re.id_requirements
                left join project p on pt.project_id = p.id_project
                LEFT join user u on r.employee_id = u.id_user
                LEFT join user u1 on r.assigned_to = u1.id_user
                $whereClause $groupBy order by $sortField $sortOrder $optLimit ";
        //echo $SQL; exit;
        $SQLCount = "SELECT count(*)
                from request_more_time r
                left join project_task pt on r.project_task_id = pt.id_project_task
                left join task_flow tf on r.task_flow_id = tf.id_task_flow
                left join requirements re on pt.requirement_id=re.id_requirements
                left join project p on pt.project_id = p.id_project
                LEFT join user u on r.employee_id = u.id_user
                LEFT join user u1 on r.assigned_to = u1.id_user
                $whereClause";
        $queryCount = $this->db->query($SQLCount);
        $resultCount = $queryCount->row_array();
        $total_count = $resultCount['count(*)'];
        //echo "<pre>"; print_r($SQL); exit;
        $result = $this->db->query($SQL);

        if($result->num_rows() > 0) {
            $custlist = $result->result();
            return array('rows'=>$custlist,'total'=>ceil($total_count/$limit),'page'=>$page);
        } else {
            return array();
        }
    }

    public function getAdditionalTimeByRequirement($data)
    {
        $this->db->select('TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(tf.additional_time))),"%H:%i") as additional_time');
        $this->db->from('requirements r');
        $this->db->join('project_task pt','r.id_requirements=pt.requirement_id','left');
        $this->db->join('task_flow tf' ,'pt.id_project_task=tf.project_task_id','left');
        if(isset($data['requirement_id']))
            $this->db->where('r.id_requirements',$data['requirement_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getStatusWiseTaskCount($data)
    {
        $this->db->select('pt.`status`,count(*) as count');
        $this->db->from('project_task pt');
        if(isset($data['requirement_id']))
            $this->db->where('pt.requirement_id',$data['requirement_id']);
        $this->db->where('pt.pm_approved',1);
        $this->db->group_by('pt.status');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectDetails($data)
    {
        $this->db->select('*');
        $this->db->from('project');
        if(isset($data['project_id']))
            $this->db->where('id_project',$data['project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getHolidaysByDate($data)
    {
        $this->db->select('*');
        $this->db->from('holidays');
        if(isset($data['start_date']) && isset($data['end_date']))
            $this->db->where('DATE_FORMAT(date,"%Y-%m-%d") BETWEEN "'.$data['start_date'].'" and "'.$data['end_date'].'"');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectYearlyStatus($projectId, $startDate)
    {
        $start_date = strtotime($startDate);
        $start_date = strtotime('first day of this month', $start_date);
        $end_date = strtotime(date("Y-m-t", strtotime('this month')));
        $sdate = '';
        $edate = '';
        $final = [];
        $index = 1;

        for($date = $start_date; ($date <= $end_date && strtotime($date. ' +15 days')<=strtotime(date("Y-m-t", $date))); $date = strtotime(date('Y-m-d', $date). ' +15 days'))
        {
            if($index!=2){
                if($sdate == ''){
                    $sdate = date('Y-m-d', $start_date);
                    $edate = date('Y-m-d', strtotime(date('Y-m-d', $start_date). ' +15 days'));
                }
                else{
                    $sdate = date('Y-m-d', $edate);
                    $edate = date('Y-m-d', $date);
                }

                $this->db->select('
                IFNULL(sum(if(t.task_status=\'progress\',1,0)),0) progress_count,
                IFNULL(sum(if(t.task_status=\'completed\',1,0)),0) completed_count,
                IFNULL(sum(if(t.task_status=\'approval_waiting\',1,0)),0) approval_waiting_count,
                IFNULL(sum(if(t.task_status=\'new\',1,0)),0) new_count');
                $this->db->from('task_member t');
                $this->db->join('task_flow tf','t.task_flow_id=tf.id_task_flow','left');
                $this->db->join('project_task pt','tf.project_task_id=pt.id_project_task','left');
                $this->db->where("STR_TO_DATE(t.created_date_time,'%Y-%m-%d') between STR_TO_DATE('".$sdate."','%Y-%m-%d') AND STR_TO_DATE('".$edate."','%Y-%m-%d') ");
                $this->db->where('pt.project_id', $projectId);
                $query = $this->db->get();
                $res = $query->result_array();
                $final[$index] = array(
                    'data'=> array(
                        'progress_count'=> $res[0]['progress_count'],
                        'completed_count'=> $res[0]['completed_count'],
                        'approval_waiting_count'=> $res[0]['approval_waiting_count'],
                        'new_count'=> $res[0]['new_count'],
                    ),
                    'range'=> array(
                        'start' =>date('d-M-Y', strtotime($sdate)),
                        'end' =>date('d-M-Y', strtotime($edate)),
                    )
                );
                $edate = strtotime($edate);
            }
            $index++;
        }
        /*echo '<pre>';print_r($final);exit();*/
        return $final;
    }

    public function addNotification($data)
    {
        $this->db->insert('notifications', $data);
        return $this->db->insert_id();
    }

    public function updateNotification($data)
    {
        $this->db->where('id_notification',$data['id_notification']);
        $this->db->update('notifications',$data);
        return 1;
    }

    public function getNotifications($data)
    {
        $this->db->select('*');
        $this->db->from('notifications');
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        if(isset($data['read_flag']))
            $this->db->where('read_flag',$data['read_flag']);
        $this->db->order_by('id_notification','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
}