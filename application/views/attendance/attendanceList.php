<div class="mainpanel" id="budgetTemplateTbl_wrapper">
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important">
        <div class="contentHeader">
            <h3>Attendance</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>
                        Attendance list
                    </h3>
                </div>
                <div class="padding0">
                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">
                        <div class="col-sm-10 padding0">
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container">
                                        <div class="input_container">
                                            <input type="text" placeholder="dd/mm/yyyy" id="sdate" value="<?=date('d/m/Y',strtotime("-1 days"))?>"
                                                   name="sdate" class="rval form-control tssDatepicker">
                                        </div>
                                    </div>
                                    <label class="control-label col-sm-12">Date:</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group padding0">
                                    <div class="multi-select-container">
                                        <div class="input_container">
                                            <input type="text" placeholder="dd/mm/yyyy" id="edate" value="<?=date('d/m/Y',strtotime("-1 days"))?>"
                                                   name="edate" class="rval form-control tssDatepicker">
                                        </div>
                                    </div>
                                    <label class="control-label col-sm-12">Date:</label>
                                </div>
                            </div>
                            <div class="col-sm-6 mt15">
                                <div class="form-group padding0">
                                    <div class="multi-select-container">
                                        <select id="employeeDropdown" class="form-control rval" multiple="multiple" includeselectalloption="true" enablefiltering="true">
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-12">Employee:</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <button onclick="validateReportsForm(0,0)" class="button button-common module mt20" div-submit="true" id=""> Search </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth bg-white padding0">
                        <div class="grid-details-table">
                            <div class="grid-details-table-header">
                                <h3>&nbsp;</h3>
                                <input type="text" class="searchInput pull-right" id="" /><a class="gridSearch pull-right" href="javascript:;" title="Search"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="grid-details-table-content clearfix padding0">
                                <div class="tbl_wrapper border0">
                                    <table id="attendance-list" class="table-responsive"></table>
                                    <div id="attendance-list-page"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="add-attendance-modal" class="modal-wrapper" style="display:none; width: 200px;">
        <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form" >
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns"> <a aria-hidden="true" data-dismiss="modal" href="#"><i class="glyphicon glyphicon-remove"></i></a> </div>
                        <h4 class="panel-title">Add Attendance</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 clearfix clearboth padding0">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input_container">
<!--                                        <input type="text" class="form-control" name="name"  id="name" disabled="disabled"/>-->
                                        <label name="name" id="name"></label>
                                    </div>
                                    <label class="control-label"><span class="req-str">*</span>Task Type Name :</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input_container">
                                        <label name="emp_code" id="emp_code"></label>
                                    </div>
                                    <label class="control-label"><span class="req-str">*</span>Employee Code :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 clearfix clearboth padding0">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input_container">
                                        <input type="text" class="form-control msk_time rval" name="start_time"  id="start_time"/>
                                    </div>
                                    <label class="control-label"><span class="req-str">*</span>Log In Time :</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input_container">
                                        <input type="text" class="form-control msk_time rval" name="end_time"  id="end_time"/>
                                    </div>
                                    <label class="control-label"><span class="req-str">*</span>Log Out Time :</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 clearfix clearboth padding0">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input_container">
                                        <select id="status" name="status" class="form-control rval">
                                            <option value="">Select</option>
                                            <option value="present">Present</option>
                                            <option value="absent">Absent</option>
                                        </select>
                                    </div>
                                    <label class="control-label"><span class="req-str">*</span>Status :</label>
                                </div>
                            </div>
                        </div>
                        <div id="swapDetails">

                        </div>
                        <input type="hidden" class="form-control" name="id_swipe_punch" value=""/>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="button-common" div-submit="true">Save</button>
                        <button class="button-color" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        var $refTblgrid = $('#attendance-list');
        TssLib.docReady(function () {
            /*$('#date').val(new Date().format('d/m/Y'));
            TssLib.datepickerBinder('#reports_frm');*/
            $(".msk_time").mask("Hh:Mm");

            postJsonAsyncWithBaseUrl("User/getAllUsers", {}, {
                jsonContent: true,
                callback: function (result) {
                    if (result.data != null) {
                        var employee = result.data;
                        TssLib.populateSelect($('#employeeDropdown'), {
                            success: true,
                            data: employee
                        }, 'email', 'id_user');
                        TssLib.selectAllOptions('#employeeDropdown');
                        setTimeout(function(){ validateReportsForm(0); },1000);
                    }
                }
            });

            $refTblgrid.jqGrid({
                url: TssConfig.TT_SERVICE_URL + 'Attendance/getAttendanceGrid',
                multiselect: false,
                datatype: "json",
                sortorder: "desc",
                extSearchField: '.searchInput',
//                colNames: ['id_attendance', 'id_swipe_punch', 'user_id' , 'Name', 'Emp Code', 'Start Time', 'End Time', 'Total Duration', 'In Time', 'Out Time', 'duration', 'Status'],
                colNames: ['id_attendance', 'id_swipe_punch', 'user_id' , 'Name', 'Emp Code', 'Date', 'Start Time', 'End Time', 'Total Duration', 'Status'],
                colModel: [
                    { name: 'id_attendance', index: 'id_attendance', hidden: true ,key:true},
                    { name: 'id_swipe_punch', index: 'id_swipe_punch', hidden: true ,key:false},
                    { name: 'user_id', index: 'user_id', hidden: true ,key:false},
                    { name: 'name', index: 'name', hidden: false ,key:false},
                    { name: 'emp_code', index: 'emp_code', hidden: false ,key:false},
                    { name: 'attend_date', index: 'attend_date', hidden: false ,key:false},
                    { name: 'start_time', index: 'start_time', hidden: false },
                    { name: 'end_time', index: 'end_time', hidden: false },
                    { name: 'total_duration', index: 'total_duration', hidden: false, sortable:false },
                    { name: 'status', index: 'status', hidden: false },
                ],
                pager: 'attendance-list-page'
            }).navGrid('#attendance-list-page', {
                edit: true, add: false, del: false, search: false, refresh: true,
                addfunc: function () {
                    addEditAttendance();
                }, editfunc: function (id) {
                    var rowData = $refTblgrid.jqGrid('getRowData', id);
                    addEditAttendance(rowData);
                }, delfunc: function (id) {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        postJsonAsyncWithBaseUrl("Task_types/deleteTaskTypesById", {'task_type_id': id}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data != null) {
                                    if(result.data.status){
                                        TssLib.notify('Deleted successfully', null, 5);
                                        $('#task-type-list').trigger('reloadGrid');
                                    }else{
                                        TssLib.notify(result.data.message, 'warn', 5);
                                    }
                                }
                            }});
                    }, 'Yes', 'No');
                }
            });
        });
        function addEditAttendance(rowData) {
            var $modal = TssLib.openModel({ width: '600' }, $('#add-attendance-modal').html());
            if (TssLib.isBlank(rowData)) {
                $modal.find('.panel-title').text('Add Attendance');
                $modal.find('[div-submit="true"]').text('Save');
            } else {
                TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
                $modal.find('.panel-title').text('Edit Attendance');
                $modal.find('[div-submit="true"]').text('Update');
                $.post("<?=site_url('Attendance/getSwapTimeByAttendanceId')?>" , {'attendance_id':rowData.id_attendance, edit: 1 }, function( data ) {
                    $modal.find('#swapDetails').html(data);
                    $(".msk_time").mask("Hh:Mm");
                });
            }
            TssLib.ajaxForm({
                jsonContent: true,
                form: $('#linen-createSchedule-form'), callback: function (result) {
                    if(result.data.data == true){ var addedTest = 'Updated'; }else{var addedTest = 'Added';}
                    if (result.data.status) {
                        $refTblgrid.trigger('reloadGrid');
                        TssLib.closeModal();
                        TssLib.notify('Task Type '+addedTest+' successfully ', null, 5);
                    }else{
                        TssLib.notify(result.data.message, 'warn', 5);
                    }
                },
                before: function (formObj) {
                    var status = $('#status').val();
                    console.log(status);
                    var swapdata = [];
                    var temp = {};
                    var collection = $(".swap");
                    if(status && collection.length > 0){
                        collection.each(function() {
                            var This = $(this);
                            var id = This.attr('data-id');
                            var swapid = This.attr('data-swap-id');
                            if(typeof temp.swapid === 'undefined' || temp.swapid != swapid) {
                                temp = {'id': id, 'swapid': swapid};
                            }
                            if(This.attr('data-swapin') == 1){
                                temp.swapin = This.val();
                            }else{
                                temp.swapout = This.val();
                                swapdata.push(temp);
                            }
                        });
                    }
                    if(swapdata.length <= 0 && status){ TssLib.notify('No Swap time found', 'warn', 5); return false; }

                    $(formObj).data('additionalData', { id_attendance: rowData.id_attendance, user_id: rowData.user_id, status: status, swipe: swapdata });
                    $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Attendance/addAttendance');
                    return true;
                }
            });
        }
        function sendRequest(formObj, rowData){
            $(formObj).data('additionalData', { id_attendance: rowData.id_attendance, user_id: rowData.user_id});
            $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Attendance/addAttendance');
            return true;
        }
        function validateReportsForm(type) {
            $('#sdate').removeClass('err-bg');
            $('#edate').removeClass('err-bg');
            $('#employeeDropdown').removeClass('err-bg');
            $('.error-msg').remove();

            var flag = 0;

            var user_id = $('#employeeDropdown').val();
            var sdate = $('#sdate').val();
            var edate = $('#edate').val();

            if (user_id == null) {
                $('#employeeDropdown').addClass('err-bg');
                $('#employeeDropdown').parent().before('<span class="error-msg">Required</span>');
                flag++;
            }

            if (flag == 0) {
                if (type == 0) {
                    var myPostData = $refTblgrid.jqGrid("getGridParam", "postData");
                    myPostData['user_id'] = user_id;
                    myPostData['sdate'] = sdate;
                    myPostData['edate'] = edate;
                    $refTblgrid.setGridParam({'postData': myPostData});
                    $refTblgrid.trigger("reloadGrid");

                    return false;
                }
                else {
                    $('#reports_frm').submit();
                }
            }
        }
    </script>
    <!-- InstanceEndEditable -->

</div>
<!--Render Body End-->