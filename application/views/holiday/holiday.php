<!-- Modal Dialog  dekete confirmation-->
<div class="modal fade" id="oasis-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" data-dismiss="modal" aria-hidden="true" title="Close">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"></h4>
                </div>
                <div class="panel panel-default panel-body padding20 clearfix">
                    <p></p>
                </div>
                <div class="panel-footer col-sm-12 clearfix text-right">
                    <button type="button" class="button-common module btn-danger">Yes</button>
                    <button type="button" class="button-common button-color btn-default" data-dismiss="modal">No
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mainpanel holiday-wrap" >
    <div class="content-wrapper padding0 main-work-area" style="overflow:visible !important;">
        <div class="contentHeader">
            <h3>Holiday Info</h3>
        </div>
        <div class="col-sm-12 clearfix clearboth pad-right-none">
            <div class="grid-details-table">
                <div class="grid-details-table-header">
                    <h3>Holiday Upload</h3>
                </div>
                <div class="padding0">

                    <div class="col-sm-12 clearboth clearfix pb20 bg-white">

                        <div id="priorities-form" method="POST" class="form-horizontal NewErrorStyle animate-fld-bg">
                            <div>
                                <div class="panel panel-default">
                                    <script>
                                        <?php if($this->session->userdata('message')){ ?> TssLib.notify('<?=$this->session->userdata('message')?>', null, 5); <?php $this->session->unset_userdata('message'); } ?>
                                    </script>
                                    <h4 class="section-heading">&nbsp;</h4>
                                    <form id="excelUploadForm" enctype="multipart/form-data" method="post"
                                          action="<?php echo site_url('/Holiday/excelHolidayUpload') ?>">

                                        <div class="col-sm-12 clearfix">
                                            <div class="col-sm-6 pl0">
                                                <div class="fileupload fileupload-new input-group"
                                                     data-provides="fileupload">
                                                    <div class="form-control" data-trigger="fileupload"><i
                                                            class="glyphicon glyphicon-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span></div>
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileupload-new">Select file</span><span
                                                                class="fileupload-exists">Change</span>
                                                        <input type="file" id="file" name="file"></span>
                                                    <a href="#"
                                                       class="input-group-addon btn btn-default fileupload-exists"
                                                       data-dismiss="fileupload" id="removeFile">Remove</a>
                                                </div>
                                                <a class="link font-12 pl0"
                                                   href="<?= WEB_BASE_URL ?>uploads/sample_holiday_sheet.xlsx"
                                                   download target="_blank">Download Sample Excel File</a>
                                            </div>
                                            <div class="col-sm-4 pl0">
                                                <button class="button-common" type="submit" id="uploadExcel"> Upload
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="type" id="type" value="0">

                    </div>
                    <?php if (count($this->session->userdata('uploadError')) > 0){ ?>
                        <div class="col-sm-12 clearboth clearfix pb20">
                            <div class="panel-window border top15">
                                <div class="grid-details-table-content clearfix padding0">
                                    <div class="tbl_wrapper border0">
                                        <table id="project-list" class="table-grid-view">
                                            <thead>
                                            <tr>
                                                <th>Row No.</th>
                                                <!--<th>Employee Code</th>
                                                <th>Date</th>
                                                <th>Start Time</th>
                                                <th>End Time</th>
                                                <th>Duration</th>
                                                <th>Status</th>-->
                                                <th>Message</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $error = $this->session->userdata('uploadError');
                                            foreach ($error as $k => $v) { ?>
                                                <tr>
                                                    <td><?= $k ?></td>
                                                    <!-- <td><? /*= $v['emp_code']*/
                                                    ?></td>
                                                <td><? /*= $v['date']*/
                                                    ?></td>
                                                <td><? /*= $v['start_time']*/
                                                    ?></td>
                                                <td><? /*= $v['end_time']*/
                                                    ?></td>
                                                <td><? /*= $v['duration']*/
                                                    ?></td>
                                                <td><? /*= $v['status']*/
                                                    ?></td>-->
                                                    <td><?= $v['errorMsg'] ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }else if ($this->session->userdata('upload')){ ?>
                    <div>   <?php echo $this->session->userdata('upload') ?>
                        <?php }
                        $this->session->unset_userdata('uploadError');
                        $this->session->unset_userdata('upload'); ?>
                    </div>

                </div>
            </div>
            <div class="">
                <div class="col-sm-12 clearfix clearboth pad-right-none mt10">
                    <div class="grid-details-table">
                        <div class="grid-details-table-header">
                            <h3>&nbsp;</h3>
                            <input type="text" class="searchInput pull-right" id=""/><a class="gridSearch pull-right"
                                                                                        href="javascript:;"
                                                                                        title="Search"><i
                                    class="fa fa-search"></i></a>
                        </div>
                        <div class="grid-details-table-content clearfix padding0">
                            <div class="tbl_wrapper border0">
                                <table id="holiday-list" class="table-responsive"></table>
                                <div id="holiday-list-page"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="add-attendance-modal" class="modal-wrapper" style="display:none; width: 200px;">
    <div class="form-horizontal NewErrorStyle animate-fld-bg align-error" method="POST" id="linen-createSchedule-form">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns"><a aria-hidden="true" data-dismiss="modal" href="#"><i
                                class="glyphicon glyphicon-remove"></i></a></div>
                    <h4 class="panel-title">Add Holiday</h4>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input_container">
                                    <input type="text" class="form-control tssDatepicker rval" name="date" id="date"/>
                                </div>
                                <label class="control-label"><span class="req-str">*</span>Date :</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 clearfix clearboth padding0">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input_container">
                                    <input type="text" class="form-control rval" name="title" id="title"/>
                                </div>
                                <label class="control-label"><span class="req-str">*</span>Title :</label>
                            </div>
                        </div>
                        <div class="col-sm-12 clearfix">
                            <div class="form-group bt-default">
                                <label>Description</label>
                                <textarea name="description" id="description" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="id_holiday" value=""/>
                </div>
                <div class="panel-footer text-center">
                    <button class="button-common" div-submit="true">Save</button>
                    <button class="button-color" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    var $refTblgrid = $('#holiday-list');
    var logAttedList = 0;
    TssLib.docReady(function () {
//        loadDatatables();
        $refTblgrid.jqGrid({
            url: TssConfig.TT_SERVICE_URL + 'Holiday/getHolidayGrid',
            multiselect: false,
            datatype: "json",
            sortorder: "desc",
            extSearchField: '.searchInput',
            colNames: ['id_holiday', 'Date', 'Title', 'Description', 'future'],
            colModel: [
                {name: 'id_holiday', index: 'id_holiday', hidden: true, key: true},
                {name: 'date', index: 'date', hidden: false, key: false, width: 15},
                {name: 'title', index: 'title', hidden: false, key: false, width: 25},
                {name: 'description', index: 'description', hidden: false, key: false},
                {name: 'future', index: 'future', hidden: true, key: false},
            ],
            pager: 'holiday-list-page'
        }).navGrid('#holiday-list-page', {
            edit: true, add: true, del: true, search: false, refresh: true,
            addfunc: function () {
                addEditHoliday();
            }, editfunc: function (id) {
                var rowData = $refTblgrid.jqGrid('getRowData', id);
                if (rowData.future == '1') {
                    addEditHoliday(rowData);
                } else {
                    TssLib.closeModal();
                    TssLib.notify('Can\'t edit past holidays', 'warn', 5);
                }
            }, delfunc: function (id) {
                var rowData = $refTblgrid.jqGrid('getRowData', id);
                if (rowData.future == '1') {
                    TssLib.confirm('Delete Confirmation', 'Are you sure to delete?', function () {
                        $(this).closest('.modal').modal('hide');
                        postJsonAsyncWithBaseUrl("Holiday/deleteHolidayById", {'id_holiday': id}, {
                            jsonContent: true,
                            callback: function (result) {
                                if (result.data != null) {
                                    if (result.data.status) {
                                        TssLib.notify('Deleted successfully', null, 5);
                                        $refTblgrid.trigger('reloadGrid');
                                    } else {
                                        TssLib.notify(result.data.message, 'warn', 5);
                                    }
                                }
                            }
                        });
                    }, 'Yes', 'No');
                } else {
                    TssLib.closeModal();
                    TssLib.notify('Can\'t delete past holidays', 'warn', 5);
                }
            }
        });
        minScroll();
    });
    function addEditHoliday(rowData) {
        var $modal = TssLib.openModel({width: '600'}, $('#add-attendance-modal').html());
        if (TssLib.isBlank(rowData)) {
            $modal.find('.panel-title').text('Add Holiday');
            $modal.find('[div-submit="true"]').text('Save');
        } else {
            TssLib.renderData($modal.find('#linen-createSchedule-form'), rowData);
            $modal.find('.panel-title').text('Edit Holiday');
            $modal.find('[div-submit="true"]').text('Update');
        }
        TssLib.ajaxForm({
            jsonContent: true,
            form: $('#linen-createSchedule-form'), callback: function (result) {
                if (result.data.data == true) {
                    var addedTest = 'Updated';
                } else {
                    var addedTest = 'Added';
                }
                if (result.data.status) {
                    $refTblgrid.trigger('reloadGrid');
                    TssLib.closeModal();
                    TssLib.notify('Task Type ' + addedTest + ' successfully ', null, 5);
                    minScroll();
                } else {
                    TssLib.notify(result.data.message, 'warn', 5);
                }
            },
            before: function (formObj) {
                $(formObj).data('additionalData', {date: $('#date').val()});
                $(formObj).attr('action-send', TssConfig.TT_SERVICE_URL + 'Holiday/addHoliday');
                return true;
            }
        });
    }
</script>